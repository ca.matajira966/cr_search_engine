Question 34

How Can We Identify
Christians if We
Ignore God’s Law?

in this the children of God are manifest, and the chit-
dren of the devil: whosoever doeth not righteous-
hess is not of God, neither he that loveth not his
brother (I John 3:10).

The doing of-righteousness and the loving of the
Christian brother are inseparably linked by John. We
can identify. Christians and Satanists by means of
God's standards of righteousness.

Two questions immediately come to mind. First,
what are the specific details of righteousness, by
which we can identity other Christians? Second, what
is the meaning of “loving the brother"?

We have already surveyed the meaning of biblical
love. It means doing righteously, in terms of the speci-
fics of biblical law (Question 33). Thus, when John di-
tects our attention to righteousness as the identifying
characteristic of the Christian, he is directing our at-
tention to biblical law.

Sometimes righteousness doesn’t involve relation-
ships with Christian brothers. Does this destroy the
meaning of John’s message? Not at all. Doing right-
eously means doing unto others as we would have
them do unto us. This same rule governs aif our rela-
tionships with other men, Christian and non-Christian.
So “doing righteousness” means honoring the speci-
fics of God's law. If we fail to honor His law, then we
are separating ourselves from the family of God. We
are identifying with Satan.

105

Questionable Answer

“Since we are dead in Christ, we cannot die the se-
cond death. Thus, we are no longer obliged to respect
Old Testament law. Christ has paid the price. We live
as free men—free from the burden of the law!”

My Reply: Yes, Christ paid the price. The death
sentence which sin placed over us—a death sentence
manifested by the law (but not created by it)—is re-
moved. Our Father in Heaven has punished Christ
rather than us. We are adopted ethically back into the
regenerate family of God. No longer are we disinheri-
ted sons. But should adopted sons turn around and
transgress the very laws that manifested their condi-
tion as disinherited sons?

We are Still fo respect Old Testament law, even
though we don’t fear its post-resurrection conse-
quences. For one thing, we know its pre-resurrection
consequences. There are earthly blessings associ-
ated with obedience to the law, and earthly curses
associated with its transgression (Deuteronomy 28).
But far more important, God wrote those laws, and
the Father judged His Son for our transgression of
them.

Throughout First John, we read that our ability to
test our salvation—though not the foundation of our
salvation—is based on the existence of Christ's com-
mandments. Paul's words aren't opposed to John’s.
Paul respected the law as a guideline for personal per-
formance, as John did. Do you? If not, why not?

 

For further study: | Jonn 3-6; 3:4, 10, 22-24; 5-2-3.

14

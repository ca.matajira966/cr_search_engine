“God's law was spiritual for the period of the Old Testa-
ment, but to regard it as ethically binding on Christians
in the New Testament era is to make it carnal. Men are
given the spirit of life today, not the spirit of bondage.”

 

My Reply: Did faithful saints.in the Old Testament
experience release from the ethical bondage of sin?
Yes. Did they still place themselves under Old Testa-
ment law as a moral guide? Paul said that he did, but
the law siew him. Why? Because he was not yet
regenerate. Even in his regenerate state, he saw the
war between his redeemed spirit and his sinful lusts,
called “the flesh.” There were two /aws warring in him:
the faw of God and the faw of sin. “But | see another
law in my members, warring against the law of my
mind, and bringing me into captivity to the law of
sin which is in my members” (v. 23). He cried out,
“Who shall deliver me from the body of this death?”
{v. 24). Then he answered his own question—an an-
swer which contrasts the two laws in his life: “l thank
God through Jesus Christ our Lord. So then with
the mind | myself serve the law of God; but with the
flesh, the law of sin” (v. 25).

Are there two different laws? Subjectively, yes. Ob-
jectively, no. Paul's “old man” was still being killed by
the sentence of God’s law. But his renewed mind was
given life by that same law code.

If we wish to subdue the “old man” in our tives, we
must use God's law to do sa, just as Paul did. There is
no other way.

For further stus

 

's. 119:21; Rom. 3:20;

 

5 | John 3:14,

 

126

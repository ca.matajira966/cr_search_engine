tuestn 47

How Can We “Mortify
the Flesh” if We
Disobey God’s Law?

For if ye live after the flesh, ye shal! die: but if ye
through the Spirit do mortity the deeds of the body,
ye shall live. For as many as are led by the Spirit of
God, they are the sons of God (Romans 8:13-14),

Paul contrasts two different minds: carnal and spir-
itual. When we read “flesh,” we must interpret it ac-
cordingly. This doesn’t mean that Paut was some sort
of materialistic determinist. He didn't mean that our
decisions are a function of our chemistry, or that our
bodies control our minds. He meant that there are two
warring principles of interpretation: the flesh and the
spirit. This same contrast was present in the Garden
of Eden.

When a person is unregenerate, his attempts to live
consistently according to God’s law must fail. The war
of his carnal mind against the law of God delivers him
into death. Sin captures him, Paul said. But a
regenerate man progressively conquers the lusts of
the flesh. How? Through the empowering of the Holy
Spirit—the Spirit's action in helping him to apply the
principles of biblicat law to every area of life.

What Paul describes is a war. Wars involve conflict.
Wars also demand self-discipline in terms of a rule
book, The carnal mind has its rule book: defy God and
His law. The spiritual mind has its rule book: honor
God and His Jaw. We follow “the Spirit of truth” who
leads us into ail truth (John 16:13). Does this Spirit call
God's law (which He co-authored) a lie?

131

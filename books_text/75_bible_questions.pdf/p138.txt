 

Questionable Answer

“Paul used this obscure rule only as an example. He did
not mean that it would always be immoral to keap oxen
from eating corn in the fields, So it is with all of the Old
Testament laws: they are only moral examples.”

 

My Reply: Quite true: they are indeed moral. ex-
amples. Question: Do we honor a moral example by
violating its explicit terms? Do we honor this principle
by. muzzling oxen? Isn't the general principle more
likely to become obscured to the minds of sinful men
than the explicit examples? Don't the explicit ex-
amples keep our eyes fixed on the true meaning of
the general principles of equity? /f we close our eyes
to the specific details of the example, how will we be
certain that we are honoring the general principle in
other cases? After all, this is precisely what Paul
warned Timothy about: Christians who were unwilling
to support the ministry of the church leaders.

We violate the genera! principle when we violate the
details. A so-called “morat exampie’” is vatid morally
precisely because it is morally binding. if it were not
morally binding, how could it be a moral example?
Are we to honor the spirit of the law while breaking the
explicit example? Aren't the examples binding if the
general principle is binding? And if the examples
aren't binding, what will pull men back from evil inter-
pretations?

Once again, we must ask that crucial question: By
what standard?

For further stud:
0 Cor. 616-48;

 
 

134

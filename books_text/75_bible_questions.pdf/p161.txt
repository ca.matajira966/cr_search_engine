question OF

Didn’t Christ's Kingdom
Begin Before the
Crucifixion?

If 1 cast out devils by the Spirit af God, then the
kingdom of God is come unto you (Matthew 12:28).

The paraile! passage in Luke is even more interest-
ing: “But if | with the finger of God cast out devils,
no doubt the kingdom of God is come upon you”
(41:20). Jesus continued to cast out devils, thereby an-
nouncing the arrival of His kingdom. The apostles pos-
sessed the same ability, indicating the continuing
power of Gad’s kingdom over His enemy, Satan.

The kingdom began with a display of spiritual power.
But this spiritual power had implications for society. It
meant a reduction in the power and influence of Satan,
as in the case of the converted Ephesian magicians
who brought their books together and burned them,
valued at 50,000 pieces of silver (Acts 19:19)—a huge
sum in the first century, A.D. (Judas betrayed Christ for
30 pieces.) “So mightily grew the word of God and
prevailed” (Acts 19:20). The emphasis on prevailing,
on overcoming, is basic to the Book of Acis.

The early apostles were invading Satan's. kingdom.
Satan now occupies land as a squatier. We have the re-
sponsibility of casting his fellow demons out of people,
and casting his influence out of society. There may not
seem to be many demon-possessed people, but demon-
possessed cultures are everywhere. Where God's word
is rejected as the proper way of life, for whatever rea-
son, there we find a form of demonic influence.

157

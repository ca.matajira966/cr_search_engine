 

Questionable Answer

“The kingdom began with Jesus’ ministry. But this king-
dom was interrupted with His crucifixion. The kingdom
which operates today is a spiritual kingdom only, not
the external, civil kingdom which was prophesied in
the Old Testament.”

 

My Reply: If the kingdom has come, which is what
Jesus said, then how could it have been cut off by His
crucifixion? Throughout the New Testament, the
kingdom is discussed in terms of the leaven principle:
growth, maturity, expansion. There is never any dis-
cussion of possible sharp breaks, either as a result of
Christ's crucifixion or because of some discontinuous
break prior to the return of Christ to set tip a millennial
kingdom. What is always pictured by Jesus is a king-
dom marked by continuity.

What are. we to do as members of His kingdom? We
are-to preach the gospel and display the fruits of right-
eousness. We are to serve as Salt in a savorless age, to
serve as lights in a dark age, and to serve as ambassa-
dors of a growing kingdom. If we do these things well,
and if God decides that the time is ripe, why shouldn't
our efforts produce visible, culturally significant trans-
formations, culture by culture? We expect the accept-
ance of the gospel to transform primitive tribes in the
jungle. Are we so corrupted by materialistic humanism
that we cannot see what changes are possible in our
civilization? Aren‘t moder humanist nations as much
in need of biblical transformation as primitive societies
are? Can't the gospel work in our societies, too?

 

For further study: Matt. 3:2; 4:17;

 

:7; 12:28; 16:28; Mark 1:15.

 

158

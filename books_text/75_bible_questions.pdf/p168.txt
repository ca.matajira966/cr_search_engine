Questionalie Answer

“The meek person does not seek power. He does not
try to lord it over other people. He goes meekly about
his affairs, not demanding anything from others. He in-
herits the whole earth immediately after the return of
Jesus to establish His earthly rule.”

My Reply: Can Mr. Christian Milquetoast earn over-
night how to run a business, command an army, run a
Congressional staff, be the president of a university, be
the chairman of a research laboratory, take responsibil-
ity for operating a $300 million investment trust, be the
editor of a newspaper, or any of a thousand other tasks
that demand forceful leadership? Isn't the development
of leadership a long-term process?

The question is: Before whom will a man be meek? If
he is meek before God, God says he will inherit the
earth. lf he is confident in his rebellion against God, he
will lose both the earth and his own soul. Satan wasn’t
meek before God; he will perish under the judgment of
God. Meekness is what we call an inescapable con-
cept. It’s not a question of “meekness vs. confidence,”
Its a question of meekness before whom or what?

Meekness is progressive. We learn to humble our-
selves before the Lord. We learn to conform our
dreams and our actions before God. But because
meekness is progressive, inheriting the earth is also
progressive. Self-disciptine brings authority. But this
authority is not to be confined to the institutional
church. It is comprehensive authority, forthe gospel’s
effects are comprehensive.

For further study: Ps. 37; 110; Isa. 4:

 

164

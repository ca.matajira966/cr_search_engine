Question 73

Didn't David Foresee
the Church Age?

For David is not ascended into the heavens: but he
saith himself, The LORD said unto my Lord, Sit thou
‘on my right hand, until | make thy foes thy footstool.
Therefore let all the house of israel know assuredly,
that Gad hath made that same Jesus, whom ye have
crucified, both Lord and Christ (Acts 2:34-36).

Peter called David “a prophet” (Acts 2:30). “There-
fore being a prophet, and knowing that God had
sworn with an oath to him, that of the fruit of his
loins, according to the flesh, he would raise up
Christ to sit on his throne; he seeing this before
spake of the resurrection of Christ, that his soul was
not left in hell, neither his flesh did see corruption”
(2:30, 31). Peter proclaimed Christ's Lordship to the
Jews, calling them to repentance. He did this just
after he had cited the tongues of Pentecost as the
fulfilment of Joel's prophecy.

Could anything be clearer? Peter believed that the
offer to the Jews to repent was still open (just as we
do today), and that they would have to enter the
church.of Jesus Christ, which had begun that morn-
ing (just as we say today). The church age had un-
mistakably begun that morning. The birthday of the
church was the morning of Pentecost! Yet here was
Peter, proclaiming Christ as Savior and Lord to the
Jews. Then he called them to repentance (2:38-40).
He galled them to join the church of Jesus Christ!

189

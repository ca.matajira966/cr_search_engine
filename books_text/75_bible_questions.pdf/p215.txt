APPENDIX A: HOW TO GET YOUR ANSWERS 211

You take the consequences if there are any. You may
wind up looking stupid, not them, and they still get
their question answered. So if you serve as a kind of
intermediary between the instructor and your equally
confused fallow students, no one is likely to get angry.

You must learn to assess their response. Part of
your responsibility is to keep from alienating them.
You want to uphold a cause, even if that cause is the
legitimacy of asking probing, controversial questions.
You must not abuse the privilege you have of being
able to ask an instructor questions. Use it to your ad-
vantage, and the advantage of other students. But
retreat from any direct confrontation unless other
students want to join in the fray on your side.

They did not pay to hear your ideas. They paid to
hear the instructors ideas. Only if you help them un-
derstand the implications of what he is saying are you
likely to impress them with your position.

6. The Rules of Classroom Discussion

Different professors prefer different teaching
methods. Some like to lecture without any interrup-
tions. Others prefer a “Socratic method” of questions.
Some permit a mixture. Be careful not ta violate the
familiar procedure of the classroom. But be warned:
an instructor who refuses to take questions is usually
extremely insecure intellectually. He is afraid. He may
react in a very hostile manner if you in any way dis-
turb. his normal presentation. It may sound amazing
to you now, but there are many teachers in class-
rooms who are barely. able to get by academically.
Tread carefully; the least competent instructor intel-
lectually can be the most dangerous.

If there is no way to get questions answered in class,

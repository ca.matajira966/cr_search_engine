APPENDIX B: THE BIBLE STUDY 249

Private. You should not do this except under extreme
duress. But if some intellectuafly insecure bureaucrat
or faculty member starts trying to twist your arm, you
may be able to end his career if you plan your strategy
carefully.

When bureaucrats understand that you know how
to fight, they probably will retreat. At this point, you
know more about how to fight than any previous
generation of students attending Christian colleges.
Don't let this knowledge go to waste. if you’re chal-
lenged to a fight, then fight, and fight to win. At the
very least, fight to inflict sufficient pain on your tor-
mMenters so that those who follow you won't have to
fight at aif. Repay evil with good, and bringing down a
tyrant is very, very good. (If you doubt this, read the
Book of Judges or the story of Elijah.) College ad-
ministrators are smart enough not to stick their hands.
into a hornets’ nest twice.

But if the school leaves you alone, then keep a very
low profile. Do no damage unto others if they do no
damage unto you or other innocent people.

ii. The Decision to Transfer Out

Some students grow so cynical in the face of poor
treatment by faculty members or the administration
that they are tempted to quit. They see that they aren't
getting anywhere academically, and that they are be-
ing pressured to give up their beliefs. Wouldn't it be a
lot easier to study somewhere else?

Generally, the answer is no. It’s probably just as bad
in other Christian colleges. There may even be
students contemplating a switch to your college in
order to escape theirs. You may trade one set of prob-
lems for a new set—a set you have less experience in
dealing with.

254 75 BIBLE QUESTIONS

Plant the seeds now: tall oaks should be there a gen-
eration from now. Conduct yourself accordingly.

if you have been poorly and unfairly treated, fret
not. Work harder to multiply the campus Bible stu-
dies. Carry on your work. Bitterness gets you no-
where. Much better to recruit another dozen students,
or to hand out another hundred books.

Cantact me today. Send a dollar for a three-month
subscription to my newsletter for Bible students,
Providential. It will contain lots of information on how
to study the Bible, how to ask sharp questions in
class, how to raise your grades, and best of all, a sec-
tion called “Whoppers.” | will reprint for all the world to
see the most preposterous, ridiculous answers to the
questions you ask. Just tell us the name of the pro-
fessor, his department, the day he said it, and his an-
swer. Sign it, and have another student sign it to con-
firm its accuracy, and send it in. I'l run the choicest
ones.

{f you subscribe. now, you will also receive three
months of Preface, a specialized Christian book re-
view newsletter that provides in-depth reviews of the
most important scholarly books that can help college
students in their course work. It specializes in history,
theology, social science, and education. Write:

Providential
Geneva Ministries
P.O. Box 8376
Tyler, TX 75711

Also, if you know the names and addresses of stu-
dents at other Christian colleges who would like to
receive a free copy of this book, send the list to us. If
you could send $2 per name, it would really help, but if

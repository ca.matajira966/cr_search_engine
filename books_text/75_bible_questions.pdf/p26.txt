Questionatie Answer

“The sovereignty of God really doesn’t mean predestin-
ation. it means that God is powerful, but in His mercy,
He allows men an area of free choice, so that in no
way is God the author of sin. Men are given free will
and free choice. He is fair because He voluntarily
limits His own total power.”

My Reply: Then what is the meaning of the phrase,
“vessels of wrath fitted to destruction” (v. 22)? What
is a vessel made by a potter unto—for the purpose of
—destruction? If it is not made for a purpose, is it ran-
dom? But Paul spoke of two sorts of vessels, with
both kinds made for respective purposes. Listen to
Isaiah’s warning to those theologians who place the
sovereignty of God in opposition to the fairness of
God:

Woe unto him that striveth with his Makert
Let the potsherd strive with the potsherds of
the earth. Shall the clay say to him that fashion-
eth it, What makest thou . . . ? (Isaiah 45:9).

Some questions should not be asked. One of them
is this one: “If God is omnipotent, how can He be fair
in judging men for their sins?” Another is: “If man is
morally responsible, how can God predestinate all
events?” Paul's answer: “Be silent!”

Test your commitment to the Bible. Are you trying to
judge the truth of the Bible by your own logic? Are you
disobeying Paul?

For further study: Job 9:12-15; 33:12-

 

22

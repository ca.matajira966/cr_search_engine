270 75 BIBLE QUESTIONS

sure just what kind of evidence you have. He can't
make a decision until he sees the evidence. He can
then decide whether to follow through on your com-
plaint. But if you have gone through all the steps—
instructor, dean, president, Board of Trustees—he
can’t very well reply that you haven't exhausted all
avenues of appeal. This makes it difficult for him to
push the matter back down to your college’s adminis-
tration, thereby delaying the investigation process.

Wait for his response. What you are after is a re-
quest to see the substantiating evidence. He may
even want a private conference. But he has to do
something. You have sent him a registered letter.
Students don’t do this very often. If he fails to reply,
send him another registered letter. Ask again for a
reply.

When you finally get his request to see the evi-
dence, send him photocopies. Never send the origi-
nals. It's best to keep the originals off campus. Maybe
you can rent 4 safety deposit box at a focal bank (you
will have to open an account).

The next step is his. Always send the material by
registered mail. This gives you a record of its having
been delivered. lt also shows that you are very
serious.

If you get nowhere, then your best tactic is to have
another student begin with his or her protest, starting
with an initial inquiry to the association. This time,
however, the complaining. student Should send a
Photocopy of the letter to the Director of the associa-
tion. What you want is the perception on the part of
the recipient that this problem isn’t going to go away.

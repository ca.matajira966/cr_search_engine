 

Productive Christians In an Age of Guilf-Manipulators
by David Chitton. 310 pp., indexed, bibliography, pb.,
$4.95. Institute for Christian Economics, Tyler, TX.

 

One of the most insidious attacks upon orthodox
Christianity has come from the so-called “Christian
Lett.” This book answers the “bible” of that movement,
Rich Christians in an Age of Hunger, by Ronald Sider.

David Chilton demonstrates that the “Christian So-
cialism” advocated by Sider is nothing more than bap-
tized humanism—the goal! of which is not charity, but
raw, police-state power.

The debate between Sider and Chilton centers on
one central issue: Does the Bible have clear guidelines
for every area of life? Sider claims that the Bible does
not contain “blueprints” for a social and economic
order. The catch, of course, is that Sider then pro-
vides his.own “blueprints” for society, calling for a tax-
ation system which is completely condemned by
God's infallible word. Chilton answers that the social-
ist “cure” is worse than the disease, for socialism ac-
tually increases poverty. Even when motivated by
good intentions, unbiblical “charity” programs will
damage the very people they seek to help.

Combining incisive satire with hard-hitting argu-
mentation and extensive biblical references, Chilton
shows that the Bible does have clear, forthright, and
workable answers to the problem of poverty. Produc-
tive Christians is most importantly a major introduc-
tion to the system of Christian Economics, with chap-
ters on biblical law, welfare, poverty, the third world,
overpopulation, foreign aid, advertising, profits, and
economic growth.

Every Thought Captive: A Study Manual for the
Defense of Christian Truth by Richard L. Pratt, Jr.
142 pp., pb., $4.50. Presbyterian and Reformed, Phil-
lipsburg, NJ.

Christians are commanded to “be ready to give an
answer’ to all men regarding the faith. In practice,
however, the standard “defense” of the faith has too
often compromised the claims of Christ and failed to
challenge the unbeliever. The Bible is presented as if
its authority is validated by the opinions of “experts.”
Jesus is subjected to the examination of autonomous
reason, in order to justify His claim to absolute lord-
ship. To put it bluntly: What often parades as a de-
fense of the faith is actually a betrayal of it!

in this important volume, Richard Pratt explains
how to present a truly biblical, God-honoring witness
for Christ. He shows that a faithful method of apolo-
getics must be grounded upon the absolute, unques-
tioned authority of the Bible. As Christians we may

. deal with every issue confronting people today, with
the complete confidence that the Bible is fully authori-
tative on everything it speaks of—and it speaks of
everything.

Mr. Pratt also shows how the non-Christian point of
view and the Christian point of view are in conflict,
and that this must be fully taken into account in every
discussion between them. As he demonstrates, the
unbeliever ultimately has no defense for his position.
Unbeliet is foolishess.

Every Thought Captive is a clearly written, forthright
exposition of the biblical principles of apologetics.
More than this, it is a down-to-earth training manual,
useful for both individual and group study.

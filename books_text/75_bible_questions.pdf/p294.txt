 

Backward, Christian Soldiers? An Action Manual
for Christian Reconstruction by Gary North. 320
pp., indexed, bibliography, pb., $4.95. Institute for
Christian Economics, Tyler, TX.

 

Jesus said to “Occupy till | come.” But if Christians
don't control the territory, they can't occupy it. They
get tossed out into cultural “outer darkness,” which is
exactly what the secular humanists have done to
Christians in the 20th century: in education, in the
arts, in entertainment, in politics, and certainly in the
mainline churches and seminaries. Today, the hu-
manists are “occupying.” But they won't be for long.
This book shows why.

*For the first time in over a century, Christians are
beginning to prociaim a seemingly new doctrine, yet
the original doctrine was given to man by God: domin-
ion (Gen. 1:28). But this doctrine implies another: vic-
tory. That's what this book is all about: a strategy for
victory.

Satan may be alive on planet earth, but he's not
well. He’s in the biggest trouble he’s been in since
Calvary. If Christians adopt a vision of victory and a
program of Christian reconstruction, we will see the
beginning of a new era on earth: the kingdom of God
manifested in every area of life. When Christ returns,
Christians will be occupying, not hiding in the shad-
ows, not sitting in the back of humanism’s bus.

This book shows where to begin. It is a unique,
challenging action manual, combining. theological
depth with practical advice on stratagies for domin-
ion, pius material on computers, newsletters and
mailing lists.

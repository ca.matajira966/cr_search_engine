The Theology of Christtan Resistance and The
Tactics of Christian Resistance (volumes 2 and 3 of
Christianity and Civilization) edited by Gary North.
908 pp., $20 for the set. Geneva Divinity Schoof Press,
Tyler, TX.

Must Christians obey every command of “Caesar”?
If not, at what point should Christians resist? Is there
a difference between lawful resistance and lawless
anarchy? These large valumes are devoted to answer-
ing such questions — questions which have become in-
creasingly important in the last decade. With dozens of
articles by such leaders as Gary North, R. J. Rush-
doony, Francis Schaeffer, and John Whitehead, the
symposium covers these issues and more: tyranny,
law, conflicting worldviews, pacifism, the American
Revolution, and the reconstruction of our civilization
in terms of biblical law.

But these volumes are not only important for devel-
oping a theology of resistance to tyranny. They deal
with tactics of resistance as well, providing many
workable plans for churches and Christian groups to
foil unlawful government intrusions. in addition, they
offer a concrete strategy for building a Christian cul-
ture; for the “bottom line” of ail Christian activity must
be a positive thrust for God’s kingdom.

There is a wealth of material here on taxation,
Christian school issues, and other legal matters
which are becoming more and more significant in our
day. A major theme developed in these pages is the
thesis that Christian churches are not exempt from
taxation —i.e., by the grace of the guardian state—but
that churches are immune from taxation.

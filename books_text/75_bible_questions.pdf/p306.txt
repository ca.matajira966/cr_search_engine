WARNING!

While this may sound a little hokey, | strongly
recommend that you keep this book out of sight
on campus or at church until you have determined
the attitude toward extreme theological contro-
versy which is held by your campus administra-
tors or pastor. There are several colleges in the
United States that have a policy of expeiling stu-
dents who even consider theological questions
as controversial as the 75 questions that are in
this book. | don’t want to get anyone kicked out of
school or church. ~ '

This is probably the most controversial Chris-
tian book ever circulated on your campus or in
your church. It may outrage administrators, pro-
fessors, and pastors who are unsure of them-
selves theologically. Don’t make extra trouble for

_ yourself. Study the 75 questions before you start
parading around with this book in your hand.
Also, find out whether you could be expelled for
owning a copy.

Gary North

Questionable Answer

“The text speaks of ‘an inheritance, being predestined;
but this only means that the predestined inheritance
will never be removed as a possibility for men to attain
through faith in Christ. It will always be there in gen-
eral, but it is not predestined to be there for each spe-
cific person who may at one point in time become re-
generate. He can fall from grace, and thereby lose his
inheritance.”

My Reply: A down payment is a promise. To whom
is ita promise? To “us,” Paul says. This clearly means
the members of Christ's spiritual body, the invisible
church:

If God gives a person a Spiritual down payment—a
guarantee of future payment to an adopted son—how
can He ever cancel the promise? If the promise is
conditional on man’s part, then in what sense is the
inheritance predestined? How can God or man be
certain that on their death beds, ail Christians will not
repent away from God and renounce their status as
adopted children of God? If men can fall from grace,
why couldn't this take place? In short, what guaran-
tees that it won't? The heart of man? How reliable is
the heart of man (Jeremiah 17:9)?

Does God waste the down payment on the predes-
tined inheritance on people who will renounce His
presentation of sonship? Isn't His adoption of His peo-
ple equally as secure as it is predestined?

If this isn't the meaning of adoption, what does
“predestination” mean?

For further study: Prov. 2!

 

Question 12

Aren't Our Good
Works Predestined?

For we are his workmanship, created in Christ Jesus
unto good works, which God hath befare ordained
that we should walk in them (Ephesians 2:10).

These words are less well known than those that
immediately precede them: “For by grace are ye saved
through faith; and that not of yourselves: it is the
gift of God: Not of works, lest any man should
boast” (2:8-9). The order of salvation is set forth: 1) by
God’s grace; 2) through faith; 3) unto predestined
good works.

The words “before ordained” are crucial. Ordained.
by whom? By God. When? Before. Again, we find that
Paul's tetter to the Ephesian church focuses on what
God has done before time began. His word is sure,
and their salvation is sure.

God does not simply predestine a man’s moment of
salva ion, in time and on earth. He also predestines a
man’s lifelong response to that salvation: his good
works. Since men are rewarded in terms of their
works (| Corinthians 3), God establishes men’s inheri-
tance before the foundation of the world. They wilt
work out on earth their assigned roles, according to
the decree of God.

if this is an incorrect interpretation, then what is it
that God ordains beforehand? Bare possibilities?
Could we be saved by grace through faith, and then
do nothing but bad works? We would not then be
Christians (1 John 4:3-6).

39

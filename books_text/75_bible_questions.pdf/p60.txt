Questionable Answer

“The Bibie says, ‘Whosoever will, may come. Anyone
may come to Christ who wills to. Whether he exercises
saving faith or nat is a matter of free will on his part, not
God's electing grace. The natural man sees the truth
well enough to make an autonomous decision.”

My Reply: Just for the record, the Bible doesn’t say
“Whosoever will, may come,” although Revelation
22:17 says something close. The question still must be
answered: Can an unregenerate person wil! to come to
Christ? Is his will “ree,” meaning autonomous?

What does | Corinthians 2:14 say? Does Paul give
any warrant for believing that the unregenerate man
is capable of properly interpreting the gospel
message, and then responding in faith to its offer of
salvation? Can the unregenerate Jew do it? Can the
unregenerate Greek? The text says no.

Both the “called Jew” and the “called Greek” can in-
ierpret the message properly. What is the difference
in response, “natural” vs. “called”? Goa’s electing
grace. God has elected some before the foundation of
the world.(Ephesians 1:4). He has not elected others.
“Therefore hath he mercy on whom he will have
mercy, and whom he will he hardeneth’” (Romans
9:18).-It's God’s decision, not man’s, since the natural
man cannot receive the things of the spirit. We are
first saved by God's sovereign grace, and immediately
we accept Christ. Redeemed men believe through
grace (Acts 18:27b). Without electing grace, men can-
not believe.

For further study: John 6:44,
8:7-8; Eph. 4:17-19; Il Tim, 2:25-26;

 
 

5 10:26; Rom. 3:10-12;
itus 3:3.

 

56

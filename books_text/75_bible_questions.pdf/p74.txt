 

70 75 BIBLE QUESTIONS

17:9; 6:64) only.

7. The doctrine of this high mystery of predestination is to
be handled with special prudence and care, that men attend-
ing the will of God revealed in his word and yielding obe-
dience thereunto, may, from the certainty of their effectual
vocation, be assured of their (I Thess. 1:4, 5; Il Pet. 1:10) ater-
nat election; so shall this doctrine afford matter (Eph. 1:6;
Flom. 11:33) of praise, reverence, and admiration of God,
and (Rom. 11:5, 6, 20) of humility, diligence, and abundant
(Luke 10:20) consolation to all that sincerely obey the
gospel.

Chapter iX—Of Free Will

1. God hath indued the will of man with that natural liberty
and power of acting upon choice, that it is (Matt. 17:12;
James 1:14; Deut. 30:19) neither forced, nor by any necessity
of nature determined to do goad or evil.

2. Man, in his state of innocency, had freedom and power
to will and to do that (Eccl. 7:29) which was good, and well
pleasing to God; but yet (Gen. 3:6) was mutable, so that he
might fall from it.

3. Man, by his fail into a state of sin, hath wholly lost
{Rom. 5:6; 8:7) all ability of will to any spiritual good accom-
panying salvation; so as a natural man, being altogether
averse from that good, and (Eph. 2:1, 5) dead in sin, is not
able, by his own strength, to (Tit. 3:3, 4, 5; John 6:44) con-
vert himself or to prepare himself thereunto.

4. When God converts a sinner, and translates him into
the state of grace, (Col. 1:13; John 8:36) he freeth him from
his natural bondage under sin, and by his grace alone
enables him (Phil. 2:13) freely to will and to do that which is
spiritually good; yet so as that, by reason of his (Rom. 7:15,
18, 19, 21, 23) remaining corruptions, he doth not perfectly
ner only will that which is good, but doth also will that which
is evil.

5. The will of man is made (Eph. 4:13) perfectly and im-
mutably free to God alone in the state of glory only.

 

80 75 BIBLE QUESTIONS

of flesh; renewing their wills, and by his. almighty power
determining them to that which is good; and effectually
drawing them to Jesus Christ; yet so as they come most
freely, being made willing by his grace.

Il. This effectual call is of God's free and special grace
alone, not from any thing at all foreseen in man; who is
altogether passive therein, until, being quickened and
renewed by the Holy Spirit, he is thereby enabled to answer
this call, and to embrace the grace offered and conveyed in
it.

lll. Elect infants, dying in infancy, are regenerated and
saved by Christ through the Spirit, wno werketh when, and
where, and how he pleaseth. So also are all other elect per-
sons, who are incapable of being outwardly called by the
ministry of the word.

iV. Others not elected, although they may be called by
the ministry of the word, and may have somé common
operations of the Spirit, yet they never truly come unte
Christ, and therefore cannot be saved: much less can men
not professing the Christian religion be saved in any other
way whatsoever, be they ever so diligent to frame their lives
according to the light of nature, and the law of that religion
they do profess; and to assert and maintain that they may, is
very pernicious, and to be detested.

Westminster Confession of Falth, 1646

Reformed
XV. Of Original Sin

We believe that, through the disobedience of Adam, origi-
nal sin is extended to all mankind; which is a corruption of
the whole nature, and an hereditary disease, wherewith in-
fants themselves are infected even in their mother’s womb,
and which produceth in man ali sorts of sin, being in him as
a root thereof; and theretore is so vile and abominable in the
sight of God, that it is sufficient to condemn all mankind. Nor
is it by’ any means abolished or done away by baptism; since

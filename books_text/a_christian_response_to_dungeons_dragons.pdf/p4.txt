Dedicated to our children:
May they ever be able to discern the difference.
Philippians 1:9-11

Special thanks to Jim Stewart and Robert Saxon, former Dungeon Masters, for
their evaluation and input on this project.

Copyright © 1987 by Dominion Press. Second edition, revised March 1988.

All rights reserved. Written permission must be secured from the publisher to use
or reproduce any part of this book, except for brief quotations in critical reviews
or articles.

Published by Dominion Press
7112 Burns Street
Fort Worth, Texas 76118

Printed in the U.S.A.

Library of Congress Catalog Gard Number 87-071865
ISBN 0-9304b2-60-2

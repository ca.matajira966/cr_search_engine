72 Armageddon Now!

letter written to a rabbi. The author of the letter encouraged the
rabbi to take comfort from Jeremiah 31:10 (“He that scattered
Israel will gather him”), and commented, “He is gathering him,
and the enemies of the Jews can not hinder it.”#7

The King's Business reflected the view that the Arabs would
quiet down once they began to share the “beneficial results of
Jewish industrial enterprise.” It was stated that the Arabs would
not suffer any hardship whatsoever from the restoration of the
Jews, because, after all, they still had the whole of Arabia with
its million square miles, and, in addition, all the land that had
been freed from the Turks. The Arabs were only nomads who
had created “neither material nor spiritual values” in the land.
In keeping with this view Lord Shaftesbury was quoted: “Give
the country without a people to the people without a country.’
One article, citing a missionary to Palestine writing in the Jewish
Missionary Herald, even reflected a blanket prejudice:

The poor Arab—what has he? The Turk has robbed him

even of his character! What could the Zionists rob him of?

The charge that the Jew is robbing the Arab is absolutely

false?
Thus some premillenarians continued to hold the naive view that
desertion by the Turks had left Palestine a great vacuum that
was sucking the Jews back by some inevitable law of nature. Many
never even considered the question of the justice or injustice
of the Jews’ replacing or overpowering the native—Arab existence
was completely ignored.

Arab resentment in Palestine erupted into rioting as a result
of an incident at the Wailing Wall on the Day of Atonement in
1929. The Wall was believed to be a fragment of Herod's Temple,
and the Jews regularly gathered there for prayers. A minor in-
cident—the attempt of the Jews to build a partition to separate
the male and female worshipers—escalated into conflicts in which
the Jews counted 133 dead and 339 wounded, and the Arabs
suffered 116 dead and 232 wounded. Virtually all the premil-
lenarian popular press took note of this tragedy. The Moody paper
quite moderately observed that the Jews were expanding and
increasing their power under English and American aid and “the
Arab is naturally jealous and alarmed.” The proper response for
the premillenarian was indicated: “But meanwhile the student

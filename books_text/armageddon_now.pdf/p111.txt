The Twenties Roar Faintly—Bear, Beast, and Young Lions 79

By 1927, the editor of the Evangel considered Armageddon
as an increasingly imminent event; the cause was to be Russia’s
long-standing need for a warm water port in the Mediterranean:
“We do not think the time is far off when the Russian bear
will make another attempt to get water, and this time it will be
to secure the sea coast of Palestine. The returning Jews have
spent such vast sums in this land, that it is presenting a vast
prize to greedy nations.” He cited C. L Scofield and James M.
Gray as authorities for the identification of Rosh in Ezekiel 38
with Russia. Another writer a year later also spoke of the
nearness of Armageddon. Referring to Ezekiel 38, he said,

Here you have read that Russia is going to war with Palestine,
That is coming....There is where we are to-day. There-
fore, we may expect very shortly that this conflict will take
place.??

Another explanation that was given for Russia’s supposed interest
in Palestine was the increasing awareness of the value of the mineral
products in the Dead Sea.7* God had supposedly reserved this wealth
for the use of His restored people in the twentieth century.4
Premillenarian discussion of the great northern confederacy
of Germany and Russia gained momentum in the 1920s, but would.
not peak until the Nazi-Soviet Pact of 1939. Speculation was
stimulated by the Genoa Conference of 1922 which produced the
Rapallo Treaty of 1922 between Russia and Germany, renewing
diplomatic relations between the former belligerents and. provid-
ing for economic and political co-operation. In its special section
on “Notes Concerning Prophecy: And Signs of the Times,” The
King’s Business at the time ran a subheading, “Russia-Germany
Combine,” which began with the assertion: “Prophetic students
have long seen the probable formation in the near future of the
great northern power spoken of in Scripture.” It was assumed
that the union was already a fact. “As to the union of Germanised
Russia, and what it signifies, do we wonder, in the light of recent
events and what is suspected, what the outcome will be?” What
was “suspected” was that a military alliance existed between Russia
and Germany, that German military officers were reorganizing
the Russian army, and that the Krupp munition workers had been
transferred to Russia to manufacture arms.?> The Evangel ex-

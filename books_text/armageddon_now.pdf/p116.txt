84 Armageddon Now!

Missionary Magazine entitled, “The Shadow of Armageddon: Will
America Take Part in the Conflict?” It proclaimed: “That the
United States will be involved is plain from Ezek. 38:13, where
Britain is referred to under the figure of ‘the merchants of Tarshish
with all the young lions thereof.’ In that word all we see the
place of our land in prophecy, for no one can deny that Britain
is the modern land of the lion symbol or that our nation is a young
lion of Britain.”°? A very high recommendation was given to a
book by Reginald T. Naish, The Midnight Hour and After! Be-
tween 1920 and 1928 it went through seven editions totaling
50,000 copies. Naish’s chapter on “Armageddon” explained that
the merchants of Tarshish clearly stood for “a great trading
nation.” Therefore, “when we learn that this trading or shop-
keeping nation has ‘young lions’ belonging to her, we can hardly
doubt but that Great Britain and her colonies are intended to
be described." Such leaps in reasoning—from Tarshish to Britain
and from lions to colonies—were perhaps in keeping with a long
tradition of apocalyptic speculation, but hardly in line with a com-
mitment to literalism.

A similar theme appeared in a sermon delivered at a Keswick
conference and reprinted in various publications including Christ
Life, The King’s Business, and Moody Monthly. The question,
“Does the United States Appear in Prophecy?” is answered, “Proba-
bly.” The key passage is Isaiah 18, particularly the first two verses:
“Woe to the land shadowing with wings, which is beyond the
rivers of Ethiopia: that sendeth ambassadors by the sea, even in
vessels of bulrushes upon the waters, saying, Go, ye swift messen-
gers, to a nation scattered and peeled, to a people terrible from
their beginning hitherto; a nation meted out and trodden down,
whose land the rivers have spoiled!” Discarding the standard view
that the reference is to Egypt, the author says that the wings
denote the spread eagle; and since the United States is the only
country which has an eagle with spread wings as its insignia, it
must be the land in question. Besides, only very important coun-
tries send ambassadors by the sea; this would rule out Egypt.
The author corroborates his thesis by pointing to the reference
to “young lions” in Ezekiel 38:13.%

And so it was that ten years after the peace treaty and ten
years before another war would burst forth, the premillenarians

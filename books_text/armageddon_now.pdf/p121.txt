Balfour Betrayed 89

moderate opinion (for premillenarians), apportioning partial blame
for the situation to the Jews themselves.

As in most unfortunate incidents, there has been fault on
both sides, It is quite evident that the Jew has not come
to Palestine to Jive with the Arab, but quite apart from him,
The Jew in Palestine is frequently bigoted, and often carries
“a chip on his shoulder.” It is well known that the Jews
themselves were partly to blame for the riots of 1929, But
this does not justify the Arab for his falsehoods which helped
to incite the trouble, nor for the murder of men, and even
women and children, of which he is guilty. It was an Arab
massacre, in which Jews sought to defend themselves.

The author then argued for a Jewish national home on a prag-
matic basis—to prevent them from turning to Bolshevism, but
he recognized that the Arabs had to be provided for too. Even
such moderation in the premillenarian camp did not prevent the
conclusion that the end was predetermined and the confident as-
sertion that “the Jewish nation is to have a future in Palestine.”*

Various elements of the restoration continued to be interpreted
as prophetic signs of the times. The revival of the Hebrew language
was seen as a fulfillment of prophecy. Writing in The King’s Busi-
ness, Louis S. Bauman said:

The renaissance of the Hebrew language is one of the most
striking present fulfillments of the prophetic Word. Hear the
prophecy:

“Therefore wait ye upon me, saith the Lord,..my de-
termination is to gather the nations, that 1 may assemble
the kingdoms, to pour upon them mine indignation.”

That means Armageddon’s battlefield, But, when the na-

2 tions are marshalling their hosts for their last awful conflict,
the Almighty says: “Then will I turn to the people a pure
language, that they may call upon the name of the Lord.”
See Zeph. 3:8, 9.°

Bauman was pastor of the Open Bible Church in Los Angeles
and was on the faculty of the Bible Institute of Los Angeles.
His articles and books were in turn cited in various other pre-
millenarian publications.

Other particular events, such as the first oil line from Iraq
to Haifa and the revival of a militaristic spirit among the Jews,
were also considered significant. In 1935 appeared a booklet by

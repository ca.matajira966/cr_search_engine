Balfour Betrayed 101

whereas with Russia they regularly applied the rule of ethics while
with Israel they always measured by the rule of determinism.

During the 1930s, liberal American Protestants found the
idea of a Jewish state repugnant. Opposition to the suggested
partition was one manifestation of this attitude. Hertzel Fishman
noted that from the theological position of liberal Protestants
“the notion of a sovereign Jewish state was simply too unpalatable
to digest. The idea of a resurrected ‘old Israel’ was too radical to
accept.”61 The Christian Century spoke out on the ethical in-
justice of the partition plan which would transfer 225,000 Arabs
out of their ancestral homeland.®? Evidently the premillenarians
did not even consider such a problem.

Throughout the decade, premillenarians continued to be almost
uniformly anti-Arab. The basic postulate of their position was
that the Holy Land belonged to God and that He had given it
to the Jews, The following argument appeared just after the 1929
riots under the heading: ‘Who Has the Strongest Title?”

At a recent meeting of Arabs at Haifa, Captain R. G. Can-
ning stated: “The soil of Palestine belongs to the Arabs,
and it has been so since 630 or even before. Is not that a
good enough title?”

In the year 1857 B.c.,, Abraham said unto Eliezer his
servant: “The Lord God of heaven sware unto me saying,
unto thy seed will I give this land.” And Eliezer testified
to Laban and Bethuel, “Sarah, my master’s wife, bare a son
to my master when she was old: and unto him hath he
given all that he hath.”

Jacob testified to Joseph also: “God Almighty appeared
unto me at Luz in the land of Canaan, and blessed me,
and said unto me, Behold I will make thee faithful and
multiply thee... and will give this land, after thee for an
everlasting possession,”

Who has the prior right to the land, the Arabs or the
seed of Jacob?63

The point was that the land was given to Isaac—not Ishmael, and
to Jacob (Israel)—not Esau. No matter which ancestry was as-
cribed to the Arabs (Ishmael or Esau), they had been left out.
In The King’s Business, one writer called the conflict “the age-
old jealousy of Ishmael toward Isaac,” but in an article appearing
eight months later, another writer spoke of “the ages-lasting
quarrel between Jacob and Esau.” Both arrived at the same con-

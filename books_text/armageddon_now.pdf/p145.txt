Mobilizing for Armageddon 113

nor could he resist the urge to set a date.** Others, such as Louis
S. Bauman, came near to succumbing to the date-setting tempta-
tion. Commenting on the belief of many premillenarians that
1936 or 1937 was the latest possible year for the end of the
times of the Gentiles, he stated, “They may not be right. We are
inclined to believe they are not far from right.”®

Bauman, however, readily admitted that there was conflict
in the premillenarian ranks over prophetic identifications: “There
has been considerable confusion as to just what peoples will make
up this great northeastern federation of nations whose allied hosts
shall swoop down ‘upon the people that are gathered out of the
nations’ at the time of the end. It is certain, however, that Russia
will be the heart and soul of that great godless federation.”"* A
few examples of this confusion will suffice. In contrast to the
majority of premillenarians whe identified Gomer with Germany,
evangelist Charles S. Price in his book, The Battle of Armageddon,
identified the descendants of Gomer as living in the Crimea?
Oswald J. Smith, disagreeing with C. I. Scofield, Louis S. Bau-
man, and many others, believed that the coming defeat of Russia
and the northern confederation was not to be the battle of Arma-
geddon—Armageddon was to come later, after the debris from
the battle with Gog had been used for firewood for seven years.
There were even discrepancies in the works of individual scholars,
such as Louis T. Talbot, after whom Talbot Theological Seminary,
the graduate school of Biola College, was named. In one radio
address, “Russia: Her Invasion of Palestine in the Last Days and
Her Final Destruction at the Return of Christ,” Talbot com-
mented on Ezekiel 38:8, “We note in verse 8 that God will lead
these people [Russia] down and on to the battle of Armageddon.”
But in another broadcast, “The Judgment of God upon the Rus-
sian Confederacy,” he said, “The Roman prince, and not Gog
of Russia, is the leader of the enemies of God at Armageddon.”
He then suggested that the seven years between the defeat of
Gog and the battle of Armageddon would be the period of the
Great Tribulation. Confusion also arose over the role of the
“king of the south” (Dan. 11:40). Louis S. Bauman identified
the king of the south as England allied to Italy as part of the
revived Roman Empire; Alva J. McClain, the president of Grace

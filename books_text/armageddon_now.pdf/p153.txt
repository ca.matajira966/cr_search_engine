Mobilizing for Armageddon 121

In his recent triumphal tour of Libya it was declared that
he henceforth is to be known as the “Defender of the Mos-
Jems.” From 2,000 Arab notables he received the sword of
Islam, and was proclaimed the “glorious and invincible
leader.” Brandishing the sword, Mussolini promised the
Arabs that in a short time Rome would show how near to
her the Arabs are.

Although Mussolini and Italy were to prove disappointments
to such wild-eyed dreams, the foundations were already being
laid in the 1930s for a successor to that myth. The role of the
revived Roman Empire was to be taken over by the European
Common Market in the post-World War II era. Even in the mid-
thirties Premier Aristide Briand of France was preparing a plan
for a “United States of Europe,” which drew speculation from
the premillenarians. Arno C. Gaebelein commented, somewhat
obscurely, on the idea:

That such a union will ultimately come is known to every

student of prophecy. There will be two great federations

in the future. The one is the Western European union, cor-

responding to the Roman Empire. The other is the North-

eastern confederacy, headed by Russia. Mussolini will proba-

bly fall more fully in Jine with Briand’s suggestion. We shall

follow this movement with keen interest and tell our readers

of its progress.
Gaebelein, however, gave no clue as to what particular role he
envisioned for Mussolini. The Evangel quoted Revelation’s com-
ments on the idea of European unity: “We know from the Word
of God that this plan will not succeed but we know that Europe
will be divided into two camps, with western Europe under the
dictator who shall later arise in Rome, while eastern Europe will
be under the control of the great union between Germany and
Russia.”®5 Apparently, the point being made was that there could
not be a union of the whole of Europe.

Perhaps it was due to the isolationist sentiment developing
in the 1930s that in spite of an imminent Armageddon, there
seemed to be less concern than before about the role of the
United States in all these events. Harry J. Steil in the Evangel
did not equivocate, however:

Will the United States be involved? Scripture says, “The
spirits of demons, working miracles, shall go forth unto the

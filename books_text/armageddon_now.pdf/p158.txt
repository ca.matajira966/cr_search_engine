126 Armageddon Now!

whole, nor will the Jews get full sovereignty over that land,

Jewry needs to know this.®
This evidently reflected the view of the Evangel’s editor. But by
1948 the Evangel was again able to see God in the movement;
“They think it is the skill of their political schemings that has
gotten them a Jewish state; but behind the scenes it must be
that God is opening the way for the Jews to return to Palestine
in greater numbers.” However, only two months before inde-
pendence day, the Evangel was still predicting, “The Zionists will
never get the Promised Land by their own political schemings
and their own armed might. They will get it when they welcome
Jesus of Nazareth back to earth as their Messiah!’

Louis T. Talbot in The King’s Business saw in secular Zionism
the coming together of Ezekiel’s “Dry Bones” which would /ater
be completely regenerated by spiritual breath from God.!? There
was little or no decline in premillenarian support of Israel as a
result of these conflicting interpretations of Scripture. Enthusi-
asm might vary from a high of ecstatic euphoria to a mere thrill
of observation, but even the least enthusiastic, Our Hope, would
after the fact be able to say in 1949: “Even if the nation of Israel
should be temporarily dispossessed of the portion of the land
over which they now hold sway, this would not invalidate for
one moment the fact that Palestine is to be eventually turned
over to them in its entirety.”2?

During the 1940s the premillenarian response to Israel tended
to center upon three focal points: the British White Paper of
1939, the United Nations partition of 1947, and Israel’s inde-
pendence in 1948. During the war, interest in Palestine declined,
but occasional comments did indicate a continued commitment to
a Jewish national home in Palestine. The Evangel was concerned
about unemployment in Palestine, blaming it on the British limi-
tation of immigration—but failed to explain how that could cause
unemployment. Readers were advised that “there is need for us
to pray for God’s purpose to be fulfilled in Palestine.”!* Later
the Evangel also expressed hope that the British would yet fulfill
the pledge of the Balfour Declaration.1>

In the midst of Nazi anti-Semitism during the war, The Sunday
School Times in “The Indestructible Jew” still spoke confidently

 

136 Armageddon Now!

from the Psalms that “they shall prosper that love thee” (Israel).
Again the question arises: If all was predetermined prophetically,
what was the purpose of prayer anyway? No one seemed to recall
Hebrews 12:6, “Whom the Lord loveth he chasteneth.” Perhaps
those who loved Israel might have chastened the upstart nation
for its own good.

Early in World War Il the Evangel cited Prophecy Monthly's
approbation of what was supposedly Britain’s first policy state-
ment on the Jewish question since the start of hostilities. Cabinet
minister Arthur Greenwood had vaguely assured the American
Jewish community that after the war “wrongs suffered by the
Jewish people in so many countries should be righted.” The lead-
ing American Zionist, Rabbi Stephen S. Wise, wishfully regarded
this as having even greater implications than the Balfour Declara-
tion. The Evangel was certain that this purportedly innovative
policy would bring the blessing of God upon Britain.“ When
Winston Churchill, who was thought to be pro-Zionist, became
Prime Minister, the following was cited from Christian Victory
in speculating about the significance of the war for Palestine:

Now that Churchill is prime minister “the future for the
Zionist Movement in Palestine looks brighter.” There has
been a corresponding change in the tide of the war. When
Chamberlain was Premier, Britain suffered one defeat after
another, but since Churchill took his place the outlook for
Britain has become brighter and brighter. There may be a
definite relation between British war successes and the Gov-
ernment’s more favorable attitude toward Zionist plans to
make Palestine a Jewish national home.*'

Yet only a year and a half later premillenarian critics were say-
ing, “There has been among Christian leaders a steadily growing
conviction that the many reverses suffered by the United Nations
in the war have had direct connection with our failure to get in
step with God in His clear purpose that Palestine is for the Jews.”6

After the war, the editor of The King’s Business attempted
to account for the dire economic situation in Britain. Refusing
to blame it entirely on the war or economic factors, he ascribed it
partly to the spiritual condition of the English people. “More
than that, there is that blot upon England’s character in their
shameful renunciation of the Balfour Declaration which prom-

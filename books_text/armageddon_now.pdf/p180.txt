148 Armageddon Now!

The only previous observation the Evangel had made was on the
occasion of Hitler’s setbacks in the winter of 1940-41, arousing
new hopes of Russian victory that would in turn lead to a northern
confederacy.2 Bauman’s book, however, had been consistently
critical of Russia, rehearsing the evil of the Communists since the
revolution. One of his themes is reflected in the title of chapter
ten, “Any American or British Alliance with Gog Is Abnormal.”
He cautiously blasted American foreign policy, arguing from a
moral stance that martyrdom was preferable to compromise: “The
author does not want to be critical of the United States and
Great Britain for accepting assistance from a beast in order to
destroy a beast. Nevertheless, it is his conviction that it would
be better for America to go down fighting a glorious fight to
preserve the political and spiritual faiths of our fathers than to
live and be so conscienceless as to wink at the crimes of Russia.”**
He predicted that the result of the war would be that atheistic
Russia in victory would pose an enormous threat to religion in
Europe, causing ten nations to unify in a great revival of the
Roman Empire to resist the great northern confederacy.™ Thus
was the world shaping up for the great battles of the end. So ran
Bauman’s thesis, and his work was glowingly recommended to
readers of The Sunday School Times.*

During the last couple of years of the war in Europe, the
Evangel, too, became openly critical of the Russian ally. In May,
1943, the observation was made: “There are many who believe
that the final struggte of this age will find Germany and Russia
arrayed together against the United States, Britain, and the Medi-
terranean powers. ...It may not be a popular thing to say at the
present time, but it is not hard to envision such a possibility in
either the near or the distant future.” Later that year, the
Evangel cited Dan Gilbert in World-Wide Temple Evangelist for
the information that Russia was forming a “Committee of Free
Germany” to provide Germany opportunity to become a junior
partner in a northern European confederacy.2¢ By February, 1944,
the “Roving Reporter” in the United Evangelical Action was toss-
ing off some flippant fatalism:

Joe Stalin is running true to form. He is letting the United
Nations know, in unmistakable terms, that he purposes to
manage eastern Europe to suit himself. We had alli better

158 Armageddon Now!

There continued to be occasional speculation as to where the
United States fitted into this scheme. In 1940 a discussion on
Armageddon by Louis S. Bauman in The Sunday School Times
brought forth this covering comment from the editor, Charles
G. Trumbull: “It is to be noted carefully that Dr. Bauman does
not say that Mussolini is the Antichrist, mor does he say that
the activities of the Antichrist have begun in the present European
War, nor that Britain will be defeated in the present war.”
But very careful examination will show that those were exactly
the things that Bauman was saying; but since this was the twenty-
third article in a series, Trumbull would have found it incon-
venient to reject the article even if he had wished to. Bauman
specifically included the alfies of Britain in this defeat. But in
1942, after the United States was allied to Britain, Bauman re-
versed himself and rejoiced that the Stars and Stripes would float
aloft in the millennial kingdom**

If our interpretation is correct, then thank God for this
divine assurance that Great Britain and her young cubs,
Canada, Australia, New Zealand, and America will be found
protesting to the very end of the age against the ravages
of the great Bolshevistic colossus of the north and its
atheistic allies.2?

After the war, Our Hope’s analysis of the East-West split in
Europe included the possibility that the revived Roman Empire
might include the whole Western Hemisphere.®* It seemed to
Gaebelein that the United States would actually be “the moving
and most powerful factor” in this Western Alliance opposing
Russia; he did not, however, in 1947 expect war for eight to
ten years.8* Evangelist H. E. Fisher, whose Doctor of Theology
thesis was “The Destiny of Soviet Russia,” explained that Russia’s
invasion of Palestine, in support of the Arabs, would be opposed
by Italy, England, and America, who would support the Jews.
This latter union was to be a Catholic confederation which would
defeat the Russians, and ‘its great leader would become the first
president of a world government. This was presented as “The
Plan,” without any hedging whatsoever. Another article by Merril
T. MacPherson in Christian Life in 1948 said the empire would
“undoubtedly” include the British dominions and the United States,
and that the Iron Curtain was a line placed there by God—so

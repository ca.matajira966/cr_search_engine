The Certainty of Survival 163

united.” He repeatedly asked Christians not for political action,
but for prayer: “Let us pray, therefore, that the leaders of the
nation and our own nation may see that God is on the side of
those who recognize this program.”? Some premillenarians, how-
ever, recognized that if God were to answer such prayers for
peace, then the prophecies of tribulation and Armageddon could
never come to pass. W. E. Vine writing on “The Future of Israel”
in The Alliance Witness pointed out that Israel’s tendency to trust
in national and material developments would lead to a period of
unprecedented tribulation for the country.*

These issues were part of the larger question of whether the
present return to Palestine was the prophesied restoration or not.
The president of Moody Bible Institute, William Culbertson, ex-
pressed his awareness that some saw no connection between what
was going on in the Holy Land and the Biblical prophecies, but
he sided with those who believed in at least a partial return
before the second coming of Christ.® Charles L. Feinberg of
the Biola faculty expressed a similar view. The Evangel thought
the present restoration was the coming together of Ezekiel's “Dry
Bones” and that an eventual repentance would invite the Breath
of God to instill new life in them.?7 As Louis H. Hauff, pastor of
the San Bernardino, California, Assembly of God expressed it:

Some have been disturbed because there have not been
signs of religious awakening in Israel.... The spiritual awak-
ening will come later.... It seems logical to expect that
Israel as a nation will continue in blindness and ungodiiness
until the Lord returns.

He believed that the Great Tribulation might be the “crushing”
that would cause the Jews to cry out in repentance and accept
Christ as the Messiah.? This position allowed premillenarians to
take the edge off the criticism which attended Israel’s unrighteous
acts. They taught that even though the Jews were still evil, they
were fulfilling the will of God, and, therefore, should not be
opposed.

New insight was available to premillenarians in May and
July of 1955 as Moody Monthly opened its pages to spokesmen
for both the Israeli and Arab views. Isaac D, Unna, vice-consul
of the Consulate of Israel in Chicago, and Fayez A. Sayegh, an

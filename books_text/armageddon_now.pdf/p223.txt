 

 

Jerusalera! What Now? 191

the war and the conquest of Jerusalem with a special issue on
Israel. In one article, President John F. Walvoord of Dallas Theo-
logical Seminary claimed the occupation of Jerusalem offered proof
of the miraculous preservation of the Jews, and he observed,
“Surely this is the finger of God indicating the approaching end
of the age.”? This special issue also offered excerpts from Wilbur
M. Smith’s new book, Israeli/Arab Conflict and the Bible, the
preface of which stated, “The Christians believe that the events
of these recent days are part of God’s plan of the ages.”® Another
article expressed the philosophy of history that God is in control
of all events and that the nations are merely a drop in the bucket
or dust on the scales.? The entire concern was with fulfillment
of prophecy and destiny; the issue of the morality of war or the
justice of the Israeli attack was not even a consideration.

One observer in Jerusalem just a few days prior to the hos-
tilities was the president of the Evangelical Free Church, Arnold
T. Olson, whose inexpensive paperback, Inside Jerusalem, City of
Destiny, appeared the following year. Olson spoke of the unifi-
cation of Jerusalem as the most exciting headline of the war and
as a sign of the end. He believed, as his title implied, “Jerusalem
has a rendezvous with destiny.”*° A special point was made criti-
cizing the extreme orthodox sect of the Jews which opposed the
establishment of the state of Israel. This view had been reflected
by Rabbi Amram Blau who called the Israeli victory a disaster:

God promised he will redeem Zion with the Messiah. That

redemption is to come in a manner and in circumstances

clearly specified in Jewish tradition. The Messiah has not

arrived. The specified circumstances do not exist. The cre-

ation of an independent Jewish state by men, not God in his

own way and time, is a grave sin. It is faithlessness,
Olson found it quite ironic that this group that called themselves
“the Guardians of the City” were fleecing the country in the crisis
days just prior to the war. Although their refusal to fight was
logically consistent with their theological position, he bitingly
observed, “It filled me with disgust to note such a display of
cowardice and disloyalty.”

Attitudes toward the war were the subject of a study by

Arnold Ages, a professor in the Department of Classics and Ro-
mance Languages of the University of Waterloo. He showed that

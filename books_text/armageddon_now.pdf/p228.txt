196 Armageddon Now!

These same views then became, after the war, the justification
of the Israeli position. Immediately after the fighting, the Evangel
editor pointed out that the restoration of Israel was not a new
idea, citing James H. McConkey’s The End of the Age, which
was copyrighted in 1897. The fulfillment of a particular interpre-
tation of a prophecy was purportedly the same as the fulfillment
of the prophecy itself. “Atheists have ridiculed this idea. Liberal
clergymen have scoffed at it. But today it is being fulfilled be-
fore our eyes, indicating that these are indeed the ‘last days’ both
for Israel and for the Church.”** Arnold T. Olson supported
Israel’s occupation of Jerusalem and approved various assertions
by the Israelis that they would never leave the city. He saw no
“justification for proposals that would again break up the unity
of the city.”°8 A 1973 Christian Life article directly asserted that
the reason Israel would not give up the Golan Heights or the
Sinai Peninsula was that God had promised all that territory to
the Jews, and went further to say that “as yet, the Israelite only
has his foot in the door in the Middle East.”2" The Evangel on
the occasion of the 1974 Geneva Conference on the disengage-
ment of troops in the Golan Heights recalled that God’s program
had been announced thousands of years before when He said
that the land of Canaan would be given to Jacob and his descend-
ants.8° Obviously, his uncle Ishmael’s children were to be left
out. The latter rain also continued to be heralded as proof of
God’s blessing—only this time the increase was said to have
begun with the establishment of the state of Israel in 1948.
Actually, average rainfall in the decade of the 50s was next to
the lowest on record.

Although support of the Israeli position was widespread among
premillenarians after the war, it was not unanimous, and even
some of the supporters were selective in their approval. Some
found themselves in an ambivatent position; they believed in the
ultimate restoration of Israel, but they could not conscientiously
endorse the method or the arrogance of Israel. These nagging
doubts about Zionism had been expressed earlier in The King's
Business in 1960 by radio preacher William Ward Ayer, the former
pastor of the Calvary Baptist Church, New York City. He pointed
out that Zionism was an enigma to Bible scholars because it was

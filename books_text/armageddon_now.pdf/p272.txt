240 Armageddon Now!

31, Etmer H. Nicholas, “The Fig Tree Is Budding,” Evangel, Match 22, 1953,
pp. 3-4. (Reprinted from The King's Business)

32. Evangel, May 9, 1954, p. 2.

33. Arnold Toynbee, A Study of History, IV, 407-08.
34. Charles E. Pont, Zhe World's Collision, p. 215.
35, Evangel, April 29, 1956, p. 2.

36. Evangel, December 2, 1956, p. 2.

37. Charles L. Feinberg, “Isracl—The Apple of God's Eye,” The King's
Business, XLV (October, 1954), 19.

38. Evangel, November 5, 1950, p. 5.

39, Leonard Sale-Harrison, “Russia's Militant Opposition to Christianity,”
The Sunday School Times, XCIL (October 15, 1950}, 867.

40, Milton R. Lindberg, “Regathering Israel,” Evangel, January 13, 1952,
Pp?

41. F.C. Schwarz, “Red Shadow!” Moody Monthly, LI (January, 1951),
306-07.

42. The King’s Business, XLII (March, 1951), 5.

43. The Sunday School Times, XCVL (December 24, 1955), 1029.

44, Evangel, September 2, 1950, p. 7.

45. Sale-Harrison, “Russia’s Militant Opposition to Christianity,” p. 867.

46. Frank M. Boyd, “Russia in Prophecy,” Evangel, June 17, 1950, p. 5.

47. Wilbur M. Smith, “World Crisis and the Prophetic Scriptures,” Moody
Monthly, L (June, 1950), 679.

48. Louis S. Bauman, “The Russian Bear Prowls Forth to His Doom,” The
King’s Business, XLI (September, 1950), 10-12.

49, Douglas Ober, The Great World Crisis, p. 75.

50. The King’s Business, XLII (April, 1951), 4.

51. J. Narver Gortner, “Russia’s Origin, Character, and Doom,” Evangel,

September 21, 1952, p. 10, Glad Tidings Bible Institute, Santa Cruz, Cali-
fornia, has since been renamed Bethany Bible College.

52. Our Hope, LXI (1954-55), 660-61.

53. H. S. Gallimore, “The North and South Prophecy," Our Hope, LXIL
(1955-56), 63.

54, Edgar Ainslie, “Russia in the Light of Prophecy,” The Sunday School
Times, XCVIIL (October 13, 1956), 807.

55. James C. Dodd, “Israel Today and Tomorrow,” Evangel, March 31,
1957, pp. 16-17.

56. W. W. Kirkby, “Ambitious Russia: Without Doubt Her Greed Will Lead’
to Her Downfall on the Mountains of Israel,” Evangel, February 17, 1957,
pp. 6-7.

57. Kenneth de Courcy, “World Crisis," Evangel, March 23, 1958, p. 3.

58. Mery Rosell, “God Pre-writes the Headlines,” The King’s Business, XLIX
Guly, 1958), 5. An editorial note on the author said, “An estimated 10
million heard him in person and he is now regularly on radjo and TV.”

59. John F, Walvoord, “Russia and the Middle East in Prophecy,” Moody
Monthly, LX (December, 1959), 26.

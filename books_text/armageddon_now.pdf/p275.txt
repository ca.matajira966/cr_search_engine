Footnotes 243

24. Louis H. Hauff, “Israel—The Budding of the Fig Tree,” Evangel, July 10,
1960, p. 19; The Prophetic Word in Crisis Days, p. 116.

25. Louis T. Talbot, “The National Resurrection of Israel and Her Future
Glory,” The King’s Business, LV Clune, 1964), 21-28.

26, Wilbur M, Smith, “Concluding Words on the Signs of the Times,” Moody
Monthly, LXVIL (September, 1966), 50.

27. Evangel, July 9, 1967, p. 4.
28. Olson, Inside Jerusalem, pp. 83-88.

29. W. D. Cook, “Arabs/Jews: Why They Fight,” Christian Life, XXXV
(August, 1973), 26.

30. Evangel, July 14, 1974, p. 31.

31. Harold Sevener, “Isracl: The World’s Timetable,” Christian Life, XXXV
(August, 1973), 28. Dov Ashbel, One Hundred and Seventeen Years (1845-
1962) of Rainfall Observations, pp. 118-31.

32. William Ward Ayer, “Anti-Christ: The Nations, the Church, Israel,”
The King’s Business, LI (May, 1960), 28.

33. Bernard Ramm, “Behind the Turmoil and Terror in the Mideast,” Eter-
nity, XVII (September, 1967), 34.

34. William Culbertson, “Perspective on Arab-Israeli Tensions," Christianity
Today, XI (June 7, 1968), 6-9.
35. Olson, Inside Jerusalem, pp. 50-51.

36. Frank H. Epp, Whose Land is Palestine? The Middle East Problem in
Historical Perspective, p. 241.

37. Tbid., pp. 243-44,

38. Charles C. Ryrie, “Perspective on Palestine,” Christianity Today, XII
(May 23, 1969), 8

39, Ralph W. Harris, “The Arab-Israeli Conflict,” Evangel, October 18,
1970, p. 8.

40. Evangel, July 9, 1967, p. 4.

41. Louis H. Hauif, “Dry Bones of Israel Come to Life,” Evangel, August
20, 1967, p. 7.

42. Wilbur M. Smith, Israeli] Arab Conflict and the Bible, p. 110.

43. Olson, Inside Jerusalem, p. 133.

44. Bruce Shelley, “The Late, Late Great Planet,” United Evangelical Action,
XXXIIE (Spring, 1974), 6.

4S, Hal Lindsey, The Late Great Planet Earth, p. 57.

46. Hal Lindsey, “The Pieces Fall Together,” Moody Monthly, LXVUL
(October, 1967), 27.

47. Walvoord, “The Amazing Rise of Israel,” p. 25.

48. “Israel: Things to Come,” Christianity Today, XU (December 22, 1967),
307.

49, Raymond L. Cox, “Time for the Temple?” Eternity, XIX (January,
1968), 17-18.

50, H.L, Ellison, “Israel—A Year Later," Erernity, XIX (August, 1968), 38.

Sl. J. Dwight Pentecost, “Which Comes First the Rapture or the Temple?”
Moedy Monthly, LXXIL (October, 1971), 30-31,

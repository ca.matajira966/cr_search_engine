256 Armageddon Now!

Case, Shirley Jackson. “The Premillennial Menace.” The Biblical World,
LI Guly, 1918), 17-23.

Christianity Today.

Christian Life,

Christian Voice for a Jewish Palestine, 1 (Summer, 1946),

Dawn, an Evangelical Magazine.

Dodge, Bayard, “Peace or War in Palestine.” Christianity and Crisis,
VIII (March 15, 1948), 27-30,

Dollar, George W. “The Early Days of American Fundamentalism,”
Bibliotheca Sacra, CXXIII (April, 1966), 115-23.

English, E, Schuyler. “The Judgment of the Nations,” Our Hope, LI
(February, 1945), 561-65.

Eternity.

Goen, Clarence C. “Jonathan Edwards: A New Departure in Escha-
tology.” Church History, XXVIII (1959), 25-40.

Goldberg, Louis, “Dimensions of the Future.” Moody Monthly, LXIX
(December, 1968), 67-72. .

Jennings, F. C. “The Boundaries of the Revived Roman Empire.” Our
Hope, XLVII (December, 1940), 386-90.

Karmarkovic, Alex. “American Evangelical Responses to the Russian
Revolution and the Rise of Communism in the Twentieth Century,”
Fides et Historia, WV (Spring, 1972), 11-27.

The King’s Business,

Kligerman, Aaron J, “Palestine—Jewish Homeland.” The Southern
Presbyterian Journal, VII (June 1, 1948), 17-19.

Ladd, George Eldon. “Israel and the Church.” The Evangelical Quar-
terly, XXXVI (October-December, 1964), 206-13.

LaSor, William Sanford. “Have the ‘Times of the Gentiles’ Been Pul-
filled?" Eternity, KVM (August, 1967), 32-34.

Moody Monthly.

Our Hope.

Oxtaby, W. G. “Christians and the Mideast Crisis.” Christian Century,
LXXXIV (July 26, 1967), 961-65.

Parker, T. Valentine. “Premillenarianism: An Interpretation and an
Evaluation.” The Biblical World, LIV (1919), 37-40,

The Pentecostal Evangel.

Prophecy Monthly.

The Prophetic Times.

Rall, Harris Franklin. “Premillennialism.” The Biblical World, LIM
(1919), 339-47, 459-69, 617-627,

Revelation.

Ryrie, Charles C. “Dispensationalism Today.” Moody Monthly, LXVI
(November, 1965), 58-59.

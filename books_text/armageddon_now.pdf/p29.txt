Foreword xxix

of the premillenarians. It is possible to track the point of view
of these superpersonalities without having to watch every tele-
cast; they write and give interviews.

The second major change I have observed is the explosion
of religious pressure groups. These are para-church organizations
outside of traditional religious denominations. David A. Lewis,
President of the National Christian Leadership Conference for
Israel, lists in his book Magog 1982 Canceled a total of seventy-
seven organizations whose purpose is to support Israel. These
have the advantage of diverting political action of individuals
and factions away from the traditional church. These groups
can siphon off the more radical fringe groups from within the
church and leave the denominations in peace. Church leaders
do not have to live down political activities that might originate
from within their own constituencies and have their missionaries
in foreign countries face violent retaliations from terrorists.

One last change that is worth noting is the relative shift of
interest from Russia to the state of Israel. Rumors of détente,
the decline of Leninist ideology, and the apparent end of the
Cold War (as of late 1990) have diffused the Soviet threat and,
it would appear from my sources at least, the limelight has
shifted to Israel. It is said that half the resolutions passed by
the United Nations are concerning Israel, and the premillenarian
focus has paralleled the public press.

Russia

As we turn to the sources of the recent past, we find glar-
ing examples of provocative paperback titles headlining the
imminence of the Battle of Armageddon. In 1974 Zola Levitt
and Thomas §. McCall produced for Moody Press The Coming
Russian Invasion of Israel. Extensive excerpts were given in
Christian Life (5/75). Billy Graham's associate evangelist John
Wesley White in 1974 published with Zondervan 65,000 copies
of WWII: Signs of the Impending Battle of Armageddon. Billy
Graham himself in 1981 published with Word Till Armageddon.
His reviewer in Charisma (2/81) says that Graham believed that
Armageddon is “practically upon us.” In his 1983 book, Ap-
proaching Hoofbeais, Graham insisted that he did not know
whether Armageddon was near or far, but he paraded before
his readers secular authors who observed that “Everybody who's

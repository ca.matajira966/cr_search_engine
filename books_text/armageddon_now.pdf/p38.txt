xaxvili Armageddon Now!

Damascus to the Euphrates River.” In the “Epilogue,” I have
labeled this predestination justification as antinomianism. This
issue was given explicit expression by Jamie Buckingham in
Charisma (4/81): “Our politicians fail to recognize the tide of
the centuries which is pushing Israel to her destiny... . They
are actually trying to hold back the prophesied move of God.”
Radio teacher Derek Prince suggested that American politicians
should look the other way if they could not stomach the Israeli
actions, saying, “They should not support anything they think
politically unjust, but should get out of the way and let God
do what He is doing” (Christian Life, 5/84).

According to this scheme, not only is it useless to resist the
prophecies, it may be fatal. Pat Robertson continued, saying,
“We do not have to be anti-Arab in order to be pro-Israel... .
But... if we as individuals and our nation collectively, interfere
with God’s plan for the Middie East, we may have to face awful
consequences” (my italics). In a reply to Mark Hanna, John S.
Feinberg writing in the Fundamentalist Journal (9/82) warned
the United States to follow its pragmatic self-interest: “The
topic is not just of theoretical interest, but also of great prac-
tical concern. God promised Abraham in Genesis 12:3 that He
would bless those who bless the descendants of Abraham and
curse those who curse them. If we do not take a proper stance
toward the nation and people of Israel, we may well fall under
that curse.” Feinberg was Chairman of the Department of
Theological Studies at Jerry Falwell’s Liberty Baptist Seminary.

Mike Evans called upon the testimony of history to support
this pragmatic view, pointing out how Spain declined after she
expelled the Jews in 1492, and how Britain had declined since
she had “dealt treacherously” with the Jews who had tried to
return to Israel since the Balfour Declaration in 1917. Derek
Prince claimed as a first-hand observer: “I ‘watched as Britain
tried to intervene and frustrate the rebirth of the State of Israel
and work out a British plan for Israel.... Since then, the
British nation has declined as a great power.” This is the
classical fallacy known as argumentum ad hominem, which says
the argument must be correct because it benefits the man hear-
ing it. Rather than worrying about curses rolling down, perhaps
Americans should “let justice roll down” and “hew to the line,
and let the chips fall where they may.” Those who bless Israel

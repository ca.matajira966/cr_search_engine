Before Balfour and Bolsheviks 7

is to be restored to Palestine, and Russia is to lead a confederacy
of nations in an invasion of Palestine. This simplistic twofold
scheme has continually governed the premillennialists’ response
to both nations throughout this century. This system of escha-
tology was the culmination of developments throughout the nine-
teenth century and had many roots even further back.

According to Peter Toon in his study of Puritan eschatology,
the belief in the restoration of Israel was widely held even among
the Puritans of the seventeenth century. Most Puritans subscribed
to the idea of a large-scale conversion of Jews to Christianity
before the end of time, and in turn believed that there would
also be a return of the Jews to Palestine. Sir Henry Finch in
The World’s Great Restauration, or The Calling of the Jews
(1621) explained that the Euphrates River would dry up for
the Jews to pass through on their return to Palestine, and that
they would be opposed by Gog and Magog (equated with the
Turks), but the Jews would win as God fought for them.’ This
interest in the restoration of the Jews was reinforced by the
horrors of the Thirty Years War in Germany, which led men
to imagine that they were living in the last days Toon cited
the great jurist Hugo Grotius for the observation that eighty
books concerned with the millennium had been published in
England by 1649. A writer of the second quarter of the seven-
teenth century, Joseph Mede, Professor of Greek at Cambridge,
is designated by Toon as the “father of premillennialism,” and
John Milton and Isaac Newton are listed among his indebted
successors.?

The radical Puritans, known as the Fifth Monarchy Men,
believed that the millennium or Fifth Monarchy—the successor
to the four kingdoms of Daniel’s prophecy—would soon be es-
tablished.1° They interpreted the prophetic symbols as contempo-
rary figures in British political life and pictured themselves as the
instruments of God that would tear down the existing structure in
order that God might establish this new kingdom of heaven. This is a
curious contrast to the twentieth-century premillennialists who tend
to be pessimistic, fatalistic, nonpolitical, and nonactivist. John Till-
inghast argued that “the present worke of God is, to bring downe
lofty men, to lay men low, and to throwe down Antichrist.”™ King

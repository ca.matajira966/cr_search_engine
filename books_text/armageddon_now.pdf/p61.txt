 

Before Balfour and Bolsheviks 29

By 1861, when Cumming wrote The Great Preparation, the
international scene had changed: British-French rivalry now in-
fluenced his interpretation. Gomer, Russia’s ally in the final con-
spiracy, was no longer identified as merely extending into Gaul,
but was specifically identified as including France as well as Ger-
many.7° In retrospect, he observed that Russia’s aggression in
1854 had been premature, but this did not thwart his confidence
as he generalized: “Every Russian looks eastward. He calculates
on taking Constantinople on his march, and occupying Palestine.”?!
Specifically, he quoted Peter the Great: “I look upon Russia
as called on to establish her rule over all Europe, and its invasion
of east and west as a decree of Divine Providence.” His cal-
culations on the end of the age were slightly modified, and he
hhazarded yet another date. If his beginning dates were correct
(he acknowledged they might be wrong), “the affliction of the
Jews will cease about the end of 1867, the Jews will be restored,
the Gentile oppression will come to an end; Jerusalem will be
no longer trodden under foot but repossessed.”74

Except for his inclination to set dates, John Cumming is
probably the best nineteenth-century example of the kind of
writer that developed the ideas which contributed to the mind-set
of the twentieth-century premillennialist. His failures in date-
setting may account for the preference of later writers for terms
such as imminent and soon rather than specific dates. The most in-
famous date-setter of all was Michael Baxter, a British preacher
who also toured the United States. From 1861 to 1908 he made
various errors, including identifying Louis Napoleon as the Anti-
christ and predicting the second advent between 2:30 and 3:00
p.m., March 12, 1903.74

The late nineteenth century reflected a broad shift in pre-
millenarian thinking from a historicist position to a futurist po-
sition, The historicists believed that the “days” of Daniel’s prophe-
cies symbolized years, and consequently identified the prophecies
as fulfilled within history (this explains the penchant for setting
dates). The futurists, on the other hand, looked for the fulfill-
ment of prophecies in the future; the period of history between
the first and second advent of Christ was just a parenthesis during
which God’s time-clock had been stopped. The clock would start
again when the church was taken out of the world (the rapture)

34 Armageddon Now!

Jewish people. The government of England offered in 1903 to
provide areas for settlement in Uganda, but the majority of the
Zionists rejected any attempt to sidetrack their main objective.
In 1910 C. I. Scofield commented on Jeremiah 16:14, 15:

I must ask you to note that the restoration is from a world-
wide dispersion, and the “land” as identified beyond per-
adventure as “their land that 1 gave unto their fathers." Not
the United States, not England, not any land where the Jews
may have temporary peace and prosperity, but Palestine.®*

James M. Gray, dean of Chicago’s Moody Bible Institute
explained the anti-Semitism as the hand of God. Israel had failed
“to be a faithful witness to Jehovah before the other nations of
the earth, and in consequence, is suffering the dispersion and
the persecution which, alas! we know about today.”®5 Gray, how-
ever, on the basis of Isaiah 14, predicted a more aggressive role
for Israel in the future. He contended that the text had not yet
been fulfilled for “Israel does not possess the people of the earth
for servants and for handmaids. She has not yet taken them
captives whose captive she was, nor does she yet rule over her
oppressors.”

In the years prior to 1917, Zionism was hailed as the leading
sign of the end of the age. William E. Blackstone, author of the
most widely read premillenarian work, Jesus Is Coming, taught
that Zionism was the sign of the budding fig tree (Luke 21:28-31).
Dr. Isaac M. Haldeman, pastor of the First Baptist Church, New
York City, in a sermon entitled “The Zionist Movement” called
Zionism “the climacteric sign of the restoration.” He felt that
“a vital and fully equipped nationality” was the Jew’s need of
the hour and that Zionism provided the proper prerequisite to a
Jewish national resurrection which would allow the Jew to be
able to “lift up his head and walk in the ordained power that
is his.”°? Haldeman’s interpretation of Scripture required, how-
ever, that the Antichrist should appear before the final restoration:
“The scriptures teach that this man will be the prime factor in
bringing the Jews back, as a body into their own land; that he
will be the power that shall make Zionism a success; that through
him the nationalism of the Jews shall be accomplished." The
sequence here is Antichrist, restoration, conversion.

Zionism and Russian anti-Semitism were both depicted as

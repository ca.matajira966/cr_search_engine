SUBSIDIZING GOD'S OPPONENTS 93.

in the future? What kind of institutional penalties
can we impose if they refuse to contribute their fair
share in the future?

These are not hypothetical. Questions like these
are always sources of conflict in any organization,
including institutional churches. It should be the
goal of peace-keepers to set up institutional buffers
and barriers to such conflicts. Smoothing over
conflicts is one of the most important functions of a
price system. To the extent that churches ignore the
price mechanism, they will produce more conflicts
than would otherwise have been necessary.

EDUCATION One of the obvious sources of conflict
in any church is the question of educating the chil-
dren. First, should the institutional church be in-
volved at all? People debate this fervently. Second,
how should a school be financed? Third, who will
screen the teachers? Fourth, which students will be
allowed in? These are hard questions.

At the level of the elementary school, the issues
are easier to resolve. The basic. curriculum is fairly
well agreed upon. The students should be taught the
fundamental skills of literacy and computation.
They may also be assigned Bible lessons, The
debates between rival schools of theology are less in-
tense, or appear to be, in the case of simple Bible
stories. So the debate over screening of teachers is
subdued, and it tends to focus on teaching com-
petency, that is, on the teacher's ability to impart
seemingly neutral, agreed-upon skills.

The higher we go up the grade ladder, the more

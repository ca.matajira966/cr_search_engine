96 BACKWARD, CHRISTIAN SOLDIERS?

CONCLUSION Is your church playing this game? If
so, what do you think you can do to remedy the
situation? When?

And if your denomination is financing another
denomination’s liberal-dominated faculty, then you
had better make some changes soon. Just because a
group of political liberals once earned Ph.Ds
doesn’t mean that conservative laymen have a moral
obligation to support them in their tenure-protected
security.

‘THE STALEMATE MENTALITY 07

Francisco, and on September 15, the invasion began,
Tt was over the day it started. The American forces
cut the North Koreans off from behind, and within
two weeks the North Koreans were defeated. Half
were in prisoner of war camps; the rest were cut off
in small units or trying to flee home.

The war did not end, of course. The Chinese
Communists invaded with a huge force of 300,000
men in late November. MacArthur had been caught
off guard, despite warning signs. The Chinese had
done to him what he had done to the North Koreans.
It was the worst military defeat of his career. He
learned first-hand what he had taught: there is no
respite during a war. The war goes on until one side wins.

The tst Marine Division was cut off and surrounded.
They had attacked the Chinese from the rear, since
they had been 40 miles to the north when the
Chinese hit the Eighth Army, But the Chinese had
anticipated the Marines’ attack, and had cut them
off. The Marines then broke out of the trap. Lt.
Gen. Walton Walker, the Marine commander, ut-
tered the classic line of the Korean war— which later
became the title of a movie about the war. When
asked by a reporter if he was retreating, he responded:
“Retreat, hell! We're only attacking in a different direction.”
Col. Lewis B. “Chesty’ Puller almost matched
Walker's line: “The enemy is in front of us, behind
us, to the left of us, and to the right of us. They won’t
escape this time.” They hacked their way out for 14
days through blizzards and thousand-foot chasms.
(See William Manchester, The Glory and the Dream
[Boston; Little, Brown, 1972], pp. 530-47.)

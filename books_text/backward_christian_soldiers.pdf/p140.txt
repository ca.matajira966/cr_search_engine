14

THE “LITTLE THINGS’ OF LIFE

 

“tt is the law-order imparted by parents to chil-
dren which will determine the success or failure
of a society.”

 

Western civilization is the historical product of
Christianity. Without Christianity, the development
of the West would have been radically different. Of
course, secular humanism and various intermediary
philosophies have contributed greatly to the growth
and shape of Western institutions since about 1660,
but without the impact of Christian thought and cul-
ture, the foundations of Western secular humanism
would not have been laid. It is impossible to think of
Western culture without considering the historical
impact of Christianity.

Most Protestants understand this fact. Yet at the
same time, they have a tendency to denigrate the
cultural accomplishments of the Roman Catholic

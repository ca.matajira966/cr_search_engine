2 BACKWARD, CHRISTIAN SOLOIERS?

forth good for nothing, but to be cast out, and to be
trodden under foot of men” (Matt. 5:13). Christians
have been losing their cultural savor for well over
two centuries, and with increasing speed since the
end of the Civil War. How could this have hap-
pened?

In the twelfth and thirteenth centuries, Christians
invented the university, one of the greatest engines
of social advancement and cultural enrichment ever
conceived. Now they build backwater Bible colleges
that send the students to the local secular college or
university for the “neutral” academic subjects —like
Freudian psychology, Keynesian economics, and evo-
lutionary anthropology. Christian professors offer no
explicitly biblical alternatives to their students.

CHALLENGING A WHOLE CULTURE The Refor-
mation saw the advent of modern printing, and the
bulk of that printing was Christian, and by no
means limited to gushy devotional tracts. Luther
was challenging the whole fabric of Western culture;
Calvin was rebuilding his city of Geneva (at the
desperate request of the local town leaders); John
Knox was winning Scotland to the gospel, using the
sword as well as the pen.. This literature still sur-
vives, even in secular college classrooms; it changed
our world. The King James Bible established a
standard of excellence in the English language that
has never been surpassed. Try to find a modern ex-
ample of Christian literature that has had this kind
of impact!

In 1974, I sent the manuscript of Foundations of

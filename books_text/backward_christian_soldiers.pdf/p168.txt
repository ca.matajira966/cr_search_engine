154 BACKWARD, CHRISTIAN SOLDIERS?

almost all other professionals, is perpetually short of
both time and money, from the beginning of his
career to the end. He seldom gets ahead on either
resource. This is the nature of pastoral service. The
pastor may be laying up treasures in-heaven, but he
is usually devoid of treasures on earth.

The problem the pastor faces is that it is very
difficult for him to put a prise tag on his time. His alter-
native uses for his time are all non-profit. He doesn’t
ask hirnself, “How much money will I lose if I take a
day off?” He asks himself, “What services will I not
be able to perform if] take a day off?” He can’t put a
dollar value on his time, precisely because the kinds
of services he provides are not normally for sale in a
competitive, profit-seeking market. We aren’t sup-
posed to sell the message of salvation to the highest
bidders. Salvation is not a mass-produced item to be
sold through mass-marketing techniques, however
often certain modern evangelists try to adapt such
techniques.

If a man finds it difficult to put a price tag on his
time, then he had better figure out another kind of
allocation standard. If five conferences want to get a
big-name evangelist to appear in one month, and he
isn’t willing to sell his time to the highest bidders,
then he had better have an alternative standard in
mind,

The standards tend to become highly personal,
since they are not fundamentally monetary, The
pastor decides to counsel someone with a family
problem rather than someone else with an employ-
ment problem. He tends to cater to those who have

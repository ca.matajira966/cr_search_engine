PASTORAL EDUCATION 163

cations of the older, established doctrine) are saying,
in effect: “Those who have gone before did not see
the full implications. Their successors have clung for
too long to outmoded views, repeating the words of
the Founders. It is time to examine and apply the
implications of the older view.” In short, the
newcomers are calling into question the vision, or in-
tegrity, or competence of all their “ideological first
cousins.” They are telling the world (meaning a
handful of people who will listen) that the people
who are defenders of the “received faith” are no
longer able to apply the underlying implications of
the received faith to modern conditions. Thus, the
directors of the existing institutions will do what is
necessary to see to it that proponents of the new vi-
sion are kept far away from the seats of institutional
power. The newcomers are regarded as a greater in-
tellectual threat than the “unbelievers outside the
camp.”

The younger men, or newly converted believers
from “outside the camp,” who respond favorably to
the explanations of the new perspective will be in-
terested in furthering their education. But they
almost invariably make a fundamental mistake.
‘They assume that educational techniques are essen-
tially the same everywhere: old movement, new
movement, and partially accepted movement. Not
so. Educational techniques vary considerably,
depending on which phase of the movement we are
dealing with.

In the early phase, there are many loose ends. A
comprehensive system has not yet been developed.

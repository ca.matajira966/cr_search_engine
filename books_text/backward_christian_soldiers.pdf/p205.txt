21
CHURCH NEWSLETTERS

«,.. they are effective because they are read.”

 

As you might imagine, ’'m a firm supporter of
newsletters. I support myself by means of Remnant
Review, my economic report. I keep people in-
terested in the Institute for Christian Economics by
means of the various letters put out by I.C.E. I have
found that my newsletter essays are quoted more
widely than my books. So in terms of short-run im-
pact, the newsletter is a better medium than bocks.

Why newsletters? There are a lot of reasons. Let
me list the readers’ reasons: 1) time constraints; 2) at-
tention span; 3) ease of reading or skimming; 4)
timeliness; 5) specificity; 6) ease of lending; 7) per-
sonalism of communication; 8) ease of filing; 9) ease
of reviewing; 10) action-orientation.

Now let me list the advantages for the publisher:
i) time constraints; 2) research limitations; 3)
timeliness; 4) specificity; 5) training programs; 6)

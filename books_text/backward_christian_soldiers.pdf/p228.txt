214 BACKWARD, CHRISTIAN SOLDIERS?

ands of pages. This is the Kaypro 10. Radio Shack’s
TRS-80 (Model 4) can be purchased with two
diskette drives for $2,000. The 4p is portable
($1,800). The Apple Ie with appropriate equip-
ment, costs about the same. Both are widely avail-
able. Both are easy to use, The Apple seems to be
most favored by users, but the TRS-80 offers quick
servicing (rarely necessary).

For maximum availability of programs and easily
available repairs, the IBM PC or PC XT (with
10-megabyte hard disk drive) probably would be
best. My favorite word-processing program is Word-
Perfect. WordPerfect is available for $500 from
Satellite Software International, 268 W. Center St.,
Orem, UT.

The advantages a computer offers to a growing
church program cannot be overestimated. Consider
the following fields: mailing list development and
maintenance, word processing, publishing, budget-
ing and records, and a legal defense program (see
“Brush Fire Wars.”) Churches that ignore the bene-
fits of a computer may wind up paying for more in
Sorfeited opportunities than the computer would cost.
(What the tool offers deacons is remarkable: the
ability to help struggling families think about the
family budget, design a budgeting program, and a means
for housewives to manage it monthly. Could each
family help finance the computer by donating money
for time used for better budgeting?)

The microcomputers are powerful tools. As soft-
ware improves—and thousands of geniuses, all look-
ing for a profit, are out there developing new

‘THE CASE FOR A SATELLITE TV. RECEPTION DISH 219

for Sileven what it did for Martin Luther King. It
brought to light what the bureaucrats were doing. I
was amused at a 1982 ABC TV report. They inter-
viewed one woman, a supporter of the local public
school, who said, “I think Nebraska’s affairs should
be run by Nebraskans, not all these outsiders.” This
was the same argument Southern whites used in
1962, the “outside agitators” argument. Television
coverage made the difference for King. It could
make the difference for Christians who want to re-
capture the nation.

The existence of satellites and cable channels has
at last broken the hold of the major TV networks.
The level of information provided by a 30-minute in-
terview is far greater than that provided by a
2-minute news snippet. News snippets are designed
to hold an impersonal audience’s attention long
enough to sell a percentage of them some soap.
There is no dedicated group of viewers who are emo-
tionally committed to an anchorman. On the other
hand, there are millions of viewers who are personally
committed to one or another of the electronic
churchmen. Thus, they will sit in front of the screen
and listen to a lengthy interview, and even try to un-
derstand it. This puts a major educational tool into the
hands of Christian leaders—a tool which the humanists
cannot match on television because of the “least com-
mon denominator” principle which governs the
Nielsen rating wars.

The lever of television gives the local Christian
soldier hope. He knows there is a potential army of
supporters behind him, if he gets in a difficult situa~

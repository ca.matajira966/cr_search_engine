‘THE CASE FOR A SATELUITE TV. RECEPTION DISH 223

izations, Chalcedon Foundation, and other educa-
tional groups. A body of explicitly Christian litera-
ture in several areas has been produced over the last
two decades. These educational resources should be
integrated into an overall program of education and
mobilization.

In the past, there has been a problem of com-
munication between “lever builders” and “fulcrum
builders.” The “lever builders” have been fearful of
becoming too intellectual, too controversial, and too
action-oriented to maintain their large, essentially
passive Christian audiences. The risk of controversy
has been too great. The “fulcrum builders” have
resented pressure from the “levers” to “water down”
their message in order to meet the needs and in-
tellectual abilities of mass audiences. They have
chosen instead to gather still more footnotes, develop
still more complex theories, and publish ever fatter
books in the quest for the near-perfect intellectual
equivalent of Augustine’s City of God or Calvin's
Institutes.

In the providence of God, both sides have been
correct, up until now. The levers are longer, and the
fulcrums are stronger, than they would otherwise
have been had the developers in each camp been too
concerned with imitating the other. But now the
levers are in place, and the fulcrums are as ready as
they need to be at this moment in history. Christian
viewers are not nearly so passive these days. They
see clearly the threat of humanism for the first time.
Therefore, it is time to meet the newly felt needs of
these viewers. There is always a need for larger au-

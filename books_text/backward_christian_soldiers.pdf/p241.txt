Part V: THE DURATION

25

OPTIMISTIC CORPSES

 

“There is hope for the kingdom of God on earth,
precisely because there is no hope for God's
people to escape the sting of death.”

 

Few concepts are more important to a man or a
civilization than the idea of time. Much of what men
and whole societies do in life is influenced by men’s

-views of how much time they have in life. For
apocalyptic thinkers, the very idea of time is called
into question: time will run out. Eternity beckons.

Secularists have no ultimate faith in time. Time ts
man’s inevitable victor. Bertrand Russell, the British
philosopher and mathematician, has expressed the
faith of the evolutionists quite well: “The same laws
which produce growth also produce decay. Some
day, the sun will grow cold, and life on earth will
cease. The whole epoch of animals and plants is only
an interlude between ages that were too hot and ages
that will be too cold. There is no law of cosmic prog-

26

HOW MUCH TIME?

 

“Aim high, aim carefully, and shoot long.”

 

It should be obvious that a suitable strategy for a
marathon runner is considerably different from a
sprinter’s strategy. If you have 26 miles to cover,
your pace will be a lot steadier, and you will be called
upon to maintain considerably greater reserves of
energy. The sprinter will give a lot more attention to
the crouch, the starting blocks, and the signal to
start. The distance runners do ‘not even bother to
crouch; there are no starting blocks; and you never
see a false start by those anticipating the starting
run. It is endurance, not speed out of the blocks, that
will determine the success of the marathon runner.

The Hebrews were promised a kingdom land in
Canaan. Yet the promise took several centuries to
come true, from Abraham’s day to Joshua’s, and the
period of training involved years of captivity and
four decades in the wilderness. So while their advent

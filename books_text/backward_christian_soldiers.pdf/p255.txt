HOW MUCH TIME? 247

builders revered. However, if they built in terms of a
broader-based kingdom ideal—an ideal encompass-
ing beauty, majesty, craftsmanship, and architec-
tural skill~then their efforts were not wasted. Of
course, it seems likely that they built for both
reasons, for the kingdom clearly encompasses the
church as an institution, so part of their desires have
been. frustrated, But time frustrates almost every
human vision to some extent, and theirs at least has
persevered in the realm of aesthetics. Like the
builders of the tabernacle, or Solomon’s temple,
their efforts were later misused by evil men, but they
Jeft a heritage nonetheless.

TRACTS AND TREATISES Is it preferable to spend
$10,000 on printing up a million tracts, or is it better
to print up 7,500 books? Again, it depends. The
tracts will not survive the test of time. The gospel is
furthered in a particular era, assuming the tract is
actually distributed and read. But the tract is short-
lived. Its effects will be brief, except insofar as those
converted by its message go on to write more tracts,
or evangelize on a face-to-face basis. The printer of
tracts knows that the results will be visible soon, or
not at all, for a tract is like a shooting star: bright,
possibly exciting, but soon dimmed.

The book producer knows that his book may stand
the test of time. It may survive and become the foun-
dation of a new movement, a new school of inter-
pretation, the basis of a new civilization. Certainly,
Augustine’s writings became just that (William
Carrol Bark, Ongins of the Medieval World). So did

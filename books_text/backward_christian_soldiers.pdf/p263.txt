THE LONG, LONG HAUL 249.

Party funds to assure his re-election, thereby
depleting the funds remaining for House and Senate
campaigns.)

So the conservatives, who believe in continuity,
pray for discontinuous events: Presidential or
parliamentary victories every few years. In between,
they return to their normal pursuits: family life,
business, education, etc. The humanistic liberals
believe in salvation by politics, and they devote large
quantities of their time, money, and energy to this
end. The conservatives specifically do not affirm a
faith in salvation by politics, so they tend to be less
devoted to the cause of politics. This has produced
the anomaly: liberals working for years to achieve a
smashing political victory across the boards, and
conservatives working sporadically, hoping for an
occasional figurehead victory. The liberals staff the
bureaucracies and the appointed positions, even
when a conservative wins the top office. The liberals
have the experience and the dedication to politics;
they have the expertise to gain the offices’ staff posi-
tions. Conservatives lack the trained, dedicated
troops to carry through on an occasional political
victory, from grass roots to staff positions at the top.

When conservatives gain a victory, they go home.
They think they have done their duty for the year, or
even for the next four years. They turn away from
politics, not having a great deal of faith in politics.
The liberals never stop pushing, subverting, or
infiltrating. They never go home. Conservatives,
because they often have the support of the mass of
actual voters on many issues, are capable of electing

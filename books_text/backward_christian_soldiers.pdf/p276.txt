CONCLUSION

 

“. .. the steady, thoughtful, informed presenta-
tion of a biblical vision of victory will eventually
Produce converts.”

 

‘This book is both educational and motivational. If
you have agreed with its overall perspective, then
you owe it to yourself, your fellow Christians, the
world around you, and most of all to God, to begin
to rethink your theology. You have an obligation to
determine for yourself whether or not you are
responsible to begin a personal program of Christian
reconstruction, beginning with your own daily
affairs, and continuing for the rest of your life. And
if you have this responsibility, then other Christians
have it, too. Will you help them to come-to grips
with this responsibility by telling them what you
have learned? Will you help to encourage your
fellow church members to step forth and take a stand
for the crown rights of King Jesus?

 

16 BACKWARD, CHRISTIAN SOLOIERS?

Christ as the head, modern Christians have adopted
the Roman Catholic practice of regarding the priest-
hood. as the head, heart, and hands, with laymen
serving as the back, feet, and legs. In other words,
the priests serve as the unquestioned “specialists in
religion,” while laymen, including elders, serve as
the “secular” hewers of wood and drawers of water.
The laymen are specialists in the things of “the
world,” while their priests take care of the spiritual
realm, We call this outlook “sacerdotalism.”

This unfortunate development began very early in
the history of Protestantism, though its full implica-
tions have taken several centuries to work out in
practice. Protestant sacerdotalism, like Protestant
scholasticism, has been with us for a long time,
Laymen have automatically assumed that the divi-
sion of labor spoken of in the New Testament is a
division of labor between secular and spiritual pur-
suits. But this is not what the New Testament
teaches, The New Testament’s vision of Christ’s
comprehensive kingdom involves the whole world.
Jesus announced: “All power is given unto me in
heaven and in earth” (Matt. 28:18). Modern Chris-
tians really cannot seem to accept Christ’s words,
They reinterpret them to say; “All power is given to
me in heaven, but I have abdicated as far as the
earth is concerned,” The priests, as representatives
of Christ’s spiritual power, which is supposedly the
only real power that Christ systematically exercises,
are understood to be the central figures in the king-
dom. Laymen, who supposedly specialize in earthly
affairs, are bearers of an inferior authority (not.

RECOMMENDED READING 299

Christianity and Civilization, No. 2: The Theology of
Christian Resistance Gary North, Ph.D., editor.
887 pages, kivar, $9.95,

There are presently 6000 cases in the courts involv-
ing churches. This symposium is designed to answer
specifically the thorny questions involved in resist-
ance to lawfully constituted authority.

Articles included are:

© Francis A. Schaeffer: “Conflicting World Views:
Humanism versus Christianity.”

¢ John W. Whitehead: “Christian Resistance in the Face
of State Interference.”

* Gary North: “Confirmation, Confrontation, and
Caves.”

‘ James B. Jordan: “Pacificism and the Old Testament.”

* and fifteen more!

The Theology of Christian Resistance is the only com-
prehensive effort to address the substantive issues of
Christian resistance to tyranny. Not only is the pres-
ent state of the conflict surveyed, but a lengthy sec-
tion details the foundational principles of Christian
resistance.

Christianity and Civilization, No. 3: Tactics of Chris-
tian Resistance Gary North, Ph.D., editor. 528
pages, kivar, $14.95.

This newly released symposium picks up where its
companion The Theology of Christian Resistance left off.
Instead of dealing with the problems resistance
raises, this book discusses how to do it!

Tactics of Christian Resistance is the most practical

4

FUNDAMENTALISM:
OLD AND NEW

 

“,.. old fundamentalists are becoming new fun-
damentalists, and the . .. new fundamentalists
are preaching a vision of victory.”

 

Historians always get themselves in trouble when
they announce, “On this date, a new movement was
born.” Their colleagues ask them, if nothing else,
“How long was the pregnancy, and who was the
father?” The so-called “watersheds” of history always
turn out to be leaky.

With this in mind, let me plunge ahead anyway. I
contend that there is one event by which you can
date the institutional separation of the old fun-
damentalism and the new. It took place in August of
1980 in the city of Dallas, Texas. It was the National
Affairs Briefing Conference. At that meeting, the
“New Christian Right” and the “New Political Right”
political technicians came together publicly and an-

26 BACKWARO, CHRISTIAN SOLDIERS?

become the law of the land, that the secular human-
ists who have dominated American political life fora
hundred years can be tossed out and replaced with
God-fearing men. Every area of life is open to Chris-
tian victory: education, family, economics, politics,
law enforcement, and so forth. Speaker after speaker
announced this goal to the audience. The audience
went wild.

Here was a startling sight to see: thousands of
Christians, including pastors, who had believed all
their lives in the imminent return of Christ, the rise
of Satan’s forces, and the inevitable failure of the
church to convert the world, now standing up to
cheer other pastors, who also had believed this doc-
trine of earthly defeat all their lives, but who were
proclaiming victory, in time and on earth. Never
have I personally witnessed such enthusiastic schizo-
phrenia in my life, Thousands of people were cheer-
ing for all they were worth—cheering away the
eschatological doctrines of a lifetime, cheering away
the theological pessimism of a lifetime.

Did they understand what they were doing? How
can anyone be sure? But this much was clear: the
term “rapture” was not prominent at the National
Affairs Briefing Conference of 1980. Almost nobody
was talking about the imminent return of Christ.
The one glaring exception was Bailey Smith, Presi-
dent of the Southern Baptist Convention, who later
told reporters that he really was not favorable to the
political thrust of the meeting, and that he came to
speak only because some of his friends in the evan-
gelical movement asked him. (It was Smith, by the

68 BACKWARD, CHRISTIAN SOLDIERS?

strated in numerous books, this appeal rests on the
assumption of human autonomy, that is, the univer-
sally valid logic of human minds. It is an invalid pre-
supposition. The only common ground is the sense
of God’s image in all men.

Secularists, especially prior to the mid-1960's, also
appealed to “natural law” or “technocratic, non-
ideological, pragmatic wisdom,” in order to convince
men of the universal validity of one or another pro-
gram of social reconstruction. However, since the
mid-1960's, this appeal increasingly has fallen on
deaf ears; Marxists, revolutionaries of all brands,
and systematic relativists have rejected the whole
idea of a hypothetical universal logic. (Marx always
rejected the idea.)

Hf there is no neutrality in human thought, then
there is certainly no neutrality in any society’s law
structure. Laws are written to prohibit certain ac-
tions, These laws rest on the presupposition that cer-
tain acts are inherently wrong, according to a partic-
ular moral and religious order. There can be no law
apart from a moral and religious law-order, and this
law-order cannot possibly be neutral.

SOCIAL RECONSTRUCTION If men are to work
out the implications of their religious faiths, then
they will attempt to reconstruct the external institu-
tions of society in terms of a particular law-order.
Only a totally internalized religion can legitimately
neglect the tasks of external renewal. Yet it is very
difficult to imagine how such a totally internalized
religion might operate. How can we speak of ethics

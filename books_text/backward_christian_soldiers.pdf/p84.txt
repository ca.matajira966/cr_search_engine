7D BACKWARD, CHRISTIAN SOLDIERS?

told to make his peace with one or another non-
Christian social order. The Christian is told to
refrain from actively opposing, and then replacing,
the prevailing social order.

Christians are in the world (a geographical
identification), but not of the world (a spiritual
identification). The question is: Should Christians
attempt to subdue the world in an attempt to make it
conform more closely to God’s guidelines for exter-
nal institutions? More to the point: Are there biblical
guidelines for social institutions? If not, have we not
asserted a fundamentally demonic universe, wherein
neither we nor the devil may be judged for our ac-
tions, since. we have violated no godly standards?

In short, isn’t the argument for neutrality—
neutrality in any sphere of human thought or
life—an argument for autonomy? Isn’t it an asser-
tion of some universal “King’s X,” an ever-growing
area of human action (or inaction), in which God
may not legitimately bring judgment, precisely
because He has no standards of action that apply?
Isn’t the idea of social neutrality a defense of the idea
that man and Satan can live beyond good and evil?

RECONSTRUCTION Dr. Martyn Lloyd-Jones was
one of the most respected preachers in England. His
books have been published and widely read in the
United States as well. He was trained as a physician,
but he left medical practice to become a minister, He
became an important advocate of Christian surren-
der to the world, and because of his prominence, we
should examine his blueprint for “Christian inaction,”

HUMANISM'S CHAPLAINS 81

of evangelical Christianity. Their job is to keep the silent
Christian majority forever silent, or, where the majority
is no longer Christian, to keep the Christian minority
fearful, despondent, and impotent. They have done
their job very well. They have been supremely victoriotis in
this century tn promoting the psychology of perpetual Chris-
tian defeat. Chaplains for the status quo, they have
paraded in the uniforms of “impossibility thinking”
— the impossibility of Christian reconstruction in to-
day’s society of humanistic evolutionism.

What Lioyd-Jones really resented was the free
market. He shared this resentment with others in the
Grand Rapids-Toronto-Wheaton-Edinburgh-London-
Amsterdam Axis. He reserved his worst epithet for
the free market: Arminian. “Arminianism: over-
stresses liberty. It produced the Laissez-faire view of
economics, and it always introduces inequalties—
some people becoming enormously wealthy, and
others languishing in ‘poverty and destitution” (p.
106). Get this: the free market :nivoduces inequality. It
apparently wasn’t there before. This is not only poor
logic, but it is inaccurate historically. As the
voluminous researches of Prof. P. T. Bauer and other
economists have demonstrated, the free market
reduces economic inequality, and it also erodes the
barriers—status quo, statist barriers—that tend to
prevent upward and downward economic mobility.

What is so unique about Lloyd-Jones’ resentment?
Nothing. It is the standard, run-of-the-mill pap that
has been stuffed into the heads of two generations of
American college students, and three generations of
British students. It is the same old Fabianism, the

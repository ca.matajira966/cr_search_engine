9

HUMANISM’S ACCOMPLICES

“As far as most Christian campuses are con-
cerned, the theology of retreat has accomplished
the goals of the secularists: to snuff out the
life-giving, society-reconstructing message of
Christ to the whole of man’s existence.”

 

What general would attempt to lead his forces into
battle without a specific battle plan? What military
commander would be content with nothing more
than verbal exhortations to his troops to “be vic-
torious” or “win one for the folks back home”? Such
noble exhortation, apart from a battle plan, equip-
ment, and explicit instructions to subordinates,
would be about as likely to produce victory as the
endless repetition of “Have a nice day.”

When we sing “Onward, Christian Soldiers,” do
we expect to lose on every battlefield? Do we expect
constant defeat to be God’s training ground for total
victory? “Victory through defeat” may be the chosen

16 Baptized Inflation

Hutt’s book, The Theory of Idle Resources (1939). Hutt argued bril-
liantly that there can be no such thing as an idle resource. Every
scarce economic resource is owned by someone. Every resource is
therefore the object of purposeful human action. People may keep
resources off the market, but they have reasons for doing so.
Keynes never replied to Hutt. He at least had an excuse: he was
running Britain’s monetary policy during World War II. His disci-
ples have no excuse.

If the economic model used by the Keynesians does not tell us
“where the market went wrong” — and the analytical bankruptcy
of Keynes’ supposed refutation of Say’s Law indicates that he had
no such model — then they are stuck with some other model, or
even worse, no model at all. What we need is explanations, not
assertions. If the market economy employs resources efficiently in
the vast majority of cases (which Keynes never denied), then how
does it accomplish this? What explanatory device can the Keynes-
ians offer which shows that the free market is almost always suc-
cessful in employing resources, yet not quite successful enough so
as to allow us to dispense with the services of an ever-growing
army of economic planners?

How Private Should Ownership Be?

Private ownership? It depends, after all, on how you define it.
Dr. Vickers is happy to concede private ownership, if the individ-
ual’s actions are subservient to those of some regulatory agency of
the State. This is not the kind of ownership which the Bible talks
about, when even the King may be refused his request.

Imagined socialism? Far from it. Dr. Vickers is advocating
government economic planning. What is socialism if not this? His
arguments are the same tired arguments that Marx and Engels
used — the alleged anarchism of free market production and distri-
bution — and his conclusions are similar: more government con-
trol over this supposed anarchy. Nevertheless, he prefers to call
his system “Christian economics .* Why? Does his reluctance to

36, Second edition: Indianapolis, Indiana: Liberty Press, 1979.

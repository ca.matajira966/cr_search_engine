Antinomianism and Autonomy 101

they will treat those who practice abortion?

A similar difficulty appears for Dr. Vickers. Having denied
the idea of a theocracy in order to deny the specifics of Old Testa-
ment law, it should not surprise us that little use is made of the
Scriptures to provide explicit guidelines on how our economics
should be developed. We have observed earlier that Keynes was
hostile to Biblical Christianity, and it comes as no surprise to find
that he was not informed at any point by the light of Holy Writ.
But to find a professing Christian paying lip service to the intellec-
tual ravings of an economic, philosophic, and moral Philistine is
somewhat disconcerting, to say the least.

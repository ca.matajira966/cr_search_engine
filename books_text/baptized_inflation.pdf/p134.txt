Economic Law itt

Dr. Vickers would like to challenge that belief. But “there does
not exist a law’ of economics in the same sense” as, to use Dr.
Vickers’ example, the “law of gravity.”*° True, the law of gravity is
an unchangeable factor in scientific investigation, and any scien-
tist wanting to send rockets to the moon, for instance, must take
this law into consideration. Laws such as this allow the scientist to
make the necessary calculations for such a venture, and the rea-
son for this is relatively simple. If there were no laws in the sense
indicated, this physical world would operate by pure chance, and
if this universe is pure chance, the possibility of scientific in-
vestigation is, in principle, impossible. The physical sciences are
dependent upon the fact that when scientists make an investiga-
tion, or conduct an experiment, there are certain “givens” which
they can count on, things that will not change. Take as an exam-
ple the attempts to place men on the moon. Certain calculations
are necessary, based on the fact of certain “laws, ” one of those laws
being in this case the law of gravity.

The idea of “laws” is inherently Christian in origin. It is no co-
incidence that the rise of scientific investigation has taken place in
those countries heavily influenced by the Protestant Reformation. |
Orthodox Christianity teaches that God controls whatsoever
comes to pass according to the counsel of His own will (Eph. 1:11).
It does not involve any violation of Scripture to make the deduc-
tion that we creatures may observe certain occurrences in God’s
universe and conclude that they will appear with some regularity.
Observation then becomes the basis for determining the outlines
of these regularities, and those things which do recur are called
“laws .“ We do not hold them on the same level as the “laws” of
Scripture, for that is the unchangeable Word of God. But on the
creaturely level, we may rightly say that a regular occurrence is a
“law” in that sense. What we must do, however, is consider that
because these “laws” originate in the observations of finite human

10, Vickers, Economics and Man, p. 91,

11, For many citations to the scholarly literature, see the essays by E, L, Heb-
den Taylor and Charles Dykes in The Journal of Christian Reconstruction, VI (Sum-
mer 1979),

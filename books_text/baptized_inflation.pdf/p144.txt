Economic Law 121

Keynesian system is a subset.) Recall his descriptive phrase concern-
ing the free market, that market forces “gyrate uninhibitedly and
randomly of [their] own accord .”” Dr. Vickers wants us to believe
that State regulation of the economy is necessary because of the mar-
ket’s apparent chaotic nature, and that his demands for conserva-
tion, development, and equity can only be met by regulation of the
economy from the top down, But what makes the “statistical regular-
ities” of State planning so predictable and scientific? What institu-
tional controls need to be implemented to make politicians and bu-
reaucrats trustworthy? Dr. Vickers never even raises the question.

Despite his assurances to the contrary, Dr. Vickers does not
want the kind of society in which individuals are free to pursue
their own goals. Why not ? There can be only one reason: he
believes 4is goals should be the goals of everyone in society!
Keynesians want to impose ‘heir view onto everyone else. If they
could prove that God’s law supports them, we should listen, but
this case, above all, is what Keynes would never have attempted.
to argue. He was self-consciously in revolt against God and God’s
law. This man-manipulating goal is the goal of all socialists, and
Dr. Vickers, as we have seen, is a socialist, if not of the first
order (government ownership), then at least the second order
(government control). Socialists are so convinced, for example,
that it is somehow “immoral” for some to be wealthy while others
are in poverty, that they insist that private wealth must be redis-
tributed by various means, and the taxing system is the method.
Dr. Vickers would use.”

Tyrants through all ages have always thought their ideas
superior to those of the common person. Plan as they will, how-
ever, the socialists cannot get past one undeniable fact, and that is
that people are human beings who will act according to the way
they think, irrespective of the rules which may be imposed upon
them. The y are inveterate “seekers after loopholes .” They may
conform for a time if sufficient force is used, but in the long run

22, Vickers, Economics and Man, pp. 234,
23, Chapter 5, above.
24, Vickers, Economics and Man, pp. 819, 840.

132 Baptized Inflation

hesitation would go, if I really want what_you want to sell. And if you
don’t have what I want, I can get someone else to sell me what I
want, if I had more money, and then he can buy what you're sell-
ing. All I need is more money. But where am I going to get more
money? How am I going to get more money?

Keynes’ answer? Get it from the government. This is called ‘fiscal
policy.”

But where will the government get it? From taxes. Response:
then those who get taxed can’t spend it. Aggregate spending
doesn’t change. All right, you have a point. The government will
have to borrow it. Response: then those who loan the government.
the money can’t spend it. Aggregate spending doesn’t change. All
right, the government has a third option. Jt can print more money.
This is called monetary policy.

No, you are saying to yourself. It can’t be. Not that. That has
been tried over and over again since the dawn of the division of
labor economy based on money. That is just the same old govern-
ment confidence game. That is the same old government-approved
counterfeiting scheme. That is the old-time religion of inflation.
Keynes must have offered something more constructive than this.
Surely there is some super-sophisticated answer buried in all that
verbiage, some magic formula hidden in all those equations.
Surely. Because if there isn’t, and if economists and politicians
were to accept his answer, we would find ourselves in the age of
inflation.

And that, my friend, is exactly where we find ourselves.

The Age of Inflation (and Unemployment, Too)

We speak of the Keynesian revolution. So do the Keynesians.
We are assured that all pre Keynesian economics is dead. The
winners in the competition for the minds of men are the Keynes-
ians. We have the proof. Keynesian policies have produced uni-
versal prosperity. We are all trading with each other again. The
depression is over. It has been over for almost half a century.
There is one man who did it all: John Maynard Keynes. We are
now ready, announced Walter Heller in 1966, for the New Frontiers

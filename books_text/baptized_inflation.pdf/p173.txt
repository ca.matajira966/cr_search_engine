152 Baptized Inflation

when Keynes quoted J. S. Mill’s version of Say’s Law, he conven-
iently omitted this important qualification. ) When prices are no
longer in equilibrium the result is a relative overproduction of some
goods (the ones out of balance), but the market can be cleared of
any surplus by a readjustment of the particular prices to reflect the
changes in people’s subjective valuations of those items. Legally
flexible prices, both upward and downward, provide the necessary
legal framework to allow profit-seeking, future-oriented entrepre-
neurs to seek out those buying and selling prices that will clear the
market of a relative overproduction of goods by restoring the bal-
ance between the supply of such goods and the demand for them.

Note also the discrepancy which exists between Dr. Vickers’
version of Say’s Law and the actual words of Say. Nowhere does
Say give any indication that “money values, or money incomes,
earned from producing goods would automatically be spent .*
Here is another fine example of Dr. Vickers’ inability to accurately
represent those with whom he disagrees.

Downwardly Flexible Prices

A flexible price mechanism, however, is something Dr. Vickers
chooses to omit from the discussion. ‘Say’s Law did not hold in fact ,“
he asserts, and cites the twenty-five percent unemployment rate in
the United States during the depression years as proof. What Dr.
Vickers neglects to discuss, however, is whether downwardly inflexi-
ble wage rates -legally inflexible, because of government interfer-
ence — were a cause of this unemployment. Given the situation of
the 1930's, and the unwillingness of many government-protected.
trade union members to take a reduction in wages, unemployment
would have been predicted by any of the classical economists.

Dr. Vickers, by endeavoring to “refute” Say’s Law, has to
adopt that form of argument which he categorically disallows: the
“fallacy of composition” argument. Since there can be a temporary
overproduction of some goods because of erroneous prior forecast-
ing by specific producers, Dr. Vickers draws the conclusion there
can be a general and continuing overproduction, implying the gen-
eral misforecasting by most producers. But is this not making the

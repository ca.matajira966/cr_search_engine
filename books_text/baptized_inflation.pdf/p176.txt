Say’s Law 155

courage competition. What they never want to admit is the point
that Dr. North keeps hammering away at: “cut-throat” competi-
tion really means “cut-throat opportunities” for consumers. ”

In the Keynesian system, however, competition has a special
meaning. Dr. Vickers illustrates the theory which favors government
economic activity with this example: the Australian government’s
national airline and bank which compete with private companies.
Ultimately, though, this kind of illustrative argument depends upon
how the word “competition” is defined. In the case of the federally
owned airline, it is worth noting that the Australian government's
airline corporation offers services against only one competitor,
other companies being refused permission to enter the industry to
offer competing services. Until July 1981, when some freedom was
allowed the two airlines, all fares were the same by government
decree, and nearly all flights were parallel — that is, the respective
flights to the same destination departed and arrived within a few
minutes of one another. (1 suspect that had the airports been
equipped with parallel runways the flights would have been at ex-
actly the same time!) Hence, the only competition that exists be-
tween the two airlines is in areas such as airport facilities, quality of
in-flight services, and the attractiveness of the hostesses. For those
Australians who would much prefer to bring their own meals in
sacks if the fares were lower, the government has limited their
choice. (I will not even entertain the possibility of hiring ugly hos-
tesses: no Australian would ever admit to preferring ugly hostesses
and lower fares. Except, of course, the male passengers’ wives. )”

20. Gary North, “Cut-Throat Opportunities,” The Freeman (June 1983),

21, Dr, North argues privately that the deteriorating attractiveness of Ameri-
can hostesses since 1970 is the product of three phenomena: 1) fast jets: their in-
creased speed over propeller-driven planes has reduced the time in which unmar-
Tied hostesses can spot potentially well-heeled husband prospects, and then strike
up conversations with them; the speed also increases their work load per minute;
2) union contracts that always favor existing employees to newcomers, and which
keep wage rates high, thereby encouraging middle-aged hostesses to stay on their
abovemarket wage jobs; and 3) price competition among airlines rather than
‘hostess attractiveness” competition, itself a product of deregulation, He em-
phasizes number 2 as the main reason,

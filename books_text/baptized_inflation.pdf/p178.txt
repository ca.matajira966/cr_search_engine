Say’s Law 187

they attempt to produce for a future, unceriaintyjilled marketplace,
and as consumers, they plan in the present to be able to buy goods
on that same future, uncertainty-filled market. The realit y of uncer-
tainty is basic to all human planning. Men are not omniscient.

The key element which is missing in Dr. Vickers’ analysis is
the central economic actor in the “Austrian School’s” analysis: che
entrepreneur. The entrepreneur’s task is to predict future conditions
of suppl y and demand. He competes against other forecasters. He
may be correct in his estimates, in which case he will reap a resid-
ual: profit. He may be incorrect, in which case he will produce
losses. But they key idea here is uncertainty. Every economic order
must deal with it.

Is the profit-seeking, loss-bearing entrepreneur the best per-
son to deal with uncertainty, or the government bureaucrat who
owns no shares of the bureaucracy he is running? Is the person
you think should act for you as your representative the entrepre-
neur (to whom you can say, “No, I don’t want to buy it; and you
suffer the loss”) or the bureaucrat (who can say to you, “You'll take
it; your taxes have already paid for it; and if you don’t like it, you
take the loss”)? The problem of “overproduction” is always entre-
preneurship. As Mises summarized Say:

Commodities, says Say, are ultimately paid for not by money, but by
other commodities. Money is merely the commonly used medium of
exchange; it plays only an intermediary role. What the seller wants
ultimately to receive in exchange for the commodities sold is other com-
modities. Every commodity produced is therefore a price, as it were, for
other commodities produced. The situation of the producer of any com-
modity is improved by any increase in the production of other [non-
competitive — I. H.] commodities. What may hurt the interests of the
producer of a definite commodity is his failure to anticipate correctly the
state of the market. He has overrated the public’s demand for his com-
modit y and underrated its demand for other commodities. Consumers
have no use for such a bungling entrepreneur; they buy his products
only at prices which make him incur losses, and they force him, if he
does not in time correct his mistakes, to go out of business. On the other
hand, those entrepreneurs who have better succeeded in anticipating the
public demand earn profits and are in a position to expand their business

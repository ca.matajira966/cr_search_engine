10
SOVEREIGNTY AND MONEY

This, however, brings us to the second of the two preliminary
Points itwasdesired to make. This has to do with the place of gold in
the monetary economic system, and with what we noticed earlier as the
claim by some Christian authors, Gary North and Rousas Rushdoony
in particular that “unbacked paper money” is immoral... . It is un-
fortunate that at this point considerable confusion has been allowed to
enter economic argument from a purportedly Christian perspective. It
was in order to contribute to a correction of that perspective that we
have developed the entire argument of this book in the manner and in
the order we have adopted. '

Money, as the song reminds us, makes the world go around.
Some of us can at least get quite dizzy when trying to wade
through the teaching of economists on this topic. Again, however,
there is nothing magic about it.

If all goods were to be offered for sale in terms of all other
goods, each item would have a horrendously complicated price
list. The price of a plane ticket from Sydney to San Francisco, for
example, would need to be expressed in all other commodities.
This might mean that the return air fare could be obtained for 700
pairs of socks, 100 shirts, two-tenths of an average small family
sedan, 1500 kilograms of grapes, or 200 bottles of fine champagne.
Such a list would make life difficult.

What has developed historically is that one particular eco-

1, Vickers, Economics and Man, p. 241.
161

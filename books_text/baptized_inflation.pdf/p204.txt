Mumble, Mumble 183

inflation-hedging investment activity in, say, gold, silver, and art
objects were to call attention to the economic results of the
Keynesian inflation game? What if workers then took note of this
and began demanding higher wages, thereby reducing the em-
ployment “kick” which the Keynesian planners hoped to achieve
for the overall economy? What, in short, if the victims should
finally catch on to the essence of the Keynesian revolution, which
is based on an illusion: higher employment as a result of lower real
wages? That would be a dark day for the Keynesian planners.
And so it was, in 1979 and 1980. So it will be again, when the des-
perate Keynesian economists recommend mass inflation to bail
out the banks. (Better put, when the desperate bankers call in the
Keynesian economists to justify the bankers’ call for the central
banks to inflate the various Western currencies, especially the
US. dollar.)

The essence of the discipline of economics is to study cause and
effect of different economic occurrences. Consider an increase in
the money supply. What can cause it, and what are its likely
effects? The answer to the first part of that question depends on
what money is. In our era, money is regarded by most people as
those pieces of paper with pictures of various politicians on them,
and token coinage made up of metals such as zinc and copper. To
determine the cause of the supply of these units we only need ask
who controls their issuance in the first place. The answer to that is
the State. Any increase in the money supply is thus a deliberate act
on the part of those in civil government.

Banking

Unfortunately, it is not that simple. Men have devised a sys-
tem whereby the money supply can be increased without physically
increasing the quantity of pieces of paper and coinage which
already exists. The method of doing this is called fractional reserve
banking. There is nothing especially magical about this, for all it
involves is certain bookkeeping procedures. We are all familiar
with banks, and what they do. They take depositors’ money,

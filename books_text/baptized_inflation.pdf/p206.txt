Mumble, Mumbie 185

Fiat Money

When a depositor places money in the bank, the bank issues a
receipt of some kind. In essence this is now a /iability to the bank.
But because the banker has worked out how to utilize this money
for his own profit, he now perceives this deposit not as a liability
but as an asset. Perhaps $100 has been deposited. He knows that at
any one time only twenty percent of depositors want their money
back, so he puts aside this percentage ($20) in his vault (non-
interest-paying), and then gleefully searches for a borrower who
needs $80. (Actually, the central bank collects most of this “vault
money.”)

He could give him the $80 cash straight out, but any time an
inventory take is done, his scheme would be exposed. Why not
use the bank’s good standing in society and issue another piece of
paper which says the borrower has access to $80? He can simply
use that piece of paper as money, the receiver remaining confident
that the bank will honor the obligation involved in that document.
This way, the banker gets to hang onto all the money. His inven-
tory remains full, balancing his liabilities.

Cause

If this was all he did, things would not be so bad. But there is a
second banker in town. When the first bank’s client (borrower)
deposits the borrowed funds in his own bank account, he can then
start writing checks. Someone else gets this $80, and the new pos-
sessor of the $80 deposits it. The second banker (or maybe it is the
first one), rather than seeing this as a return of the loaned out cap-
ital, instead views it as a “new” deposit. He proceeds to retain his
familiar twenty percent security, and determines to lend out the
remaining $64. So the merry-go-round continues. The loaned-out
money, each time it is returned to the bank by some recipient
seller of goods or services, is dealt with by the banker as a “new”
deposit. In reality, of course, it is simply a return of a portion of
that original $100.

Watch the money multiply: from $100 to $500. Here is the de-

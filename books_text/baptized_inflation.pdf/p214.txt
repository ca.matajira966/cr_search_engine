Mumble, Mumble 193

Maybe this appears a little harsh to some. Well, before making
final judgment, let us take a tour of Dr. Vickers’ understanding
and theories on inflation. A surprise may be in store.

Douglas Vickers, Inflation Fighter?

A superficial reading of Dr. Vickers’ Economics and Man could
lead to the conclusion that there is agreement with the North-
Rushdoony thesis that an increase in the money supply — that is,
monetary inflation — causes a devaluation in the purchasing power
of money which, in Biblical terms, is theft and fraud. “[ We should
avoid the theft and immorality of inflation,” says Dr. Vickers. '*

Inflation, it ought to be said, is immoral, and the immorality is
chargeable to those whose economic actions give rise to it, or to those
who, being responsible for the right administration of the economic
affairs of the nation, either adopt policies which exacerbate the inflation
rate or fail to take action more reasonably designed to correct it.

Acloser examination of the sentence quoted will reveal, how-
ever, that Dr. Vickers has a vastly different idea of the meaning of
inflation from that of North and Rushdoony. (We shall see that he
has a different definition altogether than those mentioned earlier
in this chapter. ) Notice that he says the failure to take economic ac-
tion can cause inflation. Now, failure to act cannot cause inflation
in the North-Rushdoony sense (except the government's failure to
enforce the law of honest weights and measures), since they define
inflation as any increase in the money supply. ‘ How could inac-
tion cause an increase in the money supply? Obviously, Dr. Vick-
ers employs this word in a different sense from those he opposes.
What he refuses to accept is the definition of inflation as an in-
crease in the money supply. No Keynesian will. If he did, he
would have to abandon his entire methodology. Thus, all Keynes-
ians cling religiously to any definition of inflation except the

11, Vickers, Economics and Man, p. 36.
12, Ibid., p. 98; cf. pp. 175, 242, 248, 258-254,
18. North, An Introduction to Christian Economics, p. 20

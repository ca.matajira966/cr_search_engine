202 Baptized Inflation

with heresy. They point to the “outsider” status of some the inno-
vators. But if the skepticism of younger men grows, the guild is
ripe for a revolution.

Then, seemingly overnight, someone puts forth a new expla-
nation of the anomalies. He reconstructs the guild’s paradigms. A
successful theory will retain as much as possible of the received.
wisdom, but the essence of the revolution is the new paradigm it-
self. The transformation takes about a generation. Then the guild
settles down to do “normal science” —the drudgery, puttering, and
“clearing up the doubts about the new paradigm” which character-
izes most scientific activity most of the time. *

Established economists are no different from other guild mas-
ters. They too have their favorite theories which they do not like
to have challenged, and will therefore avoid any discussion on
that particular area which might disprove their theory. Socialists,
for example, have yet to answer Mises’ criticism that rational eco-
nomic calculation is impossible in a pure socialist State. Without
private ownership, there is no entrepreneurship; without a com-
petitive private market for capital goods, there is no way for cen-
tral planners to impute accurate prices to capital goods. Calcula-
tions made by socialist planners are rational only because they
borrow from societies where prices arise in the market place.
Once they have obtained prices from outside the socialist system,
the y are able to make their calculations, but without such para-
sitic activity, rational calculations are impossible.3

The crisis in economics which has been underway since about
1970 is closely tied to this loss of faith in the prevailing Keynesian
methodologies. I have already cited Milton Friedman’s 1971

2, Thomas Kubn, The Structure of Scientific Revolutions (2nd ed.; Chicago: Uni-
versity of Chicago Press, 1970).

8, Ludwig von Mises, Socialism (Indianapolis, Indiana: Liberty Press, [1922]
1981), pp. 97-105 (pp. 119-122 in the Yale University Press edition of 1951), There
was an attempt by Oscar Lange in the late 1980's to answer Mises, but it did not
succeed, and his proposed solution was so impractical from the standpoint of so-
cialism that it has never been adopted by any socialist planning agency. Textbook
accounts, however, almost always claim that Lange answered M ises, in those
rare instances where the texthooks even mention Mises,

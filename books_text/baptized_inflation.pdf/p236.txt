26 Baptized Inflation

can only be two circumstances which produce this. Either produc-
ers cannot lower their prices to meet lower wage rates (demand)
or else they cannot raise their prices to cover the labor factor in
production costs to maintain production (supply). What would
prevent them from doing either of these? Legislation introduced by
politicians influenced by the Keynesian economists in their em-
ploy, whose shallow reasoning demands that wages be raised.
while prices are kept low, in order to “stimulate aggregate de-
mand.” This is the stupidity of Keynesianism which apparently
escapes Dr. Vickers. But Dr. Vickers has not escaped this stupid-
ity. As Van Til would say, it is not a question of intelligence; it is a
question of presuppositions. It is more a moral flaw than an intellec-
tual flaw. He who despises the law of God will soon find himself
doing stupid things and saying stupid things.

Conclusion

Dr. Vickers, following Keynes, does not tell us what we need
to know about the price system. He therefore does not tell us what
we need to know about the problem of unemployed resources,
especially unemployed people. He does not recognize how compe-
tition among all those “greedy” businessmen leads to wage rates
for laborers that tend toward equality with the value of labor’s
output. Employers do not want to pay high wages, but they want
even less to lose the services of laborers who make the employers’
business profitable. This is why high wages, high profits, and low
prices go together in a free market economy. This is why Keynes
never understood either economic reasoning or the free market
economy. This is why young, highly skilled, mathematically
adept ‘rational expectations” economists can barely disguise their
contempt for the gray-haired “revolutionaries” of the 1930’s and
1940’s as they dodder toward retirement and the grave.? The gray
heads are now reaping what they sowed: a revolution by the peo-
ple they once taught.

We have seen that Henry Hazlitt’s accusation is correct: ‘In

9, Susan Lee, “The un-managed economy,” Forbes (Dec. 17, 1984).

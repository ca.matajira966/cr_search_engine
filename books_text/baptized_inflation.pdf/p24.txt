XxViL Baptized Inflation

made me a follower of the free market system. It was his attempt
to portray Keynesian nonsense as serious economic thinking
which caused me to accept the ideas of North and Rushdoony as
having much more logic and coherence than could be found in Dr.
Vickers’ book. (Thanks, Doug!)

During 1980 I made the mistake of attempting to introduce
discussion on Christian economics into a Reformed theological
college I was attending. It was a mistake, as I have subsequently
realized, because there was no interest among staff or students to
know whether there might be a specifically biblical theory of eco-
nomics. It was as if the intellectual effort needed to make such an
analysis of Scripture was too great. (I have witnessed a similar in-
tellectual laziness among many pastors, elders, and deacons. )
Besides, I was informed, Dr. Vickers had the answers to anything
that Gary North, R. J. Rushdoony, and others might happen to
say. I should read Vickers’ book, I was told both by staff and stu-
de"nts, especially in the case of one who had been a former eco-
nomics teacher, and get “straightened out” on my economics.

Fortunately, I had already made that effort and ploughed my
way through Dr. Vickers’ book. (Ploughing is especially necessary
when the soil is hard, and believe me, the soil in this case was very
hard. ) One of the brightest students in that college during that
period actually made an effort to read Dr. Vickers’ book, but gave
up in defeat after the first chapter. He admitted he did not under-
stand it, and nothing I could say at that time convinced him of the
possibility that he could not understand it because it did not make
sense. He preferred to believe that there was something lacking in
his own intelligence.

The idea and outline for this book began during that period I
spent in what is known as a Protestant monastery: Theological
College, a recluse where staff and students escape from the real
world and the realities of life. This manuscript is an attempt to
put Dr. Vickers’ book into perspective and highlight some of the
inherent difficulties Keynesian economic theory presents in con-
trast with the Word of God. Most of what appears in this book
comes from marginal notes I made on my first reading of Dr.

The Marginal Propensity to Confuse 231

Keynes on His Own Perverse Terms

Now, for the sake of argument, let us not begin with the Bible’s
answer to Keynes, or even common sense’s answer to Keynes. Let
us just examine the logic of Keynes’ own arguments. What can we
say about his logic?

First, we must note carefully that this illustration rests on the
hypothetical assumption that people will spend eighty percent of
their increased income. No factual statistic is offered, perhaps for
the following reason. Let us accept the proposition at its face
value, that there is a multiplier which can be determined to have
an effect. Now let us ask: How is that multiplier determined? We
can agree that people will spend only a portion of any income in-
crease, but how do we determine with some degree of accuracy just what
that propensity ig? The only possible way is to get statistics from peo-
ple when they receive that income. We must follow them, moment
by moment, day by day, week by week, month by month, year by
year, to see how they might use it.

Oh, no, says the liberal-minded “I am not a socialist” Keynes-
ian, we would not want to do that. That would be like having Big
Brother continually peering over your shoulder. True, but how
else is this “magic” multiplier to be determined if we do not
attempt to obtain an accurate account of how people are spending
income? Probably the same way Dr. Vickers arrived at his sup-
position of eighty percent: pull the figure out of thin air. Think of a
number; that will do just fine. If you prefer, ask the office secre-
tary for a number which will become the multiplier. And if this
slight parody of the Keynesian system offends, I would ask: In
what manner does a number arbitrarily arrived at by the office
secretary differ from that invented by the Keynesian economist?

The economist, you say, is a little more scientific. He assumes
that if he can take a few representative samples, this will be
enough to make judgments concerning the whole economy. How
does he know these few representative examples have any relation-
ship to what is happening in the economy as a whole? He does not
know, of course. He merely makes the assumption that it does.

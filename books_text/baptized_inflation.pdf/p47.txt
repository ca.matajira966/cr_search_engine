Keynes, Vickers, and Van Til at

to say, in the strict sense of the term, immoralists. The consequences of
being found out had, of course, to be considered for what they were
worth, But we recognized no moral obligation on us, no inner sanction,
to conform or obey.

The admission that they were “immoralists” is evidence that
Keynes knew that he and his colleagues were breaking with con-
ventional morality. This conventional morality was essentially
Christian morality which had influenced the British Isles for
almost 2,000 years. In addition, however, men do not sin in ig-
norance. They have before them, as Remans 1:18-23 reminds us,
the evidence that God exists. Men are created in the image of God
and therefore know with certainty what they are doing. No man
disobeys God in ignorance; it is always willful and deliberate re-
bellion against a sovereign God.

Deliberate Obscurity

Having established some background to the character of
Keynes, it should not surprise us that his economic writings are
not at all what some would have us believe. Keynes’ major contri-
bution to economic thought, his presentation of his “new” eco-
nomics in The General Theory, is one of the most difficult books to
read. It frequently borders on the incoherent. Its style is abomina-
ble, in sharp contrast — indeed, suspiciously sharp contrast — to
his lucid Essays in Biography and his first best-seller, The Economic
Consequences of the Peace. This assessment of the style of the General
Theory is shared not just by his critics. Listen to these comments
about The General Theory publication by well-known Keynes disci-
ple and Nobel Prize-winning economist Paul Samuelson:

It is a badly written book, poorly organized; any layman who, be-
guiled by the author's previous reputation, bought the book was
cheated of his five shillings... . It abounds in mares’ nests of confusions,
... In it the Keynesian system stands out indistinctly, as if the author
were hardly aware of its existence or cognizant of its properties. . . .

18, Collected Writings, X, 446, quoted in ibid., pp. 142-148,

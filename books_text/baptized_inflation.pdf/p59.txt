Neither Capitalism Nor Socialism (Maybe) 33

else that exists apart from God was created by Him. Truth there-
fore originates in God and not created reality. Third, the Creator
has communicated Truth to His creation. God has spoken. A
Christian theory of knowledge rests upon the doctrines of God,
creation, and revelation, ’

Denying these three things, modern man finds himself incapa-
ble of determining what is true or false, right or wrong, just or un-
just. Nevertheless, his language is consistently couched in these
terms. We cannot have capitalism, says the socialist, for it is so-
cially unjust and inequitable. The same moral appeal is sometimes
implicitly used by the capitalist when he argues against socialism,
though more commonly, the economist defends capitalism by
means of an appeal to economic efficiency (reduced waste) and the
legitimate autonomy of the individual decision-maker. Over six
decades ago, the German sociologist and historian Max Weber ar-
gued that this dualism between humanist social ethics and market
efficiency is inescapable.8 He has yet to be proven wrong. Hardly
any economist has even attempted to prove him wrong. Econo-
mists just ignore him.

Dr. Vickers’ “Synthesis”

In the realm of economics, the ideas of socialism and capital-
ism were once presented as the only alternatives available (thesis
and synthesis). This certainly was Karl Marx’s vision. He be-
lieved that capitalism contained the seeds of its own destruction,
and the crisis in capitalism would inevitably lead to the proletar-
ian revolution and the establishment of socialism. John Maynard.
Keynes challenged this view by arguing that enlightened State
planning can overcome the contradictions of an economic order

7, Comelius Van T'l, A Christian Theor of Knowledge (Phillipsburg, New Jer-
sey: Preshyterian & Reformed, 1969),

8, Max Webei, Economy and Society (New York: Bedminster Press, [1924] 1968),
pp. 107-18, 588-89, 685-40, Cf, Gary North, “Max Weber: Rationalism, Irra-
tionalism, and the Bureaucratic Cage,“ in North (ed.), Foundations of Christian
Scholarship: Essays in the Van Til Perspective (Vallecito, California: Ress House,
1976),

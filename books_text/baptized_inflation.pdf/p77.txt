God’s Creation and Capitalism 53

ades, !° which Dr. North has called the mythology of Darwinian
central planning. '!

Former U.S. Secretary of Defense Robert McNamara has ex-
pressed this viewpoint forthrightly: “Some critics teday worry that
our democratic, free societies are becoming overmanaged. I
would argue that the opposite is true. As paradoxical as it may
sound, the real threat to democracy comes not from overmanage-
ment, but from undermanagement. To undermanage reality is
not to keep it free. It is simply to let some force other than reason
shape reality. That force may be unbridled emotion; it may be
greed; it may be aggressiveness; it may be hatred; it may be ig-
norance; it may be inertia; it may be anything other than reason.
But whatever it is, if it is not reason that rules man, then man falls
short of his potential. Vital decision-making, particularly in pol-
icy matters, must remain at the top.”"2 P.tes the most eloquent
critique of the results of McNamara’s dedicated top-down ration-
alism is chapter 12 of David Halberstam’s masterpiece on the
Johnson Administration’s handling of the Vietnam War, The Best
and the Brightest (1972),

Also notice McNamara’s concern with “greed.” We shall be
confronted with this theme again, when we examine Dr. Vickers’
defense of Keynesian interventionism.

Interest Rates and Market Decisions

Dr. Vickers is concerned about the “anarchy” of the free mar-
ket. He has not taken seriously the idea that there is another kind
of rationalism, the rationalism of voluntary cooperation in a pri-
vate property social order. He refuses to acknowledge the free mar-
ket’s integration of decentralized individual economic plans as the primary

10. Especially in his book, The Counter-Revolution of Science (indianapolis,
Indiana: Liberty Press, [1952] 1979).

11. Gary North, The Dominion Covenant: Genesis (Tyler, Texas: Institute for
Christian Economics, 1982), Appendix A, “From Cosmic Purposelesmness to
Humanistic Sovereignty.”

12, Robert McNamara, The Essence of Security: Reflections in Office (New York:
Harper & Row, 1968), pp. 109-10.

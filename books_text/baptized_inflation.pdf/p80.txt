36 Baptized Inflation

of interest on investments of varying time and risk, and they
decide either to invest or spend on consumer goods and services.
Either they choose future goods or present goods. There has to be
some indicator which tells rational, purposeful people whether it
is to their advantage to invest or spend. Dr. North says (following
the “Austrian School” economists) that market interest rates guide
people in their time-oriented economic decisions, and that these
various rates are the product of men’s shifting time-preferences in
the market place. While each decision-maker faces objective market
rates of interest, some personal, subjective rate of interest discounts
the future value of assets in his decision-making, with or without a
published rate of interest in some newspapet.

If the prevailing available return on his money is higher than
his subjective rate of time-preference, he will invest the money,
thereby foregoing the benefits of immediate consumption. The per-
son with lower time-preference (a comparatively high valuation of
the future) lends money to people with higher time preference (a
comparatively low valuation of the future). Each person buys what
he prefers. One person buys the present use of assets and gives up
even more future assets, while the other sells present assets in order
to receive even more future assets. These people get together as a
result of their own profit-seeking search, and the “searchlight” they
use to locate each other is the market rate of interest.

If this sounds simple enough, it is because it is simple. If Dr.
Vickers’ discussion sounds complex, it is because it is confused.
This confusion was also basic to Keynes’ discussions of the inter-
est rate.

The Keynesian View(j) of Interest

To understand Dr. Vickers’ deliberate misrepresentation of
Dr. North’s position, it is only necessary to refer to Dr. Vickers’
quotation of Dr. North. “The rai of interest is supposed to act as an
equilibrating device... .”17 Now, when Dr. Vickers concludes that

17. North, Introduction to Christian Economics, p. 63, quoted im Economics and
Man, p. 27, emphasis in original,

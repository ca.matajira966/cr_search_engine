68 Baptized Inflation

thee the worth of it in money” (v. 2). It is of interest to note here
that there is no attempt by Ahab to get out of paying @ reasonable price for
the land. His financial offer was more than fair, from a pure market
perspective. Ahab simply wanted the land because it was near to
his palace. Possibly he was lazy and did not want to travel so far.
More to the point, his servants probably exhibited the signs of all
bureaucrats and a keen eye needed to be kept on them in order
that a fair day's work was put in tending the herbs. But it is
Naboth’s answer which attracts our attention. “The Loxp forbid it
me, that I should give the inheritance of my fathers unto thee”
(v. 8). In other words, Naboth’s unwillingness to sell his land was
based on the fact that the Lord had forbidden it. His land was an
inheritance to him and his descendants and Naboth could not
disinherit them.

Some commentators might raise the question that this land, in
Canaan, was the product of a special act of God in giving it to His
chosen people, and that is true. But it is also true that the land
obtainable today and all the other possessions we have are also the
gift of God to us and to our descendants. For all possessions,
including the Promised Land, are given on condition of covenantal
obedience to God. There is no such thing as unconditional inheritance in
Scripture, not now nor in God’s past dealings with Israel. Neither is
there any Biblical warrant for some other person — even the king
— to assume the prerogatives of deciding when and how the land is
to be distributed and used. In other words, Ahab had no righiful
power or authority over Naboth’s property unless Naboth volun-
tarily chose to give him such, which he did not.

The Parable of the Employer

The second Biblical illustration of the meaning of ownership is
to be found in Matthew 20:1-16. Although it is true that the point
of this parable our Lord is telling is not the meaning of ownership,
what Jesus teaches at this point is dependent upon the view of
ownership that we have defined here. Jesus is using a parable to
teach His followers about the absolute sovereignty of God in distribut-
ing rewards for their earthly service. Sinful human beings cannot

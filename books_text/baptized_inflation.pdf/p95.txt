Man's Rebellion and Socialism 7

brought forth strong denunciations of the wealth of that church combined
with exhortations that the clergy lead a more modest life. It seemed, by
threatening an institution that owned great material wealth and by criti-
cizing the use of that wealth, to question the institution of property. But
it did not do so. Throughout the Reformation, private property contin-
ued to be considered an ethical value. In fact, the bases for its protection
were expanded. Catholic thinkers had contented themselves with claim-
ing that property was protected mainly by natural law. Protestant theo-
logians also emphasized that property was sanctioned by the Scriptures.
Not merely a broader justification was rendered, but also, the function
of property to promote progress was stressed to a greater extent. With
this emphasis, the Reformation stimulated the industry and energy of
men, and had no small influence upon the rise of such powers as the
Netherlands, England, Sweden, the United States and Germany. *

There is no reason to accept Dr. Vickers’ assertion about the
Reformation’s supporting his view of property ownership. To be
true, there was a movement which called for the commonality of
goods, but the mainline Reformers saw the falsity of the idea and.
argued against it on Biblical grounds. Luther, for example, sup-
ported the idea of private ownership as against some form of public
ownership, which in reality is what Dr. Vickers is advocating. If
property rights only inhere “in the general case,” then what happens
to the exceptions? More importantly, whe is to decide which are
the general cases and which are the exceptions? Dietze continues:

Luther’s support of private property was matched by John Calvin.
... He realized that common ownership is utopian and denounced the
Anabaptists’ plan to abolish property and inequality. God the supreme
legislator, by decreeing ‘Thou shalt not steal,’ ordained the protection of
property. What each individual possesses has not fallen to him by
chance, but by the distribution of the Sovereign Lord of all... . The
state should see to it that every person may enjoy his property without
molestation. The prince who squanders the property of his subjects is a
tyrant,

15, Gottfried Dietze, in Defense of Property (Baltimore, Maryland: The John
Hopkins Press, 1971), p. 17.
16. Ibid, p. 18.

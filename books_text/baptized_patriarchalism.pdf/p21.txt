Baptized Patriarchalism 13

Familism and Racism

Basic to pagan familism is a theology of racism, Because the
pagan family is seen by its defenders as a blood covenant, the
question of inter-racial marriage becomes decisive.

Rushdoony in 1965 wrote an essay on the doctrine of mar-
riage. In it, he argued that race and culture are overriding
considerations in marriage. His language indicates that these
considerations are equal to profession of faith. Speaking of the
wife, he wrote:

Moreover, if she is to be ‘a help meet as before him,’ a mir-
ror, there must be a common cultural background. This militates
against marriages across cultures and across races where there is
no common culture or association possible.

The new unit is a continuation of the old unit but an inde-
pendent one; and there has to be a unity or clsc it is not a mar-
riage. Thus, the attempt of many today to say there is nothing in
the Bible against mixed marriages whether religiously or cultur-
ally is altogether unfounded.®

The theological error here is monumental. Paul was emphat-
ic: “For there is no difference between the Jew and the Greek:
for the same Lord over all is rich unto all that call upon him”
{Rom. 10:12). “There is neither Jew nor Greck, there is neither
bond nor free, there is neither male nor female: for ye are all
one in Christ Jesus” (Gal. 3:28). “Where there is neither Greek
nor Jew, circumcision nor uncircumcision, Barbarian, Scythian,
bond nor free: but Christ is all, and in all” (Col. 3:11). Paul
rejected the Jews’ belief in the racial-cultural separation of their
nation. This separation had always been confessional, not racial.

Rushdoony’s exposition ignores these verses. Given his view
of the family as a blood covenant established in terms of a

8. Rushdoony, “Ihe Doctrine of Marriage” (1965); Toward a Christian Marriage,
edited by Elizabeth Fellersen (Nutley, New Jersey: Presbyterian and Reformed,
1972}, pp. 15-16.

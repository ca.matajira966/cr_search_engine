Baptized Patriarchalism 15

cultural marriages, in that they normally go against the very
community which marriage is designed to establish.”"* Note
his asserted equivalents: inter-religious marriages and inter-
racial or inter-cultural marriages. He is not speaking here
merely of civil law; he is speaking of biblical law in general.
The shift in his argument is both subtle and significant. He
is not arguing that inter-racial marriages do not produce child-
ren, Such unions are not biologically sterile. Then are inter-
cultural marriages genetically sterile? He does not argue that
they are. So, what has “hybridization” got to do with either type
of marriage? Genetically speaking, not a thing. Rushdoony has
shifted his argument from genetics to race and culture. He has moved
trom an annulled Mosaic case law regarding cattle, planting,
and clothing to a racial-cultural application. He has invented a
legal category of “hybridization” in order to apply it to inter-
racial and inter-cultural marriages. What he is saying is that
such marriages are covenantally sterile. The problem is, this is a
denial of the New Testament’s doctrine of the gospel’s power to
break down the wall separating Jew from Greek, bond from
free. His theology of sterility has mixed a false interpretation of
a case law with traditional racism’s theory of “inferior races.”
The standard of unequal covenantal yoking unquestionably
applies to marriage. Rushdoony is correct on this point: Paul
makes this clear in II Corinthians 6:14. This Pauline prohibi-
tion is universally believed by orthodox Bible commentators to
apply to marriage covenant. But covenantal yoking has nothing
to do with race. Covenantal yoking is just that: covenantal.
The judicial standard involved in the biblical concept of
“yoking” is exclusively covenantal: public confession of Trinitar-
ian faith, local church membership, the regular celebration of
the Lord’s Supper, and public obedience to God’s law. For a
Christian to deny salvation through faith in Jesus Christ is
apostasy. To refuse to join the local church is an assertion of

13. Tbid., pp. 256-57.

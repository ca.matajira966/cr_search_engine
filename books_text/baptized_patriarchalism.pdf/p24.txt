16 BAPTIZED PATRIARCHALISM

one’s judicial autonomy. To refuse to celebrate the Lord’s Sup-
per is sel{-excommunication. To deny the law of God is antino-
mian. A Christian should not marry anyone who is remiss in
any of these four areas. To be remiss in any of them is to break
covenant with God. But marrying a Christian from another
race or another culture is not covenant-breaking.

This principle of covenantal discrimination applies to cach of
the three institutional covenants: church, state, and family.
“Equal yoking” means a public commitment of all covenantal
participants to the Athanasian creed or some other Trinitarian
creedal statement, as well as church membership. The judicial
issue is faithfulness to the covenantal sath. “Equal yoking” is strictly
a judicial concept. “Unequal yoking” is therefore also strictly a
judicial concept; as such, it has nothing to do with race or
culture: in family, church, or state.!4 It has nothing to do with
community standards except to the extent that these derivative
standards are confessionally Trinitarian - a product of the
covenant. Community standards must conform to God’s law.

What criteria determine which group is excluded from what
covenantal organization? Rushdoony has made his view plain:
community standards. Once again: “The burden of the law is thus
against inter-religious, inter-racial, and inter-cultural marriages,
in that they normally go against the very community which
marriage is designed to establish.” But there are also church
communities and political communities. Are they autonomous

14, Obviously, if two people cannot speak the same language, they may have
fulure marital problems. This is not a valid covenantal objection to their marriage.
The presumption is, one or both will learn the other’s language. This is also true of
churches. Members of churches cannot lawfully be exchided from the Lord’s Supper
because of a language barrier. Should a person be excluded from citizenship because
ofa language barrier? No. But he will have trouble being elected to public office. He
can be barred from voting on the basis of funclional illiteracy in the language on the
ballot, but states that require secret ballots — only one person per booth at a time —
can and should provide translations on the ballot for major linguistic groups. Que-
bee's linguistic discrimination against its English-speaking citizens is notorious.
15. Rushdoony, Institutes, p. 257.

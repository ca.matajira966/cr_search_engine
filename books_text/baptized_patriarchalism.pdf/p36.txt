28 BAPTIZED PATRIARCHALISM

specify what citizens are supposed to do. The judicial issue is
this: What is the institutional church authorized by Ged te do as His
designated monopoly? The issue is not what Christians should do.

It is therefore misleading — ] would call it subversive - for a
theologian of Rushdoony’s stature to criticize the traditional
Protestant definition of the institutional church on this basis:
that it does not include works or faith. Faith is displayed public-
ly by confession and ethics. Any attempt to add something
emotional or experiential to this definition is a move toward
mysticism. At the same time, to require a list of responsibilities
that define the church is a denial of Protestantism’s doctrine of
salvation by grace through faith. In the name of anti-reduction-
ism, Rushdoony by 1991 had abandoned Reformed theology.

Having misled his readers on this point, Rushdoony in this
same lecture on “Reconstructing the Church” went on to mis-
lead them even more. He said that the church must perform
the Great Commission: establish the crown rights of King Jesus,
baptize nations, and teach them to obey God’s word. Notice: not
one reference to the sacrament of the Lerd’s Supper. While Matthew
28:18-20 mentions only baptism, the establishment of the
church requires the Lord’s Supper. Any theologically accurate
discussion of the Great Commission must assume the accuracy
of the three defining judicial marks of the institutional church.
But if you have just ridiculed the institutional church as a
mummy factory, your reader may not notice what you are
really doing: removing respect for the judicial authority of the institu-
tional church as the sole legitimate source of the sacraments. This
error had begun in earnest in Systematic Theology.

The Sacraments

In Systematic Theology, he says that baptism is a family act:
“Fourth, baptism is a family act, even as circumcision was a

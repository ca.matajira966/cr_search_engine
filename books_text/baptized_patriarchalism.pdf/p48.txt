40 BAPTIZED PATRIARCHALISM

Central to Rushdoony’s ecclesiology is the question of the
tithe. He argues that the Levites performed social services.
(True.) This was their claim to the tithe. (False; guarding the
temple was: Numbers 18:22-24.) So, those Christians who also
provide social services today have a lawful claim on men’s
tithes. (False.) Ile argues that the Levites performed many
social services, “providing godly education, music, welfare, and
necessary godly assistance to civil authorities.” (True.) Thus,
he concludes, it was their provision of these social services that
justified their collection of the tithe. (False.) The Levites did not
possess a legal claim on the tithe. (False.) If they failed to pro-
vide these cultural services, Israelite church members had an
obligation to cut them off financially. (Falsc.) We still have this
obligation. (False.) “Since the tithe is ‘holy unto the Lord’, it is
our duty as tithers to judge that church, mission group, or
Christian agency which is most clearly ‘holy unto the Lord’.”””
As a Christian, I judge which church I should belong to; once
I jom and am under its authority, I owe it my tithe. I no longer
possess authority over this money. It belongs to the church.

He writes: “This tithe belongs to God, not to the church, nor
to the producer.” This observation is irrelevant for any dis-
cussion about allocating the tithe. Of course the tithe belongs to
God; everything belongs to God (Ps. 50:10). The question is
this: What institution possesses the God-given monopolistic au-
thority to collect the tithe from covenant-keepers? That is,
which institution possesses the God-given authority and responsi-
bility to pronounce God’s negative sanctions against someone
who refuses to pay? The biblical answer is obvious: the church.
Rushdoony disagrees with this answer. He wants to remove
from the institutional church any legal claim to the tithe.

76. Rushdoony, “The Foundation of Christian Reconstruction,” in Rushdoony
and Edward Powell, Tithing and Dominion (Vallecito, Catifornia: Ross House, 1979},

p.9
77. Rushdoony, “To Whom Do We Tithe?” ibid., p. 30.

78. Rushdoony, “Tithing and Christian Reconstruction,” ibid,, p. 3.

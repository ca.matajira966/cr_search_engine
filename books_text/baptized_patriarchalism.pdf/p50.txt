42 BAPTIZED PATRIARCHALISM

For whosoever eateth the fat of the beast, of which men offer
an offering made by fire unto the Lorp, even the soul that eateth
it shall be cut off from his people (Lev. 7:25).

And if a man shall take his sister, his father’s daughter, or his
mother’s daughter, and see her nakedness, and she see his na-
kedness; it is'a wicked thing; and they shall be cut off in the
sight of their people: he hath uncovered his sister’s nakedness;
he shall bear his iniquity (Lev. 20:17).

And ifa man shall lie with a woman having her sickness, and
shall uncover her nakedness; he hath discovered her fountain,
and she hath uncovered the fountain of her blood: and both of
them shall be cut off from among their people (Lev. 20:18).*

But the man that is clean, and is not in a journey, and for-
beareth to keep the passover, even the same soul shall be cut off
from among his people: because he brought not the offering of
the Lorp in his appointed season, that man shall bear his sin
(Num. 9:13).

Rushdoony's thesis regarding the non-authority of the
priests is incorrect. But why did he make this particular error?
We are back to the third mark of the church, discipline. Rush-
doony’s ecclesiology rests on a near-silence regarding excom-
munication, This silence is intentional,

He immediately moves from the supposed lack of a unique,
God-given judicial authority of the special priesthood to the
topic of the priesthood of all believers." This evasive action is
necessary because he has just denied the unique authority
possessed by men ordained as special priests, as distinguished
from general, born-again priests. If authority is common to
every realm, then the special priest is not uniquely authorized

80. Rushdoony's explanation of this verse is especially interesting: /nsithutes, pp.
427-30. He connects menstruation, ovulation, and living water.
BL. Systematic Theology, p. 725.

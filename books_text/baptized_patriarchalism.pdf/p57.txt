Baptized Patriarchalism 49

Recall what he wrote in preparation for this announcement.
He asserted the authority of the marital family over the two
covenant oath signs: baptism and communion. He transferred
the locus of authority over the covenant signs from the institu-
tional church to the marital family, in which husband and wife
seck to produce a blood line (procreation). He self-consciously
and explicitly challenged the church’s entire history regarding
the sacraments. He did not cite a single creed, confession, or
theologian to defend his position. He forthrightly announced
the centrality of the marital family as the covenantal institution
on which the other two rest. Now he invokes the language of a
blood covenant. But the judicial context of the marital family is
sexual bonding! Therein lies the enormous theological danger.

This is not a product of theological confusion on his part.
He has been thinking about this for years. He is not some back-
woods preacher who has never read a treatise on theology or a
history of ancient religion. He has self-consciously wansferred
the covenantal authority based on the blood of Christ from the
institutional church to the original Adamic bloodline: the mari-
tal family. Iie does define the Christian church in terms of the
blood of Christ, but then he identifies the administrative agent
of the church’s covenantal signs: the marital family.

Amazingly, he then invokes Calvin’s authority: “Calvin, in
discussing ‘the Lord’s Supper and Its Advantages,’ declared it
to be a family rite: . . .”*! He quotes Calvin’s Institutes, Book
IV, Chapter vxii, Section 1: “After God once received us into
his family . . . he also undertakes to sustain and nourish us as
long as we live. .. .” Rushdoony is clever. He is also wrong.

Rushdoony is being tricky with words again, but this time in
English. The word family is the same, but the judicial content of
the word is different in Calvin's system. Rushdoony is speaking
of a family which is defined by a marriage oath. Calvin is also
speaking of a family, but one defined by a confessional oath.

91. Ibid.

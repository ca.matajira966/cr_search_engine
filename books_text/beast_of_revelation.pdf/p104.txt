70 The Beast of Revelation

Rev. 17: 10b-1 1). Thus, the Beast is generically portrayed as a
kingdom. But in the very same contexts the Beast is spoken of as
an individual (he is a man with a specific name, Rev. 13:18) and
as but one head among the seven (Rev. 17:11). This unusual
feature, as noted before, is recognized by a number of commenta-
tors.

As I begin consideration of the matter it must be recognized
that it is one of the heads which received a death blow “And I saw
one of his heads as if it had been slain, and his fatal wound was
healed” (Rev. 13: 3). I demonstrated earlier that Nero Caesar is
the “head” which is in view here.5 John prophesies that Nero will
die by the sword (Rev. 13:10, 14). Nero is the one mysteriously
numbered “666” (Rev. 13: 18).

Recognizing these factors takes us a long way toward resolving
the interpretive issue before us. The mortal sword wound to one of
the heads is a wound that should have been fatal to the Beast,
generically considered. This explains why it is that after the wound
was healed and ite Beast continued alive, “the whole earth was
amazed and follo wed after the beast” (Rev. 13:3 b). The seven-
headed Beast seems indestructible, for the cry goes up: “Who is
like the beast, arid who is able to wage war with him?” (Rev.
13:4b).

Now how dots all of this imagery have anything to do with
Rome and Nero Caesar?

The Historical Fulfillment

At this point ve need to reflect upon a most significant series
of historical events of the A.D. 60s. A perfectly reasonable and
historical explanation of the revived Beast lies before the inter-
preter. Here is w tere 0 many faddish interpretations of Revela-
tion go wrong. They forget the original audience relevance factor and,
consequently, overlook the history of the era.

5, See pages 11-12 supra,
6, See Chapter 3, ‘More discussion of this maybe found in Chapter 10 below.

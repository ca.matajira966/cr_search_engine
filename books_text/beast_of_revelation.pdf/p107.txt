The Revival of the Beast 73

These civil wars would, to all appearance, strike the citizens,
subjects, neighbors, and enemies of the vast empire — Christian
and pagan alike — as being the very death throes of Rome, the Beast
generically considered. Indeed, in Tacitus’s estimation it very
nearly was so: “This was the condition of the Roman state when
Serius Galba, chosen consul for the second time, and his colleague
Titus Vinius entered upon the year that was to be for Galba his
last and for the statealmost the end.”'*

Before the world’s startled eyes, the seven headed Beast
(Rome)'> was toppling to its death as its sixth head (Nero)'° was
mortally wounded with the sword. As Suetonius viewed the long
months immediately following Nero’s death, the empire “for a long
time had been unsettled, and as it were, drifting, through the
usurpation and violent death of three emperars.” ”

Josephus records the matter as perceived by the Roman gener-
als Vespasian and Titus, while they were engaged in the Jewish
War in A.D. 69: “And now they were both in suspense about the
public affairs, the Roman empire being then in a fluctuating
condition, and did not go on with their expedition against the Jews,
but thought that to make any attack upon foreigners was now
unseasonable, on account of the solicitude they were in for their
own country.” The reports of the destruction and rapine were
so horrible that it is reported of General Vespasian: “And as this
sorrow of his was violent, he was not able to support the torments
he was under, nor to apply himself further in other wars when his
native country was laid waste.” °

According to the pseudo (after-the-fact) prophecy of 4 Ezra
12:16-19, written around A.D. 100, the Empire was “in danger of

14, Tacitus, Histories 1:11. Emphsais added.

15. See Chapter 10,

16, For proof that the sixth head/king is Nero, see Chapter 10,
17, Suetonius, espasian 1.

18, Josephus, Wars 4:9:2.

19. ibid, 4102.

74 The Beast of Recelation

falling”: “In the midst of the time of that kingdom great struggles
shall arise, and it shall be in danger of falling; nevertheless it shall
not fall then, but shall regain its former power.”2°

Josephus agrees that during this time Rome was brought near
to utter “ruin.”?' He notes that “about this time it was that heavy
calamities came zbout Rome on all sides.” Josephus writes else-
where that “the Roman government [was] in a great internal
disorder, by the continual changes of its rulers, and [the Germans]
understood that every part of the habitable earth under them was
in an unsettled ar d tottering condition.” Men everywhere under-
stood that “the state of the Remans was so ill.”

But what eventually occurred at the end of these death throes?
The rest of Suetonius’s quotation begun above informs us that “the
empire, which for a long time had been unsettled and, as it were,
drifting through the usurpation and violent death of three emper-
ors, was at last taken in hand and given stability by the Flavian
family.”* Josephus sets forth this view of things when he writes,
“So upon this confirmation of Vespasian’s entire government,
which was now settled, and upon te unexpected deliverance of the
public affairs of the Romans from ruin, Vespasian turned his thoughts
to what remained unsubdued in Judea.” Thus, after a time of
grievous civil wars, the Empire was reived by the ascending of Vespasian
to the purple,

James Moffatt states the matter well when he writes regarding
Revelation 13:3: “The allusion is . . . to the terrible convulsions
which in 69 A.D. shook the empire to its foundations (Tat. Hist,

20, For an excellent analysis of 4 Ezz, see Bruce M, Metzger, “The Fourth Book
of Ezra,” in James H. Charlesworth, cd., The Old Testament Preudepigrapha, 2 vols.
(Garden City: Doublecay, 1983) 9517 £

21, Josephus, Wars ‘115

22, mid, 4101,

28, Tid, 7:42.

24, Ibid, 74:2.

25, Vespasion 1,

26. Wars 411:5.Emhasis added.

 

 

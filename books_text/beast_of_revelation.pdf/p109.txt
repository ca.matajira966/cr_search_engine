The Revival of the Beast 75

i. 11 ). Nero’s death with the bloody interregnum after it, was a
wound to the State, from which it only recovered under Vespasian.
It fulfilled the tradition of the wounded head... . The vitality of
the pagan empire, shown in this power of righting itself after the
revolution, only added to its prestige.”2’

The relevant verses in Revelation regarding the death and
revivification of the Beast can properly be understood as prophesy-
ing the earth-shaking historical events of the late A.D. 60s era.
Rome died, as it were, and returned again to life. In light ofJohn’s
original audience (Rev. 1:4, 11), his call for their careful considera-
tion (Rev. 1:3; 13:9), and his contemporary expectation (Rev. 1:1,
3), we must wonder why commentators project themselves into
the distant future seeking some other fulfillment of these events.
All the evidence heretofore dovetails nicely with this revivification
factor.

An Objection Considered

The reference to the “eighth” king in Revelation 17:11 has
caused some commentators to stumble here. There we read: “And
the beast which was and is not, is himself also an eighth, and is
one of the seven... .” In response to a view such as I am
presenting, some commentators note that the eighth emperor of
Rome was actually Otho, the second of the rulers during Rome's
awful civil wars. Thus, they point out, this head does not refer to
Vespasian. Given the interpretive approach presented above, it
would appear that the eighth head (according to my calculation)
is one of the destroying elements of Rome, not one who actually
stabilized the Empire, causing its revival. Consequently, the sup-
posed imagery fails.

This problem should not deter acceptance of the view I have
presented. A consultation of the Greek text helps alleviate the
apparent tension in the view. Exegetically, the chronological line

27. James Moffatt, The Rewlation of St. John the Divine, vol. Sin W. Robertson Nicoll,
cd., The Expositor’s Greek Testament (Grand Rapids: Eerdmans, rep. 1980), p. 430.

xiv The Beast of Revelation

misleadingly ider tified “Church Age”). This conclusion is not
warranted by the various biblical texts. The /ast days spoken of in the
New Testament were eschatological last days only for national Israel, not for
the New Covenant church. The “last days” were in fact the early days
of the church of Jesus Christ.

How do we know this? How do we know that we are not now
living in the last days and never will be? How do we know that the
New Testament was written in the last days, which came to a close
over 1,900 years ago? Because the New Testament clearly says so.
The author of the Epistle to the Hebrews specifically identified his
own era as the “last days.” He wrote that God “Hath in these last
days spoken unto 1s by his Son, whom he bath appointed heir of
all things, by whom also he made the worlds” (Heb. 1:2). He was
quite clear: he and his contemporaries were living in the last days.

The Destruction of tie Temple

So, we need ‘o ask this obvious question: The last days of
what? The answer is clear: i last days of the Old Covenant, including
national Israel. The New Testament writers were living in the ast
days of animal sacri,ices in the temple. This is the primary message of
the Epistle to the Hebrews: the coming of a better sacrifice, a
once-and-for-all s acrifice, Jesus Christ. We read: “And for this
cause he is the mediator of the new testament, that by means of
death, for the redemption of the transgressions that were under the
first testament, they which are called might receive the promise of
eternal inheritance. For where a testament is, there must also of
necessity be the de ath of the testator” (Heb. 9:15-16). The inescap-
able concomitant of Jesus’ sacrifice at Calvary was His annulment
of the Old Covenant’s sacrificial system:

And almost all things are by the law purged. with blood; and
without shedding of blood is no remission. It was therefore neces-
sary that the patterns of things in the heavens should be purified.
with these; but tt e heavenly things themselves with better sacrifices
than these. For Christ is not entered into the holy places made with
hands, which are the figures of the true; but into heaven itself, now
to appear in the presence of God for us: Nor yet that he should offer

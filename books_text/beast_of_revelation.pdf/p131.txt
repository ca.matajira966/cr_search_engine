The Thematic Evidence 99

Judea, the land where the city of Jerusalem is found, in which the
Lord was crucified (Rev. 11:1,2, 8).

The Destruction in the Land

The fact that an angel intervenes in order to prevent their
being destroyed along with the Land surely indicates the era prior
to the devastation of Israel in A.D. 70 (remember the expectation
of soon occurrence that we noted in Chapter 2 above). Were “the
Land” already destroyed (as it was in August, A.D. 70), such a
promised protection would have been embarrassingly anachronis-
tic,

In the Olivet Discourse Jesus spoke of the destruction of the
very temple to which the disciples could physically point (Matt.
24; 1-2), He warned His disciples that they should flee judea (Matt.
2416) when it was time for these things to come to pass (which
occurred in A.D. 70). He added further that they should accept
His promise that these horrendous events would be cut short
(Matt. 2422), and that he who endured to the end would be saved
through it all (Matt. 24:13). He also clearly taught that all of these
things would happen to “this generation” (Matt. 2482). Indeed,
this coming event was to be “the great tribulation” (Matt.
24:21) — the very tribulation in which John finds himself enmeshed
even as he writes (Rev. 1:9; cp. 7:14).

The protection ofJewish Christians in Jerusalem is thus indi-
cated in Revelation 7:1-7 via the symbolism of sealing. This refers
to the providential protection of those Christians ofJewish lineage
who were “in the Land.”

An extremely interesting and famous piece of history informs
us that the Jewish Christians in Jerusalem escaped the city before
it was too late, possibly either at the outset of the War or during
one of its providential lulls. Church historian Eusebius (A.D.
260-340) records the situation thus:

But the people of the church in Jerusalem had been commanded.
by a revelation, vouchsafed to approved men there before the war,

10
THE POLITICAL EVIDENCE

Here is the mind which has wisdom. The seven heads are seven
mountains on which the woman sits, and they are seven kings; jive have

fallen, one is, the other has not_yet tom; and when he comes, he must
remain a little while (Rev. 17:9-10).

In this chapter I will be considering what I believe to be the
leading objective evidence for Revelation’s date of composition.
That evidence is contained in the statement regarding the “seven
kings” in Revelation 17, which is cited above. Here we are given
what I believe to be a concrete political statement of an historically
datable quality th at clearly establishes a time-flame for the dating
of Revelation. As I will show, that time-frame not only precludes
a Domitianic date for Revelation, but firmly establishes a Neronic
one.

The prominet features of the chapter bearing upon our in-
quiry are: (1) Thre is the imagery of the Beast with seven heads
(Rev. 17:3, 7,9; c)>.13:2}. (2) These seven heads are said to repre-
sent both seven mountains and seven kings (17:9, 10). (3) Of the
seven kings, “five have fallen,” “one is” reigning, and one is to
come to rule for% little while” (17:10).

Let us consider this evidence carefully in demonstration of
Revelation’s date.

The Line of Kings
Earlier, in Chapter 1, I showed that the seven mountains rep
102

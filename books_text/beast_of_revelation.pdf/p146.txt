114 The Beast of Revelation

the soon occurrence expectation of John (Rev. 1:1, 3, 19; 3:10;
22:6ff). No Jerusa‘em in the distant future could be in view, in light
of John’s own stated temporal restrictions. In addition, the refer-
ence to Jerusalem’s being the place where the Lord was crucified
would seem more appropriate for the first century Jerusalem than
for the twentieth- or twenty-first-century Jerusalem.

Since historical, first century Jerusalem is in view, we should
expect that the prominent, historical feature of that city should be
in view, as well: the Herodian temple complex.

The Thematic Demand

Second, the theme of the book suggests the appearance of a
literal temple in Jerusalem. We must remember that Revelation
was written to warn that “those who pierced Him” (the Jews of
the first century, see Chapters 2 and 9) would experience His
cloud-judgment coming upon them (Rev. 1:7). Again, we must
recall that the judgment would be soon (Rev. 1:1, 3, 19; 3: 10;
22:6f£), not thousands of years later. Hence, the potential signifi-
cance of the literal, historical temple — the place of Jewish wor-
ship — in this passage, which speaks of the place where the Lord
was “pierced.”

Scripture Interpret Scripture

Third, Scripture elsewhere speaks of the destruction of the
historic temple existing in Jesus’ day, and with language closely
corresponding to “hat found in Revelation 11. John uses the future
tense when he speaks of the nations treading down the city (‘they
will tread,” Rev. 11 :2b). This is not a reminiscence of a past event,
but rather a futur: expectation.

Revelation 11:1-2 clearly reflects the prophecy of Christ as
recorded in Luke 21:24. The prophecy in Luke 21 (like its parallels
in Matt. 24 and Mark 13) is widely held to refer to the destruction
of Jerusalem and the temple in A.D. 70. Indeed, this must be the
case, for at least two reasons: (1) The very occasion of the pro-
phetic discourse arises from the disciples’ pointing out the beauty

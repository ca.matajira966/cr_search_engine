The Architectural Evidence 23

ished, and those in whom sovereignty remained destroyed a great
city and righteous people.” 17 About three hundred lines later we
read: “But now a certain insignificant and impious king has gone
up, cast it down, and left it in ruins with a great horde and.
illustrious men. He himself perished at immortal hands when he
left the land, and no such sign has yet been performed among men
that others should think to sack a great city.”

Josephus sadly extols Jerusalem’s lost glory after its destruc-
tion: “This was the end which Jerusalem came to by the madness
of those that were for innovations; a city otherwise of great magnifi-
cence, and of mighty fame among all mankind,”'? A few paragraphs
later we read: “And where is not that great city, the metropolis of
the Jewish nation, which was fortified by s0 many walls round
about, which had so many fortresses and large towers to defend it,
which could hardly contain the instruments prepared for the war,
and which had so many ten thousands of men to fight for it? Where
is this city that was believed to have God himself inhabiting
therein? It is now demolished to the very foundations.”2°

Clement of Rome

A number of evangelical scholars argue that the first-century
writer Clement of Rome spoke of the temple as still standing, even
though he (allegedly) wrote around A.D. 90+. Clement’s relevant
statement is: “Not in every place, brethren, are the continual daily
sacrifices offered, or the freewill offerings, or the sin offerings and
the trespass offerings, but in Jerusalem alone. And even there the
offering is not made in every place, but before the sanctuary in the
court of the altar; and this too through the high-priest and the
aforesaid ministers, after that the victim to be offered bath been
inspected for blemishes.”!

17, Sibpliine Oracks 6150-154, Emphasia added,

18, ibid, 5:408-413, Emphasis added,

19. Josephus, 7h Wars ofthJms2/ :1. Emphasis added.
20. Wars 78:7. Emphasia added.

21,1 Clement 41.

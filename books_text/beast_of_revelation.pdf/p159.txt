The Architectural Evidence 127

Rome was devastated by eonflagrations, in which her most ancient
shrines were consumed and the very Capitol fired by citizens’
hands. Sacred rites were defiled; there were adulteries in high
places, The sea was filled with exiles, its cliffs made foul with the
bodies of the dead. In Rome there was more awful cruelty. ...
Besides the manifold misfortunes that befell mankind, there were
prodigies in the sky and on the earth, warnings given by thunder-
bolts, and prophecies of the future, both joyful and gloomy, uncer-
tain and clear. For never was it more fully proved by awful
disasters of the Roman people or by indubitable signs that gods
care not for our safety, but for our punishment.

Schaff commented on this period that “there is scarcely an-
other period in history so full of vice, corruption, and disaster as
the six years between the Neronian persecution and the destruc-
tion of Jerusalem.”° Nothing approaching this chaos or even
hinting at this level of upheaval was remotely associated with
Domitian’s death. Combining the Neronian persecution begun in
A.D. 64 with the Roman Civil War in A.D. 68-69, all becomes
very clear.

Finally, there is the very temple reference in question in /
Clement 41 (cited above). All things considered, the reference to
the temple services as if they were still being conducted is best
construed as demanding a pre-August, A.D. 70 dating. Edmund-
son insists that “it is difficult to see how the evidential value af c.
xii, can be explained away.”2!

It would seem that, at the very least, reference to the statement
in I Clement 41 cannot discount the possibility of our approach to
Revelation 11, in that the date of / Clement is in question. And as
is probably the case, Clement did write his epistle prior to the
temple’s destruction.

29, Tacitus, Histories 1:2-3,

ire Schaff, History of the Christian Church, 3rd ed. (Grand Rapids: Eerdmans,
13391.

81, Edmundeon, p. 193,

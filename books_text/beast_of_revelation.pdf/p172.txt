140 The Beast of Revelation

Lawson agrees.’Iven late-date advocates Donald B. Guthrie and
Robert H. Mource lean in this direction. Guthrie writes that
“there are many common images in the two writers which are
most naturally explained if Hermas knew our Apocalypse.”*

But now. When was it written?

The Date of the Shepherd

Unfortunately, there is a problem with ascertaining the date
of the composition of The Shepherd due to the question of its
authorship. J. B. Lightfoot’s analysis of the matter will guide our
thinking.® Was it written by (1) the Hermas greeted by Paul in
Remans 16:14, as Origen suggests? Or by (2) the brother of Pius
I(A.D. 140-150), as the ancient Muratorian Canon teaches? Or
by (3) some unknown Hermas who lived in the time of the bish-
opric of Clement of Rome (A.D. 90-100), as a number of modern
scholars propose? An assured conclusion on the matter may never
be reached. Even Lightfoot, who prefers a date in the era of A.D.
140-150, acknowledges that the internal evidence strongly suggests
a date in the span of A.D. 90-100.10

Church histo: ian Philip Schaff is decisively supportive of an
earlier date for The Shepherd, arguing that it was written by the
very Hermas mentioned in Remans 16:14. He notes that the
earlier date is suggested by its authoritative usage in the writings
of Irenaeus, Clement of Alexandria, Origen, Eusebius, and Jer-
ome. '! We can add that early in his career, Tertullian seems to

i, John Lawson, A Theological and Historical Introduction to the Apostolic Fathers (New
York: Macmillan, 1961 .p. 220.

8. Donald B, Gutirie, New Yéstament Introduction 3rd ed. (Downers Grove, IL
Inter-Varaity Press, 19; '0) pp. 931-982. Compare Robert Mounce, 77aa Book of Revela-
tion (Grand Rapids: Eerdmans, 1977), pp. 36-37.

9. J. B. Lightfoot znd J, R. Harmer, The Apostolic Fathers (Grand Rapids: Baker,
[1891] 1984), pp. 208-204.

10, ibid, p. 204.
11. Philip Schalf, H'story of the Christian Church, 8rd. ed., 7 vols. (Grand Rapids:
Eerdmans, 1910) 1:6878

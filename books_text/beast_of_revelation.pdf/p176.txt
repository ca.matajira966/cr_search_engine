144 The Beast of Revelation

Clement, Section 5. Whatever dates the writer of the Canon as-
signed to Paul’s epistles, he could not have made them later than
Paul’s death! This is a most important piece of early evidence with
which to reckon. It clearly teaches, then, that John wrote his seven
letters in Revelation prior to A.D. 68.

Tertullian

Tertullian lived from A.D. 160-220. His era overlaps Ire-
naeus’s era briefly. We have many of Tertullian’s writings still
today. In his Exclusion of Heretics he makes a statement that is of
significance to our inquiry. Tertullian implies that John’s banish-
ment occurred at the same time Peter and Paul suffered martyr-
dom (about A.D. 67-68): “But if thou art near to Italy, thou hast
Rome, where we also have an authority close at hand. What an
happy Church is “hat! on which the Apostles poured out all their
doctrine, with their blood: where Peter had a like Passion with the
Lord; where Paul bath for his crown the same death with John;'
where the Apostle John was plunged into boiling oil, and suffered
nothing, and was afterwards banished to an island.” In Jerome’s
Against Jovinianum, Jerome certainly understood Tertullian to state
that John was ba ished by Nero.” In addition, when Tertullian
speaks of Domitian’s evil in the fifth chapter of his Apslogy, he does
not mention anything about John’s suffering under him! Such is
quite strange ifJohn actually suffered under Domitian.

Tt would seem that Tertullian’s reference to an attempted oil
martyrdom of Jotn is quite plausible historically. This is due to
the very nature 0! the Neronic persecution of Christians in A.D.
64. Roman histo :ian Tacitus informs us that Christians were
“fastened to crosses to be set on fire, that when the darkness fell
they might be buried to illuminate the night.”*!

18, That is, John the Baptist-

19. Tertullian, Exclwion of Heretics, 36.
20, Jerome, Against jovinianum 1:26,
21, Tacitus, Annals 15:44.

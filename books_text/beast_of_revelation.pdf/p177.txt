The Historical Evidence (1) 45

Such a spectacle doubtless could have involved the dipping of
the victims in oil to provide a lasting illumination of fire. As Schaff
observed: “If there is some foundation for the early tradition of the
oil-martyrdom of John at Rome, or at Ephesus, it would naturally
point to the Neronian persecution, in which Christians were cov-
ered with inflammable material and burned as torches.”

Epiphanies of Salamis

Epiphanies (A.D, 315-403) was elected the bishop of Salamis,
Cyprus, in about A.D. 367, and was an intimate friend ofJerome.
He is noted for his unique witness to the banishment ofJohn: He
states twice that it was during the emperorship of Claudius.” He
writes that John “prophesied in the time of Claudius .. . the
prophetic word according to the Apocalypse being disclosed.”

A number of scholars see Epiphanies’s statement not so much
as an extravagant tradition, as a rare designation of Nero. Some
have suggested that Epiphanies may have used another of Nero’s
names, rather than his more common one. Nero is often called by
his adoptive name “Claudius” on inscriptions. For instance, he is
called “Nero Claudius” and “Nero Claudius Caesar” in certain
places. Even late-date advocates Donald Guthrie, Robert Mounce,
and James Moffatt recognize that this was probably what was
intended by Epiphanies.“

It is clearly the case that Epiphanies stands solidly in the early
date tradition. It is extremely doubtful that he simply created his
“evidence” out of the blue.

22, Schaff, History 1498,

28, Heresies 51:12,88,

24, Among these we may list late date advocates Guthrie, Inirsduction, p, 967;
Mounce, Revelation, p. 81; and James Moffatt, The Revelation of St. John ta Divine, in
W. Robertson Nicoll, ed., The Expositor's Greek Testament, vol. 6 (Grand Rapids:
Eerdmans, rep. 1980), p. 505; a8 well as early date advocates F. J, A. Hort, The
Apocalypse of St.John, J-I1f (London: Macmillan and Co., 1908), p. xviii, and Robin-
aon, Redating, p, 224,

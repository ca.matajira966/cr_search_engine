180 The Beast of Relation

decline among the Galatians? In Galatians 1:6 Paul writes: “I am
amazed that you are so quickly deserting Him who called you by
the grace of Christ, for a different gospel.”

Consider also Paul’s concern over the multitude of troubles
within the church of Corinth. This church was founded in A.D.
49 and Paul wrot: to it with heavy heart in A.D. 57. Indeed, Paul
anticipated such problems to be experienced among churches
virtually as soon as he left the scene, as he noted to the elders of
the church at Ephesus (Acts 20:29). Was not Timothy urged to
remain at Ephesus because of the entry of false doctrine within
Pauls lifetime (1 Tim. 1:6)?

Paul also experienced distressing defections from fidelity to
him as a servant of Christ within his ministry (2 Tim. 4 10). Paul
expresses concern over the labors of Archippus at Laodicea (one
of the churches in question) when he warns him to “take heed to
the ministry which you have received in the Lord, that you may
fulfill it” (Col. 4:15-17).

How much more would such a problem of slackened zeal be
aggravated by the political circumstances generated from the in-
itiation of the Neronic persecution in A.D. 64! Did not Jesus’
teaching anticipai e such (Matt. 13:20, 21; 24:9, 10)? There is no
compelling reason whatsoever to reject the early date of Revelation
on the basis of the spiritual decline in certain of the Seven Churches.

Conclusion

A careful consideration of the merits of each of the major
arguments from the Seven Letters demonstrates their inconclusive
nature. Neither the arguments considered individually, nor all of
them considered collectively, compels acceptance of the Domi-
tianic date of Revelation. This is all the more obvious when their
inconclusive nature is contrasted with the wealth of other internal
considerations for an early date, as rehearsed heretofore in the
present work.

In fact, the Seven Letters even have elements more suggestive
of a period of time prior to the destruction of the temple: (1) The

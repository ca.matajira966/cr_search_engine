Conclusion 183

future” of despair. The woes of Revelation have already occurred!

Tf these views are correct — and I am convinced beyond any
doubt that they are — then Revelation was given as God’s divinely
inspired and inerrant pre-interpretive Word on the destruction of
the temple order and the divorce of Israel as God’s covenant wife.
We have God’s Word that this was brought about in the first
century by the decree of the Lord Jesus Christ.

In Revelation we have a biblical explanation of the cata-
strophic events of the A.D. 60s. The important events of that era
ineluded: the outbreak of the first, precedent-setting imperial per-
secution of Christianity; the death of Christianity’s first and most
heinous Roman persecutor, Nero Caesar; the subsequent near
collapse of Rome, followed by its revival under the non-persecuting
emperors (Vespasian and Titus); the destruction of Jerusalem and
the temple; and the hope for the increase throughout the earth of
God’s New Creational salvation.*

The Evidence for Our Answers

Our convictions regarding the identity of the Beast and the
date of Revelation’s composition have not been demanded merely
by our theological perspective or sociological outlook. The relief
we may experience regarding the vanished prospect of sending our
children into such a dismal future is a happy side-effect of our
inquiry. Now that we have looked rather carefully at the evidence
for the date of Revelation, I believe we are compelled by historical
and exegetical evidence to assert that Revelation was written in
the A.D. 60s — not in the A.D. 90s.

It is my deep conviction that much of the decline of the
influence of orthodox Christianity on our culture today is due to a
pervasive, pessimistic eschatology. As dispensationalist R. A. Tor-
rey loved to say at the turn of the century: “The darker the night
gets, the lighter my heart gets."2 If Christians refuse to be the light

1. Cp. Rev, 21-22 with 2 Cor. 5:17; Gal. 6:15; Matt. 13:3HE; 2 Cor, 5:21 ff,
2, Cited in Dwight Wilaon, Armageddon Now! (Grand Rapids: Baker, 1977), p. 37.

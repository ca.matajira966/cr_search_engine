204

Mara, 64,

Martial, 18,42, 109.

Martin of Toura, 160-162,

Martyrdom) (See also specific names of
martyrs, e.g., Jamea Paul), 25,45, 52,
69,96, 117, 142, 144145, 156, 165,

Martyr, Justin, 26n, 92 117, 181.

Melito of Sardia, 117.

Measiah, 40n, 95, 182.

Millennial views -
amillennialism, xi, x{, xvi,
postmillennialism, x?, xxi,
premillennialiam, xi, xxvi, xvii, 0c.

Millennium, xvi, 83,86, 119.

Mishnah, x.

Mithras, 65.

Moses, xxx, 95.

Mourn (See also: Weep), 26, 88, 90, 95,
a.

Mountain(s) (Gee also: Seven -
mountains), 112, 121,

Muratorian Canon, 140-142, 149-144,
187.

Musician), 18,63,65, 56.

Mystery (mysterious), :19,70, 105, 165.

“Myatery of iniquity,” <4,45.

Mythology), 158, 171-175.

Nation(s), xxxi, 26,47 93, 98, 111, 112,
114, 115, 120, 122,10, 131,

Nazarene, 135, 137,

“Near,” (See also: Revel: tion -
expectation), 21, 28, 101, 18,

Nero (emperor), (See «és: Beast; Actor;
Charioteer Musician; Theater), 13,
28, 29, 32, 34, 36>36, 39, 44, 47, 49,
60, 61, 62, 53, 64, £5, 66, 67, 62, 97,
104-110, 126, 147, 158, 158, 160, 164,
186, 187.
adoption, 15, 16.
adoptive name, 14, 15n,
appearance, 15,63.

The Beast of Revelation

Apollo faacination of, 68.

Augustus fascination of, 63,64,

burning of Rome and (Se: Rome -
burning of).

character, 14-19, 88, 40-46, 158-161,
192, 185,

Clandius (another name of Nero), 16,
145,

death, 18-19, 44, 63, 66, 71, 72, 76,
77,85, 104, 108, 110, 159, 171, 174,
181, 188,

extravagance, 17, 18,

family (See ao individual entries), 14,
15,16, 17n, 41.

homosexuality 0<17,41,46,

John and, 47, 53, 81, 104, 144-145,
146, 147, 148,

killa mother, 17,82,41-42,45.

Kills Roman nobles, 17-18,41,42.

kills wives, 17,41,42.

legend regarding (Redivivus), 44, 45,
159, 171-175.

life, 15-19, 158-161.

Lucius Domitius Ahenobarbus, 14,

Inarriage, 16, 17,41.

persecutor (Sec Persecution -
Neronic}.

pretender, 72, 127, 178.

Quinquennium Neronis, 16,68.

revival (See Nero - legend
regarding).

wife (wives), 17,

worship of, 57,63-67, 168,

youth, 15-16,41.

New Covenant (See aise: Covenant), x,
xiv, 120.

New Creation, 183.

New Testament, ix, x, xiii, xiv, xv, xvi,
xvii, 8, 4, 6, 24, 26, 87, 54, 90, 91, 94,
96, 124, 129, 132, 184, 156, 168.
expectation of, 27n.

Numbers (symbolic use of), 31-33, 98n,

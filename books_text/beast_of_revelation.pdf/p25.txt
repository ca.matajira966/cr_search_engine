Publisher's Preface wxvit

I make thine enemies thy footstool. The Lorp shall send the rod
of thy strength out of Zion: rule thou in the midst of thine enemies
(Psa. 110:1-2).

This passage makes it clear that a legitimate goal of God’s
people is the extension in history and on earth of God’s kingdom,
to rule in the midst of our spiritual enemies and opponents. But
more to the point, the Lord speaks to Jesus Christ and informs
Him that He will sit at God’s right hand until His enemies are
conquered. Obviously, God’s throne is in heaven. This is where
Jesus will remain until He comes again in final judgment.

This is also what is taught by the New Testament’s major
eschatological passage, I Corinthians 15. It provides the context
of the fulfillment of Psalm 110. It speaks of the resurrection of
every person’s body at the last judgment. Jesus’ body was resur-
rected first in time in order to demonstrate to the world that the
bodily resurrection is real. (This is why liberals hate the doctrine
of the bodily resurrection of Christ, and why they will go to such
lengths in order to deny it.)*! This passage tells us when all the
rest of us will experience this bodily resurrection. What it describes
has to be the final judgment.

For as in Adam all die, even so in Christ shall all be made alive.
But every man in his own order: Christ the firstfruits; afterward
they that are Christ’s at his coming. Then cometh the end, when
he shall have delivered up the kingdom to God, even the Father;
when he shall have put down all rule and all authority and power.
For he must reign, till he bath put all enemies under his feet. The
last enemy that shall be destroyed is death (I Cor. 15:22-26).

Jesus reigns until God the Father has put all enemies under
Jesus’ feet. But Jesus reigns from heaven; if this were not true, then
how on earth could He be seated at the right hand of God, as
Psalm 110 requires? Any suggestion that Jesus will rule physically on earth

21, A notorious example of such literature is Hugh J, Schofield, Ths Passoner Plot:
New Light on the History of Jesus (New York: Bantam, [1966] 1971 ), It had gone through
seven hardback printings and 14 paperback printings by 1971.

Publisher’s Preface xxxiii
Neither do dispensationalists:

Then Jesus sent the multitude away, and went into the house:
and his disciples came unto him, saying, Declare unto us the
parable of the tares of the field. He answered and said unto them,
He that soweth the good seed is the Son of=, The field is the
world; the good seed are the children of the kingdom; but the tares
are the children of the wicked one; The enemy that sowed them is
the devil; the harvest is the end of the world; and the reapers are
the angels. As therefore the tares are gathered and burned in the
fire; so shall it be in the end of this world. The Son of man shall
send forth his angels, and they shall gather out of his kingdom all
things that offend, and them which do iniquity; And shall cast
them into a furnace of fire: there shall be wailing and gnashing of
teeth, Then shall the righteous shine forth as the sun in the
kingdom of their Father. Who bath ears to hear, let him hear
(Matt, 13:36-43).

Dispensationalists refuse to hear.

This book presents a message of moral responsibility. Every
message of true hope inevitably is also a message of moral respon-
sibility. In God’s world, there is no hope without moral responsi-
bility, no offer of victory without the threat of persecution, no offer
of heaven without the threat of hell. Deny this, and you deny the
gospel. He who has ears to hear, let him hear.

This Book Is About Time

Why would a Christian economics institute publish a book on
the beast of revelation and the dating of the Book of Revelation?
Because a crucial aspect of all economics, all economic growth, is
time perspective. Those individuals and societies that are future-
oriented save more money, enjoy lower interest rates, and benefit
from more rapid economic growth. A short-run view of the future
is the mark of the gambler, the person in poverty, and the underde-
veloped society. Those who think in terms of generations and plan
for the future see their heirs prosper; those who think in terms of
the needs and desires in the present cannot successfully compete

The Number of the Beast 35

The difference surely is no accident of sight made by an early
copyist. The numbers 666 and 616 are not even similar in appear-
ance in the original Greek — whether spelled out in words or
written out as numerals, The letters representative of the values
for 60 and for 10 (which would make the difference between the
two readings) are as different as any two letters could be. The
letter used as the value for 60 is §; the letter for the value 10 is
If these values were originally spelled out in words there still would
be no similarity. The value for 60 would be indicated thus: hexe-
konta; that for 10 would read: dia. There is no way a copyist could
confuse the two, As textual scholars agree, it must be intentional.'
But again we ask, Why? Although we cannot be absolutely certain,
a strong and most reasonable case may be made for the following
conjecture. As shown above, John, a Jew, used a Hebrew spelling
of Nero’s name in order to arrive at the figure 666. But when
Revelation began circulating among those Jess acquainted with
Hebrew, a well-meaning copyist who knew the meaning of 666
might have intended to make its deciphering easier by altering it
to 616. It surely is no mere coincidence that 616 is the numerical
value of “Nero Caesar,” when spelled in Hebrew by transliterating
it from its more common Latin spelling.

Such a conjecture would satisfactorily explain the rationale for
the divergence: so that the non-Hebrew mind might more readily
discern the identity of the Beast. Even late-date advocate Donald
Guthrie, who rejects the Nero theory, grants that this variant gives
the designation Nero “a distinct advantage.”!9 Ag renowned Greek
scholar Bruce Metzger says: “Perhaps the change was intentional,
seeing that the Greek form Neron Caesar written in Hebrew
characters (nren gsr) is equivalent to 666, whereas the Latin form
Nero Caesar (nw gsr) is equivalent to 616.”2° Such a possibility

18, Bruce M, Metzger, A Textual Commentary on the Greek New Testament (London.
United Bible Societies, 1971), pp. 751-752.

19. Donald B. Guthrie, New Testamet Production, 3rd ed. (Downer’s Grove, IL
Tnter-Varsity Presa, 1970), p. 959,

20. Metzger, Textual Commentary, p. 752.

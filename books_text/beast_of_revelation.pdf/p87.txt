The War of the Beast 53
The Length of the Neronic Persecution

Remarkably, the Neronic persecution of Christianity lasted
almost precisely the length of time mentioned in Revelation 13:5.
The persecution began after the destructive burning of Rome,
which began on July 19, A.D. 64." Soon after Rome’s near de-
struction rumors began circulating that Nero himself intentionally
caused the fires.!®

Although Nero’s unsuccessful efforts to dispel the rumors by
his frantic largess must have taken a little time, he could not afford
to wait for an extensive period of time to quell the politically
darnaging accusations. So in the latter part of November, A.D. 64,
furious persecution broke out upon the innocent church." This
persecution continued against the church for several years, ulti-
mately claiming the lives of Peter and Paul, as noted previously,
in either A.D. 67 or 68.”

This persecution finally ended with the death of Nero, which
occurred on the ninth of June, A.D. 68."Noted church historian
Mosheim wrote of Nero’s persecution: “Foremost in the rank of
those emperors, on whom the church looks back with horror as
her persecutors, stands Nero, a prince whose conduct towards the
Christians admits of no palliation, but was to the last degree
unprincipled and inhuman. The dreadful persecution which took
place by order of this tyrant, commenced at Rome about the
middle of November, in the year of our Lord 64. ... This

17, Tacitus, Annals 15:41. See discussion in Schaff, History 1:379.

18, TacitUs, Annals 15:39; Suetonius, Nero 38,

19. John Laurence von Mosheim, Historical Commentariss, vol. 1, trans, Robert
Studley Vidal (New York S. Converse, 1854), p. 138; Moses Stuart, Commentary on
the Apocalypse, 2 vols. (Andover: Allen, Merrill, and Wardwell, 1845) 2:279.

20, Merrill F, Unger, Archaeology and the New Testeneat (Grand Rapids: Zondervan,
1963), p. 828, See also A, T, Robertson, “Paul, the Apostle” in James Orr, cd., The
Intuitional Standard Bible Engylopedia (Grand Rapids: Eerdmans, 1956) 3:2287; Rich-
ard Longenecker, The Ministry and Message of Poul (Grand Rapids: Zondervan, 1971),
pp. 85-86,

21, Stuart, Apocalypse, 2469, See also Justo L, Gonzalez, The Early Church to the
Dawn of te Reformation (San Francisco: Harper and Row, 1984), p. 36.

Additional External Witnesses 89

With regard to Hermas in particular there are good grounds for
questioning its statements. Thus Irenaeus, who resided in Rome less
than twenty years after thedeath of Pius, quotes the opening sentence
of the first Mandate of the Shepherd as “scripture” [Against Heresies
4:34:2], which would scarcely be likely if it was known to have been
composed within living memory. Not much later Tertullian strongly
disparages Hermas in contrast with Hebrews and its seems improb-
able that he would not have deployed against it the argument of its
late composition. Origen who freely cites the Shepherd as scripture,
attributes it indeed in his Commentary on Romans to the first-century
Hermas greeted by Paul in Rem. 16.14.16

A persuasive case can be made from the internal evidence which
is decidedly against not only the Muratorian Canon’s statement as
to the date of The Shepherd, but even the date of A.D. 95 or 96, as
well. Regarding the Muratorian Canon’s identification of Hermas as
the brother of Pius of Rome, it should be noted that if the identifica-
tion is correct, a most remarkable situation exists. In defiance to an
expectation based on the assertion of the Muratorian Canon, Her-
mas, a foster-child sold into slavery in Rome ( Vision 1:1; 1), never
mention-s his alleged brother Pius, bishop of Rome. And this despite the fact
he does mention other family members. Moreover, nowhere in The
Shepherd is there any indication that there exists anything approach-
ing a monarchical episcopate — whether in Rome where Pius would.
have been such (Vision 2:4:3) or elsewhere. He speaks, instead, of “the
elders that preside over the church” (Vision 2:4:3). The explanation
suggested above by Barnes and others as to the Canon’s confusion
suitably accounts for these matters.

Furthermore, in Vii?on 2:4:2f Hermas is told to write two books.
One of these is to be sent to Clement who in turn “was to send it to
foreign cities, for this is his duty.” The other was to be sent to
“Grapte,” apparently a deaconess. 17 As Edmundson" and Robin-
son’ carefully demonstrate, this implies Clement’s role as a subordi-
nate secretarial figure. Obviously, then, The Shepherd could not
have been written later than about A.D. 90 after Clement was

16, Robinson, Redating, pp. 319-820.

17, George Edmundson, The Church in. Rome in the First Century (London: Longman’s,
Green, 1918), pp. 204,

18, Ibid, pp. 203.

19, Robinson, Redoting, pp, 821 f

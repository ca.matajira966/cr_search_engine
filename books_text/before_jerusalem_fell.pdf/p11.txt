Publish /s Preface xi

of Revelation. In writing this book, Chilton adopted Ray Sutton’s

summary of the Bible’s five-point covenant model.> Days of Vengeance
shows that John’s Apocalypse is structured in terms of this same

five-point model.® Chilton’s book was the first comprehensive verifi-

cation of Sutton’s thesis based on a New Testament book.’ Days of
Vengeance discusses the Book of Revelation in terms of these themes:

As God's covenant lawsuit against Israel

Asa worship liturgy of the church

As a prophecy of the fall of Jerusalem

As a rejection of political religion (Rome)

As a prediction of Christian dominion in history

The individual theses of his book were not in themselves revolution-

ary, but taken as a unit, they were. The book presents a new way of
reading this difficult New Testament text.

Preterism Revived

If Chilton’s commentary is correct, the overwhelming majority
of the eschatological events prophesied in the Book of Revelation
have already been fulfilled. This interpretation of New Testament
prophecy has long been known as preierism, meaning “from the past
tense,” ie., the preterit tense: over and done with. It should therefore
not be surprising to discover that defenders of both premillennialism
and amillennialism are exceedingly unhappy with Chilton’s book.
The premillennialist are unhappy with the book because it shows
that the apocalyptic New Testament language of God’s visible judg-

5, Ray R, Sutton, That You May Prosper: Dominion By Conant (Tyler, Texas: Institute
for Christian Economics, 1987).

6, The implications of Sutton’s discovery are shattering for dispensationalism, If the
Old Testament covenants were all structured in terms of a single five-point model, and
if this same model appears in many New Testament texts, even to the extent of
structuring whole books or- epistles, then the case for a radical discontinuity between the
Old Testament and the New Testament collapses. As a graduate of Dallas Theological
Seminary, Sutton fully understands the threat of his thesis for dispensationalism. So do
digpensational authors H. Wayne House and Thomas D, lee, which is why they refused
to discuss Sutton's thesis in their attack on Christian Reconstructionism, They buried
their brief summary of the five-point model in their annotated bibliography (seldom
read), and then failed to refer to this in the book's index, See House and Ice, Dominion
Theology: Blessing or Curse? (Portland, Oregon: Multnomah Press, 1988), pp. 488-38.

7. It was actually published a few months before Sutton's book, but Sutton had
discussed his thesis in detail withChilton while Chilton was writing his book.

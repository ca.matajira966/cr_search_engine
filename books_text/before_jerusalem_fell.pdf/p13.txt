Publisher's Preface xiii

Postmillennialism’s earthly eschatological optimism necessarily
places great responsibility on Christians to apply the Bible to every
area of life. It is my strongly held opinion that this has been the great
resistance factor in the acceptance of the Christian Reconstructionist
position. It is very difficult to “sell” responsibility, especially broad
new responsibility. I sense that premillennialist and amillennialists
are generally disturbed by the personal and ecclesiastical implica-
tions of this enormous moral and cultural burden. Postmillennial-
ism’s view of the future makes Christians morally responsible before
God for discovering and applying a Bible-based judicial and ethical
blueprint — a blueprint that should and eventually will govern the
institutions of this world. '” This means that the world is required by
God to be run in terms of His revealed law. It also means that God.
will positively bless societies and institutions in terms of their faithful-
ness to His revealed law. '! This is a crucial and long-neglected aspect
of the biblical doctrine of sanctification — the progressive sanctifica-
tion of institutions in history — which neither the premillennialists
nor the amillennialists are willing to accept.

The Quick Fix of %

One of the first accusations against Days of Vengeance — and surely
the easiest one to make without actually having to read the book - was
that the Book of Revelation could not possibly have been what
Chilton says it was, namely, a prediction of the fall of Jerusalem.
Jerusalem fell in A.D. 70; the Book of Revelation, we are assured,
was written in A.D. 96. Thus, the critics charge, the cornerstone of
Chilton’s thesis is defective.

This criticism would be unquestionably correct if, and only if, the
Book of Revelation was written after A.D. 70. If the book was written
prior to A.D. 70, Chilton’s thesis is not automatically secured, but if
Revelation was written after A.D. 70, then Chilton’s thesis would.
have to be drastically modified. Critics noted that Chilton’s text does
not devote a great deal of space defending a pre-A.D. 70 date. His
book therefore appears vulnerable.

10, In 1986 and 1987, Dominion Press published a ten-volume set, the Biblical
Blueprints Series. It was not well-received by the academic Christian world or the
evangelical-fundamentalist community.

11. Gary North, Dominion and Common Groce: The Biblical Basis of Progress (Tyler, Texos:
Institute for Christian Economics, 1987).

The Role of Internal Evidence 119

the integrity of the Temple and Jerusalem. These arguments, along
with several others, will serve as the focus of the present study as the
primary chronological allusions.

Thus, despite Kiddie, Guthrie and others of the late date school,
and those such as Hort of the early date school, there do seem to be
both inherently suggestive and positively compelling historical time-
frame indicators in Revelation. It is remarkable that whereas Kiddie,
for instance, may absolutely deny the presence of internal indications,
others — no less scholarly — may just as strongly assert the contrary.
The internal historical evidences compel the noted F. W. Farrar to
be “all but certain” as to the date of the book.” Stuart feels the same
certainty of conviction when he writes: “If there be anything certain
in the principles of hermeneutics, it is certain that they decide in
favour of a reference to Judea and its capital in Rev. vi — xi. The
very fact, moreover, that the destruction of Jerusalem (chap. xi) is
depicted in such outlines and mere sketches, shows that it was then
Future, when the book was written. It is out of all question, except by
mere violence, to give a different interpretation to this part of the
Apocalypse.”2!

Macdonald argues that “it will be found that no book of the New
Testament more abounds in passages which pany, have respect to
the time when it was written. istorian Edmundson writes ‘hat
“the Apocalypse is full of references to historical events of which the
author had quite recently been himself an eyewitness at Rome, or
which were fresh in the memories of the Roman Christians with
whom he had been associating.”®? He chooses a pre-A.D. 70 date and
states dogmatically that “the witness of the contents of the book itself,
as will be shown, amply justifies such an assertion. “2‘Torrey Vigor-

 

be raised against the date indicated by Irenacus, is the Apocalypse passage (17:9-1 1),
which refers to the 7 heads of the one Beast, . . .“ He sees the seventh as being either
Otho or Vespasian (Andre Feuillet, The Apocalypse, trans, Thomas E, Crane [Staten
Island: Alba House, 1965], p. 90).

20, Farrar, Early Doys, p. 418.

21, Stuart, Apocalypse 1:276,

22, Macdonald, Lyfe of John, p. 152,

28, George Edmundson, The Church in Rome in the First Century (London: Longman’s
Green, 1918), p. 164, He goes on to observe that “there is a certain amount of external
evidence, which has had much more weight than it deserves, apparently supporting a
late date” Gi.e,, Irenacus).

24, Ibid,

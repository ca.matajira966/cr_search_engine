10
THE IDENTITY OF THE SIXTH KING

We find an extremely important chronology indicator in Revela-
tion 17 where the “sixth king” is mentioned. The relevant portion of
the text containing the reference to the sixth king is in Revelation
17:3,6-13:

And he carried me away in the Spirit into a wilderness; and I saw a
woman sitting on a scarlet beast, full of blasphemous names, having
seven heads and ten horns. ... And I saw the woman drunk with
the blood of the saints, and with the blood of the witnesses of Jesus.
And when I saw her, I wondered greatly. And the angel said to me,
“Why do you wonder? I shall tell you the mystery of the woman and.
of the beast that carries her, which has the seven heads and the ten
horns. The beast that you saw was and is not, and is about to come
up out of the abyss and to go to destruction. And those who dwell on
the earth will wonder, whose name has not been written in the book
of life from the foundation of the world, when they see the beast, that
he was and is not and will come. Here is the mind which has wisdom.
The seven heads are seven mountains on which the woman sits, and
they are seven kings; five have fallen, one is, the other has not yet
come; and when he comes, he must remain a little while. And the
beast which was and is not, is himself also an eighth, and is one of the
seven, and he goes to destruction.”

The particularly significant statement in this section is found in
verses 9 and 10: “Here is the mind which has wisdom. The seven
heads are seven mountains on which the woman sits, and they are seven
kings; jive have fallen, one is, the other has not yet come; and when he
comes, he must remain a little while.”

The Hermeneutical Problem
Unfortunately, an alleged difficulty seems to plague interpreters

146

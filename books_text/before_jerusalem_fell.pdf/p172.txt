158 BEFORE JERUSALEM FELL

Theophilus of Antioch lived c. A.D. 115 to 181.49 He wrote:
“Afterwards those who are called emperors began in this order: first,
Caius Julius ..., then Augustus.”5°

Other later sources (and thus less significant) also concur in
beginning with Julius Caesar. Moses Stuart lists the following wit-
nesses: The Chronicon Paschale (dated about 400), Georgius Syncellus
in his Chronography (about 800), and Nicephorus Patriarch (about
824) in his Compend of Chronography |

From the above considerations we are justified in viewing the
kings list of Revelation 17 as indicating the line of Roman emperors
as beginning with Julius Caesar. Consequently, the count of the
emperors into the first century is as follows:

1, Julius Caesar (49-44 B. C.)

2, Augustus (31 B. C.-A.D. 14)

3. Tiberius (A.D. 14-37)

4, Gaius, also known as Caligula (A.D. 37-41)
5. Claudius (A.D. 41-54)

6. Nero (A.D. 54-68)

7. Galba (A.D. 68-69) «

8. Otho (A.D. 69)

9. Vitellius (A.D. 69)

10, Vespasian (A.D. 69-79)

Revelation 17:10 says: “They are seven kings; five have fallen,
[ie., Julius, Augustus, Tiberius, Caligula, and Claudius], one is [ie.,
Nero], the other has of yet come; and when he comes, he must remain
a little while [ie., Galba reigned from June, 68 to January, 69] “ It
seems indisputably clear that the book of Revelation must be dated
in the reign of Nero Caesar, and consequently before his death in June,
A.D. 68. He is the sixth king; the short-lived rule of the seventh king
(Galba) “has not yet come.”

In addition to all the foregoing, it would seem unreasonable to

exclude Julius from the list in light of the circumstances and subject
matter of the book. As will be shown in a later chapter - and as held

49, ANF 2:87.
50, Ibid., p.120. Theophilus to Antolyaus 2:28.
51. Stuart, Aprcalypse 24-48,

168 BEFORE JERUSALEM FELL

proceedings can signs of literary composition be formed. The trouble

is not with the book, but with the prevailing theory of Christian

origins.’3
In another place he comments that “the book is a unity, in no sense
composite. Detailed proof, quite unanswerable, will be found in H.
B. Swete’s Apocalypse of St. John (1906)."1'

Moffatt surveys a number of the leading exponents of the frag-
ment hypothesis who use these two verses (among others) as evidence
for their theories. These scholars argue that Revelation 11:1, 2 was
written prior to the Temple’s destruction and were later incorporated
editorially by a Christian editor into Revelation. Besides himself, he
lists the following names: Weyland, F. Spitta, Pfleiderer, J. Weiss,
C. von Weizsacker, Schon, W. Bousset, A. C. MGiffert, A. Meyer,
Abbott, Baljon, Wiede, P. W. Schmiedel, Calmes, C. A. Briggs,
Erbes, F. Barth, Bruston, K. L. Schmidt, Eugene de Faye, Volter,
O. Holtzmann, Vischer, A. von Harnack, Martineau, Von Soden,
and C. Rausch.!> More recently Kiimmel cites such names as I. T.
Beckwith, A. H. McNeile, C. S. William, H. Windisch, S. Giet, M.
Rissi, de Zwaan, and M. Goguel.'° From a conservative perspective,
which is committed to the inspirational and revelatory character of
Scripture, the higher critical theories created by these men are deemed
woefully ill-conceived in that they operate on anti-supernaturalistic
principles. Nevertheless, the scholars who create them are working
upon real and valid evidence, even though they misconstrue the
nature and function of that historical evidence. We wholeheartedly
concur with Adams’s assessment that the fact that the Temple was
standing when Revelation was written is “unmistakable proof that
Revelation was written before 70 A.D.”!”

Let us then turn to a careful consideration of the passage before
us in order to determine its significance for a pre-A.D. 70 dating for
Revelation. It should be remembered from the introductory state-

18. Charles ©, Torrey, Documents oft/u Primitive Church (New York Harper, 1941), p.
W.
14, Ibid, p. 149,
15, Moffatt, Revelation, pp. 287, 292-298,
16. Werner Georg Kimmel, Introduction to the New Testament, 17th od., trans, Howard
©, Keo, (Nashville Abingdon, 1978), pp. 463-464,

17, Jay B, Adams, The Time Is af Hand (Phillipsburg, NJ: Presbyterian and Reformed,
1966), p. 68.

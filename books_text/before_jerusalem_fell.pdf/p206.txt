192 BEFORE JERUSALEM FELL

The Constitution of the Holy Apostles (c, second half of third century) !'°
6:2:5 (ANF 7:451) and. 6:5:25 (ANF 7:461).

Conclusion

Interestingly, one of the most datable events of ancient history is
the destruction of the Temple in Jerusalem. Christian and pagan
sources alike, as well as archaeological data, point to A.D. 70 very
clearly. The fall of the Temple and of Jerusalem were major events
in the history of not only Judaism but also Christianity. Early Chris-
tians made much of this, employing it as an apologetic datum. It has
been shown that at the time of the writing of Revelation the Temple
complex is spoken of as still standing. It is inconceivable that a book
of the nature of Revelation could fail to mention its already having
been destroyed, if Revelation were written after A-D. 70. This evi-
dence, along with that regarding the reign of the sixth king that
preceded, form unsurpassable barriers to a date post-A.D. 70.

110. The date of The Constitutions of the Holy Apostles is much disputed, Von Drey held
to the date indicated (ANF 7:388), as did Schaif (History 2:185) and Hamack (ANF
7.388).

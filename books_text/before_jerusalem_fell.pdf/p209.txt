The Role of Nero Caesar 195

In Suetonius’s Lives of the Twelve Caesars we have recorded an
interesting cryptogram from the first century. In the midst of his
Latin history, Suetonius records a sample of a Greek lampoon that
was circulated after the burning of Rome: “Nedyng@ov: Népwv iétav
pytépa anéxtewe” (“A calculation new. Nero his mother slew.”)®
It is interesting to note that “the numerical value of the Greek letters
in Nero’s name (1005) is the same as that of the rest of the sentence;
hence we have an equation, Nero= the slayer of one’s own mother.”
An additional example, also employing Nero’s name, can be found.
in the Sibylline Oracles:

One who has fifty as an initial will be commander,

A terrible snake, breathing out grievous war, who one day will lay
hands on his own family and slay them, and throw everything into
confusion,

athlete, charioteer, murderer, one who dares ten thousand things.’

Here Nero’s initial is recorded as possessing the value of 50.
Still another example is found in the Christian Sibylline Oracles
(ce. 150):

Then indeed the son of the great God will come,

incarnate, likened to mortal men on earth,

bearing four vowels, and the consonants in him are two.

I will state explicitly the entire number for you.

For eight units, and equal number of tens in addition to these, and
eight hundreds will reveal the name.?

As the translator notes: “Jesous [Jesus] has a numerical equivalence
of 888,"

A few additional early Christian references showing the alpha-
betic evaluation of numbers can be mentioned. In Barnabas, chapter
9, “Barnabas” derives the name of Christ and the fact of the cross
from the number of men Abraham circumcised in his household. In
his day Irenaeus dealt with certain heresies based on mystic num-

6, Suetonius, Nero 39:2.
7. Suetonius, Lines of the Twelve Caesars, vol, 2, trans, J. C, Rolfe. Loeb Classical
Library (Cambridge Harvard, 1913), p. 158.
8, Sidylline Oracles 5:28-31, In James H, Charlesworth, ¢d,, Old Testament Pseudepigra-
pha, 2 vols (Garden City, NY: Doubleday, 1983) 1:398.
9, Sibplline Oracles 1:324-829;OTP 1:342,
10. Collins, “Sibylline Oracles,” OTP 1:342,

The Role of Nero Caesar 213

Hellenistic] period no addition is needed to convey the sense of a
wild animal to readers. "8° The context of its occurrence in Revelation
13 certainly speaks of a most ferocious creature: “And I saw a beast
coming up out of the sea, having ten horns and seven heads. ...
And the beast which I saw was like a leopard, and his feet were like
those of a bear, and his mouth like the mouth of a lion” (Rev.
13: Ib-2a). Because of its natural association, the term is often quite
aptly used figuratively of persons with “a ‘bestial’ nature, Seast, mon-
ster.”

Now it is almost universally agreed that Nero was one who was
possessed of a “bestial nature.”® Nero often acted in “horrible vi-
ciousness as regards men and women. **! According to Suetonius,
Nero “compelled four hundred senators and six hundred Roman
knights, some of whom were well to do and of unblemished reputa-
tion, to fight in the arena.”®? He was a sodomist (Nero 28) who is said
to have castrated a boy named Sporus and married him (Nero 28,
29). He enjoyed homosexual rape (Nero 28) and torture (Nero 29).
He killed his parents, brother, wife, aunt, and many others close to
him (Nero 33-35). He even “so prostituted his own chastity that after
defiling almost every part of his body, he at last devised a kind of
game, in which, covered with the skin of some wild animal, he was
let loose from a cage and attacked the private parts of men and
women, who were bound to stakes” (Nero 29).

More particularly for Revelation’s purpose, Nero was the first of
the imperial authorities to persecute Christianity, and that with the
vilest evil and most horrendous fury. Tacitus records the scene in
Rome when the persecution of Christians broke out:

So, «dispel the report, [Nero] substituted as the guilty persons and
inflicted unheard-of punishments on those who, detested for their
abominable crimes, were vulgarly called Christians... . And their
death was aggravated with mockeries, insomuch that, wrapped in the

88, Werner Foerster, “Onpiov,” TDNT 3:134.

89, Arndt and Gingrich, Lexicon, p. 861. See their references: Aristophanes, Huites
278, Plutus 439, Nubes 184; Appian; Alciphron 2:17; Achilles Tatius 6:12:8; Josephus,
Wars 1:62, 627; Antiquities 27: 117; 120; Vettius Valens 78:9; Philo, Concerning Abraham 33

90. An almost solitary defender of Nero suggests he was a victim of bad publicity. See
Weigall, Nero.

91, Henderson, Nero, p. 415,

92, Nero 12,

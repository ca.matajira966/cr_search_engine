6 BEFORE JERUSALEM FELL

Regarding the present generation, church historian Timothy P.
Webber has noted that the “resurgence of the interest in prophetic
themes is one of the most significant developments in American
religion since the Second World War. ” ° This fact is evidenced
generally in the rising flood of eschatological literature pouring forth
from Christian publishers. It is evidenced specifically in that one of
the most widely distributed books of the present era is Hal Lindsey's
multi-million selling The Late Great Planet Earth. Lindsey’s work has
been translated into no fewer than 31 languages and circulated in
more than 50 nations. 't While emphasizing Lindsey's role in the
matter, Newsweek magazine reported a few years back that in Ameri-
can religious circles there is a “Boom in Doom.” '° Many Christians
believe that our present era is witnessing “The Great Cosmic Count-
down,”"* Countdown to Armageddon," or Countdown to Rapture.’8 That
is, they believe this era is the last era of earth’s history as we know it,
and is soon to come to a climactic close.

This frenzied interest in biblical prophecy, along with its con-
comitant concern with the book of Revelation, has given no indication
whatsoever of calming. Indeed, the calendar suggests that interest in
prophecy is more likely to increase than to diminish — at least for the

 

Hal Lindsey writes: “What a way to live! With optimism, with anticipation, with
excitement. We should be living like persons who don’t expect to be around much
longer” (The Late Great Planci Earth [Grand Rapids: Zondervan, 1970], p. 145). He also
writes later that “I don’ t like cliches but I've heard it said, ‘God didn’t send me to clean
the fish bowl, he sent me to fish,’ In a way there’s a truth to that” (The Great Cosmic
Countdown,” Eternity, Jan, 1977, p. 21).

Norman Geisler argues vehemently that “The premillennial [that is, dispensa-
tional — KLG] position sees no obligation to make distinctly Christian laws” (“A Pre-
millennial View of Law and Government,” Moody Monthly, Oct. 1985, p. 128).

Because of such statements, we sadly must agree with Pannenbergian theologian Ted
Peters when he says of dispensationalism, “it functions to justify social irresponsibility,”
and many “find this doctrine a comfort in their lethargy” (Futures: Human and Divine
[Atlanta: John Knox, 1978], pp. 28, 29).

18, Timothy P, Webber, The Future Explored (Wheaton: Victory, 1978), p. 9. Ted
Peters observes: “Our Western civilization has long been imbued with a general orienta-
tion toward the future; and the present period is witnessing an especially acute epidemic
of future consciousness” (Peters, Futures, p. 11).

14, Hal Lindsey, The 1980s: Countdown to Armageddon (New York: Bantam, 1980), p. 4

15, Kenneth L. Woodward, “The Boom in Doom,” Newsweek, 10 Jan. 1977, p. 49.

16. Stephen Board, “The Great Cosmic Countdown,” Ztermity, Jan. 1977, pp. 19tf.

17, Lindsey, Countdaon to Armageddon,

18. Salem Kirban, Countdown to Rapture (irvine, CA: Harvest House, 1977).

 
 

218 BEFORE JERUSALEM FELL

anceand the faith of the saints (Rev. 13:10).

And he deceives those who dwell on the earth because of the signs
which it was given him to perform in the presence of the beast telling
those who dwell on the earth to make an image to the beast who had.
the wound of the sword and has come to life (Rev. 13:14).

In the context of speaking of the beast, John gives encouragement to
those whom the beast was presently afflicting:”2 “Here is the perse-
verance and the faith of the saints, “i.e., that the beast who slays by
the sword would also be slain by the sword.

That Nero did in fact kill by the sword (and by many other
means) is well-attested fact. Paul, for example, is said to have died.
under Nero by decapitation by means of the sword.113 Tertullian
credits “Nero’s cruel sword” as providing the martyr’s blood as seed.
for the church. '* Just as well-attested is the fact of Nero’s own death
by sword. According to Suetonius, he “drove a dagger into his throat,
aided by Epaphroditus, his private secretary.”15 He not only killed
others by the sword, but himself, as Revelation mentions.

Again, this evidence alone cannot compel the conclusion that
Nero is in mind; many emperors died by the sword, even Domitian.
But it quite harmoniously lends its voice to the chorus of other
evidences, both major and minor.

Conclusion

The role of Nero Caesar in Revelation is written large. As all
roads lead to Rome, so do they all terminate at Nero Caesar’s palace.
The factors pointing to Nero in Revelation are numerous and varied,
including even intricate and subtle minutiae. It is difficult to discount
the many ways in which Nero fits the expectations of Revelation. He
is the only contemporary historical figure that can possibly fulfill all
of the requirements. Contrary to Swete, Mounce, and others who
fear that the key to Revelation’s “666” is lost, we suggest that the key
is actually in the keyhole.

112, John himself currently was exiled to Patmos while under “the tribulation” (Rw.
1:9). The beast was destined to die in the future (Rev. 13:10): “he must be killed by the
sword.” This was to be soon after the Revelation was written (Rw. 1:1, 8, 19; 22:6ff).

118, Eusebius, Ecclesiastical History 2:25:5; Tertullian, The Exclusion of Heretics 36,

114, Tertullian, Apology 21,

115, Nero 49:2.

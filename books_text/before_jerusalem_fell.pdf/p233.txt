The Role of Nero Caesar 219

Surely Nero’s specter haunts the pages of Revelation. That being
the case, we have a sure terminus for the book’s time of writing: June,
A.D. 68, the date of Nero’s death. This comports well with all the
other avenues explored thus far.

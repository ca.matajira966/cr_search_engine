224 BEFORE JERUSALEM FELL

forty-four thousand sealed from every tribe of the sons of Israel: from

the tribe of Judah, twelve thousand were sealed, from the tribe of
Reuben twelve thousand, from the tribe of Gad twelve thousand, from

the tribe of Asher twelve thousand, from the tribe of Naphthali twelve

thousand, from the tribe of Manasseh twelve thousand, from the tribe

of Simeon twelve thousand, from the tribe of Levi twelve thousand,

from the tribe of Issachar twelve thousand, from the tribe of Zebulun
twelve thousand, from the tribe of Joseph twelve thousand, from the

tribe of Benjamin, twelve thousand were sealed.

Inarguably, an elevated symbolism is here presented. If nothing else,
the perfect rounding of numbers along with the exact and identical
count in each of the tribes bespeak a symbolic representation. The
number “1000” is frequently used in Scripture as an indefinite, yet
significantly large number (Psa. 90:4; Dan. 7:10; 2 Pet. 3:3; Heb.
12:22),

Yet this symbolism must be founded upon some historical desig-
nation. And, of course, the “twelve tribes of Israel” is the long-
standing historical configuration of the Jewish race. '? In light of this,
it would seem that two possible interpretations easily lend themselves
to consideration: either this number represents the totality of the
Christian Church as the fulfillment of the Jewish hope, '° or it repre-
sents the saved of Jewish lineage. '* In either case the interpretation
most likely supports the early date of Revelation in that Christian
history was at a stage in which either the Church at large was called
by Jewish names or in which the bulk of Christians were Jewish.

Other indicators include the fact that not only are the expressions
of Revelation very Hebraic,!> but some words are even translated
into Hebrew (Rev. 9:11; 16: 16). The Church is pictured under a
symbol strongly expressive of a Judaistic Christianity, as a woman

12, See Gen, 35:22; 46:80; 49; Ex. 1: 1ff; Num. 1; 2; 13:4ff; 26; 34; Deut, 27:11 ff;
33:6f.; Josh, 18-22; Judg. 5; 1 Chron. 2-8; 12:24ff; 27: 16ff.; Eze, 48,

13. E.g., Swete, Revelation, pp. 98-99.

14, Eg,, Victorious, Commentary on. ike Apocalypse, ad. for.

45, “NO book in all the New Testament is so Hebraistic as the Revelation (Moses
Stuart, Commentary on the Apocalypse, 2 vols.[Andover: Allen, Merrill, and Wardwell,
1845] 1:229). Charles even develops a grammar of the language of Revelation, hased on
its Hebraic character (Charles, Revelation, I:cxvii f). Torrey suggests an Aramaic original
for it (Charles C, Torrey, The Apocalypse of John [New Haven: Yale, 1958], p. x). See
earlier discussion in Chap, 12,

 

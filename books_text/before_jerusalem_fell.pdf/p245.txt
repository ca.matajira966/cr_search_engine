The Role of Jewish Christianity 231

Conclusion

The matter seems clear enough: When John wrote Revelation
Christianity’s situation was one in which it was still operating within
Jewish circles and institutions to a very large extent. Its grammatical
peculiarities and cultural allusions are evidently of a strongly Jewish
color. Historically we know that this simply was not the case in the
post-temple era beyond A.D. 70. The cleavage between Judaism and
Christianity was too radical. Hence, this factor of the Sitz im Leben is
indicative of a pre-70 date for Revelation.

 

Gerhard Uhlhorn, The Conflict of Christianity with Heatherism, ed. and trans, Egbert C,
Smyth and C. J, H, Ropes, 2nd ed. (New York: Scribners, 1912), pp. 298-255, Merrill
GC. Tenney, New Testament Times (Grand Rapids: Eerdmans, 1965), pp. 308, 821. G.
Ernest Wright, od. Great People of the Bible and HowThey Lwed (Pleasanwille, NY: Reader's
Digest, 1974), pp. 890, 418-419, Howard Clark Keo, Understanding the New Testament, 4th
ed, (Englewood Cliffs, Nd: Prentice-Hall, 1988), pp. 291ff. J.P, M. Sweet, Revelation.
Westminster Pelican Commentaries (Philadelphia Westminster, 1979), pp. 281. Maurice
Gordon Dametz, The Focal Points of Christian History (New York Catlton,n.d.), p. 26, J.
G. Davies, The Early Church, in B. O, James, od., History of Religion Series (New York: Holt,
Rinehart, Winston, n.4.}, p. 46,

244 BEFORE JERUSALEM FELL

us that the Jewish Christians in Jerusalem escaped the city before it
was too late,*> possibly either at the outset of the War or during one
of its providential lulls. Eusebius records the situation thus:

But the people of the church in Jerusalem had been commanded by
a revelation, vouchsafed to approved men there before the war, to
leave the city and to dwell in a certain town of Peres called Pella. And
when those that believed in Christ had come thither from Jerusalem,
then, as if the royal city of the Jews and the whole land of Judea were
entirely destitute of holy men, the judgment of God at length overtook
those who had committed such outrages against Christ and his apos-
tles, and totally destroyed that generation of impious men.

Although contradicting Eusebius on some minor points, Epiphanies
also records this account of the escape of the Christians from Jerusa-
lem.’ Josephus records a major lull in the War, which would provide
opportunity for escape: when Vespasian was distracted by Rome's
Civil War.8

Revelation 11:1,2

The reference to the treading of the Temple’s courts (Rev. 11:1,
2) will be bypassed, in that it has been treated already .39 We should
be aware, however, of its relevance here as a distinctive and non-
repeatable episode of the Jewish War.

Revelation 14:19-20

The role of the bridle-depth blood in Revelation 14:19-20 is as
fascinating as terrifying: “And the angel swung his sickle to the earth,
and gathered the clusters from the vine of the earth, and threw them
into the great wine press of the wrath of God. And the wine press
was trodden outside the city, and blood came out from the wine
press, up to the horses’ bridles, for a distance of two hundred miles.”

85, Brandon is surely wrong when he asserts that the Jewish church perished in the
conflagration that overtook Jerusalem; 5. G. F, Brandon, 7he Fall of Jerusalem and the
Christian Church: A Study of the Effects of the Jewish Overthrow of A.D. 70 on Christianity
(London: SPCK, 1957), chap. 9. He follows Schwartz, Goth. Nachr. (1907) 1:284,

86. Ecclesiastical History 3:5:3.

37, Epiphanies, Heresies 29:7 and De Mensuris et Ponderibus 15. James J. L, Ratton wen.
argues that Revelation was written for the very purpose of warning the Christians to flee
Jerusalem; The Apocalypse of St. John (London: R, & T, Washbourne, 1912), pp. 3-5.

88. Wars 4:9:2.

89. See Chap. 1],

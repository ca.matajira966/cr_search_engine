246 BEFORE JERUSALEM FELL

(KJV: one talent) each, came down from heaven upon men.” It is
quite impossible that such gargantuan hailstones can be accounted.
for under the most aggravated of meteorological conditions. Yet Josephus
records for us an event so visually and effectually similar that what
he records must be the fulfillment of the Revelational prophecy:

The engines [i.e., catapults], that all the legions had ready prepared.
for them, were admirably contrived; but still more extraordinary ones
belonged to the tenth legion: those that threw darts and those that
threw stones, were more forcible and larger than the rest, by which
they not only repelled the excursions of the Jews, but drove those
away that were upon the walls also. Now, the stones that were cast,
were of the weight of a talent, and were carried two furlongs and
further. The blow they gave was no way to be sustained, not only by
those that stood first in the way, but by those that were beyond them
for a great space. As for the Jews, they at first watched the coming of
the stone, for it was a white colour?

Not only is the size mentioned the same (one talent, Gk: tadava-
atoc), but the boulders thrown by the Roman catapults were white
colored, as are hailstones. Would not the effect of the catapulting
stones be virtually that of a hailstorm of such proportions?

Although there are many other such military evidences along
these lines that could be forwarded, these will suffice to illustrate the
point: Revelation’s prophecies find an impressive fulfillment in almost
literal fashion in the Jewish War. And since Revelation is accepted
by evangelical scholars as canonical and prophetic, these events must
lie in the near future from John’s perspective. Thus, a pre-A.D. 70
date for the composition of Revelation is necessary.+

The Correspondence of Time Frames

Not only are there historical events associated with the Jewish
War that fit nicely with the statements of Revelation, but there are
certain time-frame indications that find a most interesting correspon-
dence with those presented in Revelation. And although these occur
in a highly symbolic book and in symbolic contexts, their literal
time-function should not be discounted as wholly non-historical.

48, Wars 5:6:3.

44, It must be remembered that these evidences are not the sole ones, All the
argumentation given above is to be understood when considering these. These are more
or less supplementary strands,

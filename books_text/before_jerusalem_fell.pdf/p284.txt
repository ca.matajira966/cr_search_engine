The Role of Emperor Worship 271

onwards .“51
Let us turn now to a consideration of the matter from the
perspective of Nero’s reign in particular.

The Evidence of the Emperor
Cult in Nero’s Reign

Nero was surely the most evil Roman emperor of the first century
A.D., excelling both Caligula and Domitian in notoriety. He was also
jealously vain in his proud appreciation of his own artistic talents.”
Surely his character would compel him to take advantage of the
emperor cult to feed his debased nature and vain pretensions. Al-
though there are some who doubt his use of the emperor cult, there
is significant evidence of not only Nero’s endorsement of it, but even
intimations that it may have been a factor (one of several) behind
both the persecution of Christians in Rome in A.D. 64 and the
overthrow of Israel in the Jewish War.

Nero was particularly infatuated with Apollo; he even claimed
“the title ‘Son of Apollos,’ and appeared ostentatiously in this role.”5‘
Seneca, one of young Nero’s tutors and a powerful influence in the
era of Nero’s reign designated the Quinguennium Neronis,” convinced
Nero that he was destined to become the very revelation of Augustus
and of Apollo.* Speaking as Apollo, Seneca praised Nero:

He is like me in much, in form and appearance, in his poetry and
singing and playing. And as the red of morning drives away dark
night, as neither haze nor mist endure before the sun’s rays, as
everything becomes bright when my chariot appears, so it is when
Nero ascends the throne. His golden locks, his fair countenance, shine
like the sun as it breaks through the clouds. Strife, injustice and envy
collapse before him. He restores to the world the golden age. 57

51, Moffatt, Revelation, Pp. 429,

52, Miriam T. Griffin, Nero: The End of a Dynasty (New Haven: Yale, 1984), chaps. 9
and 10,

58, Exg,, ibid, pp. 21540

54, Bo Reicke, The New Testament Era: The World of the Bible from 500 B.C.to A.D. 100,
trans. David E. Green (Philadelphia Fortress, 1968), p. 206.

55, The Emperor Trajan even noted that this era was one superior to any other
governmental era. For an able and enlightening discussion of Seneea’s influence on Nero
and the nature of these five auspicious years, see Henderson, Lif and Prim*pate, chap. 3.

56, Seneca, On Clemency 1:1 :6; Apocolacmtosis (Pumpkinification) 415-85.

57, Ethelbert Stauffer, Christ and the Caesars: Historical Sketches, 8rd ed., trans, K. and
R, Gregor Smith (Philadelphia: Westminster, 1955), p. 52.

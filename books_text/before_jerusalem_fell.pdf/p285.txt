272 BEFORE JERUSALEM FELL

Suetonius remarks of Nero that “since he was acclaimed as the
equal of Apollo in music and of the Sun in driving a chariot, he had
planned to emulate the exploits of Hercules as well.” An inscription
from Athens speaks of him as: “All powerful Nero Caesar Sebastos,
anew Apollo.”5 Nero’s portrait appears on coins as Apollo playing
the lyre. He appears with his head radiating the light of the sun on
copper coins struck in Rome and at Lugdunum: one type has Genius
sacrificing over an altar on the reverse side; another has Apollo
Citharoedus on the reverse.*! As Reicke notes of Nero’s Apollo fasci-
nation: “All this was more than pomp and show: Nero strove with
deadly seriousness to play the role of Augustus and Apollo politically,
the former primarily from 54 to 61, the latter from 62 to 68.”®

As early in his reign as 55 the Senate erected a statue of Nero “on
divine scale in the Temple of Mars at the Forum Augusti...,
thus introducing the cult into the city of Rome.” The statue was the
same size as that of Mars in Mars’s own Temple.”That Nero
actually was worshiped is evident from inscriptions found in Ephesus
in which he is called “Almighty God” and “Saviour.”® Reference to
Nero as “God and Savior” is found in an inscription at Salamis,
Cyprus. In fact, “as his megalomania increased, the tendency to
worship him as ruler of the world became stronger, and in Rome his
features appeared on the colossus of the Sun near the Golden House,
while his head was represented on the coinage with a radiate crown.
Members of the imperial house also began to receive unheard of
honours: . . . Nero deified his child by Poppaea and Poppaea herself
after their deaths. All this was far removed from the modest attitude

58, Suetonius, New 53,

59, Smallwood, Documents, p, 52 (entry #145).

60. C,H. V. Sutherland, Coinage én Roman Imperial Policy, 31 B.C, — A.D. 68 (1950),
Pp. 170, plate 16:6. See also Henderson, Nero, p. 894, Michael Grant, Roman Imperial
Money (New York: Barnes and Noble, 1954) and Grant, Roman History from Coins: Some
Uses of the Imperial Coinage to the Historian (London: Cambridge, 1958).

61, Smallwood, Documents, p, 52 (entries #148-144), A, Momigliano, The Cambridge
Ancient History, vol, 10: The Auguston Empire, 44 B.C, — A.D. 70 (New York: Macmillan,
1980), p. 498,

62, Reicke, New Testament Era, p. 241,

68. Ibid, See Tacitus, Annals 13:81,

64, Robinson, Redating, Dp. 236,

65. Ratton, Apocalypse, p. 48.

66. Smallwood, Documents, p, 142 (entry #142),

 

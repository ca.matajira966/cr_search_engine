The Role of Emperor Worship 273

of Augustus. 67 Indeed, of the imperial development of the emperor
cult it should be noted that “Caligula and Nero, however, abandoned
all reserve, Caligula was pictured on coins with the halo of the sun
god Helios, and Nero was represented as Apollo.” The archaeologi-
cal record evidences that “the emperors, around whose heads, from
the days of Nero onwards, had gilded darting rays in token of their
divine solar ancestry.”© Nero clearly “demanded divine honors while
«+ « sill alive,” 70

In A.D. 66 Tiridates, King of Armenia, approached Nero in
devout and reverential worship, as recorded by Dio Cassius:

Indeed, the proceedings of the conference were not limited to mere
conversations, but a lofty platform had been erected on which were
set images of Nero, and in the presence of the Armenians, Parthians,
and Romans Tiridates approached and paid them reverence; then,
after sacrificing to them and calling them by laudatory names, he took
off the diadem from his head and set it upon them... .

Tiridates publicly fell before Nero seated upon the rostra in the
Forum: “Master, I am the descendant of Arsaces, brother of the kings
Vologaesus and Pacorus, and thy slave. And I have come to thee,
my god, to worship thee as I do Mithras. The destiny thou spinnest
for me shall be mine; for thou art my Fortune and my Fate.””!

Dio notes also the fate of one senator who did not appreciate Nero's
“divine” musical abilities: “Thrasaea was executed because he failed
to appear regularly in the senate, ... and because he never would
listen to the emperor’s singing and lyre-playing, nor sacrifice to
Nero’s Divine Voice as did the rest.””

Stauffer points out the beginning of a new theology of the emperor
cult that was born under Nero:

67, Scullatd, Gracchi to Nero, p. 871. See also Henderson Five Roman Emperors, p. 29.

68, Eduard Lohse, The New Testament Enoironment, trans. John E. Steely (Nashville:
Abingdon, 1976), p. 220. See also Paul Johnson, A History of Christianty (New York
Atheneum, 1979), pp. 6ff.

69, Workman, Persecution, p. 40. See also Cambridge Ancient History 10493,

70. Joseph Ward Swain, The Harper History of Civilization, vol. 1 (New York: Harper,
1958), p. 229,

71. Roman History 62:5:2.

72, Roman History 62:26.

 

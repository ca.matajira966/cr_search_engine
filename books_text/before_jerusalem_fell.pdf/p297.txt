284 BEFORE JERUSALEM FELL

rode as the sun god in his chariot? “Could there be a two-fold
meaning in the concept that the Christians were “sacrificed not on the
altar of public interest, but to satisfy the cruelty of one man”? Were
they in essence being “sacrificed” as on an “altar”?

These and other matters could be developed to fill out the
picture. It should be clear, however, that the emperor cult motif in
Revelation is no stumbling block to a Neronic date for the book.

Conclusion

Despite the vigorous employment of the emperor cult motif in
Revelation as an evidence of its late date by some, the motif does not
demand a post-Neronian date at all. We have seen and late date
advocates even admit that the emperor cult was prevalent from the
times ofdulius Caesar. Its presence can be detected under each of the
forerunners to Nero. To make matters worse for the late date school,
the cult seems especially significant to Nero. Any objection to the
early date of Revelation that involves the emperor cult must be
discounted altogether. In point of historical fact, the emperor cult
motif in Revelation fits well the circumstances demanded by early
date advocacy.

121, Tacitus notes that ‘Nero had offered his own gardens for the spectacle, and
exhibited a circus show, mingling with the crowd, himself dressed as a charicteer or
riding in a chariot” (Annals 154-4),

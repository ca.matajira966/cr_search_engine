The Persecution of Christianity 297

Third, although the matter is still subject to debate, there is
reason to believe that the Neronic persecution extended beyond
Rome and into the provinces. At least there is more suggestive
evidence for this being the case under Nero than under Domitian.
Since Christianity had become a re/igio illicita and the emperor himself
had taken severe measures to suppress it, almost certainly we can
expect that at least by imitation provincial magistrates would engage
themselves in the matter. As late date advocate William Ramsay
suggests: “we conclude that if Tacitus has correctly represented his
authorities, the persecution of Nero, begun for the sake of diverting
popular attention, was continued as a permanent police measure
under the form of a general prosecution of Christians as a sect
dangerous to the public safety... . When Nero had once estab-
lished the principle in Rome, his action served as a precedent in every
province. There is no need to suppose a general edict or formal law.
The precedent would be quoted in every case where a Christian was
accused.”*! Surely it would be the case that “the example set by the
emperor in the capital could hardly be without influence in the
provinces, and would justify the outbreak of popular hatred.”*? Other
competent scholars concur.”

Evidently Pliny’s famous correspondence with Trajan (c. A.D.
113) implies a long-standing imperial proscription of Christianity, a
proscription surely earlier and certainly more severe than Domi-
tian’s.“ Although it once was held by many that Pliny’s correspon-
dence was evidence that the policy of proscribing Christianity was a
new policy of Trajan, this view is “now almost universally aban-
doned.”5"In Pliny’s inquiry to Trajan as to how to treat the Chris-
tians brought before him, he is concerned with a standing legal
proscription.

51, Ramsay, Church in Roman Empire, pp. 241, 245,

52, Schaff, History 1:884,

58, F, J, A. Hart, The First Epistle of St Peter (London: Macmillan, 1898), p. 2;
Henderson, Church in Rome, p. 137; Angus, “Roman Empire,” [SBE 42607; John Lau-
rence von Mosheim, History of Christianity in the First Three Centuries (New York, Converse,
1854) 1; 141f; Moses Stuart, Commentary on the Apocalypse, 2 vols. (Andover: Allen,
Merrill, and Wardwell, 1845) 1:22 Schaff cites Ewald, Renan, C, L. Roth, and
Weiseler as assuming “that Nero condemned and prohibited Christianity as dangerous
to the state” (History 1:884, n, 1),

54, Hort, 1 Peter, p. 2.

55, 8, Angus, “Roman Empire” in SRE 42607,

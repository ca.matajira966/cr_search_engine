The Nero Redivivus Myth B17

made for a view of the relevant passages that avoids all reference to
the Nero Redizizus myth. The earth-shaking events associated with the
death of Nero and the eventual ascension of Vespasian easily fulfill
the prophecies ofJohn. In light of such a plausible view, the objection
against the early date on the basis of the myth must be wholly
removed.

2

THE APPROACH TO THE
QUESTION OF DATING

The Importance of Dating

In several respects Revelation is reminiscent of the Old Testa-
ment book of Daniel: ( 1) Each is a prophetic work. (2) Each was
written by a devout, God-fearing Jew in times of the author's per-
sonal exile and national Jewish distress. (3) Each shares a frequent
and very obvious stylistic similarity. (4) Revelation frequently draws
from Daniel. ! Indeed, Revelation is even recognized as a New Testa-
ment Daniel by some scholars. Mounce observes that “it is the NT
counterpart to the OT apocalyptic book of Daniel.”2

Beyond these significant similarities there are two other related
issues that directly bear upon our major concern. One is that both
have widely disputed dates argued by biblical scholars, dates that
fall into two general classes: “late” and “early.” Whereas liberal
scholars invariably argue for a late date for Daniel (i.e., during the
Maccabean era), almost as invariably do conservatives argue for its
early date (i.e., during the Babylonian exile).3 The division between

1. Swete has observed that “there are certain books which [the author] uses with
especial frequency; more than half his references to the Old Testament belong to the
Psalms, the prophecies of Isaiah and Ezekiel, and the Book of Daniel, and in proportion
to its length the Book of Daniel yields by far the greatest number” (Henry B, Swete,
Commentary om Revelation [Grand Rapids: Kregel, [1906] 19771, pclii).

2, Robert H, Mounce, Tie Book of Revelation, New International Commentary on the
New Testament (Grand Rapids: Eerdmans, 1977), p. 28, Cf. also John F, Walvoord, The
Revelation of Jesus Christ: A Commentary (Chicago: Moody, 1966), p. 122.

8. As per most conservative scholars, for example: C, F, Keil and Franz Delitzsch,
Biblical Commentary on the Book of Daniel, Keil and Delitesch Old Testament Commentaries
(Grand Rapids: Eerdmans, rep. 1975), p. 436; Merrill F, Unger, Introductory Guide to the
Olé Testament (Grand Rapids: Zondervan, 195 1), pp. 394; E. J. Young, The Prophes of
Dane! (Grand Rapide: Eerdmans, 1949), pp. 236; Young, An Introduction to the Old
Testament (Grand Rapids: Eerdmans, 1949), pp. 3608; and R. K. Harrison, Introducton
to the Old Testament (Grand Rapids: Eerdmans, 1969), pp. 111011

W

360 BEFORE JERUSALEM FELL

Doubleday, 1983.

Easton, Burton Scott. “Tribe.” Iniernational Standard Bible Encyclopae-
dia, Vol. 4. Grand Rapids: Eerdmans, (1915).

Foerster, Werner. “@rnpiov” In Theological Dictionary of the New Testa-
ment, edited by Gerhard Kittel. Translated by Geoffrey W. Bro-
miley. Vol. 2. Grand Rapids: Eerdmans, 1964.

Geisler, Norman. “A Premillennial View of Law and Government.”
Moody Monthly, October 1985, 129i.

Ginzberg, Louis. “Apocalypse of Abraham.” The Jewish Encyclopedia.
New York: KTAV, 1953-68, 1:669-675.

Klijn, A. F. J. “2 (Syriac Apocalpyse of) Baruch.” In The Old Testa-
ment Pseudepigrapha, edited by James H. Charlesworth. Vol. 1.
Garden City, NY: Doubleday, 1983.

Lindsey, Hal. “The Great Cosmic Countdown: Hal Lindsey on the
Future.” Eiernity, January 1977.

Masterman, E. W. G. “Jerusalem.” International Standard Bible Encyslo-
paedia. Vol. 3. Grand Rapids: Eerdmans, (1915).

Maurer, Christian. “puds}.” In Theological Dictionary of the New Testa-
ment, edited by Gerhard Kittel. Translated by Geoffery W. Bro-
miley. Vol. 9. Grand Rapids: Eerdmans, 1974.

Milburn, R. L. “The Persecution of Domitian.” Church Quarterly
Review 139 (1944-45): 154-164,

Robertson, O. Palmer. “Tongues: Sign of Covenantal Curse and
Blessing.” Westminster Theological Journal 38 (1975-76):43-53.

Rowlingson, Donald T. In Anglican Theological Review 32 (1950): 1-7.

Rubinkiewicz, R. “Apocalypse of Abraham.” In The Old Testament
Pseudepigrapha, edited by James H. Charlesworth. Vol. 1. Gar-
den City, NY: Doubleday, 1983.

Ruble, Oskar. “dp:uée.” In Theological Dictionary of the New Testa-
ment, edited by Gerhard Kittel. Translated by Geoffrey W. Bro-
miley. Vol. 1. Grand Rapids: Eerdmans, 1964.

Scott, Kenneth. “The Identification of Augustus with Romulus
—Quirinus.” Transactions and Proceedings of the American Philological
Association 46 ( 1930):82-105,

Vilhour, P. “Apocalyptic.” In New Testament Apocrypha, edited by R.
M. Wilson. Vol. 2. London: Lutterworth, 1965.

Woodward, Kenneth L. “The Boom in Doom.” Newsweek, 10 January
197,49.

Wright, G. Ernest, ed. Great People of the Bible and How They Lived.

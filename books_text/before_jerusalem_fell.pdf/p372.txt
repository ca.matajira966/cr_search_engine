362 BEFORE JERUSALEM FELL

Bigg, Charles. The Origins of Christianity. Edited by T. B. Strong.
Oxford: Clarendon, 1909,

Blass, F., and A. Debrunner. A Greek Grammar of the New Testament and
Other Early Christian Literature. Translated by Robert W. Funk.
Chicago: University of Chicago Press, 1961.

Bleek, Friedrich. An Introduction to the New Testament. Vol. 2. Trans-
lated by William Urwick. Edinburgh: T. & T. Clark, 1870.

Box, G. H. The Ezra-A pocalypse. London: Pitrnan, 1912.

—____., and J. I, Landsman. The Apocalypse of Abraham.
London: Pitman, 1918,

Bovan, M. J. Revue de Theologie et de Philosophic. Lausanne: 1887.

Brandon, 8. G. F. The Fall of Jerusalem and the Christian Church: A Study
of the Effects of the Jawish Overthrow of A .D. 70 on Christianity. London:
SPCK, 1957.

—_____. Jesus and the Zealots: A Study of the Political Factor in:
Primitive Christianity. New York: Scribners, 1967.

Bright, John. The Kingdom of God. Nashville: Abingdon, 1963.

Bruce, F. F. The Books and the Parchments. 3rd ed. Westwood, Nu:
Revell, 1963,

—_—_____ . New Testament History. Garden City, NY: Anchor
Books, 1969.

Burrows, Miller. What Mean These Stones? New Haven: American
Schools of Oriental Research, 1941.

Butterworth, G. W. Clement of Alexandria. London: William Heine-
mann, 1919.

Barrington, Philip. The Meaning of the Bible, London: SPCK, 1931.

Carron, T. W. The ‘Christian Testimony Through the Ages. London:
Pickering & Inglis, n.d.

Cartledge, Samuel A. A Conservative Introduction to the New Testament.
Grand Rapids: Zondervan, 1938.

Gary, M. A History of Rome Down io the Reign of Constantine, 2nd ed.
New York: St. Martin’s, 1967.

Chilton, David. Paradise Restored: A Biblical Theology of Dominion.
Tyler, TX: Reconstruction Press, 1985.

Coggins, R. J., and M. A. Knibb. The First and Second Books of Esdras.
The Cambridge Bible Commentary on the New English Bible.
London: Cambridge University, 1979.

Connick, C. Mile. The New Testament: An Introduction to Its History,
Literature, and Thought. Belmont, CA: Dickenson, 1972.

368 BEFORE JERUSALEM FELL

Baker, (1868) 1953.

—_______, and J. R. Harmer. The Apostolic Fathers. Grand.
Rapids: Baker, (1891 ) 1984,

Lindsey, Hal, The Late Great Planet Earth. Grand Rapids: Zondervan,
1970.

. There’s a New World Coming. Santa Ana, CA: Vision.
House, 1973.

Lohse, Eduard. The New Testament Environment. Translated by John
E. Steely. Nashville: Abingdon, 1976.

Luther, Martin. Luther's Works. Edited by Jaroslav Pelikan. St. Louis:
Concordia, 1957.

Macdonald, James M. The Life and Writings of St. John. London:
Hodder & Stoughton, 1877.

Mahan, Mile. A Church Hisiory of the First Sewn Centuries to the Close of
the Sixth General Council. 8rd ed. New York: E. and J. B. Young,
1892,

Marshall, Alfred. The Interlinear Greek-English New Testament. Ind ed.
Grand Rapids: Zondervan, 1959.

McHale, John. The Future of the Future. New York: George Braziller,
1969.

McNeile, A. H. An Introduction to the Study of the New Testament. Revised
by C. S. C, Williams. 2nd ed. Oxford: Clarendon, 1953.

McSorley, Joseph. An Outline of the History of the Church@ Centuries. 9th
ed. St. Louis: B. Herder, 1954.

Meinardus, Otto F. A. St. Paul in Ephesus and the Cities of Galatiaand
Cyprus. New Rochelle, NY: Caratzas Bros., 1979.

Metzger, Bruce Manning. A Textual Commentary on the Greek New
Testament. London: United Bible Societies, 1971.

—____ . The Text of the New Testament. 2nd ed. New York:
Oxford University Press, 1968.

Mickelsen, A. Berkeley. Interpreting the Bible. Grand Rapids: Eerd-
mans, 1963.

Milik, J. T., and R. De Vaux. Discoveries in the Judean Desert of Jordan,
Vol. 2. Oxford: Oxford University Press, 1961.

Miller, Andrew. Church History from the First to the Twentieth Centuries.
2nd ed. Grand Rapids: Zondervan, (1873) 1964.

Milman, Henry Hart. History of the Jaws. Vol. 2. New York: E. P.
Dutton, (1909) 1943.

Moffatt, James, An Introduction to the Literature of the New Testament. 3

372 BEFORE JERUSALEM FELL

Terry, Milton S. Biblical Apocalyptics. New York: Eaton and Mains,
1898,
Biblical Hermeneutics. Grand Rapids: Zondervan,
(1883) 1974, “
Thackeray, St. John. Josephus: The Man and the Historian. London:
1924,
Thayer, Joseph Henry, cd., Greek-English Lexicon of the New Testament.
New York: American Book, 1889.
Thiessen, Henry C. Introduction to the New Testament. Grand Rapids:
Eerdmans, 1943.
Theobald, Robert. Beyond Despair. Washington: New Republic Book
Co., 1976.
Toffler, Alvin. Future Shock. Toronto: Bantam, 1970.
Torrey, Charles Cutler. Documents of the Primitive Church. New York:
Harper, 1941,
—___ . The Four Gospels. 2nd ed. New York: Harper, 1947.
Uhlhorn, Gerhard. The Conflict of Christianity with Heathenism. 2nd ed.
Edited and translated by Egbert C. Smyth and C. J. H. Ropes.
New York: Scribners, 1912.
Unger, Merrill F. Introductory Guide to the Old Testament. Grand Rapids:
Zondervan, 1951.
Van Antwerp, David D. Church History. 5th ed. Vol. 1. New York:
James Pott, 1884,
. The Principles of Church History. 3rd ed. Vol. 1. Balti-
more: George Lycett, n.d.
Vanderwaal, Cornelis. Hal Lindsey and Biblical Prophecy. St. Catha-
rine’s, Ontario: Paideia, 1978.
Vincent, Marvin R. Word Studies in the New Testament, Vol.2: The
Writings of John. Grand Rapids: Eerdmans, (1887) 1985.
Von Rad, Gerhard. Theology of the Old Testament. Vol. 2. English
translation. Edinburgh: Oliver and Boyd, 1965.

Wace, Henry, and William Smith, eds. Dictionary of Christian Biogra-
phy, Literature, Sects, and Doctrines. Boston: Little, Brown, 1877-88.
Walker, Williston. A History of the Christian Church, 3rd ed. New York:

Scribners, 1970.
Webber, Timothy P. The Future Explored. Wheaton: Victory, 1978.
Weigall, Arthur. Nero: Emperor of Rome, London: Butterworth, 1933.
Weiss, Bernhard. A Manual of Introduction to the New Testament. Trans-

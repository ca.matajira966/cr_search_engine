Subject Index 407

city, 66, 72, 75, 77, 82, 91, 94, 95, 100,
101, 119, 148, 150, 152, 164, 171,
178, 187, 258n, 254,266, 272,275,
290,299.

civil war, 19, 116, 144, 161, 180, 240,
242,244,311-814,880, 885.

emperor(a), 18, 56, 67,69, 77,96, 97,
100, 144, 152, 154-159, 161, 168,
186, 217,285,242, 258n, 254,277,
302,308,

empire, 15, 100, 116, 182, 148, 145,
150, 184, 162,206, 210, 280,241-
242,252,270, 280n, 296.

fall of, 25, 141, 240,

league with Israel, 155, 241 n, 281.

legions, 286-288, 248,

law against. associations in,

penology, 101,123n, 160, 227n, 286,
291,

revival after civil war, 913-816,

Senate, 70, 78, 144, 265,266,269,
272,802,814.

senator(s), 72, 218, 275,

Romulus, 265.

Sacrifice(s, -cd), 78,97, 148, 176,266,
272,278,276,280>282, 284,292,309,
Jewish for Rome, 250n, 281-282.

Sadducees, 284,

Saint(s), 11, 118,218, 240n, 254,276,
277 285.

Sanctification, 338,

Sardis, 326-829,

Satan (See aiso: devil), 201,210, 215, 222,
225,247,

Savior, 268, 272, 274,350.

Scorpion(s), 248.

Scripture (See: Bible).

Sea (ocean), 74, 75, 218,282,288,

Seal(s) (noun), 107,282,241,248,

Seal (verb), 135n, 140,282,236,

Sebastos (sebastenci), 74,272.

Second Advent/Coming (See: Jesus
Christ - coming).

Seer (See: Prophet).

Semitic (See: Hebrew language).

Senate of Rome (See: Rome — senate).
Seneca, 271,
Sensationalism, 7,8.
Septimus Severus (emperor), 77.
Septimontium, 149,
Septuagint (LXX), 61, 118, 188.
Serpent (See: snake).
Seven, 23n, 158, 162, 168, 209, 258,
churches (See also: individizal entries),
15,94, 100, 115, 118, 122, 189,
150, 158, 162,222,260,818-880.

cities of Revelation (See: individual
entries).

heads (See: Beast — heads).

kings (See: Kings — seven),

letters, 212,269,818-880, 348,

mountains/hills, 146, 149-151, 164,

254,
Seven hundred, seventy-seven, 204,
Seventy weeks of Daniel, 185n.
Shortly (See: Revelation —
expectation),
Sin, 135n, 285,
Sinaiticus (See: Codex Sinaiticus).
Sion (Bee: Zion).
Sitzim L-hen, 189,220, 230,
Six, 204,208.
Six hundred, sixteen (See also: numbers
— symbolic use of), 196-198.
Six hundred, sixty-six (See aiso: numbers
— aymbolic use of), 198-212,
impossibility of interpretation, 208n,
205.

Irenaeus on, 46-47,51, 197,208,
205-208.

Nero theory of, 198-201, 215,217,218,
810-816 852,

Nero theory objections, 208-212,
Sixth king (Sec: Kings — sixth king).
Slaughter, 188,

Slave(r¥), 89, 285, 266n, 807,812.

Smyrna, 228,267,269.
church in, 45n, 57,62, 222, 848,
founding of church in, 822-826.
late-date argument regarding,

922-828,

a4 BEFORE JERUSALEM FELL

Hermann Gebhardt, The Doctrine of the Apocalypse, trans. John Jeffer-
son (Edinburgh: T. & T. Clark, 1878).

dames Glasgow, The Apocalypse Translated and Expounded (Edinburgh:
1872).

Robert McQueen Grant, A Historical Introduction to the New Testament
(New York: Harper & Row, 1963), p. 237.

James Comper Gray, in Gray and Adams’ Bible Commentary, vol. V
(Grand Rapids: Zondervan, [1903] rep. n.d.).

Samuel G. Green, A Handbook of Church History from the Apostolic Era

to the Dawn of the Reformation (London: Religious Tract Society,
1904), p. 64.

Hugo Grotius, Annotations in Apocalypse (Paris: 1644). [2, 6]

Heinrich Ernst Ferdinand Guerike, Introduction to the New Testament
(1843); and Manual of Church History, trans, W. G. T. Shedd
(Boston: Halliday, 1874), p. 68. [1,3]

Henry Melville Gwatkin, Early Church History to A.D. 313, vol. 1
(London: Macmillan), p. 81.

Henry Hammond, Paraphrase and Annotation upon the N. T. (London:
1653). [2]

Harbuig (1780). [6]
Harduin. [2]
Harenberg, Erklérung ( 1759). [1]

H. G. Hartwig, Apologie Der Apocalypse Wider Falschen Tadel Und
Falsches (Frieberg: 1783). [1]

Karl August von Hase, A History of the Christian Church, 7th cd., trans.
Charles E, Blumenthal and Conway P. Wing (New York: Ap-
pleston, 1878), p. 33."

Hausrath. [1]

Bernard W. Henderson, The Life and Prim-paie of the Emperor Nero
(London: Methuen, 1903).

Hentenius. [2]

54, Cited in D, A. Hayes, John and His Writing (New York Methodist Book Concern,
1917), p. 246,

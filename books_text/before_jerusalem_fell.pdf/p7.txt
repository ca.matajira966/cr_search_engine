TABLE OF CONTENTS

Publisher’s Preface by Gary North... 0... oe ix
Preface 0 xvii

PART I: PRELIMINARY CONSIDERATIONS

 

 

 

J. Revelation Studies 2 6 0. 3
2. The Approach to the Question of Dating........ 17
PART II: THE EXTERNAL EVIDENCE
3. Introduction to the External Evidence ......... 41
4, Irenaeus, Bishop of Lyons. . 6... ee 45
5. Clement of Alexandria... 66. eee 68
6. Additional External Witnesses 6.6... eee 86
PART III: THE INTERNAL EVIDENCE
7. The Role of Internal Evidence... 0... 000005 113
8. The Theme of Revelation... 0.0 es 121
9. The Temporal Expectation of the Author ........ 133
10. The Identity of the Sixth King. .........000. 146
11. The Contemporary Integrity of the Temple....... 165
12. The Role of Nero Caesar. 6. 0. ee te 193
13. The Role of Jewish Christianity... ......0.00.% 220
14, The Looming Jewish War... « 232

PART IV: ALLEGED DOMITIANIC EVIDENCES EXAMINED

15. Introduction to Domitianic Evidences .......... 259
16. The Role of Emperor Worship .... 0.0.0.0 06 261
17. The Persecution of Christianity... 6.0... 0000s 285

18. The Nero Redivivus Myth.......
19. The Condition of the Seven Churches...

    

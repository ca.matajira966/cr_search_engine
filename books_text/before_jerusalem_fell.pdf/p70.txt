b4 BEFORE JERUSALEM FELL

the re-interpretation on the grounds that “the ancients clearly under-
stood the matter” along the lines of the common interpretation.“
Robinson points out two problems that appear to him to be fatal to
the re-interpretation of Irenaeus. 35 The first is that the Latin transla-
tion of Irenaeus stands against it by its use of visum (which better
suggests a thing, such as a book), instead of visa (which is more
suggestive of a person). This argument is closely related to Stuart’s.
The second is that Irenaeus twice elsewhere says John lived to
Trajan’s reign, not just to Domitian’s.* If Irenaeus is to be re-
interpreted here along the lines of Chase and others then there would
seem to be some confusion in Irenacus’s record.

In response to these three objections, we offer the following
explanations. First, regarding Stuart’s statement that the early fa-
thers seemed to have understood him in terms of the common inter-
pretation, it should be noted that although many ancient fathers
employed Irenaeus with high regard, they do not seem to have
regarded him as a final authority. For instance, contrary to Irenaeus,
Tertullian placed John’s banishment after his being dipped in a
cauldron of burning oil, which Jerome says was in Nero’s reign.”
Photus preserved extracts of “Life of Timotheus” in which he states
that John’s banishment was under Nero. Others who record a pre-
Domitianic date for John’s banishment include: Epiphanies (Heresies
51:12, 33), Arethas (Revelation 7:1-8), the Syriac versions of Revela-
tion, History ofJohn, the Son of Zebedee, and Theophylact (John). Though
Eusebius quotes Irenaeus as proof of the date to which John lived.
{i.e., into the reign of Trajan) ,38 he disagrees with Irenaeus as to the
dohannine authorship of Revelation. 39 In light of all this “We cannot

 

I cannot think that any other Nominative than ‘Amoxddvyntc can be fairly supplied.
here.” See Macdonald’s statement as to Guericke’s initial acceptance of the argument
followed by his later retraction of his endorsement, Lie and Writings, p. 169. Robinson,
Redating, pp. 221 ff.

84, Stuart, Apocalypse 1:265.

85, Robinson, Redating, pp. 221 -222n,

86, Against Heresies 2:22:5;3:3:4.

37. See Tertullian, On the Exclusion of Heretics 36; ep. Jerome, Against Jovinianum 1:26,

8B, Ecclesiastical History 3:23:3, citing Against Heresies 2:22:5.

39, In his Ecclesiastical History (7:25: 16) Eusebius denies what Irenaeus clearly affirms,
that the Apostle John wrote Revelation: “But I think that he wassome other one of those
in Asia; as they say that there are two monuments in Ephesus, each bearing the name
ofJohn, *

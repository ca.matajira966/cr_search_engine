Frenaeus, Bishop of Lyons 67

John could have suffered twice, under both Nero and Domitian. This
certainly could account for Irenaeus’s confusion. (3) Also it should.
be remembered that Irenaeus was at Lyons when he wrote — quite
far away from ecclesiastical tradition. Stuart comments in this regard:
‘T say this, with full recognition of the weight and value of Irenaeus’s
testimony, as to any matters of fact with which he was acquainted,
or as to the common tradition of the churches. But in view of what
Origen has said. . . , how can we well suppose, that the opinion of
Irenaeus, as recorded in Cont. Haeres. V. 30 was formed in any other
way, than by his own interpretation of Rev. 1:9. 78

A careful scrutiny of the Irenaean evidence for a late date for
Revelation tends to render any confident employment of him suspect.
The difficulties with Irenaeus in this matter are many and varied,
whether or not his witness is accepted as credible. A bold “thus saith
Trenaeus,” cannot be conclusive of the matter.

86, Stuart, Apocalypse 1:281.

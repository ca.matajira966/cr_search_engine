10° ACKNOWLEDGMENTS

in English. At least, not in the Engtish that is generally passed off as
such im our day of triviality and mundanity. He writes in an English
that literally dances in delight.

Suzanne Martin’s technical virtuosity is matched only by her
cthical passion. There is probably not another soul on earth who
could have, or would have, transliterated my yellow-pad-chicken-
scratchings into readable type. And there is probably not another soul
on earth who would have, or could have, served in thankless tasks so
long, so well.

The list could go on and on (it is for this very reason that I can
never bring myself to actually watch the Oscars or the Grammys). To
the BF pioneers who launched HELP, many thanks: to Holly and
Johnny for Del and a kitchen; to Mr. and Mrs. B, for gumbo and a
haven; to CB for lug wrenches and laughs; to Mike and Riso, for
music and stamina; to Barb and Ed, for CWV and a start: to John and
Terrie, for quiet zeal and selflessness; to Lyn and Ruth, for loving
kindness and Spurgeon: to Shelley and Allen, for steadfast vision and
vitamins; to Kathe, for bold faith and hope; and, to Kelly and Karen,
for gumption and lawnmowers. It is a glorious privilege to count you
as my friends.

Any number of people contributed time, energy, and ideas to this
manuscript as it took shape. To these friends and colleagues, I, again,
tender my thanks: Lynn Nelson, Gary North, Franky Schaeffer, Brian
and Lanita Martin, Frank Marshall, Doug DeGeus, Jim Jordan,
Bobby Saxon, Maureen Griffin, Harley Belew, Gary DeMar, Ralph
Barker, and Steve Schiffman.

Finally, I am grateful to my wife, Karen, to whom this book is
dedicated: “grow strong, my love and my life, that you may stand
unshaken when I fall, that I may know that the shattered fragments of
my song will come at last to finer melody in you.”

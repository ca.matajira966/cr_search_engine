106 BRINGING IN THE SHEAVES

nate, the youth, etc., are central, the performing and graphic arts
must by no means be ignored. Nor must we slight newsletters, books,
video tapes, audio cassettes, films, radio broadcasts, data basing, and
cable television.

Obviously, these suggestions only touch upon the many and
various ways that congregations can be motivated to take action on
behalf of the poor. In fact, no matter how many pages might be
devoted to the subject, we could no more exhaust the possibilities than
we could drain the deep. But the point is, and hopefully it is a point
well taken, any and every means the church has at its disposal must be
dispatched to the end of stirring up families with a zeal to flesh out the
Good Samaritan faith.

Biblical charity requires an army. A couple of people here and a
couple there simply won’t cut it. Our objective is to supplant entirely
the federal welfare folly with genuine Scriptural forms. But it will
take the framework of entire congregations, entire families, a whole
host of dedicated, committed believers to do it.

Nothing short of a comprehensive missions strategy, encom-
passing every aspect of church life, can hope to enlist that kind of
response.

Tf, on the other hand, we are unwilling to make the sacrificial
effort necessary to motivate our congregations and ultimately to roll
back the debilitating effects of welfare by equipping the poor through
Biblical charity, we'd better admit it. We'd better stop complaining
about the federal dole, “if not out of a sense of decency,” says Tom
Landess, “at least out of a healthy regard for the vicissitudes of
modern industrial life and the fickleness of the electorate.” That way,
“if we run across a battered and penniless stranger while travelling
from Jerusalem to Jericho, we won’t have to stop and help him
ourselves. We can just call the appropriate agency and tell the bureau-
crats where along the road to look for the body.”’? We can then wash
our Levitical hands clean of blood-guilt and scamper on our merry
way.

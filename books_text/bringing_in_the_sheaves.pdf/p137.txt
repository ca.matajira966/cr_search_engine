FACILITATING PRIVATE INITIATIVE — 137

by sclflessness and sacrifice.

Modern examples of Nehemiah-like charity pioneers may be
few and far between, but they do exist. And where they exist, whether
in the United States, India, or Ethiopia, God has blessed their

faithfulness with success and great favor.

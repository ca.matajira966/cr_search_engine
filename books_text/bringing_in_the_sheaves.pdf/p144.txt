144 BRINGING IN THE SHEAVES

against the resources of the caring community. It must discover what
kinds of help are most needed.

Stop, Look, and Listen

So, how do you go about discovering and identifying needs in the
community? How do you go about properly focusing the Good
Samaritan faith for maximum effect’?

The answer is so simple, so obvious, that it is invariably over-
looked: know your community. Look about. Examine the highways
and byways of your area with new eyes of awareness and discernment.
Do your homework.

What is the local unemployment rate? Is it rising or falling?

Are there dilapidated Torinos and Bonnevilles loaded to the hilt
with the tattered remnants of precious possessions dotting the road-
sides?

Are there fire-lit camps scattered about the fringes of your town:
under bridges, along the river, or beside the lake?

Do abandoned warehouses give sanctuary to the dispossessed
against the night?

Are the public shelters, soup kitchens, and rescue missions filled
to overflowing?

Have the newspaper want-ads shrunk from a thick bundle to a
few truncated notes tacked to the end of the business section?

How many retail failures have marred the glittering track record
of your local Chamber of Commerce?

What is the vacancy rate at the various apartment mega-
complexes in town? And how busy has the constable been in enforc-
ing evictions?

Know all the whos. whats, whens, wheres, and whys of your
community. Talk to people. Find out what’s going on at the social
service agencies that maintain nearby offices. Swap stories with other
churches. Read the local news. In short, stop, look, and listen.

Every community is different. In order to be effective, there-

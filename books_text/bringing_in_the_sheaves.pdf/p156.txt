156 BRINGING IN THE SHEAVES

motivated, and the most active members of a congregation. If the
information you need is going to be gathered at all, then the task force
must do the gathering.

Second, the task force would be responsible to keep the rest of
the church informed. When you have sorted through all the data and
can clearly identify needs in the community, then let the church know.
Motivate them. Stir them to pathos. Educate them. Invite them to
attend a special presentation where the basics of Biblical charity are
outlined, Offer a series of weekend training sessions where the
principles of Scripture and the lessons of history are applied to local
conditions and crises. A long-term, comprehensive program of Bibli-
cal charity cannot hope to get off the ground until the task force rallies
the support of the rest of the church.

Third, the task force would be responsible to map out strategics
and tactics for the rest of the church to follow. The task force is
composed of the Biblical charity pioneers in any given body. Thus,
they must give form and leadcrship ta the benevolent outreaches of
the church. Specific proposals from the task force should be peri-
odically presented to the pastor, the elders, or the deacons. Goals,
agendas, and priorities will need to be developed. Cost projections,
man-hour requirements, and facility needs also will have to be calcu-
lated. Make certain that all the bases are covered. There is no reason
in the world why privation should be perpetuated in our communities
just because we weren’l prepared or organized enough and, thus,
were bureaucratically stymicd.

Fourth, the task force would be responsible for mobilizing
families for compassionate action, It is not enough simply to inform
and train families in the congregation. It is not enough to lay out an
intricate mosaic of plans, strategies, tactics, and priorities. At the
bottom line, implementation is the name of the game. When
emergencies arise, the task force must be able to spring into action,
and to stimulate others to do likewise. Ultimately, it really doesn’t
matter that we know all the arguments against federal welfare and all

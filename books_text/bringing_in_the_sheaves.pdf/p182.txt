A compassionate heart feels for the emotionally, spiritually,
psychologically, and physically sick. And such a heart finds
ways to express those feelings. The expression will be practical,
tangible, edible.

John Mosqueda

 

“You sharin’ with us, Muley Graves?”
Muley fidgeted in embarrassment. “'I ain't got no choice
in the matter.” He stopped on the ungracious sound of his words.
“That ain’t like | mean it. That ain't... .I mean... ,” he
stumbled, ‘“What I mean is, if a fella’s got somepin’ to eat
an’ another fella’s hungry . . . why, the first fella ain’t got
no choice.”

John Steinbeck

 

To the work! To the work! Let the hungry be fed,
To the fountain of life let the weary be led;
In the cross and its banner our glory shall be
While we herald the tidings that sets the captives free.

Toiling on, toiling on,
Let us hope and trust,
Let us watch and pray
And labor till the Master comes.

To the work! To the work! We are servants of God.
Let us follow the path that our Master has trod;
With the balm of His counsel our strength to renew,
Let us do with all our might what our hands find to do.
Fanny Crosby

 

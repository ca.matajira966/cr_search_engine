LOAVES AND FISHES 189

particular food item over the course of a few months. One church in
Baltimore has a food sign-up sheet. Instead of families donating altar
flowers week by week, they can donate food for the pantry. Another
church in San Dicgo divides their restocking responsibility between
Sunday School classes.

With each family in the church supplying just a few items at a
time, a food pantry ministry wil] have no trouble whatsoever in
maintaining adequate levels of non-perishables.

Second, a garden plot can be set aside each summer so that the
pantry can be stocked with fresh vegetables. In Pittsburgh, a commu-
nity garden yielded about thirty-five tons of vegetables for the poor
and unemployed. “We could’ve done better,” said garden director
Jeff Gerson, “but the weeds did us in!" In the [ast major harvest of the
season, about 10,000 pounds of produce was picked by 500 volun-
teers. The vegetables were delivered to the Pittsburgh Community
Food Bank where they were then distributed to 750 charitable agen-
cies in 22 counties, including 300 church food pantries in the Pitts-
burgh area. Allegheny County’s parks department provided eight
acres of Jand for the garden, Mellon Bank and the Western Pennsylva-
nia Conservancy donated seeds and supplies, and volunteers did the
work. “There were a lot of sore backs,” said park spokesman
Michael! Dichl, “and a lot of smiles on the faces of the volunteers.”

Most churches have at least small plots that they could similarly
cultivate and, thus, restock their food pantries with nature’s best!

Third, grocery stores will very often donate dated or damaged
foodstuffs that are edible but unsaleable. Dented cans. day-old bread,
expired dairy products, or slow-moving stock are yours for the asking
at many stores. In Lincoln, Bernie Slavik collects over 500 loaves of
day-old bread each day from a large grocery chain. Needless to say,

his church’s food pantry never lacks for bread. In Duluth, Martin
Epsilos distributes several hundred cases of canned vegetables each
week to three food pantries. “I probably could get more,” he says,
“but neither my back nor my pick-up could handle it.”

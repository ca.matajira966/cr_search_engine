192 BRINGING IN THE SHEAVES

orchestrated, closely administrated food drive. But the headaches that
it eliminates from a church pantry program makes it an administrative
boon.

The Brown Bag Project can climinate waste, encourage con-
gregation-wide participation, stimulate community interest, and
serve the needy in a way that virtually no other ministry could. So
why not get your church in line? Why not start brown-bagging it?

Precautions

Any church that actively involves its families in an emergency food
relief program must cxercise extreme caution in avoiding legal lia-
bility for its compassion. In our overly litigious society, it is vital that
we protect our churches, our families, and ourselves as best we can.

First, since the Biblical charity food pantry operation is an
exchange program — food for work — have each applicant sign a
liability release form before putting him to his task. This standard
administrative chore will enable the ministry to sidestep legal head-
aches and heartaches should anyone ever be injured. A very simple
waiver will do. Just make certain that it is filled out completely,
signed, and dated.

Second, inventory the food stock often, culling out any items
that you even suspect may be spoiled. Even though most states protect
charities with “Good Faith Donor Laws,” there is no sense in flirting
with disaster. Keep the food supplies away from extremes of heat and
cold, and do not distribute anything you would not want to serve to
your own family.

Third, paperwork is the bane of any endeavor, but it is essential,
nonetheless. Make certain that you keep excellent records. Know
who you have served, where they are from, and where they can be
reached. A single standard form on each applicant can be quickly
filled out and filed, so that vital information is never far away. Also,
keep a record of who has donated what. Note dates, times, amounts,
and anything else that you may deem relevant. A systematic account-

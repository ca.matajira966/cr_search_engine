AGENCIES AND ORGANIZATIONS 221

ministry in recent memory.

Humble Evangelicals to Limit Poverty (HELP)
20360 Highway 59 #1120-B
Humble, TX 77338

The work of HELP is diverse and wide-ranging, with a number
of different programs, including:

Samaritan’s Purse is the emergency relief arm of HELP.
By providing food, shelter, and medical care to families in
crisis, the ministry is able not only to fulfill obligations of
Christian compassion, but also to relieve much of the
pressure on civil authorities who find themselves buried
beneath an avalanche of red tape.

The Community Clean Sweep, each summer, involves
not only unemployed adults, but idle teens as well in a
community-wide clean-up campaign. In exchange for
groceries, workers rid city streets of litter, rubbish, and
overgrowth at no cost to the taxpayers.

The HELP Job Search is a free employment posting
service. Jobs from all around the community are gathered
and posted so that anyone serious about climbing out of
poverty’s deep trap will be able to do so.

The Brown Bag Project is a food collection, storage,
inventorying, and distribution system that involves pool-
ing the resources of all area charitable organizations into
one unified front, thus eliminating waste, overlap, and
hassle.

238 BRINGING IN THE SHEAVES

37 Thid., p. 112.

38 bid.

39 Ibid.

40 Walter E. Williams, The State Against Blacks (New York, NY: McGraw-Hill,
1982),

41 Thomas Sowell, Civil Rights: Rhetoric or Reality? (New York: William
Morrow and Co., 1984).

42 Social Services Handbook and Survey (Houston, TX: Humble Evangelicals
to Limit Poverty, 1984),

43 New York Times, April 20, 1967.

44 William Simon, A Time for Truth (New York, NY: Berkley, 1979), p. 35.

45 Marlyn Marshall, Amazing State (Houston, TX: Joyeuse Garde, 1984).

46 Rothbard, p. 184.

47 Yale Brozen, “Welfare Without the Welfare State,” The Freeman,
(December 1966), p. 47.

48 Rothbard, p. 184.

CHAPTER 3 / Good Samaritan Faith

+ Ulrich Teitelbaum, Dissenting Voices: A Punctuated Legacy of Protest in the
Christian Era (New York, NY: Yale Press, 1971), p. 364.

23. W. Harrold, The Diaconate (London, UK: Gospel Seed Publications, 1926),
p. 88.
3 Ibid., p. 92.

4 John Calvin, trans, John T. McNeill, Institutes of the Christian Religion
(Philadelphia, PA: Westminster Press, 1960), p. 1097.

5 Ibid., p. 1098.

CHAPTER 4 / Biblical Charity

'.R. Andrews, George Whitefield, A Light Rising in Obscenity (London, UK:
Morgan and Chase, 1930), p. 19.

2C, H. Spurgeon, Metropolitan Tabernacle Pulpit, Volume 23 (London, UK:
Passmore and Alabaster, 1877), p. 353.

3 Nathaniel Samuelson, The Puritan Divines: Collected Sermons, Volume Six
(London, UK: Merrick and Sons, 1902), p. 91.

4 Spurgeon, p. 349.

5. L. Dabney, A Defense of Virginia and the South (Harrisonburg, VA:
Sprinkle Publications, 1977), pp. 5-6.

Gary North, Unconditional Surrender: God's Program for Victory (Tyler, TX:
Geneva Press, 1981), p. 137.

7 Hugh Latimer, Sermons Before King Edward VI (Philadelphia, PA: North
Valley Publishers, 1897), p. 184.

8 Richard Steele, The Tradesman’s Calling (Hartford, CT: Mills Printrs, 1903),
pp. 14-15.

9 Martin Luther, The Estate of Marriage (St. Louis, MO: Lutheran Educational
Foundation Press, 1969), p. 84

‘0 Hyksos Pappas, Cotton Mather: An Immigrant's Patron (NewYork, NY:

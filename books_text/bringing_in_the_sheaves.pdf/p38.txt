It is indeed possible that steps to relieve misery can create
misery. The most troubling aspect of social policy toward the
poor in late twentieth-century America is not how much it costs,
but what it has bought.

Charles Murray

 

The federal government must and shall quit this business
of relief. To dole out relief is to administer a narcotic,
a subtle destroyer of the human spirit.
Franklin D, Roosevelt

 

We have apparently reached the point where government social
spending may actually be generating poverty instead of reducing it.
Warren T. Brookes

 

Humpty-Dumpty sat on a wall;
Humpty-Dumpty had a great fall.
All the king’s horses and all the king's men
Couldn't put Humpty back together again.
Mother Goose

 

Lo, this only have I found, that God hath made man upright;
but they have sought out many inventions.
King Solomon

 

THE WAR ON THE POOR 41

critics. Valiant voices of protest articulated the concern that the war on
poverty was mismanaged, misdirected, and entirely mistaken. All to

 

no avail. Dismissing such voices out of hand, the **war’s’’ engineers
continued to expand the bounds of their giveaway juggermaut.

In less than twenty years, the war on poverty had ceased to be an
innovation and had become an institution. It had, indeed. become

“unconditional.”

The New Consensus

The ‘‘war on poverty” put asunder a longstanding consensus about
the purpose of social welfare programs, It was a consensus that had
remained virtually unchanged throughout the history of our nation,
and in fact, reached as far back as England's enactment in the 17th
Century of the Elizabethan Poor Laws. It was a consensus that
operated on the basic premise that civilized societies do not let their
people starve in the streets. Instead, they attempt to make some sort of
decent provision for those who would otherwise languish helplessly
in utter destitution,'*

That decent provision was by no means promiscuously
unqualified. It was, in fact, hedged round about with limitations,
prerequisites. and stipulations. Our forcbearers were unashamedly
wary. Though perhaps necessary to the maintenance of civilized
societies, welfare was still looked upon as a hazard of compassion at
best, a sentimental vice at worst.

Why a hazard and a vice?

Because, despite the fact that some people are deserving poor
(the “helpless” as the Poor Laws called them), many, many others are
undeserving poor (the “vagrant” and the “stoth™).!* By extending
welfare, a society attracts and encourages both. A few desperate souls
may be aided, but then the less savory are simultaneously spurred to
corruption. So, our forebcarers wisely gave heed to the scriptural
waming, “And on some have compassion, but making a distinction;
save others with fear. snatching them out of the fire, hating even the

PART IL:

 

The Solution

Are we not Christ's ambassadors? Are we not commissioned
with the joyous duty: preach the Gospel to the poor, proclaim
release to the captives and recovery of sight to the blind,
to free the shackled? Is it not our sacred duty to comfort
those afflicted with the very comfort with which we ourselves
have been comforted by the God of all comfort? Are we not. . .
the divinely-ordained solution?

Nathaniel Samuelson (1671)

 

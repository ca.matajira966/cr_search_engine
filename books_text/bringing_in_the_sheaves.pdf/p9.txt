Acknowledgments

o call this book mine is both true and false. It is true in that the
labor of commitiing words and phrases to paper was mine alone.
It is false in that the labor would have been utterly impossible apart
“Solomon saith, ‘There is no

  

from the graciousness of many others
new thing upon the earth.”” wrote Francis Bacon, “so that, as Plato
had an imagination, that all knowledge was bul remembrance, so
Solomon giveth his sentence, that all novelty is but oblivion.”

David Chilton has done more, perhaps, than any other person to
shape my thinking over the past few years, Happily, David writes in
English. I don’t mean English as opposed to the ftalian of Eco, or the
Spanish of Borges, but English as opposed to algebra. David is an
inspiration, not just because he understands Biblical economics. but
because he shows how Biblical economics can be a fit subject for
good, graceful, and entertaining prose.

Kemper Crabb, too, has contributed immensely to my growth as
a communicator, and as a Christian. Happily, Kemper does not write

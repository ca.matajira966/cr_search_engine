xil By THis STANDARD

ago. Indeed, this market barely existed as recently as
five years ago. Fundamental changes in perspective
have taken place within the American Christian
community, and are now accelerating — changes that
Christian news media recognize even less clearly
than the secular press does.

There are numerous reasons for this shift in per-
spective. In the United States, the most important
historical incident in this shift was the decision of the
United States Supreme Court to strike down state
laws against abortion, the infamous Roe v. Wade de-
cision of 1973. That decision made philosophy a life-
and-death issue. It brought to the forefront the ines-
capable reality of a philosophical position that Dr.
Bahnsen and other defenders of biblical law have
long argued, namely, that there is no such thing as neu-
frality, The issue of abortion has graphically illus-
trated the truth of this conclusion. Either the unborn
child is left alone to mature in the womb, or else it is
executed —in this case, by a state-licensed medical
professional. (It is illegal, at present, to commit an
abortion for a fee unless you are a licensed physi-
cian; to do so would involve practicing medicine
without a license, and the Supreme Court would up-
hold your being sent to jail for such a crime against
humanity —“humanity” being defined as an exceed-
ingly profitable medical monopoly.) There is no
third possibility, no neutral zone between life and
death, except for the rare case of an aborted child
who somehow survives the executioner initially, and
is born alive in the abortionist’s office, This medical
possibility has created havoc for humanism’s legal

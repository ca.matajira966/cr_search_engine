106 BY THIS STANDARD

“separated from” the Gentile nations, Paul goes on to
write: “Come out from among them and be separate,
says the Lord, and touch no unclean thing; and I
will receive you” (v. 17).

An example of these Old Testament laws which
separated Israel from the world is found in Leviticus
20:22-26, where we see that the observation of such
laws (for example, distinguishing unclean from
clean meats) was but symbolic of separation from
worldy customs. All meats are now deemed clean
(Mark 7:9; Acts 10:14-15), yet God’s people are still
obligated to separate themselves from worldliness
(Rom. 12:1-2) and union with unbelievers (2 Cor.
6:14-17). How was holy separation accomplished, ac-
cording to Leviticus 20? “You shall therefore keep my
statutes and all mine ordinances and do them” (v, 22).

The Good, Well-pleasing, and Perfect Will of God

A passage expressing the ethical themes of holi-
ness and separation from the world is Romans
12:1-2. Paul there says, “Therefore I beseech you,
brothers, by the mercies of God to present your bod-
ies a living sacrifice, holy, well-pleasing to God,
which is your reasonable service; and do not be con-
formed to this world (age), but rather be transformed
by the renewing of your mind, so that you may
prove what is the will of God, the good and well-
pleasing, and perfect.” Going beyond the themes of
holiness and separation, Paul speaks of the good,
well-pleasing, and perfect will of God. These same
concepts are combined in the benediction at the end
of the book of Hebrews: “Now the God of peace . . .

XV BY THIS STANDARD

Biblical Law and Evangelism

As Christians rediscover that at one time in
American history, this was a Christian nation, and
Western civilization was once Christian civilization,
the question then arises: What makes a Christian society
appear visibly diferent from any other kind of society? The
answer today is exactly what it was in Moses’ day: éh-
ics. In Moses’ day, as today, ethical systems were at war
with each other, and a God-given and man-enforced
ethical system was required as a form of interna-
tional evangelism. As we read in Deuteronomy 4:

Behold, I have taught you statutes and
judgments, even as the Lord my God com-
manded me, that ye should do so in the land
whither ye go to possess it. Keep therefore and
do them; for this is your wisdom and your un-
derstanding in the sight of the nations, which
shall hear all these statutes, and say, Surely this
great nation is a wise and understanding peo-
ple. For what nation is there so great, who hath
God so nigh unto them, as the Lord our God is
in all things that we call upon him for? And
what nation is so great, that hath statutes and
judgments so righteous as all this law, which I
set before you this day? (vv. 5-7).

God is glorified when His law is enforced by
those who honor Him. Similarly, God is outraged
when men turn their backs on His law, for in doing
so, they turn their backs on the social and legal
restraints that alone keep man from destroying him-
self and the creation. Someone has called God’s law

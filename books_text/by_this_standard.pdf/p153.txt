NEW TESTAMENT MORAL JUDGMENTS ENDORSE THE LaW 123

Christian has nothing whatsoever to do with the law
of the Old Testament; the believer, it is said, is not
bound to the law at all. We find others who would
put stiff limits on the extent of the Old Testament
law’s validity; the believer, they say, is bound to fol-
low only a portion of the Old Testament moral code
(usually the ten commandments).

But what does the inductively ascertained prac-
tice of the New Testament speakers and writers re-
veal about this? Do they ignore the law in moral
judgments? In ethical decision-making do they re-
strict themselves to the Decalogue? Simply put, the
answer is “No.” The New Testament speakers and
writers themselves are more than willing to put the
Old Testament law — Decalogue and extra-Decalogue
—into service in critical moral judgments. They do
not treat the Old Testament commandments like an
expired library card or a repealed speed limit. Just
the opposite is the case! They make free and unex-
plained use of the Old Testament law, thereby
assuming its moral authority for the New Testament
age (extending from Christ to the consummation).

Moreover the use of the Old Testament law in
New Testament moral judgments is quite thorough.
It is not limited to a single New Testament writer
(although that would be enough to establish the law’s
authority), to a single New Testament book (al-
though, again, the authority of one infallible docu-
ment is sufficient), or to one restricted Old Testa-
ment source. In contexts of moral application, New
Testament citations and allusions are taken from
portions of Genesis, Proverbs, Psalms, Isaiah, Jere-

126 BY THIS STANDARD

to judge which was the greatest commandment in
the entire Old Testament, Jesus did not go to the ten
commandments at all, but chose rather two laws
outside of the Decalogue: love God with all of your
heart, and love your neighbor as yourself (Mark
12:28-31, from Deut, 6:4-5 and Lev. 19:18).

Distilling the Old Testament’s moral demand
into these two particular extra-Decalogical laws was
apparently already known and discussed in Jesus’
day (Luke 10:25-28). It was a commonplace among
the rabbis to distinguish between “heavy” and “light”
commands in the Old Testament, the heavier laws
being those from which moral commands could be
deduced from others. Such rabbinic efforts can be
traced to the Old Testament itself, where its precepts
are summarized in a different number of principles
by various writers: eleven by David (Ps. 15), six by
Isaiah (Isa. 33:15), three by Micah (Micah 6:8), and
one by Amos (Amos 5:4) and by Habakkuk (Hab.
2:4).

According to Jesus the “greatest” command-
ments ~ the “first of all’~on which “the whole law
hangs” were the extra-Decalogical love command-
ments (Matt. 22:33, 36; Mark 12:28, 31). The prob-
lem with the Pharisees, said the Lord, was precisely
that they attended to the minor details of the law
(tithing) and “have left undone the weightier matters
of the law—justice, and mercy, and faith” (Matt.
23:23), that is, “the love of God” (Luke 11:42). It is
important at just this point that we pay attention to
Jesus’ words, for He does not encourage exclusive at-
tention to the weightier love commandments of the

14

THE CATEGORIES OF GOD'S LAW

“By recognizing the various categories of God's
Old Testament law we can readily understand
the continuing validity of every stroke of God's
commandments for today.”

The law of the Lord is fully and forever valid; as
such it holds moral authority over all men today, just
as it did previously during the Old Testament era.
This biblical truth has been substantiated in numer-
ous ways in past studies — from cardinal doctrines of
the Christian faith, direct assertions of God’s word,
and all three of the major perspectives on ethics:
normative, motivational, and consequential (stand-
ard, motive, and goal). Christ spoke, clearly and
forcefully on the subject when He said, “Do not
think that I have come to abrogate the law or the
prophets; I have come not to abrogate, but to fulfill.
For verily I say unto you, until heaven and earth
pass away, until all things have come about, not one

THE CATEGORIES OF GODS LAW 133

letter or stroke shall by any means pass away from
the law. Therefore, whoever breaks the least of these
commandments and teaches men so shall be called
least in the kingdom of heaven” (Matt. 5:17-19).
Those who oppose keepirig the law or paying at-
tention to its details today have a great deal to ex-
plain and defend in light of the teaching of God's
word—for instance the strong affirmation of the
Lord quoted above. If the validity of the law (or a
portion thereof) has expired in the New Testament,
as some claim, then what are we to make of scrip-
tural assertions that God does not alter His covenant
word, does not allow subtraction from His com-
mandments, is unchanging in His moral character
(which the law reflects), and does not have a double-
standard of right and wrong? Why then is the writ-
ing of the Old Testament law on our hearts central to
the New Covenant? Why does the Bible say His
commandments are everlasting? Why do New Tes-
tament writers say that the entire Old Testament is
our instruction in righteousness and to be obeyed?
Why do they cite its stipulations with authority and
use them to bolster their own teaching? Why are we
expected to model our behavior on Christ’s, while we
are told that He obeyed the law meticulously and
perfectly? Why does the sanctifying work of the Holy
Spirit entail the observance of God’s law? Why does
love summarize the law im particular? Why does
faith establish the law for us to keep, and why does
God's grace teach us to walk in the law's path of
righteousness? Why are we told in numerous ways
that the law brings blessings to those who heed it?

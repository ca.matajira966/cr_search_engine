CONTINUITY BETWEEN THE COVENANTS ON THE Law 143.

of God's law, God would punish them by expelling
them from the land (Lev. 18:24-27)—even as He
would expel Israel if she violated His laws (Deut.
30:17-18), The moral standard and the judgment on
disobedience were the same between Israel and the
nations.

Accordingly, Paul teaches that all men, Jews and
Gentiles, have sinned by violating God’s law (Rom.
2:9; 19-20), and Jude declares that God will judge
all ungodly men for all of their ungodly deeds (Jude
14-15). Where the Oid Testament taught that “Right-
eousness exalts a nation, but sin is a reproach to any
people” (Prov. 14:34), the New Testament teaches
that whatever Christ has commanded is to be propa-
gated to the nations (Matt. 28:20). God's law binds all
men at all times in all places. To this point we have seen
that the Old and New Testament agree perfectly that
the law of God is perpetual in its principles —not be-
ing uniquely Mosaic, but reflecting the eternal char-
acter of God—and thorough in its extent —touching
matters of the heart, applying to all areas of life, and
binding all mankind to obedience. At this juncture it
will be important to add that:

Ill. God's law is complementary to salvation by grace.

(A) The law was not to be used as a way of justi-
fication.

The Old Testament teaches that in God’s sight
“no man living is righteous (or justified),” for if God
marks iniquities no man can stand (Ps. 143:2; 130:3).
Instead, “the just shali live by faith” (Hab. 2:4). The
Psalmist saw that “Blessed is the man unto whom

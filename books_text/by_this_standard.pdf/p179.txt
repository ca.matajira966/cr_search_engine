‘CONTINUITY BETWEEN THE COVENANTS ON THE LAW 149

Testament, where those who choose to follow Christ
rather than the beast are identified as “those who
keep the commandments of God and the faith of
Jesus” (Rev. 12:17; 14:12). In either Old or New Tes-
tament it would be unthinkable for a redeemed
saint, who loved the Lord and was dedicated to
Him, to spurn, criticize, or disobey the law of God.

(B) God's law was to be loved as a delight and
blessing.

Although men may scoff, the delight of the godly
man is found in the law of the Lord (Ps. 1:2; 119:16);
that man is happy, said the Old Testament, who
greatly delighted in God’s law (Ps. 112:1). Paul’s New
Testament viewpoint was identical: “I delight in the
law of God after the inward man” (Rom. 7:22). To
John the law of God was such.a joy that he could
declare, “His commandments are not burdensome”
(1 John 5:3b). It is sin—that is, according to both
testaments, violation of God’s covenants (Joshua
7:1; Isa. 24:5; 1 John 3:4)—that is detested by God’s
people, for it brings death (Rom, 6:23). Apart from
man’s sinful inability, the law itself is graciously or-
dained rather unto life (Lev. 18:5; Neh. 9:29; Ezk.
20:11, 13, 21; cf. Prov, 3:7-8).

It is not the Old Testament only that recognizes
this fact. Paul discerns the connection between obe-
dience to the law and life in the Spirit (Rom. 8:2-4,
6-7, 12-14) and confesses that, apart from his sinful
corruption, the law is meant fo communicate life (Rom.
7:10). Anything that is against the law’s demands,
then, is also against health-giving (sound) doctrine,

152 BY THIS STANDARD

Anyone who suggests, without authorization
from the word of God, that some law of the Old Tes-
tament is not binding upon our behavior today
would fall under the double censure of both the Old
Testament and New Testament writers. Such a sug-
gestion would contradict the perpetuity and extent of
God's law as taught in both testaments; it would evi-
dence forgetfulness of God’s mercies, violate the cov-
enant, and deprive God’s people of one of their de-
lights. Such a suggestion would stand diametrically
opposed to the eternality and immutability of the law
as set forth in the Old and New Testaments. To chal-
lenge the law without Biblically revealed direction
from the Lord is to grieve and challenge Him, so
that those who do so will be demoted within God’s
kingdom.

Unless Scripture itself shows us some change
with respect to God’s law or our obedience to it, the
principle which governs our attitude and behavior
should be the same as the Bible’s categorical assump-
tion—namely, that our instruction in righteous be-
havior is found in every Old Testament Scripture (1
Tim. 3:16-17), every point of the law (Jas. 2:10),
even.the least commandment (Matt. 5:19; 23:23),
every word (Matt. 4:4), and every letter (Matt.
5:18). This is clear from the major points —to which
both Olid Testament and New Testament give
assent~that have been reviewed about the law
above. Given these agreed-upon points, we have no
reason to expect that the New Testament would
categorically or silently release the believer from his
moral duty to God’s law.

DISCONTINUITY BETWEEN THE COVENANTS ON THE LAW 155,

I. The New Covenani surpasses the Old Covenant in glory.

(A) While the Old Covenant was fundamentally
a ministration of condemnation and death, the New
Covenant is a ministration of righteousness and life.

Paul reflects upon the distinctives of the New
Covenant in 2 Corinthians 3, proving that anyone
who exalts the law over the gospel (as did the legal-
istic Judaizers}— anyone who is so absorbed in the
commandments that he obscures or overlooks the
good news of redemption—has made a grave mis-
take. The New Covenant, teaches Paul, far out-
shines in glory the law of the Old Covenant. The law
certainly has its glory (2 Cor. 3:9, 11), but despite
that glory, what stands out in the Old Covenant is
the feature of condemnation which brings death
(3:6, 7, 9).

The law is good—indeed, ordained unto life.
However, the sinfulness of man works through the
good law to produce death (Rom. 7:12-16). The out-
standing feature of the Old Covenant to Paul's mind
was the external tables of the law which, although
they commanded good things, could not confer good
things. These external ordinances necessarily con-
demn all unrighteous men and demand their death:
as Paul said, “the letter kills” (2 Cor. 3:6). There is
no way that sinful men can be justified by doing the
law (Gal. 2:16; 3:11).

When Moses returned from receiving the law his
face shone with the glory of God, and after reading
the law to the people, he needed to put up a veil over
his face for the sake of the people (2 Cor. 3:7, 13).
Paul sees in this fact the double character of the Old

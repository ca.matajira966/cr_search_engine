158 BY THIS STANDARD:

holy place by the blood of Jesus” (Heb. 10:19; cf.
4:15-16; 6:18-20), The assurance of forgiveness, the
purity of the believer, and the nearness of God are
far greater in the New Covenant than anything the
Old Covenant law could secure. So Calvin rightly
remarks: “The person who still holds to or wishes to
restore the shadows of the law not only obscures the
glory of Christ but also deprives us of a tremendous
blessing, in that he puts a distance between us and
God, to approach whom freedom has been granted
us by the gospel” (Commentary at Heb. 7:19).

(C) Unlike the Old Covenant, the New Cove-
nant has a permanent and unfading glory.

in 2 Corinthians 3, Paul likens the glory of the
Old Covenant with its law to the glory which shone
in Moses’ face after receiving that law (vv. 7, 13).
What Paul repeats over and over again is that this
glory was “passing away” (vv. 7, 11, 13) and had to be
veiled (vv. 7, 13-16). But the New Covenant has a
transforming glory seen in the face of Christ (3:18;
4:4, 6); this glory is beheld with unveiled face, per-
manently and progressively making us over into the
same image “from glory to glory.” Moses mirrored
the glory of God only intermittently with a fading
glory —such was the excellence of the Old Covenant
law. We constantly mirror the unfading glory of
Christ who is the very image of God. Indeed, “we re-
joice in our hope of sharing the glory of God” (Rom.
5:2). Distinctive to the New Covenant is a glory sur-
passing the law, a glory which can be gazed upon, as
well as mirrored, without interruption.

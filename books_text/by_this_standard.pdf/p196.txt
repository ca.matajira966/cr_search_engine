166 BY THis STANDARD

tional to the church (Luke 22:20; cf. Eph. 2:20).
This biblically grounded redefinition of the people of
God brings with it some corresponding alterations in
the application of the Old Testament law.

(1) Because the New Covenant does not de-
fine God’s people as an earthly nation among others,
it does not require political loyalty to national Israel
as did the Old Covenant (Phil. 3:20). Christ’s king-
dom, unlike Old Testament Israel, is not to be de-
fended with the sword (John 18:36; cf. 2 Cor. 10:4).

(2) Because the significance of Canaan as the
promised land of inheritance has passed away with
the establishment of the kingdom which it foresha-
dowed (cf. Gal. 3:16; cf. Gen. 13:15; Heb. 11:8-10;
Eph. 1:14; 1 Peter 1:4), Old Covenant laws which are
directly concerned with this land (for example, divi-
sion of the land into family portions, locations of the
cites of refuge, the Levirate institution) will find a
changed application in our day.

(3) The separation from unholy peoples re-
quired by God through the dietary laws, which sym-
bolized this separation by a separation made be-
tween clean and unclean meats (cf. Lev. 20:22-26),
will no longer be observed by avoidance of the Gen-
tiles (Acts 10) or typified by abstaining from certain
foods (Mark 7:19; Acts 10:15; Rom. 14:17). For the
Christian, this now requires separation from any
ungodliness or compromising unbelief anywhere
they may be found (2 Cor. 6:14-18).

IV. The New Covenant surpasses the Old Covenant in
finality.

NEW TESTAMENT OPPOSITION TO THE ABUSE OF GOD'S LAW 179

counter to the law’s character and intent, so that the
law's good nature might be perverted into something
evil. The abuse of the law is indirectly condemned
by Paul.

Examples of Abuse

What might such an abuse be? Where do we find
an unlawful use of the law? We need not look far in
the pages of the New Testament. Throughout the
ministry of Christ and persistently in the epistles of
Paul we encounter the Pharisaical and Judaizing at-
titude that one can, by performing works of the law,
find personal justification before God. Amazing
pride and self-deception led the Jews to believe that
they might appear righteous in the judgment of a
holy God if they but strove diligently to keep the
commandments (or at least their external require-
ments). The Pharisees liked to justify themselves be-
fore men (Luke 16:15); they trusted in themselves
that they were indeed righteous (Luke 18:9)—so
much so that they had no more need for a Savior
than a healthy man needs a physician (Matt.
9:12-13). However, God knew their hearts all too
well. Despite outward appearances of cleanliness
and righteousness, they were inwardly foul, spiritu-
ally dead, and full of iniquity (Matt. 23:27-28).
Because they went about trying to establish their
own righteousness, the Pharisees could not submit to
the righteousness of God (Rom. 10:3).

Within the early church there soon arose a party
from among the Pharisees that insisted that the Gen-
tiles could not be saved without being circumcised

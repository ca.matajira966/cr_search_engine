192 BY THIS STANDARD.

the commandments and wisdom of men. No longer
is it misused for the purposes of self-righteousness.
Within the life of the believer the law receives its
proper due; indeed, it is established by faith (Rom.
3:31). By it we can be blessed.

According to Scripture, the law has many legiti-
mate functions. We can try to summarize them in
the following list.

(1) The law declares the character of God and so re-
veals His glory.

The kind of lifestyle and attitudes which the Lord
requires of His people tells us, of course, what kind
of God He is. If you wish to see the contrast between
the pagan deities and the living and true God of the
Bible, simply observe the difference between the
things which they command. Moloch demanded
child sacrifice, while Jehovah commanded the care
and nurture of children—to take but one example.
Psalm 119 extensively applies the attributes of God
(perfection, purity, righteousness, truth) to the pre-
cepts of God, Throughout the law God reinforces the
authority of His commands by following them with
the declaration, “I am the Lord.”

In showing the true and radical demand of the
law’s requirements (Matt. 5:21-47), Christ was
showing us the perfection of God which is desired in
us (v. 48). John Newton wrote:

When we use the law as a glass to behold the
glory of God, we use it lawfully. His glory is
eminently revealed in Christ; but much of it is

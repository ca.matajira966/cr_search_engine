24

LAW AND POLITICS IN
NATIONS AROUND ISRAEL

“God did not exempt nations around Israel from
the claims of His righteousness, but rather held
them accountable for moral degeneration.”

Law and politics in Old Testament Israel revolved
around God’s law for the civil magistrate. That
much would be granted by virtually any Christian
who takes an interest in a Christian political stand
and who has read the Bible. In the “theocracy” of the
Old Testament God obviously gave laws for His peo-
ple to obey in the political sector of life.

Nevertheless, it is often thought, those “theo-
cratic” laws given to Israel for her political life are of
little help to Christian political theory today. Why?
Were Old Testament laws about crime and punish-
ment less inspired than prophecies about the coming
Messiah? Well, no, we will be told. Were Old Testa-
ment laws about crime and punishment less of @ reflec-

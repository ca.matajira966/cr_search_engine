238 BY THIS STANDARD

cording to Psalm 119:118-119. Accordingly, there was no
recognition of differing laws for differing kinds of peo-
ple (Jewish, Gentile) in the Old Testament. “There
shall be one standard for the stranger as well as the
native, for I am the Lord your God? (Lev. 24:22). With
respect to politics, as with all things, God did not have
a double standard of morality, The justice of His law
was to be established as a light to the Gentiles (Isa.
51:4), Indeed, the prophetic hope was that all nations
would flow into Zion, saying “Come and let us go up to
the mountain of Jehovah, to the house of the God of
Jacob; and he will teach us of his ways, and we will
walk in his paths: for out of Zion shall go forth the law,
and the word of Jehovah from Jerusalem” (Isa. 2:2-3).

The Old Testament perspective was that God’s
law had international and civic relevance. Its bind-
ing character was not confined to the borders of
Israel. Accordingly, the Wisdom literature of the
Old Testament (for example, the book of Proverbs)
made wise and practical application of the law of
God, and it was written for the entire world. The
wisdom of Proverbs had universal bearing, express-
ing axiomatic truths for all men. Rather than being
localized and nationalistic, the Wisdom literature
was intended for use in cultural interaction with
other peoples. God’s law--Israel’s wisdom in the
sight of others (cf. Deut. 4:6, 8)—was designed for
the moral government of the world.

Gentile Civil Magistrates
Biblical teaching about the civil magistrate in
Gentile nations during the Old Testament period,

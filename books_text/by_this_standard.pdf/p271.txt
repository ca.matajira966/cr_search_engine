LAW AND POLITICS IN NATIONS AROUND ISRAEL 241

2. Bearing religious fitles, rulers were to avenge divine
wrath.

In Israel the titles of “My Servant” and “My
Shepherd” had clear religious overtones because of
their typological significance, pointing to the coming
Messiah (for example, Isa. 53:11; Ezk. 34:23), What
is of interest to us is that such religiously significant
titles are applied to political rulers outside of Israel.
Nebuchadnezzar was called by God “My servant”
(Jer. 25:9, etc.), and Cyrus was called “My shep-
herd” (Isa, 44:28). Indeed, Cyrus is even designated
“My annointed one” (“My Christ” in Greek transla-
tion) by Jehovah in Isaiah 45:1. Such titles show how
religiously important the office of magistrate was
even in Gentile lands, according to God’s word.

It was appropriate, then, that Gentile magis-
trates be expected to avenge God's wrath against
evildoers, for the magistrates were representatives
and servants of the Most High. For instance, the
Assyrian king was to be “the rod of My anger, the
staff in whose hand is My indignation” (Isa. 10:1).
God gave “charge” to Assyria to do His work of ven-
geance, and when Assyria overlooked its servant
status under God, it was punished for its stout heart
and self-sufficient arrogance in attacking Israel (Isa.
1:12-13). In Old Testament perspective, therefore,
God was viewed as enthroned over all nations (Ps.
47:2, 7, 8), making all Gentile rulers the deputies of
God. “The shields [rulers] of all the earth belong un-
to God,” declared the Psalmist (47:9). Civil rule in
all the nations is secondary and subordinate to God’s
rule. God reigns among the nations in righteousness

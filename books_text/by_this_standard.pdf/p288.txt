258 GY THIS STANDARD

outworking of a theological conception of man, the
world, and God. The modern world is no different;
its political philosophies are simultaneously political
theologies, and its civil rulers are often seen in a re-
ligious light (even if religious vocabulary is
shunned).

Magistrates as Ministers

Paul, following the Old Testament, had a religi-
ous conception or understanding of the civil magis-
trate. In Romans 13 he twice categorized the magis-
trate in society as a “minister of God” (wv. 4, 6). If
you ask the ordinary Christian today where one can
find God’s “minister,” he will point you to the pastor
of the local church. He will not think to point you to
the city, state, or federal magistrate, for he has capit-
ulated to the mentality of humanistic secularism.
Paul had not done so, even though the Roman em-
perors of his day were far fron “religious” in the com-
mendable sense of that term. Whatever the Caesars
may have thought of themselves, Paul thought of
them as God's ministers. They were God’s prescribed
instruments for maintaining order and punishing
evildoers according to God's will.

In Romans 13:6 Paul used the title of “leitourgos”
to describe the magistrate as God’s “minister.” In the
ancient world this term was used for work done to
promote the social order, work performed in the ser-
vice of the divine-state. So Paul used the term with a
theological twist. The magistrate is not a minister of
the divine-state, but rather the state 1s the minister of
God Himself. In the Greek translation of the Old

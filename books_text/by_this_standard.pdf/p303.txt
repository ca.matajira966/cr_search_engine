CRIME AND PUNISHMENT 273

. .. What a fine mercy to me it would be, to have
mercy on the thief and murderer, and let him kill,
abuse, and rob me!” So Scripture teaches that civil
penalties are necessary. The magistrate is not to
carry his sword in vain.

Not only are such penal sanctions necessary in so-
ciety, they must also be equitable. The measure of
punishment according to the just Judge of all the
earth is to be an eye for an eye, a tooth for a tooth, a
life for a life ~no less, but no more (for example, Ex.
21:23-25; Deut. 19:21). The punishment must be
commensurate with the crime, for it is to express ret-
ribution against the offender. Especially when one
compares the Biblical code of penal sanctions with
those in other ancient civilizations does it become
apparent how just and wise God's laws are; they are
never overweighted, lenient, cruel or unusual. Far
from being arbitrary, they are laid down with a view
to perfect justice in social affairs. Indirectly, these
penal sanctions will become a deterrent to crime in
others (for example, Deut. 17:13; 19:20), but they are
designed to punish a person retributively, “according
to his fault” (Deut. 25:2). That is why, for instance,
those who commit capital crimes are said in the
Bible to have “committed a sin worthy of death” (Deut.
21:22). God always prescribes exactly what a crime
deserves; the stringency of the penalty is proportion-
ed to the heinousness of the deed. His punishments
are thus always equitable.

The agency which God enlists for executing His
just and necessary penalties in society for crimes is
the civil magistrate. The reason why, by men, the

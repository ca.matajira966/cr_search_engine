‘CRIME AND PUNISHMENT 275

ing current day magistrates follow the penal sanc-
tions of God's law usually leave us with the position
that there are no permanently just standards of punish-
ment, for magistrates are left to themselves to devise
their own penal codes autonomously. If some ruler
thought that stealing two pennies deserved death,
while killing an innocent child deserved the fine of
two pennies, many Christian teachers would have no
objective way to demonstrate the injustice of this ar-
rangement. Their failure to produce a God-glorify-
ing, Scripturally-anchored, method of knowing what
justice demands in particular cases of criminal activ-
ity would in principle leave us at the mercy of magis-
trate-despots.

When there is zo daw above the civil law, restrain-
ing and guiding its dictates, then human will
becomes absolute and fearsome. Before any reader is
tempted to turn away from the all-too-obvious prop-
osition that God’s revealed law should be followed by
the civil magistrate when it comes to crime and pun-
ishment, let the reader be clear in his or her own
mind just what the alternatives are. In many cases
those who criticize the use of God’s penal sanctions
objectively known from the Scriptures have either zo
alternative or arbitrary tyranny to offer in its place.

In addition to asking for the alternative which
the critic of God’s law has in mind, the reader should
make a point of requesting some justifying evidence
from Scripture for this rejection of the Old Testament
laws penal sanctions. This is highly important, for
Jesus warned that anyone who taught the breaking
of even the deast commandment of the Old Testament

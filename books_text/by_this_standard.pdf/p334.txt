304 By THIS STANDARD

would attempt to marshall arguments against the
conclusions to which we have been driven by our
study of Scripture. In all fairness we need to survey
some of the main reasons which people offer for say-
ing that the law of God is not generally valid in the
New Covenant dispensation, asking whether such
considerations genuinely disprove what we have said
herein.

Matthew 5:17-19

A passage of Scripture which clearly seems to
teach the presumption of moral continuity today
with the Old Testament commandments is Matthew
5:17-19. Yet some write as though this passage says
nothing of the sort. They argue, for instance, that
verse 17 deals not with Christ’s attitude toward the
Old Testament law, but rather with Christ’s life as the
prophetic realization of everything in the Old Testa-
ment canon,

It is true, of course, that the scope of Christ’s dec-
Jaration here is the entire Old Testament (“the Law
and the Prophets”). However, there is absolutely
nothing in the context of the verse or its wording
which touches on the life of Christ (in distinction
from His teaching) or on prophecy-typology. The
focus of attention is obviously the moral standards by
which Christ would have us live, and in particular
the question of the Old Testament commandments is
taken up. Verse 16 speaks of our “good works.” Verse
17 twice denies that Christ abrogates the Old Testa-
ment revelation--in which case any interpretation
which makes “fulfill” imply the abrogation of the law

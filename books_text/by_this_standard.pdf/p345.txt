ARGUMENTS AGAINST THE LAW'S GENERAL VALIDITY 315

model for Christian ethics, and the fact that the New
Covenant is the administration of God's single
promise under which we now are privileged to live,
do not imply in any logical or Biblical way that the
moral standards of the Old Testament have been
laid aside as invalid today. To insist that we are New
Covenant believers or that the Mosaic command-
ments must come to us through Christ is not to sub-
tract anything from our obligation to the Old Testa-
ment law, as interpreted and qualified by the ad-
vanced revelation of the New Testament.

Remarks Relevant to the Law’s Categories

Finally, we can survey a few popular arguments
against the general validity of the Old Testament
law, all of which relate to the categories commonly
recognized by theologians (namely, moral law,
judicial law, ceremonial law).

First, there is the argument that the Bible never
speaks of such categories, in which case the law must
be viewed as an indivisible whole. If the law has
been laid aside in any sense, then accordingly the
whole law has been laid aside, it is thought. Such
thinking is simplistic and fallacious.

To begin with, the Bible can often be correctly
summarized in ways which are not actually spoken of
in the Bible itself (for example, the doctrine of “the
Trinity”), and so the convenient categorization of the
law is not unacceptable in advance. It all depends on
whether the categories and their implications are
true to Scriptural teaching. Secondly, there is a sense
in which the law stands together as a unit; indeed,

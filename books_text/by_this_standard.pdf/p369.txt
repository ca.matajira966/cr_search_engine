ARGUMENTS AGAINST THE LAW'S POLITICAL USE 339

If they refuse to submit to the Lord (Ps. 2), they wilt
eventually answer to “the King of kings” (I Tim. 6:15)
for their rebellion, This means that there are stand-
ards of justice to which they will be answerable.

If those standards are not found in the Old Testa-
ment, then why not? Then where else? Such ques-
tions receive no convincing and theologically consis-
tent answer from those who reject the political use of
the Old Testament law. Do these critics of theonomy
believe that political rulers are free to do whatever
seems right in their own eyes?

We have seen attempts made to disprove the
validity of the socio-political laws of Moses by ap-
pealing to some special feature about Old Testament
Israel. However, such a special feature is never
clearly defined. The segment of the law which is
thought to be invalidated is never delineated on the
basis of explicit principle; specific laws are rather in-
cluded or excluded from the segment arbitrarily and
subjectively by the person advancing such an argu-
ment. The alleged unique feature is often not even
actually true about Old Testament Israel. And
finally, no demonstration is forthcoming, grounded
upon Scripture, that the validity of this intended seg-
ment of the Mosaic law rested entirely upon that
unique feature of Old Testament Israel in the first
place. Other kinds of arguments against the modern
use of the Old Testament in political ethics appeal to
considerations which are utterly irrelevant to the
truth or falsity of that idea—arguments from silence,
subjective impression, abuse, tradition, and
ridicule. In short, those who have argued against the

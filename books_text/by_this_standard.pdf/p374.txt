344 BY THIS STANDARD

instance, did not always enjoy that status in the eyes
of earlier Christians. The winds of commen opinion
have shifted, Why? Has some radical new turn or dis-
covery in Christian scholarship, some brilliant ex-
egesis and persuasive reasoning, intervened between
the Puritan age and our own today so as to account
for this shift in widespread sentiment about the use of
God’s law in the Christian life? If so, it is hard to point
to just what it might have been. It is rather changed
social circumstances and opinions, not advances in
scholarship, which have brought about the difference.
“But the word of the Lord abides forever” (I Pet.
1:25; Isa. 40:8). If our Reformed and Puritan forefa-
thers were basically correct in their approach to the
Old Testament law of God, as I believe, then the
truth of that position is still discernible in the objec-
tive revelation of God’s word, even if it is an un-
popular truth in a secularized age. Whether con-
genial to popular opinion today or not, the conclu-
sions to which we have been driven in our study of
God’s unchanging word indicate that the standard
by which Christians should live is not restricted to the
New Testament, but includes the law of God revealed
in the Old Testament. “Scripture cannot be broken”
(John 10:35). With God “there can be no variation,
neither shadow that is cast by turning” (Jas. £:17).
Our studies have pointed to the conclusion that
New Testament believers ought to maintain a pro-
nomian, rather than antinomian, attitude. They
should seek to purge themselves of “autonomous”
ethical reasoning in favor of a “theonomic” approach
to moral issues. They should presume that the com-

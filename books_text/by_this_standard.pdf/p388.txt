358 BY THIS STANDARD

and Magog), who will be saved by fire from heaven,
followed by the second resurrection now of unbe-
lievers—and the final judgment

PRIMA FACIE — on first appearance

PRO-NOMIAN — characterized by favoring, support-
ing, or defending the law

PURITY PRINCIPLES — those truths taught or sym-
bolized by ceremonial laws of outward cleanliness,
such as the pollution of sin and its repugnance to a
holy God, so that only one untainted by defilement
may approach Him (e.g., laws dealing with purifica-
tion for priests, issues of blood, disfigurement, lep-
rosy)

REDEMPTIVE HISTORY — the special, unified course
of historical events by which God prepared, accom-
plished, and applies redemption for His people and
thereby advances His saving kingdom

REDEMPTIVE LAW — ceremonial laws which taught
or symbolized the way of atonement or God’s saving
presence among His people (e.g., laws dealing with
sacrifice, the priesthood, the temple)

REFORMED — (as used in theology:) characterized by
agreement with or adherence to the doctrine, wor-
ship, ethic or polity of the Protestant Reformation,
more particularly the Swiss or Calvinist branch there-
of (in distinction from Lutheranism, Anabaptism)

RELATIVISM, CULTURAL—the view that what is

Always Ready
Directions for Defending the Faith
by Greg L. Bahnsen

But sanctify Christ as Lord in your hearts, always being ready
to make a defense to everyone who asks you to give an account
{for the hope that is in you, yet with gentleness and reverence.

(Peter 3:15)

This admonition of the Apostle Peter to prepare our-
selves for a defense (apologia) of the faith enlightens us
not only of the need to be armed for engagement with
unbelievers, it also instructs us concerning the proper
apologetic method. Every believer can and must be
equipped for the apologetic encounter with unbelief.
Always Ready will lay the foundation for pastors and
laymen alike to understand and apply basic biblical
principles for a solid defense of the Christian faith. You
will learn about the myth of neutrality, the nature of
belief and unbelief, how to critique worldviews, and how
to develop a solid biblical strategy for answering the
challenges of unbelievers. Special assistance is provided
for many of the common objections offered against
Christianity, such as the problem of evil, the problem of
knowing the supernatural, the problem of faith, the
problem of religious language, and the problem of
miracles.

289 pp., indexed, paperback: $14.95
Institute for Christian Economics, P.O. Box 8000, Tyler, Texas 75711

18 BY THIS STANDARD

cal yardstick of ethics and have substituted some-
thing else for it. The outcome has been the loss of
any respectable, vigorous, reforming ethic in the
contemporary church. “Thus said the Lord” has
been reduced to “it seems to me (or us).” Bonhoeffer
said that “God is teaching us that we must live as
men who can get along very well without Him.”!
Not only does Frank Sinatra sing out modern man's
testimony for Western culture, “The record shows I
took the blows, and did it my way,” but the German
theologian Wolfhart Pannenberg delivers the mod-
ern church’s response: “The proclamation of impera-
tives backed by divine authority is not very persua-
sive today.”? The Bible no longer directs all of life be-
cause its requirements are deemed stifling and are
viewed in advance as unreasonable.

Men repudiate the “interference” in their lives
represented by God’s commandments. This attitude
of lawlessness (1 John 3:4) unites all men because of
their sin (Rom. 3:23). Even theologians today pre-
tend to be ethical authorities in their own right who
know better than the Bible what is right and wrong.
In Christian Ethics and Contemporary Philosophy Graeme
de Graaff says, “There is no room in morality for
commands, whether they are the father’s, the school-
master’s or the priest’s. There is still not room for
them when they are God’s commands.” The leading

1, Dietrich Bonhoeffer, Letters and Papers From Prison (London:
SCM Press, 1953), p. 164.

2. Wolfhart Pannenberg, Theology and the Kingdom of God (Phil-
adelphia: Westminster Press, 1969}, pp. 103-104.

3. Graeme de Graaff, ‘God and Morality,” in Christian Ethics

‘THE FATHER'S UNCHANGING HOLINESS AND Law 47

3:4) they became “like God”—law-givers of their
own making and authority. God’s law, which should
have been their delight, became burdensome to
them.

Jesus and God’s Law

By contrast, the second Adam, Jesus Christ, liv-
ed a life of perfect obedience to the laws of God.
When Satan tempted Him to depart from the path of
utter obedience to God’s commands, the Savior
replied by quoting from the Old Testament law: you
are not to tempt the Lord your God, you are to wor-
ship and serve Him alone, and you are to live by
every word that proceeds from His mouth (Matt.
4:1-11). Here we have the very opposite of Adam and
Eve’s response to Satan. Christ said that the attitude
which is genuinely godly recognizes the moral au-
thority of God alone, does not question the wisdom
of His dictates, and observes every last detail of his
word. This is man’s proper path to God-likeness. To
live in this fashion displays the image or likeness of
God that man was originally intended to be (Gen.
1:27), for it is living “in righteousness and true
holiness” (Eph. 4:24). Genuine godliness, as com-
manded in the Scripture, is gained by imitating the
holiness of God on a creaturely level—not by
audacious attempts to redefine good and evil in some
area of life on your own terms.

Jesus concluded His discourse on God's law in
the Sermon on the Mount by saying, “Therefore you
are to be perfect as your heavenly Father is perfect”
(Matt. 5:48). Those who are not striving to become

THE FATHER'S UNCHANGING HOLINESS AND LAW 49

The Holiness of God

According to the Old Testament ethic, God’s hol-
iness is the model for human conduct: “You shall be
holy, for I the Lord your God am holy” (Lev. 19:2).
This is also the precise model of moral conduct for
the New Testament believer: “. . . but like the Holy
one who called you, be holy yourselves also in all
your behavior; because it is written, ‘You shall be
holy, for I am holy’” (1 Peter 1:15-16). There has
been no alteration or reduction of the standard of
moral behavior between the Old and New Testa-
ments. God’s permanent requirement over all of life is
God-imitating holiness. In all ages, believers are re-
quired to display, throughout their lives, the holiness
and perfection of their God. They ought to be like
God, not in the Satanic sense which amounts to law-
lessness, but in the biblical sense which entails sub-
mission to God’s commands.

Obviously, if we are to model our lives on the
perfect holiness of God, we need Him to tell us what
the implications of this would be for our practical be-
havior. We need a perfect yardstick by which to
measure holiness in our lives. The Bible teaches us
that the Lord has provided this guide and standard
in his holy daw (cf. Rom. 7:12). The law is a transcript
of the holiness of God on a creaturely level; it is the
ultimate standard of human righteousness in any
area of life, for it reflects the moral perfection of
God, its Author.

The intimate relation which the law bears to the
very person of God is indicated by the fact that it was
originally written by the finger of God (Deut. 9:10)

7

THE SON’S MODEL
RIGHTEOUSNESS

“Christ perfectly obeyed the law of God, and
this has unavoldable implications for Christian
ethics—for imitating the Christ portrayed in the
Bible.”

The Bible was written over many years, by many
people, and about many things. Yet central to the
Bible is the person of Jesus Christ. He is of para-
mount importance throughout. We know that He
was, as the Word of God, active at the creation of the
world (John 13), and that He providentially upholds
all things by the word of His power (Hebrews 1:3).
After Adam’s fall into sin through disobedience to
God’s command, relief from the wrath and curse of
God was promised in terms of one who, as the seed
of the woman, would crush Satan (Genesis 3:15).
The entire Old Testament prepares for the coming of
this promised Messiah —the prophet (Deut. 18:15-19),

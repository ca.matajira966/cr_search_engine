‘THE SPIRITS DYNAMIC FOR LIVING 67

pent, genuine guilt which drives men to Christ, and
specific guidelines for righteous behavior in the be-
liever. Taking Paul out of context, some churches
and teachers would make their message “we are not
under law but grace.” They would present evangel-
ism and Christian nurture as though mutually exclu-
sive of concern for God’s righteous standards as
found in his commandments. They would focus on
the extraordinary work of the Spirit in a supposed
second blessing and the charismatic gifts. The whole
of the Biblical message and Christian life would be
cast into a distorted, truncated, or modified form in
the interests of a religion of pure grace.

However, God’s word warns us against turning
the grace of God into an occasion or cause of licen-
tious living (Jude 4); it insists that faith does not nul-
lify God’s law (Romans 3:31). One has to be deceived,
Paul says, to think that the unrighteous could possi-
bly inherit the kingdom of God (1 Cor. 6:9-10).
Those who demote even the slightest requirement of
God’s law will themselves be demoted in the Lord’s
kingdom (Matt. 5:19).

The answer to legalism is not easy believism,
evangelism without the need for repentance, the
pursuit of a mystical second blessing in the Spirit, or
a Christian life devoid of righteous instruction and
guidance. Legalism is countered by the Biblical un-
derstanding of true “life in the Spirit.” In such living,
God’s Spirit is the gracious author of new life, who
convicts us of our sin and misery over against the
violated law of God, who unites us to Christ in salva-
tion that we might share His holy life, who enables

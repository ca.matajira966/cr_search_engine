92 CHRISTIAN RECONSTRUCTION

would not consent to use their verbal “wisdom” in his apologetic, lest
the cross of Christ be made void (I Gor. 1:17).

If Paul did not compromise the gospel in his discussions with
these pagan philosophers over the nature of religion, then why
do some Christian scholars maintain that it is permissible to
compromise in the area of law, politics, economics, and educa-
tion to develop an ethical system without regard to the Bible?

Decentralized Social Order

Reconstructionists believe in a “minimal state.” The purpose
of getting involved in politics, as Reconstructionists see it, is to
reduce the power of the State. Reconstructionists are not calling
on the State to mandate prayer and Bible reading in the public
(government) schools, as most fundamentalists advocate. Neither
do we advocate teaching “Creation Science.”*° It is the non-
Reconstructionists who petition the State for greater influence of
the Christian worldview areas over which the Bible gives the
state no jurisdiction. Reconstructionists do not believe that the
State has the God-given authority to educate our children.

Because of our belief in a minimal State, taxes would be low-
ered for every citizen. This would encourage savings, reduce
interest rates, and spur investment in high-risk technological
ventures for the long-term betterment of the citizenry. Caring
for the poor, as outlined by a book first published by American
Vision in 1985 (Bringing in the Sheaves), is not the domain of the
State. In fact, George Grant sees the State as a hindrance when
it develops policies designed to “help the poor.” Of course,
Reconstructionists are not alone in this assessment.”

Reconstructionists believe in the political process. We also

19. ibid., pp. 14-15.

20. Norman L. Geisler, an ardent critic of Christian Reconstruction, supports the
teaching of “Creation Science” in government schools. Geisler, The Creator in the Courtroom:
The Controversial Arkansas Creation-Evolution Trial (Milford, M1: Mott Media, 1982). Isn't this
mandating that the State involve itself in religion?

21. See the books by non-Christians such as Charles Murray, Thomas Sowell, Walter
E. Williams, and by Christian author E. Calvin Beisner: Prosperity and Poverty: The Compas-
sionate Use of Resources in a World of Scarcity (Crossway Books, 1988).

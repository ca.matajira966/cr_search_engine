98 CHRISTIAN RECONSTRUCTION

this that the kingdom cannot manifest itself on this earth, then
it can never be manifested on this earth. This includes the
millennium, the seventh dispensation called “the kingdom age”
by dispensational theology. “Of this world” does not have refer-
ence to where Jesus’ kingdom operates but only the source of
His kingdom’s power. His kingdom is “of heaven” — this is why
it is described as the “kingdom of heaven” — but it affects this
world. “Thy kingdom come. Thy will be done on eazth as it is in
heaven” (Matthew 6:10).

Why do so many dispensationalists misrepresent Reconstruct-
ionists on this issue? I believe the following will help shed some
light on those dispensationalists who want no part of a theology
that stipulates that the kingdom is a present reality. Dispensa-
tionalists view anyone who works for social change as trying to
“bring in the kingdom” since only Jesus can accomplish this
with His physical presence. Since Jesus is not physically present,
the kingdom is not present; it is exclusively future and millennial
in character. They then impose this (false) definition of a future
kingdom run by Jesus from Jerusalem who dispenses punish-
ment for the least infraction on a present-kingdom definition
that must operate by less than perfect sinners without Jesus
being present. They suppose if the true kingdom means Jesus
will punish any and all outward acts of disobedience, then any-
one who claims that the kingdom is a present reality must be
advocating the same type of kingdom but without the presence
of Jesus. This is an improper understanding of Christ’s king-
dom. When a dispensationalist hears the word “kingdom,” he
thinks of its governmental characteristics in earthly terms. The
following is a typical example:

The second important characteristic of the millennial rule of
Christ is that His government will be absolute in its authority and
power. This is demonstrated in His destruction of all who oppose
Him (cf. Ps. 2:9; 72:9-11; Isa. 11:4). ... The wicked are warned to
serve the Lord lest they feel His wrath (Ps. 2:10-12). It seems evi-
dent from many passages that no open sin will go unpunished. ...
(T]hose who merely profess to follow the King without actually
being saints . . . are forced to obey the King or be subject to the

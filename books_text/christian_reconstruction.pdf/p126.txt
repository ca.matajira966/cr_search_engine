104 CHRISTIAN REGONSTRUCTION

righteousness belongs entirely to Jesus. Jesus’ righteousness is
imputed or credited to those who are “dead in trespasses and
sins,” apart from any work of the law (Ephesians 2:1). This is
called justification, a judicial act of God that happens only once,
declaring sinners to be righteous based on the merits of Jesus
Christ, His perfect obedience and His perfect sacrifice.

With justification comes definitive sanctification. A support-
ing text of Scripture for definitive sanctification is 1 Corinthians
1:2: “To the church of God which is at Corinth, to those who have
been sanctified in Christ Jesus, saints by calling, with all who in
every place call upon the name of our Lord Jesus Christ, their
Lord and ours.”

Scripture goes on to talk about progressive sanctification —
spiritual growth — comparing it to natural growth: “Therefore,
putting aside all malice and all guile and hypocrisy and envy
and ail slander, like newborn babes, long for the pure milk of
the word, that by it you may grow in respect to salvation, if you
have tasted the kindness of the Lord” (1 Peter 2:1-3). If justifica-
tion is a point, then sanctification is a vector, starting at a point
and then moving in one direction. Sanctification follows justifi-
cation as growth follows birth. Paul says that “we are His work-
manship, created in Christ Jesus for good works, which God
prepared beforehand, that we should walk in them” (Ephesians
2:10). We were redeemed “from every lawless deed” so that we
might be “zealous for good deeds” (Titus 2:14). No works or
growth, no sanctification. No sanctification, no justification.
Sanctification is evidence of justification.

Growth, however, depends on nourishment. The nourish-
ment is “the pure milk of the word.” Sanctification is not simply
being “led by the Spirit.” The Spirit uses the word to lead us
into sanctification, Scripture is the standard for sanctification.
How do we know when we are going through the process of
sanctification? Feelings? Emotion? Personal opinion? Sentiment?
Extra-biblical standards? The evaluation of others? Peter tells us
that we are to “long for the pure milk of the word.” This in-
cludes the law of God since it is part of God’s word. In fact, a
case could be made that the word of God and the law of God
are one and the same, for all that proceeds out of the mouth of

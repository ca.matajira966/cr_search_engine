What Is the Legal Standard for the Nations? 113

majority opinion in Roe determined, therefore, abortion would
have to be legalized. And what were these “ancient traditions”?:
Greek and Roman legal traditions that rested on natural law.'°
Would our nation have Sunday as a day of rest and worship
if we adopted natural law over biblical law? Even the Constitu-
tion follows biblical and not natural Jaw in its regard for Sunday
as a special religious day: “If any Bill shall not be returned by
the President within ten Days (Sundays excepted) after it shall
have been presented to him, the Same shail be a Law... .”
{Article I, section 7). Would a natural law ethic permit religious
oath-taking? No! Florida no longer requires Notaries to affirm
“so help me God” on their written oath of office. The Rev.
Gerard LaCerra, chancellor of the Archdiocese of Miami under-
stands the implications of such an action: “What are we sup-
posed to base our commitments on if something like this is
removed? The State?”"! This is where natural law leads us.
Some assert, using natural Jaw as their operating principle,
that the “celebration of Eros and the unlimited pleasure of the
body should be elevated to constitutional principle.”" Are any
and all sexual practices legitimate under natural law? As nations
become officially atheistic, a natural law ethic free from biblical
influence becomes impossible to formulate, since natural law
requires the existence of a Creator who has a law to deposit in
the universe and in the heart of man. How can a natural law
ethic be formulated when different traditions come to the for-
mulating table with contrary presuppositions? Some are Chris-
tian, religious, agnostic, and atheistic. Those who believe in God
at least have some common ground, although what god we have

10. Curt Young, The Least of These: What Everyone Should Know about Abortion (Chicago,
IL: Moody Press, 1983), pp. 21-23.

11. “ ‘God’ Removed from Notaries’ Oath,” The Kansas Cily Star (February 18, 1990),
p. 2A. “The general situation in this country is that in all court proceedings witnesses
may give testimony only after they have qualified themselves by taking an oath in the
usual form ending with ‘So help me God,’ or by making an affirmation without chat
phrase. The provisions for witnesses generally apply also to jurors.” Anson Phelps Stokes
and Leo Pfeffer, Church and State in the United States, rev. one-vol. ed, (New York: Harper
& Row, 1964), p. 490.

12, Robert H. Bork, The Tempting of America: The Political Seduction of the Law (Nw
York: The Free Press, 1990), p- 210.

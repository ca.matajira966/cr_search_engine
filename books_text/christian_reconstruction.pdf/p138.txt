116 CHRISTIAN RECONSTRUCTION

ent law. One of the products was Deism, which reduced God to the
mechanic who had created “Nature,” and now “Nature” functioned
independently of God. The next step was to accept the ultimacy of
“Nature” and to drop God entirely.”

There was predictability in the created order because God
decreed all that comes to pass. The created order, what is erro-
neously described as “nature,”” was understood to be affected
by the fall of man into sin. Special revelation was needed to
correct the distortions of a creation disfigured by sin. With the
advent of evolution, a new understanding of nature developed
that supplanted the one of “antiquity.” According to Roscoe
Pound, “no current hypothesis is reliable, as ideas and legal
philosophies change radically and frequently from time to
time.”*

In addition to natural law, Geisler writes that “most premil-
lenarians recognize that God has not left Himself without a
witness in that He has revealed a moral law in the hearts and
consciences of all men (Rom. 2:14-15).”” Geisler asserts that
the heart and conscience are repositories for an ethical code.
But the heart of man “is more deceitful than all else and is
desperately sick; who can understand it?” (Jeremiah 17:9; cf.
Genesis 6:5; 8:21; Psalm 14:1; Proverbs 6:14; 12:20; 14:12).
General revelation may give a very clear ethical system, but
man suppresses “the truth” of general revelation “in unrigh-
teousness” (Romans 1:18).

Since man’s reason is imperfect, and may be swayed by his
physical and social environment, the “truths” which men “know”
have been various and self-contradictory. The law of nature has
been quoted for every cause, from that of Negro slavery in the
United States to that of red revolution in Paris. And it has often

20. Rousas J. Rushdoony, The Mythology of Science (Nutley, NJ: The Craig Press,
1967), p. 97.

21. Rousas J. Rushdoony writes that “ ‘Nature’ is simply a collective name for an
uncollectivized reality; the myth of nature is a product of Hellenic philosophy.” The
Institutes of Biblical Law (Phillipsburg, NJ: Presbyterian and Reformed, 1973), p. 608.

22, Rene A. Wormser, The Story of the Law (New York: Simon & Schuster, 1962), p.
485. Cited in Whitehead, The Second American Revolution, 48.

2. Geisler, “A Dispensational Premillennial View of Law and Government,” p. 156.

Question No. 7

DO CHRISTIAN RECONSTRUCTIONISTS
REJECT DEMOCRACY?

Most Americans are under the impression that our nation is
a democracy. To be sure, there are certainly democratic ele-
ments in our constitutional system. The First Amendment to the
Constitution states that “the people” have the right “to petition
the Government for a redress of grievances.” The petition of
the people, however, is only as good as the character of the
people. Keeping in mind the biblical doctrine of the depravity
of man, our constitutional framers steered clear of a pure de-
mocracy. Constitutional attorney John W. Whitehead writes:

It must be remembered that the term democratic appears neither in
the Declaration of Independence nor in the Constitution, Actually,
when the Constitution is analyzed in its original form, the document
is found to be a serious attempt to establish a government mixed with
democratic, aristocratic, and monarchical elements — a government
of checks and balances.’

A democracy places all power in the people. It is a govern-
ment of the masses. Democratic law is based on the will of the
majority. If the whims and fancies of the people change, the law
changes.

John Winthrop declared democracy to be “the meanest and
worst of all forms of government.”? John Cotton wrote in 1636:

1, John Whitehead, The Separation illusion (Milford, MI: Mott Media, 1977), p. 47.
2. Quoted in A. Marvyn Davies, Foundation of American Freedom: Calvinism in the
Development of Democratic Thought and Action (Nashville, TN: Abingdon Press, 1955), p. 11.

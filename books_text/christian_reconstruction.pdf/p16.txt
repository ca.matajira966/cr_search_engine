xvi CHRISTIAN RECONSTRUCTION

nantal model, discovered by Ray Sutton in late 1986, and devel-
oped in his book, That You May Prosper in 1987. It was applied
immediately by David Chilton in structuring his commentary on
the Book of Revelation, The Days of Vengeance (1987). The five-
point covenant model was the most recent major breakthrough
of the Christian Reconstruction movement, and it will remain as
the integrating structure for the ICE-Dominion Press books.

Confidence or Arrogance?

In my Forewords, Introductions, and Prefaces to ICE and
Dominion Press books, 1 have expressed the opinion that our
theological opponents are incapable of either answering us or
developing a real-world, Bible-based alternative. This tactic is
quite self-conscious: I am trying to break the seminaries’ aca-
demic black-out by exasperating them. I try to get the critics to
reply in print. This tactic generally works, but it takes time and
a great deal of publication money. My targets eventually res-
pond, thus enabling me to publish two or three volumes dem-
onstrating why their responses proved my point: no answers.

This tactic has made me very unpopular. It has also raised
questions of propriety and good taste among our followers.
Should I make such public claims? Should I tell the whole
Christian world that what we have is better than anything they
have? I get letters warning me that such an attitude is unchris-
tian and evidence of my arrogance. Perhaps so. But I have a
model: David. He also made some seemingly outrageous claims,
and they were tied to the very thing I proclaim: biblical law.

I have more understanding than all my teachers: for thy testi-
monies are my meditation. I understand more than the ancients,
because { keep thy precepts (Psalm 119:99-100).

No one can accurately accuse David of having been a shrink-
ing violet. He blew his horn about as loudly as possible. He had
the theological goods, and he let everyone know this fact in no
uncertain terms. He had the law of God, and this placed him
above his teachers and the ancients. It also placed him above
any of his contemporaries who rejected God’s law.

142 CHRISTIAN RECONSTRUCTION

ship by revolution or rebellion. Instead, we are to work at our
callings, and wait on the Lord to place us in positions of influ-
ence, in His time.”

Gary North has called those who wish to advance the king-
dom by revolution “romantic revolutionaries.”* This is not a
recent emphasis in North’s writings, His first major book was
Marx’s Religion of Revolution, in which he insisted that “faithful
men will remain orderly in all the aspects of their lives; they are
not to create chaos in order to escape from law (Rom. 13; I Cor.
14:40). It is reserved for God alone to bring his total judgment
to the world.” In the biblical worldview, “it is God, and only
God, who initiates the change.”* North has pointed out repeat-
edly that the kingdom of God advances ethically as the people of
God work out their salvation with fear and trembling. Revolu-
tionaries are lawless. Their time frame is short. In fact, one of
Dr. North’s books, Moses and Pharaoh, is subtitled Dominion
Religion Versus Power Religion. Power Religion, he writes,

is a religious viewpoint which affirms that the most important goal
for a man, group, or species, is the capture and maintenance of
power. Power is seen as the chief attribute of God, or if the religion
is officially atheistic, then the chief attribute of man. This perspec-
tive is a satanic perversion of God’s command te man to exercise
dominion over all the creation (Gen. 1:26-28). It is the attempt to
exercise dominion apart from covenantal subordination to the true
Creator God.

What distinguishes biblical dominion religion from satanic power
religion is ethics.*

2. See David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, TX: Dominion Press, 1987), pp. 511-12; James B. jordan, “Rebellion, Tyranny,
and Dominion in the Book of Genesis,” in Gary North, ed., Tactics of Christian Resistance,
Christianity and Civilization No. 8 (Tyler, TX: Geneva Ministries, 1983), pp. 38-80.

3. Gary North, “Editor's Introduction," Christianity and Civilization, Number 9 (Sum-
mer 1983), pp. xxxii-xxxvii

4. North, Mary's Religion of Revolution (Nutley, NJ: The Craig Press, 1968), p. 99. This
same quotation appears in the revised second edition (1989) on page 86.

5. (lyler, TK: Institute for Christian Economics, 1985), p. 2. Dr, North distinguishes
among “Power Religion,” “Escapist Religion,” and “Dominion Religion” (pp. 2-5). He
makes it very clear that “Power Religion” is a militant religion that is unlawful and
counterproductive.

 

 

 

 

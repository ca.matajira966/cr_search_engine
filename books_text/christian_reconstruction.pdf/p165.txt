Is Revolution the Way to Advance God's Kingdom? 143

Still, the Bible teaches that we are at war, and that we must
prepare for it. The apostle Paul tells Christians to “put on the
full armor of God” (Ephesians 6:11). At first, even Pilate consid-
ered Jesus’ kingdom to be militaristic and political (John 18:28-
40). In Acts, the Christians were described as a sect preaching
“another king, Jesus” (Acts 17:7). These were the forerunners of
The People for the American Way. They said of the first-centu-
ty Christians, “These men who have upset the world have come
here also; and Jason has welcomed them, and they all act con-
trary to the decrecs of Caesar, saying that there is another king,
Jesus” (vv. 6-7). There was another king, but those outside of
Christ put a political and revolutionary slant on Christ's king-
ship.

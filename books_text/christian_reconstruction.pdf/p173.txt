Are Our Critics Honest? 151

no theological aberration in Christian Reconstruction’s adher-
ence to Calvinism.

Biblical Law

Reconstructionists believe that the whole Bible is the Chris-
tian’s guide for every area of life: from personal holiness to civil
righteousness. ‘This includes God's law as it is found in all the
Bible, not just “Old Testament Law” or the “Law of Moses.”

To orthodox Calvinism, the law of God is the permanent, un-
changing expression of God’s eternal and unchangeable holiness
and justice. .. . God could not change this law, or set it aside, in His
dealings with men, without denying Himself. When man sins, there-
fore, it is not God’s nature to save him at the law's expense. Instead,
He saves sinners by satisfying the law on their behalf*

The Bible teaches that Jesus satisfied the requirements of the
law in the sinner’s place and only brought about a change in
those laws that had specific reference to the redemptive work of
Christ and those institutions and ceremonies that were specific-
ally designed to keep Israel a separate people and nation (e.g.,
circumcision and food laws). The law as a blueprint for personal,
familial, ecclesiastical, and civil rightcousness was not abrogated
by the work of Christ. This is the Calvinistic tradition that goes
against Neuhaus’s claim that the views of Reconstructionists are
“an aberration of historic Christianity.”

Few Christians would deny that the “moral law,” as sammar-
ized in the ‘Ten Commandments, is still binding upon the be-
liever. But a question arises: How comprehensive is the moral
law? “You shall not murder” (Exodus 20:13) is a moral law that
has personal as well as social and civil applications. An individu-
al (personal application) is forbidden to murder another person
{social application), and the State has a duty to punish those
who commit murder (civil application) (Romans 13:4). The

 

Democratic Thought and Action (Nashville, TN: Abingdon Press, 1955).

9, J. 1. Packer, “The Redemption and Restoration of Man in the Thought of Richard
Baxter,” (1954), pp. 308-5. Quoted in Ernest F. Kevan, The Grace of Law: A Study in
Puritan Theology (London: The Carey Kingsgate Press Limited, 1964). pp. 67-68.

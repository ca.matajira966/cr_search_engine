What Is the Proper Response? 173

myself into?” They are in tight little groups of antinomian piet-
ists, and they have identified themselves as Reconstructionists.
Now the little group’s spokesman has publicly called us hereti-
cal, or worse, What to do? They feel they must respond, but
they don’t feel competent to do it. So, they expect one of us to
respond to the little group’s critic. They see this as our duty.
They do not recognize that we are increasingly coming under
fire from late-responding leaders of lots of little groups. We
dare not waste time responding to every newsletter attack, yet
each member of each little fringe group expects us to respond.

Residenis of Dispensational Gheitos

Those who have still not made the break from dispensation-
alist churches (mainly charismatics who have become postmil-
lennial) may even worry about what Dave Hunt has said re-
cently, even though Mr. Hunt is arguably the world’s second-
worst debater,” even though he has steadfastly refused to re-
ply to two volumes of detailed material aimed directly at
him,”' even though he is an accountant rather than a theolo-
gian, even though the man is too lazy to provide indexes for his
books. I had one person write to me telling me that I just had
to write a reply to Hunt's Whatever Happened to Heaven?, as if
Gary DeMar’s two volumes had not already publicly disembow-
eled Hunt. Why should I? It is not written only about Recon-
struction. He refuses to respond to DeMar, just as Ronald Sid-
er’s second edition of Rich Christians in an Age of Hunger failed
to reply to David Chilton’s Productive Christians in an Age of
Guilt-Manipulators. It should be clear to anyone who has read
both sides who won each of these debates. It was not our critics.
If they refuse to respond to our arguments, why is it our re-
sponsibility to repeat them again?

Ron Sider has had the wisdom to stop writing about world

20, After Tommy Ice. For proof, order a copy of the 1988 debate: Gary DeMar and
Gary North vs. Dave Hunt and Tommy Ice. Two audiotapes: $10; videotape: $30. Insti-
tute for Christian Economics, P 0. Box 8000, Tyler, TX 75711.

21. Gary DeMar and Peter J. Leithart, The Reduction of Christianity: A Biblical Response
to Deve Hunt (Ft. Worth, Texas: Dominion Pres, 1988); DeMar, The Debate over Christian
Reconstruction (Ft. Worth, Texas: Dominion Press, 1988).

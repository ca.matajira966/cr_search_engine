188 CHRISTIAN RECONSTRUCTION

public law or physical force, the more they must rely on private
moral restraint. Men, in a word, must necessarily be controlled,
either by a power within them, or by a power without them; either
by the word of God, or by the strong arm of man; either by the
Bible, or by the bayonet. It may do for other countries and other
governments to talk about the State supporting religion. Here,
under our own free institutions, it is Religion which must support
the State.*

“Choose for yourselves today whom you will serve. . .” (Joshua
24:15).

8. Cited in Verna M. Hall, ed., The Christian History of ihe American Revolution (San
Francisco, CA: Foundation for American Christian Eduction, 1976), p. 20.

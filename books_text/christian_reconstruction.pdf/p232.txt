210 CHRISTIAN RECONSTRUCTION

Majority Rule, 122
Minimal State, 92, 126
“Movement,” xi, 22, 81
Negative Sanctions, 126
Offense, xviii
Orthodoxy, 147-59, 183
Persecution, 70
Political Action, 125
Politics Fourth, 34, 77, 123-26
Postmillennialism, 82, 87-89,
128-31
Presuppositionalism, 82, 89-92,
148
Progressive Sanctification, 104
Recruiting, xii
Regeneration, 36, 77, 81, 83-
84
Revolution, 140-43
“Salvation by Legislation,” 124
Sanctification, 103-5
Self-Government, 154
Slavery, 10
Christian Scholarship, 1, 12
Christianity Today, 9, 10, 71n, 89,
175
Chrysostom, John, 155
Church
Excommunication, 5]
Kingdom, 27-30
Church of the Holy Trinity us. United
States (1892)
Churchill, Winston, 7
Civil Government, 126
Clapp, Rodney, 9, 10, 16
Colson, Charles (Chuck)
Anti-Christian Bigotry, 186-87
Biblical Restitution, 10
Mosaic Law, 17, 83
Political Iilusion, 123
Prison Reform, 17-18, 82-83
Reconstructionist, 10, 83
Schizophrenia, 17, 123
Common Ground, xix-xxi

Common Law (English), 153
Communist Eschatology, 178
Conquest, Robert, 7
Constitution, 10, 113, 120, 123
Cotton, John 120-21, 153, 156
Covenanters, 150
Covenant

Breaking, xi, xix, 63

Church, 56

Civil, 56

Curses, 57

Dominion, 56

Ethics, 50

Family, 56

Keeping, xi, 63

Law, 59

Lord, 58

Obedience, 58

Personal, 56, 77

Structure, 53.

Suzerain Treaties, 54

Symbols, 53

THEOS, 55
Craig, Hays, xi
Craig Press, xi, 178
Cranmer, Thomas, 149
Creation Science, 92
Creeds, 14
Cromwell, 69
Cultural Decline, 46
Cultural Isolation, 29

Dabney, Robert L., 130-31

Darby, J.N., 13

Darwin, Charles, 166

Dallas Theological Seminary, x,
22,71, 177

Davies, Marvyn, 1200

Davis, John Jefferson, 20-21

Davis vs. Beason (1890), 115

Decentralized Social Order, 92

Declaration of Independence, 120

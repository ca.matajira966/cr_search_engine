216 CHRISTIAN RECONSTRUCTION

North, Gary

Bottom-Up vs. Top-Down, 94-
95

Decentralization, 94

Dominion and Common Grace,
160

Dominion Covenant: Genesis, xv,
56n

Healer of the Nations, 78n

Hyper-dispensationalism, x

Introduction to Christian Econom-
ics (1973), xiv

Is the World Running Down?,
31, 49n

Israel, 132-33

journal of Christian Reconstruc-
tion, xiv, 1540

Mar’s Religion of Revolution
(1968), xi, 142n

Majority Rule, 122

Miullennialism and Social Theory,
171, 176, 179

Moses and Pharaoh, 95n

Political Polytheism, 15n, 34,
77, 114n, 172

Politics Fourth, 34, 77, 123-26

Postmillennialism, xi

Pyramid Society, 94-95

Religion, 49n, 142n

Slavery, 10

Swaggart, Jimmy, 65, 147

Tools of Dominion, 160n

Unconditional Surrender, xv,
40n

O’Hair, J.C., x

Oath, 186

One and the Many, x

Origin of Species, 166

Orthodox Presbyterian Church,
ix, xiii, 13

“Orthodoxy by Eschatology,” 14

Orton, William Aylott, 110n
Oss, Douglas O., 20
Owen, John 134-35, 153, 156

Paganism, 94
Paradigm Shifts, 42
Paraphrase of and Commentary on
the New Testament, 156
Parr, Elnathan, 134
“Partnership of Nations,” 46
Passantino, Bob and Gretchen, 9,
174
Passport Magazine, 11
Paul, Ron, xiv
Perkins, William, 134, 153, 157
Peters, Ted, 88
Philosophy of the Christian Curricu-
lum, xv
Pietism
Antinomianism, 173
Comprehensive Gospel, 35
Definition, 32
Dualism, 32
Evangelism, 35
Humanism’s Ally, 63, 77
Kingdom of God, 29
Neutrality, 51
Sanctions, 51
Satan’s World, 29, 32
World, 29
Pluralism, 85, 162, 172, 176, 184
Polish Church, 7
Politics
Activism, 35-36, 161
Arminianism, 150
Change, 160
Fourth, 34, 77, 123-26
Judgments, 52
Law, 126
Messianic, 47, 84, 124
Ministerial, 44, 52, 124-25
Religious, 45
Salvation, 123-26

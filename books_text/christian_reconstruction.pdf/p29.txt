Introduction 7

7:54-60). Few historians write about the Turkish massacre of the
Armenian Christians, described as “the first genocide of the
twentieth century.”"°

Maxim Gorky, the leading Soviet writer under Joseph Stalin,
invented the following formula to justify genocide: “If the ene-
my does not surrender, he must be destroyed.”"' Stalin spoke
openly about the “liquidation of the kulaks as a class.” “It
was the green light for a policy of extermination, more than
three years before Hitler came to power, twelve years before
the ordering of the ‘final solution’.”'* And who was a “kulak”?
“Anyone who employed hired labor . . . , anyone who owned
two horses or two cows or a nice house.” How many “kulaks”
were executed? No one knows for sure. Winston Churchill
wrote that Stalin told him that ten million peasants had been
dealt with: One-third murdered, one-third in concentration
camps, and one-third transported against their will to Siberia or
central Asia.

A large percentage of the generation that knew Joseph Stalin died
as a result of his directives. These were purely political killings, “ex-
terminations,” “liquidations” of “the enemy class” and “undesirable
elements. “How many were involved? Solzhenitsyn's estimates reach
as high as sixty million. Robert Conquest, author of The Great Terror,
fixed the number at well into the millions. It is doubtful if we will
ever know the true total —- God alone knows.*

Religious persecution dominated both the Soviet Union and
Nazi Germany. Not a few lost their lives in systematic purges.
Stalin and Hitler “had done everything in their power to de-
stroy the Polish Church. Hitler had closed its schools, universi-

10. Mikhail Heller and Alexsandr M. Nekrich, Utopia in Power: The History of the Soviet
Union from 1917 to the Present (New York: Summit Books, 1986), p: 236.

11. Idem.

12. Idem.

18. Paul Johnson, Modem Times: The World from the Tloenties to the Eighties (New York:
Harper & Row, 1983), p. 271.

14, Heller and Nekrich, Utopia in Power, p. 284.

15. Lloyd Billingsley, The Generation that Knew Not Josef: A Critique of Marxism and the
Religious Left (Portland, OR: Multnomah Press, 1985), p. 37.

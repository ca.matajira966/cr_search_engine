This book is dedicated to the memory of
Cornelius Van Til
whose expertise in epistemological

demolitions created a new movement
as a wholly unintended consequence.

The Myth of Neutrality 53

revival. It is time to prepare ourselves for a “changing of the
guard” — in every area of life, all over the world.Our prepara-
tion must help us to answer the hoped-for question of God-
fearing new converts to Christ: “I'm saved; what now?”

The Covenant Structure

To get the correct answers, we need first to ask the right
questions. For a long, long time, Christians and Jews have had
the right questions right under their noses, but no one paid any
attention. The questions concerning lawful government are
organized in the Bible around a single theme: the covenant.

Most Christians and Jews have heard the biblical word, “cov-
enant.” They regard themselves (and occasionally even each
other) as covenant people. They are taught from their youth
about God's covenant with Isracl, and how this covenant ex-
tends (or doesn’t) to the Christian church. Yet hardly anyone
can define the word. If you go to a Christian or a Jew and ask
him to outline the basic features of the biblical covenant, he will
not be able to do it rapidly or perhaps even believably. Ask two
Jews or two Christians to explain the covenant, and compare
their answers. The answers will not fit very well.

For over four centuries, Calvinists have talked about the
covenant. They are known as covenant theologians. The Puritans
wrote seemingly endless numbers of books about it. The prob-
lem is, nobody until 1985 had ever been able to come up with
“the” covenant model in the writings of Calvin, nor in the writ-
ings of all his followers. The Calvinists had always hung their
theological hats on the covenant, yet they had never put down
on paper precisely what it is, what it involves, and how it works
— in the Bible or in church history.

The Five-Point Covenant Model

In late 1985, Pastor Ray Sutton made an astounding discov-
ery. He was thinking about biblical symbols, and he raised the
question of two New Testament covenant symbols, baptism and
communion. This raised the question of the Old Testament’s
covenant symbols, circumcision and passover. What did they

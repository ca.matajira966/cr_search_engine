58 CHRISTIAN RECONSTRUCTION

by following God now, before His judgment begins. Obedience
to God’s principles produces leadership. Disobedience to God's
principles produces His judgment: man’s disinheritance from

God's riches, both in history and eternity.

If you don’t want to be disinherited, either eternally or on
earth, then start obeying God. Jesus said: “If ye love me, keep
my commandments” (John 14:15). This is a basic theme of the

New Testament:

But whoso keepeth his word, in him verily is the love of God
perfected: hereby know we that we are in him (I John 2:5).

Beloved, if our heart condemn us not, then have we confidence
toward God. And whatsoever we ask, we receive of him, because we
keep his commandments, and do those things that are pleasing in
his sight. And this is his commandment, That we should believe on
the name of his Son Jesus Christ, and love one another, as he gave
us commandment, And he that keepeth his commandments dwelleth
in him, and he in him. And hereby we know that he abideth in us,
by the Spirit which he hath given us (I John 3:21-24).

For this is the love of God, that we keep his commandments: and
his commandments are not grievous (I John 5:3).

Blessed are they that do his commandments, that they may have
right to the tree of life, and may enter in through the gates into the
city (Revelation 22:14).

The Lord of the Covenant

Jesus Christ is Lord of the covenant. He sits at the right
hand of God, governing history. He ascended to this lofty posi-
tion at His ascension, which occurred forty days after His resur-
rection from the grave. This was Peter's message on the day of

Pentecost:

This Jesus hath God raised up, whereof we all are witnesses.
Therefore being by the right hand of God exalted, and having
received of the Father the promise of the Holy Ghost, he hath shed
forth this, which ye now see and hear. For David is not ascended
into the heavens: but he saith himself, The Lord said unto my Lord,
Sit thou on my right hand, Until I make thy foes thy footstool.

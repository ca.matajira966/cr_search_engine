Chapter 7

POSTMILLENNIALISM’S “FAITH IN MAN”

Uphold me according unto thy word, that I may live: and let me not be
ashamed of my hope. Hold thou me up, and I shall be safe: and I will have
respect unto thy statutes continually. Thou hast trodden down all them that
err from thy statutes: for their deceit is falsehood. Thou puttest away all the
wicked of the earth like dross: therefore 1 love thy testimonies (Psalm
119:116-19). (emphasis added)

Again and again, premillennialists and amillennialists accuse
postmillennial Christian Reconstructionists of having too much
faith in man. This is somewhat amusing to Christian Recons-
tructionists, since the founders and present Icaders of the move-
ment are all Calvinists. They believe in the traditional Calvinist
doctrine of the total depravity of man — a doctrine denied by all
Arminians (“free will religion”), which the vast majority of
premillennialists and amillennialists are. The Calvinist believes
that there is nothing innately good in fallen man:

But we are all as an unclean thing, and all our righteousnesses
are as filthy rags; and we all do fade as a leaf; and our iniquities,
like the wind, have taken us away (Isaiah 64:6).

The heart is deceitful above all things, and desperately wicked:
who can know it? (Jeremiah 17:9)

For I know that in me (that is, in my flesh) dwelleth no good
thing: for to will is present with me; but how to perform that which
is good I find not (Romans 7:18).

106 CONSPIRACY: A BIBLICAL VIEW

Study Groups

You have read this book. You are convinced that something
needs to be done. You may not be certain just what needs to be
done, but you understand that the church needs to get in-
volved. But what if your pastor is unaware of the material in
this book? Give him a copy and ask him to evaluate it. See what
he thinks. He may never have heard of any of this before.

On the other hand, he may know more about it than he lets
on. He may know enough not to want to get involved. Almost
no pastor ever wants to deal with these issues from the pulpit.
It is risky. He may alienate some church members. If he were
to preach about the problem, it would also require him to come
up with concrete, biblical answers to the problems raised by this
book. Very few pastors believe that the Bible really and truly
speaks specifically to modern social, political, and economic
questions. (Example: ask your pastor what the biblical criticism
of the Federal Reserve System is. There is one.* Docs he know
what the answer is? Does he even know what the Federal Re-
serve System is? This is not the sort of question that most pas-
tors seek answers to in the Bible.)

Furthermore, most pastors hold a pessimistic view of the
future. They believe that Christian people will not be able to
reverse the world’s slide into chaos. They do not believe that
God has a program of comprehensive redemption which is
going to become visible across the earth before Jesus returns
physically and ends history at the final judgment. Such an era
of victory is what the Bible tcaches.’ Not many pastors believe
this, however. Since they think Christians cannot win, they
hesitate to get involved in a long-term battle against political

6. Gary North, Honest Money: The Biblical Blueprint for Money and Banking (Ft.
Worth: Dominion Press, 1986). This is sold by the Institute for Christian Economics.

7. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth,
Texas: Dominion Press, 1985). Sold by the Institute for Christian Economics.

  

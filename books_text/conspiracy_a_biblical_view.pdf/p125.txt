Replacing Evil With Good 109

spiracy’s challenge, then the Spirit of God will pass over this
generation just as surely as He passed over Moses’ generation,
which died in the wilderness. But if just a handful respond in
our day, and adopt a vision of victory, then we will be as Josh-
ua’s generation: victorious on all fronts. God chooses the foolish
of this world to confound the wise (I Corinthians 1:20). Gid-
con’s experience proves that it only takes a handful of dedicat-
ed righteous people to achieve a victory.

So what about you? What will you do? At the very least,
begin to monitor at least one of the special-interest groups of
self-anointed elite planners. The Appendix gives you the names
and addresses of several. Get on some mailing lists and see
what they are up to. Find out who they are, too. Know your
enemy. And then discipline yourself to become sufficiently
efficient to replace him, at least at the local level.

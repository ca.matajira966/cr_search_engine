118 CONSPIRACY: A BIBLICAL VIEW

You can also get on the mailing list of the Trilateral Com-
mission. All it takes is a letter to them. They publish Tiialogue,
Triangle Papers, and other books and materials. Send your
inquiry to

Trilateral Commission
345 East 46th Street
New York, NY 10017

On a much more personal level of recruiting is the Aspen
Institute, located in Aspen, Colorado. This organization is
aimed at business leaders, educators, and similar molders of
opinion. They publish books and papers.

The Aspen Institute for Humanistic Studies
1000 N. 3rd Street
Aspen, CO 81611

Outside the United States, one of the most important organi-
zations is the Royal Institute of International Affairs.
Prof. Quigley believed that it was this organization which more
or less served as the original model of the C.FR. This organiza-
tion is sometimes known as Chatham House, the building in
which it is housed, There is a sister organization in each of the
British Commonwealth nations.

‘The Royal Insticute of International Affairs
Chatham House

10 St. James Square

London, England SWLY 4LE

120 CONSPIRACY: A BIBLICAL VIEW

President but whose perpetrators’ actual motivation was never
clearly explained, we should be curious. If all that media cover-
age and the millions of dollars of government investigative
money did not reveal an acceptable answer as to why the break-
in occurred, think of the really important political events of
history. How can we make sense of them? How can we discov-
ered what really happened and why it happened? For example,
could the Watergate break-in really have been engineered by
John Dean in order to learn whether the Democrats had
learned of his new wife’s possible connection to a prostitution
ring, which is the thesis of Colodny and Gettlin’s Silent Coup (St.
Martin’s, 1991)? If so, then there was less to Watergate than the
investigators had imagined, and the fallout from it was remark-
able when compared to this information's importance to Rich-
ard Nixon.

The Watergate investigation became a media extravaganza
that seemed to elevate the reporter's calling to national status.
Yet some of the details of the Watergate investigation raise
questions that only hard-core conspiracy buffs ever ask. For
instance, we all know that Nixon was brought down because of
the White House audiotapes. But he refused to give up these
tapes in one fell swoop. In fact, not until 1996 were scholars
given access to these tapes. Only under specific demands by
government prosecutors did Nixon turn over limited sections of
those tapes. Gary Allen in 1976 summarized the findings of
Susan Huck’s February, 1975, article in American Opinion, the
publication of the John Birch Society. Ailen wrote in The
Kissinger File (p. 179):

Consider the fantastic detail involved in the requests. On
August 14th, for example, Judge Sirica demanded the “entire
segment of tape on the reel identified as ‘White House telephone
start 5/25/72 (2:00 PM.) (skipping 8 lines) 6/23/72 (2:50 P.M.)
(832) complete.’ ” I don’t know what all the identifying numbers
mean—but you have to agree that only somebody very familiar

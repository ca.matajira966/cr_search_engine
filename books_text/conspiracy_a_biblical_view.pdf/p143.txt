Bibliography 127

Billington, James. Fire in the Minds of Men: Origins of the Revolu-
tionary Faith. New York: Basic Books, 1980.

Bird, Kai. The Chairman. John J. McCloy. The Making of the Ameri-
can Establishment. New York: Simon & Schuster, 1992.

Burch, Philip. H. Elites in American. History. 3 vols. New York:
Holmes & Meier, 1980.

Callahan, David. Dangerous Capabilities: Paul Nitze and the Cold
War. New York: Edward Burlingame Book (HarperCollinsPub-
lishers), 1990.

Chandler, Lester V. Benjamin Strong: Central Banker. New York:
Arno Press, [1958] 1979.

Chernow, Ron. The House of Morgan: An American Banking Dy-
nasty and the Rise of Modern Finance. New York: Atlantic Monthly
Press, 1990.

Collier, Peter and Horowitz, David. The Rockefellers: An American
Dynasty. New York: Holt, Rinehart & Winston, 1976.

Current, Richard N. Secretary Stimson: A Study in Statecraft. New
Brunswick, New Jersey: Rutgers University Press, 1954.

Deacon, Richard. The Cambridge Apostles: A history of Cambridge
University’s élite intellectual secret society. London: Robert Royce,
1985,

Domhoff, G. William. Who Rules America Now?: A View for the
’80s. Englewood Cliffs, New Jersey: Prentice-Hall, 1983.

Duchene, Francois. Jean Monnet: The First Statesman of Inlerdepen-
dence. New York: Norton, 1994.

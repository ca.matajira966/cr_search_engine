22 CONSPIRACY: A BIBLICAL VIEW

representation? Which officer lawfully represents which group
of people in what specified capacity before which sovereign
lord? Until men understand and accept this biblical view of
government—sclf-government, family government, church gov-
ernment, and civil government—they will be tossed to and fro
by the conflicting winds of rebellious opinion.

God does not lodge absolute sovereignty in any human
institution. Men are sinners, and no single institution can safely
be entrusted with absolute sovereignty. Absolute power would
corrupt sinful men if it were available, but it isn’t. Nevertheless,
some men seek it, and this search is a sign of their corruption
and a means of corrupting them further.

The point is, conspiracies really do exist. People conspire
together to achieve evil ends. We use the word to describe a
confederacy which is sect up for illegal or immoral ends. It is a
confederation which aims at capturing power but without legiti-
mate authorization by God or other God-ordained, lawful insti-
tutions. (The American Revolution was justified by its propo-
nents by means of constitutional arguments. Congress was seen.
by them as a lawful confederation of lawfully ordained local
state assemblies against a British Parliament which was illegally
centralizing power and against a king who had capitulated to
Parliament and who was therefore in a conspiracy against the
colonial assemblies and traditional common law liberties.)*

Anti-Conspiracy Theories

There are numerous “establishment” theories that are used
by conventional historians to explain the past: technological
determinism, psychological determinism, economic determin-
ism, geographical determinism, and random indeterminism. To
this, add political history, military history, population history,
the history of ideas, and too many other subfields to mention,

3. R. J. Rushdoony, This Independent Republic: Studies in the Nahere and Meaning of
American History (Fairfax, Virginia: Thoburn Press, [1964] 1978), chaps. 2, 3.

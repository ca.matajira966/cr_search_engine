This book is dedicated to

Antony Sutton
Otto Scott

who did their homework,
published their findings,
and paid the price.

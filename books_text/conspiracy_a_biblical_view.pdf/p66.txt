50 CONSPIRACY: A BIBLICAL VIEW

How could this earnest-looking man in his late thirties have
devoted himself to such a preposterous task, to prove that the
UN offers anyone hope in anything except, possibly, a bureau-
cratic job in some UN agency? How could Adlai E, Stevenson,
Hubert H. Humphrey, Henry Cabot Lodge, and Sen. Jacob
K. Javits have all written blurbs for it on the back of the dust
jacket? There is only one reasonable conclusion: none of them
actually read this pathetic little book, with its dry, brief descrip-
tions of endless UN agencies (GATT, UNCSAT, ICAO, WHO,
FAO, etc., etc.), and its hopeful chapter titles, such as “Turning
Point in World Trade” and “Solving the Monetary Dilemma.”
It took a Rhodes scholar to write this?

Books make them look scholarly. Books make them look
respectable. Books make them look like a bunch of academics,
meaning powerless, Adam Weishaupt, founder of the Illumin-
ati, an important late-cighteenth-century secret revolutionary
socicty,” recognized the importance of books very early. He
laid down guidelines concerning the proper concealment of a
secret society. No principle was more important than looking
unimportant. Books were part of this cover.

The great strength of our Order lies in its concealment; let it
never appear in any place in its own name, but always covered
by another name, and another occupation. None is fitter than the
three lower degrees of Free Masonry; the public is accustomed to it,
expects little from il, and therefore takes little notice of it. Next to this,
the form of a learned or literary society is best suited to our
purpose, and had Free Masonry not existed, this cover would
have been employed; and it may be much more than a cover, it
may be a powerful engine in our hands. By establishing reading societies,
and subscription libraries, and taking these under our direction, and
supplying them through our labours, we may turn the public mind which
way we will, ...A Literary Society is the most proper form for

 

2. James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith
(New York: Basic Books, 1980), pp. 93-99.

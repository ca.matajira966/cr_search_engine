54 CONSPIRACY: A BIBLICAL VIEW

The climate of opinion in America is shifting. Ask yourself:
“What is more important, the shift in the climate of opinion, or
the new facts that bring the book up to date?” If you agree with
me about the importance of widely shared ideas, you will say,
“the shift in the climate of opinion is more important than the
new edition.” In other words, it is the big picture that dom-
inates—the shift in opinion of millions of Americans, as well as
a growing minority of articulaie intellectuals—rather than the effects
of some paperback books of the early 1970's. (Unless you want
to argue that the earlier books were the primary cause of the
shift in the climate of opinion—and I don’t chink any of the
authors is arrogant enough to say it, or stupid enough to think
it.)

The Climate of Opinion

Let me put it another way. If some conspirator had put a
Mafia contract on Gary Allen, Larry Abraham, and W. Cleon
Skousen in 1968, and they had all been murdered, and if None
Dare and Naked Capitalist had never been published, would the
shift of opinion still be going on? Almost certainly.

All right, let’s take it one step farther. Do you think that if
someone had put a bomb into a room filled with Rockefellers,
Carnegics, and the other conspirators of 1913, no matter how
early in the game, do you think their deaths would have
stopped the crosion of Constitutional libertics? Yes? No? May-
be? I did not say slowed the erosion; I said stopped.

I'll bet you don’t believe that a bomb would have stopped it.
Shoot one enemy, and another one appears, on either side of the
conflict. Why? Because it is the struggle—a religious struggle over
the acceptable world-and-life view, the first principles of soci-
ety—which is central, not the specific conspirators. It is the
script, not the players, which is central.

Yes, there are important participants. John D. Rockefeller,
Jr. was one. J. Pierpont Morgan was one. “Col.” E. M. House
was onc. But if all three had died at age ten, would the fight

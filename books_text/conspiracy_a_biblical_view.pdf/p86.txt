70 CONSPIRACY: A BIBLICAL VIEW

“hard” sciences like chemistry and physics, although the pres-
ence of jobs in private industry—free-market, profit-seeking
firms—reduces the level of control. Consider: Who hands out
the big moncy for research projects? The Federal government
and private foundations.)

The manipulators offer young, aggressive, ambitious men
the necessary contacts.

The Ethics of Babel

There is yet another factor which seals the alliance: shared
presuppositions. The manipulators, like the academics, are usually
ethical relativists. They believe that there are no permanent
standards of national morality. There are only “interests.” In-
terests are shifting and temporary. (And who is best equipped
to interpret changing conditions, identify today’s “true” nation-
al interests, and formulate the appropriate national policies?
Guess who.)

Check the title of Robert Osgood’s (C.F.R.} 40-year-old, but
still in print college text, which defends our C.F.R.-controlled
foreign policy: ideals and Self-Inierest in America’s Foreign Relations
(University of Chicago Press, 1953). You get the picture. The
book’s subtitle is also interesting: The Great Transformation of the
Twentieth Century. Indeed, it was!

Don’t these scholars at least understand that nations, like
individuals, are marked by certain ultimate commitments?
Don't they understand that there are fundamental principles
that divide men permanently? Don’t they understand that the
story of the Tower of Babel really tells us a fundamental truth
about the limits on men’s ability and willingness to join together
politically? No, they don’t. There is only one overarching ethi-
cal premise for them, one which unifies all mankind: the brother-
hood of man. This must eventually lead to a new community of
man. It must lead to a New World Order.

What Osgood wanted was a New World Order, although this
term was not in use in academic circles back in 1953. He want-

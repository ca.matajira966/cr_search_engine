74 CONSPIRACY: A BIBLICAL VIEW

steps to reduce their vulnerability to the manipulators. Clearly,
outraged rabble-rousing voters can fight a self-interested group
of monopoly-seekers a lot easier than they can fight “the forces
of production” or “the Volk.” (Have you ever heard that “the
forces of production” have been convicted for having attempted
to bribe a U.S. Senator?)

But once in a while, some bright inner circle member gets
out of line. He just can’t keep a secret any more. That is the
trouble with scholars: their careers are made by discovering
some new fact or other, and yet they are supposed to know
when to keep certain facts secret. Sometimes they forget. It can
be very embarrassing.

80 CONSPIRACY: A BIBLICAT. VIEW

the air, reading directly from Quigley’s book, Quigley hung up.
Elapsed time: Jess than two minutes. So much for extended
scholarly debate.

It seems clear in retrospect that Quigley never expected the
book to become the source of ammunition for the conservatives,
nor did Macmillan. I doubt that Quigley knew what he was
getting into when he began the project in the mid-1940’s, when
he staried doing the research. That Macmillan refused to re-
ptint it indicates outside pressure. The book was a mistake
from the perspective of those exposed. Whatever their motives
for allowing him access to documentary material (which he
claimed that they had),° they later changed their minds about
the wisdom of this. Or perhaps they never expected him to
write a book using their materials. After all, he had never pub-
lished anything controversial before, and it was late in his aca-
demic career.

In the late 1970’s, Gary Allen received an unsigned letter.
The envelope was postmarked “Washington, D.C.” I have seen
it and the envelope. The sender said that he had been a friend
of Quigley’s, and that at the end of his life, Quigley had con-
cluded that the people he had dealt with in the book were not
really public benefactors, as he had believed when he wrote it.
According to the anonymous writer, Quigley had come to think
of them in the same way that Allen did, and that Quigley had
been very fearful of reprisals toward the end of his life. I be-
lieve the letter-writer.

James Billington
Quigley’s scholarship was matched by James Billington’s ac-
count of revolutionary movements in the period 1789 through
1917. Billington’s Fire in the Minds of Men: Origins of the Revolu-
tionary Faith (Basic Books, 1980) is nothing short of a master-

5. Carroll Quigley, Tragedy and Hope: A History of the World in Qur Time (New
York: Macmillan, 1966), p. 950,

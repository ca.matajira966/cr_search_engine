The Structure of the Book of Leviticus 21

twenty-eight occurrences in Leviticus 17~22, indicates that chapter
19 may be (and I shall argue is) a third commandment section
within the larger section. Leviticus 17-22 is the third command-
ment section within Leviticus, and Leviticus is the third com-
mandment section in the Pentateuch. Arguably, then, Leviticus
19 is the heart of the book, in this aspect.

‘The fourth aspect of the covenant is the promise of future evalua-
tions, to entail either blessings or curses depending on the steward’s
performance. The sabbath (fourth commandment) is the time of
God’s meeting with His people for judgment, and consequent rest.
The ninth commandment also has to do with judgment, though with
human courts. Sabbaths are the concern of Leviticus 23.

The final aspect of the covenant has to do with its continuance
into the future. Here are found arrangements for deposition, ap-

Creations Command- Command-

Covenant ments 1-5 ments 6-10 Leviticus

Transcendence/ No other No man- Chapters 1-7

Relationship/ Gods slaughter Sacrifices

Immanence God-man relationship
No other gods

Restructuring/ No other No adultery Chapters 8-16

New hierarchy mediator Creation and fall

Defilement and cleansing

The judgment of man
recapitulated

Mediation and access

Distributing? Don't wear No stealing Chapters 17-22

Stipulations God's Name God’s property and holiness
emptily Perfection, not vanity,
required
God’s Name
“cutting off”
Evaluations/ Sabbath False witness Chapter 23
Judgments in court Sabbaths
Festivals
Succession/ Honor No coveting; Chapters 24-27
New adminis- parents, honor God Historical overview for the
trators/Artistic so that you —_with your fature

enhancements _ live long money Payment of monetary vows

26 Covenant Sequence in Leviticus and Deuteronomy

the order of creation in Genesis 2. In a general way, Moses is
presented in Exodus as a new Adam to lead the people. The
building of the Tabernacle was the erection of a new Garden. The
system of animal sacrifices just considered was the “bringing” of
the animals to Adam.

Gen, 2:7 formation of Adam _calll of Moses
Gen. 2:8 planting of Garden _ erection of Tabernacle
Gen. 2:19 animals for Adam _ sacrifices of Lev. 1-7

Now, however, Aaron is set up as another “new Adam” in the
completed Garden, and we find the sequence repeated. If the
Tabernacle is a new Garden-sanctuary for God, then the High
Priest is a new Adam. The consecration of Aaron as High Priest in
Leviticus 8 parallels in several ways the creation of Adam in Gen-
esis 2. This re-creation episode is highly complex and symbolic,
and thus no simple repetition of the creation of Adam. All the
same, there are enough parallels to enable us with confidence to
assert that the creation of a new Adam is the heart of Leviticus 8.

The noteworthy events of Genesis 2 are these:

. Formation of Adam (v. 7a)

. Spirit’s quickening of Adam (v. 7b)

. Creation of the Garden-sanctuary (v. 8)

. Establishment of Adam as guard (v. 15)

. Creation of helper fit for Adam (v. 18)

. Bringing of animals to Adam (v. 19) .

. A command to obey on pain of death (v. 17)

NOuUPWNE

In Leviticus 8 we find the same events, though not all in the
same order. Also, by this stage of redemptive history, men who
carry the office of headship are invested with robes of office, and
are not naked as was Adam (cf. Gen. 9:5-6, 21-23).3 The sacrifi-
cial system has been revealed. Oil has come in as a symbol of the

3. See James B. Jordan, “Rebellion, Tyranny, and Dominion in the Book of
Genesis,” in Gary North, ed., Tactics of Christian Resistance. Christianity & Civili-
zation 3 (Tyler, TX: Geneva Ministries, 1983).

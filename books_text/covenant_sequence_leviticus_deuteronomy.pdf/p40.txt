36 Covenant Sequence in Leviticus and Deuteronomy

fifth area of the covenant, considered in its five pentateuchal
aspects.

Chapters 21-22 appear to follow the pentateuchal pattern also.
The holiness or integrity of the priests, imaging the transcendence
of God, is the theme of 21:1-15. The question of access is the theme
of 21:16-24. Leviticus 22:1-16 revolves around the profanation of
God’s Name, His grant of gifts to the priests, and the problem of
uncleanness, which separates the priests from the gifts. In 22:17-25
we are told what kinds of defects disqualify animals from accept-
ability. This requires judgment and evaluation. Finally, 22:26-33
focusses on temporal questions, matters having to do in a general
way with succession,

Leviticus 23

Chapter 23, on sabbaths, itself the fourth section of the book,
has five speeches and nine topics. From what we have seen thus
far in Leviticus, we would be surprised if these did not in some
way correlate with the sequence of the covenant.

Speech 1:
1, Sabbaths, 1-3
Affirming God’s sovereignty
2. Passover, 4-8
Affirming deliverance, restructuring

Speech 2:
3. First-sheaf, 9-14
Thanksgiving for transition into the land
4, Firstfruits (Pentecost), 15-22
Thanksgiving for possession of the land

Speech 3:
5. Trumpets, 23-25
Convocation, Holy War, Judgment!’

15. In the book of Revelation, the blowing of trumpets has to do with the proc-
lamation of judgment. See David Chilton, Days of Vengeance: An Exposition of the
Book of Revelation (Fort Worth: Dominion Press, 1987), pp. 17f., 225f., ¢t passim.

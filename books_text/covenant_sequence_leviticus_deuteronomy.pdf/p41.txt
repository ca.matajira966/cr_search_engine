The Structure of the Book of Leviticus 37

Speech 4:
6. Day of Atonement, 26-32
Restoration (cp. Lev. 16)

Speech 5:
7. Feast of Tabernacles, 33-36
Festivity, artistic enhancement

8. Food offerings, summary, 37-38

9, Tabernacles as festival of remembrance and continuity,
39-44

It does not appear that the five speeches correlate precisely with
the five aspects of the covenant sequence. More generally, though,
the covenant sequence can be seen in the Israelite calendar, and in
the order of presentation in Leviticus 23. The sabbath affirms
God's sovereignty. Passover can be related to the first command-
ment (“who brought you out of the land of Egypt”) and also with
the second aspect of the five-fold sequence (transition). First-sheaf
definitely celebrates the transition. Thus these first three items
move through the first two stages of the covenant considered as
five stages. Firstfruits celebrates the actual possession of the land,
and, as Pentecost, is also the festival of the giving of the law, since
the law was given on Pentecost.'® This is stage three. Trumpets
has to do with war and judgment, stage four, The Day of Atone-
ment has to do with the clearing of sin and the possibility of resto-
ration for the future. In a sense it stands between stages four and
five. Finally, Tabernacles relates to the future, both as the feast of
the seventh (eschatological) month, and in that it involves rest
and festivity.

The actual effect of this is to produce a seven-fold sequence of
the covenant:

16. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23
(Tyler, TX: Institute for Christian Economics, 1984), p. 58.

46

Section

16.

7.

Pivot:

II:1a.

Covenant Sequence in Leviticus and Deuteronomy

Verse Numbered
Law

14. 24, You shall not curse the deaf.
25, And in front of the blind you shall not put a
stumbling block.
26. And you shall fear your God.
J am the Lor,

15. 27. You shall not do a perversion of the justice.
28. You shall not lift the face of the poor.
29. And you shall not favor the face of the great.
30. In fairness you shall judge your fellow citizen.

16. 31. You shall not walk about slandering among
your people.
32. You shall not stand against the blood of your
neighbor.
Zam the Lorp.

17. 33. You shall nat hate your brother.
34. With your heart you shall most certainly
rebuke your fellow citizen.
35. And you shall not share guilt with him.

18. 36. You shall not seek revenge.
37, And you shall not bear a grudge against the
sons of your people.
38. And you shail love your neighbor as yourself.
Tam the Lor.

19a, 39. | My decrees you shall guard.

19b. 40. Your animal: you shall not mate two kinds.
41, Your field: you shall not plant two kinds of seed.
42. And clothing: two kinds woven together you
shall not wear on yourself.

20. 43. Andaman: if he sleeps with a woman ~emis-
sion of seed—and she is a slave-girl promised
to another man,

And she has not been formally ransomed [being
ransomed she has not been ransomed],

Or freedom has not been given to her,
Damages must be paid for her.

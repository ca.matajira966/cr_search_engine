56 Covenant Sequence in Leviticus and Deuteronomy

Section IT:6 (vv. 33-34) has three laws stating that the alien
must be treated well. This is very similar to Exodus 23:9, which
falls in the section on the ninth commandment.®

Section 11:7 (vv. 35-36) has two laws requiring honest weights
and measures. I associate this with covetousness, the tenth com-
mandment. In Deuteronomy this law is found in the tenth com-
mandment section (Dt. 25:13-15, see p. 67 below). It is fitting that
the covenant rehearsal of Leviticus 19 should end with the last
commandment.

Ihave made attempts to correlate this second set of seven with
the days of creation and the heptamerous patterns in Leviticus 23
and 24-27. I have not been able to come up with anything worth
sharing, and have concluded for the present that no such correla-
tion is intended.

Conclusion

Leviticus 19 is a very profound and complex commentary on
the law. If my analysis is in general correct, the passage seems in-
tended to create new insights at every point. Those parts of the
law that seem most civil and open are dealt with largely in the first
part of the chapter. In this way, such violations of the second table
as would be hidden from view are dealt with. Similarly, the first
table is dealt with more largely in the second half of the chapter,
which concerns crimes that are done openly.

Additionally, by combining commandments within a section,
by using new vocabulary to express the commandments, by treat-
ing the commandments in unusual sequences, and by using a
double heptamerous organization, Leviticus 19 causes us to medi-
tate on deeper ethical analogies. Finally, at the heart of the chap-
ter (vv. 19-25) we find a long, complex, symbolic section dealing
with the primacy of God’s claims.

6, Jordan, Law of Covenant, p. 66.

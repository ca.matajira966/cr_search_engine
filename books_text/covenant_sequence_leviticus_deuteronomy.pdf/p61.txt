4

THE STRUCTURE OF THE
BOOK OF DEUTERONOMY

The Book of Deuteronomy is a covenant renewal document.!
It sets forth the second covenant renewal of the Sinaitic covenant.
The first renewal came after the incident of the Golden Calf,
when Moses shattered the tables of the covenant, and then God
wrote them again.” The second renewal was rendered necessary
by Israel’s refusal to obey God and enter Canaan, which apostasy
resulted in the wilderness wanderings. With the dying off of the
apostate generation, God was ready to lead Israel into Canaan
once again.

Covenant/Re-creation Pattern in Deuteronomy

A. Taking Hold— Transcendence — Initiation, 1:1-5

B. Historical Overview— Breakdown and Renewal of Order,
1:6-4:43

C, Stipulations— Given with view to the coming Distribution of
the Land, 4:44-26:19

D. Sanctions — Witnesses, 27-30

E. Succession — Rest— Enhancements — Continuity, 31-34.

4. See Meredith G. Kline, The Structure of Biblical Authority, 2d ed., (Grand
Rapids: Eerdmans, 1975), esp. chap. 1:2, “Dynastic Covenant.”

2. Moses reviews this in his sermon on the first commandment (Dt. 6-11),
especially 7:17-26 with 9:7-10:11. Moses states that the covenant breakdown and
renewal carried with it the replacement of the firsthorn with the Levites as
priests, of Aaron with Eleazar (eventually), and of the open tablets of stone with
new tablets hidden in an ark, The renewed covenant put God at a greater dis-
tance from the people (separated by Levites and by the ark-box), but this served
for the people's protection as well — protection from the wrath of God.

57

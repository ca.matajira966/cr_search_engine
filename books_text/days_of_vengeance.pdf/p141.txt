THE SPIRIT SPEAKS TO THE CHURCH: OVERCOME! 2:14-16

9, 24), As St. John will reveal in Chapters 12-13, Satan is the
moving force behind the Jewish/Roman attempt to destroy the
Church.

The close relationship in Pergamum between organized Jud-
aism and the imperial officials, combined with Christianity’s op-
position to statism and the worship of the creature, made it only
natural that persecution and martyrdom would begin here, if
anywhere in Asia. And on this account, Christ regards the
church at Pergamum as faithful: They hold fast to His name —
confessing Him alone as Savior, Mediator and Lord, proclaim-
ing that His identity as the link between heaven and earth was
absolutely unique. They did not deny the faith, even when bitter
persecution came in the days of Antipas... who was killed
among you, where Satan dwells. No one now knows who this
Antipas was, but it is enough that Christ singles him out for spe-
cial acknowledgment: My faithful witness, He calls him. By his
very name—Against Al— Antipas personifies the steadfastness
of the Pergamene church in resisting persecution.

14-16 Yet not all in the church were of the faithful character
of Antipas; moreover, a threat that posed a danger to the integ-
rity of the faith, even greater than the danger of persecution, is
the sly, insidious working of heresy. St. John draws on the his-
tory of the Church in the wilderness to illustrate his point: You
have there some who hold the teaching of Balaam, whose name
means, like Nikolaos, Conqueror (or Destroyer) of the people.
When it was discovered that the people of God could not be
defeated in open warfare (see Num. 22-24), the false prophet
Balaam suggested another plan to Balak, the evil King of Moab.
The only way to destroy Israel was through corruption. Thus
Balaam kept teaching Balak (cf. Num. 31:16) to put a stumbling
block before the sons of Israel, to eat things sacrificed to idols,
and to commit fornication (cf. Num. 25}.!* Thus you also have
some who in the same way —i.e., in imitation of Balaam—hold
the teaching of the Nicolaitans: In other words, those who hold
the teaching of Balaam and those who hold the teaching of the
Nicolaitans (cf. 2:6) comprise the same group. The church in
Pergamum was standing steadfastly for the faith when it came

16, Josephus provides an expanded version of the story in his Antiquities af
the Jews, iv.vi.6.

107

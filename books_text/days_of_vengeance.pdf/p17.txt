PUBLISHER’S PREFACE

Old Testament symbols that he has sifted from the historical
narratives and the descriptions of the Tabernacle and Temple.
Then he applies these symbols and models to other parallel Bible
stories, including the New Testament’s account of the life of
Christ and the early church. No one does this better than Jor-
dan, but Chilton has successfully applied this Biblical hermen-
eutic (principle of interpretation) to the Book of Revelation in
many creative ways. Chilton is not the first expositor to do this,
as his footnotes and appendixes reveal, but he is unquestionably
the best at it that the Christian church has yet produced with
tespect to the Book of Revelation. These Old Testament back-
ground stories and symbols make sense of the difficult passages
in Revelation, He makes clear the many connections between
Old and New Testament symbolic language and historical
references. This is why his commentary is so easy te read,
despite the magnitude of what he has accomplished academically.

The Missing Piece: The Covenant Structure

There was a missing piece in the puzzle, however, and this
kept the book in Chilton’s computer for an extra year, at least.
That missing piece was identified in the fall of 1985 by Pastor
Ray Sutton. Sutton had been seriously burned in a kitchen acci-
dent, and his mobility had been drastically reduced. He was
working on a manuscript on the symbolism of the sacraments,
when a crucial connection occurred to him. The connection was
supplied by Westminster Seminary Professor Meredith G. Kline.
Years earlier, he had read Professor Kline’s studies on the an-
cient suzerainty (kingly) treaties of the ancient Near East.3
Pagan kings would establish covenants with their vassals. Kline
had pointed out that these treaties paralleled the structure of the
Book of Deuteronomy. They had five points: (1) an identifica-
tion of the king; (2) the historical events that led to the establish-
ment of the covenant; (3) stipulations (terms) of the covenant;
(4) a warning of judgment against anyone who disobeyed, but a
promise of blessing to those who did obey; and (5) a system of
reconfirming the treaty at the death of the king or the vassal.

3. Kline, Treaty of the Great King (Grand Rapids: Eerdmans, 1963);
reprinted in part in his later book, The Structure of Biblical Authority (Grand
Rapids: Eerdmans, 1972).

xvii

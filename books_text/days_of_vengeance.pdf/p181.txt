THE THRONE ABOVE THE SEA 4:1

placed in 19:11), which in turn proves that the Church has been
raptured and is absent, in heaven, away from ali the excitement —
all because the word “Church” is missing! On the basis of such a
curious principle of interpretation we could say with assurance
that Revelation doesn’t tell us anything about Jesus either until
chapter 12, because the name “Jesus” does not occur until then
(thus “the Lion of the tribe of Judah” and “the Lamb that was
slain” [5:5-6] must be terms for someone else).* Of course, this
method of interpretation involves even more problems for the Dis-
pensationalist: for the word “Church” never again appears in the
entire Book of Revelation at ail! This interpretation of the words
Come up here does not, therefore, support the pretribulation
rapture of the Church; it possibly even teaches the pretribulation
annihilation of the Church. After the last verse in Revelation 3,
the Church simply disappears, and is never heard from again.
Obviously, this is not true. The Church is known by numer-
ous names and descriptions throughout the Bible,’ and the mere
fact that the single ferm “Church” does not appear is no indica-
tion that the concept of the Church is not present. Those who
see in this verse some “rapture” of the Church are importing it
into the text. The only one “raptured” is St. John himself, The
fact is that St. John only uses the word Church with reference to
particular congregations—noft for the whole body of Christ.
Nevertheless, we must also recognize that St. John does as-
cend to a worship service on the Lord’s Day; and this is a clear
image of the week/y ascension of the Church into heaven every
Lord’s Day where she joins in the communion of saints and
angels “in festal array” (Heb. 12:22-23) for the heavenly liturgy.
The Church acts out St. John’s experience every Sunday at the
Sursum Corda, when the officiant (reflecting Christ's Come up
here!) cries out, Lift up your hearts! and the congregation sings

4, This principle can be fruitfully applied elsewhere in Scripture as well. For
example, the word /ove does not appear anywhere in the Book of Ruth; thus
her story Wurns out not to be, after all, one of the greatest romances in the
Bible, for Boaz and Ruth did not love each other. Again, the word God does
not appear in the book of Esther; on these principles, He must not have been
involved with those events, and the book must not tell us anything about Him.
In addition, the first fifteen chapters of Pauls letter to the Romans doesn’t
concern the Church, for the word Church doesn't appear there cither!

5. Paul Minear lists ninety-six of them in the New Testament alone: Images
of the Church in the New Testament (Philadelphia: The Westminster Press,
1960), pp. 222ff., 268f.

147

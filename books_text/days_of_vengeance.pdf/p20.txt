THE DAYS OF VENGEANCE

Tyler leaders of the Christian Reconstruction movement. Theo-
logically speaking the original “four points of Christian Recon-
structionism” that Chilton and I have summiarized®—prov-
idence (sovereignty of God), Biblical presuppositionalism (Van
Til’s apologetics: the Bible is the starting point and final court of
appeal), eschatological optimism (postmillennialism), and Bibli-
cal law (theonomy)—were insufficient. The fifth point, cove-
nantalism, and specifically Sutton’s five-point model, was added
in late 1985 to complete the theological outline.

The Days of Vengeance is especially concerned with the Reve-
lation’s covenant structure and the historical focus of its judg-
ment passages. If, as Chilton argues so brilliantly, these passages
of imminent doom and gloom relate to the fall of Jerusalem in 70
A.D., then there is no legitimate way to build a case for a Great
Tribulation ahead of us. It is long behind us. Thus, the Book of
Revelation cannot legitimately be used to buttress the case for
eschatological pessimism. A lot of readers will reject his thesis at
this point. The ones who are serious about the Bible will finish
reading it before they reject his thesis.

Pessimism

The vast majority of Christians have believed that things will
get progressively worse in almost every area of life until Jesus
returns with His angels, Premillennialists believe that He will es-
tablish an earthly visible kingdom, with Christ in charge and
bodily present. Amiilennialists do not believe in any earthly visi-
ble kingdom prior to the final judgment. They believe that only
the church and Christian schools and families will visibly repre-
sent the kingdom on earth, and the world will fall increasingly
under the domination of Satan.’ Both eschatologies teach the
earthly defeat of Christ's church prior to His physical return in
power.

One problem with such an outlook is that when the predict-
able defeats in life come, Christians have a theological incentive
to shrug their shoulders, and say to themselves, “That’s life.
That’s the way God prophesied it would be. Things are getting

6. Gary North and David Chillon, “Apologetics and Strategy,” Christianity
and Civilization, 3 (1983), pp. 107-16.

7. Gary North, Dominion and Common Grace (Tyler, Texas: Institute for
Christian Economics, 1987), especially chapter 5.

KX

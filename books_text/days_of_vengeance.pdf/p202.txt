$114 PART THREE; THE SEVEN SEALS

at Sinai (1:6). The theme of the book, stated in 1:7, is Christ’s
coming in the Glory-Cloud; then, almost immediately, St. John
uses three words that almost always occur in connection with
covenant-making activity: Spirit, Day, and Voice (1:10). The fol-
lowing vision of Christ as the glorious High Priest (1:12-20)
combines many images from the Old Testament — the Cloud, the
Day of the Lorp, the Angel of the Lorn, the Creator and uni-
versal Sovereign, the Son of Man/Second Adam, the Con-
queror of the nations, the Possessor of the Church—all of
which are concerned with the prophecies of the coming of the
New Covenant. The vision is followed by Christ’s own message
to the churches, styled as a recounting of the history of the Cov-
enant (Chapters 2-3), Then, in Chapter 4, St. John sees the
Throne, supported by the Cherubim and surrounded by the
royal priesthood, all singing God’s praises to the accompan-
iment of Sinai-like lightning and voices and thunder. We should
not be surprised to find this magnificent array of covenant-
making imagery culminating in the vision of a testament/treaty
document, written on front and back, in the hand of Him who
sits on the Throne. The Book is nothing less than the Testament
of the resurrected and ascended Christ: the New Covenant.

But the coming of the New Covenant implies the passing
away of the Old Covenant, and the judgment of apostate Israel.
As we saw in the Introduction, the Biblical prophets spoke in
terms of the covenantal treaty structure, acting as prosecuting
attorneys on behalf of the divine Suzerain, bringing covenant
lawsuit against Israel. The imagery of the document inscribed
on both sides is used in the prophecy of Ezekiel, on which St.
John has modeled his prophecy. Ezekiel tells of receiving a scroll
containing a list of judgments against Israel:

Then He said to me, “Son of man, I am sending you to the
sons of Israel, to a rebellious people who have rebelled against
Me; they and their fathers have transgressed against Me to this
very day... .” Then I looked, and behold, a hand was extended
to me; and lo, a Book was in it, When He had spread it out be-
fore me, it was written on the front and back; and written on it
were lamentations, mourning and woe. (Ezek. 2:3-10)

As St. John sees the opening of the New Covenant, there-
fore, he will also see the curses of the Old Covenant fulfilled on

168

THE DAYS OF VENGEANCE

silence engulfed the formerly vociferous critics. Now comes The
Days af Vengeance. The silence will now become deafening.
Few critics will reply iz print, 1 suspect, though if they refuse to
reply, they have thereby accepted the validity of the coroner’s
report: death by strangulation (footnotes caught in the throat).

Oh, there may be a few hastily written book reviews in un-
read Christian scholarly journals. Dallas Seminary’s Prof.
Lightner may write one, like the one-page bit of fluff he wrote
on Paradise Restored, in which he said, in effect, “See here, this
man is a postmillennialist, and you need to understand that we
here at Dallas Seminary aren’t!”!! There may be a few brief dis-
paraging remarks in popular paperback books about the insigni-
ficant and temporary revival of full-scale dominion theology.
But there will be no successful attempt by scholarly /eaders of
the various pessimillennial camps to respond to Chilton. There
is a reason for this: They cannot effectively respond. As we say
in Tyler, they just don’t have the horses. If 1 am incorrect about
their theological inability, then we will see lengthy, detailed ar-
ticles showing why Chilton’s book is utterly wrong. If we don’t
see them, you can safely conclude that our opponents are in
decp trouble. To cover their naked flanks, they will be tempted
to offer the familiar refrain: “We will not dignify such preposter-
ous arguments with a public response.”

That is to say, they will run up the intellectual white flag.

Chilton’s critics will have a problem with this silent ap-
proach, however. The problem is Professor Gordon Wenham,
who wrote the Foreword. There is probably no more respected
Bible-believing Old Testament commentator in the English-
speaking world. His commentary on Leviticus sets a high in-
tellectual standard. If Gordon Wenham says that The Days of
Vengeance is worth considering, then to fail to consider it would
be a major tactical error on the part of the pessimillenialists.

1 will go farther than Wenham does. This book is a landmark
effort, the finest commentary on Revelation in the history of the
Church. It has set the standard for: (1) its level of scholarship,
(2) its innovative insights per page, and (3) its readability. This
unique combination — almost unheard of in academic circles—
leaves the intellectual opposition nearly defenseless. There may

UL. Bibliotheca Sacra (April-June 1986).

xxiv

7:4-8 PART THREE: THE SEVEN SEALS

St. John begins at the eastern corner with Judah (because the
sealing ange] comes from the east, v. 2), goes through Reuben
and Gad to Asher at the north comer, then down the northwest
side with Naphtali and Manasseh; starting over again (we'll see
why in a moment), he lists Simeon and Levi on the southeast
side to Issachar at the south, then turns round the corner and
goes through Zebulun and Joseph, ending with Benjamin at the
western corner.

HVST WOINAE

 

210

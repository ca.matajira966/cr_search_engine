LITURGY AND HISTORY 8:3-5

figures are not necessarily “identical” to the angels of the
churches), They are clearly meant to be related to each other, as
we can see when we step back from the text (and our preconceived
ideas) and allow the whole picture to present itself to us. When
we do this, we see the Revelation structured in sevens, and in
recurring patterns of sevens. One of those recurring patterns is
that of seven angels (chapters 1-3, 8-11, 14, 15-16). Just as earthly
worship is patterned after heavenly worship (Heb. 123-24),
so is the government of the Church (Matt. 16:19; 18:18; John
20:23); moreover, according to Scripture, there are numerous
correspondences between human and angelic activities (cf.
21:17). Angels are present in the worship services of the Church
(1 Cor. 11:10; Eph. 3:10)—or, more precisely, on the Lord’s Day
we are gathered in worship around the Throne of God, in the
heavenly court.

Thus we are shown in the Book of Revelation that the gov-
ernment of the earthly Church corresponds to heavenly, angelic
government, just as our official worship corresponds to that
which is conducted around the heavenly Throne by the angels.
Moreover, the judgments that fall down upon the Land are
brought through the actions of the seven angels (again, we can-
not divorce the human angels from their heavenly counterparts).
The officers of the Church are commissioned and empowered to
bring God’s blessings and curses into fruition in the earth.
Church officers are the divinely appointed managers of world
history. The implications of this fact, as we shall see, are quite
literally earth-shaking.

 

3-5 St. John sees another angel standing at the heavenly
altar of incense, holding a golden censer. A large amount of in-
cense, symbolic of the prayers of all the saints (cf. comments on
5:8), is given to the angel that he might add it to the prayers of
God’s peaple, assuring that the prayers will be received as a
sweet-smelling offering to the Lord. Then the smoke of the in-
cense, with the prayers of the saints, ascends before God out of
the angel’s hand, as the minister offers up the petitions of his
congregation.

What happens next is amazing: The angel fills the censer
with coals of fire from the incense altar and casts the fire onto
the earth in judgment; and this is followed by peals of thunder

231

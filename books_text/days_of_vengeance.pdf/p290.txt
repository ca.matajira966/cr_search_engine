9:20-21 PART FOUR: THE SEVEN TRUMPETS

Did the Jews really worship demons and idols? We have al-
teady noted (see on 2:9 and 3:9) the Satanic character of Jud-
aism, which is not Old Testament religion, but is rather a false
cult claiming Biblical authorization (just as Mormonism, the
Unification Church, and other cults claim to be Biblical). As
Herbert Schlossberg points out, “Idolatry in its larger meaning
is properly understood as any substitution of what is created for
the creator.”24 By rejecting Jesus Christ, the Jews had inescap-
ably involved themselves in idolatry; they had departed from the
faith of Abraham and served gods of their own making. More-
over, as we shall see, the Jewish idolatry was not some vague,
undefined, apostate “theism.” Forsaking Christ, the Jews ac-
tually became worshipers of Caesar.

Josephus bears eloquent testimony to this, writing repeat-
edly of God’s wrath against the apostasy of the Jewish nation as
the cause of their woes: “These men, therefore, trampled upon
all the laws of man, and laughed at the laws of God; and as for
the oracles of the prophets, they ridiculed them as the tricks of
jugglers; yet did these prophets foretell many things concerning
the rewards of virtue, and punishments of vice, which when
these zealots violated, they occasioned the fulfilling of those
very prophecies belonging to their own country.”25

“Neither did any other city ever suffer such miseries, nor did
any age ever breed a generation more fruitful in wickedness than
this was, from the beginning of the world.”25

“J suppose that had the Romans made any longer delay in
coming against these villains, the city would either have been
swallowed up by the ground opening upon them, or been over-
flowed by water, or else been destroyed by such thunder as the
country of Sodom perished by, for it had brought forth a genera-
tion of men much more atheistical than were those that suffered
such punishments; for by their madness it was that all the people
came to be destroyed.”2?

“When the city was encircled and they could no longer
gather herbs, some persons were driven to such terrible distress

24, Herbert Schlossberg, Idols for Destruction: Christian Faith and Its
Confrontation with American Society (Nashville: Thomas Nelson Publishers,
1983), p. 6.

25, Josephus, The Jewish Wer, iv.vi.3.

26. Ibid., v.x.5.

27, Ibid., v.xiii.6.

256

9:20-21 PART FOUR: THE SEVEN TRUMPETS

regarding its worship of false gods with the holy utensils from
the Temple. Daniel said to king Belshazzar: “You have exalted
yourself against the Lord of heaven; and they have brought the
vessels of His House before you, and you and your nobles, your
wives and your concubines have been drinking wine from them;
and you have praised the gods of silver and gold, of bronze,
iron, wood, and stone, which do not see, hear, or understand.
But the God in whose hand are your life-breath and your ways,
you have not glorified” (Dan. 5:23).

St. John’s implication is clear: Israel has become a Babylon,
committing sacrilege by worshiping false gods with the Temple
treasures; like Babylon, she has been “weighed in the balance
and found wanting”; like Babylon, she will be conquered and
her kingdom will be possessed by the heathen (cf. Dan. 5:25-31).

Finally, St. John summiurizes Israel’s crimes, all stemming
from her idolatry (cf. Rom. 1:18-32): This led to her murders of
Christ and the saints (Acts 2:23, 36; 3:14-15; 4:26; 7:51-52,
58-60); her sorceries (Acts 8:9, 11; 13:6-11; 19:13-15; cf. Rev.
18:23; 21:8; 22:15); her fornication, a word St. John uses twelve
times with reference to Israel’s apostasy (2:14; 2:20; 2:21; 9:21;
14:8; 17:2 [twice]; 17:4; 18:3 [twice]; 18:9; 19:2); and her thefts, a
crime often associated in the Bible with apostasy and the result-
ant oppression and persecution of the righteous (cf. Isa. 61:8;
Jer. 7:9-10; Ezek. 22:29; Hos. 4:1-2; Mark 11:17; Rom. 2:21;
James $:1-6).

Throughout the Last Days, until the coming of the Romans,
the trumpets had blown, warning Israel to repent. But the alarm
was not heeded, and the Jews became hardened in their impeni-
tence. The retreat of Cestius was of course taken to mean that
Christ’s prophecies of Jerusalem’s destruction were false: The
armies from the Euphrates had come and surrounded Jerusalem
{ef. Luke 21:20), but the threatened “desolation” had not come
to pass. Instead, the Romans had fled, dragging their tails be-
tween their legs. Increasingly confident of divine blessing, the
Jews recklessly plunged ahead into greater acts of rebellion, un-
aware that even greater forces beyond the Euphrates were being
readied for battle. This time, there would be no retreat, Judea
would be turned into a desert, the Israelites would be slaugh-
tered and enslaved, and the Temple would be razed to the
ground, without a stone left upon another.

238

THE END OF THE BEGINNING 11:19

joy the sparkling fountain that issues from the body of Christ.”22

The early Christians who first read the Book of Revelation,
especially those of a Jewish background, had to understand that
the destruction of Jerusalem would not mean the end of cove-
nant or Kingdom. The fall of old Israel was not “the beginning
of the end.” Instead, it was the sign that Christ’s worldwide
Kingdom had truly begun, that their Lord was ruling the nations
from His heavenly throne, and that the eventual conquest of all
nations by the armies of Christ was assured. For these humble,
suffering believers, the promised age of the Messiah’s rule had
arrived. And what they were about to witness in the fall of Israel
was the end of the Beginning.

22. St. Irenaeus, Against Heresies, iii.xxiv.l; translation by Henry Betien-
son, ed., The Early Christian Fathers (Oxford: Oxford University Press, 1956,
1969), p. 83.

293

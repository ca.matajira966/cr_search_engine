PUBLISHER’S PREFACE

From this time on, there will be only three kinds of commentar-
ies on the Book of Revelation:

Those that try to extend Chilton’s
Those that try to refute Chilton’s
Those that pretend there isn't Chilton’s

Tyler, Texas
December 17, 1986

xxxiil

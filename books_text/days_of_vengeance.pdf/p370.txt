13: PART FOUR: THE SEVEN TRUMPETS

Image to the Beast who has the wound of the sword and has
come to life.

15 And there was given to him to give breath to the Image of
the Beast, that the Image of the Beast might even speak and
cause as many as do not worship the Image of the Beast to
be killed.

16 And he causes all, the small and the great, and the rich and
the poor, and the free men and the slaves, to be given a mark
on their right hand, or on their forehead,

17 and that no one should be able to buy or to sell, except the
one who has the mark, either the name of the Beast or the
number of his name.

18 Here is wisdom. Let him who has understanding calculate
the number of the Beast, for the number is that of a man;
and his number is 666.

N11 Just as the Beast from the sea was in the Image of the
Dragon, so we see another creature who is in the Image of the
Beast. St. John saw this one coming up from the Land, arising
from within Israel itself. In 16:13 and 19:20, we are told the iden-
tity of this Land Beast. He is the False Prophet, representing
what Jesus had foretold would take place in Israel’s last days:
“Many will come in My name, saying, ‘l am the Christ,’ and will
mislead many. . . . Many false prophets will arise, and will mis-
lead many” (Matt, 24:5, 11), The rise of the faise prophets paral-
leled that of the antichrists; but whereas the antichrists had
apostatized into Judaism from within the Church, the false
prophets were Jewish religious leaders who sought to seduce
Christians from the outside. As Cornelis Vanderwaal has noted,
“In Scripture, false prophecy appears only within the covenant
context”;}4 it is the imitation of true prophecy, and operates in
relation to the Covenant people. Moses had warned that false
prophets would arise from among the Covenant people, per-
forming signs and wonders (Deut. 13:1-5)}.

It is important to remember that Judaism is not Old Testa-
ment religion at afl; rather, it is a rejection of the Biblical faith
altogether in favor of the Pharisaical, Talmudic heresy. Like
Mormons, Jehovah’s Witnesses, the Unification Church, and

14. Cornelis Vanderwaal, Search the Scriptures, Vol. 10: Hebrews-Revelation
Gt. Catherines, Ontario: Paideia Press, 1979), p. 89; cf. p, 100.

336

 

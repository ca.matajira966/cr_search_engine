Copyright ¢ 1987 by Dominion Press

First Printing, January, 1987
Second Printing, December, 1987
Third Printing, March, 1990

All rights reserved. Written permission must be secured
from the publisher to use or reproduce any part of this
book, except for brief quotations in critical reviews or
articles.

Published by Dominion Press
P.O. Box 8204, Ft. Worth, Texas 76124

Typesetting by Thoburn Press, Box 2459, Reston, VA 22090

Printed in the United States of America
Library of Congress Catalog Card Number 86-050798

ISBN 0-930462-09-2

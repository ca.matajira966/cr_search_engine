INTRODUCTION

confiscated property at will, tock citizens’ wives (and husbands)
for their own pleasure, caused food shortages, exercised the
power of life and death over their subjects, and generally at-
tempted to rule every aspect of reality throughout the Empire.
The philosophy of the Caesars can be summed up in one phrase
which was used increasingly as the age progressed: Caesar is
Lord!

This was the main issue between Rome and the Christians:
Who is Lord? Francis Schaeffer points out: “Let us not forget
why the Christians were killed. They were not killed because
they worshiped Jesus. . . . Nobody cared who worshiped whom
so long as the worshiper did not disrupt the unity of the state,
centered in the formal worship of Caesar. The reason the Chris-
tians were killed was because they were rebels. . . . They wor-
shiped Jesus as God and they worshiped the infinite-personal
God only. The Caesars would not tolerate this worshiping of the
one God only. It was counted as treason.”23

For Rome, the goal of any true morality and piety was the
subordination of all things to the State; the religious, pious man
was the one who recognized, at every point in life, the centrality
of Rome. “The function of Roman religion was pragmatic, to
serve as social cement and to buttress the state.”?4 Thus, ob-
serves R. J. Rushdoony, “the framework for the religious and
familial acts of piety was Rome itself, the central and most sac-
red community. Rome strictly controlled all rights of corpora-
tion, assembly, religious meetings, clubs, and street gatherings,
and it brooked no possible rivalry to its centrality. ... The
state alone could organize; short of conspiracy, the citizens
could not. On this ground alone, the highly organized Christian
Church was an offense and an affront to the state, and an illegal
organization readily suspected of conspiracy.”?5

The witness of the apostles and the early Church was noth-
ing less than a declaration of war against the pretensions of the
Roman State. St. John asserted that Jesus is the on/y-begotten
Son of God (John 3:16); that He is, in fact, “the true God and

23. Francis A. Schaeffer, How Shall We Then Live? (Old Tappan, NJ:
Fleming H. Revell, 1976), p. 24.

24. Rousas John Rushdoony, The One and the Many: Studies in the Philos-
ophy af Order and Ultimacy (Tyler, TX: Thoburn Press, [1971] 1978), p. 92.

25. Ibid., pp. 92f.

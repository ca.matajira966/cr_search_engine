IUDGMENT FROM THE SANCTUARY 16:18

proclaims: It is done! (cf. 21:6). “The utterance is a single word,
Bhegonen, which is as thunderlike as the word wai is like the
scream of an eagle (8:13). ‘It is come to pass’ is the seal of an ac-
complishment, like that other one-word speech, ‘It is achieved,’
tetelestai [John 19:30], uttered by the Johannine Christ, as He
dies upon the cross.”3¢

18 Again appear the phenomena associated with the Day of
the Lord and the covenant-making activity of the Glory-Cloud:
flashes of lightning and peals of thunder and voices; and there
was a great earthquake. Seven times in Revelation St. John
mentions an earthquake (6:12; 8:5; 11:13 [twice]; 11:19; 16:18
[twice]), emphasizing its covenantal dimensions. Christ came to
bring the definitive earthquake, the great cosmic earthquake of
the New Covenant, one such as there had not been since the men
came to be upon the Land, so mighty an earthquake, and so
great (cf. Matt. 24:21; Ex. 9:18, 24; Dan. 12:1; Joel 2:1-2),

This was also the message of the writer to the Hebrews.
Comparing the covenant made at Sinai with the coming of the
New Covenant (which would be established at the destruction of
the Temple and the complete passing of the Old Covenant), he
said:

See to it that you do not refuse Him who is speaking. For if
those did not escape when they refused Him who warned them
on earth, much less shall we escape who turn away from Him
who warns from heaven. And His Voice shook the earth then,
but now He has promised, saying: Yet once more I will shake not
only the earth, but also the heaven (Hag. 2:6]. And this expres-
sion, “Yet once more,” denotes the removing of those things that
can be shaken, as of created things, in order that those things
that cannot be shaken may remain. Therefore, since we receive a
Kingdom that cannot be shaken, let us show gratitude, by which
we may offer to God an acceptable service with reverence and
awe: for our God is a consuming fire. (Heb. 12:25-29)

The eminent Puritan theologian John Owen commented on
this text about this definitive “earthquake”: “It is the dealing of
God with the church, and the alterations which he would make

30. Farrer, p. 179.
413

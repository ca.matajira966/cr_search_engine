17:1-2 PART FIVE: THE SEVEN CHALICES

God had bestowed on Babylon, and which she prostituted for
her own glory. Thus St. John describes the Great Harlot of his
day in terms of her prototype and model. Later, in 17:15, we are
informed of one aspect of the symbolic meaning of the “many
waters,” but for now the point is merely the identification of the
Harlot with Babylon.

At the same time, however, we must recognize that at every
other point in Revelation where the expression many waters is
used, it is set within a description of God’s covenantal relation-
ship and liturgical interaction with His people. We have noted
that the Voice from the Glory-Cloud sounds like many waters,
and that this Voice is produced by the innumerable angels in the
heavenly council (Ezek, 1:24), Similarly, in Revelation 1:15
Christ’s Voice is “like the sound of many waters” (cf. Ezek.
43:2); in 14:2 St. John again hears the Voice from heaven as “the
sound of many waters”; and in 19:6 the great multitude of the
redeemed, having entered the angelic council in heaven, joins in
a song of praise, which St. John hears as “the sound of many
waters.” The expression is thus reminiscent of both God’s gra-
cious revelation and His people’s liturgical response of praise
and obedience, Given the Biblical background and context of
the phrase, it would come as no surprise to St. John’s readers
that the Woman should be seen seated on “many waters.” The
surprise is that she is a whore. She has taken God’s goad gifts
and prostituted them (Ezek. 16:6-16; Rom. 2:17-24).

The Harlot-City has committed fornication with the kings of
the earth. This expression is taken from Isaiah’s prophecy
against Tyre, where it primarily refers to her international com-
merce (Isa, 23:15-17); Nineveh as well is accused of “many har-
lotries” with other nations (Nahum 3:4).? Most often, however,
the image of a city or nation playing the harlot with the king-
doms of the world is used in reference to the rebellious Covenant
people. Speaking against apostate Jerusalem, Isaiah mourned:

2. It is noteworthy that Tyre and Nineveh —the only two cities outside of
Israel! that arc accused of harlotry—had both been in covenant with God. The
kingdom of Tyre in David and Solomon’s Lime was converted to the worship of
the true God, and her king contracted a covenant with Solomon and assisted
in the building of the Temple (1 Kings $:1-12; 9:13; Amos 1:9); Nineveh was
converted under the ministry of Jonah (Jon, 3:5-10). The later apostasy of
these two cities could rightly be considered harlotry.

424

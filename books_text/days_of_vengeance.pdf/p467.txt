THE FALSE BRIDE 17:8

12 And the ten horns which you saw are ten kings, who have
not yet received a kingdom, but they receive authority as
kings with the Beast for one hour,

13 These have one purpose and they give their power and au-
thority to the Beast.

14 These will wage war against the Lamb, and the Lamb will
overcome them, because He is Lord of lords and King of
kings, and those who are with Him are the called and chosen
and faithful.

15 And he said to me: The waters which you saw, where the
Harlot sits, are peoples and multitudes and nations and
tongues.

16 And the ten horns which you saw, and the Beast, these will
hate the harlot and will make her desolate and will make her
naked, and will eat her flesh and will burn her up with fire.

17 For God has put it into their hearts to execute His purpose,
to execute one purpose, and to give their kingdom to the
Beast, until the words of God should be fulfilled.

18 And the Woman whom you saw is the Great City, which has
a Kingdom over the kings of the earth.

8 The angel begins his explanation by speaking about the
Beast, since the Harlot’s intimacy with the Beast is so integral to
her character and destiny. Again, we must note that this is a
composite Beast (cf. v.3 above), comprising the attributes of
both the Roman Empire and its original, the Dragon. Milton
Terry says: “In his explanation the angel seems to point our at-
tention particularly to the spirit which actuated the dragon, the
beast from the sea, and the false prophet alike; and so what is
here affirmed of the beast has a special reference to the different
and successive manifestations of Satan himself... . Hence we
understand by the beast that was and is not an enigmatical por-
traiture of the great red dragon of 12:3. He is the king of the
Abyss in 9:11, and the beast that killed the witnesses in 11:7. He
appears for a time in the person of some great persecutor, or in
the form of some huge iniquity, but is after a while cast out.
Then he again finds some other organ for his operations and en-
ters it with ali the malice of the unclean spirit who wandered
through dry places, seeking rest and finding none until he dis-

433

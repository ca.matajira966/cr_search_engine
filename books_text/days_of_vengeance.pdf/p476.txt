17:18 PART FIVE: THE SEVEN CHALICES

potter have a right over the clay, to make from the same lump
one vessel for honor, and another vessel for dishonor?” (Rom.
9:20-21). St. Augustine observed: “It is, therefore, in the power
of the wicked to sin; but that in sinning they do this or that is
not in their power, but in God’s, who divides the darkness and
regulates it; so that hence even what they do contrary to God's
will is not fulfilled except it be God’s will.”5

The whole purpose for the heathen kings’ wrath, for their
joining in conspiracy against both the Bride and the Harlot, for
their surrendering their kingdoms to the Beast and receiving
power for one hour with him, is now revealed. God has put it
into their hearts to fulfill His purpose, until the words of God
should be fulfilled. The war between Christ and the Beast, cul-
minating in the desolation of the Harlot, took place in fulfill-
ment of God’s announcements through His prophets. The
curses of the Covenant (Deut. 28) were executed on Israel
through the Beast and the ten horns. They were the instruments
of God’s wrath, as Christ had foretold in His discourse on the
Mount of Olives. During these horrifying “days of vengeance,”
He said, aff things that were written would be fulfilled (Luke
21:22). Vision and prophecy would be sealed and completed in
the destruction of the old world order (Dan. 9:24).

18 The angel now identifies the Harlot as the Great City,
which, as we have seen, St. John uses as a term for Jerusalem,
where the Lord was crucified (11:8; 16:19). Moreover, says the
angel, this City has a Kingdom over all the kings of the earth. It
is perhaps this verse, more than any other, which has confused
expositors into supposing, against all other evidence, that the
Harlot is Rome. If the City is Jerusalem, how can she be said to
wield this kind of worldwide political power? The answer is that
Revelation is not a book about politics; it is a book about the
Covenant. Jerusalem did reign over the nations. She did possess
a Kingdom which was above all the kingdoms of the world. She
had a covenantal priority over the kingdoms of the earth. Israel
was a Kingdom of priests (Ex. 19:6), exercising a priestly minis-

25. St. Augustine, Anti-Pelagian Works, Peter Holmes and Robert Ernest
Wallis, trans. (Grand Rapids: William B. Eerdmans, reprinted 1971), p. 514,
italics added; cf. John Calvin, Institutes of the Christian Religion, ii.iv.4.

442

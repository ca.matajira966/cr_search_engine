18:24 PART FIVE: THE SEVEN CHALICES

This language cannot be used of Rome or any other city.
Only Jerusalem was guilty of “all the righteous blood shed on
the earth,” from Abel onward. Historically, it was Jerusalem
that had always been the great Harlot, continually falling into
apostasy and persecuting the prophets (Acts 7:51-52); Jerusalem
was the place where the prophets were killed: as Jesus Himself
said, “It cannot be that a prophet should perish outside of Jeru-
salem. © Jerusalem, Jerusalem, the city that kills the prophets
and stones those sent to her!” (Luke 13:33-34). St. John’s “Cove-
nant Lawsuit” was true, and effective. Jerusalem was found
guilty as charged, and from a.p. 66-70 she suffered the “days of
vengeance,” the outpouring of God’s wrath for her agelong
shedding of innocent blood.

466

THE FEASTS OF THE KINGDOM 19:6-8

our God (cf.John 20:17, where Jesus says, “I ascend to My
Father and your Father, and My God and your God”). That this
is addressed to the Church as a whole is clear from the descrip-
tion of the worshipers: His servants, those whe fear Him, the
small and the great.

6-8 As the entire Church responds to the officiant’s inyita-
tion, she speaks with the familiar Voice of the Glory-Cloud (cf.
Ex. 19:16; Ezek. 1:24), indicating her full identification with the
glorious Image of God: St, John hears, as it were, the voice of a
great multitade and as the sound of many waters and as the
sound of mighty peals of thunder. The Cloud has assumed the
Church into itself.

The first Hallelujah! of the “great multitude” had praised
God for His sovereignty, as shown in the judgment of the great
Harlot. The fourth Hallelujah!, in this fifth and final portion of
the liturgy, praises God again for His sovereignty, this time as
shown in the marriage of the Lamb to His Bride. The destruc-
tion of the Harlot and the marriage of the Lamb and the Bride
—the divorce and the wedding ~ are correlative events, The ex-
istence of the Church as the congregation of the New Covenant
marks an entirely new epoch in the history of redemption. God
was not now merely taking Gentile believers into the Old Cove-
nant (as He had often done under the Old Testament economy}.
Rather, He was bringing in “the age to come” (Heb. 2:5; 6:5),
the age of fulfillment, during these Last Days. Pentecost was the
inception of a New Covenant. With the final divorce and de-
struction of the unfaithful wife in a.p. 70, the marriage of the
Church to her Lord was firmly established; the Eucharistic cele-
bration of the Church was fully revealed in its true nature as
“the Marriage Supper of the Lamb” (v. 9).

The multitude of the redeemed exults: His Bride has made
herself ready! The duty of the apostles during the Last Days was
to prepare the Church for her nuptials. Paul wrote of Christ’s
sacrifice as the redemption of the Bride: He “loved the Church
and gave Himself up for her; that He might sanctify her, having
cleansed her by the washing of water with the Word; that He
might present to Himself the glorious Church, having no spot or
wrinkle or any such thing; but that she should be holy and
blameless” (Eph. 5:25-27). Paul extended this imagery in speak-

473

THE COVENANT LAWSUIT

And I shall appoint over them four kinds of doom, declares
the Lorn: the sword to slay, the dogs to drag off, and the birds of
the sky and the beasts of the earth to devour and destroy. (Jer. 15:3)

Thus says the Lord Gon: I shall send My four evil judgments
against Jerusalem: sword, famine, wild beasts, and plague to cut
off man and beast from it! (Ezek. 14:21}

The imagery of a sevenfold judgment coming four times is
most fully developed in the Book of Revelation, which is expli-
citly divided into four sets of seven: the Letters to the Seven
Churches, the opening of the Seven Seals, the sounding of the
Seven Trumpets, and the outpouring of the Seven Chalices.3? In
thus following the formal structure of the covenantal curse in
Leviticus, St. John underscores the nature of his prophecy as a
declaration of covenant wrath against Jerusalem,

The four judgments are preceded by an introductory vision,
which serves to highlight the transcendence and immanence of
the Lord—precisely the function of the Preamble in the cove-
nantal treaties. As we read through the four series of judgments,
we find that they also conform to the treaty outline: The Seven
Letters survey the history of the covenant; the Seven Seals have
to do with the specific stipulations set forth in the corresponding
section of the covenantal treaty; the Seven Trumpets invoke the
covenant sanctions; and the angels of the Seven Chalices are in-
volved in both the disinheritance of Israel and the Church’s suc-
cession in the New Covenant. Thus:

Revelation

1. Preamble: Vision of the Son of Man (1)

2. Historical Prologue: The Seven Letters (2-3)

3. Ethical Stipulations: The Seven Seals (4-7)

4. Sanctions: The Seven Trumpets (8-14)

5. Succession Arrangements: The Seven Chalices (15-22)

St. John has thus combined the four-part Curse outline of
Leviticus 26 with the familiar five-part outline of the Covenant

37. Most commentaries, it is truc, seek to find seven ar more sets of seven,
but in doing so they are not adhering to St. John’s formal outline. Certainly,
there is nothing wrong with attempting to discover the many subtle structures
of the book; but we must at Jeast begin with the author's explicit arrangement
before making refinements.

17

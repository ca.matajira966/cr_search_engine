19:16 PART FIVE: THE SEVEN CHALICES

Come, behold the works of the Lorp,

Who has wrought desolations in the earth.

He makes wars to cease to the end of the earth;
He breaks the bow and cuts the spear in two;
He burns the chariots with fire. (Ps. 46:8-9)

“We are thus bound to believe that those occurrences by
which guilty nations are scourged and chastised for their sins,
are not merely brought about in providence, but ordered and
directed by the Mediator. And whether, therefore, we behold
the desolating sword cutting off the inhabitants, or the blasting
mildew destroying the crops, or commercial stagnation ob-
structing the sources of wealth, or wasting disease stalking with
ghastly power over a land, or the upheavings of popular com-
motion overturning the foundations of social order, we recog-
nize the wisdom, and might, and righteous retribution of Prince
Messiah, carrying into execution the divine decree, The nation
and kingdom that will not serve thee shail perish: yea, those na-
tions shall be utterly wasted” (Isa. 60:12).22

16 St. John sees Christ’s title “which no one knows except
Himself” (v, 12) written on His robe and on His thigh, the place
where the sword is worn (cf. Ps. 45:3). “The title is the ground,
not the result, of the coming victory; he will conquer the mon-
ster and the kings because he is already King of kings and Lord
of lords,”23 Riding out on His war-horse, followed by His army
of saints, He conquers the nations with the Word of God, the
Gospel. This is a symbolic declaration of hope, the assurance
that the Word of God will be victorious throughout the world,
so that Christ’s rule will be established universally. Jesus Christ
will be acknowledged everywhere as King of all kings, Lord over
all lords. From the beginning of Revelation, Christ’s message to
His Church has been a command to overcome, to conquer (2:7,
Nl, 17, 26-28; 3:5, 12, 21); now He assures the suffering Church
that, regardless of the fierce persecution by Israel and Rome, He
and His people will in fact be victorious over all enemies.

22. William Symington, Messiah the Prince: or, The Mediatorial Dominion
of Jesus Christ (Philadelphia: The Christian Statesman Publishing Co., [1839]
1884), p. 224,

23. Caird, p, 246.

488

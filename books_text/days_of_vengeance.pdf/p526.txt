19:19-21 PART FIVE: THE SEVEN CHALICES

losers: Not only are they defeated, but the very nations that they
led in battle against Christ are conquered by His victorious
Word.

At their very worst, Leviathan, Behemoth, and their co-
conspirators could do no more than fulfill the decrees of the
sovereign God (17:17). He ordained their every move, and He
ordained their destruction. The nations rage, but God laughs:
He has already set up His King on His holy mountain, and all
nations will be ruled by Him (Psalm 2). Ail power in heaven and
earth has been given to Christ (Matt. 28:18); as Martin Luther
sang, “He must win the battle.” As the Gospel progresses
throughout the world it will win increasing victories, until all
kingdoms become the kingdoms of our Lord, and of His Christ;
and He will reign forever and ever, We must not concede to the
enemy even one square inch of ground in heaven or on earth.
Christ and His army are riding forth, conquering and to con-
quer, and we through Him will inherit all things.

492

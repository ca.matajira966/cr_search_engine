20:2-3 PART FIVE: THE SEVEN CHALICES

over man by apostasy, so again his apostasy might be deprived
of power by means of man turning back again to God.” St.
Augustine agreed: “The devil was conquered by his own trophy
of victory. The devil jumped for joy, when he seduced the first
man and cast him down to death. By seducing the first man, he
slew him; by slaying the last man, he lost the first from his snare.
The victory of our Lord Jesus Christ came when he rose, and
ascended into heaven; then was fulfilled what you have heard
when the Apocalypse was being read, ‘The Lion of the tribe of
Judah has won the day’ [Rev. 5:5]. .. . The devil jumped for
joy when Christ died; and by the very death of Christ the devil
was overcome: he took, as it were, the bait in the mousetrap. He
rejoiced at the death, thinking himself death’s commander. But
that which caused his joy dangled the bait before him. The
Lord's cross was the devil’s mousetrap: the bait which caught
him was the death of the Lord.”!5

But the precise thrust of Revelation 20 seems to be dealing
with something much more specific than a general binding and
defeat of Satan. St. John tells us that the Dragon is bound with
reference to his ability to deceive the nations —in particular, as
we learn from verse 8, the Dragon’s power “to deceive the na-
tions . . . fo gather them together for the war.” The stated goal
of the Dragon’s deception is to entice the nations to join forces
against Christ for the final, all-out war at the end of history.
Satan’s desire from the beginning has often been to provoke a
premature eschatological cataclysm, to bring on the end of the
world and the Final Judgment now. He wants to rush God into
judgment in order to destroy Him, or at least to short-circuit
His program and destroy the wheat with the chaff (cf. Matt.
13:24-30). In a sense, he can be considered as his own agent pro-
vocateur, leading his troops headlong into an end-time rebellion
that will call down God’s judgment and prevent the full matura-
tion of God’s Kingdom.

Writing of Jesus’ parable of the leaven—“The Kingdom of
heaven is like leaven, which a woman took, and hid in three

14. St. Irenaeus, Against Heresies, v.xxiv.4.

15. St. Augustine, Sermons, 261; trans. by Henry Bettenson, ed., The Later
Christian Fathers: A Selection From the Writings of the Fathers from St. Cyrit
of Jerusaiem tu St. Leo the Great (Oxford: Oxford Universily Press, 1970,
1977), p. 222.

504

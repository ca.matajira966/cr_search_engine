THE MILLENNIUM AND THE JUDGMENT 20:11

Satan’s cause will be finally and thoroughly overthrown. To pic-
ture this St. John again uses imagery based on the holocaust of
Sodom and Gomorrah (Gen. 19:24-25, 28) and the destruction
of the rebels in the wilderness of Kadesh (Num. 16:31-33), based
on Isaiah’s similar usage to describe the utter ruin of Edom (Isa.
34:9-10). He has already represented the eternal destruction of
the Beast and the False Prophet and their followers by such im-
agery (see 14:10-11; 19:20); now he shows that the prime insti-
gator of the cosmic conspiracy is inevitably doomed to suffer the
same fate.

The Judgment of the Dead (20:11-15)

11 And I saw a great white Throne and Him who sat upon it,
from whose face earth and heaven fled away, and no place
was found for them.

12 And FE saw the dead, the great and the small, standing before
the Throne, And books were opened; and another book was
opened, which is the Book of Life; and the dead were judged
from the things which were written in the books, according
to their works.

13 And the Sea gave up the dead which were in it, and Death
and Hades gave up the dead which were in them; and they
were judged, each one according to his works.

14 And Death and Hades were thrown into the lake of fire. This
is the Second Death, the lake of fire.

15 And if anyone was not found written in the Book of Life, he
was thrown into the lake of fire.

Ti The sixth vision begins with the familiar formula: And I
saw (kai eidon), History has ended; the crack of doom has
fallen; and now the apostle’s vision is filled with a great white
Throne, and Him who sat upon it. Usually, it is implied in Reve-
lation that the One seated on the Throne in heaven is the Father
(cf. 4:2-3; 5:1, 7); but in this case St. John may have in mind the
Son, since He is seated on a white Throne, and He has been seen
previously seated on a white cloud (14:14) and a white horse (6:2;
19:11), The Lord Jesus Christ is the great “Shepherd and Bishop”
(I Pet. 2:25); Farrer points out that “the idea of a ‘white throne’
may perhaps have been familiar to St. John’s hearers as the dis-
tinguishing character of the focal bishop's chair in the church.
The practice of spreading a white cover over it was certainly early;

529

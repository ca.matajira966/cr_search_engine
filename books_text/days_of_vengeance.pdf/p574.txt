21:1 PART FIVE: THE SEVEN CHALICES

heaven and earth” is 2 Peter 3:1-14. There, St. Peter reminds his
readers that Christ and all the apostles had warned of accelerat-
ing apostasy toward the end of the “last days” (2 Pet. 3:2-4; cf.
Jude 17-19)— which, as we have seen, was the forty-year transi-
tional period (cf. Heb. 8:13) between Christ’s Ascension and the
destruction of the Old Covenant Temple, when the nations were
beginning to flow toward the Mountain of the Lorn (Isa. 2:2-4;
Acts 2:16-17; Heb. 1:2; James 5:3; 1 Pet. 1:20; 1 John 2:18), As
St. Peter made clear, these latter-day “mockers” would be Cove-
nant apostates: Jews who were familiar with Old Testament his-
tory and prophecy, but who had abandoned the Covenant by re-
jecting Christ. Upon this evil and perverse generation would
come the great “Day of Judgment” foretold in the prophets, a
“destruction of ungodly men” like that suffered by the wicked of
Noah’s day (2 Pet. 3:5-7; cf. the same analogy drawn in Matt.
24:37-39; Luke 17:26-27), Just as God had destroyed the
“world” of that day by the Flood, so would He destroy the
“world” of first-century Israel by fire in the fall of Jerusalem.

St. Peter describes this as the destruction of “the present
heavens and earth” (2 Pet. 3:7), making way for “new heavens
and a new earth” (v. 13), Because of the “collapsing-universe”
terminology used in this passage, many have mistakenly assumed
that St. Peter is speaking of the final end of the physical heaven
and earth, rather than the dissolution of the Old Covenant
world order. The great seventeenth-century Puritan theologian
John Owen answered this view by referring to the Bible’s meta-
phorical usage of heavens and earth, as in Isaiah’s description of
the Mosaic Covenant:

But I am the Lorp thy God, that divided the sea, whose
waves roared: The Lorp of hosts is His name.

Aud I have put my words in thy mouth, and J have covered
thee in the shadow of mine hand, that I may plant the Aeavens,
and lay the foundations of the earth, and say unto Zion, Thou
art my people. (Isa. 51:15-16)

Owen writes: “The time when the work here mentioned, of
planting the heavens, and laying the foundation of the earth,
was performed by God, was when he ‘divided the sea’ (v. 15),
and gave the law (v. 16), and said to Zion, ‘Thou art my
people’—that is, when he took the children of Israel out of

540

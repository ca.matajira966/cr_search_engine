22

COME, LORD JESUS!

As we saw in the Introduction, St. John wrote the Book of
Revelation as an annual cycle of prophecies, meant to be read ta
the congregation (coinciding with serial Old Testament readings,
especially Ezekiel) from one Easter to the next.! Chapter 22 thus
brings us full circle, verses 6-21 being read exactly one year after
Chapter | was read. For that reason, as weil as recapitulating
many of the themes of the prophecy, Chapter 22 also has much
in common with Chapter 1, We read again, for example, that the
prophecy is of “things that must shortly take place” (22:6; cf.
1:1); that it is communicated by an angel (22:6; cf. 1:1) to St.
John (22:8; cf. 1:1, 4, 9); that it is a message intended for God’s
“bond-servants” (22:6; cf. 1:1); that there is a special blessing for
those who “keep” its words (22:7; cf. 1:3); and that it specifically
involves the Testimony of Christ (22:16, 18, 20; cf. 1:2, 5, 9), the
Alpha and the Omega, the First and the Last (22:13; cf. 1:8, 17),
who is “coming quickly” (22:7, 12, 20; cf. 1:7).

Paradise Restored (22:1-5)

1 And he showed me a River of the Water of Life, clear as
crystal, coming from the Throne of God and of the Lamb,

2 in the middle of its street. And on each side of the River was
Tree of Life, bearing twelve crops of fruit, yielding its fruit
every month; and the leaves of the Tree were for the healing
of the nations.

3 And there shall no longer be any Curse; and the Throne of
God and of the Lamb shall be in it, and His servants shall
serve Him;

1. See M. D. Goulder, “The Apocalypse as an Annual Cycle of Prophecies,”
New Testament Studies 27, No. 3 (April 1981), pp. 342-67.

565

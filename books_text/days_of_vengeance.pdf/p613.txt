COME, LORD JESUS! 22:17

David. In him alone is the race preserved; while otherwise it
would have vanished without a trace. The race of David is more
than his offspring; it indicates that the race of David should,
save for Christ, have ceased to exist. The race of David is here
brought into view in respect to the unconquerable strength and
everlasting dominion promised it by God (comp. Luke 1:32-33).
What he testifies, in whom the glorious race of David culmin-
ates, will assuredly go into fulfillment.”!3

In Numbers 24:17, Balaam prophesied of Christ under the
symbols of a star and a scepter; Christ’s scepter is promised to
the overcomer in Thyatira (2:26-27), in an allusion to Psalm
2:8-9; then, as the promise to the overcomer continues, Christ
offers Himself as the Morning Star (2:28), and that promise is
repeated here, partly in order to complement the promise of
Light in verse 5, and partly in keeping with other connections
which this passage shares with the Letters to both Pergamum
(the mention of idolatry and the allusion to Balaam) and Thya-
tira (the mention of sorcery and fornication).

17 And the Spirit and the Bride say: Come! This is a prayer
to Jesus, the Spirit inspiring the Bride to call for Him (cf. Cant.
8:14: “Hurry, my beloved!”} to come in salvation and judgment,
even as the four living creatures called forth the Four Horsemen
(6:1, 3, 5, 7). The liturgical response is then set forth: And let
the one who hears say: Come! Finally, the expression is inverted
(cf. 3:20-21, where Christ first asks to dine with us, then invites
us to sit with Him), for the certainty of Christ’s coming to us in
salvation enables us to come to Him for the Water of Life: And
let the one who is thirsty come; let the one who wishes take the
Water of Life without cost. The expression without cost is
dérean, meaning as @ gift, used by Christ in a particularly telling
reference: “They hated me without a cause” (John 15:25), Our
salvation is free, “without a cause” as far as our own merit is
concerned; its source and reason are wholly in Him, and not at
all in us. We are “justified as a gift by His grace through the re-
demption which is in Christ Jesus” (Rom. 3:24).

13. E, W. Hengstenberg, The Revetation of St. John, 2 vols., trans, Patrick
Fairbairn (Cherry Hill, NJ: Mack Publishing Co., n. d.), Vol. 2, p. 373.

379

THE LEVITICAL SYMBOLISM IN REVELATION

1. Seven angels are given seven trumpets.
2. The Incense is offered.
3. The trumpets are sounded.

The same particularity is shown in the case of the seven bowls (see
15:1).

Let us return to the killing of the lamb. The signal for the killing of
the lamb was three blasts on the trumpet; these three blasts were also a
signal for the gates of the temple and sanctuary to be opened. This is
what we find in St. John:

Seven Trumpets (8:1 to 11:18).
Opening of the Sanctuary of God in Heaven (11:19).

We are justified in concluding, therefore, that he is following,
though in a rough manner, the temple ceremonial. The likeness be-
comes more exaci when we recollect that Dr. Charles has given very
good reason to suppose that in Revelation also the number of trum-
pets was originally three. The argument from ceremonial converts Dr.
Charles’ hypothesis into a certainty. The series of seven seals and seven
trumpets as 1 have observed in the text of my book, is not a key to the
construction of Revelation; it abscures it; il was introduced to bind to-
gether visions that did not cohere.

In dealing with the Naos or Sanctuary in Heaven, we are on very
delicate ground. Two things seem clear. One is that the “visible” Pres-
ence or Glory is departed from Jerusalem so that the Naos there is a
Naos no longer; the other is that the Naos in heaven is the number of
elect believers in which the Presence is henceforth to Tabernacle. It is
universal, in the “heavens,” open to all. I believe that the alder series
of visions was to have ended, or perhaps did end, with the descent of
this Temple not made with hands. Two traces of it, I think, are to be
found: the promise in 3:12, J will make him a pillar in the Naos af my
God, and the statement about the triumphant martyrs, 7:15, They
serve him day and night in his Naos.

This thought of the new Naos from heaven was superseded by
something better, the vision of the New City which has no Naas, and
no day or night either.

Now we see why the death of the lamb had to come first. It was the
death of Christ that opened the way. When thou hadst overcome the
sharpness of death, thou didst open the Kingdom of heaven to all be-
fievers. Comparing St. John with the temple ritual, we now gel:

599

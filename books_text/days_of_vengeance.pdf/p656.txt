APPENDIX C

commendeth his lave toward us, in that, while we were yet sinners,
Christ died for us” (Rom. 5:8). God selects those on whom He will
have mercy (Rom. 9:18). He has chosen these people to be recipients
of His gift of eternal salvation, and He chose them before the founda-
tion of the world (Eph. 1:4-6).

But there is another kind of grace, and it is misunderstood. Com-
mon grace is equally a gift of God to His creatures, but it is distin
guished from special grace in a number of crucial ways. A debate has
gone on for close to a century within Calvinistic circles concerning the
nature and reality of common grace. 1] hope that this essay will con-
tribute some acceptable answers to the people of God, though | have
little hope of convincing those who have been involved in this debate
for 60 years.

Because of the confusion associated with the term “common
grace,” let me offer James Jordan’s description of it. Common grace is
the equivalent of the crumbs that fal! from the master’s table that the
dogs eat. This is how the Canaanite woman described her request of
healing by Jesus, and Jesus healed her because of her understanding
and faith (Mart, 15:27-28).2

Background of the Debate

In 1924, the Christian Reformed Church debated the subject, and
the decision of the Synod led to a major and seemingly permanent
division within the ranks of the denomination. The debate was of con-
siderable interest to Dutch Calvinists on both sides of the Atlantic,
although traditional American Calvinists were hardly aware of the
issue, and Arminian churches were (and are still} completely unaware
of it. Herman Hoeksema, who was perhaps the most brilliant system-
atic theologian in America in this century, left the Christian Reformed
Church to form the Protestant Reformed Church. He and his follow-
ers were convinced that, contrary to the decision of the CRC, there is
no such thing as common grace.

The doctrine of common grace, as formulated in the disputed
“three points” of the Christian Reformed Church in 1924, asserts the
following:

1. There is a “favorable attitude of God toward mankind in
general, and not alone toward the elect, ...” Furthermore,

2. Dogs in Israel were not highly loved animals, so the analogy with com-
mon grace is biblically legitimate. “And ye shall be holy men unto me: neither
shall ye eat any flesh that is torn of beasts in the field; ye shall cast it to the
dogs” (Ex. 22:31). If we assume that God Joves pagans the way that modern
people love their dogs, then the analogy will not fit.

624

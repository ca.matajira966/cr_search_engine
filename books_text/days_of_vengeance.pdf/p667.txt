COMMON GRACE, ESCHATOLOGY, AND BIBLICAL LAW

sive. They are commanded to be active, to seek dominion over nature
(Gen. 1:28; 9:1-7). They are to manage the field. As both the good and
the bad work out their God-ordained destinies, what kind of develop-
ment can be expected? Who prospers most, the saved or the lost? Who
becomes dominant?

The final separation comes at the end of time. Until then, the two
groups must share the same world, If wheat and tares imply slow
growth to maturity, then we have to conclude that the radically discon-
tinuous event of separation will not mark the time of historical devel-
opment. It is an event of the last day: the final judgment. It is a discon-
tinuous event that is the capstone of historical continuity. The death
and resurrection of Christ was the last historically significant event
that properly can be said to be discontinuous (possibly the day of
Pentecost could serve as the last earth-shaking, kingdom-shaking
event), The next major eschatological discontinuity is the day of judg-
ment. So we should expect growth in our era, the kind of growth in-
dicated by the agricultural parables.>

What must be stressed is the element of continuous development.
“The kingdom of heaven is like to a grain of mustard seed, which a
man took and sowed in his field: Which indeed is the least of all seeds:
but when it is grown, it is the greatest among herbs, and becometh a
tree, so that the birds of the air come and lodge in the branches there-
of” (Matt. 13:31-32). As this kingdom comes into maturity, there is no
physical separation between saved and lost. That total separation will
come only at the end of time. There can be major changes, even as the
seasons speed up or retard growth, but we should not expect a radical
separation.

While I do not have the space to demonstrate the point, this means
that the separation spoken of by premillennialists—the Rapture—is
not in accord with the parables of the kingdom. The Rapture comes at
the end of time. The “wheat” cannot be removed from the field until
that final day, when we are caught up to meet Christ in the clouds
(I Thess. 4:17). There is indeed a Rapture, but it comes at the end of
time — when the reapers (angels) harvest the wheat and the tares. There
is a Rapture, but it is a postmillennial Rapture.

Why a postmillennial Rapture, the amillennialist may say? Why
not simply point out that the Rapture comes at the end of time and let
matters drop? The answer is important: We must deal with the ques-
tion of the development of the wheat and tares. We must see that this
process of time leads to Christian victory on earth and in time.

5. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion
(Tyler, Texas: Institute for Christian Economics, 1985), ch. 12: “Continuity
and Revolution.”

635

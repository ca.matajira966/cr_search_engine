COMMON GRACE, ESCHATOLOGY, AND BIBLICAL LAW

8710-12; 10:16), and as the unregenerate come under the sway and influ-
ence of the law, common grace must increase, not decrease. The cen-
tral issue is the restraint by God inherent in the work of the law. This
work is in every man’s heart,

Remember, this has nothing to do with the supposed favor of God
toward mankind in general. It is simply that as Christians become
more faithful to biblical law, they receive more bread from the hand of
God. As they increase the amount of bread on their tables, more
crumbs fail to the dogs beneath,

Third, the amillennial view of the process of separation or differen-
tiation is seriously flawed by a lack of understanding of the power
which biblical law confers on those who seek to abide by its standards.
Again, we must look at Deuteronomy, chapter eight. Conformity to
the precepts of the law brings external blessings. The blessings can
(though need not) serve as a snare and a temptation, for men may for-
get the source of their blessings. They can forget God, claim auton-
omy, and turn away from the law. This leads to destruction. The form-
erly faithful people are scattered. Thus, the paradox of Deuteronomy
8: covenantal faithfulness to the law— external blessings by God in re-
sponse to faithfulness— temptation to rely on the blessings as if they
were the product of man’s hands judgment. The blessings can lead
to disaster and impotence. Therefore, adherence to the terms of bibli-
cal law is basic for external success.

Ethics and Dominion

As men become epistemologically self-conscious, they must face
up to reality —God’s reality. Ours is a moral universe. It is governed by
a law-order which refiects the very being of God. When men finally
realize who the churls are and who the liberals are, they have made a
significant discovery. They recognize the relationship between God’s
standards and the ethical decisions of men. In short, they come io
grips with the law of God. The éaw is written in the hearts of Chris-
tians. The work of the aw is written in the hearts of all men. The
Christians are therefore increasingly in touch with the source of earthly
power: biblical law. To match the power of the Christians, the unre-
generate must conform their actions externally to the law of God as
preached by Christians, the work of which they already have in their
hearts. The unregenerate are therefore made far more responsible be-
fore God, simply because they have more knowledge. They desire
power, Christians will some day possess cultural power through their
adherence to biblical law. Therefore, unregenerate men will have to
imitate special covenantal faithfulness by adhering to the demands of
God’s external covenants. The unregenerate will thereby bring down

641

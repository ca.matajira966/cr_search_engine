SELECT
BIBLIOGRAPHY

Studies in Revelation

Barclay, William. The Revelation of Jokn. 2 vols. Philadelphia: The
Westminster Press, revised ed., 1960.

Beasley-Murray, G. R. The Book of Revelation. Grand Rapids: Wil-
liam B. Eerdmans Publishing Co., [1974] 1981,

Beckwith, Isbon T. The Apocalypse of John: Studies in Introduction
with a Critical and Exegetical Commentary. Grand Rapids: Baker
Book House, [1919] 1979.

Caird, G. B. The Revelation of St. John the Divine. New York:
Harper and Row, Publishers, 1966.

Carrington, Philip. The Meaning of the Revelation, London: SPCK,
1931,

Charles, R. H. A Critical and Exegetical Commentary on the Reveia-
tion of St. John. 2 vols. Edinburgh: T. and T. Clark, 1920.

Corsini, Eugenio. The Apocalypse: The Perennial Revelation of Jesus
Christ. Translated by Francis J, Moloney, 8.D.B, Wilmington, DE:
Michael Glazier, 1983.

Farrar, F. W. The Early Days of Christianity. Chicago: Belford, Clarke
and Co., 1882.

Farrer, Austin. A Rebirth of Images: The Making of St. John’s Apec-
alypse. London: Dacre Press, 1949; Gloucester, MA: Peter Smith,
1970.

. The Revelation of St. John the Divine. Oxford: At the
Clarendon Press, 1964.

667

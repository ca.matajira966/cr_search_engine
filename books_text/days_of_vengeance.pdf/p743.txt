SUBFECT INDEX

sovereignty of, 251, 403-4, 441-42
‘Supper of. See Supper of God, great
unity of, 38-59
unregenerate and favor of, 624-29. See also
Common grace

wrath of, 364-66, 383-84, 389-90, 391-92,
412-13, 416. See aiso Jerusalem,
destruction of; Judgment{s)

Godhead, 57. See also Trinity

Gog, 520-24, 536

Gold, 557

Goliath, 345-46

Gomer, 522

‘Gomorrah, 226, 365, 472, 491, 525

Gospel, 188, 361-62. See also Mystery
first proclamation of. See Protevangelinm
light of, 563
triumph of, 215-16. See afso Postmillen-

nialism

Gospels
Litite Apocalypse in, 182
Synoptic, 182

Government
Church, 231
origin of, 105

Grace, 57
common, See Common grace
future, 663
irresistible, 123
jaw and, 644-51
meaning of, 623
special, 623-24, 629, 645-46
unregenerate and God's, 624-29

Grass, burning of, 195, 236

Great Commission, 510-11

Greek Orthodox church, 421

Grief, Biblical, 269, 547

Habakkuk, prayer of, 184-85
Hades, $32, 533, 534
keys of, 77-78, 191
Hagar, 373
Hailstones, 47-18
Halldujah, 469-70, 472, 473
Haman, 308
Hanitah, 299
Harlot, Great, 421-43, 471. See also Babylon,
fall of
Beast’s support of 428-29, 432
beauty of, 432
communion of, 430, 431
confession of, 450
explanation of, 433-43
garments of, 425-30
identity of, 421, 423-32, 442, 457, 465-66
as mother of hartots, 431
mystery and, 431, 432
primary application of, 421

Harlot, Royal, 89, 90, See also Harlot, Great
Harlotry, 19, 356. See also Adultery;
Fomication
Harps, 384
Head(sy
death-wound of one, 329-31
seven, 327-28, 329
Heaven, 197, 299
fight of, 532
new, See Heaven(s) and earth, new
silence in, 229-30
true church in, 244, 332-33, See also
Ascension; Church, worship of
‘Heaven(s) and earth, new, 537-45, 570. See
also Creation, new
Isaiah's, $38-39
Peter's, 539.42
phrase, 540-42
scope of, 541-44
time of , $38
Hell, 283. See also Hades
false church in, 244
gates of, 313-14
Heresy. See alsa Apostasy; Apostates
Iezebel's, 113-14, 15
warning against, 109
Heretics, 16
Herod Agrippa, 241
Herod Antipas, 241, 280, 282, 438
Herod the Great, 217, 241, 310
Hezekiah, 229
High Priest(s), 8%, 241, 327
apparel of, 74, 220
breastplate of, 557
‘Christ as, 50, 153, 272
description af, 74-75
forehead of, 31, 205, 483
Historical theology, 38
History, 170-71, 489. See also Church history
al philosophy of, 180
Christ as central to, 173
Sifferentiation in, 634-35, 642-44
goal of, 180
God’s sovereignty over, 52, 59-60, 100, $06
meaning in, 632-34
Progress in, 642-44
separation in, 634.35
threat of, 639
world, related to church history, 234
orship related to, 61, 232-33
Hivites, 632
Holocaust, 205, 285. See also Jerusalem,
destruction of
Holy of holies, heavenly, 150. See also
‘Tabernacle; Temple
Holy Spirit, 57, 276, 292
being in the, 70-71, 148

  

 

  

71

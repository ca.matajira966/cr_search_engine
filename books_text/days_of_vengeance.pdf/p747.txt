SUBJECT INDEX

choices of unregenerate, 642
Fall of, See Fall of man
form of, 155, 158
good done by, 630-31
longevity of, $14, 644, 648, 6530
number of, 345
Reuben as the, 159
unregenerate, and grace, 624-34, See alse.
‘Common grace
Manasseh, 212
Manna, hidden, 87, 109
Mascionism, 365
Mark Antony, 218
Marriage Supper of the Lamb, 89, 382,
468-80. See also Eucharist
Martyrs, 194-95, 316, 367-69, 512-13, $95-96
Marxism, 495
threat of, 656-57
Mary, virgin, 298, 299, 310, 313. See aso
Woman
Megiddo, 411-12
Merchants, 448, 457, 458, 464-65
lament of, 454
Mercy-seat, 150, 235
Meshech, 522
Methuselah, 54m, 644, 646
Micaiah, 408
Michael, 316, 319
Dragon ws., 31b-18, 437
identity of, 31i-13
Midianites, 524
Mill, 464
grinding at, 426
Millenarianism
church’s rejection of, 494
two forms of, 494-95
Millennium, S15, 518, 519, $20, $25, 573,
$84.88, 618. See also Kingdom af God; One
thousand
central question about, 493-94
orthodox Christianity and, 494-97
unity regarding, 494
views on, 493-98
Millstone, 450
Monasticism, 548
Moon, 297, 3001-2, 303
bloodlike, 195
darkened, 240-41
Moral Majority, 619
Mormonism, 256, 278n, 336
Moscow, 522
Moses, 32, 128, 169, 201, 217, 234, 277, 278,
2H, 283, 288, 320, 335, 380, 382, 535, 570,
46
body of, 312
Jews and, 102
law of, See Law of God, Ten Commandments

rod of, 304
Moslems, 612, 614, 618
Mountain(s}, 197, 265
holy, 238, 384-55
moving of, 238-39, 460
Mountain People, The (Turnbull), 656
Minster Revolt, 49S
Murderers, $50, $78
Music, 163, 164, 464, See also Liturgy
Joss of, 463
Myriads, 250, 251
Mystery, See afso Gospel
‘of God, 265-66, 247, 286, 292
Harlot and, 431, 432

Nahum, 65

Nakedness, 136, 439

Name, new, 87, INL

Natural law, (57 400, $89

Nature, 157

Nazism, 495, 613

Nebo, Mount, 382

Nebuchadnezzar, 279, 346

Neco, lt

Nehemiah, 88

Neoplatonism, "spirituulistic,* 472

Nero, 35, 41, 79, 218, 241, 325, 418, 434,
436
as Beast, 329, 344-45, 350, 351, 583
persecution under, 4
zedivivus myth about, 330

New Testament, 101-2

New Yeur's Day, Old Covenant, 235, 301
Judaism and, 289

Nivea, Council of, $8

Nicene Creed, 119

Nicolaitans, 97-98, 101, 107, 114
doctrine of, 108-9

Nicolas, 97

Nile, 238

Ninevab, 11, 424

Noah, 167, 186, 288, 474, 514, 644, 645, 646,
on

North, 250-51

Oath, 266, $77, See also Covenant
Occultism, 158-59. See ulsv Sorcerers
Jewish, 94, 485
Octavian, See Caesar, Augutus
Oferiny(s). See alse Sacrifices)
burnt, 608
daily, 603
drink, 608
incense. See Incense, offering of
meal, 603
purification, 248-49, 391
Oil, i91

75

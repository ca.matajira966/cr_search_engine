KING OF KINGS 1:

calis into being that which does not exist” (Rom. 4:17). God
knows all things exhaustively because He planned all things ex-
haustively.

Arthur Pink wrote: “The Lord God omnipotent reigneth.
His government is exercised over inanimate matter, over the
brute beasts, over the children of men, over angels good and
evil, and over Satan himself. No revolving of a world, no shin-
ing of a star, no storm, no movement of a creature, no actions
of men, no errands of angels, no deeds of the Devil — nothing in
all the vast universe can come to pass otherwise than God has
eternally purposed. Here is a foundation for faith. Here is a
resting place for the intellect. Here is an anchor for the soul,
both sure and steadfast. It is not blind fate, unbridled evil, man
or Devil, but the Lord Almighty who is ruling the world, ruling
it according to His own good pleasure and for His own eternal
glory.”?

Now St. John says that these things regarding the future
were signified, or “sigr-ified,” to him by the angel. The use of
this word tells us that the prophecy is not simply to be taken as
“history written in advance.” It is a book of signs, symbolic rep-
resentations of the approaching events. The symbols are not to
be understood in a literal manner. We can see this by St. John’s
use of the same term in his Gospel (12:33; 18:32; 21:19). In each
case, it is used of Christ “signifying” a future event by a more or
less symbolic indication, rather than by a prosaic, literal de-
scription. And this is generally the form of the prophecies in the
Revelation. It is a book of symbols from beginning to end. As
G. R. Beasley-Murray well said, “The prophet wishes to make
clear that he does not provide photographs of heaven.”? This
does not mean the symbols are unintelligible; the interpretation
is not what any individual chooses to make it. Nor, on the other
hand, are the symbols written in some sort of code, so that all
we need is a dictionary or grammar of symbolism to “translate”
the symbols into English. The only way to understand St. John’s
systern of symbolism is to become familiar with the Bible itself.

2. Arthur Pink, The Sovereignty of God (London: The Banner of Truth
Trust, 11928] 1968), pp. 43.

3. G. R. Beasley-Murray, The Book of Revelation (Grand Rapids: William
B. Eerdmans Publishing Co., [1974] 1981), p. 51.

53

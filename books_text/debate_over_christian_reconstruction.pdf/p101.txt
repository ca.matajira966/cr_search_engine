Putting Eschatology into Perspective 83

premillennialists (dispensationalists) do not insist on such a view.
Paul D. Feinberg writes:

The time of the Rapture is neither the most important nor the
most unimportant point in Ghristian theology. For some the
Rapture question is a bellwether; its surrender marks the first
step on the proverbial slippery slope that leads one to the rocks of
liberalism. But such is neither logically or actually the case.
When one considers the whole spectrum of Christian theology,
eschatology is only a small part of it. Moreover, the Rapture
question constitutes only a small segment of eschatology.

Matthew 24:1-34 and Revelation 20 support the edifice of the
dispensational system. But as we will see, Matthew 24:1-34 had its
fulfillment in a.p. 70 with the destruction of Jerusalem. George
Eldon Ladd, a premillennialist, writes that the “strongest objec-
tion to millennialism is that this truth is found in only one passage
of Scripture — Revelation 20. Non-millenarians appeal to the ar-
gument of analogy, that difficult passages must be interpreted by
clear passages. It is a fact that most of the New Testament writ-
ings say nothing about a millennium.” This is a weighty argu-
ment. Why is so much of the church preoccupied with a doctrine
that most of the New Testament does not even discuss?#!

A Different Emphasis

Historically, the church has never made eschatology — that is,
a particular millennial position—a test of orthodoxy, Christian
Reconstructionists, as heirs of the Reformation, put the emphasis
where the Bible puts it: The sovereignty of God, justification by
grace through faith alone, and keeping the commandments of
God out of love for God (John 14:15; cf. James 2:14-26).

19. Archer, et al., The Rapture, p. 47.

20. George Eldon Ladd, “Historic Premillennialism,” in Meaning of the Millen-
nium, p. 38.

21. William Masselink, Way Thousand Years?, or Will the Second Coming be Pre-
Millennial? (Grand Rapids, MI; Eerdmans, 1930), pp. 196-208.

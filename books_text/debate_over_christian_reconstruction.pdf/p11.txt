Foreword xi

tive (the total worldview) of Christian Reconstruction, but tar-
geted only one element of it: its victorious millennial eschatology.
This one faux pas alone precluded their winning the debate be-
cause the question being debated was whether Christian Recon-
struction is a “deviant theology,” and millennial eschatology has
never in the history of the evangelical Christian church been
made a creedal point of orthodoxy which defines heresy or apos-
tasy. All Christians of good will who profess “the holy catholic
[universal] church” (the Apostles’ Creed) recognize that others
who hold millennial interpretations different from their own are
nevertheless their Christian brothers and sisters in the Lord. They
may be mistaken, but to accuse them of “deviant” theology is an
altogether different—and very serious—charge. Hunt and Ice
were incapable (perhaps unqualified) to substantiate such a grave
charge. Heresy-hunters bear a heavy responsibility for theological
proficiency, and (like all Christian teachers) will come under
greater judgment for their inaccuracies (James 3:1).

Even more, as the present book demonstrates, Hunt and Ice
left themselves open to ready refutation on the particular points of
eschatology they addressed, from the exegesis of Matthew 24 to
the biblical concepts of victory and dominion. Their historical
claims were equally flimsy. The reader can explore this general
observation for himself, I wish to point out but one particular and
conspicuous defect in the argumentation of the Reconstructionist
critics and comment upon it: their penchant for misrepresentation
of what they were called upon to criticize. It is especially because of
this (and not simply the academic shortcomings) that we must judge,
ethically, that critics Hunt and Ice lost the debate. Repeatedly we
encounter allegations and critical assumptions about Reconstruc-
tionist eschatology which are misleading, false portrayals of it—
for instance, the suggestion that a preterist interpretation of the
Olivet Discourse is essential to it, or that it is an innovation from
theological liberalism which claims no Biblical support, or that it
has affinity with the positive confession movement or Manifest Sons
of God, or that it promotes dominion “over people” (tyranny?), or
that it does not allow Christ to rule over His earthly kingdom, etc.

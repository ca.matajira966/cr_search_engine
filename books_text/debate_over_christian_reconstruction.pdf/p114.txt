96 The Debate over Christian Reconstruction

faith of the Church.” But not all agree. Take, for example, a
Master’s Thesis presented to the faculty of the Department of His-
torical Theology of Dallas Theological Seminary, a dispensational
premillennial school. The author writes:

It is the conclusion of this thesis that Dr. Ryrie’s statement is
historically invalid within the chronological framework of this
thesis. The reasons for this conclusion are as follows: 1), the writ-
ers/writings surveyed did not generally adopt a consistently ap-
plied literal interpretation; 2), they did not generally distinguish
between the Church and Israel; 3). there is no evidence that they
generally held to a dispensational view of revealed history; 4).
although Papias and Justin Martyr did believe in a Millennial
kingdom, the 1,000 years is the only basic similarity with the
modern system (in fact, they and dispensational premillennialism
radically differ on the basis for the Millennium); 5). they had no
concept of imminency or a pretribulational rapture of the
Church; 6). in general, their eschatological chronology is not
synonymous with that of the modern system. Indeed, this thesis
would conclude that the eschatological beliefs of the period
studied would be generally inimical [i.c., contrary] to those of the
modern system (perhaps, seminal amillennialism, and not nas-
cent [i.c., emerging] dispensational premillennialism ought to be
seen in the eschatology of the period).2”

So then, it’s amillennialism that shows up in the early church.
As was noted earlier, amillennialism and postmillennialism are
very similar in that many of the millennial blessings are mani-

26. Charles C. Ryrie, The Basis of the Premillennial Faith (Neptune, NJ:
Loiseaux Brothers, 1953), p. 17.

7. Alan Patrick Boyd, “A Dispensational Premillennial Analysis of the
Eschatology of the Post-Apostolic Fathers (Until the Death of Justin Martyr)”
(Th.M, thesis, Dallas Theological Seminary, 1977), pp. 90-91. In a footnote, the
author states the following:

Perhaps a word needs to be said about the eschatological position of
the writer of this thesis. He is a dispensational premillennialist, and he
does not consider this thesis to be a disproof of that system, He originally
undertook the thesis ta botster the system by patristic research, but the evidence of the
original sources simply disallowed this (p. 91, note 2). Emphasis added.

100 The Debate over Christian Reconstruction

But Tommy Ice is not satisfied with the general eschatological
view of the Nicene Creed. He is eager to have it read as a premil-
lennial tract. He tells us that “their own written document inter-
prets the final statement as a future kingdom.” The following is
quoted by Tommy Ice as a commentary on the Nicene Creed that
he maintains was written by the creed’s framers: %6

The world was made less on account of God's providence, for
God knew beforehand that man would sin. For that reason we
look forward to new heavens and a new earth according to the Holy
Scriptures: the appearance in the Kingdom of our Great God and
Savior, who will become visible to us. And as Daniel says, “The
holy ones of the Most High shall receive the Kingdom.” And
there will be a pure holy earth, the land of the living and not of the dead,
of which David, seeing with eye of faith, is speaking (Ps. 27:13):
*I believe that I shall see the goodness of the Lord in the Jand of
the living” — the land of the meek and humble.2”

Let’s suppose for a moment that this “commentary” on the
Nicene Creed is official®* and that it does teach premillennialism.
The fact that the Creed itself avoids taking a position supports the
contention of postmillennialists that other millennial positions
operated and were considered orthodox in the early church per-
iod. If premillennialism had been the only orthodox position, the
creeds themselves would express the position forthrightly. They
do not.

But the question remains: Does this “commentary” on the

36. This material was included in a letter written by Tommy Ice to Pastor
John A. Gilley, November 13, 1987,

37, Tommy Ice offers no bibliographical information for this quotation.
Although there are a number of differences in translation, this quotation can be
found in Historia Actorum Concilit Niceni, quoted in J. W. Brooks, Elements of Pro-
phetical Interpretation (London: R. B. Seeley and W. Burnside, 1836), p. 55.

38. Philip Schaff writes: “Official minutes of the transactions [of the Council]
were not at that time made; only the decrees as adopted were set down in writing
and subscribed by all (comp. Euseblius] Vita Const[antinel. iii, 14). All later ac-
counts of voluminous acts of the council are sheer fabrications (comp. Hefele, i.
p. 249 sqq.).” History of the Christian Church, 8 vols. (Grand Rapids, MI: Eerd-
mans, [1910] 1979), vol. 3, p. 622.

 

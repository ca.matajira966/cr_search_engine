xii The Debate over Christian Reconstruction

None of this is even remotely accurate. And that fact is highly sig-
nificant if we are sensitive to Biblical ethics.

Of course, this is not the first time by any means that Recon-
structionist thought has suffered abuse from those who have not
responsibly studied the issues or bothered to be fair in how they
depict its distinctives. The faulty scholarship has been witnessed
over and over again, from minor points to thundering accusa-
tions. Ten years ago at a faculty forum on theonomic ethics at Re-
formed Theological Seminary (Jackson, Mississippi), one pro-
fessor publicly criticized the author of Theonomy in Christian Ethics
for the scholarly shortcoming of failing to interact with Delling’s
treatment (in Kittel’s Theological Dictionary of the New Testament) of
“fulfill” from Matthew 5:17, only to be informed to his embarrass-
ment that Delling’s treatment was rehearsed and rebutted on page
64 of the book he was criticizing! This may seem a minor point,
and relative to others it is.

In that same year, Evangel Presbytery (of the Presbyterian
Church in America) publicly declared that ministerial candidates
holding a theonomic view were unacceptable to the church. More
remarkable than this harsh judgment, however, was the fact that
it was after the decision to promulgate it that the presbytery deter-
mined to appoint a committee to study the matter! A year later
the study committee recommended a reversal of the previous
judgment, acknowledging that it “was taken without proper study
and deliberation.” The committee’s report said: “We admit that
many of our minds were made up before we began this study. . . .
The vast majority of us... had never seen, much less read a
copy of the book [Theonomy in Christian Ethics].”

In 1978 Aiken Taylor, as editor of the Presbyterian Journal, wrote
in criticism of the theonomic (or Reconstructionist) position that it
was contrary to the Westminster Confession of Faith,? even as
others had hastily declared that it was not part of mainstream Re-
formed theological thinking. Such claims were readily refuted by

2. Aiken Taylor, “Pheonomy Revisited,” The Presbyterian Journal (December 6,
1978); Taylor, “Theonomy and Christian Behavior,” The Presbyterian Journal
(September 13, 1978).

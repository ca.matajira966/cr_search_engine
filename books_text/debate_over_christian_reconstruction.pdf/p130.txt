112 The Debate over Christian Reconstruction

while a debate over how Matthew 24:1-34 should be interpreted is
an interesting one, a preterist interpretation is not unique to
Christian Reconstruction. Christian Reconstruction does not
stand or fall on any single interpretation of this controversial pas-
sage. We do believe, however, that the preterist interpretation is
the correct one.

Does Tommy Ice know when these events will be fulfilled?
Will it be tomorrow or a thousand years from now? Let's assume
it’s tomorrow. Does this mean that Christians should abandon re-
forming their own lives and the world in which they live? I don’t
think so, What if the fulfillment is a thousand years off ?? Keep in
mind that it’s been nearly two thousand years since Jesus pre-
dicted His “imminent” appearing.3 If Tommy Ice’s views had been
prevalent in the church for the last two millennia, the church
would be much worse off than it is today. Such a view of history
only cripples the body of Christ (1 Thessalonians 5:1-1t; 2 Thessa-
lonians 3:6-15).

As has been mentioned numerous times in previous chapters,
a postmillennial eschatology is only one pillar of support for

 

words are as follows: “Woe unto them that are with child, and to them
that give suck in those days! But pray ye that your fight be not in the
winter, neither on the Sabbath day. For there shall be great tribulation,
such as was not since the beginning of the world to this time, no, nor ever
shall be.” Eusebius, History of the Church, “Predictions of Christ,” Book III,
Chapter VII. Many editions.

The verses that Eusebius quotes are found in Matthew 24:19-21, the very sec-
tion of Scripture that Tommy Ice relegates to a future fulfillment. Eusebius
writes that “these things took place in this manner in the second year of the reign
of Vespasian [a.p. 70], in accordance with the prophecies of our Lord and
Saviour Jesus Christ, who by divine power saw them beforehand as if they were
already present, and wept and mourned according to the statement of the holy
evangelists. . . .” Eusebius had Luke’s description of the destruction of Jeru-
salem in mind: Luke 19:42-44; 21:20, 23-24. The passages in Luke 21 parallel
thase in Matthew 24:1-34.

2. Traditional premillenniatism has taught that “Christ may return tomorrow
or a thousand years from now.” Garry Friesen, “A Return Visit,” Moody (May
1988), p. 31.

3. This makes perfect sense with a preterist interpretation. It makes no sense
with dispensationalism.

 

 

14 The Debate over Christian Reconstruction

say about Matthew 24:1-34, It is the key to Tommy Ice’s presenta-
tion and one of the most difficult chapters in the Bible to interpret.

Matthew 24; Setting the Scene

Matthew 24:1-34, like all Scripture, cannot be understood
without surveying its context, The context for Chapter 24 is found
in Chapter 23. Keep in mind that in the original manuscripts,
there were no chapter and verse divisions. In the Greek text,
Chapter 24 follows immediately after Chapter 23. The disciples
had just heard Jesus pronounce His seven “woes” on the Pharisees.
Jesus ends with this bombshell: “Behold, your house is being left
to you desolate!” (Matthew 23:38). Chapter 24 begins with, “And
Jesus came out from the temple and was going away when His
disciples came up to point out the temple buildings to Him” (24:1).
So then, the “house” that is being left “desolate” is the “temple.”
The disciples were obviously curious. So they asked the following
question: “Tell us, when will these things be, and what will be the
sign of Your coming, and of the end of the age” (v. 3).

Jesus told the Pharisees, “Truly I say to you, all these things
shall come upon this generation” (23:36). Jesus answers the disciples’
questions relating to the time and signs of Jerusalem’s destruc-
tion. The Old Covenant order would end with the destruction of
Jerusalem. This would be the “sign” of the “end of the age” of the
Old Covenant and the consummation of the New Covenant.

The Time Text: “This Generation”
The time texts are found in Matthew 23:36 and 24:34. They
form eschatological bookends for this study:

‘Truly I say to you, all these things shall come upon this gener-
ation (23:36).
Truly I say to you, this generation will not pass away until all

these things take place (24:34).

Sandwiched between these two time texts are the “sign” texts. Ice
asserts that “this generation” docs not mean the generation to

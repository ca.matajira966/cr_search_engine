118 The Debate over Christian Reconstruction

There are numerous prophetic statements in the Gospels re-
garding Jerusalem's demise (e.g., Matthew 21:33-46; 22:1-14;
23:31-38; 24:1-34).

Later, in Acts 2:16-24, the Pentecostal manifestation of
tongues in Jerusalem was an indicator of the approaching “day of
the Lord.” Tongues-speaking was a warning sign to the present
audience of the necessity of their being “saved from this perverse gen-
eration” (Acts 2:40).6

Acts 2:43-47 and 4:32-37 present corroborating evidence that
the destruction of Jerusalem was imminent. There would be little
use for property that was located in a nation dominated by invad-
ing Roman armies. Practically speaking, the revenue from such
sales would be better spent on gospel outreach, This is quite a
different scenario from Jeremiah’s day, when the Lord instructed
him to éyy land in Israel for an eventual return ( Jeremiah 32:25),

The selling of property is mentioned as occurring only in Jeru-
salem. This action did not become part of early church doctrine,
contrary to contemporary Christian socialists.’ The sale of prop-
erty and distribution of the profits can be related to the imminent
destruction of the city prophesied by Jesus in Matthew 24:34 and
elsewhere, and heeded by the disciples. Jerusalem's destruction
was coming in that generation. The land would be worthless to
the escaping Christians who were warned by Jesus to “flee.”

The Jewish leaders continued to reject Jesus during this forty
year period of extended mercy. Stephen called them “stiff-necked
and uncircumcised in heart and ears and always resisting the Holy
Spirit” (Acts 7:51). Paul speaks of these Jews as those who “always

6. See O. Palmer Robertson, “Vongues: Sign of Covenantal Curse and Bless-
ing” in Westminster Theological Journal (1977), pp. 43ff.; Richard Gaffin, Perspectives
an Pentecost (Phillipsburg, NJ: Presbyterian and Reformed, 1979), pp. 102A;
Kenneth L. Gentry, Jr., Crucial Issues Regarding Tongues (Mauldin, SC: Good-
Birth, 1982), pp. 14-20.

7. For a critique of a socialistic interpretation of this passage, see David
Chilton, Productive Christians in an Age of Guilt-Manipulators (3rd rev. ed; Tyler, TX:
Institute for Christian Economics, 1988), pp. 169-170; and Gary DeMar, God and
Government: Issues in Biblical Perspective (Atlanta, GA: American Vision, 1984), pp.
203-206.

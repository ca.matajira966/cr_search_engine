128 The Debate over Christian Reconstruction

What then is the abomination that makes desolate? Let’s put
all the references together, reconciling Matthew 24:15, Mark 13:14,
Luke 21:20, Daniel 9:24-27, 11:31, 12:11, 2 Thessalonians 2:1-12,
and Revelation 13:11-18 and see how these fit an a.p. 70 fulfillment.

We can dismiss Revelation 13:11-18 as having nothing to do
with the abomination of desolation. Therefore, we should not be
looking for the fulfillment of an image being set up in the temple
that comes to life for the people to worship. Remember Ice’s first
point: “It happens in the Jewish temple in Jerusalem.” There is no
mention of the “temple in Jerusalem” in Revelation 13, the only
place where an image is said to come to life. In fact, the temple is
last mentioned in Revelation 11:19, and it’s “in heaven.” The tem-
ple is mentioned again in 14:14, It too is in heaven. None of the
other passages, including 2 Thessalonians 2:1-12, refer to an im-
age coming to life. We can conclude, therefore, that Revelation 13
has nothing to do with the abomination of desolation.

What about Daniel 9:27? Again, the dispensationalist, in or-
der to make this a future event, must separate the 70th week from
the 69th week. Whatever the interpretation is, it comes on the
heels of the 69th week. It’s a past event from our perspective. We
do not need to look for a future fulfillment.‘

19. Dispensational premillennialism, in order to salvage their eschatological
system, must construct a revived Roman Empire. Leon J. Wood, a noted dis-
pensational commentator writes:

At some point in this symbolism [of Nebuchadnezzat’s statue] an ex-
tended gap in time must be fixed, because by verse 44 the interpretation
describes the future day of Christ's millennial reign, as will be seen, Dan-
iel: A Study Guide Commentary (Grand Rapids, MI: Zondervan, 1975), pp.
39, 40. Emphasis added.

The gap “must be fixed” because a 70th week that immediately follows the
69th week docsn’t fit the dispensationalist’s eschatological system. Since no gap is
mentioned or even inferred, the interpreter should not speculale and create an
unnatural gap of nearly 2000 years!

For a rather humorous “picture” of this interpretation, see “Daniel and Reve-
lation Compared,” a chart designed and drawn by Clarence Larkin and repro-
duced in George M. Marsden, Fundamentalism and American Culture: The Shaping of
Twentieth Century Evangelicalism: 1870-1925 (New York: Oxford, 1980), pp. 58-59.
The “ten toes” are stretched like “Silly-Putty” over more than two thousand years
of history.

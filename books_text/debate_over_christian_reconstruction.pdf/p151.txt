Tommy Ice:.A Response— Part II 133

Behold, your house is being left to you desolate! For I say to
you, from now on you shall not see Me until you say, “Blessed is
He who comes in the name of the Lord” (Matthew 23:38-39),

When Jesus left the Temple for the last time, He was leaving it
empty and desolate. Just as when the shekinah glory departed
from the Temple in Ezekiel 8-11, the Temple was left desolate. The
desolate Temple was shortly filled with demons (Luke 11:20-26),
What was spiritually true in a.p. 30 became visibly true in a.p.
70: The Temple and the city were made a desolation. The Bible
teaches this in no uncertain terms. Jesus told His disciples that all
these things would come upon “this generation.”

Can we really maintain that this happened in a.p, 70 when
Jesus says that this tribulation is greater than anything before it,
and it will be greater than anything after it? We know that not
even the predicted future “Great Tribulation” of dispensationalism
will be greater than the flood that left only eight people alive (cf.
Revelation 8:8-12). Jesus is using a figure of speech, proverbial
language, common to the Jewish ear to make His point of certain
destruction.

And the locusts came up over all the land of Egypt and settled
in all the territory of Egypt; they were very numerous. There had
never been so many locusts, nor would there be so many again

(Exodus 10:14).

And I will give you [Solomon] riches and wealth and honor,
such as none of the kings who were before you possessed, nor
those who will come after you (2 Chronicles 1:12; cf. 1 Kings 3:12).

A day of darkness and gloom, a day of clouds and thick dark-
ness. As the dawn is spread over the mountains, so there is a
great and mighty people; there has never been anything like it,
nor will there be again after it to the years of many generations

(Joel 2:2),

In terms of this single generation’s great sin of crucifying their
Savior, the judgment that was poured out upon the once-holy city
was quite appropriate. The language that Jesus used to describe

Tommy Ice: A Response — Part IT 41

and is enthroned at the Father’s right hand. They will “see the sign
of the heavenly enthronement of the Son of Man” in the destruc-
tion of Jerusalem. The Old Covenant order with its types and
shadows has passed away. The heavenly Jerusalem remains.
After all, the destruction of Jerusalem was to serve as a sign to
Jesus’ tormentors in Matthew 26:64 that He was the predicted
Son of Man who was to receive “dominion, glory, and a kingdom,
that all the peoples, nations, and men of every language might
serve Him’—a kingdom “which will not pass away” or “be de-
stroyed” (Daniel 7:13-14).

But what is the “sign”? Jesus said that the only sign that would
be given to that generation, which is the generation upon which
all these things was going to come, was the sign of Jonah (Matthew
12:38-45; 16:1-4), These verses show that the “sign of Jonah” in-
volved three things, First, the death, resurrection, and glorification
of Jesus Christ. Second, judgment upon demonized (apostate)
Israel, as God told Jonah to leave Israel. And third, salvation to
the Gentiles, as Jonah preached to Nineveh and Nineveh was con-
verted, or as the Queen of Sheba came to Solomon.

The sign of the Kingdom, then, is the whole complex of events
in the first century, as pictured in type by Jonah. After the destruc-
tion of Jerusalem, the Gentiles would perceive this sign as well.
They would perceive the redemptive work of Jesus Christ, and
they would perceive that the gospel had been sent to them.

7. And He will send forth His angels WITH A GREAT
TRUMPET and THEY WILL GATHER TOGETHER His
elect FROM THE FOUR WINDS, FROM ONE END OF THE
SKY [HEAVEN] TO THE OTHER?” (v. 31).

Here again we find symbols that we should let the Bible inter-
pret for us. Immediately after the destruction of Jerusalem, God
begins to shake down the world (Matthew 24:29). The nations
begin to recognize Christ as King (v. 30). What else? In context,
verse 31 is not jumping to the end of the world. Rather, it is speak-
ing of the sending out of the gospel to the nations of the world.

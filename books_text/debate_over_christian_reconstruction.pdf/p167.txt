Dave Hunt: A Response 149

I believe these statements from Barnhouse and Johnson are
extreme® and certainly “on the edge” of what the Bible teaches and
historic Christianity has taught. Consider the Bible. God's stan-
dard of justice is the same for all His creatures, whether Jew or
Gentile. This even includes nations which consider themselves to
be non-Christian. Some believe that because they do not acknowl-
edge God as Lord and King they somehow are exempt from fol-
lowing the law of God, Sodom and Gomorrah enjoyed no such
exemption: “Now the men of Sodom were wicked exceedingly and
sinners against God” (Genesis 13:13). This wicked city was de-
stroyed for breaking God’s law: in particular, the sin of homosexu-
ality (Genesis 19:4-5; Leviticus 18:22; 20:13).7 Jonah went to
preach to the non-Israelite city of Nineveh because of its national
sins. If the Ninevites were not obligated to keep the law of God,
then how could they be expected to repent, and why was God
about to judge them (Jonah 3)?

The stranger, an individual outside the covenant community of
Israel, was obligated to obey the law of God: “There shail be one
standard for you; it shall be for the stranger as well as the native,
for I am the Lorp your God” (Leviticus 24:22; cf. Numbers 15:16;
Deuteronomy 1:16-17).

The law as given to Israel was a standard for the nations sur-
rounding Israel also. When these nations heard of the righteous
judgments within Israel, they would remark with wonder: “Surely
this great nation is a wise and understanding people” (Deuteron-
omy 4:6). The psalmist proclaims to the kings and judges of the
earth “to take warning... and worship the Lorp with rever-
ence...” and to do “homage to the Son” (Psalm 2:10-11).

It is striking how frequently the other nations are called upon
in the Psalms to recognize and to honor God, and how complete
is the witness of the prophets against the nations surrounding
Israel. God does not exempt other nations from the claim of his

6. In talking with a number of dispensationalists and former students of
Dallas Theological Seminary, I've been told that this statement by Barnhouse is
mild in comparison with some other dispensational writers.

7. Sec Gary DeMar, “Homosexuality: An Hlegitimate Alternative Death-
style,” The Biblical Worldview ( January 1987}, Vol. 3, No. 1.

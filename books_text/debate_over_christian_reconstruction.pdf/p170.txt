152 The Debate over Christian Reconstruction

Again, love without specifics becomes license. Love must always
be defined in some way. The law gives definition to love. Besides,
did not Paul “confirm” the law (Romans 3:31), which was “holy,
just, and good” (7:12, 14)?

The redemptive work of Jesus does not free us from an obliga-
tion to keep the moral law — including the social ones—laid down
in the Bible. Scripture shows no instance of an individual, Chris-
tian or pagan, who is no longer required to keep the laws outlined
in Scripture. Christians are freed from the “curse of the law”
(Galatians 3:13), but not from the demands of the law: “Do we
then nullify the Law through faith? May it never be! On the con-
trary, we establish the Law” (Romans 3:31). Of course, the non-
Christian is free neither from the curse of the law nor from the de-
mands of the law: “He who believes in Him is not judged [because
he is free from the law’s curse]; he who does not believe has been
judged already, because he has not believed in the name of the
only begotten Son of God” { John 3:18).

Now, I recognize that Geisler, Johnson, Barnhouse, and other
dispensationalists might want to say that the “fundamental moral-
ity” of the Old Testament law is binding on all peoples and binding
in the New Testament. They would go to New Testament pas-
sages to show this. Also, I recognize that seme orthodox dispensa-
tionalists may teach that the New Testament “church” is the
“mystery form” of the “Millennial Kingdom,” and thus that the
laws of the Kingdom have some “shadowy” relevance to us. It is
also true that ignoring the Old Testament law is hardly a problem
unique to dispensationalism. No branch of Christianity has done
much with the Mosaic law in the last two centuries. It remains a
fact, however, that the Mosaic law is absolutely fundamental to
the wisdom of Proverbs, the praise of the Psalms, the preaching of
the Prophets, and the glories of the New Covenant. We are not
“bound” under the Mosaic law in the sense that we live under the
Old Covenant, but at the same time we dare not despise its wis-
dom. After all, God wrote it for our good. Some dispensationalists
say that “All the Bible is for us, though not all the Bible is ad-
dressed to us.” Very well, then, let us assiduously study the Mosaic

Dave Hunt: A Response 155

nor depth, nor any other created thing, shall be able to separate us from the
love of God, which is in Christ Jesus our Lord (Romans 8:35-39).

Reformed theology has always taught that suffering is in God’s
hands, that He administers and oversees it. Because it is God’s
doing, we can rejoice even in suffering. The book of Job is a testi-
mony of one man’s experience in the midst of suffering. God was
in control throughout the ordeal. Satan was on God's leash (Job
1:6-12; 2:1-6). In the end, God gets the glory for Job’s suffering.
R. J. Rushdoony writes that it was the book of Job that made him
a Calvinist.'* The book of Job forces the Christian to give up any
lingering autonomy to the uncompromised sovereignty of God.
Rushdoony shows the significance of suffering in terms of God's
sovereignty:

Many a godly man has been afflicted as Job was afflicted, has
seen his life’s work dissolved by catastrophe, has seen the wicked
prosper while he has been brought low, humbled, and destroyed,
has cried out with Job in agony of spirit and bitterness and won-
dered at the ways of God that permitted such things to come to
pass. The conclusion that Job reached therefore, whereby he
understood the standard of God in dealing with himself and with
all men, becomes especially relevant to our generation. When
Job was first laid low, found himself stripped of all his posses-
sions, his family destroyed, and he himself sick both in body and
soul, his immediate reaction was one of faith, “Naked I came out
of my mother’s womb, and naked shall I return thither; The Lord
gave, and the Lord hath taken away: blessed be the name of the
Lord. In all this Job sinned not, nor charged God foolishly” (Job
1:21, 22).15

Job didn’t deny suffering, confess wellness, give a “positive confes-
sion,” think positively, or blame God. He responded in faith in the
midst of his distress, knowing that in the end, either in life or in

14. R. J. Rushdoony, By What Standard: An Analysis of the Philosophy of Cornelius
Van Til (Tyler, TX: Thoburn Press, [1958] 1983), p. 189.
15. Ibid., pp. 189-90.

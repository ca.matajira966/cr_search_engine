184 The Debate over Christian Reconstruction

of dispensationalism. Without it, the entire system falls to pieces.
J. Dwight Pentecost states that a post-tribulational rapture is in-
consistent with dispensationalism, He writes:

(1) Posttribulationism must be based on a denial of dispensa-
tionalism and all dispensational distinctions. It is only thus that
they can place the church in that period which is particularly
called “the time of Jacob’s trouble” (Jer. 30:7). (2) Consequently,
the position rests on the denial of the distinction between Israel
and the church.”

The debate over the rapture is a disagreement over timing and
meaning. There is little consensus in the church today over the
timing of the rapture, even among premillennialists.® Premillen-
nialists of all types subscribe to pre-, mid-, and post-tribulational
theories of the rapture, Amillennialists and postmillennialists see
the rapture simply as the ascension of the saints on the last day be-
fore the general judgment of the living and the dead. For them,
the rapture is not separated from the resurrection by a seven year
tribulation period or a thousand year millennium.

Dave Hunt states that the rapture comes from the Bible.
There is no disagreement about this. For Hunt, the “rapture sim-
ply means ‘an ecstatic catching away.” But when does this hap-

7, J, Dwight Pentecost, Things to Come: A Study in Biblical Eschatology (Grand
Rapids, MI: Zondervan, [1958] 1964), p. 164

8. Robert H. Gundry, The Church and the Tribulation (Grand Rapids, MI:
Zondervan, 1973); Gleason L. Archer, Jr, et al., The Rapture: Pre-, Mid-, or Post-
Tribulational? (Grand Rapids, MI; Zondervan, 1984); John F. Walvoord, The
Rapture Question (rev, and enl.,; Grand Rapids, MI: Zondervan, 1979), William
R, Kimball, The Rapture: A Question of Timing (Grand Rapids, MI: Baker Book
House, 1985); Dave MacPherson, The Great Rapture Hoax (Fletcher, NC: New
Puritan Library, 1983), and The Incredible Cover-Up (Medford, OR: Omega
Publications, [1975} 1980).

John Walvoord states that “four different views of the rapture have heen ad-
vanced.” He asserts that “among premillenarians a wide variety of views can be
found, varying from the extreme of date-setting on the one hand to discounting
any imminent hope of the Lord’s return on the other.” The Blessed Hope and the
Tribulation: A Historical and Biblical Study of Posttributationism (Grand Rapids, MI:
Zondervan, 1976), p. 7.

Questions and Answers 185

pen? The clearest text on the rapture, 1 Thessalonians 4:13-18,
doesn’t say anything about a rapture prior to the Tribulation, nor
does it say anything about a rapture prior to the millennium. Nor
does it speak of a “secret” rapture. As a matter of fact, 1 Thessalo-
nians 4:13-18 is one of the noisiest in Scripture — with the voice of
the archangel, the trumpet of God, and a shout! It simply states
that “we shall always be with the Lord” (v. 17). There is no hint of
coming back to the earth after a seven year period of tribulation.

Question 2; What is the relationship between postmillennialism
and Christian Reconstruction, and is Christian Reconstruction
solely dependent on any millennial position?

At this juncture in the debate, the real differences between
Christian Reconstruction and dispensational premillennialism
become evident. Tommy Ice states that he and David Schnittger,
author of Christian Reconstruction from a Pretribulaitonal Perspective,

tried to put together a premillennial Reconstructionist ethic. Z
don't believe you can do it. The issue of eschatology is: How are you
involved in the present world, not whether or not you are in-
volved in the present world. Premillennialists have always been
involved in the present world. And basically, they have picked up
on the ethical positions of their contemporaries. Gary North
keeps telling us that ethics is the issue and not theology [cschatol-
ogy]. The issue is that your theology tells you what your ethics are.
... We are motivated ethically by the Second Coming of Jesus
Christ. Scripture constantly talks about that, like in 1 John 3:2:
“Beloved, now we are children of God, and it has not appeared as
yet what we shall be. We know that when He appears, we shal] be
like Him, because we shall see Him just as He is. And every one
who has this hope fixed on Him purifies himself, just as He is pure.”

Tommy Ice admits that a Reconstructionist ethic, that is, an
ethical system that can be applied, for example, to economics,
law, politics, and education, cannot be developed within dispen-
sationalism. Ice tells us that dispensationalists must “pick up on
the ethical positions of their contemporaries.” What does this

192 The Debate over Christian Reconstruction

parable” Jesus states, “The kingdom of heaven is like leaven,
which a woman took, and hid in three pecks of meal, until it was
all leavened.” The parable of the leavened loaf teaches us that the
kingdom of God grows among the nations as leaven permeates the
dough “until it was aff leavened” (Matthew 13:33).

The gospel message went as far as Spain in Paul’s day (Romans
15:24, 28), Doesn’t this constitute progress? From twelve apostles,
to seventy disciples, to one hundred and twenty gathered in one
place to await the arrival of the Holy Spirit, to five hundred who
saw the risen Christ, to nearly three thousand conversions after
Peter’s first sermon. Aren’t these examples of progress? In a span
of forty years, the gospel of Jesus Christ had spread throughout
the entire inhabited world (Colossians 1:23). Paul preached the
gospel to the highest-ranking rulers in the Roman Empire. He
may have even preached to Caesar himself. Now, where is the
Roman Empire today? It does not exist. Where is God’s king-

13. Dispensationalists try to get around the growth of the kingdom by main-
taining that “leaven is always evil.” Ray C. Stedman, a popular dispensational
Bible expositor, calls the parable of the woman who places the leaven in the
dough, “The Case of the Sneaky Housewife.” Ray C. Stedman, Expository Studies
in Matthew 13: Behind History (Waco, TX: Word, 1976), pp. 79-102. For Stedman,
leaven is evil. He contends that Jesus is teaching in this parable that the kingdom
is permeated with corruption.

But putting leaven (yeast) in dough is natural to the bread-muking process.
There is nothing sinister about this. Anyway, leaven is not always evil. Leaven is
to be included for a “wave offering”: “They shall be of fine flour, baked with
leaven as first fruits to the Lorn” (Leviticus 23:17; also 7:13). Jesus chose leaven
because of its expansive quality to illustrate the growth of the kingdom, Like the
“wave offering” depicting “first fruits,” this parable describes God's kingdom ac-
tivity among the nations. In other cases leaven is evil: It’s called the “leaven of the
Pharisees* and the “leaven of Herod” (Mark 8:15; cf. 1 Corinthians 5:7-8).

If we follow the logic of “leaven is always evil,” then we end up with impossible
interpretive problems. The “serpent” is generally associated with evil (Genesis 3:13;
Psalm 58:4; 140:3; Proverbs 23:32; Isaiah 27:1; Matthew 23:33; 2 Corinthians
11:3; Revelation 12:9, 14-15; 20:2). But God instructs Moses to create a “bronze
serpent” so that everyone who looks at it will live (Numbers 21:6-9). In the New
‘Testament, Jesus is io “be lifted up” as “Moses lifted up the serpent” (John 3:14).
Jesus instructed His disciples to be “shrewd as serpents, and innocent as doves”
{John 10:16). Of course, Satan is described as a “roaring lion” (1 Peter 5:8) and
Jesus is the “Lion that is from the tribe of Judah” (Revelation 5:5).

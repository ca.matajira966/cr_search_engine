Concluding Remarks by Dave Hunt and Tormmy Ice 221

this view was around during the time of the carly church fathers.
So then, some postmillennialists hold the position that the millen-
nium is tied to the year 2000, while others extend it way into the
future. But let's all stay close to biblical reality: No one knows when
Jesus will return. Let's get on with the work at hand: Leading peo-
ple to Jesus and showing them how to live in terms of Gad’s Word.

What should the Christian take comfort in? Hunt says it’s the
rapture. For nearly 2000 years the church did not experience this
comfort. No, the comfort is that when we die we will always be with
the Lord. Every generation of Christians can rejoice in this truth,
“For to me, to live is Christ, and to die is gain” (Philippians 1:21).

Tommy Ice: Summary and Critique

Tommy Ice began his summary by answering a question that I
put to Dave Hunt: “Where in Revelation 20 does it say that Jesus
will reign on the earth for a thousand years?” This was Tommy
Tce’s answer:

Gary, Dave said I could tell you the Scripture. It’s Revelation
20:6: “Blessed and holy is the one who has a part in the first resur-
rection; over them the second death has no power, but they will
be priests of God and of Christ and will reign with Him for a theu-
sand years.”

Notice that my question specifically asked where in Revelation 20
does it say that Jesus will reign on the earth. There is no mention
of Jesus being on the earth in Revelation 20. The passage tells us
that those who rule with Him “will be priests of God and of
Christ.” Scripture says this has already happened: “But you are a
chosen race, @ royal priesthood, a holy nation, a people for God’s
own possession, that you may proclaim the excellencies of Him
who has called you out of darkness into His marvelous light”
(1 Peter 2:9). The first chapter of Revelation tells us that “He has
made us to be a kingdom of priests to His God and Father” (Revela-
tion 1:6; cf. 5:10). God has “delivered us from the domain of dark-
ness, and transferred us to the kingdom of His beloved Son” (Col-
ossians 1:13).

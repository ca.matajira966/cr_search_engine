246 The Debate over Christian Reconstruction

In the Spring 1988 issue of New Hope Notes, the author of an ar-
ticle with the title “Political Power: Battle for the Soul of the
Church,” writes this about postmillennialism:

Postmillenniatism, then, does not respect the promises Ged
made to the Jews; those promises have been abrogated — nullified
because of perfidy [treachery]; repealed because of treason — the
betrayal of Messiah. Anti-Semitism is a stalking phantom which
often preys upon postmillennia! believers —not always, but quite
frequently (p. 9).

The author offers no evidence for this claim, If the premise is
true, that the promises to Israel have been abrogated, then no ethnic
group has them. Why then aren’t postmillennialists anti-Italian,
anti-German, anti-Spanish, anti-Chinese, anti-Japanese, and anti-
Trish, since none of these ethnic groups has the promises? Anti-
Semitism has very little to do with the promises being abrogated.
No, the reasons for anti-Semitism must be found elsewhere.

Anti-Semitism, along with other forms of racism, is the unfor-
givable sin of the twentieth-century West. Generally, however,
anti-Semitism is left undefined. The two articles just mentioned
seem to define an anti-Semite as anyone who denies that Israel
should be the “most honoured of all nations,” or says a bad word
about things Jewish, or suggests that the suffering of the Jews is
God's judgment upon their unbelief, or believes that the Church is
the New Israel made up of Jews and Gentiles (John 10:16;
Romans 11:24; Galatians 3:28-29; Ephesians 2:13-16; 3:4-6), or
believes that the Israeli state begun in 1948 has nothing to do with
biblical prophecy, Obviously, this usage of “anti-Semitism” is so
broad as to be virtually meaningless, since the great majority of
Christians do believe that the Church, made up of Jews and Gen-
tiles, is the New Israel. Even dispensationalists, moreover, deny
that 1948 was a significant date on the prophetic calendar, Charles
L, Feinberg, writing in the October 1955 issue of Bibliotheca Sacra,
the theological journal of Dallas Theological Seminary, stated em-
phatically that “the present return to the land is not the fulfillment

NAME INDEX

Adams, Jay, 61, 90, 237
Adams, John Quincy, 43
Alexander, J. A., 113, 181

Allis, Oswald T., 1, 106

Alsted, Johann Heinrich, 250
Anderson, Kerby, 179

Archer, Gleason L., 81, 83, 184
Armstrong, Herbert W., 109
Athanasius, 181

Augustine, 179, 181, 182

Bacchiocchi, Samuele, 226

Bahnsen, Greg L., 3, 9, 67, 92,
186-87, 199, 20¢

Barnhouse, Donald Grey, 148, 149, 152

Barron, Bruce, 156-57

Bass, Clarence B., 1, 102, 107, 180, 181

Bavinck, J. H., 150, 152

Bell, William Everett, 97, 101

Benware, Paul N., 109

Berkhof, Louis, 77, 97

Beza, Theodore, 247

Blackstone, William, 258

Blue, Ron, 24

Blumenfeld, Samuel, 39

Bobgan, Martin and Deidre, 13, 36, 6

Boersma, T., 88, 121

Boettner, Loraine, 88, 170

Bogue, David, 104

Bowman, Robert M., Jr., 3-5, 89

Bowsher, Herbert, 172

Boyd,, Alan Patrick, 96-97, 98, 180

Brewer, Josiah, 17

Bridge, William, 157

Brightman, Thomas, 247

Brooks, J. W., 100
Brown, David, 123, 260
Brown, Harold O. J., 9, 103
Brown, John, 123, 123, 253-54
Bucer, Martin, 247-48

Butler, Paul T., 134

Calvin, John, 34, 78, 129-30, 138-39,
181, 182, 247, 258

Campbell, Roderick, 120

Campolo, Anthony, 234

Canfield, Joseph M., 102

Carroll, B. H., 113

Capps, Charles, 157

Carver, Everett I., 90

Chafer, Lewis Sperry, 9, 148, 206-7

Chantry, Walter J., 88

Chilton, David, 45, 79, 116, 118, 120,
127, 134, 137, 167, 168, 203, 217,
218-19, 224, 237, 245

Glapp, Rodney, 154

Clouse, Robert G., 1, 81, 90

Colson, Charles, 159

Coolidge, Calvin, 43

Copeland, Ken, 156

Cox, William E., 1, 90, 102

Grane, Rev. E., 177

Crenshaw, Curtis I., 1, 9, 10, 207

Cushner, Harold S., 85

Dabney, Robert L., 254
Dallimore, Arnold, 105
Darby, J. N., 180, 182
Dawson, Christopher, 193
Dean, Robert L., 137

275

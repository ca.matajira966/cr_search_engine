1
TURNING THE WORLD UPSIDE DOWN

For nearly 2000 years the church has believed in the life-trans-
forming power of the gospel, the regenerating power of the Holy
Spirit, the sufficiency of Scripture as “an Instruction Book on how
to live,”!_ the Sovereignty and Providence of God in time and in
history, the subjection of Satan to the finished work of Christ and
the church (Matthew 16:18; Romans 16:20; Colossians 1:13; 2:15;
Revelation 12:7-9; Mark 3:27; Luke 10:18; 11:20; John 14:30;
16:11; 1 John 3:8; 5:18; James 4:7), the discipling of the nations
(Matthew 28:18-20), and the ultimate victory of God’s kingdom
that will one day be delivered up by Jesus to His Father (1 Corin-
thians 15:20-28; cf. Luke 11:20; Colossians 1:13-23).

This was the faith of the early church, a faith that prompted
those outside of Christ to acknowledge that in a short span of time
these Christians had “turned the world upside down” (Acts 17:6).
What a testimony! These few rag-tag disciples of Christ, with lit-
tle if any money, no national television ministries, and no publish-
ing houses or newsletters, had turned the world upside down.

How did they do it?

They took with them the Word of God, “sharper than any two-
edged sword” (Hebrews 4:12-13) and able to equip us “for every
good work” (2 Timothy 3:16-17), the gospel which is “the power of
God for salvation to every one who believes” (Romans 1:16), and
the Holy Spirit who equips us “in every good thing to do His will”

 

1. Martin and Deidre Bobgan, Psychoheresy: The Psychological Seduction of Christi-
anity (Santa Barbara, CA: Eastgate Publishers, 1987), p. I.

13

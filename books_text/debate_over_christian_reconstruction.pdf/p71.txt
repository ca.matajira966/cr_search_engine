The Future Is Ours 53

there is no earthly future hope? Who would invest in a losing
proposition? Why should anyone work to establish a godly home,
school, business, or civil government when all such institutions
seem doomed despite our efforts?

“We must become optimists concerning the victory that lies be-
fore Christ’s people, in time and on earth. We must be even more
optimistic than Joshua and Caleb, for they were only asked to spy
out the land of Canaan. They were called to give their report prior
to Christ’s sacrifice at Calvary. Why should we be pessimistic, like
that first generation of former slaves? Why should we wander in
the wilderness, generation after generation? Why should we
despair?”? The hope of the future is real because the Christian
knows that God governs the affairs of men and nations (Psalm
22:28; 47:8; Daniel 4:35).

Progress for the Godly

The Apostle Paul informs Timothy “that in the last days diffi-
cult times will come” (2 Timothy 3:1), The ungodly will manifest a
variety of characteristics which evidence their opposition to God’s
purposes: “For men will be lovers of self, lovers of money, boast-
ful, arrogant, evildoers, disobedient to parents, ungrateful, un-
holy, etc.” (vv. 2-5). Timothy is told to “avoid such men as these”
(v. 5).

Will the ungodly dominate culture? At first reading, 2 Timo-
thy 3 would seem to indicate that the ungodly will prevail, and
godly influence decline. Further study shows that the Apostle
Paul offers a different conclusion. Paul compares the progress of
the ungodly in Timothy’s day with that of Jannes and Jambres,
the Egyptian sorcerer-priests who opposed Moses (cf. Exodus
7:M): “But they will not make further progress; for their folly will
be obvious to all, as also that of those two came to be” (2 Timothy
3:9), While it is true there is an attempt by the ungodly to dominate
culture, the fact is, “they will not make further progress”; their

2. Gary North, Unconditional Surrender (3rd ed.; Tyler, TX: Institute for Chris-
tian Economics, 1988), p. 364.

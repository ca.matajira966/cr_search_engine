The Future fs Ours 55

vest for the present-oriented consumer is disastrous. With no
reserves, he possesses no hope for the future.

If the Christian looks only at present happenings he loses his
hope of becoming a cultural influence, since he perceives the
statement, “evil men and impostors will proceed from bad to
worse, deceiving and being deceived” (2 Timothy 3:13) as some-
thing permanent. But we also must remember the previous words
of Paul: “But they will not make further progress; for their folly
will be obvious to all” (v. 9), In the short-term, it appears that the
ungodly will prevail. Christians, however, must begin to think
long-term; while the ungodly burn themselves out, the godly
steadily influence their world: “You, however, continue in the
things you have learned and become convinced of” (v. 14), In
time, the effects of dominion will be seen: “And let us not lose
heart in doing good, for in due time we shall reap if we do not
grow weary” (Galatians 6:9).

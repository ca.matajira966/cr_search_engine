60 The Debate over Christian Reconstruction

delight in the “law of the Lorn” will be “like a tree firmly planted
by streams of water, which yields its fruit in its season, and its leaf
does not whither; and in whatever he does, he prospers” (Psalm 1:3).
Who can expect the benefits from God’s inscripturated Word?
Faithful individuals, families, and nations. There is a domino
effect of good government, beginning with godly self-government
under God and extending to family, church, and State.? Families,
churches, business establishments, and the nation at large are
simply a reflection of individuals, for either good or evil. There is
also a domino effect of poor self-government (1 Timothy 3:1-7).

The Bible is filled with a “feedback” concept, both positive and
negative, God tells us in Deuteronomy 28 that “all these blessings
shall come upon you and overtake you, if you will obey the Lorp
your God? (v, 2). Deuteronomy 28 goes on to describe these bless-
ings in individual, family, and national terms. They can be
summed up with these verses:

 

The Lorp will make you abound in prosperity, in the off-
spring of your body and in the offepring of your beast and in the
produce of your ground, in the land which the Lor swore to
your fathers to give you.3 The Lorp will open for you His good
storehouse, the heavens, to give rain to your land in its season

2. Gary DeMar, Ruler of the Nations. Biblical Principles for Government (Ft. Worth,
TX: Dominion Press/Atlanta, GA: American Vision, 1987), pp. 3-53.

3. Jesus tells us that zee will “inherit the earth” (Matthew 5:5). Abraham is “heir
to the world [4osmos]” (Romans 4:13). Is this promise limited to ethnic Israel? No!
All “those who are of the faith of Abraham” share in the promise because he ‘is
the father of us all, (as it is written, ‘A FATHER OF MANY NATIONS HAVE 1
MADE YOU’) in the sight of Him whom he believed, even God, who gives life
ta the dead and calls into being that which does not exist” (vv. 16-17).

What is the scope of this promise? John Murray writes:

It is defined as the promise to Abraham that fe should be heir of the
world, but it is alsa a promise to his seed and, therefore, can hardly in-
volve anything less than the worldwide dominion promised to Christ and
to the spiritual seed of Abraham in him. It is a promise that receives its
ultimate fulfillment in the consummated order of the new heavens and
the new earth. The Epistle to the Romans, 2 vols. (Grand Rapids, MI:
Eerdmans, 1968), vol. 1, p. 142.

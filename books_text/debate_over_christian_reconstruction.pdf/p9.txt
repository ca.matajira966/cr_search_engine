FOREWORD
by Dr. Greg L. Bahnsen

The recent “debate” over Christian Reconstruction has been
going on for over ten years now. The debate in contemporary
Christian circles could actually be pushed back to the early 1970s,
with the publication or general recognition of certain probing
theological works by R. J. Rushdoony. (Of course, to the extent
that Reconstructionist theology is true to God’s Word, the debate
has been carried on throughout redemptive history, since the time
of the fall!) The active and open criticism of Reconstructionist dis-
tinctives as such, however, surfaced about a year following the
publication of Theonemy in Christian Ethics (1977) — and ironically,
surfaced within the context of that theological tradition which has
given historical impetus to the Reconstructionist perspective: the
circles of Presbyterian and Puritan conviction. The school of
thought, however, which most conspicuously and naturally stands
opposed to Reconstructionist theology is dispensationalism.

Reconstructionism contradicts the dispensationalist view of the
Old Testament (which emphasizes discontinuity with Old Testament
ethics) as well as the dispensationalist view of the millennium (which
emphasizes discontinuity with the present church age). There-
fore, dispensationalism most clearly and diametrically opposes
Reconstructionist distinctives. The first public debate between
a Reconstructionist and a dispensationalist took place at the annual
Evangelical Theological Society meeting, held in Toronto in 1981."

1. “The Bahnsen-Feinberg Debate.” Available from Covenant Tape Ministry,
Box 4134, Reno, Nevada 89510, tape #00340.

ix

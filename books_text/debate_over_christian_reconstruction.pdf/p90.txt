72 The Debate aver Christian Reconstruction

wanted to saddle the church with the bloody rite of circumcision.
What would he think of the revival of the entire sacrificial system
instituted while Jesus is in the midsi of His people? James H.
Snowden writes:

But this [memorial] interpretation cannot be allowed on pre-
millenarian principles, because the command given to the priests
in Ezekiel's temple is positive that “the priests” shall be given “a
young bullock for a sin-offering. And thou shalt take of the blood
thereof, and put on the four horns of it, and on the four corners
of the ledge, an upon the border roundabout; thus shalt thou
cleanse it and make atonement for it” (Ezckicl 43:19-20).2*

In the original edition of his reference Bible, Scofield has the fol-
lowing footnote on Ezckiel 43:19: “Doubtless these offerings will
be memorial, looking back to the cross, as the offerings under the
old covenant were anticipatory, looking forward to the cross. In
neither case have animal sacrifices power to put away sin (Heb.
10:4; Rom, 3:25).”25

The New Scofield Reference Bible attempts to clarify the issue re-
garding Ezekiel 43, but to no avail:

A problem is posed by this paragraph (wv. 19-27). Since the
N.T, clearly teaches that animal sacrifices do not in themselves
cleanse away sin (Heb. 10:4) and that the one sacrifice of the Lord
Jesus Christ that was made at Calvary completely provides for
such expiation (cp. Heb. 9:12, 26., 28; 10:10, 14), how can there
be a fulfillment of such a prophecy? Two answers have been sug-
gested: (1) Such sacrifices, if actually offered, will be memorial in
character. They will, according to this view, look back to our
Lord’s work on the cross, as the offerings of the old covenant an-
ticipated His sacrifice. They would, of caurse, have no expiatory
value. And (2) the reference to sacrifices is not to be taken literally, in
view of the putting away of such offerings, but is rather to be re-

24. James H. Snowden, The Coming of the Lord: Will it be Premillennial? (New
York: The Macmillan Company, 1919), pp. 216-17.
25. First edition, Scofield Reference Bible, p. 890.

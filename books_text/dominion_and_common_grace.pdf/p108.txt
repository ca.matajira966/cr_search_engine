92 DOMINION AND COMMON GRACE

over time. An increase of special grace (more bread
on the table of the faithful) leads to more common
grace (crumbs under the table). Common grace is
not earlier grace; it is later grace.

Van Til rejects such a view. Neither Kline nor
Rushdoony recognizes the extent to which Van Til’s
amillennialism has colored and distorted his whole
doctrine of common grace. Perhaps unconsciously,
he selectively structured the biblical evidence on this
question in order to make it conform with his Neth-
erlands amillennial heritage. This is why his entire
concept of common grace is incorrect: his eschatol-
ogy is incorrect.

It is imperative that Christians scrap the concept
of “earlier grace” and adopt a doctrine of common
(crumbs for the dogs) grace. As special grace in-
creases, so will common grace. As the world gets
richer and more peaceful, the “dogs” benefit.

The amazing irony of all this is that Rushdoony
never recognized the threat to his postmillennialism
that Van Til’s view of common grace presents. He
therefore never attempted to explain how postmil-
lennialism and Van Til’s common grace doctrine can
be reconciled. Obviously, they cannot be reconciled.
They are opposites, Nevertheless, Rushdoony’s in-
frequent and undeveloped references to common
grace indicate that this doctrine has not been very
important in his thinking, and the contradiction be-
tween Van Til’s common grace doctrine and postmil-
lennialism was a loose end that Rushdoony, in his
enormous output of books and essays, unfortunately
overlooked. Yet Kline, in his rush to condemn what

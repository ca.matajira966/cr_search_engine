YAN TILS VERSION OF COMMON GRACE 97

ten in their hearts. Without a true motive,
without a true purpose, they may still do that
which externally appears as acts of obedience to
God’s law. God continues to press his demands
upon man, and man is good “after a fashion”
just as he knows “after a fashion.”

What I want to point out is Van Til’s under-
standing of the possibility of unregenerate men’s ex-
ternal conformity to the external requirements of biblical
law. Unregenerate men can obey the law externally
“after a fashion.” This is a very important insight for
developing a proper understanding of common
grace. (Sadly, Van Til failed to develop it.) They do
what is right for the wrong motive. But any right ex-
ternal action counts for something, temporally and
eternally. Better to be an Albert Schweitzer, on earth
or in hell, than an Adolph Hitler.

Adherence to biblical law brings external re-
wards, including legitimate temporal power (Deut.
28:7, 13). To match the God-ordained legitimate
power of covenantally faithful Christians, the unregener-
ate must conform their actions externally to the law
of God as preached by Christians, the work of which
they already have in their hearts. The unregenerate
are therefore made far more responsible before God,
simply because they have more knowledge. They de-
sire power. Christians will some day possess cul-
tural, economic, and political power through their

Mt. Van Til, An Introduction to Systematic Theology, Vol. V of In
Defense of the Faith, p. 105.

108 DOMINION AND COMMON GRACE

5. Only Christians can act increasingly self
consistent with their epistemological presupposi-
tions and still increase in the blessings of God.

6. Evil men will become (act) even more
evil. (Here I agree with Van Til.)

7. To exercise maximum evil, they must
act to some extent consistently with the Bible’s
view of man, time, and law.

8. The favor of God will not be withdrawn
from them over time. They never had any
favor after Adam’s fall. The favor of God has
nothing to do with their situation.

9. They will increase in power only when
they act in conformity to much of external bib-
lical law (the terms of the covenant).

10. They will attempt to use this power to
iad Christians. (Here I agree with Van
Til.

11. As in the case of the Pharaoh of the ex-
odus and Sodom, this will bring them under
the visible judgment of God.

12. Christians will therefore not come under
progressive judgment by the reprobate; pagan
rule will be cut short a “millennium” (a long
period of time) before the final judgment.

13. At the end of time, Satan will act con-
sistently with his ethics, but using the power
God grants to him. He will therefore act incon-
sistently with his factual knowledge (as he did
when he moved men to crucify Jesus Christ).

14, He will try to destroy the church.

15. God will intervene at the end of time to
save His briefly threatened church.

ESCHATOLOGY AND BIBLICAL LAW 115,

Jaw-order, and He has been gracious in order to pile
the maximum possible quantity of hot coals on their
God-hating heads. [In contrast to Van Til’s amillen-
nialist vision of the future, we must say: when common
grace is extended (not reduced) to its maximum limits possible
in history, then the crack of doom has come—doom for the

rebels.

Van Til’s Dilemma

Van Til destroyed any remaining hope in natural
law or a common-ground philosophy. He took the
insights of Abraham Kuyper and Herman Bavinck
and extended these insights to their biblical and logi-
cal conclusion: the impossibility of any natural law com-
mon ground link between covenant-keepers and covenant-
breakers. But Van Til never adopted biblical law as an
alternative to the natural law systems that he so thor-
oughly destroyed. This always hampered the devel-
opment of his own philosophy, for the older Re-
formed view of the moral law was based squarely on
the natural-law concepts Van Til had destroyed. He
was unwilling to challenge the older Reformed
creeds on this point. His ideas have made creedal re-
visions mandatory, but he was unwilling to call pub-
licly for a revision of the creeds leading to more bibli-
cally precise definitions of such seventeenth-century
concepts as “general equity”* “moral law,”” and “the
covenant of works,”8

6. Westminster Confession of Faith, XIX:4.
7. Westminster Confession of Faith, XIX:5.
8. Westminster Confession of Faith, XIX:6.

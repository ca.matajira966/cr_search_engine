138 DOMINION AND COMMON GAACE

time. Biblical law, even when empowered by the
Holy Spirit, is culturally impotent.

Kline vs. Bahnsen

If you preach that biblical law produces “positive
feedback,” both personally and culturally —that God
rewards covenant-keepers and punishes covenant-
breakers in history—then you are preaching a sys-
tem of positive growth. You are preaching the pro-
gressive fulfillment dominion covenant. Only if you
deny that there is any long-term sustainable rela-
tionship between external covenant-keeping and ex-
ternal success in life~-a denial made explicit by
Meredith G. Kline*— can you escape from the post-
millennial implications of biblical law.

This is why it is odd that Greg Bahnsen insists on
presenting his defense of biblical law apart from his
well-known postmillennialism. “What these studies

26. Meredith Kline says that any connection between bless-
ings and covenant-keeping is, humanly speaking, random, “And
meanwhile it [the common grace order] must run its course with-
in the uncertainties of the mutually conditioning principles of
common grace and common curse, prosperity and adversity be-
ing experienced in a manner largely unpredictable because of
the inscrutable sovereignty of the divine will that dispenses them
in mysterious ways.” Kline, “Comments on the Old-New Error”
Westminster Theological Journal, XLI (Fall 1978), p. 84. Dr. Kline
has obviously never considered just why it is that life insurance
premiums and health insurance premiums are cheaper in
Christian-influenced societies than in pagan societies. Appar-
ently, the blessings of long life that are promised in the Bible are
sufficiently non-random and “scrutable” that statisticians who
advise insurance companies can detect statistically relevant
differences between societies.

ESCHATOLOGY AND BIBLICALLAW 149

to be a time after that point when there is even less
room for indecision and choices are even more mo-
mentous. Good is always getting better and bad is
always getting worse: the possibilities of even appar-
ent neutrality are always diminishing. The whole
thing is sorting itself out all the time, coming to a
point, getting sharper and harder.”#

The problem with Lewis’ outlook is that he never
suggested any way that Christians could make these
moral decisions in the public realm. He told us of the
war, told us that we would not be able to escape our
responsibilities, told us that our decisions would be-
come ever-clearer, and yet refused to offer any hope
that the public issues of any era could be solved by
an appeal to the Bible. Indeed, he specifically re-
jected such a suggestion.

He dismissed as unrealizable the creation of any
distinct or distinctly Christian political party—a
long-time ideal of many Dutch Christians. Chris-
tians do not agree on the means of attaining the
proper goals of society, he argued. A Christian
political party will wind up in a deadlock, or else the
winning faction will force all rivals out. Then it will
no longer be representative of Christians in society.
So this minority party will attach itself to the nearest
non-Christian political party.

The problem as Lewis saw it is that the party will
speak for Christendom, but will not in fact represent
all of Christendom. “By the mere act of calling itself

33. C. 8. Lewis, That Hideous Strength: A Modern Fairy-Tale for
Grown-Ups (New York: Macmillan, [1946] 1979), p. 283.

ESCHATOLOGY AND BIBLICALLAW 155

learned in Warsaw, this strategy has distinct limits.
So the theonomists call men to pick up God’s
weapon, biblical law, to carry with them when they
bring the gospel to the lost. There can be no more
excuses for cultural impotence. Christians have the
tool of dominion. It will do no good to say that
Christians cannot win in history, for we have the
weapons to win. Any excuse now is simply an un-
willingness to join the battle. But as in the days of
Deborah, there are many who choose not to fight.
And some day, some future Deborah will sing a
modern version of: “Gilead abode beyond Jordan:
and why did Dan remain in ships? Asher continued
on the sea shore, and abode in his breaches [inlets]”
(Jud. 5:17).

If progress is seen as exclusively internal, or at
most ecclesiastical, as it is in all forms of amillennial-
ism, then history inescapably becomes antinomian.
Biblical law must be abandoned. Biblical law in New
Testament times does noi permit long-term failure. Biblical
law necessarily must lead to positive visible results,
which in turn should reinforce faithfulness, as well
as serve as a light to the unconverted (Deut. 4:6-8), a
city on a hill (Matt. 5:14). Amillennialism implicitly
denies that a biblical city on a hill will be built.
There will only be congregations in the catacombs,
groups in the Gulag. Van Til makes this plain. Once
more, I cite his uncompromising analysis:

But when all the reprobate are epistemo-
logically selfconscious, the crack of doom has
come. The fully self-conscious reprobate will

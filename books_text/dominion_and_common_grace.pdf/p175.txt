6

SUSTAINING COMMON GRACE

Beware that thou forget not the LORD thy God, in
keeping his commandments, and his judements, and
his statutes, which I command thee this day... . And

, thou say in thine hearl, My power and the might of
mine hand hath gotten me this wealth... . And it
shall be, if thou do at all forget the Lorp thy God, and
walk after other gods, and serve them, and worship
them, 1 testify against you this day that you shall surely
perish (Deut. 8:11, 17, 19).

Here is the paradox of Deuteronomy 8: the bless-
ings of God can lead to the cursings of God. God’s
gifts can also lead to arrogance and the temptation to
think of oneself as autonomous. Autonomy leads to
false worship. False worship leads to destruction.
‘Therefore, what appears to be a good thing, wealth,
can become a snare and a delusion. A person's or so-
ciety’s preliminary external obedience to biblical law
produces benefits that in turn lead to the destruction
of that individual or people who were only in exter-

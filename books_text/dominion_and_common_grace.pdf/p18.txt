2 DOMINION AND COMMON GRACE

thought, the issue of “continuity vs. discontinuity.”

The discontinuity in this passage is the final
judgment. Will the owner of the field (God) allow
the servants (angels) to tear out the tares (evil men)
before the harvest date (the end of time)? The an-
swer is no. The owner insists that the tares be left
alone until both wheat and tares have fully matured,
and the harvest day has come.

God's plan for history involves both continuity
and discontinuity. His continuity is His grace. “The
Lord is gracious, and full of compassion; slow to
anger, and of great mercy” (Ps, 145:8). The phrase
slow to anger is crucial. Eventually, He brings judg-
ment, but only after time passes. But judgment
eventually comes to the wicked: “The Lord preserv-
eth all them that love him: but all the wicked will he
destroy” (Ps. 145:20). God announced the following
to Moses, after Moses had completed his task of
carving the ten commandments into two stones:

And the Lord passed by before him, and
proclaimed, The Lord God, merciful and gra-
cious, longsuffering, and abundant in goodness
and truth, keeping mercy for thousands, for-
giving iniquity and transgression and sin, and
that will by no means clear the guilty; visiting
the iniquity of the fathers upon the children,
and upon the children’s children, unto the third
and to the fourth generation (Ex. 34:5-7).

The Lord suffers long; in this case, three or four
generations, This is exactly what God had told

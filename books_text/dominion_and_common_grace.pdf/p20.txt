4 DOMINION AND COMMON GRACE

nomic doctrines and economic experiments. The
concept goes back at least to John Calvin’s writings.*

Before venturing into the forest of theological
debate, let me state what I believe is the meaning of
the word “grace.” The Bible uses the idea in several
ways, but the central meaning of grace is this: a gift
given to God’s creatures on the basis, first, of His
favor to His Son, Jesus Christ, the incarnation of the
second person of the Trinity, and second, on the
basis of Christ's atoning work on the cross. Grace is
not strictly unmerited, for Christ merits every gift,
but in terms of the merit of the creation— merit de-
served by a creature because of its mere creature-
hood—there is none. In short, when we speak of any
aspect of the creation, other than the incarnate Jesus
Christ, grace is defined as an unmerited gift. The
essence of grace is conveyed in James 1:17: “Every
good gift and every perfect gift is from above, and
cometh down from the Father of lights, with whom is
no variableness, neither shadow of turning.”

Special grace is the phrase used by theologians to
describe the gift of eternal salvation. Paul writes:
“For by grace are ye saved through faith; and that
not of yourselves: it is the gift of Gad: Not of works,
lest any man should boast” (Eph. 2:8-9). He also
writes: “But God commendeth his love toward us, in
that, while we were yet sinners, Christ died for us”
(Rom. 5:8). God selects those on whom He will have
mercy (Rom. 9:18). He has chosen these people to be

1. John Calvin, Zastitutes of the Christign Religion (1559), Book
IL, Chapter IL, sections 13-17; U:111:3; I1:X1V:2.

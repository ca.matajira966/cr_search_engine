204 DOMINION AND COMMON GRACE

autonomous (humanist) man still left in his thinking.
If any variation of the forbidden argument appears
in a person’s mind —“Why doth he yet find fault? For
who hath resisted his will?”—then he still is thinking
humanistically. If he clings to the doctrine of free will
because it is a logical corollary of personal responsi-
bility, then he is still thinking rebelliously. He has
substituted humanism’s logic for the express teach-
ing of Romans 9, Paul makes it clear: (1) complete,
eternal responsibility is inescapable, and (2) God
predestines some to be vessels of wrath and others to
be vessels of His grace. We must affirm both doc-
trines, not because of their logic or lack of logic, but
because of God's explicit revelation in Romans 9. To
reject Paul's conclusion is to reject a portion of His
inspired Word, which is the essence of humanism
and rebellion. It is Adam’s sin.

Did you pass the test?

Few Christians do. This is why the doctrine of
common grace has gone on in Calvinist circles and
not in anti-Calvinist circles. Those who are not Cal-
vinists do not believe in God’s double predestination,
meaning the equal ultimacy of wrath and grace. (For that
matter, some Calvinists don’t accept it either, which
has affected some of the debates over common
grace.) The question arises: How does God view
those who are not predestined to eternal life? Does
He regard them with some degree of favor, or none,
during their earthly lives? Do they as “creatures as
such” or “men as such” become the recipients of his
love or favor, “after a fashion”? Is the unregenerate
vessel of wrath in some way the object of God’s favor

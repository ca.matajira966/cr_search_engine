FILLED VESSELS 205

to “clay in general”? The Synod of 1924 said yes.
Hoeksema said no. Hoeksema was correct.

Two Kinds of Love

The theological confusion arises because of the
conventional definition of love, which is defined as
favor or the emotional attachment of one person to
another. This is not how the Bible defines love. Love
in the Bible comes in two forms, depending on
whether a person is a vessel of blessing or a vessel of
wrath. There is love with attachment and love with-
out attachment. The first is positive in its emotive at-
tachment and also judicial; the second is negative in
its emotional detachment, and also judicial. The first
involves continuity (inheritance by God’s adoption);
the second involves discontinuity (disinheritance by
God’s wrath).

For those in God’s covenant, love has all five cat-
egories of the covenant in the form of blessing: (1)
the presence of God the transcendent; (2) hierarchy
(a place in God’s church); (3) the law of God (law
written in the heart); (4) the judgment of God (justi-
fication as God’s forensic declaration of their right-
eousness}; and (5) inheritance (as adopted sons}.
Those outside the covenant also have all five points,
but in the form of wrath: (1) presence of God as ac-
cuser, even in hell (Ps. 139:8); (2) hierarchy (God is
sovereign over them); (3) the law of God (the work of
the law in the heart); (4) the judgment of God (God’s
forensic declaration of their guilt); (5) inheritance
(from earthly wrath to eternal wrath).

Thus, when God tells us to love our fellow man,

FILLED VESSELS 209

edge that was passed on through Noah's family.
Their external wealth was laid up for destruction, a
testimony to all men throughout history concerning
the final judgment to come. The flood was as close to
ending history as God ever came. He promised never
to do it again, until the day of judgment. The rain-
bow is His covenant sign of this promise (Gen. 9:17).

The Canaanites

This was a culture so perverse that God in-
structed Joshua to destroy all of them, or at least
chase them forever out of the land. God was so
serious about this that He said that if they refused to
destroy them, He would depart from them (remove
His presence: Josh. 7:12). Every man, woman, and
child of Jericho was killed, except for Rahab's cove-
nanted household.

Samuel later told Saul to destroy the Amalekites,
“and utterly destroy all that they have, and spare
them not; but slay both man and woman, infant and
suckling, ox and sheep, camel and ass” (I Sam, 15:3).
But Saul was greedy—he kept the animals for Israel
—and lenient to a “fellow ruler,” king Agag. For this,
God removed the kingship from Saul (15:11). To em-
phasize the point, Samuel hacked King Agag to
pieces (15:33). Agag was a murderer of women and
children, Sarnuel reminded him; so will his mother
be childless. This was also a reminder to Saul that
Saul had broken the terms of the covenant; this is
what God does to those who break it. This is what
“cutting the covenant” means (Gen. 15:9-17).

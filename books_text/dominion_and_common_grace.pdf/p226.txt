210 DOMINION AND COMMON GRACE

The Canaanites had received the testimony of
Abraham and Isaac. The Philistines’ response was to
fill up Abraham’s water wells with dirt, in a display
of envy (Gen. 26:14-15). The Canaanites later came
under judgment during the famine that drove Jacob
and his family down to Egypt. Then they saw that
Jacob’s son had become ruler of Egypt and the source
of bread for Canaan. Still they did not repent.

Then came the exodus. For a generation, Israel
wandered in the wilderness. Canaan grew richer, yet
the people of Canaan did not repent. They built
houses and planted vineyards, but they would not
inherit. God was making an inheritance for His peo-
ple—“houses full of all good things, which thou fill-
edst not, and wells digged, which thou diggest not,
vineyards and olive trees, which thou plantedst not”
(Deut. 6:11).

God’s common grace was heaping coals of fire on
their heads. They continued in their sins, filling up
the iniquity of the Amorites. God was also making
an inheritance for His people. The wealth of the
wicked is laid up for the righteous.

This is why there can be an increase in God's
common grace in response to evil man’s increasingly
evil ways. It is a means of testifying to them of a
coming judgment which will be historically total.
Their external blessings make them worth destroy-
ing. God is going to remove their inheritance and
give it to His people. He fills up their vessels with
blessings because He is about to break them as
vessels of wrath, and pour the wealth into the vessels
of honor,

8 DOMINION AND COMMON GRACE

The serious Christian eventually will be faced
with the problem of explaining the good once he
faces the biblical doctrine of evil. James 1:17 informs
us that all good gifts are from God. The same point is
made in Deuteronomy 8:18. It is clear that the unre-
generate are the beneficiaries of God’s gifts. None of
the participants to the debate denies the existence of
the gifts. What is denied by the Protestant Reformed
critics is that these gifts imply the favor of God as far as
the unregenerate are concerned. They categorically
deny the first point of the original three points.

For the moment, let us refrain from using the
word grace. Instead, let us limit ourselves to the word
gift. The existence of gifts from God raises a whole
series of questions:

Does a gift from God imply His favor?
Does an unregenerate man possess the
power to do good?

Does the existence of good behavior on the
part of the unbeliever deny the doctrine of total
depravity?

Does history reveal a progressive separation
between saved and lost?

Would such a separation necessarily lead to
the triumph of the unregenerate?

Is there a common ground intellectually be-
tween Christians and non-Christians?

Can Christians and non-Christians cooper-
ate successfully in certain areas?

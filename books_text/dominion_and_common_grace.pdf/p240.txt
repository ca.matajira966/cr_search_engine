224 DOMINION AND COMMON GRACE

welcome speech to King Louis XVI and Marie An-
toinette. At Paris, he studied at Louis-le-Grand, a
college of the University of Paris. He spent much of
his time reading Enlightenment literature. He even
visited Rousseau once. The unsuspecting priests
awarded him a special donation of 600 livres upon
graduation.® He was their star student scholar of the
classics.

The Communist Counterfeit

Consider the phenomenon of Communism. Karl
Marx and Frederick Engels had both been fervent
Christians in their teens.” Marx was baptized at age
six in 1824 and confirmed a decade later.® At age six-
teen he wrote an essay, “On the Union of the Faith-
ful with Christ... .” In it, he affirmed: “. . . the
history of peoples teaches us the necessity of our
union with Christ.” “And where is there expressed
more clearly this necessity for union with Christ than
in the beautiful parable of the Vine and the Branches,
where He calls Himself the Vine and calls us the
Branches.” “Who would not willingly endure sorrows
when he knows that through his continuing in
Christ, through his works God himself is exalted,
and his own fulfillment raises up the Lord of Crea-
tion? (John XV, 8).”9

6. Otto Scott, Rebespierre: The Voice of Virtue (New York:
Mason & Lipscomb, 1974), pp. 18-19.

7. Richard Wurmbrand, Marx and Satan (Westchester, Ili-
nois: Crossway, 1986), chaps. 1, 3.

8. Robert Payne {ed.), The Unknown Karl Mare (New York:
New York University Press, 1971), p. 33.

9. Tbid., pp. 40, 41, 43.

240 DOMINION AND COMMON GRACE

His rivals head (Gen. 3:15) at Calvary? Admittedly,
the church suffers from a limp, just as Jacob did
(Gen, 32:25). The church’s heel is injured, just as
God promised that Christ’s would be (Gen. 3:15). But
the enemy’s head is crushed. When going into battle,
which wound would you prefer to march in with?

Unbelievers appear to be culturally dominant to-
day. Christians have for too long seen themselves as
the dogs sitting beneath the humanists’ tables, hop-
ing for an occasional scrap of unenriched white
bread to fall their way. They have begged human-
istic college accreditation associations to certify the
academic acceptability of their struggling little col-
leges. They worry about their own competence.
They think of themselves as second-class citizens.

And the humanists, having spotted this self-
imposed “second-class citizen” mentality, have taken
advantage of it. They have sent Christians to the
back of the bus.

Pietism’s Retreat

Believers have for over a century retreated into
antinomian pietism and pessimism. This retreat
began in the 1870’s.! They have lost the vision of vic-
tory which once motivated Christians to evangelize
and then take over the Roman Empire. They have
abandoned faith in one or more of the four features
of Christian social philosophy that make progress
possible: (1) the dynamic of eschatological optimism, (2)

1. George Marsden, Fundamentalism and American Culture: The
Shaping of Twentieth-Century Evangelicalism, 1870-1925 (New York:
Oxford University Press, 1980).

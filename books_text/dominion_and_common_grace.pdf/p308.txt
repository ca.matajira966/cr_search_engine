292 DOMINION AND COMMON GRACE

overnight judgment, 83-84,
161, 165, 167, 208
progress since, 101
special grace to, 161, 208
Noll, Mark, 264-65

obedience, 122, 237
occultism, 183
optimism, 113, 231
original sin, 20

Oulanem (Marx), 225-26

peace, 69, 70, 92, 170
perfection, 61, 248
pessimism, 238, 240
Pharaoh, 17, 168, 211, 214
pietism, 240-41
Plato, 148
politics, 70, 149-50
population, 212
positive feedback
amillennialism vs., 138, 146
biblical law &, 99, 122, 134,
142, 146
covenantal blessings, 32, 112
dominion &, 135, 138, 146
God's glory manifested, 30-31
growth, 138
Holy Spirit's empowering,
122, 142
New Heavens, 30-31
postmillennialism &, 139-40,
142
premillennialism vs., 146-47,
154
special grace &, 161-62
(sce also: Scripture Index,
Deut. 8:18)
postmillennialism
Bahnsen vs. Kline, 138-43

biblical law & dominion, 122,
139
common grace &, 114-15,
170-71
cultural impotence vs,, 155
Edwards’ pietism, 175
external covenants, 114
final rebellion &, x, 114, 170,
242-43
Great Awakening, 175
Holy Spirit’s empowering,
140-41
Rapture, 68
world conversion?, xiv-xv,
242-43
potter 201-2, 203
power
biblica) law brings, 97-98, 114
Christians &, 97-98, 238
covenant-breakers, 105, 114,
117, 129, 131, 250
ethics &, xi, 33, 105
prelude to judgment, 208
religion of, 131, 238
Satan’s army, 114, 134
Van Til’s view, 117
{see also: chaos, consistency)
predestination, 203, 204, 228
premitlennialism, 74-75, 146-47,
154
Presbyterian Church, 255-56
presuppositionalism, 228
Princeton Seminary, 121
apologetics, 261-66
creedal, 257
eschatology, 268-70
family enterprise, 256
Princeton University, 10n
prison analogy, 215
progress, 101, 155, 176, 227

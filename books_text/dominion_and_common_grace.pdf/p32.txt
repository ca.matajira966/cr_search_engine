The righteousness of the perfect shall direct
his way: but the wicked shall fall by his own
wickedness.

The righteousness of the upright shall deliver
them: but transgressors shall be taken in their
own naughtiness.

When a wicked man dieth, his expectation

shall perish: and the hope of unjust men
perisheth.

The righteous is ‘delivered out of trouble,
and the wicked cometh in his stead.

Prov. 11:5-8

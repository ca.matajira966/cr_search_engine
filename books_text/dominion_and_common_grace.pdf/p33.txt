1

THE FAVOR OF GOD

Do nat I hate them, O Lorb, that hate thee? And
am I not grieved with those that rise up against thee? I
hate them with a perfect hatred: I count them mine ene-
mies (Ps. 139:21-22).

For the scripture saith unto Pharaoh, Even for this
same purpose have I raised thee up, that I might shew
my power in thee, and that my name might be declared
throughout all the earth. Therefore hath he mercy on
whom he will have mercy, and whom he will he hard-
eneth (Rom. 9-17-18).

“Perfect love casteth out fear” (I John 4:18). If
David's encounter with Goliath is evidence, so does
perfect hatred against God’s enemies. David was the
greatest warrior in Israel’s history; I would argue
that this was to a large degree because he hated
God's enemies with a perfect hatred. The perfect
love of God necessarily involves the perfect hatred of
God’s enemies.

Van Til has argued that men are to think God’s

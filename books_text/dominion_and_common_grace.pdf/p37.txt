THEFAVOROF GOD 21

qualification, “Concerning the favorable attitude of
God toward mankind in general and not only toward
the elect, the Synod declares that according to Scrip-
ture and the Confession it is certain that, besides the
saving grace of God bestowed only upon those chosen
to eternal life, there is also a certain favor or grace of
God manifested to His creatures in general. . . .”! 1
assume that creatures in general means, basically, crea-
tures in general. If the Synod had wanted to exclude
Satan and his demonic host, it had that opportunity
in 1924, when its actions led to the splitting of the
church and the exodus of a large portion of its more
theologically conservative members. The fact that it
refused to exclude Satan has created some real prob-
lems for Van Til, and it has placed in the hands of
the Protestant Reformed Church a large-caliber
theological gun.

argue throughout this book that there can be no
favor shown to “creatures in general,” since “crea-
tures in general” includes Satan. The historical and
eternal problem facing Satan is that his status as an
angel no longer protects him from God's wrath and
perfect hatred; he is in sin, which makes him a fallen
angel. Similarly, the historical and eternal problem
facing ethical rebels is that their status as men made
in God’s image no longer protects them from God’s
wrath and perfect hatred; they are in sin, which
makes them reprobates. God therefore shows no
favor to them, any more than He shows favor to

1. R. B. Kuiper, 7 Be or Not to Be Reformed: Whither the Chris-
tian Reformed Church? (Grand: Rapids, Michigan: Zondervan,
1959), p. 105.

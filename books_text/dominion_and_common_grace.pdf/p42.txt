26 DOMINION AND COMMON GRACE

volves biblical law. What Jesus was saying was that
His people must deal with unbelievers in terms of
biblical law, just as God deals with them. Love means
the fulfilling of the law toward all men (Rom. 13:8).

It is understandable how such verses, in the ab-
sence of other verses that more fully explain the
nature and intent of God’s gifts, could lead men to
equate God’s favor and gifts. Certainly it is true that
God protects, heals, rewards, and cares for the unre-
generate. But none of these verses indicates an attitude of
favor toward the unregenerate beneficiaries of His gifts. The
attitude of favor is simply assumed by Van Til and the
Synod of 1924, Only in the use of the word “favor” in
its English slang form of “do me a favor” can we
argue that a gift from God is the same as His favor.
Favor, in the slang usage, simply means giff—an un-
merited gift from the donor. But if favor is under-
stood as an attitude favorable to the unregenerate, or
an emotional commitment by God to the unregener-
ate for their sakes, then it must be said, God shows
no favor to the unrighteous.

Coals of Fire
One verse in the Bible, above all others, informs
us of the underlying attitude of God toward those
who rebel against Him after having received His
gifts. This passage is the concomitant to the oft-
quoted Luke 6:35-36 and Matthew 5:44-45. It is
Proverbs 25:21-22, which Paul cites in Romans 12:20:

6. Ray Sutton, That You May Prosper: Dominion By Covenant (Ft.
Worth: Dominion Press, 1986). Sutton shows that the third
aspect of the covenant is law, meaning ethics.

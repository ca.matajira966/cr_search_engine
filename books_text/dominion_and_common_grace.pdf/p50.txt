34 DOMINION AND COMMON GRACE

Christians will have come so close to self-consistent
living that rebels cannot stand to live close to them.
So God will at last separate the wheat from the tares.
This is the central theme of this book.

Favor to Satan?

The idea that God shows favor to ethical rebels
eventually comes into conflict with the idea of God’s
gifts to Satan, which are said by the defenders of the
1924 Synod not to involve God's favor. They have ta
admit that God gives Satan gifts. There is no ques-
tion that the grace of God in extending temporal life
to mankind after the rebellion of Adam also involved
extending time to Satan and his angels. There is no
question that the grace of God in extending all other
temporal blessings to mankind after the rebellion of
Adam—knowledge, law, power, peace, etc.—also
involved extending these same blessings to Satan
and his angels. If nothing else, Satan is empowered
by God to establish satanic covenants with his fol-
lowers, covenants that bear four of the five marks of
God’s covenant.”

All of life is covenantal. The battles of life are
therefore covenantal. Anything that happens to
those who are under God's covenant affects the

7. The five aspects of God’s covenant, as Sutton shows in That
You May Prosper, are these: (1) transcendence/immanence (the
presence of God), (2) hierarchy/authority (organization), (3)
law/ethics (dominion), (4) judicial (evaluational), and (5) inher-
itance (continuity). Satan’s covenants lack only the first, for he is
not divine and omnipresent. He compensates for this lack by
creating a centralized top-down hierarchy in place of God's
bottom-up appeals court covenantal structure.

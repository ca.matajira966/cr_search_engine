THEFAVOROFGOD 39

their lost condition.”"° Men are men eternally.

Common grace, and therefore God's supposed
common favor toward mankind in general, is exclu-
sively an aspect of history. “When history is finished
God no longer has any kind of favor toward the repro-
bate. They still exist and God has pleasure in their ex-
istence, but not in the fact of their bare existence. God
has pleasure in their historically defeated existence.”"

So it cannot be the fact that Satan is not made in
God’s image that disqualifies him from the favor of
God. Reprobate men in eternity are disqualified,
too. The issue is historical, not the image of God in
man. So what is it in Satan that disqualifies him
from God’s supposed favor in general? Why is the
creation in general, including Satan, not the recipi-
ent of God's favor?

Van Til must distinguish favor to mankind in gen-
eral in history from favor to creatures in general in his-
tory, if he is to preserve his distinction between favor
to mankind and no favor to Satan. Could it be that
Satan is totally evil now, and that his evil does not
develop in history? Is his evil in principle identical to
his evil at every point in time? Is he in this sense
non-historical, and therefore not the object of God's
favor?

Satan and History
Van Til writes little about Satan, but in the few
pages of his Introduction to Systematic Theology in which

10. A Reply to Criticism, in ibid., p. 198.
11. Common Grace, in ibid., p. 30.

50 DOMINION AND COMMON GRACE

know also that all things work together for evil for
them that hate God, to them who are the rejected ac-
cording to His purpose (Rom. 9:17-22). Common
grace—common curse, special grace—special curse:
we must affirm all four.

The Transgression of Biblical Law

The transgression of biblical law brings a special
curse to the unregenerate. It is a curse of eternal dur-
ation. But this same transgression brings only a com-
mon curse to God’s people. A Christian gets sick, he
suffers losses, he is blown about by the storm, he
suffers sorrow, but he does not suffer the second
death (Rev. 2:11; 20:6, 14). Perhaps his nation suffers
a plague or a military defeat because of the sinful-
ness of most of his neighbors and his nation’s rulers.
For the believer, the common curses of life are God’s
chastening, signs of God’s favor (Heb. 12:6).

The difference between common curse and spe-
cial curse is not found in the intensity of human pain
or the extent of any loss; the difference lies in Gad’s
attitude toward those who are laboring under the ex-
ternal and psychological burdens. There is an atti-
tude of favor toward God’s people, but none toward
the unregenerate. The common curse of the unregenerate
person is, in fact, a part of the special curse under
which he will labor forever. The common curse of the re-
generate person is a part of the special grace in terms of
which he finally prospers.

The common curse is nonetheless common,
despite its differing effects on the eternal state of
men. The law of God is sure. God does not respect
persons (Rom. 2:11), with one exception: the Person

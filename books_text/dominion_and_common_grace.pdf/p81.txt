3

WHEAT AND TARES:
HISTORICAL CONTINUITY

Another parable put he forth unto them, saying,
The kingdom of heaven is likened unto a man which
sowed good seed in his field. But while.men slept, his
enemy came and sowed tares among the wheat, and
went his way. But when the blade was sprung up, and
brought forth fruit, then appeared the tares also. So the
servants of the householder came and said unto him,
Str, didst thou not sow good seed in thy field? From
whence then hath it tares? He said unto them, An
enemy hath done this. The servants said unio him,
Wilt thou then that we go and gather them up? But he
said, Nay, lest while ye gather up the tares, ye root up
also the wheat with them. Let both grow together until
the harvest: and in the time of harvest I will say to the
reapers, Gather ye together first the tares, and bind
them in bundles to burn them: but gather the wheat into
my barn (Mait. 13:24-30).

The parable of the wheat and tares is instructive
in dealing with the question: Does history reveal a
progressive separation between the saved and the lost?

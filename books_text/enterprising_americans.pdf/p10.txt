xl PUBLISHER’S FOREWORD

ades by federal loan guarantees— have cast a darkening shadow
over public optimism regarding business. It may be, as political ana-
lyst Kevin Phillips is saying in 1990, that an anti-business political
reaction is getting ready to hit, especially if the U.S. economy
stumbles into recession, inflation, or both. (Ironically, in late 1989,
Eastern Europe and even segments of the Soviet Union rushed to
throw off the chains of Communism and join the enterprising West.)

We have seen similar shifts in public opinion in the past. In the
early nineteenth century, the attitude of most Americans toward
business and enterprise was overwhelmingly positive. America was
seen as a revolutionary, historically unprecedented land of oppor-
tunity. Reality actually matched imagination; America remained
just such a land throughout the nineteenth century, as is testified to
by the millions of immigrants who sailed into Boston and New York
harbor after 1847, followed by the flood that steamed in after the
Civil War.

A Shift of Opinion

Their optimism was not shared by the intellectuals. In the late
nineteenth century, the attitude of American intellectuals became
much more hostile to business. A series of economic depressions,
beginning in the mid-1870’s and continuing until the late 1890's,
brought sporadic financial and emotional misery to millions of
workers, despite the fact that national economic output nearly
quadrupled and output per capita nearly doubled, raising the standard
of living of most Americans. The disorienting shocks of temporary
unemployment and the threat of bankruptcy were what people wor-
tied about, deflecting their attention from the stead y increase of na-
tional and personal wealth for three decades, 1867-1897.

The visible excesses of the rich — the rich have, throughout man’s
history, exercised both their extraordinary bad taste and their finan-
cial capacity to indulge it — contrasted sharply with the daily life of
the average man. So, Mark Twain’s title for his novel, The Gilded
Age, stuck to the 1870’s and 1880's, at least in the memories of sub-
sequent historians. But the critics, then and now, have conveniently
forgotten the obvious: it was the blessing of political freedom and
the resultant economic growth that made possible the cheap mass-
circulation novels, tracts, and tabloid newspapers that have called

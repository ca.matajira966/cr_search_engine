EARLY AMERICA GOES PLACES - 75

 

Courtesy Cooper Union for the Advancement of Science and Art

Peter Cooper

$17,000 per mile, they had thirteen miles of track laid for horse or
even sail-car operation.

At this point Peter Cooper, a New York enterprise, merchant,
and philanthropist, came up with a better idea. Working in the
B. & ©.’s shops, he managed in 1829 to build a small engine, the

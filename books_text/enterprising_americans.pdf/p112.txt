FRONTIER AND FACTORY - 87
a conveyer system that was to dump a hundred million feet of
Penobscot logs for the Bradstreets of South Gardiner, Maine, into
a sluice leading to the Kennebec River, and a Gary Peavey, born
and raised to lumbering know-how on the Penobscot, ended his
crew-bossing days on Puget Sound after cutting timber in Penn-
sylvania, another big logging area, and fighting off the Indians who
molested his loggers in Minnesota.

Tn the fullness of time in the 1890s, the elder Robert La Follette,
the first Progressive governor of Wisconsin, was to make great
political capital assailing the timber barons of Wisconsin for skul-
duggery in exploiting the state’s public domain. But without loggers
such as Cadwallader C, Washburn of Maine, who himself became
a governor of Wisconsin, the prairie towns and the farm buildings
to the south of the lake states would never have been built; and
pioneers farther to the west would have gone on living in sod huts.
Logging money was to make the school system of Menomonie, in
northem Wisconsin, a showpiece for educators who visited it from
all over the nation. The loggers’ cry was “Let a little light into the
swamp!” Slashing a thousand and more miles of white pine with-
out thinking of a replacement, the lumbermen thought they were
making new lands available for farms. They could hardly foresee
that the acid soil of the cutover lake states was not destined to make
good farming country for anyone other than those frugal Northmen,
the Swedes and the Finns. To their own generations, lumber kings
like Charles Merrill of Maine, who bought his first western forest
land along the St. Clair River in Michigan in 1836, or like Charles
H. Hackley, who came to Muskegon in 1856 without money and
died in 1905 leaving $6 million to his adopted town for culture
(including oil paintings), were giants in the same earth that bred
the legend of Paul Bunyan’s blue ox Babe, whose footprints made
the Great Lakes.

Along with the peavey the lumber industry found other ingenious
ways to solve its problems. The log boom, a Maine invention
designed to channel river-borne timber into the pens of its rightful
owners, was still being used in river regions of Michigan in the
late 1880's and 1890's by the Tittabawassee Boom Co., a joint
concern backed by Saginaw and Bay City lumbermen. In the Far
West, lumber had to be dragged to mills over toteroads by “buli-

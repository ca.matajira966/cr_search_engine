6 The Pre-Civil War Speedup

A “madman” exploits ice and sawdust.

Charles Goodyear experiments with malodorous “gum elas-
tie.”

Sam Morse turns from his easel to electrical circuits, “
Farm boy Howe and Isaae Singer thread the sewing machine.
“Household science” flourishes on evaporated milk.

From blacksmith shop to rolling mills.

Te years before the Civil War were politically a time of portent,
filled with the alarums that heralded the gathering of the “irrepressi-
ble conflict.” Looking back on those years, one sees little besides
the turmoil of Southemers fighting Free-Soilers over Bleeding Kan-
sas or the rage of the Charleston fire-eaters as they rise to denounce
the Abolitionists. Manfully, the gaunt Abe Lincoln debates with
the “Little Giant,” Stephen A. Douglas; manfully, the great com-
promiser Dan'l Webster joins with Henry Clay in the futile effort
to patch things up and avoid a showdown; and just as manfully the
philosopher Ralph Waldo Emerson denounces the godlike Dan’l
as a trimmer. So the nation slides toward the abyss past the great
milestones of the Dred Scott decision and John Brown’s fanatic
attempt to steal government arms for a slave rebellion.

When people are living through a period of endemic crisis, how-
ever, the “big” problems often seem of less moment than the smaller
problems of daily existence. While the political thunderheads were
rolling up in that pre-Civil War period, business enterprise con-
100

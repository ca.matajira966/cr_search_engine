126 . THE ENTERPRISING AMERICANS

multiplied by displaced Border State “neutrals” and the immigrants
who thronged through New’ York City’s Castle Garden to take
advantage of wartime wages carried on a score of activities that
were sometimes only distantly related to the war. As we shall see,
the Pennsylvania oil industry really got its start in the war years.
In Michigan, the Saginaw valley became a great producer of salt
brine. The lure of the western mining camps was one of the greater
attractions—the Civil War years witnessed the prodigious develop-
ment of the Comstock silver and gold deposits in Nevada, where
George Hearst, the father of William Randolph Hearst, mined ore
that was worth $2,200 a ton. The territory of Colorado, which had
contained some 30,000 people in 1860, tripled in population by
1864 as prospectors and their camp followers poured in after the
tapping of the Gregory silver lode. In Idaho and Montana there
were great gold strikes which, after the notorious Plummer gang
of robbers had been cleaned out of the territory, helped compen-
sate for the decline of the California mining industry. In Utah,
where the disciplined Mormons stuck to such prosaic occupations
as farming, shopkeeping and carriage making, some unlooked-for
profits were made by supplying the miners by stagecoach lines run-
ning north to Montana and by cutting telegraph poles for the new
wires that were crisscrossing westem territory. Meanwhile, through-
out the entire war period, what amounted to a “continuous caravan”
of emigrants crossed the ferry at Omaha, Nebraska, in good
weather. Besides the hundreds of converts to Mormonism, the end-
Jess caravan included a vast and restless floating population that
wanted to get away from war on any excuse. Indeed, at one point
the wartime governor of Iowa had to forbid anyone leaving his
state until after the federal draft of troops had been completed.
Despite the distractions of the West and draft riots in New York
City, the Union managed to raise a decisive army. And in the matter
of armament the industrial power of the North also proved decisive
even though it commenced the struggle with only enough saltpeter
available to provide the powder for a few battles. The du Pent
powder mills along the Brandywine River in Delaware, which had
been started in Jefferson’s day, had been growing slowly, producing
black powder for guns and for mine blasting in California and
elsewhere. Despite the fact that it was in vulnerable territory (at

134 . THE ENTERPRISING AMERICANS

and $32,000 for inter-mountain stretches. In addition to the cash,
each railroad was to get large grants of land along the rights-of-
way. The four ex-storekeepers who had got in on the ground floor
of the Central Pacific, Collis P. Huntington, Leland Stanford,
Mark Hopkins, and Charles Crocker, quickly devised a means of
channeling much of the subsidy money into their own pockets,
setting up dummy construction companies that (in effect ) paid
themselves huge building fees at the taxpayers’ expense. The
proprietors of the Union Pacific were also on two sides of the
construction bargain, subletting their building contracts to their
own creature, the oddly named Crédit Mobilier of America, which
paid them some $50 million in profits.

The Crédit Mobilier has been defended as a useful buffer device
for building a road through howling wildemess that was populated
by fierce Indians who followed the buffalo. The price of iron rails .
was high in the sixties; wood for crossties had to be packed in
from eastern forest lands that were far away; and there was no way
of calculating gains in advance, no assurance that the railroad
could originate much freight. In the light of such uncertainty, the
new railroad entrepreneurs felt justified in limiting their risks.
Nevertheless, the bribery of Congressmen that figured in the crea-
tion of the Crédit Mobilier constituted skullduggery even in an age
that was inclined to overlook the corner-cutting “smart” men
considered to be an inevitable part of smart business. But the
skullduggery, which touched depths of rascality, was accompanied
by engineering feats that gave the West “men to match its moun-
tains.” Despite all the seaminess, splendor was there.

The man who had surveyed the route over the Califomia Sierra
for the Central Pacific and who had stirred the federal government
into the subsidizing action, Theodore (or “Crazy”) Judah, did
not live to see how well he had plotted the future; he died of
yellow fever contracted in Panama while going east on business.
But Charlie Crocker, a mountainous figure of a man who could
not endure office work, took over where Judah left off. When the
white laborers he hired deserted him to seek their fortunes in the
silver diggings of Nevada’s Comstock Lode, Crocker looked at
his servant, a frail but perdurable Chinese named Ah Ling. It
flashed across Crocker’s mind that maybe here was his answer

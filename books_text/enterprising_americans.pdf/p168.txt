THE GILDED AGE- 143

pre-Civil War order managed not only to survive the shift but to
dominate it. Quite in line with the ethics of his age, the New York
Central’s Cornelius Vanderbilt did not scruple to use even the
most outright trickery to get control of properties he wanted.
Legislators, to him, were holdup men who had to be bribed to
keep them from selling out to his opponents, who in most cases hap-

 

Courtesy the Library of Congress
Comelius Vanderbilt

pened to be Fisk, Drew and Gould, the pirates of the Erie Rail-
road “ring.” But the old Commodore was no vulture; and when
he owned something he worked relentlessly at its physical improve-
ment, provided, of course, that he intended to keep it.

Way back in 1833, when he was a young steamboat man, the
Commodore had been injured in a railroad accident in New Jersey.
He disliked railroads, and thought to have little to do with them.
At the age of sixty-nine, however, sensing that his beloved river
steamboats had seen their best days, the Commodore swallowed his
distaste for the Iron Horse and decided to become a railroad man.

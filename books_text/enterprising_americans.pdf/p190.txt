THE RISE OF THE MONEY POWER 165

essayed the role of a private Interstate Commerce Commission. In
all of these roles the great Pierpontifex Maximus symbolized the
new and rising “money power’ of Wall Street, which had grown out
of a few relatively small specialty banking houses such as Moses
Taylor’s City Bank, J. & W. Seligman & Co., and Kuhn, Loeb,
among others. In the beginning these houses had simply financed
imports such as sugar and copper or exports such as cotton and had
dealt in government bonds. But by the turn of the century they
were in just about everything. Seeking outlet for their funds and
energies, they had become bone and marrow of the scary “trustifi-
cation” of American business.

Well before the formation of U.S. Steel there were big trusts like
the American Sugar Refining Co. and the American Tobacco Co.,
with capitalizations running into the hundreds of millions. And
there were little trusts like those in the hide and leather or the
chicle industry, where mere millions seemed the co-ordinating fac-
tor. Some of these trusts had been created by promoters with a
legal knowledge of how to pyramid small companies into big ones
by an exchange of paper securities; and as paper-created pyramids
they represented no great initial dependence on the “money power.”
But “Wall Street’—meaning men like Morgan—was behind
enough of the new combinations to give many of the 1901 genera-
tion the creeps. Despite the fact that the Sherman Antitrust Act had
been passed in 1890, people thought they saw “restraint of trade”
sprouting up everywhere.

Fears for the future were further abetted by old political and
social enmities that refused to die, The farm-border Populists, who
had carried the banner for Free Silver in the Bryan campaign of
1896, had popularized the idea that Wall Street was oppressing
the hinterlands by nailing the country to a “cross of gold.” Ameri-
can labor, though somewhat mollified when President McKinley
promised the “full dinner pail,” nevertheless was still smarting from
its defeats in the Homestead strike of 1892 and the Pullman strike
of 1894, Many a businessman who had sought refuge from compe-
tition in “pools” such as the early whisky and cordage trusts was
disillusioned when price fixing broke down and sent marginal pro-
ducers to the wall. The grousing on all sides was magnified into
predictions of Red Revolution whenever labor violence occurred.

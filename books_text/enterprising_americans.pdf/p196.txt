THE RISE OF THE MONEY POWER ITI

for help in disposing of part of his New York Central stock, Mor-
gan could see an opportunity to get into the railroad picture with
both feet; but he insisted on naming his conditions.

At the time of Vanderbilt’s visit to Morgan, which came in 1879,
two years after the old Commodore’s death, there was a vast hue
and cry about “one-man rule” of the Central. Possessing 87 per
cent of the Central’s stock, and lacking his father’s stomach for a
fight, young William H. decided for prudential reasons to cut back
his ownership of the Central to a point where his interest would be
not quite equal to half the number of outstanding shares. Morgan
offered to dispose of 150,000 shares of the Central stock to over-
seas purchasers in private sales at $120 a share, with an option of
taking 100,000 more shares at the same price-a deal that proved
highly profitable. But in return for helping Vanderbilt out, Morgan
insisted that he be given a seat on the Central’s board as the holder
of proxies for English purchasers who trusted his judgment. This
marked Morgan’s emergence as a positive force in the railroad
field. It meant that henceforward there would be no tampering with
Central stock, no use of the railroad’s funds to forward the private
fortunes of insiders. It was also the first venture by an important
American investment banker into the sort of thing that has been
called “finance capitalism,” and significantly it was undertaken with
the long-term good of the property in mind. Old Cornelius Vander-
bilt, who would certainly have disapproved of his son’s timidity,
would have understood Morgan’s motives.

As the years passed Morgan threw his influence against what
seemed to him suicidal rate wars, and also sought to prevent the
railroads from needlessly duplicating facilities in order to black-
mail each other. In 1885, for instance, the Pennsylvania, seeking
to discommode the New York Central with something more than
a $1 “immigrant rate” to Chicago, started buying the bonds of the
half-bankrupt West Shore Railroad running on the west bank of
the Hudson from northern New Jersey to Albany. Worried lest the
Pennsylvania should steal its traffic from right under its nose, the
Central, in turn, started work on something known as the South
Pennsylvania Railroad, which was to go from the Susquehanna
River to Andrew Carnegie’s mill sidings in the Pittsburgh region.
None of this duplication made sense to Morgan, for it promised a

XX INTRODUCTION

role of the profit-seeking entrepreneur is obviously of critical im-
portance. For far from being some kind of surplus value extorted
from the worker, profit is the evanescent margin of return for the
tisk an enterprise takes when he brings factors of production to-
gether. In a static economy, where consumer taste and technology
are by definition frozen, there might be no mathematical room for
profit-or loss. But the American story has never been a static one;
from the days of Eli Whitney to those of Alfred P. Sloan there has
been a constant upending of equilibrium. In the process whole in-
dustries have gone to the wall—the canals, the early toll roads, the
carriage business, the traffic in whale oil. But always new ventures
have arisen to fulfill and to create new demands.

The manner in which creative busy-ness has worked its wonders
in America will be detailed as our business story progresses. When.
the early colonists arrived in the New World, and indeed at the
time when they formed the Republic, there was no Point Four Pro-
gram to help them. Yet the very shortages of men, money, and capi-
tal inspired a tremendous inventiveness. In 1810 we were still steal-
ing industrial secrets from England, which had an anti-Point Four
policy; but by 1850 the British were inviting gunmaker Samuel Colt
of Hartford to appear before a parliamentary committee in London
to explain the secrets of American production. By 1906 America
was already the world’s greatest producer.

It had all been a matter of a couple of generations wherein small-
business ventures proliferated into larger ones, and big business in
turn found itself faced with new competition. The Washburn &
Moen Co., wire makers of Worcester, Massachusetts, got a boost
when the crinoline dress trade began demanding wire stays. Forty
years later, as part of the American Steel& Wire Company, the old
Worcester concern was merged into the U.S. Steel Corp., the fright-
ening new behemoth conjured up by J. P. Morgan to do 66 per cent
of the steel business in America. In another forty years U.S. Steel
itself, challenged by the growth of Bethlehem, Republic, Jones &
Laughlin, and other new giants, had to solace itself with a mere 33
per cent of total steel output. And in 1962 it could no longer make
a price rise stick by mere say-so.

Monopolies-oil was the most notorious of them—waxed fat
only to recede into the pack, sometimes pursued by antitrust laws,

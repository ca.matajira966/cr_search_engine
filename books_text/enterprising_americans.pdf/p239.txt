214 . THE ENTERPRISING AMERICANS

what they lost in 1920-21 they soon recovered in 1922-24, when
the Model T sold better than ever. Indeed, in 1924 Ford production
ran to 1,600,000 units, representing a 51 per cent “penetration” of
the total U.S. car market.

This, however, was the high point of popularity for the famous
“Tin Lizzie,” and well before this the tide had subtly begun to
change. The Model T was the farmer’s friend because of its dura-
bility, and in a masculine world it reigned supreme. But by the
twenties-and even before—the American housewife had become
a powerful factor in car buying, demanding amenities that Ford
was reluctant to offer. Among the amenities was a self-starter,
which was ultimately to replace the hand crank on all cars. Ford
remained impervious to the general upgrading in quality, but other
manufacturers, notably General Motors, capitalized on the new
trend and eventually upset Ford’s hold on the family car market.

G.M. itself had its full share of early growing pains, partly be-
cause Durant, while bringing together Buick, Oldsmobile, and
Cadillac, had also cumbered it with many an unprofitable division
including a head-lamp company with a fraudulent patent. In 1910,
two years after he put the combination together, Durant himself
lost control of it to two investment banking companies, Lee, Hig-
ginson and Co. of Boston and J. and W. Seligman of New York,
because of a Buick bank debt of $7,000,000.

The bankers’ representative in G.M. was James J. Storrow, a man.
who happened to have an interest in technology. Storrow hired the
consulting firm of Arthur D. Little, Inc., of Cambridge, Mass., to
advise him—and the consulting company came up with the study
that resulted in the General Motors Research Department, which
was to pay off fabulously over the years. Though Storrow was
charged with carrying out the bankers’ extremely stiff financial
terms, his reorganization of the company was sufficiently canny to
justify even the most seemingly exorbitant of fees. He gave Charles
W. Nash his head as president of the company and Nash in turn
made Walter P. Chrysler-a railroad mechanic with mechanical
and electrical engineering knowledge learned from the Interna-
tional Correspondence Schools—the boss of Buick. Most important
in a time of shakedown and change, Storrow had the good sense to
continue Henry Leiand in power at Cadillac. More than that, he

F.0.8. DETROIT . 215

heeded Leland’s general advice. It was Leland who argued down
the bankers when they proposed to slough off most of the G.M.
divisions .and concentrate on saving Buick alone. Because of
Leland’s prestige, G.M.’s full divisional structure was maintained.

By then a white-bearded patriarch of impressive mien, Leland
had a bad conscience because a good friend, Byron T. Carter of
the Carter Car Co., had been killed in the attempt to crank a balky
car for a lady. This constituted a blot on the entire automobile busi-
ness, which Leland was resolved to wipe out. Accordingly, he gave
Charles F. Kettering of Dayton, Ohio, a commission to come up
with a practical electric self-starter. Kettering had designed small
electric motors for cash registers when in the employ of the National
Cash Register Co. of Dayton, and, using an old barn as headquar-
ters, he had recently formed the Dayton Engineering Laboratories
Co. When Kettering offered some blueprints of a small motor de-
signed to replace the hand crank, the G.M. directors summoned
experts from General Electric, Westinghouse, and the German elec-
trical trust of Siemens and Halske to inspect them. Unanimously the
experts informed the directors that Kettering’s device wouldn’t
work. But Leland persisted in spending money on Kettering’s ex-
periments. When the first Delco self-starter sparked a Cadillac
motor on February 27, 1911, Leland gave Kettering a contract for
four thousand self-starters even though the Dayton Engineering
Laboratories had no manufacturing facilities. “Ket,” as was to be
his habit, carried the assignment through, thus forming a tie with
G.M. that was to prove more and more fruitful in all departments
of engineering as the years went by.

But it was not just G.M.’s technical know-how that was to prove
decisive for its success. During the years of World War I it acquired
both the car and the strong financial backing that were to make it
the leader of the industry. Once more the story turns on the queer
and volatile character of Durant. Banished by the bankers from
the management of G. M., Durant nevertheless held on to his stock,
and, equally important, acquired the rights to manufacture a car
designed by a French racing driver, Louis Chevrolet. Using the
Chevrolet Motor Co. of Delaware as a holding company for vari-
ous activities including the production of Chevrolet cars, Durant
steadily increased his holdings of G.M. by exchanging Chevrolet

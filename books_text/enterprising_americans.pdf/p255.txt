230 + THE ENTERPRISING AMERICANS

raise taxes in 1932, though at the time it seemed the soul of dis-
cretion and necessary to maintain faith in the American dollar.
Tn the event, the famous Hoover “comer” was nearly turned in
the summer of 1932, European recovery set in, and almost all U.S.
pundits agreed that the bottom had been reached. Came then
another period of doubt and hesitation as battle was mounted for
the national elections and rumors spread of currency devaluation.
It proved one hesitation too much for the wobbly banking struc-
ture. In early 1933 the RFC was unable to bail out a key bank,
the Union Guardian Trust Co. of Detroit, and its failure gave new
impetus to the withdrawal of deposits from other banks. While
Hoover and Roosevelt exchanged messages at cross-purposes the
run became general. And on March 6, two days after the in-
auguration, and by executive order, the banking system of the
richest nation of the world took a famous holiday.

The closing, however, proved momentary, and in the first hundred
days of the New Deal there came a new wave of optimism. In many
ways it seemed to be justified. President Roosevelt was magnificently
right in seeing that confidence was the key to the situation, and
his own courage and jaunty optimism—’ ’there is nothing to fear
but fear itself’—did much to break the mood of despair and
national paralysis. But the baffling question, to which historians
have paid all too little attention, is why business confidence was
never fully restored, and why the great depression dragged on for
six more painful years. In 1933, when F.D.R. gave his first fireside
chat, there were some thirteen million unemployed ‘in the U.S.
As late as 1939 there were still nine million unemployed men and
women, and on the record it was not Doctor New Deal but Doctor
Win the War who, in Roosevelt’s phrase, finally put the country
back to work.

The explanation of this failure of the New Deal to accomplish
its primary mission lies partly in Roosevelt’s inability to decide for
himself just what he was putting his confidence in. His far-reaching
decision to follow Britain off gold was deflationary in purpose,
but his subsequent failure to restore full redeemability of the
currency, once the metal had been repriced, deprived the U.S.
and the world of a needed monetary discipline. Many of his busi-
ness reforms—notably his insistence on “truth in securities” and

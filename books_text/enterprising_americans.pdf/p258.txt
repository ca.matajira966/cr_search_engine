THE DEPRESSED THIRTIES + 233
The pay-off story, indeed, is suggested by the figures for in-
dustrial profits and private investment-the key to industrial ad-
vance in a capitalist system. From their inflated peak of $8.3
billion in 1929, corporate profits after taxes plunged to minus $3.4
billion in 1932, recovered to $4.7 billion in 1937, and then col-
lapsed again in 1938. Domestic investment followed the same
pattern, falling from $16 billion in 1929 to a bare $900 million
in 1932, rising to an $11.7-biilion temporary peak in 1937, and
then dropping back to $6.7 billion in the 1938 slide. Under such
circumstances it is little wonder that the economy failed to pick up
the huge pools of unemployed left by the crash and open new
job opportunities for the growing labor force. To uncertainties
at home must be added the facts that despite Cordell Hull’s drive
for a reciprocal lowering of tariffs the Roosevelt regime remained
highly nationalistic in its orientation, that autarkic governments
were everywhere sprouting in Europe, and that expanding world
trade, based on freely convertible currencies, was hardly compatible
with European and Asiatic preparations for coming military show-
downs. Indeed, it was not until war orders from Europe broke the
pattern that the famous Keynesian “multiplier” took hold.

Yet the magnitude of the response of U.S. business to the war
is in itself refutation of the thesis that in the thirties businessmen
simply sat on their hands and the economy reached “maturity.”
The really surprising thing about the decade, in fact, is that
while investment was quantitatively lower than needed to restore
full employment, it was qualitatively impressive. While many men
were lamenting the disappearance of the old western frontier and
the lack of a new “ladder” industry such as automobiles, techno-
logical advance continued without abatement, and the scientific
revolution took hold. In time this revolution, gathering a momentum
of its own, would produce frontier after frontier and ladder after
ladder at a pace almost too dizzy to follow.

The big sleeper of the thirties was the chemical industry, which
began its march toward making “anything out of anything.” To
use the term “sleeper” for the chemical thirties is to speak relatively,
of course, for important companies had already begun to wheel
themselves into place as far back as 1920. The first forward step
came during World War I, when the British blockade of the

l Free Enterprises Before the Revolution

Pepperrell at Kittery creates a business barony.

Whaie-spermaceti candles and privateering enrich the coastal
towns,

A complex of forges and ironworks rises in Pennsylvania.

Indigo, rice, and tobacco bring wealth, and trouble, to the
South,

An energetic George Washington becomes the Colonies’ most
tnaginative businessman.

Wen the first Americans faced the rocky woodlands and inter-
vales of New England and the humid valleys of the Delaware and
James rivers, they were already on the way toward becoming the
“new men” later celebrated by those visiting Frenchmen, Michel de
Crévecoeur and Alexis de Tocqueville. The “new man” was inher-
ently a self-starter. Religion was the main propelling urge behind
his trek across the seas and, as Tocqueville emphasized, cast a
lasting and indispensable influence over the development of free
American institutions. But the man of the seventeenth century saw
nO opposition between faith and practical works. The high-pooped
ships that bore the colonists westward were launched, after all, by
joint stock companies, and carried the dreams of merchant adven-
turers no less than those of men seeking religious freedom.

Once here as colonizers, the first English invaders engaged the
extremes of summer heat and winter freezing with what shiftiness
they could muster. Inhospitable until the first tricks of adaptation

1

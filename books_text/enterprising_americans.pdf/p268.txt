13 The Modern World of Enterprise

Industry delivers the LST’s, bombers, proximity fuses, and
Spare.

It reconverts to automobiles, refrigerators, and radio sets and
to suburban split-levels.

Diversification breeds new competition.

R. and D. spins out transistors, antibiotics, rare alloys, and
Space vehicles.

Despite various threats and stumbling blocks the international
market widens,

Baar in the 1950’s commentators on the American scene began
to notice a new phenomenon: the depression-born animus against
the free-market system was disappearing. To use the cliché that was
soon to be in everyone’s mouth, the “image” of business was
shifting from nefarious to good. The change in the climate of
opinion manifested itself in the unlikeliest quarters: even old New
Deal stalwarts seemed to be altering their view. For example,
David Lilienthal, former boss of the govemment-owned TVA, was
loud in his praise of the accomplishments of large private corpora-
tions; and ex-brain-truster Adolf Berle, now the prophet of an ethi-
cal and socially beneficent business system, no longer read the
doom of the free market in the statistical tea leaves that he liked

periodically to consult.
The popularity of the new image of business went hand in hand
243

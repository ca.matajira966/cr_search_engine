250 . THE ENTERPRISING AMERICANS

committed to one type of product tried periodically to “remake”
their markets. The competition between substitutes—between
stainless steel, coated steel, aluminum, wood, plaster-board,
brick, cinder and cement blocks, and a whole host of plastics, for
example—was fierce, and there were so many companies involved
in providing viable alternatives in most areas that there could be
no possibility of an effective and lasting cartel agreement even if
one were desired.

As for mergers, which were undertaken in the 1890-1910 period
in hopes of achieving an almost complete monopoly, they were now
pursued for competitive motives that were wholly in keeping with
the spirit of the Sherman Antitrust Act. Railroads sought “hori-
zontal” mergers with other railroads in order to get into a position
to maintain themselves in a transportation world that was increas-
ingly dominated by trucks, buses, private automobiles, and air-
planes. The Ford Motor Co. sought a “vertical” merger with certain.
component units of the Electric Autolite Co. in order to compete
with General Motors, which owned its own spark-plug division. The
Snyder Co. of Detroit bought a company making pharmaceutical
equipment and filling machinery in order to wriggle free of total
dependence on the machine-tool purchases of the automobile com-
panies. Other enterprises mixed mergers with internal diversifica-
tion. The oil ‘companies, with a commitment in petrochemicals,
invaded the territory of the old-line chemical companies by an
incursion into nitrogen. And the du Pent Co., baffled by the govern-
ment’s objection to its part ownership of General Motors, licensed
nylon manufacture to Chemstrand and let out cellophane to Olin
Mathieson as it put development capital, not into new nylon and
cellophane capacity, but into such things as Orlon and Delrin.

To provide the needed diversification when saturation threatened,
the modern corporation continued to put ever greater effort into R.
and D. (research and development). It also pursued R. and D. to
keep its older products in competitive trim. R. and D. resulted in
the oxygen process, which cut the cost of making steel. It de-
veloped the process of turning taconite ore into pellets that can be
used as a high-grade blast furnace feed. In the coal industry, R.
and D. had not only mechanized the mines as fast as labor costs
rose, but resulted in the pulverization and liquefaction of coal

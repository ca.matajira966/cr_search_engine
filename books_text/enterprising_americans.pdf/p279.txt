254 . THE ENTERPRISING AMERICANS

run by electronic signaling and “feedback” devices. But continu-
ous-process industries such as petroleum, chemicals, and paper
used electronically guided flow systems. And after the Korean War
electronics began to be applied to the automation of hard-goods
manufacture.

While electronics was the star of the postwar business world, it
was not the only great performer. Without any big governmental
R. and D. subsidy, drug manufacturers took off on their own,
jumping their sales from $300 million in 1939 to an almost in-
credible $3.3 billion in 1961. The $410 million that the public
spent on antibiotics alone in 1961 represented more money than
was spent for all types of drugs in 1939—as did the $350 million
spent on synthetic vitamins. Though the sums involved would, on
the face of things, argue that medical costs had been skyrocketing,
the prices for such “wonder drugs” as penicillin dropped and
dropped over the years. As for preventives such as the Salk polio
vaccine, the cost of almost total immunization in any com-
munity represented only a few cents per child. What the whole
phenomenon suggested is that the drug industry had been getting
money for the prevention or quick cure of diseases that used to
go to doctors for long and often far more’ expensive cures; pneu-
monia was now eradicated within the week, and tuberculosis in most
cases no longer demanded long sanatorium treatment. All this con-
tributed significantly to keeping people healthy and able to enjoy
the good life that rising incomes made possible.

This rise was spectacular by almost any standard, and the good
life—sometimes called “keeping down with the Joneses’’—had
become a commonplace. Entering the 1960’s, American society
could no longer be represented by a pyramid, with the few at
the top having most of the purchasing power and the millions at
the bottom having little. Modern society is “bunched in the mid-
die,” financially speaking: some 47 per cent of all non-farm families
in 1959 had after-tax cash incomes of $5,000 to $10,000 a year.
The “proletarian” worker was disappearing; the old “blue-collar”
man, now a machine watcher, lived as often as not in a split-level
“ranch house” next to a white-collar contemporary. Blue collar’s
diversions were likely to be skiing, bowling, boating—which created
big business in themselves.

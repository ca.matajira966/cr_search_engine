FREE EN TERP RISERS BEFORE THE REVOLUTION. 3

slaer (Dr. Hamilton’s spelling) still held on to a manor forty-eight
miles long, twenty-four miles wide. Moving into Connecticut, Dr.
Hamilton noted the “large towns and navigable rivers . . . people
are chiefly husbandmen and farmers . . . the staples same as Mas-
sachusetts’ . . . horses are shipped to the West Indies, one town
is famous for its onions, a sloop is loaded with them.” But Con-
necticut oppressed him a bit with “its ragged money, rough roads,
and enthusiastic people.”

Boston was better, the Bostonians “more decent and polite in
their dress, tho more fanatical in their doctrine. . . .“ They gave
“indirect and dubious answers to the plainest questions . . . but
there is more hospitality shown to strangers than in New York and
Philadelphia . . .“* and an “abundance of learning and parts. . . .“

The local talk was “on commerce and trade,” the staples were
“shipping, lumber, and fish.” Salem, a “pretty town” with a long
street, had pleasing architecture; at Marblehead, just next door,
there was little but fish to talk about: fish flakes--or racks—spread
out to dry on 200 rocky acres around the town; ninety fishing
sloops employed out of the port; the yearly value of the fishing in-
dustry &.34,000 sterling for some 30,000 quintals (or three million
pounds) of dried and salted fish. At Portsmouth, New Hampshire,
where geese were kept in the fort to give alarm in case of a night
attack by the French from the north, the trade was in fish and in
masting for ships.

As Dr. Hamilton noted, the New Englanders had really to scrape
for their sustenance. They were the New World’s first real business
leaders because, with them, it was a case of root hog, or die. They
bulk larger than the Pennsylvania Quakers in the first commercial
annals of North America, partly because they made more noise
about their affairs and partly because, in the words of the English-
man Edward Randolph, their lack of a rich hinterland compelled
them “to trye all ports to force a trade.” As early as 1713 the New
Englanders began building schooners for the nascent Grand Banks
fishing fleet; and it was in the first part of the eighteenth century
that their distilleries made rum into a ubiquitous medium of ex-
change that flowed to the Guinea coast of Africa (to pay for gold
dust and slaves), to the fishing stations off Newfoundland and Nova
Scotia (where, as grog, it kept the hardy suppliers of the fish mar-

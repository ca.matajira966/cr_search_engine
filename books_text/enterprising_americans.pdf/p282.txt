THE MODERN WORLD OF ENTERPRISE “ 257

of the Gompers formula—’’More’’—was too strong to be lightly set
aside. In Detroit Walter Reuther continued to push the idea that
purchasing power depends on ever rising hourly wage rates. So
the incrustations on the U. S. system grew as wages and fringe
benefits went beyond productivity. The working man who kept his
job was better off than ever even in a time of rising inflation, but
the steel union lost a third of its membership as Europe and Japan,
getting the jump on America with the new oxygen furnaces, made
steel at better competitive rates.

Instead of Marx conquering the world, it was the enterprising
American who had rammed home his lesson all too well. In the
sixties we lost our overseas electronics markets to Japanese com-
panies such as Sony. Remington and Underwood typewriter trade-
marks gave way to the Italian Olivetti; Hollywood gasped for life
as France, Britain, Italy, and even Yugoslavia made better and
cheaper films; the German Volkswagen and the Japanese Toyota
and Datsun squeezed the high-powered and oversized Detroit car
out of foreign markets and even invaded the U. S. in ever increasing
numbers; and Hong Kong textiles were all over the place. Only in
airplanes and computers did we continue to hold our old lead. The
Boeing 747 remained in great demand (the German Lufthansa
began using it as a transatlantic freighter), and the Red Chinese
signaled their return to the world community by bidding for Boe-
ing’s smaller 707. But Boeing itself, made fearful by the retraction
of government support for the giant supersonic, the SST, felt con-
strained to make provision for manufacturing planes in Japan and
Italy. IBM was still the great name in jumbo-sized computers, but
the Japanese had made big inroads in the mini-computer market,
and it was only a question of time before they would go for the
bigger stuff. In the meantime we had suddenly had to face up to our
growing energy shortages. The need for oil from the Persian Gulf
and liquified natural gas from Algeria threatened to add pro-
gressively to our balance of payment woes. It remained a matter
of pride to realize that American international companies had been.
dominant factors in developing Saudi Arabian oil fields, but this did
not keep the price of oil from rising as we lagged in exploiting new
sources on our mainland, in Alaska, and on our continental off-
shore shelves.

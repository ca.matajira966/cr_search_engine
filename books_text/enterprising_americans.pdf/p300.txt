Communism, 263

Compact car, 249-250

Competition, suppression of, 150-153
Compton, Arthur H., 236
Computers, 252-253

Comstock Lode, 126, 134

Conestoga wagon, 65
Conrad, Dr. Frank, 199-200
Consolidated Edison Co., 186, 193
Consolidated Tobacco Co., 174, 180
Constitution, U.S., 36, 38, 55

Cook, Capt. James, 47
Cooke, Jay, 130-132, 138, 169

Cooper, Peter, 75-76, 105-106, 113-116
Copper industry, 57, 184

Cornell, Ezra, 106, 115

Corporation, first U.S. 41

Cort, Henry, 112

Coster, Charles H., 173

Cotton gin, 60

Cotton industry, 5, 48, 55, 69, 83, 120-

Couzens, Jame 211, 213
Coxe, Tenth, 40
Credit Mobilier, 134, 140
Crocker, Charles, 134
rem ton, Samuel, 53
Cross Co., 253
Crowther, Samuel, 205
Cumberland Road, 63, 66
Currency problems, 31, 34, 82
Curtiss-Wright Corp., 241
Cushing, John P., 49
Custis, John Parke, 23
Cutten, Arthur, 228
Cyclotron, 236

Dabney, Charles H., 169

Daimier, Gottlieb, 205

Dale, Sir Thomas, 2

Davis, Ari, 107

Davis, Jefferson, 120

Davis, Phineas, 76

Davison, Henry P., 181, 183

Day, Benjamin Henry, 104

Dayton Engineering’ ‘Laboratories Co.
(Delco), 215-216

Deane, Silas, 30-31

Declaration of Independence, 10

Deere, John, 94

De Forest, Lee, 199

Delco self-starter, 215-216

De Mine, Cecil B., 198

Depew, Chauncey, 172

Derby, Elias Hasket, 45-47

INDEX . 275

Derby, Richard, 7, 10

Derby Turnpike, 65

Detroit, growth of, 73-74,202-222

Dickens, Charles, 70

Diesel-electric locomotives, 238

Dill, James B., 174

Dillon, Read & Co, 219

Disston saws, 88-89

Dodd, Samuel C. T., 153

Dodd, William E., 122

Dodge, Gen, Grenville, 135

Dodge brothers, 207, 211, 243,219

Douglas Aircraft Cog Im Cy 241

Dow Chemical Co, 236

Drake, Co}. E. L. 149

Drew, Daniel, 77, 141, 143, 145

Drug industry, 254

Duer, William, 39, 44

Duke, James B., 174

Dunne, Peter Finley, 164

Du Pent, Lammot, 127, 234

Du Pent, Pierre, 216-217

Du Pent de Nemours, L E. Co., 101,
126, 129, 181,216,228,246,250

Durant, William C., 183, 202, 214-215,

Duryea brothers, 205
Dynamo, 186, 190, 192

Eastern Air Transport, 239

Eastman, George, 198

Eastman Kodak Co., 237

Eaton, Maj. Amos Beebe, 92-93

Echert, J. Presper, 252

Edison, ‘Thomas ‘A, 50, 147, 173, 184
201,251

Edison effect, 199

Edison Electric Light Co., 193-194

Eisenhower Administration, 256

Electric Autolite Co., 209,250

Electric automobile, 201

Electric Bond & Share Co., 195-196

Electric light and power, 185-198

Electronics industry, 199,236,251

Elkins, William, 196

Erode, Carl, 212

Emerson, Ralph Waldo, 100

Emery, Lewis, Jr, 155

Enfield rifle, 92

Equitable Life Insurance Co., 181

Eriecson, John, 129

Erie Canal, 36, 66-68, 74-77

Erie Railroad, 143, 169-170

Erskine, Robert, 113

Ethyl Gasoline Corp., 235

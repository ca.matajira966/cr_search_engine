Gross national product, 224, 244, 256,

Guaranty Trust Co., 181
Guffey, Col. J, M., 155
Gulf Oil Co,, 155, 183

Hacker, Louis, 40
Hackley, Charles H., 87
Hadley, Arthur

Hall, Charles Morte 183
Hambleton, John, 238

Hamilton, Alexander, 33, g74191
Hamilton, Dr. Alexander,
Hancock, John, 10, 24, 3.
Hancock, Thomas, 8-10
Hansen, Alvin, 224

Hargreaves, James, 53
Harkness, 8. D., 150

Harnden, William, 101

Harper & Brothers, 116
Harriman, E Edward H., 174, 177

Harvard University, 9,56, 196,238

Harvey, Charles T., 80

Hasenctever, Peter, 113

Havemeyer, Henry ©., 174

Haynes, Elwood, 205

Hays, John Coffee, 90

Hearst, William Randolph, 126

Helper, Hinton Rowan, 122

Hendrick, Burton J., 142, 158

Henry, Joseph, 187

Henry, William, 71

Hewes, George Robert, 24

Hewitt, Abram S., 114, 128
Andrew Jackson, 246

Hill, James Jerome, 138, 175

Hindenburg, Paul von, 241-242

Hoe, Richard March, 103-104,

Holbrook, Stewart, 86

Holding companies, 195

Homestead Act, 133

Homestead strike, 165

Hood, Thomas, 107

Hoover, Herbert C., 227,229

Hopkins, Mark, 134

Hopkins, Samuel, 12

Houdry Process Corp., 236

Howe, Elias, 107-111

Howe, F. W., 93

Howells, William Dean, 141-142

Hubbard, Gardiner G., 1!

INDEX . 277

Hudson, J. L., 209

Hull, Cordell, 233

Hunt, Walter, 108

Hunter, 8. V. A., 111
Huntington, Collis P., 134
Hussey, Obed, 96
Hutchinson, B. E., 219

Hyatt Roller Bearing Co., 216

IBM, 257

Ice industry, 101-102

Illinois Central Railroad, 116, 125, 174

Incomes, 1960's, 254

Indians, 35, 63-64, 83-84, 137, 187

Indigo, | 17-18,27-28,234

Industrial revolution, American, 62

Inflation, 34, 82-83, 138

Inland Steel Co., 183

insull, Samuel, 193, 196

Insurance companies, 101

Interchangeable parts, 61, 211-212

Interlocking directorates, 69, 181

Internal-combustion engine, 201, 221

International Business Machines Corp.,
200, 252,262

International Harvester Co., 98, 181

International Mercantile Marine Co.,
180-181

International Paper Co., 166

Invention, rise of, 43-62, 185-201

Investment capital, 49-50, 138

Iron and steel industries, 12, 16, 57, 71,
80, 111-118, 128-129, 133

Irving, Washington, 83

Jackson, Andrew, 40, 66, $1, 106, 129

Jackson, Patrick Tracy, 55

Jefferson, Peter, 22

Jefferson, Thomas, 19, 27, 36, 47, 61,
65-66, 94, 132

Jeffery, Thomas B., 203

Johnson, Herbert, 231

Johnson, Gen, Hugh S., 231-232

Jones, Capt. Bill, 158-159

Jones & Laughlin Steel Corp., 117, 183

Joy, Henry, 209

Judah, Thesdore, 134

Juniata iron works, Pittsburgh, 117

Kaiser, Henry J., 246, 248

Kaiser Aluminum & Chemical Corp.,
221

Kansas Pacific Railroad, 135-137

KDKA, radio station, 199-200

Kelly, William, 117, 128

BUSINESSMEN JOIN IN AN UNBUSINESSLIKE WAR. 29

in 1767. For the colonists, after all, were prudential men; even
the doughty John Adams managed to reconcile himself to drinking
the lightly taxed beverage at John Hancock’s home; he merely
“hoped” that his particular cup might be a smuggled variety from
Holland. But in early 1773, the mercantilist advisers to King George
HI were particularly concerned over the possible failure of the
East India Company, a “favored instrument” of the Crown which
had a seven-year supply of tea stored in its warehouses along
the Thames. The company had to get this tea to market to keep
from going bankrupt. Since, unlike the colonists, the East India
Company was represented in Parliament, the 1767 Tea Act
was quickly supplemented to give the company a monopoly of
the American market. It was specified that the bitter leaf should
be shipped only in the East India Company’s own bottoms, and
that it should be delivered not to indigenous Boston and Newport
retailers, but to favored royal consignees. This cut out both the
colonial shipowner and the colonial storekeeper (who were often
united in the same person).

The result, quickly felt, was the dark excursion at Griffin
Wharf, with the wild “Indians” whooping their defiance in the
night. Benjamin Franklin, along with other prominent citizens
of the middle colonies, was shocked by the violence of the Tea
Party. But as Britain proceeded to punish the town of Boston by
cutting off its commerce and literally trying to starve it into sub-
mission, the other colonies reacted swiftly to the severity of the
retribution. To help keep Boston alive, Charleston, South Carolina,
sent rice, which was bootlegged into the port over the neck con-
necting it with the Massachusetts mainland. Marblehead patriots
provided codfish; Baltimore dispatched rye and bread. And Colonel
Israel Putnam—’’Old Put’’—arrived one day from Connecticut
driving a flock of sheep. As Esther Forbes has described it, Old
Put was so much “caressed” by the grateful Bostonians that he
had a hard time getting away. And when the merchant-financed
opposition to the British finally erupted in gun smoke near the
tude bridge that arched the flood in Concord, enough of the busi-
ness leaders in New York, Philadelphia, Virginia, and South
Carolina stood by Boston to make a united front.

The war itself was not a businesslike war in the modern sense;

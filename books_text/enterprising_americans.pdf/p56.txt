BUSINESSMEN JOIN IN AN UNBUSINESSLIKE WAR 31

foraged as best they could, helped by such public-spirited business-
men as Jeremiah Wadsworth of Hartford, Connecticut. As an as-
sociate of Quartermaster General Nathanael Greene (who was
himself a silent partner with Wadsworth in private operations), the
“sachem” Wadsworth helped pick Washington’s army up after
some of its early sufferings. Wadsworth also helped build the
frigate Trumbull on a 5 per cent commission, and with his
partner, Barnabas Deane, held a contract from the Continental
Congress to supply masts and spars for ships to all the states.

The food, clothing, and hay that were forwarded to the Con-
tinental soldiers as they hacked about New England, the Hudson
Valley, and New Jersey had to be paid for somehow. At first
Congress tried to foot the bill by an emission of paper money
(the continentals). It also issued “loan certificates,” and still later
asked for “grants” from the separate state governments. Finally
the whole business of paying for the war was turned over to the
states save in the instances that American representatives abroad
managed to cajole loans from the French and the Dutch. The
final cost of the war has been estimated at $104 million in terms
of gold, but most of it was paid in currency that a few years later
was all but worthless. The irrepressible Franklin remarked in
1779; “This Currency, as we manage it, is a wonderful Machine.
It performs its Office when we issue it; it pays and clothes Troops,
and provides Victuals and Ammunition: and when we are obliged
to issue a Quantity excessive, it pays itself off by Depreciation.”

Actually, financing the war was not quite so simple as that, and
printing-press money alone would never have done the job. Powder
and shot from abroad had to be paid for in hard money and
Congress relied heavily on merchants to lay their hands on it
through manifold trading operations. The best known of these
merchants was Robert Morris of Philadelphia, who in the early
years of the war co-ordinated foreign procurement. Son of a
British trader, Morns served his apprenticeship in the Philadelphia
mercantile house of the Willings, and married Mary White, a
Philadelphia belle who was the sister of the first Episcopalian bishop
of Pennsylvania. By the time of the war he was a full partner
of Thomas Willing, and was also a personal friend of Washing-
ton. Morns did extremely well by the struggling young confedera-

44. THE ENTERPRISING AMERICANS

and silver specie from abroad through all kinds of complicated
foreign transactions. Tracing the course of any hard bit of cur-
rency over the decades must, of course, defeat the ingenuity of
even the closest student of the Keynesian “multiplier”: gold notori-
ously leaves no scent. But the success of American trade in the
early years of the Republic can be roughly measured from the
fact that U.S. reserves of specie, at first desperately short, increased
by 1820 to a point that permitted the federal government to pay
off most of its foreign creditors in hard coin.

The great American sea story, which culminates in the clipper
ships of the 18 50's, had its beginnings, of course, in the earliest
ventures of New England and Delaware Valley merchants to
exploit the wealth of the West Indies and Africa. During the
Revolution trade took on a new dimension as the colonists, daring
the British blockade, sought new sources of supply. The Cabots of
Beverly sent ships to Géteborg, Sweden, and even all the way into
Russia to pickup duck (a textile, not a fowl). The war also opened
the eyes of other bold men—Girard and Morris of Philadelphia,
the Browns of Providence, the patriotic Jeremiah Wadsworth of
Hartford-to the possibilities of assembling and delivering goods
over long distances and in big bulk. And almost inevitably trade
in the post-revolutionary period btoke out of the Atlantic and
sought the distant Far East.

The first big breakthrough voyage in the grand manner came
in 1784, when the Empress of China, a promotion of Philadelphia’s
Robert Morris, New York’s William Duer, and Daniel Parker of
Watertown, Massachusetts, set sail out of New York Bay bound
for Macao and Canton in China. This first venture into the “China
trade” set a pattern for what the early economic historian Weeden
called “the new order of merchants.” The Empress of China, 360
tons burden, was loaded with ginseng root, which she carried into
the Whampoa roads some six months after leaving New York.
Lacking charts, the Empress negotiated the perils of Sunda Strait
in East Indian waters in company with a French man-of-war.
Since there were no cabled analyses of the market in those days,
the bartering of the cargo was in the hands of an attractive young
man, Major Samuel Shaw, who had served through the Revolution
as aide-de-camp to General Knox.

THE QUEST FOR CAPITAL - 49

forget the outer world in the excitements of developing the new
continental market. Meanwhile Stephen Girard of Philadelphia
had joined the merchants of Salem, Boston, and New York in
assembling great cargoes for the Far East (including Turkish
opium), and bringing home a fabulous wealth in return. New Bed-
ford, Nantucket, Sag Harbor, and New London specialized in
whaling, a hard-bitten business which became notorious after 1830
for its cruel skippers and its tight-fisted owners who often cheated
their crews of their “lays” (titles to a proportion of a voyage’s
profits). The cheating was done by various stratagems such as
reckoning a crew’s shares in prices below the actual market. Until
“tock oil,” or petroleum, began to service the lamps of China
(along with those of the United States), whaling and the manu-
facture of whaling products followed after cotton textiles and shoes
as Massachusetts’ leading industry.

It was commerce that provided the vital capital needed to stimu-
late and expand domestic U.S. manufacturing and businesses of
all kinds. With money made out of the China trade, Stephen Girard
of Philadelphia moved into banking, offering in 1812 a “complete
service under one-man management .“ Money made by the Browns
of Providence out of foreign trading and by the Lowells and
Jacksons of Boston and Newburyport out of shipping spilled over
into textile manufacturing. So, too, did the whaling money of New
Bedford, Sag Harbor, and New London. In the 1830's and 1840's
came the really dramatic shifts. For example, John Murray Forbes,
brother of the charming Robert Bennet Forbes who had originally
gone to sea at the age of thirteen “with a capital consisting of a
Testament, a Bowditch, a quadrant, a chest of sea clothes, and a
mother’s blessing,” took his own capital won in the China trade
and, with a number of merchant friends, bought into the unfinished
Michigan Central Railroad. John P. Cushing, Thomas Handasyd
Perkins’ Canton agent, put his money into banks, insurance com-
panies, and railroads, most of them in New England. Perkins him-
self, working anonymously behind the scenes, became one of the
early investment capitalists. Thus the original merchant capital
became the first investment capital of the New World. It had all
come out of the sea.

This capital never could have gone to work, however, had not

Publisher’s Foreword
Gary North

Aiwericans’ attitudes regarding the moral legitimacy of business
ebb and flow, generation by generation. When John Chamberlain
wrote this classic history of American business in 1960, the attitude
of American intellectuals toward business was almost universally
skeptical, when not actually hostile. The only major exceptions to
this rule were on the college campus — and really only at the best col-
leges — where a handful of innovating scholars were beginning to
revolutionize the study of American economic history, men such as
the present “dean” of American business historians, Thomas C.
Cochran. They understood the magnitude of the achievements of
American entrepreneurs in a nation which long has enjoyed com-
parative freedom from government intervention into markets.
What is remarkable in retrospect is how well (and how early) John
Chamberlain told essentially the same story, but in the language of
the journalist rather than the college professor. The Enterprising
Americans was itself an act of intellectual entrepreneurship. It
pioneered a new vision of the American past.

By the mid-1970's, the “new” economic historians’ revision of
American economic history had begun to penetrate the thinking of
a growing number of conventional historians, at least the younger
ones. A reappraisal of American business history was later paral-
leled - I would even say encouraged — by the influence of President
Reagan’s moving rhetoric in favor of economic freedom, coupled
with seven years of economic boom (following the sharpest eco-
nomic decline since 1940, in 1981-82). Business in the Reagan years
generally had a good press. Even the intellectuals had to admit that
things were turning out better than they had expected.

But the story of the seven lean years that follow the seven fat
ones is a familiar one in man’s economic history. The partiai break-
down of the “junk bond” (higher risk, higher interest rate) market in
1989, coupled with the growing scandals of the faltering savings and
loan industry — an industry subsidized into irresponsibility for dec-

XI

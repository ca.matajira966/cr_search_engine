116 Granp I:Lusions

theory, educational imagery can bridge the gap between making
the decision and behaviorally incorporating the decision. The
theory is that if the decision is clearly imagined and acted out re-
peatedly, then when the opportunity comes to enact the decision,
the process will be facilitated.”55

And what fantasies and decisions are the curricula facili-
tating? One government-sponsored program used widely by
Planned Parenthood educators told teens that they could have
fantasies which involved “sexual feelings about people of the
same or opposite sex, parents, brothers and sisters, old people,
animals, nature, inanimate objects, and almost anything you
can imagine. It is unusual for a person not to have some strange
sexual fantasies.”57

Carrie Lipscombe and Laura Gibbs participated in a Posi-
tive Imaging exercise in a Planned Parenthood-sponsored class
at their neighborhood YWCA. “The teacher told us to close our
eyes,” Carrie remembered. “We were to imagine ourselves stand-
ing on the end of a diving board.”

“She went into a lot of detail, helping us to imagine the crys-
tal clear water, the bright blue sky, and the warm, dry sunshine
on our skin,” Laura said. “She asked us to feel ourselves bouncing
off the board and splashing into the cool, refreshing pool.”

“Then she told us that that feeling was very much like an
orgasm,” Carrie said. “After that, we were supposed to imagine
all kinds of situations where we could relive that feeling of going
off the diving board sexually.”

“I was pretty shook up by that,” admitted Laura.

“Me, too,” Carrie said. “The whole deal was pretty manipu-
lative. I didn’t like it. Not a bit.”

“The objectionable feature of these programs now being
promoted by Planned Parenthood,” says economist and social
analyst Jacqueline Kasun, “is not that they teach sex, but that
they do it so badly, replacing good biological education with ten
to twelve years of compulsory consciousness raising and psycho-
sexual therapy, and using the public schools to advance their own
peculiar worldview ,”5®

Like Catherine Toleson, Carrie Lipscombe, and the others,
Rhonda Williams was shocked by the foul and indecent mate-
rials Planned Parenthood was distributing in her junior high

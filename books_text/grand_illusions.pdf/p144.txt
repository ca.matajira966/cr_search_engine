124 Granp ILLustons

about it. Kinda crazy, isn’t it? Well, the point is, Tina was a little
amazed by the whole deal.”

“When she found out she was pregnant, she seemed to take it
real well. All in stride,” Sarah noted. “She even talked about it in
our group meeting. The Planned Parenthood counselors set up
an appointment for her to get an abortion and that was that.”

“Only that wasn’t the end of the story,” Jackie said.

“Not by a long shot,” agreed Lucy.

“No, after the abortion, we all got together for our regular
meeting. And Tina was there. She’d had the abortion three days
earlier,” Lucy said.

“She really looked awful,” commented Jackie.

“The counselor asked her to talk about it,” Lucy went on.
“But she just sat there not saying anything at all. The counselor
then went into this /ong lecture about how important it is to get
all your feelings out, to communicate, to be honest — you know,
all that psycho-therapy stuff. Well, before any of us knew what was
happening, Tina just went berserk.”

“Yeah, she started screaming and crying and throwing stuff
around,” Jackie said.

“She said that the ‘peer’ training project had pushed her into
sex, filled her mind with all sorts of obscene ideas, and then
forced her into an abortion,” Deborah remembered. “She said
she’d learned everything except the right things and that she hated
what she’d become.”

“After a while, she was just sobbing uncontrollably,” Lucy
said, “And none of us knew what to do.”

“I think we were all pretty confused,” agreed Jackie.

“And, what was worse, for me anyway,” Sarah said, “was that
I knew she was right. We'd been sold a bill of goods. None of us
wanted to learn all that stuff about lesbianism and masturbation
and orgies and abortion and birth control and kinky fetishes and
stuff. And the things we did need to know we never even talked
about ~-things like a baby’s development, guilt, venereal dis-
eases, the health hazards of birth control, alternatives to abor-
tion, PMS, and depression. None of that.”

“After a minute or two, Tina left,” Jackie continued with the
story. “We were pretty stunned. But one of the Planned Parent-
hood counselors, well, she just started rattling on about how good

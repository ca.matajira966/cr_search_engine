Selling Sex: The Educational Scandal 5

it was that Tina was ‘able to ventilate her frustrations,’ and how
the group was ‘obviously growing in honesty toward one
another, and all that stuff.”

“That night, Tina committed suicide,” Lucy concluded.

The girls were all quiet now. Heads bowed in sadness and
shame.

“When that happened, we all got together,” Deborah said
finally, “without anybody from Planned Parenthood to look over
our shoulders. And we just talked.”

“And cried,” added Lucy.

“For all that hype about Aonesty,” Jackie admitted, “that was
the first time we actually were honest.”

After another long pause, Deborah noted, “I think we learned
a lot of lessons out of this, but two really kinda stand out. First,
Planned Parenthood was trying to force us to learn about—and
think about, talk about, and experiment about—things none of
us wanted to. And, second, Planned Parenthood skipped over the
stuff that we did want—and need—to know,”

“Yeah, the whole deal was really backwards, wasn’t it?” Lucy
said.

“Pm just glad it’s over, and I'm glad that Pm out of it—that
we're ail out of it,” Deborah sighed.

“Really!” the other girls nodded. “Really!”

According to Planned Parenthood’s own national survey,
conducted by the Louis Harris pollsters, most teens agree with
Lucy, Deborah, Sarah, and Jackie.!2° More than eighty-seven
percent said that they did net want comprehensive sexuality ser-
vices in their schools.127 Sixty percent said they didn’t even want
such services near their schools.128 Only twenty-eight percent of
the teens had actually become involved in sexual activities,12°
but ninety percent of those admitted that they had become pro-
miscuous simply because of a percetved peer pressure.'3° Nearly
eighty percent of them felt that they had been drawn into sexual
activity far too soon,

The teens in the poll admitted that their comprehensive sex
education courses fad affected their behavior. There was a fifty
percent higher rate of sexual activity for them after the classes.!5?
Sadly, their understanding of the consequences of such activity
was not correspondingly enhanced.

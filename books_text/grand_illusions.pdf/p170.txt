150 Granp ILLustons

deep square excavation pit, foot by foot, taking its shape—a
guild-hall rakishness in fire-flecked Italian granite, Fascinated by
the odd mix of Old World appointments and space-age technol-
ogy, she noted each day's progress on the building with a cat-like
curiosity and a child-like ardor.

“Tt’s an architectural icon,” she whispered to herself, breath
frosting on the cold, damp glass. “A contradiction in terms. A
post-modern anomaly. Beautiful, but frightening. Confusing.
Like me.”

“That's the first sign of the onset of senility, you know —talk-
ing to yourself into the wee hours of the night.” Bill Maxwell,
from accounting, had poked his head through the doorway of
her office. “Positively foreboding.”

“Senility!” she chuckled, taking off her smart, horn-rimmed
glasses and turning slowly away from the window. “Thanks a lot,
Bill. You know, you have an unfailing ability to inflict a venial
wound in passing whenever I least expect it. Charming.”

“Well, you know what Wodehouse used to say. When he was
past ninety?”

“I'm not sure ’'m in the mood. I doubt I'll find this too ter-
ribly amusing.”

“Oh, sure you will. He said that as long as you're going to get
old, you might as well get as old as you can,”

“Very funny.”

“T thought so.”

She turned. toward her desk. A drilling lease and several con-
tracts lay there, lounging in a puddle of soft light.

“Come on,” Bill beckoned. “Enough already. You've been at
it all day. Let’s go grab some supper at Ninfa’s. Maybe see if we
can get tickets for the theater after. What do you say?”

“Thanks, but no thanks, Bill. I’ve got a couple of things to tie
together and then I’ve got to get home. I’m utterly bushed.”

“Oh, baloney. Come on. Loosen up a little, will ya?”

“Really, P'm sorry, Bill. Another time.”

“Okay, okay. Try not to make it a late one.” He pulled on his
trench coat. “Good night. And don’t forget to lock up.” He went
away whistling.

She heard the door close out in the reception area and breathed
a deep sigh of relief. She just wanted to be alone. To think.

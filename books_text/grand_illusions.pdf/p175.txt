Strange Bedfellows. The Institutional Scandal : 155

and communications.”5° It conducts major national media cam-
paigns, produces films and audio-visual presentations, adminis-
ters staff and volunteer development training, and publishes
dozens of booklets, pamphlets, and directories every year.5! It
assists local service groups by conducting company-wide and
community-wide campaigns and cultivating increased corporate
giving through donor development programs.°? United Way
funds help organizations like the Salvation Army, Goodwill, the
Red Cross, and Big Brothers do what they do best: care for the
needy.53 Though recent charges of misappropriation of funds,
strong-arm tactics, and management skimming has sullied its
image, the philanthropic legacy of United Way is unquestionable5+
Millions of dollars every year are used to strengthen the work of
drug rehabilitation, medical research, emergency food relief,
sheltering the homeless, crisis counseling referrals, legal services
to the poor, and job re-training for the unemployed.

But they also go to Planned Parenthood— millions of dollars
worth every year.*® Even those few local United Way groups that
have yielded to pro-life pressure over the years, and removed
Planned Parenthood as a direct recipient of funds, continue to
support hot lines, family counseling centers, health service agen-
cies, and community associations that counsel for abortion and
refer clients to the organization’s abortuaries.*”

Nationally, United Way, over the years, has strongly defended
its commitment to Planned Parenthood and has consistently upped
its share of the annual fund-raising bounty.** It has even gone so
far as to entangle itself in Planned Parenthood’s political spats,5?
even to the point of risking its own tax-exempt status.°°

Jim Singleton is an executive with an international oil tool
manufacturing company based. in Oklahoma City. Always ac-
tive in civic affairs and community development, he has long
been an enthusiastic sponsor for United Way’s corporate pro-
gram. “I would personally go to each of our employees,” he said,
“and encourage them to give. I would help executives in other
companies set up incentive programs. I even did some volunteer
work at the regional United Way office.”

Jim’s enthusiasm was dampened significantly when he dis-
covered that the funds he had worked so hard to raise were being
used for Planned Parenthood’s abortion and birth control
crusade. “I went to several of the directors and board members

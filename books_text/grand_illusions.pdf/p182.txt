162 Granp ILLusions

Parenthood. Instead of supporting organizations, institutions,
services, charities, and ministries that strengthen the family,
lower taxes, produce individual initiative, and promote health
and justice, they tend to endow the narrow, sectarian, and
destructive programs and pogroms of the Left.'9#

The Rockefellers began funding Planned Parenthood in 1952
when Margaret Sanger charmed John D. Rockefeller at a lavish
téte-a-téte in her Tucson mansion.!® [n 1959, the Fords began
giving and, before long, virtually all the others had followed
suit.!© Since then, ‘millions of dollars have been pumped into
the organization by their foundations. As nimble as a dry leaf
in the whirlwind, Planned Parenthood has been able to sidestep
its ideological commitment to Revolutionary Socialism long
enough to stretch a supplicating hand toward America’s greatest
capitalists. And, often enough, to make it as rich as they.

Gerald Wilson is an heir to a multi-million dollar privately
held business. For years, he fought the patronage of the Left,
generally, and Planned Parenthood specifically, in his family’s
philanthropic foundation. But the task turned out to be both
thankless and fruitless, In the end, he was forced to endow his
own foundation, one that he could control. “It is terribly
frustrating,” he told me, “to see so much opportunity for good
translated into so much opportunity for evil.”

He especially bemoaned the vanity, graft, and manipulation
that is almost inherent in the endowment process. “In many
ways,” he said, “the system is just a series of unholy alliances,
meant for right, destined for wrong.”

Unholy Alliances
Throughout the Bible, warnings against entering into un-
holy alliances are abundant and clear.

“Woe to the rebellious children,” declares the Lord, “who exe-
cute a plan, but not Mine. And make an alliance, but not of
My Spirit, in order to add sin to sin” (Isaiah 30:1).

Do not enter the path of the wicked, and do not proceed in the
way of evil men. Avoid it, do not pass by it; turn away from it
and pass on. For they cannot sleep unless they do evil; and they
are robbed of sleep unless they make someone stumble. For

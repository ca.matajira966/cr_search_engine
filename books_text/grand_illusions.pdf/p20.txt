xe Granp ILLUsIONS

Aside from those substantive changes, only cosmetic alter-
ations have been made to the work. Even so, I pray that they
will enable it to find a whole new era of usefulness as the pro-life
movement enters its third decade of struggle for the sanctity of
human life—and that it will continue to fluster the professional
bureaucrats and ideologues at Planned Parenthood into an em-
barrassed silence.

The Gnarled Tare
It seems that the minions of the death industry can face any-
thing except the obstacle of truth, They will go to absurd lengths
to avoid it, to rail against it, to deny it, or failing all else, to
smother it in a morose blanket of subterfuge. .
Their end can not but be as described so powerfully in the
violet prose of Tristan Gylberd:

When ere a man has ought to hide,
Or follows hard,
Beside the tried;
When ere he feigns true delight,
Or captures lust,
Behind the night:
Beware, I say, beware.
Leviathan has sown;
And his is the gnarled tare.?

Its grand evasions notwithstanding, Planned Parenthood—and
its dastardly confederates — stands condemned by its own pride
of silence.

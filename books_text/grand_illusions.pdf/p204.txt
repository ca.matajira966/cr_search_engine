184 Grano ILLusions

in America.'53 [It took a strident stand against the racist and
Eugenic programs of Margaret Sanger.'5+

During most of the nineteenth century, abortion was an open
cornmercial enterprise.!% In fact, between 1830 and 1880, abor-
tionists advertised their services quite freely.6 And they did a
booming trade.157

One woman, Ann Lohman, built her abortion business into
an incredibly profitable enterprise with clinics in Newark, Provi-
dence, Boston, Philadelphia, and five more in New York.18 She
operated a mail-order business that ranged across the continent,
with dozens of salesmen up and down the East Coast peddling
her abortifacient pills.5° Known professionally as Madame
Restell, Lohman spent as much as sixty thousand dollars a year
on advertising—an incredible amount in that day—to support
her empire.16 By the 1870s, she was living in the lap of luxury,
with an opulent mansion on Fifth Avenue, a magnificent horse-
drawn coach, and a social standing of the highest order.!®

Lohman was not alone in her prosperity. Abortion was big
business.16? Any number of opportunists cashed in. The dogma
of Mammon leaked into the whole land, touching the air with
benediction. !6

At the time, there was not even a semblance of.a unified
Christian opposition to the trade which claimed as many as a
million young lives a year. A number of doctors complained
that the public was apathetic and unaware, and that even the
“clergy, with very few exceptions, have thus far hesitated to enter
an open crusade against criminal abortions.”!65 One pro-life
leader of the day contended that ministers “have been very
derelict in handling this subject too delicately and speaking of it
too seldom.”!6° He wondered if anyone would ever take leader-
ship in alerting and arousing the public.1°

The New York Times—then owned and edited by committed
Christians—stood in the gap, grasped the ring, and took the
leadership. In 1870, it published an editorial entitled “The Least
of These Little Ones.” Filled with Biblical references, editor
Louis Jennings complained that the “perpetration of infant mur-
der . . . is rank and smells to heaven. Why is there no limit of
punishment?”!®8 Again, in 1871, he wrote that abortionists “have
openly carried on their infamous practice . . . to a frightful ex-
tent, and have laughed at the defeat of respectable citizens who
have vainly attempted to prosecute them.”!?

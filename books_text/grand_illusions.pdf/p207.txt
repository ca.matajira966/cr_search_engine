The Camera Blinked: The Media Legacy 187

Absalom wreaked a lot of havoc. So has the media. But there
is one thing that neither of them counted on: The good guys
always win in the end. There may be defeats along the way.
There may be major setbacks from time to time. Tranquility
may be dashed. The faithful may be sent into flight. But only for
atime. In the end, the cause of the righteous will be upheld ( Job
27:16-17). The.true truth will come out (Ezekiel 36:33-36). God’s
people will prevail (Matthew 6:10). If—and that is a big “if” —
if they will only do right, cling to the blessed hope, and stand
steadfast on the very great and precious promises of God
(Joshua 1:7-9).

Absalom abandoned the good legacy of his past, shielding his
wickedness with a cover of sophistication and moral indignation.
Similarly, the media has abandoned the good legacy of its past,
shielding its tainted advocacy of Planned Parenthood with a smoth-
ering cover of professional objectivism and market manipulation,

In the end, though, it wil! have its due.

A fool's lips bring strife, and his mouth calls for blows. A fool's
mouth is ‘his ruin, and his lips are the snare of his soul. The
words of a whisperer are like dainty morsels, and they go down
into the innermost parts of the body (Proverbs 18:6-8).

Conclusion

Nat Hentoff was at one time a card-carrying establishment
media spokesman, He was a board member of the ACLU. He
was a renowned advocate of civil liberties and radical liberal
causes. And he was a tenured journalist with the left-of-gauche
newspaper, the Village Voice.

But then he began to cover several widely publicized infan-
ticide and abortion cases. And he did the unthinkable: he be-
gan to deviate from “the orthodox liberal position that women
cannot achieve their basic rights without the right to kill incon-
venient fetuses.”!72

Hentoff’s colleagues were shocked. He was quickly
dropped from the board of the ACLU.1% And pressure from the
left beckoned for him to return to the fold. To conform.

Tt seems that freedom of the press in this country is little
more than theoretical these days.

And about that, Planned Parenthood couldn't be happier.

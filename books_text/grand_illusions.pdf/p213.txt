To the Uttermost: The International Legacy £93

McAlpin in New York.'6 She had grown increasingly concerned
that societal, civic, and religious pressure might snuff out her
nascent Eugenic ideals. As she asserted:

The government of the United States deliberately encourages
and even makes necessary by its laws the breeding—with a
breakneck rapidity — of idiots, defectives, diseased, feebleminded,
and criminal classes. Billions of dollars are expended by our
state and federal governments and by private charities and
philanthropies for the care, the maintenance, and the perpetuation
of these classes. Year by year their numbers are mounting.
‘Year by year more money is expended . . . to maintain an in-
creasing race of morons which threatens the very foundations
of our civilization.!7

She was especially distressed by the dim prospects that
democratic suffrage afforded her dystopic plans to implement a
universal system of inhuman humanism:

We can all vote, even the mentally arrested. And so it is no sur-
prise to find that the moron’s vote is as good as the vote of the
genius. The outlook is not a cheerful one.®

If there was little for her to cheer about in America, there
was even less on the international scene. Europe, decimated by
the Great War, was desperate to reverse its dramatic decline in
population, while the developing world was no less desperate to
stoke the hopeful fires of progress with aggressive population
growth.!9 Sanger’s message was falling on increasingly deaf ears.2°

By convening dozens of like-minded “neo-Malthusian pio-
neers” from around the world, she was hopeful that together they
would be able to circle the wagons, to “develop a new “evangelistic
strategy,” and ultimately to reverse the tide of public opinion and
public policy —and thus “to keep alive and carry on the torch of
neo-Malthusian truth,”*4

For six days representatives from France, England, Norway,
Holland, Austria, Hungary, Germany, Belgium, Spain, Sweden,
Switzerland, Italy, Portugal, India, South Africa, Russia, Mexico,
Canada, Japan, and China listened as “experts” delivered papers,
made speeches, held workshops, and offered dire prophesies.”*

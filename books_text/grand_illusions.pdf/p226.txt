206 Granp ILLusions

family as to the church.” After all, she said, “It is the church that
has been commissioned to hold the line against such things—
and so when such things continue unchecked, it is the church’s
authenticity and veracity that must first come under our scrutiny.”

Conclusion
Political theorist, Ayn Rand, may well have had the chilling
international legacy of Planned Parenthood in mind when she
said that:

Every major horror of history has been committed in the name
of an altruistic motive. Has any act of selfishness ever equalled
the carnage perpetrated by the disciples of altruism? Hardly.2+

With the demise of ideological totalitarianism toward the
end of the twentieth century, most neo-Malthusians turned to
the practical totalitarianism of social control through birth con-
trol as the last best hope of their altruism. Undoubtedly, it has
become—as Margaret Sanger predicted that it would—their
“Love Potion” and their “Holy Grail.”#

It is a faith—a kind of mirror-image reversal of Christianity.
It cannot be opposed by mere logic, reason, or political savvy.

G. K. Chesterton once quipped:

Tf you argue with a madman, it is extremely probable that you
will get the worst of it; for in many ways his mind moves all the
quicker for not being delayed by the things that go with good
judgment. He is not hampered by a sense of humor, or by char-
ity, or by the dumb certainties of experience.®6

We cannot hope to subdue Planned Parenthood’s madness
with argument—even with brilliant argument. Instead, its
counterfeit evangelism must be overwhelmed with authentic
evangelism. And that requires an authentic—and uncom-
promised —church.

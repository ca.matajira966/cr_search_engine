232 Granp ILLusions

H. G. Wells, Bertrand Russell, and George Bernard Shaw
all dabbled in their eclectic esoterica.'? And, not surprisingly,
Margaret Sanger accepted their tenets and practiced their eth-
ics as well.!! She was an avid Rosicrucian and a dues-paying
member of Unity.!?

The Light of Day

It is no coincidence that the extremism, brutality, excessive-
ness, and promiscuity of Planned Parenthood and its various
programs so directly matches that of the Cathari. Planned
Parenthood is little more than an institutional incarnation of
Margaret’s faith,

It is also no coincidence that the objective standard that
Bernard and Gregory applied to the cults is just as relevant
today as it was then. Virtually every major dogma of those
heretical sects is a plank in the Planned Parenthood philosophical
platform: promiscuity, greed, deception, revolution, socialism,
abortion, sorcery,'? birth limitation, and materialism.

Bernard and Gregory knew that such things were evi! in the
thirteenth century. They are no less evi in the twentieth.

To expose such evil is a Christian duty (Luke 17:3). To
rebuke sin and admonish error is never an option (2 Timothy
2:4). Instead, it is an obligation (Titus 2:15). Modeled by Christ
before His disciples (Luke 9:41), and before the world (John
6:26), it is like bearing testimony, an essential aspect of true
discipleship (Hebrews 12:5). It was openly practiced by the
Apostles Paul (Romans 15:14), James (James 5:1-6), Peter
(2 Peter 2:1-22), John (3 John 9-12), and Jude { Jude 4-23). And
it has been responsible for many of the church’s great revivals
throughout history.1*

Evil must be exposed.

Planned Parenthood must be exposed.

But exposing evil is not a thing to be taken lightly. It is a seri-
ous matter.

When the Apostle Paul was hounded day in and day out by a
demonic medium, he refrained from acting hastily or rashly
(Acts 16:16-17). Finally, after “many days” had passed, he con-
fronted the evil, exorcising the woman (Acts 16:18).

Slaying Dragons: The Character to Confront 247

expertise that we can consecrate for the Kingdom. Whatever
those areas are, we need to concentrate on heightening our skill-
fulness and perfecting our abilities.

Third, we need to support the causes and ministries that are
really accomplishing something.

The scandalous goings-on in TV evangelism have only served
to underscore the need to carefully target our giving and to sup-
port the works of genuine excellence. Ministries that are little
more than fundraising operations or personality cults should be
avoided at all costs. Causes that do little more than stoke the
star-maker machinery are worse than worthless. Time, re-
sources, and money are in precious short supply. They should be
distributed only to those ministries that actually manifest the ex-
cellent power of God.

Fourth, we need to promote and support youngsters that
show genuine promise. Scholarships must be secured for Chris-
tian students in journalism, medicine, economics, political
science, philosophy, history, education, fine arts, biology, and
business. And support must be developed for those few institu-
tions that are actually taking those young, impressionable stu-
dents and turning them into excellent champions for Christ,
dynamic activists for the Kingdom, and mighty workers for life
and truth.

Confronting the evil of Planned Parenthood demands the
unbending fortitude of Christian might. It requires the excellent
dynamic of Christian power. It necessitates the righteous charac-
ter of Christian strength.

For this reason, I bow my knees before the Father, from whom
every family in heaven and on earth derives its name, that He
would grant you, according to the riches of His glory, to be
strengthened with power through His Spirit in the inner man;
so that Christ may dwell in your hearts through faith; and that
you, being rooted and grounded in love, may be able to com-
prehend with all the saints what is the breadth and length and
height and depth, and to know the love of Christ which sur-
passes knowledge, that you may be filled up to all the fullness of
God. Now to Him who is able to do exceeding abundanily
beyond all that we ask or think, according to the power that
works within us, to Him be the glory in the church and in
Christ Jesus to all generations forever and ever. Amen (Ephe-
sians 3:14-21).

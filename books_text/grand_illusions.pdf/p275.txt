THIRTEEN

IDOLS FOR DESTRUCTION:
A STRATEGY FOR
PROPHETS AND PRIESTS

in hoc signo vinces*

 

Human affairs are moved from deep springs. A spirit moves them. It is by the
acceptation, the denial, and the renewal of philosophies that this society of immortal
mortals is marked, changed or restored.”

Fiilatre Belloc

Sand was blowing in off the desert, carried into the city by
freak Santa Ana winds. Carbon arcs, mounted on the roofs of
several police cars, threw the cracked roadbed into stark relief
and made the sand look like static on a video screen.

Reba Elvin stared at the surreal scene, blinking back her
tears, Her husband, her son, her pastor, and her Sunday School
teacher had all just been arrested. For praying outside a Planned
Parenthood clinic.

“I was in shock,” she told me later. “To think that in America
you can be handcuffed and dragged off to jail for praying on a
sidewalk! It’s frightening. Where will all this end?”

Unfortunately for Reba and the members of her small Presby-
terian church, it didn’t end that day with a trip to jail. Three days
later, several pro-abortion groups, including Planned Parenthood,
asked a federal judge to revoke the church’s tax-exempt status, to
force the pastor to hand over the church’s files and financial records,
and to restrain the church’s members “and all those acting in con-
cert with them” from any further public protests against abortion 3

Planned Parenthood’s spokeswoman told the press the
church was “in violation of IRS restrictions on political activity

235

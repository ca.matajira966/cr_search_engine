258 Granp ItLusions

might be found, public and private, civil and congregational.'*
The church was to encourage the magistrates, pray for them,
support them, instruct them, and advise them.'5 In other words,
the church was to serve as society’s prophet and priest.

The American system was, thus, set up as a decentralized,
confederated, and republican social structure.** It followed the
Biblical order of multiple jurisdictions, separate but cooperat-
ing, under the sovereignty of God and the rule of His Law.!7

The current notion of the wall of separation between church
and state is, thus, a far cry from what our Founding Fathers
intended: 18 It is, in fact, a denial of the multiplicity of institu-
tions and jurisdictions. It cripples the church and exalts the
state.19 It asserts the absolute authority of the state and ex-
cludes believers from participation in the cultural, social, and
political processes.?°

The wall of separation idea was slow to catch on in our nation.
Until the War Between the States erupted, Christianity was uni-
versally encouraged at every level of the civil government.#!
Then, in 1861, under the influence of the radical Unitarians, the
Northern Union ruled in the courts that the civil sphere should
remain “indifferent” to the church.*? After the war, that judg-
ment was imposed on the Southern Confederation as well.23
One hundred years later, in 1961, the erosion of the American
system of checks and balances continued with the judicial decla-
ration that all faiths were to be “leveled” by the state.2+ By 1963,
the courts were protecting and favoring a new religion —Ahuman-
ism had been declared a religion by the Supreme Court in 1940—
while persecuting and limiting Christianity.25 The government in
Washington, much to the delight of groups like Planned Parent-
hood, began to make laws “respecting an establishment of re-
ligion” and “prohibiting the free exercise thereof.” It banned
posting the Ten Gommandments in school rooms, allowed the
Bible to be read in tax-supported institutions only as an histor-
ical document, forbade prayer in the public domain, censored
seasonal displays at Christmas, Easter, and Thanksgiving,
regulated church schools and outreach missions, demanded
IRS registration of religious institutions, and denied equal ac-
cess to the media for Christian spokesmen.’ In short, it has
stripped the church of its jurisdiction and dismantled the insti-
tutional differentiation the Founding Fathers were so careful to

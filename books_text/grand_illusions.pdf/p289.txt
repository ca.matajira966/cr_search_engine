Idols for Destruction: A Strategy for Prophets and Priests 269

One way or another, we need to recapture the media. We
need to serve it as prophets and priests, guiding and guarding
the land.

Local Governments

Local governments are truly the nuts and bolts of the Ameri-
can system. Without the administration of cities, counties, par-
ishes, districts, precincts, zones, commissions, municipalities,
and states, civil stability would surely disintegrate. The Found-
ing Fathers of our nation recognized that, and the system of gov-
ernment that they designed was purposefully decentralized and
localized.57 They wanted to make certain that the villages,
townships, communities, and states shaped the policies of the
nation and the priorities in Washington, not vice versa.

Napoleon once asserted that the way to capture a nation is
not to “storm the palaces,” but to “capture the countryside.” He
was right.

The impact of Planned Parenthood has been enhanced as
much by local zoning ordinances, health codes, and funding ap-
praisals as it has by court rulings and legislative packages. There
is a basic principle at work here —not just a constitutional princi-
ple, but a Biblical principle: whoever will be faithful with “few
things” and “small things,” will be made master over “many
things” and “great things” (Matthew 25:14-30).

If we are going to serve society as prophets and priests,
we are going to have to participate in, and influence, our local
governments.

There are several things that each of us can do to do just that,

First, we can attend meetings. Again, it is the small meeting
that determines the course of community life: city council meet-
ings, county health board meetings, utility district meetings, tax
appraisal meetings, county development meetings, and environ-
mental impact meetings. Only a tiny handful of people— about
one tenth of one percent of the registered voters— attend those
meetings on a regular basis. Only about two percent of the elec-
torate ever attend such meetings. Needless to say, that minuscule
minority has an inordinate amount of influence over the day-to-
day administration of their local governing bodies. So why not
be a party to that powerful minority? Why not sit on a few com-

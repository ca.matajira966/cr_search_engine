280 Granp ILLustons

That makes for a very dangerous situation. Who monitors
the bureaucracy? Who checks and balances its administration?
Who even knows what it does?

When section 1008 of the Title X appropriations bill for pop-
ulation control went into effect, the bureaucratic minions simply
ignored it and did whatever they jolly well pleased. Section 1008
stated that no funds could be issued to “any program or agency
where abortion is utilized as a method of family planning.”
Despite that clear injunction, the Department of Health and
Human Services has poured millions of dollars into the coffers of
Planned Parenthood. Thus, the bureaucracy has illegally pro-
vided more than half of the budget for Planned Parenthood’s pro-
gram of death, deception, and perversion, out of our tax dollars.

Because the bureaucracy is unmonitored and unchecked, it
is above the law. Quite literally.

If we are going to serve society as prophets and priests, guid-
ing and guarding the land, we are going to have to bring the
bureaucracy back under control.

There are several things we can do to do just that.

First, we can call for the enforcement of existing laws. Every
day, the bureaucracy publishes several hundred new pages of
rules, regulations, statutes, policies, orders, directives, and re-
strictions that carry the force of law. But, all too often, these new
ordinances in the Federal Register are contrary to existing law.
They are nothing but an attempt to anonymously end-run the
legislature and the courts.

If we could monitor both the Federal Register and the policy
programs that go unpublished, holding the various civil servants
accountable to existing law, the humanistic loopholes would be
virtually closed. Planned Parenthood would lose its tax funding.
The administration would, at long last, be returned to the sanity
of a standard of law. Why not specialize in a particular area of
the law and then hold the bureaucracy accountable to it? Why
not turn the screws and actually make the system work the way
it is supposed to work? If we don’t make it work, no one will.

Second, we can hold bureaucrats accountable for their ac-
tions. The great advantage of most civil servants over elected
magistrates is the cover of anonymity, No one knows who they
are, They are nameless and faceless. Thus, they are able to

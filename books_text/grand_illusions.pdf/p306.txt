286 Granp Iniusions

the system to promote Scriptural resistance and positive
reclamation (1 Kings 18:3-16).

Jeremiah counseled the people of Israel to submit to the to-
talitarian rule of Nebuchadnezzar. Many commentators have
taken this to be an example of humility and subservience, but
Jeremiah actually exercised a tactic of Scriptural resistance. It is
what we might call the wounded lamb approach. In contrast to the
wounded water buffalo strategy so articulately advocated by his
contemporary Hananiah, Jeremiah’s plan was to humiliate the
invaders through the Godly goading of their own consciences
(Jeremiah 44:1-30). Ezekiel, too, avoided civil disobedience in
his struggle against tyranny. His resistance tactic involved
graphic public protest. He took his prophetic. message to the
streets and therein garnered both public and divine favor (Eze-
kiel 4:1-5:17).

Each of these heroes of the faith was forced by circumstance
to work for cultural reform. But they all understood that when
the messianic state begins to violate its divine trust, there are still
a wide variety of tactical options available to them short of civil
disobedience. Like them, we can work within the system with
the wise appeal, lawyer delay, lobbying, legislative reform,
suffering servanthood, and public protest. Even when faced with
awful debilitating oppression, believers should draw on these al-
ternative avenues of resistance before resorting to more dire
strategies. A veritable arsenal of Scriptural tactics has been sup-
plied to the believer in order to stay him from the last resort of
rebellious confrontation.

Scriptural respect for divine institutions must forever remain
foremost in our minds (Romans 13:1-7; 1 Peter 2:13-25). Though
tyranny may incline us toward libertarian activism, though god-
lessness may provoke grief in our bowels of compassion, though
the barbarism of Planned Parenthood may rankle our wrathful
ire, believers have a Scriptural mandate to do God’s work, God’s
way, in God’s time. Until the tactics of the wise appeal, lawyer
delay, lobbying, legislative reform, suffering servanthood, and
public protest have been exhausted and entirely frustrated, civil
disobedience is not a live option. Again, we must say civil dis-
obedience is a last resort. There is no ground whatsoever in the
Biblical narrative for skipping ahead to the drastic when the
mundane might just as well do. To advocate civil disobedience

298 Granp ILLusions

Jesus for good works, which God prepared beforehand that we
should walk in them (Ephesians 2:8-10).

God saves us by grace. There is nothing we can do to merit
His favor. We stand condemned under His judgment. Salvation
is completely unearned—except by Christ—and undeserved —
except to Christ. But we are not saved capriciously, for no reason
and no purpose. On the contrary, “We are His workmanship,
created in Christ Jesus for good works.” We are “His own posses-
sion,” set apart and purified to be “zealous for good deeds.” Word
and deed are inseparable. Judgment is answered with grace.
Grace is answered with charity. This is the very essence of the
Gospel message.

So, Paul tells Titus he must order his fledgling ministry
among the Cretans accordingly. He himself was “to be a pattern
of good deeds” (Titus 2:7). He was to teach the people “to be
ready for every good work” (Titus 3:1). The older women and
the younger women were to be thus instructed, so “that the
Word of God might not be dishonored” (Titus 2:5). And the
bondslaves, “that they might adorn the doctrine of God our
Savior in all things” (Titus 2:10). They were all to “learn to main-
tain good works, to meet urgent needs, that they might not be
unfruitful” (Titus 3:14). There were those within. the church
who professed “to know God, but in works they denied Him,
being abominable, disobedient, and disqualified for every good
work” (Titus 1:16). These, Titus was to “rebuke . . . sharply,
that they might be sound in the faith” (Titus 1:13). He was to
affirm constantly, “that those who believed in God would be care-
ful to maintain good works” (Titus 3:8).

As a pastor, Titus had innumerable tasks that he was respon-
sible to fulfill. He had administrative duties (Titus 1:5), doctrinal
duties (Titus 2:1), discipling duties (Titus 2:2-10), preaching
duties (Titus 2:15), counseling duties (Titus 3:12), and arbitrat-
ing duties (Titus 3:12-13). But intertwined with them all, funda-
mental to them all, were his charitable duties. Almsgiving was to
be central to his task.

What was true for Paul and Titus in the first century is just
as true for us today, for “these things are good and profitable for
all men” (Titus 3:8).

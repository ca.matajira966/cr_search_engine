44 Granp ILLUSIONS

Suddenly, I was confronted with the central anomaly of these
modern times: a liberal tust for life, a lavish love of life, a lux-
uriant litany of life, and yet, simultaneously, a leaden loathing of
life. I was struck by the complex absurdity of our cultural dance:
a compulsive rehearsal of the rite of life confused and confounded
by a chronic denial of the right to life.®

Back in my hotel room, alone with my thoughts, a haunting
refrain rang in my ears:

We must cry out for the young
How long must this crime go on?
Until we see

The Church in unity?

We must cry out for the young
Sound the warning, make it strong
And move as one

The time has come.

The time has come.’

I caught myself pining, Maudlin moments.®

I shook the mood before it took hold, though. I decided to
put on my “Matt Scudder Cap”? and do a bit of research.!° The
time had come.

It didn’t take me long to get the ball rolling, even at that late
hour. I perused the phone directory. I made several calls to hos-
pitals, pathology labs, disposal services, and emergency clinics —
adding to my hotel bill an obscene fifty cents apiece, And I asked
a few key questions of a few key people."

Within half an hour I knew with a fair amount of certainty
what I would find in the morning. It would not be pleasant.

A shiver went up and down my spine.

In the Belly of the Beast
Dawn broke tawny as a lion and somnolent as a hearthside
tom, A belvedere weekend, teal true and rumor red, beckoned
through the hotel window sheers.
I hastily went through my regular morning ritual: shower,
shave, devotions, and a frantic search for my wallet. I always try
to put my money in a “safe place” when I'm away from home.

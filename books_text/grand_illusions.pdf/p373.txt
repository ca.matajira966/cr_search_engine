135.

136.

137.

138.
139.
140.

i4t.
142.
143.
144,

145,

146.

147,

148,

149.

150,

End Notes 353

“The Effects of Sex Education on Adolescent Behavior,’ Family Planning Perspec-
fives 18:4 (July/August 1986), pp. 162-169,

Sce Jacqueline R, Kasun, “Teenage Pregnancy: Media Effects Versus Facts,”
American Life League, 1986; Fred Glahe and Joseph Peden, The American Fam-
ily and the State (San Francisco: Pacific Institute for Public Policy Research,
1986); “Eleven Million Teenagers: What Can Be Done About the Epidemic of
Adolescent Pregnancy in the United States?,” Planned Parenthood Federation
of America, 1976; and Wattleton, pp. 3-4, 60, 86.

See, for example, the tremendous reception that alternative programs for the
public schaols have received: like Sex Respect (Respect, Inc., P.O, Box 349;
Bradley, IL 60915) and Why Wait? (Josh McDowell Ministry, P.O. Box 1000,
Dallas, TX 75221); also note the success of alternative books like Connie
Marshner’s Decent Exposure (Brentwood, TN: Wolgemuth & Hyatt Publishers,
1988) and Josh McDowell's How to Help Your Child Say “No” to Sexual Pressure
(Waco, TX: Word Books, 1986).

Roberta Weiner, ed., Taen Pregnancy: Impact on the Schools (Alexandria, VA:
Capitol Publications, 1987), p. 17.

Ibid., pp. 17, 24.

Wendy Baldwin, Adolescent Pregnancy and Childbearing Rates, Trends, and Research
Findings from the CPR-NICHD (Bethesda, MD: Demographic and Behavioral
Science Branch, NIGHD, 1985), p. 5.

Weiner, p. 10.

Harris, pp. 8, 18, 60; Rutf, p. 48; and Marsiglio and Mott, p. 141.
Marshner, p. 42.

“Celebrating,” pp. 3, 18-21; also see Alan F. Guttmacher, Pregnancy, Birth, and
Family Planning (New York: Signet, 1973), pp. 163-75; and Alan F. Guttmacher
and Irwin H. Kaiser, Pregnancy, Birth, and Family Planning (New York: Signet,
1986), pp. 203-20; 463-64

See the testimony of several veterans of both the back alley and Planned
Parenthood in John T. Wilson's Abortion and Repentance (Los Angeles: Advocates
for Life Press, 1979), pp. 19-23, 36-39, 44-46.

See Ruff, pp. 36-42.

The VPT Consent Form for Planned Parenthood of Houston and Southeast
Texas speaks of the “inherent risks” in abortion procedures, The Minor Consent to
Abortion Form informs the applicant that: “complications could develop that
might require hospitalization” and speaks of “major complications” and “severe
consequences” of abortion.

Not aif abortions are legal, of course. In fact, according to a number of studies,
criminal and self-inflicted abortions did not decrease following liberalization of
abortion statutes. See L. Huldl, “Outcome of Pregnancy When Legal Abortion
is Readily Available,” Lancet, March 2, 1968, pp. 467-468; and Thomas W.
Hilgers and Dennis Horan, Adortion and Social Justice (New York: Sheed and
Ward, 1972).

See Thomas W. Hilgers, Dennis Horan, and David Mall, ed., New Perspectives
om Human Abortion (Frederick, MD: Aletheia Books, 1961), pp. 69-150; Jeff Lane
Hensley, The Zero People (Ann Arbor, MI: Servant Books, 1983), pp. 97-105;
and Susan M. Stanford and David Hazard, Will I Cry Tomorrow? (Old Tappan,
NJ: Fleming H. Revell Co., 1986); also see George Grant, The Dispossessed:
Homelessness in America (Westchester, IL: Crossway Buoks, 1986), pp. 74-75; and
Wilke, pp. 90-131.

‘The New American, January 20, 1986.

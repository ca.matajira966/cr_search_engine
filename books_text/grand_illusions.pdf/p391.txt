78.

79.
. Josh McDowell and Dick Day, Why Wait? What You Need to Know About the Teen

al,

82,
83.

£28

92.
93.

94.
95,
. Hbid., p. 257.
97.
. Tristram Gylberd, The Qaken Rithel: An Epic Poem (New York: Saxony

101.

102.
403.

104,
105,

106.
107,

End Notes 371

See Robert Ruff, Aborting Planned Parenthood (Houston: New Vision Books,
1988).
Mosbacker, p. 75.

Sexuatiy Crisis (San Bernardino, CA: Here’s Life Publishers, 1987), p. 23.
Melvin Zelnik and John Kantner, “Sexual Activity, Contraceptive Use, and
Pregnancy among Metropolitan Arca Teenagers: 1971-1979," Family Planning
Perspectives, Volume 12, Number 5, September/October 1980, pp. 233-234.
McDowell and Day, p. 23.

Judie Brown, “Planned Parenthood: The Deception,” A.L.L. About Issues,
February 2985, p. 25.

. David Chilton, Power In the Blood: A Christian Response to AIDS (Brentwood, TN:

Wolgemuth & Hyatt Publishers, 1987), p. 50.

. Zeinik and Kantner, pp. 233-234.
86.

See, for instance, Wilham Marsiglio and Frank Mott, “The Impact of Sex
Education on Sexual Activity, Contraceptive Use, and Pre-Marital Pregnancy
Among Teenagers,” Family Planning Perspectives, Volume 18, Number 4, July/
August 1986, p. 151; Zelnik and Kantner, pp. 230-237; Douglas Kirby, “Sexual
ity Education: A More Realistic View of Its Effects,” Journal of School Health,
Volume 58, Number 10, December 1985; and Scott Thompson, “Sex Educa-
tion: A Charade,” San Diego Union, April 27, 1981.

. Thompson, p. 15.
. “Planned Parenthood-Style Sex Education,” American Bureaux of Educational

Research Newsletter, Fall 1981, p. 2.

. Weiner, p. 30.
. Ibid, pp. 19-20.
. Lana D, Muraskin, “Sex Education Mandates: Are They the Answer?” Family

Planning Perspectives 18:4 (July/August 1986), p. 173.

Mosbacker, p. 77.

Quoted in Harold Tribble Cole, The Coming Terror: Life Before the Great War (New
York: Languine Brothers Publishers, 1936), p. 92.

George Orwell, 1984 (New York: Harcourt Brace Jovanovich, 1949).

Tbid., p. 251.

Cole, p. 92.
Limbarte, 1972), p. 6.

. Cole, p. 92.
100.

Allan Bloom, The Closing of the American Mind (New York: Simon & Schuster,
1987).

See Robert G. Marshall, The Tiwo Faces of Planned Parenthood (Stafford, VA:
American Life Education and Research Trust, 1984); and “Sexuality Alphabet,”
pp. 3-4.

Luke 19:12-27.

“Would You—Could You—Can You?” Discussion Group Guide, Planned
Parenthood of West Central Ohio, 1986, p. 2; “Sexuality Alphabet,” pp. 4-7;
and Alcorn, p. 73.

Colossians 3:5-10.

“Guidelines for Discussion-Based Sexuality Education,” Memorandum 2/6/64,
Planned Parenthood-Essex County, p. 1; and “Sexuality Alphabet,’ p. 7.
Acts $7:10-12.

Bulay and Garcia, Session V, “Values; and “Sexuality Alphabet,’ pp. 5-7.

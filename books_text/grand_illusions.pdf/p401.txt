End Notes 38t

Chapter 9—The Camera Blinked: The Media Legacy

1.
2.
3.

»

1.
12,
13.
14.

15.

16.
17,

18.
19,
20.
21.
22.
23.
24,

25.
26.
27.
28.

29.
30.

32,

“In order to win the masses.”

Hilaire Belloc, The Path to Rome (New York: Penguin Books, 1958), pp. 126-127.
Peter Prichard, The Making of McPaper: The Inside Story of USA Today (Kansas
City: Andrews, McMeel, and Parker, 1987), p. xiii.

. See George Grant, The Changing of the Guard: Biblical Principles of Political Action

{Ft. Worth: Dominion Press, 1987).

. Rael Jean Isaac and Erich Isaac, The Coercive Utopians: Social Deception by

America’s Power Players (Chicago: Regnery Gateway, 1983), p. 251.

. Alvin Toffler, Future Shock (New York: Bantam Books, 1971), p. 155.
. Bid.
. E. F. Schumacher, Small Is Beautiful: Economics As If People Mattered (New York:

Harper and Row, 1975), p. 82.

|. James W. Sire, How to Read Slowly: A Christian Guide to Reading With the Mind

(Downers Grove, IL: Inter-Varsity Press, 1978), pp. 14-15.

. Allan Brownfield, “Journalists: How Much Gan We Trust Them?” Human

Events, May, 1987.
Bid.

Bid.

Ibid,

See Marvin Olasky, Prodigal Press: The Anti-Christian Bias of The American News
Media (Westchester, IL: Crossway Books, 1988); and Marvin Olasky, 6 ead Beat:
The Press and Abortion, 1838-1988 (New York: Lawrence Erblum Associates,
1988); also see Colonel V. Doner, A Christian Parent’s Guide to TV (Shreveport:
Huntington House, 1988),

Marlin Maddoux, Free Seach or Propaganda? How the Media Distorts the Truth
(Nashville, TIN: Thomas Nelson, 1990), p. 13,

Alvin Toffler, Future Shack (New York: Bantam Books, 1971}, p. 155.

S, Robert Lichter and Stanley Rothman, “Media and Business Elites,” Public
Opinion, October/November 1981, pp. 42-44.

IBid.; also see Olasky, Prodigal Press and Dead Beat.

  

Tid.

Franky Schacifer, A Time For Anger: The Myth of Neutrality (Westchester, IL:
Crossway Books, 1982), p. 27,

Public Opinion, October, 1981,

Los Angeles Times, July 1, 1990.

Ibid.

See John Naisbitt and Patricia Aburdene, Re-Jnventing the Corporation (New
York: Warner Books, 1985).

Values in Media Alert, Spring 1986, p. 2.

See John Naisbitt, Megatrends (New York: Warner Books, 1982); Marvin Olasky
argues in Prodigal Press that this decentralization trend will in the end mean a
reshaping of the national news centers in New York, but not their elimination.

. See Bernard N, Nathanson, The Abortion Papers: Inside the Abortion Mentality

(New York: Frederick Fell Publishers, 1983), pp. 7-109.
The Tennessean, February 20, 1988,

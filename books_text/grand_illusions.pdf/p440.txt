Research, detail, accuracy. Three obvious distinctions of George Grant's Grand IMlusions.
How can a society pass more laws to protect animals, but continue by the minute to kill
its own unborn? Exposing Planned Parenthood as a scam should be our goal, Today . . .
and everyday . . . until it is totally aborted!

—David T. Clydesdale, Music Composer & Conductor

 

George Grant has added yet another important and increasingly needed work to sub-
stantive pro-life literature. As the author of a recently published book on Planned
Parenthood, I welcome the updated, expanded version of Grand Hiusions, It includes
additional material to arm the activist with information to fight the evil of Planned
Parenthood.

— Douglas R. Scott, Author, Bad Choices: A Look Inside Planned Parenthood

 

Because George Grant is a superb historian as well as relentless reporter, Grand Hlusions
reveals the wuth that few know about Planned Parenthood’s origins— that its founder,
Margaret Sanger, was a vehemently anti-Christian, anti-marriage, anti-capitalist ad-
mirer of Nazi Germany's “race purification” program. Even more surprising, Grant
documents how Sanger’s radicalism, under a thin veneer, still guides Planned Parent-
hood today.

—Charles Hull Wolfe, Historian, Coral Ridge Ministries

 

If George Grant’s book Grand Mlusions is not quite Uncle Tom’s Cabin, it may
nonetheless have the same galvanizing effect on the pro-life movement as Stowe’s book
had on the antislavery movement—but without her wild sensationalism.

—Gary Whitby, Christianity Today

 

George’s writing always yields a double edged sword . . . through the power of lan-
guage and factual information. Add prayer and action to that... . who can stand?

—Ted Gerk, Kelowna Right to Life

 

‘We all know that evildoers flourish and prosper in darkness! George Grant’s Grand Hlu-
sions places the spotlight of righteous scrutiny upon the works of those involved in the
evil of Planned Parenthood.”

—John Clemens, News Director, USA Radio Network

 

George Grant has done an excellent job of cataloging the deleterious medical effects of
abortion— especially RU-486.

—Douglas G. Kay, President, National Institute for Healthcare Research

 

When I read the writings of George Grant, I am struck by his anointed ability to
analyze and discern his subjects. His depth of knowledge and clarity of thought will
impact your life and sharpen your views.

James Robison, President and Founder, LIFE Outreach International

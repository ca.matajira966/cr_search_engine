All that Glitters 25

The Pro-Choice Illusion

Planned Parenthood claims to advocate the freedom of women to
choose if and when they wiil have children, without government interfer-
ence.3* But that is an illusion.3>

Planned Parenthood is anything but a “pro-choice” organi-
zation. And it is anything dut a populist, non-intcrventionist
champion of liberty against governmental coercion.

The truth is that from its very inception, Planned Parenthood
has sought mandatory population control measures— measures
carefully designed to deny the freedom to choose.3* Over the
years it has proposed that our government implement such things
as “compulsory abortion for out-of-wedlock pregnancies,”3”
federal entitlement “payments to encourage abortion,”3* “com-
pulsory sterilization for those who have already had two chil-
dren,”3 and “tax penalties” for existing large families,*°

Although Planned Parenthood’s sterilization crusade has
only seen acceptance in the United States from time to time—
especially among the ill, the infirm, the poor, and the incarcer-
ated"! — most of its other coercive programs have been embraced
enthusiastically elsewhere around the globe.**

China, for example, has taken Planned Parenthood’s sug-
gestions to heart, launching a brutal, no-holds-barred, one-
child-per-couple policy.#® Nearly one hundred million forced
abortions, mandatory sterilizations, and coercive infanticides
later,*# Planned Parenthood continues to maintain that the com-
munist government's genocidal approach to population control
is a “model of efficiency.”# It has fought to maintain United
States funding of the Chinese operation,** and has continued to
increase its own funding and program support involvement‘?
despite widespread reports of human rights atrocities.

Similar draconian measures have been implemented: at
Planned Parenthood’s behest in dozens of countries throughout
the Third World.49 Providing many of these countries with
detailed restraints and quotas, suggested compulsory incentives
and disincentives, and assistance in circumventing public opin-
ion and moral opposition, Planned Parenthood has taken the
lead in the international campaign to crush the rights of women
to choose if and when they will have children.5¢

The slow advance of Planned Parenthood’s coercive pro-
grams has eroded freedom of choice in the United Siates as well.

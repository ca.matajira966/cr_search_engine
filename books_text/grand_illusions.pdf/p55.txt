All that Glitters 35

contraceptive pill as well. They have thus thrust the drug— which
they helped to develop along with the World Health Organiza-
tion, the United Nations Population Fund, the Rockefeller
Foundation, and Roussel-Uclaf—before a woe-weary world
with a hearty yo-heave-ho.

Etienne-Emile Baulieu, the chief developer of the drug and
an international spokesman for Planned Parenthood, says with
no little hyperbole, that it is, “the most important invention of
the twentieth century,” and that it therefore has been rightfully
“elevated to mythic status.”!63 Apparently his assessment is
something more than political posturing or personal braggadocio—
for he is by no means alone:

According to Patricia Ireland, the president of the National
Organization for Women—and another strong supporter of
Planned Parenthood — the drug is indeed, “symbolic of the battle
for women’s rights. It is the cornerstone of our future.”

Molly Yard, who was Ireland’s immediate predecessor at
NOW, agrees, saying that RU-486 is “a most critical drug.” Per-
haps even, “the most significant medical advance in human history
and the symbol of a brighter future for women everywhere.7!65

Paul Ehrlich, population researcher and author —as well as a
Planned Parenthood board member — asserts that it is the “medi-
cal breakthrough” that “women everywhere have been hoping
and praying for,”!66

Nita Lowey, a congresswoman from New York, claims it is
“an important medical innovation that could dramatically
enhance women’s privacy and health.”167

Eleanor Smeal, president of the Feminist Majority Founda-
tion, says that RU-486 is a “truly remarkable” drug that has
“amazing properties which hold tremendous promise for the
benefit of women.” Indeed, she bubbles, it is “an historic break-
through in medicine.”168

Syndicated columnist and Planned Parenthood mouthpiece
Ellen Goodman opined that, “RU-486 offers the best possibility
of muting the abortion conflict while at the same time protecting
privacy,716 That is a marriage that she apparently believes was
made in heaven.

But the most laudatory praise of all comes from Lawrence
Lader, longtime Planned Parenthood advocate and founder of
the National Abortion Rights Action League. He said:

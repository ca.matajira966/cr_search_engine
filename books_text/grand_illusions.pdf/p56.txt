36 Granp ILLusions

RU-486 presents a classic case of how scientific progress can
revolutionize our lives. Within the last century, the railroad
opened up Western America and became a major factor in turn-
ing the United States into an economic colossus. The elevator
was essential to the development of the skyscraper, the vertical
city, and the concentration of business and services in a unified
geographic area. The automobile gave us more than speed; it
opened up the suburbs and the possibility of combining a rural
or semi-rural lifestyle with employment in the central city. The
cathode-ray tube made television possible. Antibiotics and
other pioneering drugs extended our life-span and improved
the quality of these added years. But when it comes to making
an impact on our personal relationships, the science of control-
ling human reproduction must be considered unique. No other
development—not even the telephone, with its advantage of
bringing families and friends together—has so drastically
changed our lives.17

According to Lader, “With the development of RU-486,
scientific progress has reached a whole new stage.”!7!

It is difficult to argue with an invention that is touted as even
more significant than the railroad, the elevator, the automobile,
television, antibiotics, and the telephone —one that would put an
end to bitterness and strife and offer mankind a dazzling new hope.

The fact is, after a decade of feverish fine-tuning, RU-486 is
unreliable, unsafe, and utterly unremarkable.

For starters, it doesn’t work very well. That it has a stand-
alone failure rate of between fifteen and forty percent—and yet
is still taken seriously —is itself a marvel of modern medicine.172
Even with the addition of the prostaglandin, the failure rate is
abysmally high. One out of every twenty RU-486 abortions fail~
whereas only one in two hundied surgical suction procedures
need to be repeated.173

It is not at all safe or easy either. In a recent clinical study in
Britain, five hundred eighty-eight women were given abortions
with RU-486 combined with the prostaglandin gemeprost. Five
of the women bled so much that they required transfusions. One
hundred sixty-six of them needed narcotics to ease the pain.
Some one hundred fifty vomited, and another seventy-three
suffered diarrhea. Thirty-five failed to abort and had to undergo

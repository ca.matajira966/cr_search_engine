50 Granp Intusions

caucuses, getting acquainted with the foremost radicals of the
day: John Reed, Eugene Debs, Clarence Darrow, Will Durant,
Upton Sinclair, Julius Hammer, and Bill Haywood.33 She joined
the Socialist Party and attended all of its functions. She even vol-
unteered as a women’s organizer for Local Number Five, speak-
ing at labor organization meetings and writing for the Party
newspaper, The Cail.

By this time, virtually all the revolutionary elements of Amer-
ican political life had been unified in the Socialist Party: the Radi-
cal Republicans, the Reformist Unitarians, the Knights of Labor,
the Mugwumps, the Anarchists, the Populists, the Progressi-
vists, the Suffragettes, the Single Taxers, the Grangers, and the
Communists.34 From ten thousand members in 1901, it had
swollen to fifty-eight thousand by 1908, and more than twice that
number were recorded four years later. And its voting strength
was many times greater even than that, accounting for more
than six percent of all the votes cast in the national elections of
1912. When Margaret and William Sanger entered the fray that
year, the Party had elected twelve hundred public officials in
thirty-three states and one hundred and sixty cities, and it regu-
larly published over three hundred periodicals.3° Especially en-
ticing to Margaret was the fact that no other political movement
in American history had fought so consistently for women’s
suffrage, sexual liberation, feminism, and birth control.

While William was happy that Margaret had finally found a
cause that satisfied her. restless spirit, he gradually became con-
cerned that she was taking on too much too soon, Their apart-
ment was in a perpetual state of disarray. Their children were
constantly being farmed out to friends and neighbors. And their
time alone together was non-existent.

But then when Margaret fell under the spell of the militant
utopian Emma Goldman, William’s husbandly concern turned
to extreme disapproval. Margaret had gone from an arch-typical
“material girl’ to a revolutionary firebrand almost overnight.
And now she was taking her cues from one of the most contro-
versial insurrectionists alive. It was just too much.

Goldman was a fiery renegade who had close connections with
revolutionaries the world over: Bolsheviks in Russia, Fabians in
England, Anarchists in Germany, and Malthusians in France.

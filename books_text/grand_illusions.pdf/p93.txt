Back-Alley Butchers: The Medical Legacy 7

Dilatation and Curetiage (D@&C). This once-favored method of
abortion now accounts for only about five percent of all abortions
performed due to. its poor medical track record.*® The procedure
involves the insertion of a curette—a very sharp loop-shaped
knife—up into the uterus. The placenta and the child are then
dismembered and scraped out into a basin, In addition to all of the
complications of menstrual extractions and suction-aspirations, D&C
abortions also carry the risk of uterine perforation, hemorrhaging,
pelvic abscesses, genital tract infections, bowel lacerations,
and thromboembolism. *!

Jared McCormick took his girlfriend, Susie Glanze, to Planned
Parenthood for a pregnancy test late last year. “She was really
scared, and so was I,” he said. “I told her that we could go ahead
and get married. We were planning on it anyway. We'd just have
to move things up a little, is all. But she wanted me to finish up
with school first. So, there we were. At Planned Parenthood.”

‘The test was positive and Susie made an appointment for an
abortion the next Saturday. “I really went berserk,” Jared said. “I
was dead set against the abortion. I begged her to marry me and
keep our baby. But she wouldn’t listen.”

The doctors performed a D&C. There was profuse bleeding,
but since that is quite common with D&C abortions, the clinic
personnel didn’t think anything of it.

That was a terrible mistake. An hour later, Susie was still
hemorrhaging and had to be rushed to the nearest hospital
emergency room. There she was given two units of blood and
treated for severe lacerations of the cervix and uterus. It would
be almost two days later before she would be released.

“It’s amazing what can happen between two people in just a
couple days’ time,” Jared said. “Susie was so grieved over what
she’d done—over what they'd done —that she just couldn’t stand
to be with me any more. Just like that. It was all over between
us. I’m convinced that if she’d known how risky the operation
was we'd be together today. And our baby would still be alive.”

Dilatation and Evacuation (D&E). This particularly brutal
method of abortion is commonly used when pregnancies have
reached well into the second and third trimesters. Strips of lami-
naria—a spongy seaweed — are placed in the cervix to stretch it
open. A pliers-like pair of forceps is then used to crush the child’s

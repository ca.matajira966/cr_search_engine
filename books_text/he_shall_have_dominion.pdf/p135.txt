Introduction to Postmillennialism 89

Address to King Francis I of France, Calvin writes: “Our doc-
trine must tower unvanquished above all the glory and above
all the might of the world, for it is not of us, but of the living
God and his Christ whom the Father has appointed King to
‘rule from sea to sea, and from the rivers even to the ends of
the earth... . And he is so to rule as to smite the whole earth
with its iron and brazen strength, with its gold and silver bril-
liance, shattering it with the rod of his mouth as an earthen
vessel, just as the prophets have prophesied concerning the
magnificence of his reign.”®* This is not the language which is
commonly associated with eschatological pessimism, and it was
adopted by Calvin’s Puritan and postmillennial successors. They
had good reasons to see in Calvin a postmillennial optimism.

I have already mentioned the most important systematizer of
English postmillennialism, Thomas Brightman (1562-1607). In
addition to him, there was a growing and influential number of
English Puritans that held postmillennial views well before
Whitby, as a number of important historical works have amply
demonstrated.” We think of Thomas Goodwin (1600-1679),
John Owen (1616-1683), William Gouge (1575-1653), John
Cotton (1585-1652), Thomas Brooks (ca., 1662), James Renwick
(d. 1688), John Howe (1678), William Perkins (1558-1602), and
others. John Cotton’s The Churches Resurrection, or the Opening of
the Fift and Sixt Verses of the 20th. Chap. of the Revelation [sic.],

(Philadelphia: Westminster Press, 1960), 2:904; Murray, Puritan Hope, pp. 89ff; Greg
L. Bahnsen, “The Prima Facie Acceptability of Postmillennialism,” pp. 69-76; James
R. Payton, Jr., “The Emergence of Postmillennialism in English Puritanism,” Journal
of Christian Reconstruction 6:1 (Summer 1979) 87-106; Aletha Joy Gilsdorf, The Puritan
Apocalyptic: New England Eschatology in the 17th Century (New York: Garland, 1989).

92. John Calvin, Institutes of the Christian Religion, 1:12.

93. Toon, Puritans, the Millennium and the Future of Israel; Richard H. Popkin, ed.,
Millennialism and Messianism in English Literature and Thought 1650-1800 (Leiden,
Holland: Brill, 1988); Ball, Great Expectation. See also: the previous references to
historical works by Iain Murray, J. A. DeJong, James R. Payton, Greg L. Bahnsen, A.
J. Gilsdorf.

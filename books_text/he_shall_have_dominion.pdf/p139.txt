Introduction to Postmillennialism 93

faith. Postmillennialism is not a fringe eschatology. It has been
particularly influential in reformed circles, as the list on page
91 demonstrates.

When postmillennialism is properly defined, it expresses the
glorious hope of all of Scripture. When its advocates are care-
fully read, its antiquity and influence may be better understood.
The widespread confusion regarding postmillennialism’s na-
ture, origins, and advocates is to be lamented. The modern
Church, sapped of the power of hope, largely through poor
exegesis and a lack of an understanding of Church history, is
the weaker for it.

98 HE SHALL IIAVE DOMINION

men from God’s very mouth.”? God so carefully revealed His
Word to His prophets that it might be stated of the prophet: “I
will put my words in his mouth; and he shall speak unto them
all that I shall command him” (Deut. 18:18b*). Consequently,
a constant refrain of Scripture is, “Thus saith the Lord.”

Being the personally revealed Word of the Living and True
God, the Scriptures are inerrant in their original autographa in
anything they assert. This is as true in historical matters as in
spiritual.* Christ affirmed: “Thy word is truth” (John 17:17).
God’s wisdom is infinite and unsearchable.® There is no limita-
tion or imperfection in His knowledge (Job 37:16). He knows
all things fully and exhaustively.’ His Word is inerrant and will
always be demonstrated in history as such. Thus, the eschatolo-
gical prophecies of Scripture, when properly interpreted, must
absolutely come to pass.

If the Bible teaches that anything is true and to be expected,
then no matter how difficult for us to imagine, no matter how
strongly arrayed against it are the historical forces of Satan, we
must bow to the authority of Scripture. “With God all things
are possible” (Matt. 19:26). No historical or philosophical argu-
ment counterpoised against Scriptural revelation regarding
eschatological eventuation may be allowed to prevail in the
thought of the Christian. The fundamental framework of the

2. John Calvin, The Tustitutes of the Christion Religion (1559) 1:7:5.

3. Cf. Jer. 1:9, 17; John 14:26; Rom. 3:2; 1 Cor. 2:13; 1 Thess. 2:13. Although
it is true that the ultimate reference of Deut, 18:18 is to the Great Prophet, Jesus
Christ, it is also true that this reference involves all the divinely commissioned
prophets of Scripture, establishing and authorizing the prophelic line, See: Gentry,
The Charismatic Gift of Prophecy: A Reformed Response te Wayne Grudem (2nd ed.; Mem-
phis: Footstool, 1990), ch. 1.

4. Greg L. Bahnsen, “Inductivism, Inerrancy, and Presuppositionalism,” Evangel-
icals and Inerrancy, Ronald F. Youngblood, ed. (Nashville: Thomas Nelson, 1984), pp.
199-216.

5, Job 11:7-8; 37:5, 14, 23; Isa. 40:28; 55:10; Rom. 11:33-36.

6. Psa. 147:5; Prov. 15:3; Acts 15:18; 1 John 3:20.

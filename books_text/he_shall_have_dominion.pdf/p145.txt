The Revelation of Truth 99

Christian eschatology must be rooted firmly in the Bible if it is
to be realistic and true.

The Scriptures stand as absolute authority over man, provid-
ing a sure record of supernaturally revealed, propositional
truth. For instance, the apostles of the New Testament insisted
on the acceptance of their authority.? The commands of Scrip-
ture compel obedience for the believer, despite and against the
wisdom of man and the power of Satan. “The weapons of our
warfare are not carnal, but mighty through God to the pulling
down of strong holds,” therefore we are obliged to be about the
duty of “casting down imaginations, and every high thing that
exalteth itself against the knowledge of God, and bringing into
captivity every thought to the obedience of Christ” (2 Cor 10:4-
5). Indeed, Paul commands: “Be not conformed to this world:
but be ye transformed by the renewing of your mind, that ye
may prove what is that good, and acceptable, and perfect, will
of God” (Rom 12:2).

The Word of God is not only the theoretical foundation of the
Christian worldview, but is a practical revelation of Truth, serv-
ing as a motivation to action in terms of that Truth. It spiritual-
ly and intellectually equips the belicver for every task in every
realm of human endeavor. “All scripture is given by inspiration
of God, and is profitable for doctrine, for reproof, for correc-
tion, for instruction in righteousness: That the man of God may
be perfect, thoroughly furnished unto all good works” (2 Tim.
3:16-17).° Because of this the godly labor of the believer is “not
in vain in the Lord” (1 Cor. 15:58). When the prophetic data
of Scripture compel us to a particular historical hope and cer-
tain course of action, we are in error when we refuse them. We
also stand in sinful unbelief when we fearfully doubt them.

7. 1 Gor. 14:37; 2 Cor. 11:31% 13:2-10; Gal. 1:6-9; 1 ‘Thess. 1:5; 2:13; 2 ‘Thess.
2:13-15; 3:6-15; 2 Tim. 2:1 ff; 3:1 3ff Titus 1:9; 2 Pet. 1:12-2:3; 1 John 2:21-24.

8. See also Heb. 13:21; 2 Tim. 2:21.

9. Cf. 1 Cor. 10:13; 2 Cor. 9:8; Phil. 4:13.

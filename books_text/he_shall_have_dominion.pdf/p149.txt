The Revelation of Truth 103

about in a vacuum with nothing to “push” against; he operates
within the all-encompassing plan of God. It is up against the
plan of God that he gets his “footing.” God’s control of man,
however, is not “across the board,” as our control of another
would have to be. Rather it is a control that cuts across planes:
God above and man below, Such a control guarantees man true
significance (he is no automaton), while guaranteeing God’s
true sovereignty (all things issue forth under the direction of
His wise counsel).

God’s control governs both the evil acts of free moral
agents,” as well as their righteous acts."* The classic evidence
of this is Peter’s statement: “For truly against Your holy Servant
Jesus, whom You anointed, both Herod and Pontius Pilate,
with the Gentiles and the people of Israel, were gathered to-
gether to do whatever Your hand and Your purpose deter-
mined before to be done” (Acts 4:27-28).

Though our holy and righteous God is not implicated in sin,
nevertheless, He ordains it and controls it toward a good
end.” God not only ordains all the events of history, he has
also ordained the free moral agency of man and the secondary
causes associated with all events. The evil act is always that of
the individual agent performing it; nevertheless it is also always
under the all-controlling power of God. Those who act in an
evil manner thereby admit their own responsibility, despite
God’s ultimate control. Of course, the classic illustration of this
is in the crucifixion of Christ, which is definitely the most evil
act of history. Yet, it was prophesied and foreordained of God
(Acts 2:23; 4:26-28),

17, Gen. 50:20; Exo. 4:21; 9:12,16; Josh. 11:20; Jdgs. 9:23; 1 Sam. 2:25; 16:14;
2 Sam. 17:14; 24:1, 10; 1 Kgs. 12:11, 15; 22:20-23; Matt. 21:42; Luke 22:22; John
12:39-40; 17:12; Acts 3:18; 4:27-28; 13:27-29; Rom. 9:22; 11:8; 1 Pet. 2:8; 2 Pet.
2:12; Jude 4; Rev. 17:17.

18. John 15:5, 16; 1 Cor. 12:6; 15:10; Eph. 1:12; 2:10; Phil. 1:6; 2:12, 13; Heb.
13:21.

19. Greg L. Bahnsen, “The Problem of Evil” (Parts I and 11), Biblical Worldview
3:9 (Oct. 1991) and 3:11 (Dec. 1991),

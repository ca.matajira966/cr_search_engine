xviii HE SHALL HAVE DOMINION

half a century after its publication.’ Contrary to a widely held
opinion, Allis was a postmillennialist, not an amillennialist — a
true heir of the theology of the old (pre-1929) Princeton Theo-
logical Seminary, including its eschatology. This is why he was
so enthusiastic about Israel and the New Covenant. The book went
out of print in the late 1960's. It was reprinted jointly by P&R
and Geneva Divinity School Press in 1981. It is again out of
print. But this is not to say that it never was in print, which is
why the critics are wrong when thcy assert that there has never
been an exegetical case for postmillennialism.

They are wrong for many other reasons. There have been
many presentations of various aspects of postmillennialism over
the years. There is David Brown’s Christ’s Second Coming. Will It
Be Premillennial?, published in 1842 and reprinted in 1990.*
There is the postmillennial interpretation of Romans 11: the
conversion of the Jews, which will launch a great era of God’s
blessing on the Church. This interpretation has appeared re-
pceatedly in Calvinist expositions, such as in the commentaries
by Robert Haldane, Charles Hodge, and John Murray. There
is the huge Commentary on the Prophecies of Isaiah by Princeton
Seminary theologian J. A. Alexander, which is not well known
because of its enormous bulk and detailed argumentation.
There are the theological writings of Benjamin B. Warfield,
another Princeton Seminary theologian, who carried on the
Princeton eschatology until his death in 1921. There is Loraine

3. Charles C. Ryrie’s short book, Dispensationalism Today (Chicago: Moody Press,
1965), attempted a generation ago to refute Allis’ case for continuity in New Testa-
ment history — that is, no dispensational “secret” rapture in the midst of history, no
premillennial Second Advent of Christ prior to the final judgment - by citing ultra-
dispensationalism’s arguments for discontinuity in history. Then he used Allis-type
arguments for prophetic continuity in order to refute ultradispensationalism’s
arguments that the Church did not begin in Acts 2 or Acts 4, but later, after Paul was
called to minister to the Gentiles. For a more detailed consideration of the issues
raised by Ryrie, see my comments in Publisher’s Foreword, Greg L. Bahnsen and
Kenneth L. Gentry, Jr., House Divided: The Break-Up of Dispensational Theology (Tyler,
TX: Institute for Christian Economics, 1989), pp. xxiv-xxv.

4, Edmonton, Alberta; Canada: Still Water Revival Books (1882 edilion).

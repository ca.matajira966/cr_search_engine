8

THE HERMENEUTIC OF SCRIPTURE

Knowing this first, that no prophecy of Scripture is of any private inter-
pretation. (2 Peter 1:20)

An issue that has received much attention in the eschatologi-
cal debate among evangelicals is hermeneutics: the principle of
biblical interpretation. How are we to approach the prophecies
of Scripture? For instance, what are the historical expectations
of eschatological significance set forth by the Old Testament
prophets? Although I will not go deeply into hermeneutic
discussion,’ it is necessary that certain aspects of the debate be
highlighted. There are full-length books that more than ade-
quately set forth the principles of biblical interpretation.” Three
particularly relevant issues that I will consider are literalism,
preterism, and Israel.

1. For the most part my hermeneutic will be illustrated below in the actual
exposition of key passages in Part III: Exposition.

2. An excellent study in prophetic hermeneutics is Hans K. LaRondelle’s The
Israel of God in Prophecy: Principles of Prophetic Interpretation (Berrien Springs, MI:
Andrews University, 1983), even though he is premillennial (non-dispensational). See
also: Vern Poythress, Understanding Dispensationalisis (Grand Rapids: Zondervan,
1987). Milton Terry’s classic on the history of interpretation is also helpful: Biblical
Hermeneutics: A Treatise on the Interpretation of the Old and New Testaments (Grand
Rapids: Zondervan, [n.d] 1983).

146 HE SHALL HAVE DOMINION

14:10), and there is no reason to doubt but that this will be
literal and that the city by means of certain physical changes
shall be exalted above the surrounding hills”!” Of the “future”
battle of Gog and Magog, Ryrie suggests: “A cavalry in this day
of jets and atom bombs? It does seem unbelievable. But Ezekiel
saw the mighty army from the north coming against the land of
Israel on horses (Ezekiel 38:4, 15).”* Can anyone accept such
views as reasonable, especially since it is so easy to understand
these elements as figurative?

Ryrie gives three arguments for the literalistic hermeneutic.®
(1) “Philosophically, the purpose of language itsclf seems to
require literal interpretation. . . . If God be the originator of
language and if the chief purpose of originating it was to con-
vey His message to man, then it must follow that He, being all-
wise and all-loving, originated sufficient language to convey all
that was in His heart to tell man. Furthermore, it must also
follow that He would use language and expect man to use it in
its literal, normal, and plain sense.” (2) “[P]rophecies in the Old
Testament concerning the first coming of Christ — His birth,
His rearing, His ministry, His death, His resurrection — were all
fulfilled literally. There is no non-literal fulfillment of these
prophecies in the New Testament.”"° (3) “If one does not use
the plain, normal, or literal method of interpretation, all objec-
tivity is lost.”

Despite the vigorous assertions of dispensationalists, “consis-
tent literalism” is an impossible ideal. Consider the following
problems for the Ryrie-style consistent literalist.

7. Charles C. Ryrie, The Basis of the Premillennial Faith (Neptune, NJ: Loizeaux
Bros., 1953), p. 148.

8. Ryrie, The Living End, p. 54.

9. Ryrie, Dispensationalism Today, pp. 87-88.

10. See also: Charles L. Feinberg, Mitlenniatism: The Two Major Views (3rd ed,;
Chicago: Moody Press, [1936] 1980), p. 41; J. Dwight Pentecost, Things ta Come: A
Study in Biblical Eschatology (Grand Rapids: Zondervan, 1958), p. 10; Robert PB
Lightner, Last Days Handbook (Nashville: Thomas Nelson, 1990), pp. 126-127.

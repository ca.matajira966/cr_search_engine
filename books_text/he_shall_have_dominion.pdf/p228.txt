182 HE SHALL HAVE DOMINION

made him a little lower than the angels, And You have crowned
him with glory and honor. You have made him to have domin-
ion over the works of Your hands; You have put all things
under his feet” (Psa. 8:4-6).

The optimistic expectations of postmillennialism comport
well with God’s creational purpose as evidenced in the Cultural
Mandate. They highlight the divine expectation of the true,
created nature of man gua man. Postmillennialism expects the
world as a system (kosmos)'* to be brought under submission to
God’s rule by the active, sanctified agency of redeemed man,
who has been renewed in the image of God (Col. 3:10; Eph.
4:24), In other words, postmillennial eschatology expects in history
what God originally intended for history. It sees His plan as main-
tained and moving toward its original goal, but now on the new
basis of God’s sovereign and gracious redemption, Hanko’s
objection to postmillennialism’s employment of the Cultural
Mandate is rooted in a very deep sense of the genuine fearsome
power of sin. The postmillennialist, however, secs God’s contin-
uance of the Cultural Mandate, but upon a new principle: the
very real and even greater power of redemption in Christ.

The Post-Fall Expectation of Victory

The first genuinely eschatological statement in Scripture
occurs very early: in Genesis 3:15. In keeping with the progres-
sively unfolding nature of revelation, this eschatological datum
lacks the specificity of the order of later revelation. “Revelation
is the interpretation of redemption; it must, therefore, unfold
itself in installments as redemption does. . . . The organic prog-
ress [of redemptive revelation] is from seed-form to the attain-
ment of full growth; yet we do not say that in the qualitative
sense the seed is less perfect than the tree. . . . The truth is in-

16. Kosmos (“world”) is the Greek word (used in the New Testament) that is
expressive of the orderly system of the world; it is contrary to chaos. For a discussion
of this concept, see Chapter 12, especially pp. 263-68.

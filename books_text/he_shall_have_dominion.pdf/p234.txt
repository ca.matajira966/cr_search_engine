188 HE SHALL HAVE DOMINION

redemptive relation as the ongoing basis of the Cultural Man-
date, which is likewise necessary to eschatology.”

This covenant is established with God's people: the family of
Noah, who alone escaped the deluge by the grace of God.
Thus, this should not be deemed solely a common-grace cove-
nant, for it was directly made with God’s people (Noah’s fami-
ly), was established on the basis of grace and redemptive sacri-
fice (Gen. 6:8; 8:20-22), and is united with God’s other redemp-
tive covenants (cf. Hos. 2:18 with Gen. 6:20; 8:17; 9:9ff). The
Cultural Mandate, then, has an especial relevance to the func-
tion of God’s people in the world: the Noahic reaffirmation of
the Mandate is expressly made with God’s people, the “you” of
Genesis 9:1-12. On the basis of divine covenant, God’s people
are called to the forefront of cultural leadership, with the reli-
gious aspects of culture being primary.

In the Noahic covenantal episode, we also witness the objec-
tivity of God’s relationship with man: the world was judged in
history for its sin. The rainbow, which signifies God’s covenant
mercy, is established with Noah and all that are with him, and
their seed (Gen. 9:12).* This indicates that the world will be
protected from God’s curse through the instrumentality of the
Church (the people of God). This covenant is only made indir-
ectly with unbelievers, who benefit from God’s protection only
as they are not opposed to God’s people. Because of Gad’s love
for His people, He preserves the orderly universe (Gen. 8:20-
22). His enemies serve His people: common grace (Gen. 9:10b).

the command to “be fruitful and multiply” (Gen. 9:1, 7 with Gen. 1:28), and the
dominion concept (cf. Gen. 9:2 with Gen. 1:28).

2. O, Palmer Robertson, The Christ of the Covenants (Phillipsburg, NJ: Presbyterian
& Reformed, 1980), p. 111. He refers to L. Dequeker, “Noah and Israel. The Ever-
lasting Divine Covenant with Mankind,” Questions disputees d’Ancien Testament, Methode
et Theologie (Gembloux, 1974), p. 119.

3. James B. Jordan, “True Dominion,” Biblical Horizons, Special Edition (Dec.
1990), p. 1. CE Robertson, Christ of the Covenants, p. 111.

4, It seems that the rainbow did not exist prior to this time. Apparently, the
Flood was the first instance of rain (and the rainbow): Genesis 2:5-6.

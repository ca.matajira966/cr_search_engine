Consummation 277

When the Lord returns at His second advent, this event will
signal the end of history. Paul notes that when Christ returns,
the end will come. “Christ the firstfruits, afterward those who
are Christ’s at His coming. Then comes the end, when He
delivers the kingdom to God the Father” (1 Cor. 15:23-24a).
This is why the era we have been living in since the coming of
Christ is known as “the last days” (Heb. 1:1-2). There are no
other days to follow! The resurrection occurs at the “last
trump” (1 Cor. 15:52). Christ’s coming does not open a whole
new era of redemptive history, known as the millennium. Rath-
er, it concludes history.

Consequently, just as Christ was involved in the beginning of
history (John 1:3; Col. 1:16), so will He be gloriously involved
in the conclusion of history (1 Cor. 15:23-24), He is the “Alpha
and the Omega, the Beginning and the End” (Rev 1:8; cf. 1:11;
21:6; 22:13). (Below I will survey a few of the major concomi-
tant events associated with His second advent.)

A Dispensational Distortion

Scripture teaches that Christ’s eschatological return is a
singular, visible, glorious event. Dispensationalism, with its
systemic pandemonium, however, teaches multiple literal com-
ings of Christ from heaven to earth, with the initial one (the
“rapture”) being a secret coming: First Corinthians 15:51-52
“cannot refer to the Second Coming of Christ because that
event was not a mystery unrevealed in the Old Testament. The
reference is to something distinct, that is, the rapture of the
Church before the tribulation.” First Thessalonians 4:13-18
“speaks of the same event.””! “The rapture as expressed in 1

20. See next chapter for a discussion of the last days, pp. 324-328.

21. Charles C. Ryrie, The Basis of the Premiliennial Faith (Neptune, NJ: Loizeaux
Bros., 1953}, p. 133. Other dispensationalists agree: John F. Walvoord, Prophecy
Knowledge Handbook (Grand Rapids: Eerdmans, 1990), p. 481. J. Dwight Pentecost,
Thy Kingdom Come (Wheaton, IL: Victor, 1990), pp. 248-249 and Things to Come
(Grand Rapids: Zondervan, 1958), pp. 206-207. Thomas L. Constable, “1 Thessaloni-

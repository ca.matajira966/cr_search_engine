Consummation 289

Fourth, the resurrection is that which signals the destruction
of death: “But each one in his own order: Christ the firstfruits,
afterward those who are Christ’s at His coming. Then comes
the end, when He delivers the kingdom to God the Father,
when He puts an end to all rule and all authority and power.
For He must reign till He has put all enemies under His feet.
The last enemy that will be destroyed is death” (1 Cor. 15:23-
26). Clearly the “last enemy” is destroyed at “the end,” and
both occur in conjunction with the resurrection.

The Final Judgment

The covenant God of Scripture deals with men legally, reck-
oning to them their just desserts on the basis of their infractions
of His Law, which is a divine transcript of His holy charac-
ter. Man desperately needs salvation because he is a coven-
ant-breaker (Isa. 24:5; Hos. 6:2). He has transgressed God’s
Law (Gal. 3:10; Jms. 2:10), which calls forth God’s “righteous
judgment” (Rom. 1:32; 2:5; 2 Thess. 1:5). God’s judgment is
characteristically described by use of forensic terminology, such
as krinein (“to judge”), Arisis (“judgment”), and dikaios (“justifica-
tion”).

The Law of God is naturally the legal standard of judgment.
In the context of speaking of the day of judgment (Rom. 2:5),
Paul says: “when Gentiles, who do not have the law, by nature
do the things contained in the law, these, although not having
the law, are a law to themselves, who show the work of the law
written in their hearts, their conscience also bearing witness,
and between themselves their thoughts accusing or else excus-
ing them, in the day when God will judge the secrets of men by
Jesus Christ, according to my gospel” (Rom. 2:14-16; cf. Matt.
7:23; 13:41; Jms. 2:10-12). Although the temporal aspects of
God’s justice occur in history,®! there are complex providential

60, See discussion of the Law of God in Chapter 7, above.
61. Lev. 26:1 ff; Deut. 9:5; 28:1fF; Psa. 9:16f 37:28; 59:13; Prov. 11:5; 14:11; Isa.

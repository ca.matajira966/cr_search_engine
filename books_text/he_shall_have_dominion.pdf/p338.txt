292 HE SHALL HAVE DOMINION

judgment of God, who will render to each one according to his
deeds: eternal life to those who by patient continuance in doing
good seek for glory, honor, and immortality; but to those who
are self-seeking and do not obey the truth, but obey unrigh-
teousness; indignation and wrath.”

The simultaneity of the judgment is inescapable in Matthew
25:31-46, where we read (in part): “When the Son of Man
comes in His glory, and all the holy angels with Him, then He
will sit on the throne of His glory. All the nations will be gath-
ered before Him, and He will separate them one from another,
as a shepherd divides his sheep from the goats” (Matt. 25:31-
32). The judgment occurs when Christ returns, not hundreds
of years later. This general judgment is found elsewhere in
Scripture.

Although the judgment is one concomitant event encompass-
ing both the just and the unjust, there will be an order to it. It
seems that the wicked will be judged immediately prior to the
righteous, according to the order of events in Matthew 13:30,
41, 43 and Matthew 25:46. It is “as if, in some literal sense,
‘with thine eyes shalt thou behold and see the reward of the
wicked’ (Psalm 91:8).””°

Of course, the judgment of the righteous is not to condem-
nation, but is to future reward.”! And we should understand
that “the relation between our works and our future reward
ought, however, to be understood not in a mechanical but
rather in an organic way. When one has studied music and has
attamed some proficiency in playing a musical instrument, his

69, Dan. 12:2; Eccl. 12:13-14; Matt. 12:36; 13:41; Matt. 25:14-30; Acts 10:42;
Rom. 3:6; 2 Cor. 5:9-11; 2 Thess. 1:6-10; 2 Tim. 4:1; Heb. 10:27-31; Jude 14-15;
Rev, 20:11-15.

70. David Brown, “The Gospel According to S. Matthew,” A Commentary, Critical
and Explanatory on the Old and New Testaments, Robert Jamicson, A. Faussett, and
David Brown, eds., 2 vols. (Hartford: S. 8. Scranton, n.d.), 2:44.

71. Rom. 2:5-10; 1 Cor. 1:4-8; 3:8; 15:32, 58; 2 Cor. 4:16; 5:10; 9:6-8; Gal. 6:5-
10; Phil. 1:10, 26; 2:16; Col. 1:5; 3:24; | Thess. 3:13; 5:23; 2 Thess. 1:7; 1 Tim. 2:18;
4:8; 5:25; 6:18-19; 2 Tim. 2:11; 4:4-16.

 

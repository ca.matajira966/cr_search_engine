Consummation 297

time and then be allowed into the body of the redeemed.
This is a rare view.

Annihilationisis hold that the unrepentant will be punished to
elimination. Arnobius (ca. A.D, 327) was an ancient annihila-
tionist.°? Modern evangelical annihilationists include Philip E.
Hughes, John R. W. Stott, and Clark H. Pinnock.” Annihila-
tionists generally argue that only believers receive immortality,
as a consequence of their union with Christ.*’ A recent poll
among evangelical college and seminary students showed that
almost one-third agreed that “the only hope for heaven is
through personal faith in Jesus Christ except for those who
have never had the opportunity to hear of Jesus Christ.” This
has been interpreted by James Davison Hunter as agreeing with
Clark Pinnock’s second chance before annihilation viewpoint.”
We should note that unbelieving thought is a species of annihil-
ationism. Bertrand Russell, the famous atheistic philosopher,
held that man is simply an accidental collection of atoms, which
in the very nature of the case is annihilated.

Hell is the judicial outcome of God's covenantal curse upon
rebellious man, who is created with an ever-living soul. Hell
represents the final legal sanction upon all those who have re-

88. John Hick, Evil and the God of Love (New York: Macmillan, 1975). Hick was
once an evangelical theologian.

89. Arnobius, Against the Nations 2:14.

90. John R. W. Stott, Evangelical Essentials (Downer’s Grove, IL: InterVarsity
Press, 1988). “John R. W. Stott on Hell: Taking a Closer Look at Etcrnal Torture,”
World Christian 8 (May 1989) 31-37. Philip E. Hughes, The True Image: The Origin and
Destiny of Man (Grand Rapids: Eerdmans, 1989). Philip Edgcumbe Hughes was
relieved of his duties at Westminster Theological Seminary in the late 1980s due to
his denial of hell. Regarding Pinnock, see John H. Gerstner, Repent or Perish (Ligon-
ier, PA: Soli Deo, 1990) and Erickson, “Is Universalistic Thinking Now Appearing
Among Evangelicals?” 6.

91. D. A. Dean, Resurrection: His and Ours (Charlotte, NC: Advent Christian
General Conference of America, 1977), pp. 110M. Edward William Fudge, The Fire
That Consumes (Houston: Providential Press, 1982).

92. Cited by Erickson, “Universalistic Thinking,” 6. On the growing liberalism of
evangelicalism, see: James Davison Hunter, Evangelicalism: The Coming Generation
(Chicago: University of Chicago Press, 1988).

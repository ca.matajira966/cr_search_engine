326 HE SHALL HAVE DOMINION

be regarded as properly expressive of that... . It was a phrase
in contrast with the days of the patriarchs, the kings, the pro-
phets, etc. The last days, or the closing period of the world,
were the days of the Messiah.”*° His coming was “nothing less
than the beginning of the great eschaten of history.”**

It is when Christ came that “the fullness of times” was real-
ized: “The phrase pleroma tou chronou, Gal. iv. 4, implies an
orderly unrolling of the preceding stages of world-history to-
wards a fixed end.”®? Hence, the preparatory preaching at the
beginning of His ministry: “[{T]he time is fulfilled, the kingdom
of God is at hand” (Mark 1:15; Matt. 4:17). Prior to this, the
Old Testament era was typological and anticipatory, The Old
Testament era served as the “former days” (Mal. 3:4) that
gave way to the “last days,” the times initiated by Christ’s com-
ing: “God, who at various times and in different ways spoke in
time past to the fathers by the prophets has in these ast days
spoken to us by His Son, whom He has appointed heir of all
things” (Heb. 1:1-2),

Thus, we find frequent references to the presence of the last
days during the New Testament time. The last days are initiat-
ed by the appearance of the Son (Heb. 1:2; 1 Pet. 1:20) to effect
redemption (Heb. 9:26) and by His pouring out of the Spirit
(Acts 2:16, 17, 24; cf. Isa. 32:15; Zech. 12:10). The “ends of the
ages” came during the apostolic era (1 Cor. 10:11). These will
run until “the last day,” when the resurrection/judgment occurs
to end history (John 6:39; 11:24; 12:48). But before the final
end point is reached, perilous times will punctuate the era of
the end (2 Tim. 3:1) and mockers will arise (2 Pet. 3:3).

50. Albert Barnes, Barnes’ Notes on the New Testament, 1 vol. edition (Grand
Rapids: Kregal, [n.d.] 1962), p. 381.

51. Herman Ridderbos, The Coming of the Kingdom (Philadelphia: Presbyterian &
Reformed, 1962), p. 36.

52. Geerhardus Vos, The Pauline Eschatology (Phillipsburg, NJ: Presbyterian & Re-
formed, [1930] 1991), p. 83.

53. See: Jer. 46:26; Lam. 1:7; Amos 9:11; Mic. 7:14, 20.

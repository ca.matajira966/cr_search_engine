15

FEATURES

Now as He sat on the Mount of Olives, the disciples came to Him pri-
vately, saying, “Tell us, when will these things be? And what will be the
sign of Your coming, and of the end of the age?” (Matthew 24:3)

In this chapter, I turn to three of the familiar features of the
eschatological debate that I have not considered in detail so far.
The features I will deal with in this chapter are the Great Trib-
ulation, the rebuilding of the Temple, and the New Creation.

It is ironic that the biggest-sclling prophetic studies pub-
lished today deal with the horrible Great Tribulation.’ Despite
the prominence of the glorious millennium in the eschatological
debate, it seems that the Christian public has more interest in
the tribulational woes than in the millennial glory. What is
worse, the Great Tribulation is greatly misinterpreted — even
being placed at the wrong end of history.

In this book, space does not permit a thorough analysis of
the Great Tribulation? A truly preteristic approach to the

1. Hal Lindsey’s The Late Great Planet Earth (Grand Rapids: Zondervan, 1970) has
sold over 35 million copies in fifty-four languages, Recent books on the Gulf War and
the supposed looming Great Tribulation have sold millions.

2. For more detail, see: Gentry, The Abomination of Desolation: A Study in Eschatol-
ogical Evil (forthcoming).

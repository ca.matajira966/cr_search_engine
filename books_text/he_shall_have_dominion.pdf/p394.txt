348 HE SHALL HAVE DOMINION

great glory.” Futurists see these verses as “of particular impor-
tance” in demonstrating the error of preterism,” showing that
“this approach to 24:29-31 cannot be sustained.””

The darkening of the sun and moon is common apocalyptic
language for the collapse of nations, such as in Old Testament
judgments on Babylon (Isa. 13:1, 10, 19), Idumea (Isa. 34:3-5),
Israel (Jer. 4:14, 16, 23ff; Joel. 2:10-11), and Egypt (Ezek. 32:2,
7-8, 11-12). This interpretation of the apocalyptic language
of these passages is not exceptional. Even allegedly literalistic
dispensationalists can write of Isaiah 13:10: “The statements in
13:10 about the heavenly bodies . . . no longer function may
figuratively describe the total turnaround of the political struc-
ture of the Near East. The same would be true of the heavens
trembling and the earth shaking (v. 13), figures of speech sug-
gesting all-encompassing destruction.”*! He figured it out.

The final collapse of Jerusalem and the Temple will be the
sign that the Son of Man, whom the Jews rejected and cruci-
fied, is in heaven (Matt. 24:30)."* The fulfillment of His judg-
ment word demonstrates His heavenly position and power. This
causes the Jewish tribes of the Land (ge) to mourn (kopto, cf.
Luke 23:27-28). Through these events the Jews were to “see”
the Son of Man in His judgment-coming in terrifying cloud-
glory: clouds are symbols of divine majesty often entailing
stormy destruction (Isa. 19:1; cf. Psa. 18:10-14; Lam. 2:1; Ezek.
30:3-5). The members of the Sanhedrim and others would
experience such in their life times (Matt. 26:64; Mark 9:1; cf.
Rev. 1:7 with Rev. 1:1, 3).

28, Hoekema, Bible and the Future, p. 178.

29. ‘Turner, “Structure and Sequence of Matthew 24:1-41,” p. 19.

30. See: David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft.
Worth, TX: Dominion Press, 1985), pp. 98-100.

31. John A. Martin, “Isaiah,” Bible Knowledge Commentary: Old Testament, p. 1059,
See also; comments at Isa. 34:2-4 (p. 1084); Jer. 4:23-28 (p. 1136); Joel 2:2a, 10-11
(pp- 1415-1417).

$2. Chilton, Paradise Restored, pp. 100-101.

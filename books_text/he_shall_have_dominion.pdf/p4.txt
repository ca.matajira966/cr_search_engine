Copyright, Kenneth L. Gentry, Jr., 1992

 

Library of Congress Cataloging-in-Publication Data

Gentry, Kenneth L.
He shall have dominion / Ken Gentry
cm.
Includes bibliographical references and indexes.
ISBN 0-930464-62-1 (alk. paper) : $19.95
1. Eschatology. 2. Millennialism. 3. Postmillennialism.
4. Dominion theology. 5. Calvinism. 6. Reformed
Church — Doctrines.

I. Title
BT821.2.G46 1992 92-15890
236’ .9--dc20 CIP

 

Institute for Christian Economics
B O. Box 8000
Tyler, TX 75711

380 HE SHALL HAVE DOMINION

menced when John wrote Revelation,” Rome was stained with
the blood of the saints. But Rome had only recently entered the
persecuting ranks of God’s enemies. Throughout Acts, Jerusalem
is portrayed as the persecutor and Rome as the protector of
Christianity.** Interestingly, in the Olivet Discourse context,
Jesus reproaches Jerusalem: “Therefore, indeed, I send you
prophets, wise men, and scribes: some of them you will kill and
crucify, and some of them you will scourge in your synagogues
and persecute from city to city, that on you may come all the
righteous blood shed on the earth, from the blood of righteous
Abel to the blood of Zechariah, son of Berechiah, whom you
murdered between the temple and the altar. . . . Jerusalem,
Jerusalem, the one who kills the prophets and stones those who
are sent to her! How often I wanted to gather your children
together, as a hen gathers her chicks under her wings, but you
were not willing!” (Matt. 23:34-35, 37). Before his stoning,
Stephen rebukes Jerusalem: “Which of the prophets have not
your fathers persecuted? And they have slain them who showed
before of the coming of the Just One, of whom ye have been
now the betrayers and murderers” (Acts 7:51-52).

Paul warns of Jewish persecution: “For you, brethren, be-
came imitators of the churches of God which are in Judea in
Christ Jesus. For you also suffered the same things from your
own countrymen, just as they did from the Jews, who killed
both the Lord Jesus and their own prophets, and have persc-
cuted us; and they do not please God and are contrary to all
men, forbidding us to speak to the Gentiles that they may be
saved, so as always to fill up the measure of their sins; but
wrath has come upon them to the uttermost” (1 Thess. 2:14-
16).

37. Gentry, Before Jerusalem Fell.

38. See for example: Acts 4:3; 5:18-33; 6:12; 7:54-60; 8:1ff; 9:1-4, 13, 23; 11:19;
12:1-3; 13:45-50; 14:2-5, 19; 16:23; 17:5-13; 18:12; 20:3, 19; 21:11, 27; 22:30; 23:12,
20, 27, 30; 24:5-9; 25:2-15; 25:24; 26:21. See also: 2 Cor. 11:24; 2 Thess. 2:14-15;
Heb. 10:32-34; Rev. 2:9; 3:9; etc.

Preface xd

Yet I believe there is a system of biblical eschatology that has
in the past and will yet again demonstrate itself a valid force in
the development of world events. And that eschatology is post-
millennialism.

For the last fifty years many Christians (wrongly) deemed
postmillennialism a theologically dead issue.? It held too opti-
mistic a prospect for the future for those who lived in an era
that witnessed the rise of Communism and two World Wars.
But postmillennialism has begun to make headway once again
as a theologically credible alternative to the more popular es-
chatologies of despair. And it is important to realize that its
remarkable resurgence antedates the collapse of Soviet and
Eastern Bloc communism. These events cannot be laid down as
the psychological bases for the modern resurgence of postmil-
lennial optimism.

The market for works on eschatology is ripe. Many of the
best-selling Christian works in the last few years have dealt with
prophecy. In this work I hope to set forth compelling reasons
for a return to postmillennialism by evangelical Christians.
These reasons will be shown to be pre-eminently exegetical and
theological. For the Christian, exegesis and theology should
provide the basis of expectation for the future, not current
events.

I would like to thank several friends for assisting me in
proofreading the chapters: Tim Martin, Bill Boney, Edmond
Sandlin, and Kim Conner. Their friendship, assistance, advice,
and encouragement are much appreciated. They are Christians
who are persuaded that He Shall Have Dominion. Thanks also to
my son Stephen for spending several days helping me to dou-
ble-check direct quotations for accuracy.

wood, TN: Wolgemuth and Hyatt, 1991).
3. See discussion in Chapters 4 and 18.

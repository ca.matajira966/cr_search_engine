Characters 385

that Jesus was the Messiah (Acts 17:1-3). Though some Jews
believed Paul, others were riled to mob action regarding the
Christian message (17:4-5), They dragged “some of the breth-
ren to the rulers of the city” complaining: “These who have
turned the world upside down have come here too. Jason has
harbored them, and these are all acting contrary to the decrees
of Caesar, saying there is another king — Jesus” (17:6-7). After
taking security from Jason and the others, the civil rulers let
them go (17:9). This allowed Paul to depart safely to Berea.
The Jews were not so easily quieted, however, for “when the
Jews from Thessalonica learned that the word of God was
preached by Paul at Berea, they came there also and stirred up
the crowds” (17:13). This resulted in the immediate sending
away of Paul to Athens (17:14-15).

This explains the strong language against the Jews in the
Thessalonian epistles, and helps uncover some of the more
subtle concerns therein. In his first letter, he writes: “For you,
brethren, became imitators of the churches of God which are in
Judea in Christ Jesus. For you also suffered the same things
from your own countrymen, just as they did from the Jews,
who killed both the Lord Jesus and their own prophets, and
have persecuted us; and they do not please God and are con-
trary to all men, forbidding us to speak to the Gentiles that
they may be saved, so as always to fill up the measure of their
sins; but wrath has come upon them to the uttermost” (1 Thess.
2:14-16).

This Jewish context is important for grasping the situation
Paul confronts. I show in the exposition to follow that there are
a number of allusions to the Olivet Discourse. The Olivet Dis-
course speaks of the destruction of the Temple and the judg-
ment of the Jews for rejecting Jesus as the Messiah (cf. Matt.
23:35-24:2; cf. Acts 17:3; 18:5).’°

46, Page attempts to draw the parallel with Revelation 20, comparing the res-
traint and deception of Satan and the flaming coming of Christ there with the decep-

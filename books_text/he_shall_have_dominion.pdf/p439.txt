Characters 393

legitimate. When the characters highlighted above are carefully
studied in terms of their historical context, the prophecies
concerning their imminent appearance are shown to mesh well
with postmillennialism. This is largely due to the preteristic
conception of these characters — good (Elijah) and bad (the
Harlot) ~ in terms of a proper biblical hermeneutic. The histor-
ical Elijah had gone to his reward long before Jesus appeared;
he will not be back. The covenantal, baptizing Elijah has done
the same. So has the Harlot, Jerusalem, in A.D. 70, destroyed
by the corporate Beast, Rome. The personal Beast, Nero, had
gone to his reward the year before. Unlike out-of-print dispen-
sational prophecy books,®! they cannot be revived.

61. John F. Walvoord, Armageddon, Oil and the Middle East Crisis: What the Bible
says about the future of the Middle East and the end of Western Civilization (Grand Rapids:
Zondervan, [1974] 1990).

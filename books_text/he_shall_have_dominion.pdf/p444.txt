398 HE SHALL HAVE DOMINION

would be a cruel mockery of their circumstances for John to tell
them that when help comes, it will come with swiftness - even
though it may not come until two or three thousand years later.
Or tell them that the events are always imminent — even though
the readers of his letter may never experience them. Or that
God will send help soon - according to the way the Eternal
God measures time: just a few days, or perhaps millennia.

In addition, each of these approaches is destroyed by the
very fact that John repeats and varies his terms as if to dispel
any confusion. Think of it: If these words in these verses do not
indicate that John expected the events to occur soon, what words
could John have used to express such? How could he have said it
more plainly?

Date of Writing

The date of the writing of the Book of Revelation is certainly
pre-A.D. 70, and probably as early as A.D. 65-66. I will not
rehearse here the argument for this “early date” (as opposed to
A.D, 95-96), because I have dealt with this in depth in another
place.* But we do need to keep this in mind, because a large
portion of the prophecies in Revelation find fulfillment in the
era leading up to the destruction of the Temple, as I will show.

Revelational Theme

The theme of Revelation is set forth in Revelation 1:7: “Be-
hold, He is coming with the clouds, and every eye will see Him,
even those who pierced Him; and all the tribes of the earth will
mourn over Him.” This theme is easily applicable to Christ’s
jedgment-coming on first-century Israel.? This cloud-coming of
Christ in judgment is reminiscent of Old Testament cloud-

8. Kenneth L. Gentry, Jr., Before Jerusalem Fell: Dating the Book of Revelation
(Tyler, TX: Institute for Christian Economics, 1989).

9. For the different ways in which Christ is said to “come” in Scripture, see pp.
271-277, above.

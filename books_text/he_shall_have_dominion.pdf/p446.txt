400 HE SHALL HAVE DOMINION

But as awful as the Jewish loss of life was, the utter devasta-
tion of Jerusalem, the final destruction of the Temple, and the
conclusive cessation of the sacrificial system were lamented even
more. The covenanial significance of the loss of the Temple
stands as the most dramatic outcome of the War. Hence, any
Jewish calamity after A.D. 70 would pale in comparison to the
redemptive-historical significance of the loss of the Temple.

So then, the expectation of a judgment-coming of Christ in
the first century is easily explicable in terms of the biblical and
historical record. Thus, the point remains: John clearly expect-
ed the imminent occurrence of the events of Revelation.

Primary Focus

One of the most common terms of significance in Revelation
is the Greek word ge. It occurs cighty-two times in the twenty-
two chapters of Revelation. This word may be translated in two
ways: (1) “earth” (indicating the entire globe) or (2) “land”
(referring to a particular portion of the earth, such as the Pro-
mised Land). It would seem that the overwhelming majority of
occurrences of this term in the context of Revelation would
suggest its reference as being to “the Land,” i. e., the famous
and beloved Promised Land.” The reasons justifying such a
translation are as follows:

The very Jewish nature of Revelation suggests the plausibili-
ty of such a translation. The lexical and syntactical peculiarities
of Revelation are extremely Hebraic.” Furthermore, the first
occurrence of the term appears in the theme verse in Revela-
tion 1:7 and must mean the Promised Land (see previous argu-
ment). In addition, it is used later in ways strongly suggestive of

12. “Palestine was to the Rabbis simply ‘the land’, all other countries being
summed up under the designation of ‘outside the land.’ ” Alfred Edersheim, Sketches
of Jewish Social Life (Grand Rapids: Eerdmans, [1876] n.d.), p. 14.

13. See R. H. Charles, The Revelation of St. john, 2 vols. (Edinburgh: T. & T.
Clark, 1920), l:exvii-clix.

Revelation 41]

the wine of the wrath of her fornication.’ ” As noted in the
preceding chapter, “Babylon” stands for Jerusalem.

In Revelation 14:14ff, Israel is “harvested” in judgment. In
the days of Christ and the apostles, Israel became ripe for judg-
ment (Matt. 23:31-36; 1 Thess 2:16). The grucsome action
taking place here is spoken of as “outside the city,” i.e., outside
Jerusalem. This corresponds to Christ’s crucifixion “outside” the gate
or the city (Heb. 13:12-13; John 19:17), It also clearly relates the
scene to the area surrounding Jerusalem, i.e., the land of Israel.
The land of Isracl as a Roman province stretched from the
Leontes River to Wadi el Arish, a distance of 1,600 furlongs, or
about 200 miles (Rev. 14:20).

The blood flow to the horses’ bridles seems to be a poetic
description of the blood that covered the lakes and rivers dur-
ing several dramatic battles between the Romans and the Jews.
“But as many of these were repulsed when they were getting
ashore as were killed by the darts upon the lake; onc might
then see the lake all bloody, and full of dead bodies, for not
one of them escaped. And a terrible stink . . . as for the shores,
they were full of shipwrecks, and of dead bodies all swelled.”

The Seven Vials of Wrath

In Revelation 15, we have a vision of the saints in heaven
Just preceding the pouring out of the vials of wrath. Again, the
saints’ prayers of Revelation 6 are being answered.

These vials bring increasing woe (Rev. 16). The Roman
armies come with ease from the Euphrates (Rev. 16:12). Jose-
phus notes that “there followed [Titus] also three thousand,
drawn from those that guarded the river Euphrates” (Wars
5:1:6). The Roman soldiers were supplemented with troops
provided by auxiliary kings from the east (Rev. 16:12; Wars
3:4:2; 5:1:6). With the convergence of so many traincd soldicrs,

43. Josephus, Wars 3:10:9; 4:7:5-6; 6:8:5.

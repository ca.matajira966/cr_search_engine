430 HE SHALL HAVE DOMINION

be the case that the sales figures for Lindsey’s book are indica-
tive of this decline!) But has this decline completely wiped out
centuries of Christian progress? The answer clearly is that it has
not. Will our present slippage and decline continue and spread
into total apostasy and chaos to a point beneath that of the first
century? Of course, we cannot answer this today on the basis of
historical analysis. Answering this question is the task of the
Christian exegete who recognizes that God’s Word “shall not
return to Me void, but it shall accomplish what I please, and it
shall prosper in the thing for which I sent it” (Isa. 55:11). It is
the point of this book to demonstrate that God’s will is for the
redemption of the world as a system in the historical long-run and
before the physical return of Christ in final judgment.

Of course, the postmillennialist can turn the tables on the
pessimistic historical short-run argument. Consider the collapse
in 1991 of Communism in Russia and Eastern Europe. As re-
cently as 1987, anti-postmillennialists would have scoffed at the
very notion of the collapse of Communism - a threat to their
eschatologies. Twenty years ago, dispensationalist Gannet saw
the Second Advent as the only hope for the overthrow of Com-
munism and other forms of political oppression: “What peace
of mind this brings to Christians as the end time approaches.
What a cause for rejoicing that righteousness, not. Russia, shall
ultimately triumph. This triumph of Christ over Communism
emphasizes the folly of getting side-tracked in spending our
time primarily in opposing Communism rather than in an all-
out proclamation of the gospel of grace.”" Also keep in mind
Adams’ comment, which obviously had the Communist Soviet
Union in view: Postmillennialism “is spurned as highly unrealis-
tic because it predicts a golden age around the corner in a day
in which the world nervously anticipates momentary destruc-

10, Alden A. Gannet, “Will Christ or Communism Rule the World?” Prophecy and
the Seventies, Charles Lee Feinberg, ed. (Chicago: Moody Press, 1971), pp. 64-65.

434 HE SHALL HAVE DOMINION

ly inclined, but they should have no bearing upon the theologi-
cal argument in light of the above observations. Abram was old
and childless when the Lord promised Him an innumerable
seed (Gen. 15:5). He even died with only one legitimate son.
Yet he believed God would perform the work promised. Could
not righteous Simeon have been mocked for awaiting the con-
solation of Israel, since God’s voice had been silent for four
hundred years (Luke 2:25)? Warfield writes:

The redemption of the world is similarly a process. It, too,
has its stages: it, too, advances only gradually to its completion.
But it, too, will ultimately be complete; and then we shall see a
wholly saved world. Of course it follows, that at any stage of the
process, short of completeness, the world, as the individual, must
present itself to observation as incompletely saved. We can no
more object the incompleteness of the salvation of the world
today to the completeness of the salvation of the world, than we
can object the incompleteness of our personal salvation today
{the remainders of sin in us, the weakness and death of our
bodies) to the completeness of our personal salvation. Everything
in its own order: first the seed, then the blade, then the full corn
in the ear. And as, when Christ comes, we shail each of us be like
him, when we shall see him as he is, so also, when Christ comes,
it will be to a fully saved world, and there shall be a new heaven
and a new earth, in which dwells righteousness.”

In the final analysis, Rushdoony is on target when he notes
the underlying modernism of eschatological arguments that are
based on current world conditions: “Such comments are in
principle modernistic, in that they assess Scripture, not in terms
of itself, but in terms of the times, the modern age.””

27. Benjamin B. Warfield, The Plan of Salvation (Grand Rapids: Eerdmans, [n.d.]
1970), pp. 101-102.

28. R. J. Rushdoony, “Introduction,” J. Marcellus Kik, An Eschatology of Victory
(n.p.: Presbyterian & Reformed, 1971}, p. vii.

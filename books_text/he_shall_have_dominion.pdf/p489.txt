Pragmatic Objections 443

on the theory of evolution and humanism than on any inter-
pretation of the Bible and need not occupy our attention here.
The present heirs of modernism, the neoorthodox and neolib-
eral people, are scarcely more optimistic about the course of the
present era than premillenarians and so are not inclined to

postmillennialism.”*

I have already dealt briefly with the issue of hermeneutics in
Chapter 8. Virtually no evangelical scholars except dispensa-
tionalists assert that “literalism” is a protection against liberal-
ism. Premillennialist Ladd complains against dispensationalists:

Walvoord goes on to say that ‘the diverse theological systems of
Roman Catholic, modern liberal, and modern conservative wril-
ers are found to be using essentially the same method.’ This
amounts to the claim that only dispensationalism, with its literal
hermeneutic of the Old Testament, can provide a truly evangeli-
cal theology. In my view this simply is not true. B. B. Warfield
did not use the same ‘spiritualizing’ hermeneutic as the liberal.
The liberal admits that the New Testament teaches the bodily
resurrection of Christ, but his philosophical presuppositions
make it impossible for him to accept it. On the other hand, B. B.
Warfield was the greatest exponent of a high view of biblical
inspiration of his day. He was prepared to accept any doctrine
which could be proved by the Scriptures. If he ‘spiritualized’ the
millennium, it was because he felt a total biblical hermeneutic re-
quired him to do so. This is not liberalism.“

In his response to dispensationalist Hoyt, Ladd laments:
Hoyt’s essay reflects the major problem in the discussion of the
millennium. Several times he contrasts nondispensational views

with his own, which he labels ‘the biblical view’ (pp. 69-70, 84).
If he is correct, then the other views, including my own, are

48. Culver, Daniel in the Latter Days, pp. 206-207.

49. George Eldon Ladd, “Historic Premillennialism,” Meaning of the Millennium,

pp- 19-20.

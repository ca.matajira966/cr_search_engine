Theological Objections 451

A fatal objection to postmillennialism cannot be made by
pointing to the power of sin. The power of God to save greatly
overshadows the might of sin! Indeed, “with God all things are
possible” (Luke 18:27). The issue is the will of God. We have
seen that the Bible teaches that it is God’s will to bring redemp-
tion gradually to the whole world as a system through the proc-
lamation of the Christ’s gospel and building His Church.

In a sense it is true that the postmillennialist overlooks the
depravity of man. He overlooks it, or better: looks over and
beyond it to see the resurrection of Jesus Christ.” We see the
glorious reconstructive power of the resurrection of Christ as
overwhelming the very real destructive power of the Fall of
Adam.° We need to consider the strength of grace in comparison
to the power of sin. The Christian should ask himself: Have I
ever seen a lost man become saved? The answer is: Yes. This
being the case, it is evident that grace is stronger than sin!
Then again, he should ask: Does the Bible teach that a saved
man can lose his salvation? Here the answer is: No. In both
cases, we see the superior power of God’s grace over man’s sin.

“Souls that have felt the Saviour’s grace know right well its
matchless power. After their own conversion, they can never
doubt its converting efficacy on any scale that may be required.””
To respond to Lindsey’s plaint: We do not believe in the inher-

5, Chilton well states: “Like Peter walking on the Sea of Galilee, they [despairing
evangelicals] looked at ‘nature’ rather than at the Lord Jesus Christ; like the Israelites
on the border of Canaan, they looked at the ‘giants in the land’ instead of trusting
the infallible promises of God; they were filled with fear, and took flight.” David
Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth, TX: Dominion
Press, 1985), p. 232.

6. Gary North, Is the World Running Down? Crisis in the Christian Woridview (Tyler,
TX: Institute for Christian Economics, 1988). Strangely, a sentence after stating that
postmillennialism “fails to reckon properly with the fact of sin,” Hanko stumbles past
the postmillennial resolution to the problem: “Nothing will be changed until sin is
taken away. Christ did this on His cross”! Hanko, “The Illusory Hope of Postmillen-
nialism,” p. 159.

7. David Brown, Christ's Second Coming: Will It Be Premillennial? (Edmonton, AB:
Still Waters Revival, [1882] 1990), pp. 302-303.

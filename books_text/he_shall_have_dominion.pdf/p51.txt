The Significance of Eschatology 5

record. As Jirgen Moltmann puts it: “From first to last, and not
merely in the epilogue, Christianity is eschatology, is hope,
forward looking and forward moving, and therefore also revo-
lutionizing and transforming the present.” J. J. Van Ooster-
zee agrees: “All true Theology is at the same time Teleology,
which must of itself lead to Eschatology.”!* Or, to put the mat-
ter statistically in this econometric age, some research suggests
that the prophetic element in Scripture accounts for more than
one-fourth, or about 27% of the biblical record, because predic-
tive prophecy is found in 8,352 of the Bible’s 31,124 verses.

Berkhof puts the significance of eschatology in proper per-
spective regarding its relation to the other branches of system-
atic (or dogmatic) theology:

In theology it is the question, how God is finally perfectly glori-
fied in the work of His hands, and how the counsel of God is
fully realized; in anthropology, the question, how the disrupting
influence of sin is completely overcome; in christology, the ques-
tion, how the work of Christ is crowned with perfect victory; in
soteriology, the question, how the work of the Holy Spirit at last
issues in the complete redemption and glorification of the people
of God; and in ecclesiology, the question of the final apotheosis
of the Church. All these questions must find their answer in the
last locus of dogmatics, making it the real capstone of dogmatic
theology.’®

13. Jiirgen Moltmann, Theology of Hope, trans. by J. W. Leitch (New York:
Harper & Row, 1967), p. 16. Berkhof also laments the epilogical placement of
eschatology: “In such a scheme eschatology could only appear as the finale of history,
and not at all as one of the constitutive elements of a system of truth.” Berkhof,
Systematic Theology, p. 664.

14, J.J. Van Oosterzee, Christian Dogmatics, 2 vols. (New York: Scribner’s, n.d.),
2:581,

15. J. Barton Payne, Encyclopedia of Biblical Prophecy (New York: Harper & Row,
1973), pp. 675, 681. Of course, this is not to say that all of these prophecies are
future to our time.

16. Berkhof, Systematic Theology, p. 665.

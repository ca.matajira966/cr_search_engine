Biblical Objections 473

under Christ’s spiritual administration as His worship spreads
through the earth (vv. 10ff’). Even Jerusalem and the Jews
shall be nourished by the waters of life eventually (vv. 10-11; cf.
Ezek. 47:1{f; John 7:38-39). The enemies of God’s people will
either be vanquished (vv. 12-13, 14), converted (vv. 16, 20-21)
or reduced to insignificance (vv. 14, 17-19).

The Feast of Tabernacles is mentioned, not as a literal rein-
stitution of the Old Testament feast, but as the ultimate hope
pre-figured in that feast: the time of the fullness of the field
and its harvest (cf. John 4:35-38). Those who do not convert
will be reduced to servile labors, lacking the blessing of God
(vv. 17-19).

Overall, however, the kingdom of God (represented here by
a rejuvenated Jerusalem) will be spread throughout the earth.
All areas of life will be consecrated to the Lord: even the horses’
bells will contain the inscription written on the High Priest’s
miter (vv. 20-21).

Matthew 7:13-14

Enter by the narrow gate; for wide is the gate and broad is the
way that leads to destruction, and there are many who go in by
it. Because narrow is the gate and difficult is the way which leads
to life, and there are few who find it.

This is a popular passage that seems on the surface to be
detrimental to the postmillennial outlook. These words are
often cited to show the paucity of the number of saved: “[T]he
great mass of humanity are engulfed in the maelstrom of sin,
which is sweeping its millions down to graves of destruction
(Mat. 7:13), and compared to them, in numbers, the true be-
lievers are but a handful. In the Millennium all this will be
changed.”* “The way to salvation is narrow, and only a few

7. See Isa. 40:4; Zech. 4:7; Mark 11:23; Luke 3:5.
8. William E. Blackstone, jesus Is Coming (3rd ed.; Old Tappan, NJ: Revell,

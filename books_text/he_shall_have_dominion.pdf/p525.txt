Biblical Objections 479

The point of the Parable of the Tares is not to reiterate what
the Mustard Seed and Leaven Parables teach. Rather, it is
designed to illustrate the fact that there will always be a mixture
of the unrighteous and the righteous. Neither sin nor sinners
will ever totally be rooted out of the world in history - not even
during the kingdom’s highest development in the future. Con-
sider the boldness of Christ’s instruction here in light of the
contemporary setting of the parable. The gospel was having only a
minuscule influence in the world, the Lord Himself was soon to
be cruelly crucified. Christ warned His people of the great trials
and tribulations through which they must go. Yet He speaks
here of what those possessing power should do with sinners in
His kingdom! Surely this concern has behind it the concept of
a massive influence of His kingdom: concern about what to do
with the defenseless leftover people, the wicked!

It is clear upon reading the parable that the entire world is
considered God’s field, where He desires fully to plant wheat:
He “sowed good seed in his field” (v. 24); “the field is the
world” (v. 38). The effort - surely a great and purposeful one
— is expended in order to create a field of wheat (the righteous,
vy. 38a) in all of the world (cf. Matt. 28:18-20). An enemy (the
devil, v. 39) intervenes and sows tares (the wicked, v. 38b) —
surely not equally great and successful, particularly in light of
the parables of the Mustard Seed and Leaven. The point of the
parable is that tares will be found among the predominant
wheat: the tares are the intruders, not the wheat. That to which
the Son of Man returns in the parable is a wheat field, not a tare
field. The tares are to be left alone for the sake of the wheat.

Luke 18:8

] tell you that He will avenge them speedily. Nevertheless, when
the Son of Man comes, will He really find faith on the earth?

This verse of late has been employed with great confidence
by dispensationalists against postmillennialism. House and Ice

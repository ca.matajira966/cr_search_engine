492 HE SHALL HAVE DOMINION

Employing these or similar verses, premillennialist Krom-
minga and amillennialists Hocksema, Berkhof, Hanko, and
Morris agree with Hockema that “the postmillennial expecta-
tion of a future golden age before Christ’s return does not do
justice to the continuing tension in the history of the world
between the kingdom of God and the forces of evil.”*! Hen-
driksen comments on this passage: “These seasons will come
and go, and the last will be worse than the first. They will be
seasons of ever-increasing wickedness (Matt. 24:12; Luke 18:8),
which will culminate in the climax of wickedness.”

Dispensationalists agree: “[T]he Bible speaks of things pro-
gressing from ‘bad to worse,’ of men ‘deceiving and being
deceived’ (2 Timothy 3:13), we look out at our world and see
how bad things really are.”** “(With the progress of the pres-
ent age, in spite of the dissemination of the truth and the avail-
ability of Scripture, the world undoubtedly will continue to
follow the sinful description which the Apostle Paul gave
here.”** “Passages like 1 Timothy 4 and 2 Timothy 3 paint a
dark picture of the last days."

Such interpretations of this passage, however, are exeget-
ically flawed and anti-contextual. Nothing taught in these verses

51. Hoekema, Bible and the Future, p. 180. See also: D. H. Kromminga, The
Millennium in the Church: Studies in the History of Christian Chitiasm (Grand Rapids:
Eerdmans, 1945), pp. 72, 265. Louis Berkhof, Systematic Theology (Grand Rapids:
Eerdmans, 1941}, p. 718. Herman Hoeksema, Reformed Dogmatics (Grand Rapids:
Reformed Free, 1966), p. 817. Herman C. Hanko, “An Exegetical Refutation of
Postmillennialism,” pp. 16-17. Leon Morris, “Eschatology” Encyclopedia of Christianity,
P E. Hughes, ed., 4 vols. (Marshallton, DE; National Foundation for Christian
Education, 1972), 4:85. This encyclopedia was never completed.

52. William Hendriksen, J and I] Timothy and Titus (NTC) (Grand Rapids: Baker,
1957), p. 283.

53, House and Ice, Dominion Theology, p. 183

54. Walvoord, Prophecy Knowledge Handbook, p. 495.

55. Wiersbe, Bible Exposition Commentary, 1:249; see also 2:249. Cf. Charles F.
Baker, A Dispensational Theology (Grand Rapids: Grace Bible College, 1971), p. 623.
Chapman, “The Second Coming of Christ: Premillennial,” Rindamental Christian
Theology, 2:341. Bruce Milne, What the Bible Teaches About the End of the World (Whea-
ton, IL: Tyndale, 1979), pp. 80-81.

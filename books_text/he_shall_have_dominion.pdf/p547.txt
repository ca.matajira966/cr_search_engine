Concluding Remarks 501

The kingdom of Christ is essentially a spiritual kingdom
(John 18:36-37, Rom. 14:17) that operates from within the
heart (Luke 17:20-21). We enter the kingdom of Christ by
means of salvation (Col. 1:12, 13; John 3:3). Christ rules His
kingdom by His mystical presence from heaven (John 18:36;
Eph. 4:8-14) and through the indwelling of the Holy Spirit
(John 7:39; Rom. 8:9; 1 Cor. 3:16). The basic power of the
kingdom is the “gospel of the kingdom” (Matt. 4:23; 9:35; Mark
1:14-15). The basic function of the kingdom is the promotion of
God’s truth (John 18:37; 2 Cor. 10;4-5).

The kingdom of Christ is not a future, Armageddon-intro-
duced, earthly, political kingdom. The first-century Jews want-
ed a political kingdom to overthrow Rome, and when Christ
did not offer them this (John 6:15), they rejected Him. Even his
disciples were confused and disappointed for a time (Luke
24:21-27). Isracl as a political entity has once for all been set
aside as the specially favored nation of God (Matt. 8:11-12;
21:43), because of their prominent role in crucifying Christ
(Acts 2:22-23,36; 3:13-15; 5:30; 7:52; 1 Thess. 2:14-15). The
Messianic kingdom includes people of all races on an equal
basis (Isa. 19:19-25; Zech. 9:7; Eph. 2:12-17); even great num-
bers of Jews will eventually enter it (Rom. 11:11-25).

The New Testament-phase Church is “the Israel of God”
(Gal. 6:16), “the circumcision” (Phil. 3:3), “the seed of Abra-
ham” (Gal. 3:7, 29), the “Jerusalem above” (Gal. 4:24-29), the
“temple of God” (Eph. 2:21), “a royal priesthood” and a “pecu-
liar people” (1 Pet. 2:9-10). Consequently, Jewish promises are
applied to the Church (Jer. 31:31-34; Matt. 26:28).

Evangelism is the essential pre-condition to postmillennial,
theocratic success. Apart from Christ we can do nothing (John
15:5; Matt. 19:26); in Christ we can do all things (Phil. 4:13, 19;
Matt. 17:20). Because He possesses “all authority in heaven and
on earth” (Matt. 28:18; Eph. 1:19-22), Christ’s Great Commis-
sion expects His people to win converts, who are then baptized
into His body, and then instructed in “all things” He taught

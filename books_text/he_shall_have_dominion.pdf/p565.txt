Cultural Antinomianism 519

law”; they “have fallen from grace” (Gal 5:4). And it is the
ceremonial elements of the Law that are being pressed by these
Judaizers — particularly circumcision (Gal. 2:3-4; 5:2, 3, 11; °
6:12-13), but also ceremonial festivals (Gal. 4:10). There is no
mention of the civil laws, or of the moral law; neither is inchud-
ed in Paul’s denunciation of law keeping (of course, the keep-
ing of these cannot merit salvation, either).

The Sanctified Conscience

The final statement by Hanko in his dismissing of the civil
laws is clearly antinomian: “But how each child of God lives his
life in his own station and calling and how he applies the abid-
ing principles of the kingdom of heaven to his own place in
that kingdom is a matter of Christian liberty. The principles are
all in the Scriptures. The application of them is in the sanctified
consciousness of the child of God.”’’ This differs little from
House and Ice’s “wisdom” approach to the Law of God.”
House and Ice are dispensationalists, hence antinomians.”"

Does God allow His children to choose to apply laws accord-
ing to their own determination? Or does He expect us to follow
His revealed laws in obedience? May we pick and choose
among God’s commandments according to our “sanctified
consciousness”? “Woe to you, scribes and Pharisees, hypocrites!
For you pay tithe of mint and anise and cumin, and have ne-
glected the weightier matters of the law: justice and mercy and
faith. These you oughé to have done, without leaving the others
undone” (Matt. 23:23). “Thus you have made the command-
ment of God of no effect by your tradition” (Matt. 15:6b).

19. Hanko, “An Exegetical Refutation of Postmillennialism,” p. 21.

20. H. Wayne House and Thomas D. Ice, Dominion Theology: Blessing ar Curse?
(Portland, OR: Multnomah, 1988), pp. 86-137.

21. John H. Gerstner, Wrongly Dividing the Word of Truth: A Critique of Dispensa-
tionalism (Brentwood, TN: Wolgemuth and Hyatt, 1991), chaps. 11-12.

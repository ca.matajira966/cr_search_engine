530 HE SHALL HAVE DOMINION

Is suffering ordained to continue throughout the entirety of
history until the end?

The postmillennial position is that suffering is ethically necessary
in many times.

The Role of Suffering in History

It is abundantly clear from Scripture that the people of God
can expect suffering in their temporal experience. Given the
sinful condition of the fallen world, the righteous are a stum-
bling block and an offense to the unrighteous. Believers are in
the world but not of it (john 15:19). Christ warned His discip-
les: “Remember the words I spoke to you: ‘No servant is great-
er than his master.’ If they persecuted me, they will persecute
you also” (John 15:20a) and “in this world you shall have trou-
ble” (John 16:33). Paul and Barnabas carried forth this caution
by “strengthening the disciples and encouraging them to re-
main true to the faith” and instructing that “we must go
through many hardships to enter the kingdom of God” (Acts
14:22), Paul later reminds Timothy of the trial that lay before
him: “In fact, everyone who wants to live a godly life in Christ
Jesus will be persecuted” (2 Tim. 3:12). Peter urged his reader
not to “be surprised at the painful trial you are suffering, as
though something strange were happening to you” (1 Pet.
4:12).

The Case of Job

The book of Job is the classic demonstration of the purpose
of the suffering among God’s people in history. It specifically
addresses the question: “Why do the righteous suffer?” In Job,
we discover at the very outset that suffering does occur to “the
perfect and upright” (Job 1:1), contrary to health-and-wealth
advocates.” Job’s suffering was very real and quite grievous:

13. This may be seen also in the persecution and illness endured by faithful

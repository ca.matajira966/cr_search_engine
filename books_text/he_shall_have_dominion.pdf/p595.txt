Bibliography 549

Jordan, James. The Law of the Covenant: An Exposition of Exo-
dus 21-23. Tyler, TX: Institute for Christian Economics, 1984.
A clear introduction to the issues of the case laws of the Old
Testament.

North, Gary. The Dominion Covenant: Genesis. Tyler, TX:
Institute for Christian Economics, (1982) 1987. A study of the
economic laws of the Book of Genesis.

North, Gary. Moses and Pharaoh: Dominion Religion us. Power
Religion. Tyler, TX: Institute for Christian Economics, 1985, A
study of the economic issues governing the Exodus.

North, Gary. Political Polytheism: The Myth of Pluralism. Tyler,
TX: Institute for Christian Economics, 1989. A 700-page cri-
tique of the myth of neutrality: in ethics, social criticism, U.S.
history, and the U.S. Constitution.

North, Gary. The Sinai Strategy: Economics and the Ten Com-
mandments. Tyler, TX: Institute for Christian Economics, 1986.
A study of the five-point covenantal structure (1-5, 6-10) of the
Ten Commandments. Includes a detailed study of why the Old
Covenant’s capital sanction no longer aplies to sabbath-break-
ing.

North, Gary. Tools of Dominion: The Case Laws of Exodus. Ty-
ler, TX: Institute for Christian Economics, 1990. A 1,300-page
examination of the economics of Exodus 21-23.

North, Gary. Westminster's Confession: The Abandonment of Van
Til’s Legacy. Tyler, TX: Institute for Christian Economics, 1991.
Refutes, chapter by chapter, the criticisms offered in Westmin-
ster Seminary’s Theonomy: A Reformed Critique. North shows that
the seminary has returned to natural Jaw theology in order to
defend political pluralism. North says that this is by far his best
polemical book. Rushdoony has publicly said so, too. It reprints
H. L. Mencken’s 1937 obituary of J. Gresham Machen.

North, Gary, ed. Theonomy: An Informed Response. Tyler, TX:

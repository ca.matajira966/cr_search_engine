Subject Index 583

Theology
contemporary, 5, 29
evangelical, 31
fundamentalism, 124
liberal (See aése: Gospel
— social), 31, 34,
liberation theology, 124
practical, 7, 17, 30
Theonomy (See: Law of God)
Third World, xl
Thousand years (See:
Millennium)
Throne, David’s, 155, 205,
Q11, 454, 486
Time, 4, 493
Titus, 317, 346, 391, 406
Transformation
Cultural, 5, 71, 72, 124-125,
203, 219, 233, 263, 507
New Creational, 360f
Tribulation
coming event, 22, 24, 164
Great, 56, 59-60, 63, 230,
328, 338-349, 469fF
Revelation of John and,
396
Trinity, 125
Triumphal Entry (See:
Christ — Triumphal Entry)
Triumphalism, 21
Trumpet, 348
Twelve Tribes of Israel (See:
Israel — Twelve Tribes)
Type/typology (See:
Hermeneutics — typology)

Unification Church (See:
Cults — Unification

Church)

United Nations, 41
Universalism, 245, 253-255,
268-269, 296-297, 477
Universe, 13, 275

Utopia, 57

Vespasian, 317, 404

Victory (work of Christ and),
5

Vision, 42

Volcano, 405

Wars (See also: Peace)
cessation of, 71, 244f
Gulf, 40n, 41, 330
sign of end, 250, 344
World Wars, xlii, 32-33,

324, 426-427

Watchfulness, 435-439

Water of life, 359, 472

Western civilization, 15

Westminster Standards/
Confession of Faith, 90,
282

Wisdom, 123, 519

Witness, 31, 344-345

World
as a system, 182, 264
salvation of (See:

Salvation — of world)

Worldview
creation and, 119, 177
development of, 25-26
eschatology and, 16, 24, 64
models, 123-125
Scripture and, 6, 99, 123,

261-268

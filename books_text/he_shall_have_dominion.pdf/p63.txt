The Significance of Eschatology 17

costly labors of our Christian forefathers, particularly from the
Reformation era through the early 1900s.

Eschatological Pessimism

Three of the four major evangelical eschatolggical systems**
may be categorized as “pessimistic,” whereas the view to be set
forth in the present work may be seen as “optimistic.” In cate-
gorizing them as pessimistic, I am speaking of the following
issues:

(1) As systems of gospel proclamation each teaches the gospel
of Christ will not exercise any major influence in the world
before Christ’s return;

(2) As systems of historical understanding each holds that the
Bible teaches that there are prophetically determined, irresistible
trends downward toward chaos in the outworking and develop-
ment of history; and therefore

(3) As systems for the promotion of Christian discipleship,
each dissuades the Church from anticipating and laboring for
wide-scale success in influencing the world for Christ during this
age.

The pessimism/optimism question has very much to do with
the practical endeavors of Christians in the world today.” All
evangelical Christians are optimistic in the ultimate sense that
God will miraculously win the war against sin and Satan at the
end of history by direct, supernatural intervention, either in an
earthly millennial kingdom introduced by Jesus at the Second

54. See Chapters 3 and 4 for a study of the four major evangelical eschatological
systems.

55. Gary North has stated that Christians who are either premillennial or
amillennial tend to become operational postmillennialists when they begin to get
involved in social action projects, whether or not these are political activities. North,
“Ghetto Eschatologies,” Biblical Economics Today 14:3 (April/May 1992) 3-4, 6. He
points out that dispensational activists in the United States after 1975 ceased discuss-
ing in public the details of their eschatology (p. 3).

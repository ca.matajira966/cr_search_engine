38 HE SHALL HAVE DOMINION

postmillennialism could be called “the commonly received
doctrine,” as it was in 1859,** Historians can state that the var-
ious premillennial views “were scattered throughout the major
denominations, but none of those successfully challenged the
hegemony of postmillennialism before the last decades of the
century.” Such has certainly not been the case in the 1900s.

Although any historical analysis of the decline of postmillen-
nialism is complex, there does appear to be merit in the view
that “in a word, the erosion of postmillennialism was part of the
waning of supernaturalism” in the early 1900s. Evangelical
postmillennialism held to a high supernaturalism that could
shake heaven and earth. With the decline of a widespread
commitment to supernaturalism in conjunction with the arising
of various radical critical theories, interest in postmillennialism
waned.

Nevertheless, “postmillennialism, since 1965, has exper-
ienced a renaissance.”* As indicated above, there has begun
to flow an ever increasing stream of postmillennial literature. In
the 1980s, that stream has become a flood. Yet at the same time
{and at least partly because of the renewal of postmillennial
advocacy) there is evidence of a decline in adherence to premil-
lennialism, the dominant evangelical view of the 1900s.

Some recent dispensational works have begun mentioning
the slipping of the numbers of premillennialists. One dispensa-

herg’s 1936 book defended premillennialism against amillennialism, with virtually no
mention of postmillennialism,

42. James H. Moorhead, “Millennialism in American Religious Thought,” journal
of American History 71:3 (Dec. 1984) 525.

43. Ibid. Sce also: “History of Opinions Respecting the Millennium,” American
Theological Review 1 (Nov, 1859) 655. George M. Marsden, The Evangelical Mind and
the New School Presbyterian Experience: A Case Study of Thought and Theology in Nineteenth-
Century America (New Haven: Yale University Press, 1970), pp. 185ff.

44. James H. Moorhead, “The Erosion of Postmillcnnialism in American Reli-
gious Thought, 1865-1925,” Church History 53 (1984) 76.

45, Gary North, “Towards the Recovery of Hope,” Banner of Truth, No. 88 (Jan.
1971) 12.

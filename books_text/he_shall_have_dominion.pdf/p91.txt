The Purpose of This Treatise 45

3. Interaction With Rival Views

Third, a major design of the present work is to provide a
comparison and interaction with the other major evangclical
millennial views. It is too often the situation today in popular
millennial literature that many make unwarranted assumptions
implying the universal recognition of a particular view, without
informing the reader of competing systems. This is particularly
true in dispensational circles, especially among the popular pro-
ponents, more so than among the theologians. Thus, I will
interact with the non-postmillennial systems, attempting to
summarize their salient features and expose their flaws, as
understood from a biblically based postmillennial viewpoint.

Interestingly (or perhaps, tragically), the resurgence of post-
millennialism has been rather harshly attacked recently by some
dispensationalist writers.®? The underlying assumption in these
works is always dispensationalism’s implicit monopolistic claim
to orthodoxy.” There is a distressing ignorance in too many
Christians today regarding the cxistence of non-premillennial
eschatologies among Bible-believing, evangelical Christians.”
A kind of blackout exists within dispensational circles.

Hunt, with historical naiveté, castigates the resurgent post-
millennialism of the 1980s: “When confronted with an alleged
key doctrine that men and women of God have failed to uncov-
er from Scripture in 1900 years of church history, we have
good reason to be more than a little cautious. After all, this is

69, A helpful corrective and rebuke to such may be found in Bob and Gretchen
Passantino, Witch Hunt (Nashville: Thomas Nelson, 1990), see especially Chapter 8.
It needs to be pointed out that the authors are premillennialists.

70. Hunt, Whatever Happened to Heaven?; Hal Lindsey, The Read to Holocaust (New
York: Bantam, 1989).

71. In a taped program in February of 1989, I was interviewed with dispensa-
tionalist Thomas D. Ice on a Christian radio program in Austin, Texas. The dialogue
regarded dispensationalism and postmillennialism. On several occasions the inter-
viewer, who tried (at first) to be “objective” in his interview, kept referring to dispen-
sational distinctives in common dispensationalism parlance: he called these “dispensa-
tional truths.” Who can argue with truth?

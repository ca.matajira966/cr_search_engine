The Pessimistic Millennial Views 53

ral interpretation of the [Rev. 20] passage, and it is the view of
the present author. One thing must be granted: this is the only
place in Scripture which teaches a thousand-year reign of
Christ.”*

The Standard Millennial Positions

In developing a systematic eschatology, the standard evan-
gelical viewpoints have tended to be sorted out along millennial
lines. The term “millennium” is used in association with pre-
fixes that tend to modify the Second Coming of Christ as to its
relation to the millennium: emillennial, premillennial, and post-
millennial. The privative a in “amillennialism” emphasizes that
there will be no earthly millennial kingdom as such.” The
prefix pre indicates that system of eschatology that expects there
to be a literal earthly millennial kingdom that will be introduced
by the Return of Christ before (pre) it. The prefix post points to
the view of the millennium that holds there will be a lengthy
(though not a literal thousand years) earthly era of righteous
influence for the kingdom that will be concluded by the Return
of Christ. Puritan era postmillennialism tended to expect a
literal thousand-year millennium introduced by the conversion

8. George Eldon Ladd, The Last Things: An Eschatology for Laynen (Grand Rapids:
Eerdmans, 1978), pp. 108-110.

9. Many amillennialists arc disturbed by the negative prefix: “The term amillen-
nialism is not a very happy one. It suggests that amillennialists either do not believe
in any millennium or that they simply ignore the first six verses of Revelation 20,
which speak of a millennial reign. Neither of these two statements is correct.” Hoek-
ema, Bible and the Future, p. 173. Hamilton, Adams, and Hughes agree. Philip E.
Hughes, Interpreting Prophecy: An Essay in Biblical Perspectives (Grand Rapids: Eerd-
mans, 1976), pp. 99-100 and Floyd E. Hamilton, The Basis af the Millennial Faith
(Grand Rapids: Eerdmans, 1942), p. 35. See the discussion of the problem with a
proposed solution to it in: Jay E. Adams, The Time fs at Hand (Nutley, NJ: Presbyter-
ian & Reformed, 1966), pp. 7-11. There are amillennialists, however, who do not
mind the term, when literally interpreted. The word ‘amillennial’ is “a term which
indicates a denial of any future millennium of one thousand years’ duration.” George
L. Murray, Millennial Studies: A Search for Truth (Grand Rapids: Baker, 1948), p. 87.
Sec also Berkhof, Systematic Theolagy, p. 708.

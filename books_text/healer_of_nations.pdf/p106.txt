92 Healer of the Nations

not gather with Me scatters abroad” (Matthew 12:30). There is no
moral neutrality between the rival kingdoms. There can therefore
be no long-term covenantal neutrality between them: sovereignty,
hierarchy, law, judgment, and continuity.

A lot of Christians affirm that there is no moral neutrality in
life, but they do not really believe it. Any attempt to place both
kingdoms under a neutral, hypothetically universal, hypotheti-
cally permanent “natural law” judicial system is taking Adam’s ap-
proach: affirming the existence of an independent testing ground
that can judge between God’s Word and Satan’s word. But no
such independent judicial testing ground exists. Adam died
because he refused to believe this. Why do Christians continue to
maintain it?

Let us face reality: if natural law is @ myth, then there is no Biblical
alternative to theocracy. Anyone who says that he is opposed to the
tayth of neutrality but who simultaneously insists that he is
equally opposed to theocracy is intellectually schizophrenic. He
cannot long remain in the front lines of the spiritual battlefield.
He will have to go do something else with his life, laboring in the
shadows where the stark contrast between God’s law and Satan’s
laws is not highlighted by the glare of white-hot conflict. If you are
a Christian, either you affirm the myth of neutrality (natural law)
or else you affirm theocracy. There is no convenient, uncontrover-
sial halfway house in between.

Since 1980, we have seen several nationally prominent leaders
march onto the battlefield carrying two banners: “We must oppose
the myth of neutrality” and “We must oppose theocracy.” What is
noticeable about these leaders is that after a few brief skirmishes
with the humanists, they and many of their followers quietly and
unobtrusively retreat from the front lines. Intellectual schizo-
phrenia affects a person’s ability to lead the troops during a battle.”

Covenant Law vs. Natural Law: The Nation

Few if any Christians today believe that the Word of God ap-
plies exclusively to a particular nation or race. They say that the

10. Gary North, “The Intellectual Schizophrenia of the New Christian Right,”
Christianity and Civilization, No. 1 (1982), pp. 1-40.

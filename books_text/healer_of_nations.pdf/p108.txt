94 Healer of the Nations

schools. (See Robert Thoburn’s book in the Biblical Blueprints
Series, The Children Trap: Biblical Blueprints for Education.) They
have publicly and covenantally turned over civil government to
Satan through his “neutral” human followers.

It is time for Christians to abandon the myth of natural law. It
is time for them to declare instead the covenants of God. It is time
for them to proclaim the ethical terms of the covenant, God’s re-
vealed law, for God is the Sovereign Creator who governs all of
history. It is time to abandon the myth of neutrality.

If neutrality is illegitimate in the heart of each individual, if it
is illegitimate in marriage, if it is illegitimate in the church, then
why is judicial neutrality legitimate in civil government? If God’s
law is the standard of judgment on judgment day, why isn’t it the
standard now, when we serve as apprentice judges? Doesn't God
judge individuals, institutions, and nations in history, as well as at
the end of history? Isn’t Deuteronomy 28 true today, just as it was
in Moses’ day?

And if we say that God’s law is the only valid standard of
righteousness for a person, a family, a church, and a nation, then
how can we deny that it is valid for all nations? If the gospel
proves successful, and the Great Commission is steadily fulfilled,
and a majority of people convert to Christ in nation after nation,
and then they seek to do God’s will in every area of life, won’t we
see the creation of a worldwide Christian order that will steadily
replace the worldwide disorder of Satan’s divided kingdom? If
not, why not?

World Government Through World Law

There are many volumes of books dealing with the law of na-
tions. These laws are somewhat vague. Because there is no hierar-
chical court of appeals to enforce the law, there is no system of
sanctions to reward those who obey the law or punish those who
refuse to obey. Victorious nations can impose “victor’s justice” on
losers, but this sort of activity can hardly be said to promote right-
eousness, unless we believe that military governments in the flush

98 Healer of the Nations

In summary:

1. A disciple of Christ is disciplined by Christ.
2. A disciple of Christ is to work in a comprehensive evangel-
ism program to discipline the nations.
3. The standard of God’s discipline is His revealed law.
4. No law of God, no authority of God; no law of God, no jur-
isdiction of God; no law of God, no kingdom of God.
5. Discipleship begins with (but does not end with) self-disci-
pline under God’s law.
6. The kingdom is universal.
7. The kingdom is to be made visibly universal.
8. One means of evangelism is through the visible blessings of
God.
9. God’s laws are designed to impress pagan nations: evangel-
ism through visible success.
10. There is no legitimate alternative to Biblical law.
11. Natural law is a satanic myth.
12. Relativism replaces natural law when it fails.
13. Power replaces relativism: might makes right.
14. The source of law is the god of any society.
15. Natural law theory was first presented by a handful of
pagan Greek stoic philosophers.
16, Roman scholars used it to defend the empire.
17. Darwin's Origin of Species destroyed the case for natural
moral law,
18. The work of Biblical law is written on every man’s heart.
19. Rebellious men suppress their knowledge of this law.
20. Conscience alone is not a reliable guide to action.
21. Israel was required to hear the law once every seven years.
22. We are to be doers of the law, not hearers only.
23. Rival kingdoms use rival law-orders.
24. The kingdoms cannot be fused; their laws cannot be fused.
25, Nations are to be brought publicly under God’s covenant
law for nations,
26. There is no neutrality in national laws.
27. The Bible proclaims universal covenant law.
98, It therefore affirms universal civil government.
29. The universal kingdom of God is to find universal expres-
sion covenantally.

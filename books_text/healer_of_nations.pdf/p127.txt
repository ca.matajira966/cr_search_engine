Nations With Rival Covenants Are Always at War 113

over $20 billion.22 These deals, he asserts, will make Occidental
Petroleum the number-one American corporation operating in
the Soviet Union until the year 2000.23 As I write this, in the sum-
mer of 1987, Armand Hammer is still trading with the Soviet
Union, as well as Communist China. He is almost certainly the
only person in the world whose private jetliner is entitled to land
in both nations.* Hammer told The Times of London in 1972, “In
fifty-one years of dealing with the Soviets I've never known a bet-
ter climate for growth. We’re moving towards socialism, they
towards capitalism. Between us there’s a meeting ground.”%
(Notice the dating of 51 years: 1972 minus 51 is 1921: he desper-
ately wants the world to believe that he made his first deal with the
Soviets only after having become a millionaire on his own as a
risk-taking entrepreneur.)

That same year, 1972, the U.S. Department of Commerce au-
thorized the sale to the Soviet Union of the ball bearing machines
that alone make possible the construction of MIRVed nuclear
warheads. A single Soviet missile can now launch at least 12 inde-
pendently targeted nuclear warheads. This made several million
dollars for the Bryant Chucking Grinder Corporation of Spring-
field, Vermont, who had begun petitioning the government to
make this sale in 1961.26 This sale soon brought the one con-
vergence the Soviets had always wanted: the ability to kill 80 per-
cent to 90 percent of all North Americans within 25 minutes after
launch.”

The only possible convergence of the West with international
Communism is in the concentration camp, the mental institution,
and the grave. Only to the extent that the West has abandoned

22. Ibid., pp. 400-1.

23. Ibid., p. 406.

24, Ibid., p. 458.

25. Cited by Joseph Finder, Red Carpet (Ft. Worth, Texas: American Bureau
of Economic Research, [1983] 1987), p. 262.

26. Richard Pipes, Survival Is not Enough: Soviet Realities and America’s Future
(New York: Simon & Schuster, 1984), p. 264.

27. Arthur Robinson and Gary North, Fighting Chance: ‘Ten Feet to Survival (Ft.
Worth, Texas: American Bureau of Economic Research, 1986).

ng Healer of the Nations

operate in terms of peace. Military aid to victims of Communist
aggression is sporadic and sometimes secret. The West does not
recognize military encirclement until the last moment.

If Hitler had not foolishly declared war against the United
States on Thursday, December 11, 1941, it is difficult to know what
the U.S. would have done. We subsequently committed most of
our military resources to the European war, not to the Pacific,
where Japan had attacked us. Roosevelt had wanted to take the
U.S, into the war in Europe, as published documents from
Churchill’s secret cabinet speeches revealed thirty years later. For
years, American historians had denied this.

LONDON, Jan. 1 (AP)— Formerly top secret British Govern-
ment papers made public today said that President Franklin D.
Roosevelt told Prime Minister Winston Churchill in August, 1941,
that he was looking for an incident to justify opening hostilities
against Nazi Germany.

But Roosevelt could not have accomplished his goal without
Hitler’s help. Even today, hardly any American without an ad-
vanced degree in U.S. history or modern European history knows
the legal basis of our entry into the war in Europe in 1941. The
Soviets are too wise to make a similar error. The encircling pro-
cess goes on, yet the West extends long-term credits to the Soviet
Union as if the Soviet Union were a U.S. ally.

Summary

The humanistic West is engaged in a long-term policy of sur-
render to totalitarian Communism. Our diplomats are not com-
mitted to the survival of the West. They seek peace, but it is the
peace of surrender. They hope to attain conditional surrender
through convergence. The Communists are more consistent: they
seek unconditional surrender, but in bite-sized portions.

There can never be peace in history outside of Christ. There
can be temporary cease-fire agreements, but never a lasting

33. New York Times (Jan. 2, 1972), p. 7.

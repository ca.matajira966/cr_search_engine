Nations With Rival Covenants Are Always at War 9

peace. What Christians must understand is that peace is attained
through the preaching of the gospel and the discipling of the na-

tions. There is no other way. God will not permit peace on any

other terms. War and peace are always covenantal concepts. As
Jong as God and Satan are engaged in a spiritual, historical, and
cosmic battle, so their covenanted disciples will be engaged in
spiritual, historical, and earthly conflict.

Foreign policy must be restructured in every Christian nation

to reflect this struggle. It, too, must be reconstructed in terms of
the Bible. The goal is international peace, but only on Christ’s

terms.
In summary:

. Acosmic battle is always going on between God and Satan.
. These wars are the result of sin in men’s hearts.
. Men universally want peace.
. Peace cannot be defined neutrally.
. The Communists have seen this clearly, and define peace in
terms of a worldwide Communist victory.
6. God also defines peace in terms of conquest: every knee
shall bow to Jesus Christ.
7, Christians can be peaceful warriors.
8. Peace is covenantal and attained through covenant-
keeping.
9, The war of this age is a war of first principles: God as God
vs. man as God.
10. The West's political leaders refuse to see that such a theolog-
ical war is going on.
11. Democracies wil! not admit that a war between two rival
systems is going on.
42. The West's humanists seek a convergence between liberal-
ism and Communism.
13. They seek the visible political unification of man.
14. This is a statement of faith: the unity of the godhead.
15. The explanation for the West’s policy of détente is found in
the West’s humanist theology.
16. The ultimate expression of this faith is the doctrine of

QPrwne

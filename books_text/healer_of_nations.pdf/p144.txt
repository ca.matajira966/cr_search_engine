130 Healer of the Nations

promote, and encouraging voters and allies to stick with the pro-
gram of military victory.

This is the key, in peace and war, day in and day out: a vision of
victory. Nothing else will suffice. Any diplomat who is not on the
job to make victory possible should be fired. Any foreign policy
officer whose goal is not the victory of the West over the Commu-
nists should be fired.

Life is covenantal. God’s covenant governs a nation’s life, or
else Satan’s covenant does. We wear Christ’s yoke (which is light)
or else we wear Satan’s yoke. There is no life without a covenantal
yoke. Life is therefore a war between covenanted societies.

Diplomats or Ambassadors?

Such a vision of victory is rejected by the West’s humanists,
though not by the Communists. A good statement of this no-
victory view is found in a book by Henry Wriston, one of the most
influential humanists in United States foreign policy in the twen-
tieth century. He was the father of Walter B. Wriston, the former
chairman of Citicorp, the holding company of the largest and
most influential bank in the United States. Henry Wriston served
as President of the Council on Foreign Relations from 1951 until
1964. The CFR is the United States’ most influential foreign pol-
icy organization, even more influential than the State Department
itself, which CFR members have dominated at the senior posi-
tions since 1941. Senator Prescott Bush, father of the Vice Presi-
dent of the United States (1981- _), said in 1961 to his colleagues in
the Senate concerning Dr. Wriston: “He is credited with having
‘Wristonized’ the Foreign Service of the United States.”*

In 1956, Wriston introduced his series of lectures at Claremont
Colleges in California with these words: “The foreign Service
Officer Corps has been virtually invisible throughout the larger
part of its half century or more of history. This is not peculiar or
abnormal. . . . But their effectiveness is in almost inverse ratio to

4. Cited by Dan Smoot, The Invisible Government (Boston: Western Islands,
[1962] 1977), p. 12.

158 Healer of the Nations

ments, from which all blessings flow. This centrality of the institu-
tional Church is the basis of a fundamental office in the Bible, the
office of ambassador of Christ. “Therefore we are ambassadors for
Christ, as though God were pleading through us: we implore you
on Christ’s behalf, be reconciled to God” (2 Corinthians 5:20).

This office is the model for civil ambassadors. It is not that the
office of civil ambassador for some earthly kingdom has become
the model for the Church office of witness-ambassador. On the
contrary, the Church office of witness-ambassador is God’s model
for the civil government's office of ambassador. The witness-
ambassador is the person who brings God’s covenant lawsuit be-
fore the people and kings of rival kingdoms, as Jonah did. It is
crucial to notice that Jonah did not ask Nineveh to submit cove-
nantally to Israel. He informed them of God's requirement that
they surrender unconditionally to God.!

Understand, however, that unconditional surrender to God is
not the same as unconditional surrender to some human govern-
ment. When national leaders insist that other national leaders
surrender unconditionally as covenantal representatives of their
nations, they are acting sinfully. They are imitating God, pre-
tending that a nation-state is the equivalent of God’s sovereign
kingdom. It is this demand for unconditional surrender that
reveals the paganism of modern statism. Underlying the doctrine
of unconditional surrender to a nation-state is the doctrine of the
absolute perfection of one nation and the total depravity in history
of its rival. Also underlying the insistence on unconditional sur-
render is a doctrine of unconditional hatred. It is this uncondi-
tional international hatred that has repeatedly led to world wars
in our era.?

The individual witness-ambassador of Christ’s kingdom in-
vites the enemies of God to join the family of God, person by per-
son, family by family — adoption into God’s family (John 1:12). At

1. Gary North, Unconditional Surrender: God’s Program for Victory (3rd ed.; Ft.
Worth, Texas: Dominion Press, 1987).

2. Russell Grenfell, Unconditional Hatred: German War Guilt and the Future of
Europe (Old Greenwich, Connecticut: Devin-Adair, [1953] 1971).

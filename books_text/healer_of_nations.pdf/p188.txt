174 Healer of the Nations

10. Christians are to baptize the nations.

11. Baptism and communion covenantally link all Christians in
the church during history.

12, Formal covenanting of individual Christian nations cove-
nantally link these nations under God’s judgment throne during
history.

13. Baptism of the nations means baptism of national represen-
tatives.

14. God sets pagan citizens aside covenantally to bless them ex-
ternally for the sake of faithful national representatives.

15. The process of covenanting is a long-term, bottom-up proc-
ess of evangelism.

16. Missionaries make the best ambassadors because they rep-
resent the kingdom of God.

17, They are in close contact with the people of the pagan na-
tion.

18. They are the best source of information about local condi-
tions.

19, They are not identified as an agent of an earthly colonial
power,

20. They are colonists: colonists representing heaven.

21. A head of State should decentralize foreign policy.

22. God provides national continuity.

23. No missionaries, no covenant.

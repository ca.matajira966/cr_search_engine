III, Ethics/Dominion

8
BUSINESSMEN MAKE THE BEST DIPLOMATS

Now as they heard these things, He spoke another parable,
because He was near Jerusalem and because they thought the
kingdom of God would appear immediately. Therefore He said:
‘A certain nobleman went into a far country to receive for himself
a kingdom and to return. So he called ten of his servants, de-
livered to them ten minas, and said to them, ‘Do business till I
come’” (Luke 19:12-13).

The third point of the Biblical covenant refers to the legal
terms of God’s covenant. These are His laws that He gives to men
in order that they might obey Him, and thereby extend their do-
minion over their assigned areas of responsibility. Obedience to
God's law is the public sign of men’s subordination to God (point
two) and the basis of God's rewards (point four). God’s revealed
law is man’s tool of dominion (point three).

Christ’s parable of the rebellious servants presents the familiar
kingdom picture of a nobleman and his servants. He entrusts to
his servants a sum of money, and tells them to do business with it.
The King James Version translates this last phrase, “Occupy till T
come.” By implication, this is what the word means, but not liter-
ally. The Greek word is pragmaieuomat. Interesting, isn’t it? The
Greek word for doing business comes from the same root word as
the English word “pragmatic.” In English usage, the word pragmatic
means “realistic” or “businesslike.” The pragmatist asks: “Does this
work?” His answer is governed by what goes on in what we call
“the real world.”

The Greek word pragma means “affairs,” or “business,” or

175

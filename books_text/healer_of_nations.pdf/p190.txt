176 Healer of the Nations

“andertaking.” These are worldly affairs. The pragmatist in this
sense is a man of the world, skilled in the practical affairs of men.
The word used in Luke 19:13 means “to pursue with vigor.” The
focus in the parable is profit-seeking business. It is equally reveal-
ing that in the Jews’ Greek translation of the Old Testament,
called the Septuagint (third century 8.c.), the same Greek word is
used to translate the Hebrew word for “the king’s business” in
Daniel 8:27, meaning the king’s service. To be in the service of the
king means that you must be a worldly, wise, practical person, a
pragmatist. But a Biblical pragmatist is not a person who denies
fundamental Biblical principles. A Biblical pragmatist pursues
God’s goals for mankind in every area of life, as effectively as cir-
cumstances allow him.

Christianity: This-Worldly and Otherworldly

This sounds strange to most Christians. They think to them-
selves: “Aren't we supposed to be principled people? Aren’t we
supposed to be Spiritual?” We are indeed. But because few Chris-
tians take seriously the Biblical doctrines of creation, providence,
and ethics, they mentally create a false conflict between what is
principled Biblically and what “really works” in history. They
assume that God’s law is not designed to be applied in this world.
They forget that God created this world, established His laws to
govern it, and created man to administer it as His steward in His
name (Genesis 1:26-28). They forget that God’s law is practical
because the God who established it also runs the world. They
dismiss as “dead Old Testament doctrine” the specific promises of
God to His people, that if they obey His laws as a people, they will
receive external, visible blessings as a people (Deuteronomy
28:1-14), They also forget that He has promised to curse His peo-
ple visibly and externally if they disobey His law (Deuteronomy
28:15-68). On this point, the humanists agree completely.

Both groups— “otherworldly” Christians and “underworldly”
humanists —hasten to agree that God’s law is irrelevant in New
Testament times. Both groups agree that Christians are supposed
to be irrelevant in history. They all conveniently choose to forget

Other books by Gary North

Marx's Religion of Revolution, 1968
An Introduction to Christian Economics, 1973
Unconditional Surrender, 1981
Successful Investing in an Age of Envy, 1981
The Dominion Covenant: Genesis, 1982
Government by Emergency, 1983
The Last Train Out, 1983
Backward, Christian Soidiers?, 1984
75 Bible Questions Your Instructors
Pray You Won't Ask, 1984
Coined Freedom: Gold in the Age of
the Bureaucrats, 1984
Moses and Pharaoh, 1985
Negatrends, 1985
The Sinai Strategy, 1986
Unholy Spirits: Occultism and
New Age Humanism, 1986
Conspiracy: A Biblical View, 1986
Honest Money, 1986
Fighting Chance, 1986 [with Arthur Robinson]
Dominian and Common Grace, 1987
Inherit the Earth, 1987
Ts the World Running Down?, 1987
The Pirate Economy, 1987
Liberating Planet Earth, 1987
(Spanish) La Liberacién de la Tierra, 1987
The Scourge: AIDS and the Coming Bankruptcy, 1988
Tools of Dominion, 1988

Books edited by Gary North

Foundations of Christian Scholarship, 1976

Tactics of Christian Resistance, 1983

The Theology of Christian Resistance, 1983

Editor, Journal of Christian Reconstruction (1974-1981)

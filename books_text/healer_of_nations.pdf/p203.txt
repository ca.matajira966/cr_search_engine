Businessmen Make the Best Diplomats 189

policy, especially foreign policy, is to subject them to the full pres-
sure of international market competition, These people must
learn to compete in a world with low or no tariffs, import quotas,
export bounties, government-guaranteed loans, and similar re-
strictions on trade.

Market competition is a form of internationalism. Perhaps the
best known and most successful mechanism of free market inter-
nationalism was the gold standard, 1815-1914. It kept national in-
flationists under tremendous pressure for a century. Wholesale
prices in England were about 20 percent lower in 1914 than they
had been in 1821, the year Britain introduced a full gold coin
standard, with relatively little variation in between.

Free trade with allies and neutral nations— meaning peaceful
free trade with citizens everywhere—is perhaps the greatest in-
centive for contacts between missions-minded societies and those
that are still pagan. It was Israel’s key location on the trade routes
of the ancient world that enabled them to demonstrate to many
foreign visitors the wonders of Biblical law (Deuteronomy 4:5-8).

This is not necessarily an argument for trading with self-
declared enemies of the West, which the Communist empires
surely are. Yet, even in this case, for a Christian nation that is
self-consciously on the offensive, free trade with identified ene-
mies during peacetime could become a successful weapon. Just as
we send medical missionaries abroad, just as we allow the Red
Cross to send medicine to all combatants, so the gospel can
penetrate enemy nations through their demand for goods and
skills produced in productive Christian societies. The decision to
trade or not to trade with a self-identified enemy should be made
by the Commander-in-Chief of the armed forces, on the advice of
senior military experts, subject to legislation to the contrary by
the legislature. The decision should not be made by the foreign
policy professionals. (It is not surprising that the Council on
Foreign Relations systematically recruits our senior military

24. Edwin Walter Kemmerer, Gold and the Gold Standard (New York: McGraw-
Hill, 1944), pp. 188-91.

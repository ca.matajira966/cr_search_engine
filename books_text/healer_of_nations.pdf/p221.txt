Alliances Are Not Covenants 207
will be characteristic of the millennial triumph of Christianity:

The eyes of those who see will not be dim, and the ears of those
who hear will listen. Also the heart of the rash will understand
knowledge, and the tongue of the stammerers will be ready to
speak plainly. The foolish person will no longer be called generous,
nor the miser said to be bountiful; for the foolish person will speak
foolishness, and his heart will work iniquity: to practice ungodli-
ness, to utter error against the Lorn, to keep the hungry unsatis-
fied, and he will cause the drink of the thirsty to fail. Also the
schemes of the schemer are evil; he devises wicked plans to destroy
the poor with lying words, even when the needy speaks justice. But
a generous man devises generous things, and by generosity he
shall stand (Isaiah 32:3-8).

Evil people will remain evil, but they will be readily identified,
and their schemes will fail. When Christians at last learn the skills
of judging between evil and good, between lies and falsehoods,
then they will be able to exercise dominion, God the Judge calls
His people to make judgments—accurate, God-honoring, Bible-
based judgments. Matthew 7 calls us to make judgments with the
judgments we want to receive; we are to judge positively, in terms
of the Bible, so that we are not judged falsely because we have
judged others falsely (Matthew 7:1-2).

Alliances to Spoil Satan’s House

A Christian nation should distinguish between six types of na-
tions: 1) Christian nations that are covenanted with each other; 2)
Christian nations that for some reason are outside the covenanted
group or groups; 3) pagan allies that are nonetheless on the side of
God’s representative nation or nations if war with pagan empires
breaks out; 4) pagan neutral nations that are sitting on the fence,
weighing costs and benefits of choosing one side or the other; 5)
pagan nations that are aligned with the empire; and 6) pagan em-
pires that are determined to serve as international satanic leaven.

The Bible forbids us to make covenants with His enemies.
“Covenant” is not some vague concept in the Bible. A prohibition
against making a covenant with another nation means that we

228 Healer of the Nations

sively domestic: to exclude small firms from the market. Only the big-
gest U.S. corporations in the West have the lawyers and political
connections to get their products out of the U.S. legally and then
inside the Soviet bloc nations. These trade restrictions serve only
to create monopoly opportunities for a handful of politically
favored Western corporations. The so-called “most-favored-
nations” trade agreements are in fact most-favored corporation agree-
ments for those doing business with those Communist nations that
are not on the most-favored-nations list. The favored corporations
are discreetly silent about their special immunities. For example,
the membership list of the secret tax-exempt foundation, the US-
USSR Trade and Economic Council,"® is impossible to obtain
legally. All we know is that major U.S. corporations advertise in
its journal, which is also extremely difficult to locate. (I have
photocopies of sections of this journal in my files, but not a com-
plete set.)

Thus, it is difficult to aid allies to fight wars against Soviet-fi-
nanced national liberation movements (revolutions). On what
basis could wealth transfers be made by a Christian nation? First,
by identifying enemies before a shooting war breaks out. They
should be dealt with as if war had broken out, except for actual
armed intervention. Thus, aid could be sent to allies who are at
war with such an enemy. We would then treat them as surrogates
(representatives) of our interests, fighting on their soil before we
have to fight on ours.

Second, in our day, foreign aid would be handled only by the
military services. All funds would come directly out of service
budgets. It would be clearly an aspect of military defense.
Military services could buy information from foreign intelligence
services, such as Israel’s Mossad. The Navy could pay to lease
bases on foreign soil. In short, the military services would buy for-
eign cooperation the same way they buy hardware. This would
remove foreign aid decisions from the peacetime bureaucracies.
(When the government of the Philippines suggested in June of

18, Joseph Finder, Red Carpet, pp. 254-60.

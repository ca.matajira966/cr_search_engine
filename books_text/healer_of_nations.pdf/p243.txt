Government Foreign Aid Disinherits the Faithful 229

1987 that the U.S. should pay for its military bases there,
Secretary of State George Shultz vehemently rejected such a sug-
gestion, insisting that the bases are for the defense of the Philip-
pines, and therefore are not space that the U.S. should lease. He
may have recognized that such leases would transfer power from
the Department of State to the Department of Defense.) Foreign
aid would be part of legitimate military operations rather than a
program of deliberately subsidizing evil with funds extracted from
the righteous on threat of violence.

Private Wealth Transfers

These should take two forms. First, open charity that is dis-
tributed by privately financed voluntary organizations. Mis-
sionaries should take the lead here. Second, capital investment
abroad.

This is no different from what should operate in a nation’s
domestic economy: voluntary charity and capital formation. The
tithe and the free market are the two economic engines of Chris-
tian dominion through inheritance. This keeps responsibility in
the hands of those spending their own money or serving as re-
sponsible individuals (trustees) who represent others. Their ac-
tions are restrained either by the donors or by the profitability of
their business ventures, They cannot steal capital from the right-
eous in order to give to unrighteous, soctalistic foreign govern-
ments or petty dictators.

The goal of charity is to build up the deserving poor, not the lazy
poor. Poverty is God’s judgment on lazy people and unrighteous
societies. The Book of Proverbs is filled with warnings against laz-
imess. The Book of Deuteronomy is filled with warnings against
national rebellion.

Capital will always flow into societies that do not confiscate
property though heavy taxation, A nation that honors the Biblical
principle of private property and equality of foreigners under the
law will have no trouble attracting foreign investment capital. In-
vestors are always seeking out places where their investments will
be legally protected. Capitalists are willing to take market risks if

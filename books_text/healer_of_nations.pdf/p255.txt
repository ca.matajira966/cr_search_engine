Conclusion 241

development of the Soviet-Chinese conflict. I emphatically deny
the paternity of any efforts to invoke that doctrine today in situa-
tions to which it has, and can have, no proper relevance.”!”? He
had learned first-hand the lesson of Western chess players: the
Soviets cannot be permanently contained by their enemies’ defensive
maneuvers. His implicit (though undeclared) solution: retreat in the
face of Communist expansion. He had become an isolationist.
Not a “bright example,” “city on the hill” isolationist of an earlier,
self-confident America, not a “build up the defense industry” mod-
ern conservative isolationist, but an “avoid confrontation for a
while longer, for all is lost in the long run” liberal isolationist. He
is an “eat, drink, and be melancholy, for tomorrow we die” liberal.
Most of the liberal humanist intellectuals—with the exception of
New York Jews (many of whom were ex-Marxists) who began to
see the light in the mid-1960’s'® —joined him in this shift.

In 1979, the Soviets invaded Afghanistan. Kennan, by now a
confirmed isolationist who saw the faint outlines of the handwrit-
ing on the wall for Western civilization, criticized U.S. politicians’
critical reaction for “a disquieting lack of balance...” He
pointed out that Afghanistan shares a border with the USSR, as
well as sharing “ethnic affinity on both sides of the border,” which
has created “political instability.” These specific factors all suggest
“defensive rather than offensive impulses,” he concluded.'® Defen-
sive measures? Did the Soviets fear an invasion by Afghanistan?
(Few Americans know that their tax dollars paid for the building
of the modern roads in 1966 down which Soviet tanks rolled in
December of 1979—U.S. foreign policy in action!)?¢

By 1980, Kennan’s “doctrine” of containment had become the
doctrine of delaying the inevitable defeat of the West. Thus,

17. Kennan, Memoirs, p. 367.

18. See, for example, Norman Podhoretz, The Present Danger: “Do We Have the
Will to Reverse the Decline of American Power?” (New York: Simon & Schuster, 1980).

19. George Kennan, The Nuclear Delusion: Soviet-American Relations in the Atomic
Age (New York: Pantheon, 1982), p. 162.

20. “Rugged Afghan Road Jobs Fill Gaps in Trans-Asian Network,” Engineer-
ing News-Record (Nov. 3, 1966).

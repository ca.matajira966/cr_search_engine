280 Healer of the Nations

for particular immigrant groups. For instance, Spanish-speaking
Latin American immigrants are going to be served by Roman
Catholics, Pentecostals, and Baptists. Individuals must pick
which church is closest to their views, and then finance that one’s
missions efforts or its local Spanish-speaking churches.

Foreign pen pals programs are a good idea for children.
Foreign children are trying to learn English. English-speaking
children can locate foreign students through a missions board and
start writing.

Specific Ministries

Several ministries have “shoebox evangelism.” Children put
together shoeboxes filled with soap, toothpaste, small toys, and
similar scarce items, and send them to a foreign missionary group
or other charity. Extremely important is the photograph of the
child who sends it. This photograph establishes visual contact
with the recipient child,

A very good example of such a program is the one operated by
Kefa Sempangi, a Ugandan pastor. He was a victim of Idi Amin’s
dictatorship, and suffered some harrowing experiences. He wrote
aremarkable book about his experiences, A Distant Grief. He oper-
ates an orphanage for 8,000 children. He says that the official gov-
ernment estimate is that there are at least 800,000 orphans in
Uganda, and this was before the AIDS epidemic broke out.

He related this story to me. After having received a shipment
of shoeboxes from American children, he tried this experiment.
He went before the children to tell them of some terrible people
who hate Ugandans, “They are called Americans.” He said the
response was immediate. “No, no!” they shouted. “Americans are
our friends. We have pictures of Americans. They are our
friends.”

IT asked him what they needed in Christian literature. “Any-
thing,” he said. They have no printing press, almost no Bibles, no
tracts, nothing. Uganda had been evangelized in the late-nine-
teenth century, but no Christian testimony remains. Idi Amin
was ruthless and efficient. Now the AIDS epidemic threatens the

284 Healer of the Nations

paying menial jobs. The humanist welfare State has made eco-
nomic cowards of the workers of the West, just as it has made mil-
itary cowards of the leaders of the West. The answer is faith in
God and God’s law:

“Only be strong and very courageous, that you may observe to
do according to all the law which Moses My servant commanded
you; do not tum from it to the right hand or to the left, that you
may prosper wherever you go. This Book of the Law shall not
depart from your mouth, but you shall meditate in it day and
night, that you may observe to do according to all that is written in
it. For then you will make your way prosperous, and then you will
have good success” ( Joshua 1:7-8).

Summary

There will be terror and consternation when economic disrup-
tions, Communist terrorism, AIDS, and political extremism hit
the United States and other Western nations. People will not
know where to turn. At that point, Christians had better be ready
with answers, both theoretical and practical. If they are unpre-
pared, as they are today, then those who take responsibility will
inherit the power, and they will not be Christians.

Christians should prepare themselves mentally for the crisis to
come. They must discipline themselves today to take minimal ac-
tion, so that they will have more experience later on.

International relations begins with contacts between people,
not governments, It is our job to establish personal contacts now,
before the crisis hits. We must do small things now to get ready to
do larger things later. We start with first steps. We cannot win the
whole world in one generation. But we must begin.

And we must never forget: winning the world to Christ means
winning it back from the clutches of the professional diplomats.
International relations begins with people, not diplomats.

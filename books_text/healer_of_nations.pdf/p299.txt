12
WHAT THE STATE CAN DO

Why do the nations rage, and the people plot a vain thing? The
kings of the earth set themselves, and the rulers take counsel
together, against the Lorp and against His Anointed, saying, “Let
us break Their bonds in pieces and cast away Their cords from us.”
He who sits in the heavens shall laugh; the Lor shali hold them in
derision (Psalm 2:1-3).

‘We are in a war that has been going on for a long time. The
theological issues have not changed. The issue is the sovereignty of
man vs. the sovereignty of God, man’s covenant vs. God’s covenant.

This does not mean that nothing has changed. A great deal
has changed. Faces and tactics have changed; capital and technol-
ogy have changed; and most important of all, time has changed.
Satan is closer to the end, as are his followers. The shift from ra-
tionalism to irrationalism that the West has seen since 1965, paral-
leling the shift from science to occultism, indicates that we are
close to the end of an era. Humanists are reverting to older, occult
forms of humanism, indicating that Satan is just about out of new
diversions and new visions. His followers are returning to the “old
time religion” —the second-oldest religion. Such is the fate of the
power religion. Its underlying faith in occultism rather than ra-
tional science has begun to manifest itself, just as C. S. Lewis said
it would in his great novel, That Hideous Strength (1945).

Unfortunately, Christianity also slipped backward theologi-
cally and culturally at the beginning of this century.t American

1. George Marsden, Fundamentalism and American Culture: The Shaping of Feventicth-
Century Evangelicalism, 1870-1925 (New York: Oxford University Press, 1980).

285

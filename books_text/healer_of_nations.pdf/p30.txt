16 Healer of the Nations

Bryton Barron, a conservative State Department official,
resigned on February 1, 1956. He and Dr. Donald Dozer had been
involved in editing the Yalta Conference papers and the Tehran
Conference papers, respectively. Both claimed that the published
documents had been altered significantly from the originals, and
many important documents had not been released. Barron wrote
a limited-edition book, Inside the State Department: A Candid Ap-
praisal of the Bureaucracy, later that year. In its introduction, he
made this statement: “This globe-encircling bureaucracy which is
the Department of State, with its thousands of employees in
Washington and many thousands more all over the world, is
fifteen times larger and many times more costly than when I first
knew it. Operating behind a curtain of regulations which conceals
its workings and protects this bureaucratic empire, the Depart-
ment is now almost beyond the reach of Congress and the people.
Unlike other departments, it does not have to submit an annual
report. It cannot be required to show important papers to the
Congress.”

He had joined the Department in 1929. It had grown 15-fold
by 1956. It has grown almost five-fold since 1956. It now has so
much autonomy, that only one government agency in Washington
can claim greater autonomy, the Federal Reserve System, which
in fact is not really a government agency. (In the Washington,
D.C. telephone book, only the Board of Governors of the Federal
Reserve System is listed under “U.S. Government.” The other
operations are not. Regional Federal Reserve Banks pay postage;
they do not enjoy the free mail “franking” privilege that belongs to
U.S. government agencies.) It is these two organizations that are
the heart of the invisible government of the United States,

A Century of Bureaucratization

Richard W. Leopold’s standard history of U.S. diplomacy has
about eight hundred pages of text. Slightly under one hundred

19. Bryton Barron, Inside the State Department: A Candid Appraisal of the Bureau-
cracy (New York: A Reflection Book, Comet Press Books, 1956), p. 12.

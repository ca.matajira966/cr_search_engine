308 Healer of the Nations

Crucial to this operation is a complete housecleaning of every
present official in charge of broadcasting news into the Soviet
Union. Fire them, transfer them, retire them, or even keep them
on the payroll as consultants—and never, ever consult them.
Keep the foreign language announcers, if they are known to have
a good audience, Everyone else must be removed from any area
of responsibility, for they are the appointees of today’s liberal hu-
manist elite. They will torpedo the shift to offensive broadcasting.
Get them out.

New technologies exist for beaming radio broadcasts into
Communist nations. These should be financed and used.

Democratic humanism suffers from a major propaganda prob-
Jem: there is no universal consensus of the voters concerning what
is most positive about the West. To broadcast positive features of
Western religious life is anathema to humanists. Yet you can’t
fight something with nothing. You can’t successfully fight hard-
core humanism with soft-core humanism.

This is why Christians must pioneer the propaganda effort
privately until the West returns to the covenantal faith that made
the West possible in the first place. Let the President go on televi-
sion and call for tax-deductible donations to Christian-oriented
radio broadcasting beamed into the Soviet Union. Let him pro-
mote Brother Andrew’s Open Doors program of smuggling in
Bibles.

By all means, he should focus on all aspects of religious
persecution. He should publicly call attention to the desire of Jews
to emigrate,

The Soviet Union must be embarrassed at every point.

Creating Internal Disruptions

No more trading with the enemy, unless approved by the mili-
tary high command, and unless they allow missionaries to enter
the country and hand out Bibles and literature. No more taxpayer-
guaranteed loans at below-market interest rates. No private loans
to Communist nations. A war is in progress.

No more summit conferences between the President and

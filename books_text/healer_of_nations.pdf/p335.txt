timid, 249

USSR, 263

weakness imitated, 286
church

American, 267

body of Christ, 25, 264-65

bondage, 82

covenant, 5

defensive?, 14

dominion, 82

failure, 261

fragments, 260

Reography, 30

God's assignment, 10

inheritance, 52

kingdom &, 4

loser?, 21

members, 264

model, 4, 5, 10, 73, 206, 259

mountain imagery, 156

new nation, 52

of churches, 10

state &, 10

sword & 30

taxation by, 29-30

traditions, 266-67

unity, 10, 46, 53, 160, 164,

235, 260-61

utopian?, 53

“Zion 33
Church Age, 57, 104, 273
Citicorp, 223-24
citizenship

adoption, 49

bload, 48

classical, 147

covenantal, 152

dual, 141, 142

empire, 87, 88

heaven, 139

heavenly model, 142

historical manifestation, 140

inclusion-exclusion, 143

Subject Index 321

judges, 36
jury system, 36
missionary’s, 165
under biblical law, 36
world, 146
civil defense, 14
Civil Service, 131,133, 305
clans, 147
Clark, Grenville, 95-96
Claude, Inis, 123-24
Clausewitz, Carl, 100
colonialism, 166-70
colonies (Western hemisphere), 186
Colorado River, 50
comprehensive redemption, 255
common grace, 209, 222, 265;
see also natural law
Communion, 10
Communism
chariots &, 107
containment, 238-42
confederation vs., 63
diplomatic relations, 117
exclusion, 146
five points, 237
family of nations, 78
gun barrel, 86
humanism &, 38
intellectuals &, 106
loss of faith, 39
messianic, 168
rolling back, xi, 239-40
Satanic, 237
time frame, 237, 238
victory over, 242
war to the death, 133
Western support for, 38-39
Communist Party, 101
competition, 180, 109
confederation, 51, 58, 103, 159,
206, 272
confessions, 48, 53, 68; see also oaths
conflict, 99, 152

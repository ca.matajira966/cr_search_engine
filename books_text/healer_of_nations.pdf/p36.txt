In that day five cities in the land of Egypt will speak the
language of Canaan and swear by the Lorn of hosts; one will be
called the City of Destruction. In that day there will be an altar to
the Lorp in the midst of the land of Egypt, and a pillar to the
Lorp at its border. And it will be for a sign and for a witness to the
Lop of hosts in the land of Egypt; for they will cry to the Lorp
because of the oppressors, and He will send them a Savior and a
Mighty One, and He will deliver them. Then the Lorp will be
known to Egypt, and the Egyptians will know the Lorp in that
day, and will make sacrifice and offering; yes, they will make a
vow to the Lorp and perform it. And the Lorp will strike Egypt,
He will strike and heal it; they will return to the Lorp, and He
will be entreated by them and heal them. In that day there will be
a highway from Egypt to Assyria, and the Assyrian will come into
Egypt and the Egyptian into Assyria, and the Egyptians will serve
with the Assyrians. In that day Israel will be one of three with
Egypt and Assyria, even a blessing in the midst of the land, whom
the Lorp of hosts shall bless, saying, “Blessed is Egypt My people,
and Assyria the work of My hands, and Israel My inheritance.”

Isaiah 19:18-25

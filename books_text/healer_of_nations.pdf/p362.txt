348 Flealer of the Nations

The Marxists have had such a vision, or at least those Marx-
ists who don’t live inside the bureaucratic giants called the Soviet
Union and Red China. The radical (please, not “fundamentalist”)
Muslims of Iran also have such a view.

Now, for the first time in over 300 years, Bible-believing
Christians have rediscovered these four points in the theology of
Christianity. For the first time in over 300 years, a growing num-
ber of Christians are starting to view themselves as an army on
the move. This army will grow. This series is designed to help it
grow. And grow tougher.

The authors of this series are determined to set the agenda in
world affairs for the next few centuries. We know where the per-
manent answers are found: in the Bible, and only in the Bible. We
believe that we have begun to discover at least preliminary an-
swers to the key questions. There may be better answers, clearer
answers, and more orthodox answers, but they must be found in
the Bible, not at Harvard University or on the CBS Evening
News.

We are self-consciously firing the opening shot. We are calling the
whole Christian community to join with us in a very serious de-
bate, just as Luther called them to debate him when he nailed the
95 theses to the church door, over four and a half centuries ago.

It is through such an exchange of ideas by those who take the
Bible seriously that a nation and a civilization can be saved.
There are now 5 billion people in the world. If we are to win our
world (and these billions of souls) for Christ we must lift up the
message of Christ by becoming the city on the hill. When the
world sees the blessings by God upon a nation run by His princi-
ples, the mass conversion of whole nations to the Kingdom of our
Lord will be the most incredible in of all history.

If we’re correct about the God-required nature of our agenda,
it will attract a dedicated following. It will produce a social trans-
formation that could dwarf the Reformation. This time, we're not
limiting our call for reformation to the institutional church.

This time, we mean business.

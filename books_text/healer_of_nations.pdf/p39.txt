God Created the Nations 25

vast majority of cases in the Old Testament, the English word “na-
tion” is based on the Hebrew word goy.

The New Testament Greek word is transliterated ethnos. The
English word “ethnic” comes from this Greek word. Kittel’s Theo-
logical Dictionary of the New Testament defines ethnos as follows: “This
word, which is common in Greek from the very first, probably
comes from ethos, and means ‘mass’ or ‘host’ or ‘multitude’ bound
by the same manners, customs or other distinctive features. Ap-
plied to men, it gives us the sense of people; but it can also be used
of animals in the sense of ‘herd’ or of insects in the sense of
‘swarm.’. . . In most cases ethnos is used of men in the sense of a
‘people,’” The word “ethnos” is used to describe Israel in many in-
stances. It is not exclusively used to specify gentile nations.?

If a nation is a collection of people, in the sense of a swarm of
bees, then two questions immediately arise. First, on what basis
do individuals inc/ude others in a collective unit with each other?
Second, on what basis do members of one group exclude other peo-
ple? The very word “member” indicates the inability of people to
speak of associations and collectives in general without using the
organic analogy of the human body, as Paul speaks of the Church
as a body in Romans 12 and 1 Corinthians 12. Parts of a body are
called “members.”

The Biblical concept of “nation” is related closely to the Bibli-
cal concept of government. We must begin our search for a Bibli-
cal definition of “nation” with a study of the Biblical doctrine of the
covenant,

Government Is Covenantal

The covenantal nature of all institutional government—
church, State, and family —is surveyed in two books in the Bibli-
cal Blueprints Series, Gary DeMar’s Ruler of the Nations and Ray
Sutton’s Who Owns the Family? The key book is Ray Sutton’s study

2. Gerhard Kittel (ed.), Theological Dictionary of the New Testament (Grand Rap-
ids, Michigan: Eerdmans, 1964), H, p. 369.

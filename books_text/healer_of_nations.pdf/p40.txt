26 Healer of the Nations

of the covenant, That You May Prosper.3

A government, Biblically speaking, is a monopolistic institu-
tion created by God. Membership in it is established by an oath or
vow before God, explicit or implicit (for example, registering to
vote), The oath places it under the sanctions of God: blessings
and cursings. A government is not merely an association, such as
a business or a club. Associations cannot legitimately invoke
God’s curses and blessings by means of a public oath. This is why
international alliances are not always covenants; alliances have no
shared oaths (see Chapter Nine). If alliances take on the charac-
teristics of international civil governments, but without shared
faith in the same God, then they become prohibited alliances,
what President Jefferson long ago called “entangling alliances.”
Civil government always exercises God-given authority as a rep-
resentative of God (Romans 13:1-7), either explicitly or implicitly.
Government is therefore representative: it represents God to man,
and man to God.

A Biblical covenant has five features:

1. An affirmation of the transcendence yet presence of God
2. A hierarchical system of appeals courts

3. Biblical law

4. A system of sanctions (blessings and cursings)

5. A system of inheritance or continuity

In short, a Biblical covenant is based on a shared faith in God.
Every imitation covenant also must offer its members a faith of
some sort: the pagan equivalent of the Biblical doctrine of the ne-
cessity of not being yoked unequally with unbelievers (2 Corinthi-
ans 6:14). This necessary unity of confession must be enforced by
a sovereign agent who will guarantee the integrity and reliability
of the covenant. There are many modern imitation sovereigns:
the People, the Party, the Fuhrer, the dialectical forces of history,
the mode of production, the march of democracy, and so forth.

3. Ray R, Sutton, That You May Prosper: Dominion By Covenant (Tyler, ‘Texas:
Institute for Christian Economics, 1987).

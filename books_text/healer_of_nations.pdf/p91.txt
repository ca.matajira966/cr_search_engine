All Nations Under God 77

court of appeal. We need plural civil governments under God’s
law: township, county, city, state, nation, and international order.

The idea of Christian civilization is valid. The idea of any
other kind of civilization is invalid. It is God's civilization or
man’s; there is no middle ground in principle. God definitely
desires for redeemed humanity to form a “one-world Christian
order’—but never a one-State order. This one-world Christian
order is not based on might or power, but on the Spirit of God:
“This is the word of the Lorp to Zerubbabel: ‘Not by might nor by
power, but by My Spirit,’ says the Lorn of hosts” (Zechariah 4:6).
The Christian idea of a one-world order is not statist, but spiri-
tual. A Christian one-world order can be brought into existence
only by the outpouring of the Holy Spirit. Those who deny that
such an outpouring will ever come in history therefore reject the
idea of a one-world Christian order, or else they predict its crea-
tion only after Jesus Christ has physically returned to earth dur-
ing the millennium, when He sets up a top-down international
bureaucracy.

Christopher Dawson gives us a picture of international Chris-
tian community when he writes concerning the Middle Ages: “For
a thousand years Christian Europe has existed as a true super-
natural society —a society that was intensely conscious of its com-
munity of culture in spite of the continual wars and internal divi-
sions that made up its history. . . . Today this is no longer so.
Europe has lost her unity and the consciousness of the spiritual
mission. There is no longer any clear line of division between
Christian and non-Christian peoples, and with the disappearance
of her Christian consciousness, Europe has begun to doubt her
own existence.”#

What Dawson is getting at is that in a Christian world there
are many nations, many cultures, many languages, and many
governments — but all share a common bond of faith and coopera-
tion. There is a true unity, but it is not a unity at the level of the

44, Christopher Dawson, The Judgment of the Nations (New York: Sheed and
Ward, 1942), p. 73.

Protecting Licensed Counterfeiters 97

Do you think your local bank could get a tide-me-over loan of
$22.6 billion?

Question: Why doesn’t the Fed buy these bonds directly? An-
swer: because it wouldn’t generate commissions for the favored 20
banks,

The government allows the central bank, legally a private or
ganization, to manipulate the money supply of the United States.
The central banks of every nation possess this same prerogative.
Why do. the governments tolerate it? Because they always need
‘money. The central banks stand as “lenders of last resort” to the
government.

The government pays interest on the Treasury bills held by
the Federal Reserve. It amounts to about $15 billion a year these
days, At the end of the year, the Fed sends back about 85% of this
money to the U. S, Treasury. It keeps 15% for “handling,” (It pays
for all check-clearing transactions in the U. S., for example.)

The Fed’s Declaration of Independence -

The Fed has never been audited by any agency of the United
States government. The Fed’s officials have resisted every effort of
any congressman or senator to impose an audit by the Govern-
ment ‘Accounting Office (GAO).

The Federal Reserve Board meets to formulate U.S. eco-
nomic policy every few months. No information on the Board’s
decisions can be released to anyone, including the President of the
United States, for 45 days. The Fed says so, and Congress won't
call the Fed’s bluff. It used to be 90 days, but Congress forced the
Fed to speed up the reporting date. Fed chairman Paul Volcker
protested strongly. He said such a release of information interferes
with the decision-making ability of the Fed.

The U.S. money supply is totally regulated by decisions of the
Board of Governors of the Federal Reserve System. The Fed es-
tablishes the “reserve requirements” of the commercial banks’
(10% ,5%, or whatever, depending on where the bank is located,
and whether it’s a checking account or a savings account). The
Fed buys or sells U.S. Treasury bills (U.S. government debt cer-

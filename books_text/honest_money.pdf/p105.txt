Protecting Licensed Counterfeiters 99

Whose reserves? Member banks. Who holds title? The Federal
Reserve System. Who owns the Federal Reserve? Member banks.

This leads me to conclude that if you're going to become a
counterfeiter, you might as well become an audacious one. The
backyard operators risk going to jail. Central bankers don’t.

Sure, by law, Congress and the President could demand that
the Fed sell the gold back at $42.22 per ounce. By law. Have you
ever seen anyone propose such a law? Has Congress ever brought
it up for consideration since 1918? Have you seen anyone discuss
the wisdom, or even the possibility, of such a law, except for
“kooks” who write newsletters and paperback books? Have you
ever heard of a Ph.D.-holding university economist recommend-
ing it? No? Neither have I.

Sure, Congress controls the Fed. Legally, the Fed must report
to Congress. Just as the Politburo in the Soviet Union must report
to the Russian people.

Congress can get its gold back any time it wants to. Just as an
alcoholic can quit drinking any time he wants to. Just as American
private citizens can get thar gold back from Congress at $42.22 per
ounce (or even at a market price), any time we want to.

If you believe this, I’ve still got that New York bridge to sell
you. ,

The Depositors’ Insurance

The Federal Deposit Insurance Corporation came into exist-
ence in 1934, the year after the government confiscated the
public’s gold. The FDIC is promoted as a government-guaranteed
ingurance program for private citizens’ bank accounts. Well, as
they say, yes and no.

No, it isn’t an agency of the Federal government. No, the gov- ‘
ernment has never promised to bail it out if it gets swamped with
banks that are going bankrupt. No, the Joint Resolution of Con-
gress in 1980 to insure every bank account up to $100,000 isn’t a”
law. The President never signed it, so it isn’t a law. There has
never been any such law.

Yes, if the FDIC really did look as though it was about to go

106 Honest Money

To fix a price between silver and gold brings Gresham’s law
into operation. The artificially overvalued currency will drive out
of circulation the artificially undervalued currency. Gold and sil-
ver aren’t immune from this law. of price-controlled moneys. Gov-
ernments, including the U.S. government, have tried to discover
“the” price between gold and silver, and invariably this has led to
the disappearance of one of the two metals. One of them will be
artificially undervalued in comparison to what the free market de-
termines. Fixing the exchange value between two forms of money
is just another fruitless example of government price controls.

Never forget, there’s no such thing as a price control. There
are only people controls. Price controls in fact restrict what people
are allowed to do. It interferes with their freedom.

Similarly, the government is not to set up price controls over
interest rates. Interest rate ceilings restrict the voluntary agree-
ments between borrowers and lenders. The State should enforce
all moral, legal contracts, and there is nothing in the Bible that in-
dicates that any particular rate of interest is immoral.

The Gold Standard

The gold standard is not theoretically preferable to any other
honest money standard. The only standard that matters is the no
fractional reserves standard, coupled with the uo false balances standard.

Gold historically has been one of the two most preferred stand-
ards, along with silver. It has all the characteristics of money: di-
visibility, transportability, durability, recognizability, and scarcity
(high value in relation to weight and volume). It has been a
money standard.

Most important, gold is a rare metal. It is therefore expensive
to mine. Not much new gold comes into circulation every year.
This keeps its price relatively stable but normally appreciating in
relation to mass-produced goods and services. Prices of goods de-
nominated in gold should normally be slowly falling in a produc-
tive, growing economy.

Governments should probably collect taxes in gold. Gold is
convenient. Income in other moneys can be computed (with

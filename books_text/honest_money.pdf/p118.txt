112\ Honest Mong

issued currency it will accept as payment of taxes.

9, The big banks eventually capture thecontrol over govern-
ment monetary policy, so get government out of the money busi-
ness,

10, There is no fixed value of gold or silver.

11, The State must not fix the price of anything, including the
exchange ratios between moneys.

12, Many moneys can exist in an economy.

18, A gold standard has been popular in history.

14, Gold and silver are expensive to mine; hence, they main-
tain their value relatively well.

16. Traditional gold standards are nevertheless State standards.

16, Freedom of money leads to parallel standards: no fixed
price between any two moneys.

17, Bank charters interfere with freedom.

18, For every loan there must. be a deposit of corresponding
maturity.

19, Banking can become a means of Christian dominion.

20. Christians extend credit; non-Christians borrow.

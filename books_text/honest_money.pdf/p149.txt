The Reform of Debt * 145

evil men, and a tool of dominion in the hands of God’s people.

God’s kingdom is to be extended over ethical rebels. Ethical rebels

are snared and brought under God’s authority by means of debt.
But what about the modern world?

The Book-Value Game

The insolvent deadbeat governments to which our banks’ loan
officers have lent your money and mine are now “in the driver's
seat.” They call the shots. We see today the truth of that old
slogan:

I owe you $10,000. You've got me.
I owe you $10,000,000. I've got you.

Only they don’t owe us $10 million. They owe the West’s banks
about $500 billion. Not only do they have us, they have us in a
death grip. They can bring down the West’s economies overnight,
or close to it.

The banks have begun to stop lending to them. The banks do
allow them to “restructure their debt,” meaning the banks extend
the day of repayment. Why? Because they expect to be repaid?
Nonsense; they know the money will never be repaid. They are
playing the big banks’ most important game, the bock-value game,

Would you pay $100 cash for a Mexican bond with a face value
of $100? Not if you’re smart. You would discount it by 50% or
more, since you know it’s a high-risk bond. With oil at $15 a bar-
rel, you might discount it by 7570. They can’t repay. They have
said so publicly,

But if you area bank, and you are sitting on top of $100 mil-
lion in Mexican bonds, the government allows you to keep those
bonds on the books at whatever you paid for them originally. The
regulators allow you to keep them listed at the price you put down
in your account books, back when there was a high market value
for those bonds (when oil was at $30, and before the Mexican gov-
emment nationalized the banking system and allowed the peso to
fall from 8 cents to one-seventh of a cent). In other words, when
you lent them the money before August of 1982. (Less than four

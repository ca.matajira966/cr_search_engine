The Value of Mong 9

In short, value is subjective. The economist uses fancy lan-
guage and says that Crusoe imputes value to scarce resources. He
decides what it is he wants to accomplish, and then he evaluates
the value to him personally of each tool. In other words, the value
of the tool is completely dependent on the value of the toal’s expected fu-
ture output. He mentally calculates the future, value of the expected

future output of each tool, and then he makes judgments about the

importance of any given tool in producing this output. Then he
calculates how much time he has until the ship sinks, how much
weight each tool contributes, how large his raft is, and how
choppy the water is. He selects his pile of tools and other goods ac-
cordingly.

In other words, he doesn’t look to the past in order to evaluate
the value to him of any item; he looks to the future. The past is
gone. No matter what the goods cost originally, they are valuable
now only in terms of what income (including psychic income) they
are expected to produce in the future. Whatever they cost in the
past is gone forever. Bygones are bygones. The economist calls
this the doctrine of sunk costs. In the case of Crusoe’s ship, that’s
exactly what they are about to become: sunk. That’s why he has to
act fast in order to avoid losing everything.

There are objective conditions on the island, and the various
tools are algo objective, but everything is evaluated subjectively by
Crusoe. He asks the question, ‘What value is this item to me?”
His assessment is the sole determining factor of what each item is
worth. He may make mistakes. He may re-eyaluate (re-impute)
every item’s value later, when he better understands his conditions
on the island. He may later wish that he had picked up some other
item instead. The point is, it’s his decision and his evaluation that
count. Because he is all alone, he and he alone determines what ‘
everything is worth. He doesn’t ask, “How much money did this
item cost in the past?” He asks instead, ‘What goods and benefits
will it produce for me in the future?” Then he makes his choices.
He allocates the scarce means of production. He allocates some to
the raft and the rest he leaves on the sinking ship. He loads his ,
top-priority items onto his raft, and floats it back to shore.

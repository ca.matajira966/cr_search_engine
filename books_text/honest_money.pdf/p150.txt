146 Honest Mong

years: that’s how fast Mexico’s economy, collapsed.)

If the government required the big New York banks to list
their assets at true market value, every one of them would be
legally bankrupt: more liabilities than capital assets in reserve. So
the government doesn’t require them to do it.

But to keep the game going, there has to be an illusion that the
bonds are still good. Banks have to get some interest payments oc-
casionally. If ever there were an outright default by the Third
World debtors, the game would end — or, better put, it would have
to be changed very fast, with full government cooperation. Once
the foreign nation says, ‘We quit. We aren’t paying,” the bonds
fall to zero value. Then the banks do have to write them down to
market value. Then there is a true banking crisis.

So the bankers play the game as long as they can. The Third.
World debtors also play along, just as long as they don’t really
have to sacrifice, stop inflating, and turn their nations into pro-
ductive non-socialist societies.

But someday they will get tired of playing the game. They will
get tired of being told by the West to “get your economic houses in
order, you deadbeats!” And they will say, in so- many words,
“Adios, gringos!” Then the bankers will have to scurry around and
find a new solution.

Some Quick Fixes
I assume that the Fed has some taxpayer-financed aces up its
sleeve. The game can go on a bit longer. Let me discuss a couple

of them that even I can figure out, and the Fed’s economists are
employed full-time to think of even better schemes.

The Debt Swap

The Federal Reserve System is sitting on top of about $175
billion in 90-day Treasury debt certificates, the most safe and
secure debt instrument in the world (the U.S. government. keeps
telling us). So, if it looks as though a major default is coming, the
Fed could simply swap some of its $175 billion in instantly salable
T-bills for the near-dead Third World bonds that the private com-

22 Honest Money

her forehead and earrings. “Thus you were adorned with gold and
silver, and your clothing was of fine linen, silk, and embroidered .
cloth. You ate pastry of fine flour, honey, and oil. You were ex-
ceedingly beautiful, and succeeded to royalty” (Ezekiel 16:13).

The Most Marketable Commodity

Gold has been the most marketable commodity fur thousands
of years, A seller of gold has not had to stand in the streets despe?-
ately begging people to consider bu ying his gold. If anything, he
has needed bodyguards to keep people from stealing his gold.

Understand from the beginning that the State was not neces-
sarily a part of the development of gold and silver as money.
There is nothing in the Bible that indicates that gold and silver
became money metals because Abraham, Moses, David, or any
other political leader announced one afternoon: “From now on,
gold is money!” The State only affirmed what the market had cre-
ated. It collected taxes in gold and silver. It thereby acknowledged
the value which market forces had imputed to gold and silver. But
the State didn’t create money.

Notice also that if Mises’ argument is correct concerning the
development of money, the original money units must have been
commodity-based. If the unit of account (for example, gold) must
have come into popular use because of its past value, at some
point we must conclude that it was valuable as a commodity for
some benefit that it brought besides serving as the most market-
able commodity: money. Money had to start somewhere. It had
to originate sometime. Before it was money, it must have been a
commodity.

In short, money was not originally a piece of paper with a poli-
tician’s picture on it.

Money and Taxes
There is no doubt that the State can strongly influence the
continuation of one or more metals as an acceptable unit of
money. All the State has to do is to announce: “From now on,
everyone will be required to pay his taxes in a particular unit of

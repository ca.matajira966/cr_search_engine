The Origins of Money 23

account .” After all, taxes are an expense. There is no escape from
death and taxes. (But, fortunately, the death rate doesn’t go up
every time Congress meets.) The State has power. If it says that
people must pay their taxes in a particular unit of account, there
will be strong incentives for people to store up this form of money.

Still, the State doesn’t have an absolutely free hand in select-
ing this unit of account. If it imposes on people a legal obligation
to pay what the people cannot actually gain access to, there will be
no revenues. In the Middle Ages, for example, there were no gold
coins in circulation in Western Europe until the mid-1200’s. There
was no way that a king or emperor could compel people to pay
gold in the year 1100 or 900, because his subjects couldn’t get any
gold. They had nothing valued by the East (Byzantium, the East-
ern Roman Empire) that could be exchanged for gold.

The Bible is clear: taxes to the State were paid both “in kind”
(a tithe of actual agricultural production: 1 Samuel 8:14-15) and
“in cash,” meaning silver. A head tax was required when the na-
tion was numbered immediately before a military conflict (Ex-
odus 30:12-14) — the only time that it waslawful for the State to
conduct a census, as King David later learned (2 Samuel 24:1-i7).
Solomon collected 666 talents of gold (1 Kings 10:14), presumably
from taxes, gifts from other nations, and from the sale of any agri-
cultural produce he collected. (We aren’t told where he got this
huge quantity of gold.)

Tribute in silver and gold was paid to a militarily victorious
State. There were incidents when Israel had to pay such tribute
(2 Kings 15:19; 23:33) and also when foreign nations paid tribute
to Israel (2 Chronicles 27:5).

The State also hired military forces with gold (2 Chronicles
25:6). Thus, taxes came into the treasury in the form of silver and
gold, but then expenditures by the State came back out in the
same form. There is no doubt that this process made silver and
gold the familiar forms of money in the ancient Near East. There
are plenty of examples in ancient records from other Near East
societies that they asked for tribute in gold and silver. It was the
common currency of the ancient world.

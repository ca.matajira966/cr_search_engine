Maintaining Honest Money 35

The key word is represent. The paper money, to remain honest,
must be issued by the money-issuer on a strict one-to-one basis. If
it announces that it represents a one-ounce gold coin, .999 pure,
then the issuer must have that one-ounce coin in reserve, ready to
be redeemed by anyone who walks in and presents the piece of
paper.

To issue. a piece of paper that serves as an IOU for precious ,
metals without having 100% of the promised metal in reserve is
fraudulent. It is theft. It is a form of tampering with weights and
measures.

How would such a system work? The coin owner might
deposit his coins at a warehouse. He wants his coins kept safely.,
He pays a fee for the safekeeping, the same way we rent safety
deposit boxes at our bank. The warehouse issues a receipt. Since
the receipt promises to pay the bearer a specific amount of coins,
or ingots, on demand, the paper circulates as if it were gold,
assuming that everyone knows and trusts the warehouse that
issued the receipt.

Warning: whenever someone promises to store your precious
metals for free, watch out. You never get something for nothing.
Either there is a hidden payment, or else there is fraud. Any sys-
tem of paper money or credit that doesn’t somewhere involve a fee
for storage is unquestionably and inevitably fraudulent. Keep
looking until you identify the form of fraud.

The paper certificate is a metal substituie, sometimes called a ‘ “
money substitute. But it isn’t a money substitute; it really is money, if
the metal on reserve 18 regarded as money. Its value in exchange rises or
falls according to the exchange value of the money metal in
reserve.

The big problem is counterfeiting. It is a lot easier to counter.
feit a piece of paper than it is to counterfeit a gold coin, A counter-
feit coin is easier to detect. It can be weighed. A piece of paper
looks just like other pieces of paper. So issuers take care to identify
pieces of paper by serial numbers, or special water marks, or by
using special paper that is easy to identify by the public.

A counterfeiter is clearly a thief when he prints up false

Debasing the Currency 45

replaced silver in the ingot. But understand: this extra silver can
be translated into profit only by “spending it into circulation”— in
other words, by selling it in exchange for additional goods and
services,

Who Wins, Who Loses?

Our world is a world of scarcity. It is also a world of God-
imposed law. The rule says: “You never get something for noth-
ing, except as a gift .” This testifies to God’s mercy in redeeming
us: salvation is a gift: “For by grace you have been saved through
faith, and that not of yourselves: it is the gift of God” (Ephesians
2:8). But this gift had to be paid for: Jesus died on the cross to
meet God’s stiff requirements against sin. “But God demonstrates
His own love toward us, in that while we were still sinners, Christ
died for us” (Remans 5:8).

So if someone is able to win by cheating, someone else
becomes a loser. It is not a question of everyone winning because
there has been new wealth created in the economy. It is profit
based on deception. No new wealth has been created. Someone
has to lose. :

The person who buys the “silver” ingot uses it for something.
Perhaps he makes an ornament. He sells the ornament, or barters
with it. But the buyer gets stuck with an ornament which is over-
priced. Why? Because the original cheater can collect his profit.
only by selling the extra silver into the market. Someone will
make more ornaments (or whatever) with this extra silver. The
supply of “silver” ornaments goes up; therefore, the value (price)
of the existing supply of “silver” ornaments will drop. The early
buyer has overpaid.

What if the cheater just produces ingots, and “spends them
into circulation”? He trades the debased ingots for something he
wants. If whoever sells him what he wants then turns around and
sells the newly produced debased ingot for whatever he wants, he
will not be hurt economically. The secret is this: sell the ingot before a
lot more phony silver ingots hit the market. In other words, “get while
the getting is good.” Or “take the money and run’ run to the

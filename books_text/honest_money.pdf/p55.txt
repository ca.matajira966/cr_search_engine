5
THE CONTAGION OF INFLATION

Your silver has become dross, Your wine mixed with water
(saiah 1:22).

In Chapter Four, I focused on the actual process of debasing a
precious metal currency. I discussed Isaiah’s accusation against
the rulers that they were not enforcing God’s law, and this was re-
flected in the corruption of the nation’s silver. He used-this to
point out the spiritual rebellion of all the people, from the highest
to the lowest. The corruption had spread from top to bottom, and
back again.

If moral corruption is widespread, then there is no more char
acteristic sin than monetary debasement, for money is the com-
mon medium of exchange. Everyone in an advanced economy
uses it. Thus, if the money is corrupt, everyone will eventually
recognize this. Corrupt money testifies to the corruption of the
producers of money, the defenders of money, and the users of
money. Corrupt money testifies to corrupt people.

Where we find debased money without widespread protests,
we also find debased people. The people of Judah could see the
debased money. This is why Isaiah called it to their attention. He
knew that they would recognize the truth of what he said concern-
ing their corrupt money; he used this to call their attention to their
corrupt hearts. He started with the simple, and worked toward
the more subtle. (Jesus did the same thing when he used parables
to make His points: “pocketbook” parables and agricultural
parables.)

Once the process of moral debasement begins to spread, it is,

“4°

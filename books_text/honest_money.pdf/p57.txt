The Contagion of Inflation 51

something else instead, and then the high-price producers have to
lower prices or suffer losses.

It is obvious that if good wine were easy to produce in huge
quantities, consumers would not have to bid so much money to
buy the wine. But it isn’t cheap to produce in large quantities, so
they do have to pay high prices.

Now, let’s return to the problem that faced the prophet Isaiah:
the moral corruption of the people. It was not just the silversmiths
who were corrupt. It was the winemakers, too. It was everyone.

How did the winemaker practice his corruption? By a process
almost. identical to that of the silversmith: debasement. The silver-
smith was pouring cheaper base metals into the molten silver, and
calling the product silver. This was precisely the process of the
corrupt winemaker. He was pouring “debased wine” (water) into
the pure wine, and calling it pure wine.

How could he make his profit? The same way the corrupt sil-
versmith made his, He would displace pure wine when he poured
in the water. This displaced wine could then be used to pour into
other wineskins along with more water. Then he could take, say,
20% more wineskins full of “wine” to market and sell them.
Presto: a 20% profit, at least initially.

He was trading on his own former reputation. Before, he had.
produced a high-quality product (just as the silversmith had
formerly produced). People trusted his products because they trusted his
morals. So he could take advantage of this trust by pouring water
in the wine. He was simply imitating the silversmith.

But moral corruption being what it is, it never stays in one
place. It gets worse. So more and more water would wind up in
the wine, just as more and more dross would windup in the silver.
Pretty soon, everyone would begin to see that a particular silver-
smith’s silver was mostly dross, and a particular winemaker’s wine
was mostly water. At that point, ‘people would stop doing business
with these corrupt people, or else start offering them fewer
valuable goods and services in exchange.

Unless... .
Unless the existing silversmiths were acting as a giant monop-

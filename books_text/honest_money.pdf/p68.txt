62 Honest Money

over the last two thousand years or 80.)

The government then spends these extra coins into circula-
tion. It makes little difference in the long run whether it’s the gov-
ernment or a private silversmith who does this. The result is more
coins in circulation. Prices will eventually go up. The trick is to
spend the debased money before everyone else catches on and
hikes selling prices.

There is a new problem, however. The people may trust the
State more than they trust private silversmiths. They think that
the State is honest. In the old days, they thought the State was
divine. Thus, when the State starts producing debased money, it
threatens people’s confidence in law and order. In the ancient
world, it made people doubt the honesty of the gods.

We are back to God’s laws regarding honest weights ‘and
measures. If God is the Judge, then His lawful representatives in
the civil government should not cheat. To cheat here is to call into
question the reliability and the integrity of God.

The State may be able to get away with the debasement proc-
ess longer, since people trust the State. But coins are coins,
more of them are coming into circulation, people are building up
a supply in reserve. Why not spend some of the extra ones? As
they are spent, prices begin to climb, compared to last year’s
prices, which were produced by an economy with fewer coins in
circulation.

Ultimately, it doesn’t matter who produces the coins. People
will respond to the new conditions of the supply and demand for
money. If there is a greater supply of money, the price (exchange
value) of the money will drop. Holders of cash will be hurt.

A New Form of Debasement
The trouble with money metals from the politicians’ point of
view is the very measurable character of metal. If a user can
measure it and weigh it, he can tell if someone has added a
cheaper metal to the precious metal. The coin’s weight will
change. It also starts to change color as more and more base
metals are added. Then everyone finds out about the corruption.

and if

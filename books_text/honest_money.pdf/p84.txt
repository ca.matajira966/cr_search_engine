78, Honest Mong

asset. A man’s reputation as an honest and efficient businessman
who pays his debts (and has few of them) is certainly a valuable
asset. It can be borrowed against under certain circumstances.
Certainly, by putting collateral against the loan, he adds to his
credibility.

Why should someone with a great idea to serve consumers but
without enough cash on hand to finance the initial delivery of this
service or product not be allowed to seek out other people to put
up the money? Why shouldn’t others be allowed to share this vi-
sion and share in the rewards? Some people may want an “equity”
position: shares of ownership in the business, rain or shine, boom
or bust. Others may not want to become entrepreneurs, but they
are willing to forgo the use of their money for an interest return.
Jesus teaches in His parable of the talents that both kinds of in-
vestments are legitimate: higher-risk profit seeking, as well as
guaranteed-return interest seeking.

The fundamentals of Biblical banking are these:

1, The King James translators erroneously translated the
Greek word for “interest” as ‘usury.”

2, Usury in the Old Testament refers exclusively to interest
taken from a poor fellow-believer.

3. Jesus described the kingdom of God in terms of profit seek-
ing and interest seeking: a positive rate of return.

4, Interest is a basic category of human action; it is in-
escapable,

5, It arises from the fact that we discount the present value of
future goods as against what those same goods are worth to us
right now.

6, Information isn’t a free good; someone pays for it.

7. Some people prefer to lend money at a high enough rate of
interest, as a means of providing for themselves in the future.

8. Other people have needs and opportunities that they prefer
to satisfy now, and pay for through interest owed in the future.

9, Middlemen bring these two sorts of people together; these
middlemen are called bankers.

10, Their information isn’t free.
11, They make their money through the “spread”: the difference

54 House Divided

since postmillennialist have a positive hope for the future of the
Jews which is somewhat different from the dispensationalist’s
positive hope for the future of the Jews, House and Ice feel they
are allowed to conclude that postmillennialist are actually negative
about the Jews (“anti’-Semitic) — so that a word with extreme,
emotive and horrible connotations of Jewish hatred may be ap-
plied to them. The authors ought to be ashamed of themselves for
talking this way. They should also realize that the same line of
fallacious thinking could just as easily (and fallaciously) be used
by postmillennialists to accuse them of “anti-Semitism.” That is,
since House and Ice do not agree with the postmillennialist’s
positive hope about the future of the Jews, the dispensational view
is not really positive at all, but actually negative. Dispensational-
ists are thus ‘anti-Semitic.” What is sauce for the goose is sauce for
the gander.

A Taste of Their Own Medicine

Perhaps by means of a few more “counter-examples” we can
drive home the extremity of the inappropriateness and insult in-
volved in the kind of criticism (“potential dangers”) House and Ice
have taken up against Reconstructionism. “For with what judg-
ment you judge, you shall be judged; and with the measure you
mete it out, it shall be meted out to you” (Matt. 7:2).

Notice, first, the general form of this reasoning, taking the ab-
breviation MUD for any monstrous unbiblical doctrine or prac-
tice you choose. “(a) Our theological opponents say nothing in
their writings which indicts them of MUD, are not actually guilty
of MUD, do not endorse MUD, work to avoid MUD in them-
selves, and actually take a public stand against MUD in others.
(b) However, their theological system does not render it abso-
lutely, personally impossible for some individual holding it to be
later persuaded to depart from the position and be ‘tempted’ into
the MUD. (c) Indeed, a few of the characteristics or beliefs of
those who promote MUD can also be found in our opponents,
despite their opposition to MUD. (d) It is then ‘possible’ that our
opponents are not really, completely clean of MUD. (e) Moreover,

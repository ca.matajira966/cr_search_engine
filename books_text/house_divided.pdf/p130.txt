The Failure of Accurate Portrayal 81

life in terms of God’s revelation. °°

@The way in which House and Ice try to set Van Til’s “apolo-
getics at odds with the postmillennial spirit”® is equally fraudu-
lent.®! In attempting to give an appearance of discrepancy, the
authors resort to attributing things to Reconstructionism which
are utterly false, thereby doing nothing more than tripping over
themselves. Postmillennialism does not, as House and Ice assert, in-
volve “improper synthesis with the world’® or require us to do
anything like “intermingle with non-Christian systems.”** That
surely would be contrary to Van Til’s emphasis upon antithesis.
But the remarks are utterly baseless —just as baseless as the claim
that “Van Til believed that orthodox Christians are to separate
from ... world systems, institutions, and, to some extent, society
itself.” Where ig Van Til supposed to have said such an isolationist
thing? Where does Reconstructionism “intermingle its Christianity
with non-Christian thinking” or “mix the holy with the common”? ©

59, Van Til commended and approved of the theonomic lectures delivered by
Bahngen at Westminster Seminary in March of 1980, and later Van Til wrote
these words about Babnsen’s extensive tape series giving a postmillennial inter-
pretation of the book of Revelation: “I have greatly profited spiritually and my
knowledge of Scripture expanded by the hearing of the lectures on ‘Revelation’

‘Greg Bahnsen., ,, These tapes will be a comfort toall those that hear them”
(Supplement Catalog of Mt. Olive Tape Library, p. 3). Apparently he did not
see or have concerns about any fundamental incompatibility.

60, House and Ice, Dominion Theology, p. 340,

61, It should be added that, in my CBshnsen" s) evaluation, Gary North’s at-
tempt to set Van Til’s view of common grace at odds with postmillennialism
@ominion and Common Grace [Tyler, TX: Institute for Christian Economics], 1987)
is also misconstrued, but this is not the place for a detailed analysis and critique
of that book also. (North’s own words are that Van Til “builds his whole theory of
common grace in terms of his hidden eschatology [an undeclared amillennial-
ism], probably never realizing” that this was the case {idid., p. 15]. North’s
examination is a drawn-out verbal dispute over talk about God’s “favor” and “ear-
lier grace.”) The important thing to observe here, though, is that the criticism of
‘one aspect of Van Til's writings by one Reconstructionist cannot cogently be
turned into Van Til expressing by his apologetical system wholesale incon-
patibility with postmillennialism.

62, House and Ice, Dominion Theology, pp. 342, 344,

63, Ibid., p. 340,

64, Ibid., p. 344,

65, Ibid., p. 341,

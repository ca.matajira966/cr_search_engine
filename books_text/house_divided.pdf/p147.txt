98 House Divided

onomists do from time to time make mistakes in their understand-
ing, revising, codifying or applying the judicial laws. That does
not count as a fundamental reason to reject the distinction alto-
gether — any more than other theological distinctions (“dispensa-
tions,” maybe) should be scuttled because theologians do not
always make proper use of them. The dictum about babies and
bathwater still makes common sense.

The objection raised by House and Ice to the ceremonial cate-
gorization of the law (redemptive-restorative provisions) is more
serious — and more seriously wrong. The same (overly broad)
complaint is voiced which we heard above: namely, that drawing
this distinction in general, or in particular cases, can be difficult. 3°
Granted. However, the necessity of drawing just such a dis-
tinction in order to preserve evangelical theology is more than an
offsetting counter-weight to this objection.

It has to be obvious even to House and Ice that what is called
the moral and judicial law did not serve éhe same soteriological function
as did the laws which are retrospective y called ceremonial, and
for instance, did not “foreshadow” the work of the Messiah i the
same way that the sacrificial laws did so. To think otherwise is exe-
getically groundless and theologically misleading. The civil laws
of Old Testament Israel did not, as was characteristic of the cere-
monial laws, expound the way of gaining redemption or symbol-
ize the setting of God’s people redemptively apart from the world.

Now the word “redemption” can certainly be used broadly
enough by a theologian to cover both the means of redemption
(Christ’s sacrificial, substitutionary death) as well as the effects of
that redemption (the holy conduct of His people). But to inter-
mingle or confuse those two different senses of “redemption” would
be a grave theological mistake which is bound to obscure the purity
of the gospel. We are not saved by our righteous behavior, but
rather saved unto righteous behavior (e. g., Eph. 2:8-10; Rem.
3:28; 8:4). One overlooks that very gospel truth in the Old Testa-
ment if he says the moral and ceremonial laws are not separable

35, House and Ice, Dominion Theology, pp. 38-39.

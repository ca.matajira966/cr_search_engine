7
THE JURISDICTION OF THE LAW

A reply to the argument that the nations and New Testament belicv-
es. are not under the law.

In Chapters 6 and 7 of Dominion Theology House and Ice argue
against the theonomic understanding of the law of God, which
holds that the moral regulations and precepts of the Mosaic law
(along with the rest of Scripture’s revelation of moral duty) are
binding upon the New Testament Christian, just as they were
binding upon the Gentiles of the Old Testament era. Biblical evi-
dence and exposition for this conviction can be reviewed in Chap-
ter 3 above. In their attempt to offer a biblically based reply to the
theonomic position about the universality of God’s moral princi-
ples (wherever and however they are expressed in the Bible),
House and Ice hope to do two kinds of things: to undermine the
apparent biblical support which is enjoyed by the theonomic
thesis, and to prove the contrary conclusion on the basis of sepa-
rate considerations. With due respect for their sincere involve-
ment in the biblical text, we cannot see that their efforts have met
with success on either score.

The Argument over Matthew 5:17-20

Does the New Testament teach that the commandments of the
Mosaic law continue to have moral authority in the New Testa-
ment era unless revised or set aside by the authority of God Him-
self ? Theonomic ethics answers “yes ,” and it sets forth a large
assortment of biblical and theological arguments in support of

103

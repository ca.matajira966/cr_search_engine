154 House Divided

for the Messiah: “A star shall come forth from Jacob, and a scepter
shall rise from Israel, and shall crush through the forehead of
Moab, and tear down all the sons of Sheth. And Edom shall be a
possession, Seir, its enemies, also shall be a possession, while
Israel performs valiantly. One from Jacob shall have dominion,
and shall destroy the remnant from the city.”

The Prophetic Era Expectation of Victory

In the prophetic era we find a continuing and expanding de-
velopment of the plan of redemption, and with it the promise of
victory for the redeemed. Again, a list of biblical references will
be ushered before the reader.

Particularly significant in this regard are the Messianic
Psalms. In Psalm 2, Jehovah God laughs at the opposition of man
to Him and to His Messiah. The Messiah is promised the “na-
tions” and “the ends of the earth” as His “possession.” On the basis
of this promise, the kings and judges of the earth are exhorted to
worship and serve the Son (Psalm 2:10-12).

In Psalm 22, it is prophesied that “all the ends of the earth will
remember and turn to the Lord, and all the families of the nations
will worship before Thee” (v. 27; cp. Psalm 66:4; 68:31-32; 82:8;
86: 9). This obviously anticipates the fruition of the covenant of
God given to Abraham and expanded in Moses and David.

In Psalm 72, this Messianic Gospel Victory Theme is tied to
pre-consummative history, before the establishment of the eternal
New Heavens and Earth. “Let them fear Thee wiile the sun endures,
and as long as the moon, throughout all generations. May he come
down like rain upon the mown grass, Like showers that water the
earth. “In his days may the righteous flourish, and abundance of
peace till the moon is no more, may he also rule from sea to sea,
and from the River to the ends of the earth (w. 5-8).”

8, The imagery of pouring rain here reflects the spiritual presence of Christ in
the Person of the Holy Spirit (Rem,8:9; John 14:16-18) being poured out upon
the world from on high (Isa, 32:15; 44:3; Eze, 39:29; Joel 2:28-29; Zech. 12:10;
Acts?:17-18). Christ is“in” us via the Holy Spirit, which is poured out upon us
since Pentecost.

 
   

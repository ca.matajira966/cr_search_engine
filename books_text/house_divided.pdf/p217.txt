172 House Divided

Second, the disciples could have been mistaken. House and Ice
admit the disciples were often in error. °° Jesus constantly had to
demand: “He that has ears to hear, let him hear” (Matt. 11:15; 13:9,
15, 16, 43; Mark 4:9; 4:23; 7:16; Luke 8:8; 14:35). For example,
did they not for a long time after Christ’s departure think John
would live to the Second Coming because of something Jesus
taught them, which the y misunderstood (John 21: 22/7.) ? Were not
they constantly mistaken about the necessity of His death (John
2:22: 12:16; 16:4; Luke 24:7-8)? If they sought an earthly, political
kingdom in Acts 1:6, may it not be reasonably supposed that they
were mistaken — especially since they desired places of promi-
nence in it (Matt. 20:21-23)?

As a matter of fact, Jesus specifically told them there would be
things they could not learn until the coming of the Spirit at
Pentecost: “I have yet many things to say unto you, but ye cannot
bear them now. Howbeit when he, the Spirit of truth, is come, he
will guide you into all truth: for he shall not speak of himself; but
whatsoever he shall hear, that shall he speak: and he will shew you
things to come” (John 16:12-13). And Acts 1:6 is before Pentecost.
Could this have been one of the things they did not understand,
especially in light of their hesitancy at accepting Gentiles into the
Church (Acts 10-11)?

Third, the emphasis of the question is overlooked. In the
Greek of Acts 1:6 the question emphasizes “this time.” A strong
case (which we accept) may be made for an alternative under-
standing of the passage to both the dispensationalist one and the
immediately preceding one. In light of the abundance of evidence
we have presented above regarding Israel’s demise, and in light of
the data to be brought up in the next chapter regarding the pres-
ence of the kingdom, there is a viable alternative construction to
this episode. May we not just as legitimately conclude that the
disciples did understand the true conception of the spirituality of
the pan-ethnic kingdom of Christ, and thus that they were here
merely asking the Lord, “Is it now time for Israel to be converted

39, Ibid., p. 271,

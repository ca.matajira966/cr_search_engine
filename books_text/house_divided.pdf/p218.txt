The Nature of the Kingdom 173

to You and enter the kingdom, which You have established?’ This
would fit well within the semantic, theological, and psychological
framework of the episode: These were (theologically) postmillen-
nialists and (psychologically) devout Jews (Acts 10-11; Rem.
9:1-2). In addition, Christ’s answer would speak to this question.

Summary

1, The kingdom of Christ is not a future, Armageddon-intro-
duced earthly, political kingdom.

2, The first-century Jews wanted a political kingdom to over-
throw Rome and when Christ did not offer them such, they re-
jected Him (John 6:15), and even his disciples were disappointed
(Luke 24:21-27).

3. The basic power of the kingdom is the “gospel of the king-
dom” (Matt. 4:23; 9:35; Mark 1:14-15), the basic function of the
kingdom is the promotion of God’s truth (John 18:37), and the
basic operation of the kingdom is via humility (Matt. 21:4-5).

4, The kingdom of Christ is essentially a spiritual kingdom
(Rem. 14:17) that operates from within the heart (Luke 17:20-21).

5. We enter the kingdom of Christ by means of salvation
(Col. 1:12-13; John 3:3).

6. Christ rules His kingdom by His mystical presence from
heaven (John 18:36) and through the indwelling of the Holy Spirit
(John 7:39; Rem, 8:9; 1 Cor, 3:16).

7. Israel as a nation has once for all been set aside as the spe-
cially favored nation of God (Matt. 8:11-12; 21:43), because of
their prominent role in crucifying Christ (Acts 2:22-23, 36;
3:13-15; 5:30; 7:52; 1 Thess. 2:14-15).

8. Christ’s kingdom includes people of all races on an equal
basis (Isa. 19:19-25; Zech. 9:7; Eph. 2:12-17), even great numbers
of Jews will eventually enter it (Rem. 11:11-25).

9. The New Testament-phase Church is “the Israel of God”
(Gal. 6:16), “the circumcision” (Phil. 3:3), “the seed of Abraham”
(Gal. 3:7, 29), the “Jerusalem above” (Gal. 4:24-29), the “temple
of God” (Eph. 2:21), “a royal priesthood” and a “peculiar people”
(1 Peter 2:9-10). Consequently, Jewish promises are applied to the

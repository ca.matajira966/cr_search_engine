The Presence of the Kingdom 19t

The sitting at the “right hand” of God is a semantic equivalent to
sitting on God’s throne, as is evident in Revelation 3:21: “I also
overcame and sat down with My Father on His throne .*

Summary

1, The prophesied kingdom of Christ came near in the early
ministry of Christ, because the “time was fulfilled” for it to come
(Mark 1:14-15).

2, John the Baptist is a marking line separating the fading
Old Testament era from the dawning kingdom era (Matt. 11:11-14;
Mark 1:14-15).

3. Christ’s power over demons was evidence the kingdom had
come in His earthly ministry (Matt. 12:28); it was not to await
some future, visible coming (Luke 17: 20-21).

4, Christ claimed to be king, while on earth (ohn 12:12-15;
18: 36-37) and was enthroned as king following His resurrection
and ascension, at Pentecost (Acts 2: 30ff. ). From then on we hear
of his being in a royal position, at the right hand of Almighty God.
(Rem. 8:34; Eph. 1:20; 1 Peter 3:22).

5. Because of this, first century Christianity proclaimed Him
as king (Acts 3:15; 17:7; Rev. 1:5) with regal dignity, authority,
and power (Eph. 1:22; Phil. 2:9).

6. Beginning with the first century, people are, at their con-
versions, translated into the kingdom of Christ (Col. 1:12-18; 4:10;
1 Thess, 2:12).

7. Christians are composed as Christ’s kingdom (Rev. 1:6; 9)
and are now mystically seated with Him in rulership position
(Eph. 1:3; 2:6; 1 Cor, 3:21-22).

8. Christ’s kingdom is multi-dimensional, including salva-
tion while on earth (Col. 1:13) and its ultimate fruition in heaven
(2 Peter 1:11).

 

216 House Divided

history. Earlier it was awaiting until all rule, authority and power
were abolished; here it is said to await occurrence until “He has
put all His enemies under His feet.” The repetition of the expecta-
tion of His sure conquest before the end is significant. Further-
more, the last enemy that will be subdued will be death, which is
subdued in conjunction with the Resurrection that occurs at His
coming. '? But the subduing of all other of His enemies occurs be-
fore this, before the Resurrection.

In verse 27 it is clear that He has the title to rule, for the
Father “has put everything under His feet.” This is the Pauline ex-
pression (borrowed from Psalm 8:6) that is equivalent to Christ’s
declaration that “all authority has been given Me.” Christ has the
promise of victory; He has the right to victory. Psalm 110, espe-
cially as expounded by Paul in 1 Corinthians 15, shows He will
have the historical, pre-consummation victory as His own before
His coming. This verse from Psalm 110 is one of the most fre-
quently referred to Old Testament promises to appear in the New
Testament; '* the expectation is a frequently recurring theme!

Remans 11

In addition, Remans 11 is helpful for the understanding, not
only of the worldwide conquest of the Gospel, but of the Jewish
hope for salvation. In Remans 11 Paul presents a sustained argu-
ment for the future conversion of the Jews. In verses 1-10 he shows
very clearly that he has in mind the racial Jew, and not the spiri-
tual Jew (i. e., the Christian): He speaks of tribal distinctions (v.
1) and the rebellion of this nation (vv. 3-10). Later he sets them
over in contrast to Gentiles (w. 11-13, 25). Thus, he is speaking
directly to the question: “Has God forever rejected the Jews to cer-
tain, irrevocable, and final doom?”

17, Contrary to dispensationalist confusion, the resurrection of the lost is not
mentioned here only because his primary concern (as in 1 Thess.4:13) is with
Christians and their ethical actions.

18, See Matthew 22:44; 26:64; Mark 12:36; 14:62; Luke 20:42-43; 22:69; Acts
2:34-35; Ephesians 1:20-22; Hebrews 1:1 3;1 Peter 3:22. There are a num-
ber of other allusions to it (e. g., Rem. 8:34 and Col. 3:1), but those listed are
fuller and include the idea of expected dominion,

  

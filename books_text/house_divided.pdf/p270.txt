The Victory of the Kingdom 227

followers: “Christians have no immediate solution to the problems
of our day.”** In fact, they aver that “to attempt to establish a
long-term change of institutions before Christ returns will only
result in the leaven of humanism permeating orthodox Christian-
ity." They even castigate Reconstructionists (or anyone else, for
that matter) for trying: “Tragically, this will contribute to the fur-
ther unfaithfulness of the church in these last days before the
return of Messiah.”4¢

The pessimism of House and Ice is flushed out in their answer
to Jesus’ question in Luke 18:8: “When the Son of man cometh,
shall he find faith on the earth?” As pessimists House and Ice
write: “This is ‘an inferential question to which a negative answer
is expected.’ So this passage is saying that at the second coming
Christ will not find, literally, ‘the faith’ upon the earth.”*”

There is, however, some doubt as to whether this question is
even dealing with the future existence of Christianity.** But if it
does, why is a negative prospect expected? Could not Christ be
seeking to motivate His people, driving them to strive to see that
the answer issue forth in an optimistic prospect, as Peter’s answer
was an optimistic one to another question in John 6:67-68.49
Could it not be that ‘the question is asked for the purpose not of

44. John F, Walvoord, in Charles Lee Feinberg, Prophecy and the Seventies
(Chicago, IL: Moody Press, 1971), p. 212, Walvoord continues: “A solution to
this unrest and turmoil is provided in the Bible, and there is no other. That solu-
tion is that Jesus Christ Himself is coming back to bring peace and rest to the
world” (p. 210).

45, House and Ice, Dominion Theology, p, 340,

46, Ibid., p. 161,

47, Ibid, p. 229, Ironically, not even House and Ice believe that at either the
Secret Rapture Coming or the Second Advent there will bea total absence af the
Christian faith.

48, Warfield convincingly suggests that the reference to “the faith” has to do
with the faith-trait under question in the parable: perseverance. He doubts the
reference even touches on whether or not the Christian faith will be alive then,
butrather: “Will Christians still be persevering in the hope of the Lord’s retum?”
See Warfield, “The Importunate Widow and the Alleged Failure of Faith,” ix
Selected Shorter Writings, vol. 2, pp. 698-710.

49, For similar ethical promptings, see Warfield, Biblical and Theological Studies,
pp. 334-50.

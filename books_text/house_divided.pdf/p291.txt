History of Theology on the Kingdom 249

gone on before. House and Ice should not have any problem with
the slow development of systematization, for they write: “The
futurist interpretation is the approach used by the earliest church
fathers, We do not argue that they had a sophisticated system, but
the clear futurist elements were there.”®? We would argue simi-
larly for postmillennialism, After all, did not Ryrie argue regard-
ing dispensationalism’s “recency”: “informed dispensationalists

.. recognize that as a system dispensationalism was largely for-
mulated by Darby, but that the outlines of the dispensationalist
approach to the Scriptures are found much earlier.”**

House and Ice question Gary North: Who are the early church
fathers who were postmillennial? There are none.”®* Interestingly,
two paragraphs later they cite as a source for their argument the
historical work of Robert G. Clouse on millennial views. Clouse
apparently would not agree that antiquity provides no evidence of
the postmillennial hope, for he clearly states of premillennialism,
amillennialism, and postmillennialism: ‘Although these interpre-
tations have never been without adherents in the history of the
church, in certain ages a particular outlook has predominated.”8*
We will just briefly survey some of the evidence for the existence
of the postmillennial hope centuries prior to Whitby.

As a matter of fact, there are indicators in antiquity of a genu-
ine hope for the progress of the Gospel in history. Tertullian (a. ».
160-220) was a Montanist and Montanists were by and large pre-
millennial. Nevertheless, as Kromminga, an amillennialist, has
noted, although most Montanists were premillennialist, “others
were at least containing also the germs for later fullfledged Post-
millennialism.”®? This nascent postmillennialism was resultant

83, Ibid., p. 27.5,

84, Ryrie, Dispensotionalism Today, p. 66.

85, Ibid., p. 208,

86, Clouse, The Meaning of the Millennium, p. 9 (emphasis mine).

87, Kromminga, The Millennium, p. 76, Even the premillennialist Tertullian
could have something of a postmillennial hope: We pray, too, for the emperors,
for their ministers and forall in authority, for the welfare of the world, for the
prevalence of peace, for the delay af the final consummation” (Tertullian, Apology

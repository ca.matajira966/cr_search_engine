250 House Divided

from the hope (rooted in Scripture) that there would be a period of
the Holy Spirit’s dominance in the affairs of history.®*

Although much in Origen (a. D. 185-254) is not acceptable, he
is a noteworthy church father of considerable influence. As Philip
Schaff has noted regarding Origen’s views, there was in them a
place for a great evidencing of the power of the Gospel: “Such a
mighty revolution as the conversion of the heathen emperor was
not dreamed of even as a remote possibility, except perhaps by the
far-sighted Origen.”®°

In Eusebius (A. D. 260-340) there is a fuller expression of hope
that is evident. In Book 10 of his Ecclesiastical History he believes he
is witnessing the dawning of the fulfillment in his day of Old Tes-
tament kingdom prophecies. Of Psalms 98:1-2 and 46:8-9, which
he specifically cites, he writes: “Rejoicing in these things which
have been clearly fulfilled in our day."® Later in Chapters 4
through 7 of Book 10 he cites dozens of other such passages as
coming to fulfillment. He writes: “For it was necessary and fitting
that as her [the Church’s] shepherd and Lord had once tasted
death for her, and after his suffering had changed that vile body
which he assumed in her behalf into a splendid and glorious body,
leading the very flesh which had been delivered from corruption
to incorruption, she too should enjoy the dispensations of the Sav-
iour.”*! After quoting several passages from Isaiah he writes:
“These are the things which Isaiah foretold; and which were an-
ciently recorded concerning us in sacred books; and it was nec-
essary that we should sometime learn their truthfulness by their
fulfillment “

 

39), The prayer for the delay of the end and peace is spoken of as a liturgy of the
church, Although there is chiliasm, there are also some adumbrations of cosmic
hope in prayer. In his Scapula there is a desire for the saving of all men (Scapula
3:3-4), See Pelikan, The Christian Tradition, vol, 1, p. 130.

88, Kromminga, The Millennium, p. 84,

89, Philip Schafl, History of the Christian Church, vol. 2, p. 581.

90, Eusebius, Keclesiastical History, 10:1:6.

91, Tid., 10:4;46.

92, Ibid., 10:4:53, ep, sections 46-52, Citing Isaiah 51:10-11; 54:4;54:6-8; 51:17-
18, 22-23; 52:1-2; 49:18-21.

  

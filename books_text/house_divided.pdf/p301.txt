260 House Divided

tion (sixth century) have as a heading to Revelation: “written in
Patmos, whither John was sent by Nero Caesar.” ** Arethas (sixth
century) applies a number of the prophecies to the fall of Jerusalem
in A. D. 70, noting that Eusebius merely “alleges” that Revelation
was written under Domitian. '? Though Andreas (sixth century)
holds to a Domitianic date, he notes that “there are not wanting
those who apply this passage [Rev. 6] to the siege and destruction
of Jerusalem by Titus,” thus evidencing a number of early-date
advocates before him. Also we can probably add to the list Papias
({a.p, 60-130), who teaches that John the Apostle died before Jeru-
salem fell, and The Shepherd of Hermas (ea. a, », 80#}, which evi-
dences influence by Revelation. !° We seriously suspect that
House and Ice have not even read the original references from
Clement of Alexandria and Origen, which they put forth as two of
their four non-Irenaean “witnesses” supporting the late date. 20
Neither mentions the name of Domitian! *! Apparently for historical
evidence, they adopt the common jargon: “It goes without saying”!

There simply is no “voice [singular] of church tradition con-
cerning the date of Revelation.” Neither may it be stated, as they
do, that Clement of Alexandria (1), Origen (!), Victorious, and
Eusebius “had no witnesses to the contrary.” Nor should it be said
that “if there were some validity to the early date, some trace of
this competing tradition should have surfaced. However, it has
not!”22 To quote House and Ice against themselves: Their critique
of the early church tradition seems to be “speculative” and a “de-
bater’s technique.”

16. Moses Stuart, Commentary on the Apocalypse, 2 vols. (Andover: Allen, Merrill,
and Wardwell, 1845), vol, 1, p. 267.

17, Cited by A, R. Fausset, in Robert Jamieson, A, R, Fausset, and David
Brown, A Commentary Critical and Explanatory on the Old and New Testaments (Hart-
ford, CT: Scranton, n.d.), vol, 2, p. 548,

18, See Stuart, Apocalypse, 1:267.

19, See Gentry, Before Jerusalem Fell, Chapters 5 and 6,

20, House and Ice, Dominion Theology, p, 253,

21, See Before Jerusalem Fell, pp. 68¥. and 978. See Clement of Alexandria,
Who Is the Rich Man that shall be Saved? 42; Origen, Matthew 16:6.

22, House and Ice, Dominion Theology, pp, 253-54 (emphasis mine),

23, Ibid., pp. 252-53,

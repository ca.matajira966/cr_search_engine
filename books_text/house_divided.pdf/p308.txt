The Preterist Interpretation of the Kingdom 267

These events will come upon “this generation.” He uses the far de-
monstrative in 24:36 to point to the Second Advent: that day.” As
House and Ice admit; “It is probably true that the disciples
thought of the three events (the destruction of the temple, the sec-
ond coming, and the end of the age) as one event. But as was al-
most always the case, they were wrong.”** Thus, Christ divided
up the events for them. The coming “tribulation” (24:21; cp. Rev.
1:9) was to come upon “this generation” (Matt. 23:36; 24:34; cp.
1 Thess. 2:16) and was to be foreshadowed by certain signs (Matt.
24:4-8). But the Second Advent was to be at ‘that” far day and
hour, and was not to be preceded by particular signs of its near-
ness, for no man can know it (24:36).*9

Regarding Revelation: The past fulfillment of most of the prophe-
cies in Revelation 4-19 is compellingly suggested by the various
time indicators contained in its less symbolic, more didactic intro-
duction and conclusion. House and Ice rehearse a good principle
regarding Matthew 24, which is equally relevant to Revelation
and which they should heed: “The key to understanding the dis-
course is found in the first sentence.”

Although they make one passing reference to the fact of the
employment of Revelation 1:1 in preterist literature, *! they never
engage the interpretation of the verse, or of related verses in Revela-
tion ! But Revelation 1:1 opens the prophecies of Revelation and
prepares the reader to understand them: “The Revelation of Jesus
Christ, which God gave unto him, to shew unto his servants things
which must shortly come to pass.” And this despite the fact they

48, Ibid., p. 271.

49, Despite the clarity of Christ’s statement that no man can know the day or
the hour, House and Ice write of the Tribulation: “The ruler sets up himself as
God in the temple by placing his image in the holy of holies. This will occur three
and a half years before the second coming of Christ” (p. 288), Sounds quite
datable to us! Other elements that space constraints forbid our exploring include,
for example, the preparatory parable of the householder (Matt. 21:35-44), which
explained the demise of Israel (21:33-44), which was to happen before the very
eyes of the chief priests (21:23, 45) and the especial reference to Judea — the tribu-
lation could be escaped by fleeing Judea (24:16).

50, House and Ice, Dominion Tieciogy, pp. 299-300.

51, Ibid., p. 53.

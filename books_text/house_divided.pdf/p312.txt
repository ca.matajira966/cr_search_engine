The Preterist Interpretation of the Kingdom 271

here indicate the Second Coming, and (2) the term comes four
verses after the supposed shift: How do they know where to shift
gears?

Still further, they note regarding the disciples’ questions: “The
first question is answered in Luke 21:20-24, since Luke is the one
who specializes in the a. D. 70 aspects. Luke records Jesus’ warn-
ing about the soon-to-come destruction of Jerusalem — the days of
vengeance. The second and third questions are answered in Mat-
thew 24.”°” This seems rather arbitrary. Why does Matthew list
more questions than he answers? Besides, lay Luke 21:20-24 side
by side with Matthew 24-15-21; what is the compelling difference
that leads us to conclude Matthew is speaking of the Second Ad-
vent and Luke ofa. ». 70, events totally different and separated.
thus far by over 1900 years? They have an interpretive bias as op-
posed to our contextual time indicator.

2, The Abomination of Desolation. House and Ice state boldly:
“One major reason Matthew 24 could not have been fulfilled in
A.D. 70 is that ‘the abomination of desolation’ (24:15) was not ac-
complished in the destruction of Jerusalem.”* Here we also detect
an incredible arbitrariness: They aver that Luke 21:20 and Mat-
thew 24:15 speak of “two separate events” because “In thea. », 70
destruction of Jerusalem there was no image set up in the holy
place, no worship of the image required, and no three-and-a-half
year period of time between that event and the second coming of
Christ. . . . Finally, no image came to life and beckoned men to
worship it .”° Incredibly, they charge that “Chilton cannot make
his interpretation of the abomination of desolation fit the text of
Scripture. Instead, he ignores the details of the passage he is sup-
posed to be studying and goes to other unrelated passages import-
ing them into the passage .“”

67, Ibid, pp. 299-94

68. Ibid. , p. 287.

69. Ibid, p. 290, The abomination of desolation phrase is important to House
and Ice as a “major reason Matthew 24 could not have been fulfilled in A, D. 70 is
that ‘the abomination of desolation’ (24:15) was not accomplished in the destruc-
tion of Jerusalem” (p. 286).

70, House and Ice, Dominion Theology, p, 290.

 

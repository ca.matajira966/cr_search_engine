276 House Divided

sistency with their dispensationalist Gap Theory: “The better view
is that the seventy weeks of Daniel have yet to be completed .“”In
other words, there is a gap between the 69th and 70th weeks of
Daniel, which comprises the time between the Triumphal Entry in
A.D. 338s and the Secret Rapture in our future. Now who can take
seriously a claim to “precise accuracy” of fulfillment of Daniel 9 on
this basis? Consider the situation: Daniel predicts 70 weeks of
years. Though the whole prophetic period in Daniel covers 490
years, the dispensationalist has inserted a 1,966 year gap (thus
far!) into those weeks-of-years. Thus, the gap has already covered
a period of time almost four times larger than the whole period of
490 years! “Precise accuracy?”

The Historical Basis of Preterism

Nascent Preterism in Antiquity

Statements as fallacious as they are bold are made by House
and Ice regarding the destruction of Jerusalem in prophecy. In
response to Chilton’s comment that “Revelation is primarily a
prophecy of the destruction of Jerusalem by the Remans,” House
and Ice ask:’9 “If this were such a clear ‘fact,’ then why did none of
the early church writings reflect Chilton’s views in their interpre-
tation of Revelation? If the a. D. 70 destruction of Jerusalem ful-
filled so much of biblical prophecy, then why is this nof reflected in
the views of the early church? Why is it that all of the early
fathers, when referring to Revelation and Matthew 24, see these

 

those various economies.” That is, you cannot understand the revelation without
the feature, but you cannot find the feature without the revelation!
Interestingly, House and Ice write: “The coming of Christ appears to bring a

new order in New Testament teaching, and even various statements and actions
of Christ in the Gospel accounts seem to indicate his rejection af a literalistic obe-
dience to the law (John 7:53-8:11; Matthew 12:1-4)” House and Ice, Dominion
Theology, p.104.

87. House and Ice, Dominion Theology, D. 259.

88, id, p. 321.

89, Though writing under the heading of “Internal Evidence,” here they slip
into the external evidence.

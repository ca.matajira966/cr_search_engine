312 House Divided
they call ‘Biblical law.”4

Misrepresentations

They totally misrepresent a Jim Jordan statement in another
setting: “Premillennialists plead guilty to the desire to have ‘a top-
down’ kingdom. . . . In fact, fellow Reconstructionist James Jor-
dan... favors the top-down approach: ‘American (evangelical)
like to believe the myth that society is transformed from the bot-
tom up and not from the top down. This flies squarely in the face
both of history and of Scripture’ ”5 But Jordan is shocked at their
employment of his quotation: “On p. 237 they quote me out of
context to make it appear that I believe in a ‘top-down’ takeover of
society, while North believes in a ‘bottom-up’ approach. Had they
read my remarks in context they would have seen that I was
speaking of influence, not conquest. People follow leaders. No one
disputes this. North was speaking of conquest. There is no contra-
diction between what he wrote and what I wrote, read in context.
See Jordan, The Sociology of the Church [1986], pp. 17 1f.”6

Elsewhere we read them stating: “With their belief that ulti-
mate victory is assured (the church will set up a theocratic govern-
ment, will evangelize the world, and will usher in the earthly
reign of Christ)... .”7 Both the order and manner of their state-
me-nt in the quotation are significant: They assert, first, that the
‘church” (not Christians in society) “will set up a theocratic gov-
ernment”! Then (after setting up a theocratic government?) it
“will evangelize the world .” This clearly suggests an appalling mis-
placement of priority for the “Church” by Reconstructionists, and
leaves the distinct impression, with other of their statements and
innuendos, * of a Reconstructionist evangelism by the sword!

And they make these statements despite the fact Reconstruc-

4, Ibid. 0, 184,

5. Ibid. , p. 237,

6. James B, Jordan, ‘A Review of H, Wayne House and Thomas Ice, Domin-
ion Theology: Blessing or Curse?: An Analysis of Christian Reconstructionism” (Tyler,
TX: Biblical Horizons, 1988), p. 12.

7. House and Ice, Dominion Theology, p. 21-22,

8, For example, ibid., pp. 77-80.

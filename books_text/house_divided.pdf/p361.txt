Ethical Lapses 323

Mosaic sacrificial system. Hebrews flatly affirms that the whole
Mosaic system is obsolete and about to pass away. Therefore the
popular Dispensational position that Israel is the ‘clock of proph-
ecy’ is misguided.”4*

*6. Restoration.

“‘MSOG: We do not live in the time of the great falling away.
We are leaving that day and entering a new day, a new age, a new
beginning of life and restoration.’ “°

B.B. Warfield: John “teaches the salvation of the world through
a process; it may be — it has proved to be — a long process; but it is
a process which shall reach its goal... . We are a ‘little flock’
now: tomorrow we shall be the world. We are but the beginnings:
the salvation of the world is the end.””

“7, The Rapture.

“MSOG: ‘Some are also anti-rapture — rather than go to
heaven. God’s Army of Overcomes will establish the Kingdom of
God on earth. "5!

Warfield (see above on #4).

*8. Breakthroughs/New Revelation.

‘MSOG: The offices of apostle and prophet have the ancinting
to perceive and proclaim new revelation truth. The apostle and
prophet have the ministry to establish and lay the foundation for
new truth in the Church. ... Once an apostle or prophet re
ceives by the spirit a new restorational truth and establishes it as a
valid ministry then the teacher teaches it in detail.’ “”

House and Ice: “By the 1830s J. N. Darby began teaching that
the timing of the Rapture would be pretribulational .”53

48, George E, Ladd, The Last Things (Grand Rapids, MI: Wm, B, Eerdmans,
1978), p. 25.

49, House and Ice, Dominion Theology, p. 386.

50, Warfield, Selected Shorter Writings [article from 1921], vol, 1, pp. 176-77.

51, House and Ice, Dominion Theology, p. 387.

52, Ibid., pp. 386-87.

53, Ibid,, p. 422,

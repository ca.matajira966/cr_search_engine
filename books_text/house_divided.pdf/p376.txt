338 House Divided

Dispensationalists: “The idea that the unchangeableness of God
requires that the specific details of the Mosaic code be transferred
to all times and cultures simply does not follow.” !°* “Both the
topological and nontypological aspects of the law were of tempor-
ary purpose ‘until the seed should come’ (Gal. 3).”™

Abraham Was Not Under a Mosaic-type Law:

Jehovah’s Witnesses: On Deut. 5:1-15: In those words it distinctly
says that Israel’s forefathers, including most prominently of all
Abraham, Isaac and Jacob, and the twelve sons of Jacob, were
not under this law covenant .” 16

Dispensationalists: “In reaffirming his promise to bless Abraham
and his descendants, God makes clear to Isaac that Abraham had
been obedient to a pre-Mosaic revelation of God’s law.” 137

The Sabbath Was a Sign for Israel Alone:
Jehovah’s Witnesses: “The sabbath was a distinguishing feature
of Jehovah’s covenant arrangement with Israel alone. . ..” '%*
Dispensationalists: ‘Then the Sabbath was revealed to
Israel ... and invested with the character of a ‘sign’ between the
Lord and Israel.”129

Christ Fulfilled the Law, Thus Removing It:

Jehovah’s Witnesses: On Matt. 5:17ff.: “Destroying the Law by
breaking God’s law covenant is far different from fulfilling it and.
thus moving it out of the way and lifting its obligations from his
disciples. ... So, in order to fulfill the Law and the Prophets,
Jesus by Jewish birth ‘came to be under law’.”1#°

134, House and Ice, Dominion Tholngy, p. 87,

135, ibid, p. 42,

136, Let God Be True, p. 173,

137, House and Ice, Dominion Theology, p. 87.

138, Let God Be True, p. 174,

139, The New Scofeld Reference Bible, ed. C. I. Scofield (New York: Oxford Uni-
versity Press, 1909), p. 1010 (at Matt, 12:1),

140, Let God Be True, p. 175,

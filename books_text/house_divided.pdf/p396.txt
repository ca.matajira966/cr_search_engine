360 House Divided

Norman Geisler and Natural Law

What the reader finds conspicuously absent in Dominion Theol-
ogy: Blessing or Curse? is an evaluation of “natural law.” The reason.
for this may be that House and Ice do not subscribe to a natural
law ethic. Why then would they call on Dr. Norman Geisler to en-
dorse their book since he is an advocate of natural law?

w
Premillennialists, unlike postmillennialists, do not attempt to
set up a distinctly Christian government; they work rather for
good government, Premillenarians need not work for Christian
civil laws but only for fair ones, 25

Comment: Postmillennialists have in mind a biblical government.
rather than a Christian government since the State is jurisdiction-
ally separate from the church as an institution. 7° What determines
“good government” and “fair” civil laws? This is the crux of the
matter. Every government claims to act on the basis of the good
(Plato) and what’s fair (Socialism). Putting someone in the Gulag
might be considered ‘good for the nation.” Communism’s dictum.
from each according to his ability and to each according to his
need is based on fairness.”

Q)

Building on the natural law ethic of Richard Hooker, an
Anglican, who followed Thomas Aquinas, [Isaac] Watts argued
that “the design of civil government is to secure the persons,
properties, the just liberty and peace of mankind from the inva-
sions and injuries of their neighbours.””

2§, Geisler, “A Premillennial View of Law and Government; p. 258,

26, 've made the distinction between °Christian’ and “biblical” to separate gos-
pel proclamation (the means by which people are introduced to Christ = Christian),
the sacraments and church discipline (the exclusive jurisdiction of the church),
and civil government's God-ordained authority to wield the sword to “punish evil
doers and promote the good” (the exclusive jurisdiction of the state= biblical civil
government), The State cannot use the power of the sword to force anyone to be-
come a Christian, take the Lord’s Supper, or be baptized. The State is, however,
under the jurisdiction of the whole Bible to enforce certain civil legislation,

27, Geisler, “A Premillennial View of Law and Government,” p. 259,

366 House Divided

There is a tendency, however, for dispensationalists to get
carried away with compartmentalizing truth to the point that
they can make unbiblical distinctions. An almost obsessive
desire to categorize everything neatly has led various dispensa-
tionalist interpreters to draw hard lines not only between the
church and Israel, but also between salvation and discipleship,
the church and the kingdom, Christ’s preaching and the apos-
tolic message, faith and repentance, and the age of law and the
age of grace.

The age of law/age of grace division in particular has wreaked
havoc on dispensationalist theology and contributed to confu-
sion about the doctrine of salvation. *

Comment: Drawing a “hard line” between Israel and the church
is fundamental to dispensationalism. Once these “hard lines” go,
the entire system is in jeopardy of collapsing. MacArthur takes
issue with Chafer’s beliefs about the radical separation between
Jaw and grace. He goes on to maintain that other dispensational-
ists have taken the separation even further than Chafer. “That the
teachings of the Sermon on the Mount ‘have no application to the
Christian, but only to those who are under the Law, and therefore
must apply to another Dispensation than this.”4? MacArthur calls
this a “lamentable hermeneutic.” But this “lamentable herme-
neutic” is the natural outgrowth of dispensationalism. Mac-
Arthur, Ice, and House break with this legacy. But have they told
their readers?

41, MacArthur, Gospel, p. 25,

42, Clarence Larkin, Dispensational Troth (Philadelphia, PA: Larkin, 1918), p.
87. Quoted in MacArthur, Gospel According to Jesus, p. 26.

43, Ibid,

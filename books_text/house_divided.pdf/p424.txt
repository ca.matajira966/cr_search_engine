388 House Divided

Postmillennialism and the Jews

De Jong, J. A. As éhe Waters Cover the Sea: Millennial Expectations in
the Rise of Anglo-American Missions 1640-1810. Kampen: J. H.
Kok, 1970. General history of millennial views; throughout
mentions the importance of prophecies concerning the Jews.

Fairbairn, Patrick. The Prophetic Prospects of the Jews, or, Fairbairn us.
Fairbairn. Grand Rapids, MI: Eerdmans, 1930. Nineteenth-
century scholar Fairbairn changed his mind about the conver-
sion of the Jews; this volume reproduces his early arguments
for the historic postmillennial position, and his later argu-
ments against it.

Sutton, Ray R. “A Postmillennial Jew (The Covenantal Structure
of Remans 11) ,“ Covenant Renewal (June 1989). Sutton has a
conversation with a postmillennial Messianic Jew!

Sutton, Ray R. “Does Israel Have a Future?” Covenant Renewal

(December 1988). Examines several different views of Israel’s
future, and argues for the covenantal view.

Toon, Peter, ed. Puritans, the Millennium and the Future of Israel: Puri-
tan Eschatology 1600-1660, Cambridge: James Clarke, 1970. De-
tailed historical study of millennial views with special atten-
tion to the place of Israel in prophecy.

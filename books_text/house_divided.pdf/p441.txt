406 House Divided

Sutton, Ray R,, 2%, »00xvii, 70
Swaggart, Jimmy, xix
Swete, H, B,, 263, 369

Tacitus, 262

Tenney, Merrill, 300

Tertullian, 167, 242, 243, 249-50,
255, 259, 279

Thieme, Col. R, B,, 376-77

Thornwell, J. H., 143

Titus (Roman Emperor), 260, 272,
279

Van Til, Cornelius, x, x00, xii,
80-81

Vespasian, 278, 279

Victorius, 260

Viltellius, 278

Wall, Joe L,, 376
Waltke, Bruce, 200

Walvoord, John F,, xiv, 184, 222,
227, 235, 237, 304, 330, 332, 337,
350, 361

Warfield, Benjamin B., xxviii, 143,
206, 220, 224, 227, 282, 304, 321,
322, 323, 324, 325, 326

Watts, Isaac, 360

West, Nathaniel, 247

Whisenant, Edgar C., »odi, 144, 361,
364, 374

Whitby, Daniel, x0dii-ondiv, 240,
245-48, 249, 253, 254, 2,55, 306-7,
316-17, 318

Whitehead, John W,, 47-48

Wilberforce, William, 359

Wilburn, Gary, 363

Wilson, Dwight, »odi, 144

Wright, Christopher J. HL, 32

Young, E, J., 119-20, 279

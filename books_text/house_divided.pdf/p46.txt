xlvi House Divided

House Divided is a postmillennial book. It does not seek to fight
something (dispensationalism) with nothing (amillennialism).
You are not being asked to abandon hope in dispensationalism’s
escape hatch in the future (the pre-tribulation Rapture) only to
take up residence in amillennialism’s Fort Contraction, with a
tribe of howling Darwinian Indians circling it, all armed with re-
peating rifles. You are being asked instead to join a victorious
army led by Jesus Christ, who sits at God’s right hand, and who will
remain seated there until He subdues all His enemies under His
feet. “Then cometh the end, when he shall have delivered up the
kingdom to God, even the Father; when he shall have put down
all rule and all authority and power. For he must reign, till he
bath put all enemies under his feet” (I Gor.15:24-25).

P.S. Neither Dr. Bahnsen nor Dr. Gentry is responsible for
my “unchristian, offensive, insensitive, uncharitable, confronta-
tional, argumentative, arrogant, unscholarly” style, as it has been
described on occasion. They are both certified for seminary em-
ployment. As for me, I prefer off-campus bonfires.

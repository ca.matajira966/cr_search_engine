4 House Divided

love the Savior with all their heart. They know that their new life
in Him, their new status of being right with God, and their hope
of eternal life have been granted to them by the grace of God.
They have nothing of which to boast (Eph. 2:8-9). With Paul they
would say, “Far be it from me to glory, save in the cross of our
Lord Jesus Christ, through which the world has been crucified.
unto me, and I unto the world” (Gal. 6:14). Having been saved
from the world, their concern is to love their Lord with all their
heart, soul, strength and mind (Matt. 22:36-38). They now want
to walk in those good works which God intends for them (Eph.
2:10), They make a sincere effort to heed the words of Christ to
“seek above all the kingdom of God and His righteousness” (Matt.
6:33). They know that this kingdom, for which they pray regu-
larly (Matt. 6:10), will not be consummated until after the return
of Jesus Christ and the final judgment, when all believers will
then rejoice in “a new heaven and earth wherein righteousness
dwells” (2 Peter 3:13). In the meantime they seek to perfect per-
sonal holiness in the fear of God (2 Cor. 7:1) and to make all the
nations disciples of their Lord and Savior, Jesus Christ (Matt.
28:18-20). It is only in the light and context of these beliefs and
practices that they see and understand their Reconstructionist po-
sition in ethics and eschatology.

The authors of this work are also educated Christians, each
with seminary training, an advanced masters degree and a doc-
toral degree. They make their mistakes, but they have been trained
well to try and avoid them. They approach their Christian convic-
tions and theology with a zeal to pursue the truth wherever it
Jeads (Rem. 3:4). The ultimate standard of truth, in their estima-
tion, is the inspired word of God (John 17:17) found in the Scrip-
tures of the Old and New Testaments. The authors have sought to
study and properly understand the Scriptures, giving their best
mental efforts to reaching the conclusions which they have. Be-
cause Scripture is the ultimate authority, not their own present
convictions, the authors hope to remain teachable and open to
correction in what they believe. They also want to be responsible

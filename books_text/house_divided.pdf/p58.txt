6 House Divided

The dispute between dispensationalism and Reconstruction-
ism has not abated, however. Tommy Ice and Wayne House of
Dallas Theological Seminary have published what they call “the first
book-length reply to the Christian Reconstruction movement,”! a
460-page volume entitled Dominion Theology: Blessing or Curse? The
book aims to be thorough and appears to have been written by
two authors who are familiar with much of the literature pub-
lished by Reconstructionists (cf. their annotated bibliography).
There is no doubt how the authors answer the question posed in
the title of their book. They view Reconstructionism — in particular
theonomic ethics and postmillennial eschatology — to be a curse.
They find the position to be startling, upsetting, dangerous and.
unbiblical. They are concerned over its growing advocacy and in-
fluence. Accordingly their book is hard-hitting. Despite occasional
words of agreement and even praise for aspects of Reconstruc-
tionism, authors House and Ice-feel that the curse and theological
error of Reconstructionism can be refuted and should be repudi-
ated by Christians who wish to be faithful to Scripture.

Upon examination, however, the book by House and Ice seemed
to the authors of this book to have fallen far short of its goals - and
indeed of expected standards of scholarship. Reconstructionism
had not been represented fairly and correctly at many places. The
reasoning of Ice and Hunt was weighed down with fallacies. The
attempted exegetical rebuttal of Reconstructionist distinctive was
flawed and ineffective. Nevertheless, the book set itself forth and.
was being hailed (by a number of prominent evangelical personal-
ities) as the decisive answer to Reconstructionism which the Church
sorely needed today.

The Debate That Never Was

It therefore pleased Bahnsen when the Simon Greenleaf Debate
Society contacted him with a proposal that he debate one of the au-
thors of Dominion Theology, Wayne House. In the early fall of 1988,
Hlias Hernandez of the debate society confirmed a date for this debate
(May 13, 1989) and the commitment of House to meet Bahnsen in

4, H, Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse?
(Portland, OR: Multnomah Press, 1988), p. 9.

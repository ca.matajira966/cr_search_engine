An Opportunity for Cross-Examination 9

the reader will understand well why the critique offered in Domin-
ion Theology is not credible. We also hope that the reader will see
what is at stake in the dispute and be encouraged to test all things
by the infallible standard of God’s holy word. If Reconstruction-
ism comes up wanting, then it is unworthy of your support. If it
communicates to you its biblical credentials and strength, then it
poses an important challenge concerning your life and involve-
ment as a Christian in this world.

“Prove all things; hold fast that which is good” (1 Thess. 5:21).

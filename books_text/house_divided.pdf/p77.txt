The Conflict of Visions a7

ity.”42 So cultural impotence becomes a mark of “separation” from
the “world system” and of orthodoxy.

Summary

Dispensationalism is a view of God’s work in history which
(commendably) wants to take account of the redemptive discon-
tinuities between the Old and New Testaments, such as the fact
that we do not offer animal sacrifices today. Dispensationalists are
motivated by a driving desire, as we all should be, to guard the
principle of salvation by grace, which believers enjoy today, from
the terrible error of seeking salvation through works of the law, as
taught by the Jews both before and after the advent of Jesus
Christ. However, the way in which dispensationalism attempts to
accomplish these ends unnecessarily divides the Bible into differ-
ent regimes of ethics and incorrectly separates Israel from the
Church as the covenant people of God.

We have found that:

1. The dispensational answer to how we should then live
comes to be characterized by cultural relativism, which jeopar-
dizes moral absolutes.

2. The dispensational answer to how we should then live
comes to be characterized by antinomianism, which jeopardizes
the Scripture’s unity.

3. The dispensational answer to how we should then live
comes to be characterized by legalism, which detracts from the
Scripture’s sufficiency.

4, The dispensational answer to how we should then live
comes to be characterized by pietism, which detracts from the
Christian calling to social transformation.

42, Ibid. , pp. 335, 240.

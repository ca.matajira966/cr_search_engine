The Reconstructionist Option 43

tine religious values (and thus the influence of Jesus Christ,
speaking in the Scripture) from the decision-making process of
those who set public policy. Reconstructionists repudiate the sacred/
secular dichotomy of life which implies that present-day moral stan-
dards for our political order are not to be taken from what the
written word of God directly and relevantly says about society
and civil government. This stance is a theologically unwarranted.
and socially dangerous curtailing of the scope of the Bible’s truth
and authority (Psalm 119-160; Isa. 40:8; 45:19; John 17:17; Deut.
4:2; Matt. 5:18-19).

We beseech men not to be conformed to this world, but to be
transformed by the renewing and reconciling work of Jesus Christ
so as to prove the good, acceptable and perfect will of God in their
lives (2 Cor. 5:20-21; Rem. 12:1-2). We call on them to be de-
livered out of darkness into the kingdom of God’s Son, who was
raised from the dead in order to have pre-eminence in ail things
(Col. 1:13-18). We must “cast down reasonings and every high
thing which is exalted against the knowledge of God, bringing
every thought into captivity to the obedience of Christ” (2 Cor.
10: 5) in whom “al! the treasures of wisdom and knowledge are
deposited” (Col. 2:3). Thus believers are exhorted to be holy in all
manner of living (1 Peter 1:15), and to do whatever they do for the
glory of God (1 Cor. 10:31). To do so will require adherence to the
written word of God since our faith does not stand in the wisdom
of men but rather in the work and teaching of God’s Holy Spirit
(1 Cor. 2:5, 13; cf. 1 Thess, 2:13; Num. 15:39; Jer, 23:16). That
teaching, infallibly recorded in “every scripture” of the Old and
New Testaments, is able to equip us “for every good work” (2 Tim.
3:16-17) — thus even in public, community life.

In light of the Biblical truths discussed above, Reconstruction-
ists are committed to the transformation (Reconstruction) of every
area of life, including the institutions and affairs of the socio-politi-
cal realm, according to the holy principles revealed throughout
God’s inspired word (theonomy).

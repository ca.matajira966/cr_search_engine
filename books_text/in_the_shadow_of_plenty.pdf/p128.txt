M2 In the Shadow of. Plenty

sword nor spear found in the hand of any of the people who were
with Saul and Jonathan, But they were found with Saul and Jona+
than his son (1 Samuel 13:22).

Imagine that! An entire army with no weapons.

Only the king and his son had any really efficient armaments.

No power. No resources. No army. No decent weapons. No
hope?

Perhaps the people should wait for another day to work for
their deliverance. Perhaps they should wait for the day of advan-
tage. Perhaps they should do nothing for now, waiting for a more
Opportune moment, After all, dominion doesn’t happen overnight.
Peace isn’t won in a day.

But, no.
Perhaps God desires for his people to “walk by faith, not by
sight” (2 Corinthians 5:7). “Perhaps . . . ,* thought Jonathan, “it

may be that the Lord will work for us. For nothing restrains the
Lord from saving by many or by few” (1 Samuel 14:6).

So he set out, just he and his armor bearer, alone, to attack the
Philistine garrison. To gain the promised “peace of the land.”

Then Jonathan said, “Very well, let us cross over to these men,
and we will show ourselves to ther. If they say thus to us, ‘Wait
until we come to you, then we will stand still in our place arid not
go up to them, But if they say thus, ‘Come up to us,’ then we will
go up. For the Lord has.delivered them into our hand, and this
shall be the sign to us.” So both of them showed themselves to the
garrison of the Philistines. And the Philistines said, “Look, the
Hebrews are coming out of the holes where they have hidden.”
Then the men of the garrison called to Jonathan and his armor
bearer, and said, “Come up to us, and we will show you sorme-
thing.” So Jonathan said to his armor bearer, “Come up after me,
for the Lord has delivered them into the hand of Israel.” And Jona-
than climbed up on his hands and knees with his armorbearer after
him, and they fell before Jonathan. And as he came after him, his
armor-bearer killed them. That first slaughter which Jonathan arid
his armorbearer made was about twenty men within about-half an
acre of land. And there was trembling in the camp, in the field,
and among all the people. The garrison and the raiders also

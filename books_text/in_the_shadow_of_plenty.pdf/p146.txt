130 In the Shadow of Plenty

quered tribes. It was understood as an instrument of conquest and
subjection, The Indians were placed on the reservations in order to
control them. There was no doubt about this in anyone’s mind.
The government funneled money down the bureaucracy, and the
Indians wound up in poverty,

That’s where they still are. There is no group beneath them.
Every American realizes that the Indians are on the bottom of the
pile: little hope, not much future, and not much money.

What most Americans don’t understand is that the govern-
ment funnels about a billion and a half dollars through the BIA
every -year. Now, there are about, 735,000 Indians in the U.S.
This means that the government spends $2,000 per year on every
Indian —man, woman, and child. If families average six people,
this is $12,000 a year.

Indians on the reservations pay no taxes, state, local, or fed-
eral. Thus, a reservation family can earn money without suffering
any tax consequences. Furthermore, almost no zoning or other
state and local regulations apply, so they can start any sort of busi-
ness on the reservations.

Here we see the incredible failure of a socialist experiment
that is well over a century old. The Indians are in poverty, yet the
government is providing the. BIA with enough money to make
every Indian. family middle class, and upper middle class if they
earn as little as $5,000 per family.

Conclusion: the Indians aren't getting the money. The BIA
bureaucracy is.

But this is only part of the story. The federal government owns
the reservations’ 52 million acres of land. It holds this land “in
trust” for the Indians. Tribal councils (socialist bureaucracies)
have some say in the use of the land. But only some.

R. J. Rushdoony served for a decade as a missionary to the
Western. Shoshone Indians in the Idaho-Nevada area. Here was
his evaluation of the program in 1954; -

Whatever the pre-reservation Indian was—and his faults were
real —he was able to take care of himself and had a character becom-
ing to his culture and religion. He was a responsible person. Today

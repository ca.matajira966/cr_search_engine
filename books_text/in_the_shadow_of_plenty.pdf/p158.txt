142 In the Shadow of Plenty

minds of many believers, Christians are to withdraw from human
culture in order to focus on “religious” exercises,

By clinging to a defective, incomplete view of Christ's Lord-
ship, we scuttle ourselves into a cultural backwater. By clinging to.
a defective, incomplete. view of spirituality, we imprison ourselves
within an evangelical ghetto. By clinging to a defective, incom-
plete view of spirituality, we minimize all impact, we sequester all
significance, and we stifle all hope.

Rather than nurturing the flock of God and the world at large
with the rich truths of practical Biblical instruction, we indulge in
theological junk food.-Rather than building all of life upon the un-
wavering foundation of God’s Word, we humor ourselves with in-
tellectual white elephants,

Is it any wonder that the sober secularists have captured the
attentions of our leaders? Is it any wonder that the humanists
have moved. with ease into the vacuum? Is it any wonder that
Christianity has been overthrown as'the foundational authority in
Western culture? The Bible has answers to the great and perplex-
ing problems of our perilous times, but because the church has
failed, those answers have gone largely unheard.

The collapse of Western culture, thus, has two “culprits.”
First, there is inhuman humanism; but there also is irrelevant,
passive Christianity.

Standing at the Crossroads
Our culture, thus, stands at the crossroads, On the brink of
crisis due to the twin evils of an aggressive, godless humanism
and a passive, irrelevant church, we now must make a choice. We
can either do nothing while our loved ones march ‘glibly down the
road to ruin, or we can reclaim all areas of life and culture to the
sanity of Scriptural moorings. The choice is ours.

. Choose for yourselves this day whom you will serve. . .
But as for me and my house, we will serve the Lord (Joshua
24:15),

Okay. So, what do we do? What should the church do? Now

160 In the Shadow of Plenty

—__.__. Unholy Spirits: Occultim and New Age Humanism,
(Fort Worth: Dominion Press, 1986).

—___..... An Introduction to Christian Economies, (Nutley,
New Jersey: Craig Press, 1973).

R. J. Rushdoony, The Institutes of Biblical Law, (Phillipsburg, New
Jersey:, Presbyterian and Reformed Publishing Company,
1973).

Herbert Schlossberg, Idols for Destruction, (Nashville, Tennessee:
‘Thomas Nelson Publishers, 1983).

Ray Sutton, That You May Prosper: Dominion by Covenant. (Fort
Worth, Texas, Dominion Press, 1986).

Thomas Sowell, Civil Righis: Rhetoric or Reality?, (New York, New.
York: William Morrow and Company, 1983).

Walter Williams, The State Against Blacks, (New York, New York:
McGraw Hill Book Company, 1982).

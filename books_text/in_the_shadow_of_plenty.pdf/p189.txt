‘SUBJECT INDEX

AEDG, 5
Athanasius, 18
Augustine, 18, 19
Almshouses, 18, 19

Bernard of Clairveaux, 18, 19
Body of Christ, 133-134, 136-150
boundary of fear, 147

Camelot, 4
Calvin, John, 19
Carey, William, 19

Communion, 31-36, 43-46, 145, 146,

147
covenant, 90-96 i
Carter, Jimmy, 121
Carson, Clarence, 122 :
church, the, 133-134, 136-150!
cause and effect, 138-140
crisis pregnancy center, 156 |

decentralization, 71-77

deacons, 81-86, 147-149

discrimination, 123, 124-126

declining industries 129

dominion, 34, 40-41; 43-46, 50-51,
111-414, 137, 150

discipline, 147

 

evangelism, 9-24, 26, 32-35, 79-81

elders, 81 :

entitlement, 55-56, 93-96, 122-124,
132 :

Eisenhower, Dwight, 5

enterprise zones, 134 i

ecclesiastical irresponsibility, 145
employment placement, 149
encouragement, 155, 156

Food stamps, 5

Francis of Assisi, 19

families, 72-76, 3-85, 147-149,
151-158

famine, 78-81

Ford, Gerald, 121

fairness, 132

fellowship, 146

food pantries, 148, 156

family businesses, 157

Great Society, 4
Great Awakening, 18, 19
gleaning, 58-66

Gilder, George, 122
Gaughin, Paul, 138

homelessness, 3, 4

humanism, 5-6, 115, 139-140

hospitals, 18

Hus, Jan, 18-19

Hazlitt, Henry, 122

hermeneutical irresponsibility,
143-144

Indian Affairs, 129-131
in-kind payments, 122
insurance, 148

Johnson, Lyndon, 4
justice, 132

173

20 In the Shadow of Plenty

knew “that God is not.one to show partiality,.but in every nation
the man who reveres Him and works righteousness, is welcome to
Him” (Acts 10:34-35). And this is “the word which He sent to, the
sons of Israel, preaching peace through Jesus Christ” (Acts 10:36).

Thus, they knew that righteous deeds of compassion were
essential for the fulfillment of the church’s mission and could not
be subjugated even to the mast critical of tasks: discipleship, pas-
toral care, or cultural reclamation. They knew that the words of
their mouths had to be authenticated by the works of their hands.

Faith at Work

In writing to Titus, the young pastor of Crete’s pioneer
church, the Apostle Paul pressed home this fundamental truth
with impressive persistence and urgency. The task before Titus
was not-an easy one, Cretan culture was marked by deceit, un-
godliness, sloth, and gluttony (Titus 1:12). And he was to provoke
a total Christian reconstruction there! He was to introduce peace
with God through Christ. Thus, Paul’s instructions were strate-
gically precise and to the point. Titus was to preach judgement
and hope, but he was also to make good deeds the focus of his out-
reach. Charity was to be a central priority.

Paul wrote:

For the grace of God that brings salvation has appeared to all
men, teaching us that, denying ungodliness and worldly. lusts, we
should live soberly, righteously, and godly in the present age, look-
ing for the blessed hope and glorious appearing of our great God
and Savior Jesus Christ, who gave Himself for us, that He might
redeem us from every lawless deed and purify for Himself His own
special people, zealous for good works (Titus 2:11-14).

Word and deed.

This was a very familiar theme for Paul. It wasn’t exclusively.
aimed at the troublesome Cretan culture. Like Isaiah before him,
he returned to it at every opportunity, Earlier, he had written to
the Ephesian church saying,

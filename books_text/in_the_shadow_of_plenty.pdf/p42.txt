26 In the Shadow of Plenty

But he wanting to justify himself, said to Jesus, “And who is my
neighbor?” Then Jesus answered and said: “A certain man went
down from Jerusalem to Jericho, and fell among thieves, who
stripped him of his clothing, wounded him, and departed, leaving
him half dead. Now by chance a certain priest came down that
road. And when he saw him, he passed by on the other side: Like-
wise a Levite, when he arrived at the place, came and looked, and
passed by on the other side. But a certain Samaritan, as he jour-
neyed, came where he was. And when he saw him, he had compas-
sion on him, and went.io him and bandaged his wounds, pouring
on oil and wine; and he set him on his own animal, brought him to
an inn, and took care of him. On the next day, when he departed,
he took out two denarii, gave them to the innkeeper, and said. to
him, ‘Take care of him; and whatever more you spend, when I
come again, I will repay you.’ So which of these three do-you think
was neighbor to him who fell among the thieves?” And he said,
“He who showed mercy on him.” Then Jesus said to him, “Go and
do likewise” (Luke 10:29-37).

Quite a story! What started out to be a test, a theological con-
frontation over the law, was suddenly transformed by the Lord
Jesus into a moment of conviction. The Pharisee found himself in
the valley of decision, And at the same time he was on the horns of.
a dilemma.

A Samaritan! How odd!

Seven hundred years earlier, Assyria had overrun and depop-
ulated the northern kingdom of Israel, including Samaria. The.
conquerors had a cruel policy of population-transfer that scattered
the inhabitants of the land to the four winds. Then, the empty
countryside was repopulated with a ragtag collection of vagabonds
and scalawags from the dregs of the Empire (2 Kings 17:24-41). In-
stead of regarding these newcomers as prospects for Jewish evan-
gélism, the people of Judah, who continued in independence for
another full century, turned away in contempt, and the racial
division between Samaritan and Jew began its bitter course.

Samaritans were universally despised-by good Jews. They
were half-breeds who observed a half-breed religious cultus.
Worse than the pagan Greeks, worse even than the barbarian

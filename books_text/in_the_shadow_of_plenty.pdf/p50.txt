34 In the Shadow of Plenty

11:23-25). He came to sup with us, and us with Him (Revelation
3:20). He came to prepare a table before us in the presence of our
enemies, to anoint our heads with oil, and to fill our cups to over-
flowing (Psalm 23:5). He came to-feed us at the glorious marriage
supper of the Lamb (Revelation 19:7-9).

Ever since, Christians have made’ the focus of their weekly
worship the Lord’s Supper. They have come together to eat. God
gathers His people about Him and feeds them from His bounty.

As a result, God's “Good Samaritans” in turn feed the hungry
because they themselves have been fed.

And because of that, they are the on/y ones capable-of feeding.
They are the only ones who have partaken of God’s own provision,
They are the ony ones to have been fruy sheltered, truly fed. They
are the ondy ones to have learned genuine refuge, genuine nourish-
ment. And they are the only ones to have before their eyes con-
stant reminders of that provision:.the land of promise, and the
memorial communion.

God’s people are the only ones fit far “Good Samaritan” service
because they are the only ones fit 4y “Good Samaritan” service.

Christians are supposed to exercise dominion over the whole
earth (Genesis 1:28; Matthew 28:19-20). It is their job—an assign-
ment from God that they and they alone will be equipped to ful-
All in eternity. But power and authority come through service. There is
no more fundamental principle of dominion in the Bible. Charity
is the first step toward reformation and victory (Isaiah 58:10-12).

- From the Table of the Lord

Historically, Christians have cared for the poor, remembering
their-own care from above. And they have particularly remem-
bered this in connection with the Lord’s Supper. The Free Presby-
terian Church of Scotland has for centuries now provided for
almsgiving immediately following the communion meal. Like-
wise, the Christian Reformed churches traditionally have a spe-
cial offering for the poor after the quarterly Lord’s Supper service.
Primitive Baptists make special provision for charitable offerings
as the communion elements are passed, that all may feast. The

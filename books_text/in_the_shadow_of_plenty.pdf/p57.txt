Dominion by Service 4

The kings of the Gentiles exercise lordship over them, and
those who exercise authority over them are called “benefactors.”
But not so among you; on the contrary, he who is greatest among
you, let him be as the younger, and he who governs as he who
serves. For who is greater, he who sits at the table, or he who
serves? Is it not he who sits at the table? Yet [ am among you as the
One who serves. But you are those who have continued with Me in.
My trials. And I bestow upon you a kingdom, just as My Father
bestowed one upon Me, that you may eat and drink at My table in
My kingdom, and sit on thrones judging the twelve tribes of Israel
(Luke 22:25-30).

Unfortunately, Christians have not understood this link be-
tween charity and authority. They have time and again fallen into
the trap that snared Ahaz: alliances with the enemy.

Someone must be in charge. There is no escape from responsi-
bility. When people are needy, or fearful, or desperate, they seek
protection. Who will give it to them? And what will the protector,
the benefactor, ask in return?

This is why the question of the responsibility for charity is ulti-
mately a question of authority. And this is why the issue of charity
is such a volatile issue. At stake is ultimate control over the soci-
ety. For that men will go to war.

Thus, the battle for the control over charity is very similar to a
military campaign. And God’s people are warned repeatedly by
God: make no alliance with foreign gods. Make no alliances with
the enemy.

Unholy Alliances

God’s warnings against entering into unholy alliances are
abundant and clear.

Do not enter the path of the wicked, And do not walk in the
way of evil, Avoid it, do not:travel on it; Turn away from it and
pass on. For they do not sleep unless they have done evil, And
their sleep is taken away unless they make someone fall. For they
eat the bread of wickedness, And drink the wine of violence. But
the path of the just is like the shining sun, That shines ever brighter

Daminion by Service : 45

Unmarked by unholy alliances and the striving of mere men,
the service of the Lord’s Supper was thus the ultimate sign of the
church’s influence over the world. It marked her dominion over
the powers and principalities and it distinguished her from the so-
called “benefactors” of the world. It set her apart as the nursery of
the Kingdom: serving from the very table of God.

The crumbs from this spiritual table are to feed the world
(Matthew 15:27).

Bureaucracies and Benefactors

The contemporary church has, in utter defiance of this basic
Biblical truth, struck a deal with the “benefactors,” thus diluting
or perhaps even nullifying their influence over the world. Though
her leaders readily acknowledge their responsibility to care for the
poor, they have “gone down to Egypt” for help. Like Ahaz, they
have looked around at apparently impossible circumstances and
have decided to enter into an unholy alliance. Instead of trusting
God, feeding the hungry, clothing the naked, sheltering the home-
less, and comforting the distressed His way, from the table of the
Kingdom, they have run to the lumbering bureaucracies of big
government for assistance.

Scripture demands that Christians do the work of charity, not
bureaucrats. Scripture asserts that the church is to be society's pri-
mary advocate for the needy, not the state. Believers are to lead the
way with unswerving compassion and concern.

In no other way can God’s people gain long-term dominion and
authority. The principle of service is the foundation of dominion.

When the church and her leaders call for more government in-
terference in the economy, more programs to “help the poor,” and
more legislation to provide entitlements, benefits, and affirmative
action, the work of the Kingdom is inevitably compromised and
paralyzed. An unholy alliance has been forged. When the church
and her leaders go and ask the state to do what she should be do-
ing, when she attempts to escape her responsibility by placing it
on the shoulders of bureaucracy, dominion is subverted. When
the church abandons the table of the Lord, and the responsibility

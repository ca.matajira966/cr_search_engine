Sheaves for the Provident 59

So Ruth the Moabitess said to Naomi, “Please let me go to the
field, and glean heads of grain after him in whose sight I may find
favor.” And she said to her, “Go, my daughter.” Then she left, and
went. and gleaned in the field after the reapers. And she happened
to come to the part of the field belonging to Boaz, who was of the
family of Elimilech (Ruth 2:2-3).

According to God’s Law, charity was essentially opportunity.
Opportunity to work. Opportunity to labor. Opportunity to pull
up by the bootstraps,

And that’s all Ruth wanted! She didn’t need a handout. She
didn’t need to stand in lines, to fill out forms, to wade through bu-
reaucratic red tape. She needed the opportunity to work.

Now behold, Boaz came from Bethlehem, and said to the reap-
ers, “The Lord be with you!” And they answered him, “May the
Lord. bless you!” Then Boaz said to his servant who was in charge
of the reapers, “Whose young woman is this?” So the servant who
was in charge of the reapers answered and said, “It is the young
Moabite woman who came back with Naomi from the country of
Moab. And she said, ‘Please let me glean and gather after the reap-
ers among the sheaves.’ So she came and has continued from morn-
ing until now, though she rested a little in the house” (Ruth 2:4-7).

Gleaning was hard, backbreaking work. Following behind
harvesters, collecting the overlooked, cast off, and leftover grain
could not have been easy or pleasant for Ruth. But she was deter-
mined to live in terms of God's covenant (Ruth 1:16-17) and to
move up out of destitution by working with all diligence and fervor.

‘This determination was impressive. And it especially impressed
Boaz, the owner of the field from which Ruth was gleaning.

Then Boaz said to Ruth, “You will listen, my daughter, will
you not? Do not go to glean in another field; nor go from here, but
stay close by my young women. Let your eyes be on the field which
they reap, and go after them. Have I not commanded the young
men not to touch you? And when you are thirsty, go to the vessels
and drink from what the young men have drawn.” Then she fell on
her face, bowed down to the ground, and said to him, “Why have I
found favor in your eyes, that you should take notice of me, since I

6
CHARITY BEGINS AT HOME

But if anyone does not provide for his own, and especially for
those of his household, he has denied the faith and is worse than an
unbeliever (1 Timothy 5:8).

The job was just too big for one person. Even for a person as
gifted, as dynamic, as extraordinary as Moses was.

Day alter day, week in and week out, from morning until
evening, the children of Israel thronged around Moses. Ques-
tions, Disputes, Complaints, Concerns.

Tt was just too much.

Physically, emotionally, and spiritually, Moses could hardly
keep up such a demanding schedule. Besides, he had other mat-
ters to attend to. He had to plan. He had to lead. He had to care
for his family. He had to attend to his relationship with the Lord.

But on the other hand, he just couldn’t tum away all these
people, needy people, Ais people.

So when Moses’ father-in-law saw all.that he did for the people,
he said, “What is this thing that you are doing for the people? Why
do you alone sit and all the people stand before you from morning
until evening?” And Moses said to his father-in-law, “Because the
péople come to me to inquire of God. When they have a difficulty,
they come to me, and I judge between one and another; and [
make known the statutes of God and His laws” (Exodus 18:14-16).

What Moses was attempting to do was quite admirable, but it
was also quite foolish. He wouldn't be able to hold up long under
such a tremendous strain. And Jethro, Moses’ father-in-law, knew it.

67

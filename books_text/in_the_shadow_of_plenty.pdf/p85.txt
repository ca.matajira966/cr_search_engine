Charity Begins at Home "69

at all times; the hard cases they brought to Moses, but they judged
every smail case themselves (Exodus 18:24-26).

Jethro was simply encouraging Moses to take advantage of the
principle of the division of labor.

Moses may have been able to give judicious counsel based on
God’s direct revelation, he may have been able to dispense “per-
fect” justice, and he may have been able to provide instantaneous
satisfaction to the people, but he was ultimately limited by time
and space. And those were limitations that no amount of bril-
liance or inspiration could overcome.

Besides, there were literally hundreds of qualified and gifted
men among the people who were not being utilized at all in the
work of the ministry. They were.men who feared God, men of
truth, men who hated dishonest gain~men who were simply
wasting their leadership capabilities (Exodus 18:21).

God’s plan was to make them a “nation of priests” (Exodus
19:6). He wanted to equip them al/ to be a peculiar people (Deu-
teronomy 26:18), a light to the nations (Isaiah 42:6), zealous for
good works (1 Peter 2:9). It would be necessary then to give them
individual responsibilities. To decentralize. To effect a division of
labor. To recognize the gifts and callings of the people. A// the people.

Empire vs. Kingdom

What is the source of mankind’s earthly blessings? The Bible is
clear: God is. “And you shall remember the Lord your God, for it
is He who gives you power to get wealth, that He may establish
His covenant which He swore to your fathers, as it is this day”
(Deuteronomy 8:18). “Every good gift and every perfect gift is
from above,and comes down from the Father of lights, with whom
there is not variation or shadow of turning” (James 1:17).

What is the proper source of earthly authority? The Bible is
clear on this point: God is. God executes judgement, in time and
on earth (Deuteronomy 8:19-20).

How does God exercise His authority? Both directly and in-
directly. God has established several earthly institutional inter-

Social progress comes with the accumulation and development
of wealth. Wealth comes, in a free economy, as a product of work
and thrift—in short, of character. Capital is often accumulated by
inheritance, a God-given right which is strongly stressed in the
Bible. According to Proverbs 13:22, “A good man leaveth an in-
heritance to his children’s children: and the wealth of the sinner is
laid up for the just.” Inheritance makes possible the accumulation
not only of wealth within a family but of social power. Power is in-
escapable in any social order: it can either be concentrated in the
state, or it can be allowed to flourish wherever ability makes it
possible among the people. This decentralized wealth means also
decentralized and independent power. Instead of a concentration
of power in the state, there is instead a decentralization of power
which moves in terms of varying and independent goals.

Again, in a free economy, properly is freed from the restrictions
of the state because it is under the restrictions of the family and of
a religiously oriented community. In biblical law, there is no prop-
erty tax, which means a basic and inalienable social security in
the family and in property. The security of a man in his property,
and in his inheritance, means a stability in the social order which
is productive of progress.

R. J. Rushdoony*

*Rushdoony, Politics of Guilt and Pity (Fairfax, VA: Thoburn Press (1970)
1978), pp. 236-37.

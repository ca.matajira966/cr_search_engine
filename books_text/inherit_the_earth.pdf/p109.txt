Let’s Make a Deal 101

Slaves of the State

In contrast, we have the government-controlled socialistic so-
ciety which places many restrictions on what men can do for a liv-
ing, when they can do it, and how much they'll be allowed to
receive for the performance of their duties. It also imposes lots of
paper work and other kinds of official requirements on people,
who are required to report constantly to superiors who have no
direct economic incentives (“take-home bonuses”) in the perform-
ance of those under their authority. Productive people have to
spend more time filling out forms than they do dreaming up crea-
tive ways to serve the consumer.

One of the reasons why Western society over the last two hun-
dred years has had more rapid economic growth than any soctety
in history is because men have been left free before God to do
their best, and to serve God and man in the way that they felt they
could best serve them, given their own limitations of skills, limita-
tions of money to invest, and limitations of vision. They are also
allowed to keep the fruits of their labor, their foresight, and their
cost-effective planning. In short, they are allowed to profit.

By allowing individuals to compete in an open marketplace,
with each man doing his best to serve the needs of consumers,
Western society has taken advantage of the skills of hundreds of
millions of individuals— individuals who probably would never
have made the effort to refine their skills and abilities had they re-
mained slaves of the State, or slaves of other individuals.

The basis in the Bible of free labor, meaning legally free labor,
is the doctrine of the personal responsibility of each man to exer-
cise his occupation, meaning his calling, before God. Because
each man is told to work out his salvation with fear and trem-
bling—to become responsible for his own life and sustenance~
the State is required by God to avoid telling people how they are
to bargain contractually with other people.

When we allow men to serve as free men in the marketplace,
we are thereby affirming a system in which each man has an op-
portunity to prove his ability to his fellow man. Each man has an
opportunity to come before any other man and offer to exchange

CONCLUSION

I have introduced some important topics of economic theory
by way of the Bible’s covenant structure. This covenant structure
has five parts:

1. God’s transcendence (absolute difference from and sover-
eignty over man), yet also His immanence (constant presence with
man)

2. Authority-hierarchy (man’s chain of command structure
under God)

3. Ethics-dominion (God’s authoritative laws over man that
give man power when obeyed)

4. Judgment-punishment (God’s promised judgments and
man’s judging activities)

5, Legitimacy-inheritance (God’s system for man’s in-
heritance)

I have tried to show that all five concepts are inescapable.
Every society must adopt these Biblical covenantal principles, or
else imitate them. In economics, these principles translate into the
following applications in relation to God:

. Ownership: original and delegated
. Authority through obedience

. Prohibitions against theft

. Scarcity as a curse-blessing

. World conquest through obedience

The Bible teaches that God is the Creator and Sustainer of the
universe. The Bible also teaches that only God is both all-knowing

133

MPO

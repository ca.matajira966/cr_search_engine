Introduction 7

meaning the restoration of God’s economic principles,

If 1am correct, then Christians must begin immediately to re-
construct their own lives, families, and churches before God’s
judgment on society begins. We must prove ourselves ready to
lead. We do this by following God now, before judgment begins.
Obedience to God's principles produces leadership. Disobedience
to God’s principles produces His judgment: man’s disinheritance
from God’s riches.

If you don’t want to be disinherited, either eternally or on
earth, then start obeying God.

State Responsibilities 157

California for over a decade. Voters kept the “rich” from enjoying
the service; they also kept themselves from enjoying it.

Pay T.V. is now beginning to erode the government-granted
(Federal Communications Commission-licensed) monopoly of
“free” T.V. This was what free T.V. feared a generation ago. The
monopolists wanted a hammerlock on the consumer. This is what
government restrictions on trade are all about. It’s not a question
of monopoly vs. no monopoly when it comes to government-
mandated restrictions on trade; it’s a question of who gets the
monopoly.

The Loss of Freedom

As the State grows, it gains support for even further growth by
promising benefits to its supporters. It promises offsetting finan-
cial grants that will supposedly make up for losses to those who
are being abused by the existing State-created monopolies, It is a
never-ending competitive struggle for control aver the granting of
monopolies.

And behind every monopoly is the ultimate institutional mo-
nopoly: the legal monopoly of violence.

The problem for justice-seekers is that as men lose their free-
doms, they become increasingly dependent on their master, the
State. They want more and more. After all, at a below-market
price for anything, there is greater demand than supply. The
State offers “free” services. There will be heavy demand for them.
The State has told people that they have a “right” to these “free”
services, Now the voters are demanding “rights”—a lot more po-
tent appeal than simply a request for a hand-out or political
pay-off.

The State finas that it cannot afford to defend the nation and
provide justice when it has to feed the hungry, clothe the poor,
house the homeless, and give a college education to every semi-
literate student who wants to stay out of the labor force for a few
more years. So the politicians take the path of least resistance:
they buy the votes of the irresponsible. They try to walk away
from the State's God-assigned responsibilities in the few areas that

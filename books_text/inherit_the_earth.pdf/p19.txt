God Owns the World ii

Creation and Original Ownership

The first principle of a Biblical covenant is the principle of
sranscendence: God’s absolute supremacy. God reigns supreme
over everything. This means that He is high above the creation,
and totally different from it. We deal with a sovereign God. In
short, God runs the show.

This principle of transcendence relates to economics because
ownership is ultimately theocentric (God-centered). He created all
that exists, and He is at the center of the universe as its owner.
This means that ownership is ultimately a religious concept. It cannot be
properly understood without reference to God as the absolute
owner of the creation. Similarly, it is impossible to discuss prop-
erly the responsibilities of ownership (which is what this book is alt
about) without also discussing what God specifically requires of
men in their capacity as owners of property.

Providence

The doctrine of creation leads to a second doctrine, the doc-
trine of providence, meaning God’s full-time maintaining and
sustaining of the creation. God watches over and cares for the uni-
verse in a personal way. Not only did He create it, but He also
sustains it. He makes certain that it continues through time, and
it’s solely through the power of God that the earth and the uni-
verse around it are sustained.

We read in the New Testament book, Colossians: “For by Him
all things were created that are in heaven and that are on earth,
visible and invisible, whether thrones or dominions or principali-
ties or powers. All things were created through Him and for Him.
And He is before all things, and in Him all things consist” (Colos-
sians 1:16-17). The point is quite clear: God not only created the
earth but He also sustains it. It is through His son Jesus Christ
that history exists, that the world continues to operate. In short:
no Gad—no universe.

God created and sustains everything. This is why David the
Psalmist announced that it is God who is the owner of all the

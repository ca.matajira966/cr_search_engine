God Oums the World 19

satisfaction of knowing that the seller bore a financial loss because
of his decision. If they lose, then at least he loses, too.

The Auction

How does society convince individuals to meet the needs of
the largest and most productive segments of that society? It does it
through competitive bidding. Ownership, therefore, is very much
like a giant auction; the high bid normally wins. Even when the
high bid doesn't win, the dnown high bid always makes itself felt in
the actual decision of the seller.

The seller must decide whether he wants more money, or
more satisfaction from giving all or a portion of that property to a
particular individual or group. But the social function of owner-
ship cannot be escaped. Hour by hour, minute by minute, the in-
dividual who owns a piece of property gives up all of the income
he otherwise could have received if he had just sold the property
or put it to some other use.

There is no escape from this process. The market makes itself
felt every moment of the day because of the income which is lost
from afl competing uses of the property. In the mind of the prop-
erty owner, the benefits received from a particular use of the prop-
erty offset these losses.

Theocentric Ownership

1 have already said that all ownership is given by God and
guided by God. All ownership is therefore providential. Ownership
is therefore theocentric. God is at the center of all ownership. This
has important economic implications.

Man’s Limited Knowledge

One of the characteristics of God is that He knows everything
there is to know. He is omniscient (all-knowing). Before time
began, God knew everything that is taking place today, and He
knows everything that will take place in the future (Ephesians 1).
There is nothing in the universe that takes place that God is not

Dominion by Subordination 31

leader of other individuals. This is true in families, in businesses,
and in military operations. It’s true in every area of life. Authority
over others is always by rank. Authority over others is always by
covenant (a legally binding interpersonal bond under God:
church, State, or family) or contract (a legally binding agreement
among people). Every covenant or contract requires men to sub-
mit themselves to the authority of some ruling agent, if only the
enforcing authority.

The Trinity

Christianity teaches that there are three persons in the God-
head. Each person is equal in honor and majesty, power, and
glory, yet each of the persons has a different function with respect
to mankind and the creation. Throughout His career, Jesus
affirmed that He was simply doing the will of His Father, from His
youth (Luke 2:49) to His death (Matthew 26:39). This does not
mean that Jesus was inferior to God the Father in terms of His being;
it simply means that He was subordinate in His function in relation-
ship to God, meaning subordinate in His activities with respect to
the church and the creation in general. Jesus’ submission to the
Father’s authority in history doesn’t imply moral inferiority or in-
feriority in terms of His being. (It is always a sign of false theology
when the inferiority of Jesus is proclaimed, in contrast to His sub-
ordinate functional status in history under God the Father.)

We also find that the Holy Ghost is sent by both the Father
(John 14:26) and the Son (John 16:7) to minister unto mankind.
In other words, the Holy Ghost is under the authority of both the
Father and the Son. This also does not mean that the Holy Ghost
is inferior in His being to the Father and the Son. It means only
that He is under Their authority in His relationship to history.

If two of the very persons of the Godhead are not jealous of the
Father, neither should men be jealous of lawful authority.

The Family
Husbands are to exercise godly, responsible dominion, and

they are to take responsibility for the actions of the members of
their families, precisely because heads of households are God's

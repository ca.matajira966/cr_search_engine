Theft 45

To the extent that the State in this century has squandered its
resources on other kinds of goals besides protection of private
property, protection of life and limb, to that extent the State has
forfeited its claims to receive support from the public.

When individuals do not honor the law of God by self-govern-
ment, it becomes extremely expensive for society to protect itself
against the loss of its assets. Billions of dollars must be spent in
law enforcement, court systems, and all the other devices that de-
fend us against theft. If men were self-governed by the fear of
God, and also by their own sense of personal integrity, we would
see a drastic reduction in theft and a rapid increase in economic
growth and individual wealth,

Restraining Theft

How do you keep a society from indulging in theft? The most
important of all restraints is the fear of God. If men believe that
God is a perfect Judge, and that He will condemn them through
perfect punishment throughout eternity, they will be much more
careful about indulging their sins and their lusts. So the first and
most important restraint is the fear of God. Self-government under
God is the primary means of restraint.

Second, it is the responsibility of the family to teach basic
principles of righteousness, so the father’s role in the early years as
a disciplinarian and moral instructor (Deuteronomy 6:6-7) is ex-
tremely important.

Third, the preaching of the churches against theft is basic to
molding a righteous society. Two thousand years of such preach-
ing made possible the wealth of Western civilization.

Finally, of course, the civil government is the God-ordained
earthly agent of punishment. The State is to be an agent of anti-
theft, anti-coercion, and anti-fraud.

Theft by Ballot Box

What happens to society if men begin to vote for ownership of
their neighbor’s property? In other words, what happens if men
begin to steal from one another by means of the ballot box? What

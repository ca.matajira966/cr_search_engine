46 Inherit the Earth

if they decide to “vote themselves rich” (political covetousness)?
More to the point, what if they decide to “vote their richer neigh-
bors poor” (political envy)? What restraint can then be imposed?

If people believe that they can tax other citizens at a higher
percentage of taxation than they are burdened with, they will also
be tempted to give the State the authorization to confiscate the
property of those citizens.

Never forget, the extremely rich members of society always
have a sufficient number of lawyers, accountants, and tax Ioop-
holes to escape the highest levels of taxation. The people who at
Jirst are the primary victims of “tax reform” are members of the up-
per middle class. These are the people who are the most innovat-
ive; they are the backbone of Western society. They work smarter
than other people (though not necessarily harder). Socialism is
designed to break this backbone.

Ultimately, through mass inflation, everyone is pushed up
into the highest income tax brackets, and the trap of ballot box
theft is sprung on them personally. Surprise, Surprise! God will
not be mocked.

One way of restraining people in their lust to get a greater por-
tion of their fellow citizens’ wealth is to have all tax rates apply
equally to every citizen. This is sometimes called a flat tax. In the
Bible, it’s called the dthe. Then, if people vote to increase the rate
of taxation, it hurts them as much as it hurts their neighbors,

Socialists and Communists hate the idea of a flat tax, This is
why Karl Marx included a highly graduated income tax (higher
tax rates for rich people) as the second plank in his ten-part pro-
gram to destroy capitalism. (Communist Manifesto, 1848, last sec-
tion of Part II.)

The Bible teaches that all laws are to apply equally to all
members of society. The Bible teaches that God is not a respecter
of persons. This means that God does not play favorites. This is
emphasized over and over in the Bible as a principle of justice
(Leviticus 19:15; Deuteronomy 1:17; 16:19; Acts 10:34). Laws must
not be passed that discriminate against any one segment of the
population, unless God’s law defines them as criminals.

64 Inherit the Earth

Christians as the Heirs

God had transferred title to the land of Canaan when He
promised Abraham that his heirs would inherit it (Genesis 15:7).
They didn’t possess it for four generations, as God predicted
(15:16). Possession of the inheritance was not automatic.

The land of conquered Canaan was supposed to serve the
Hebrews as the garden was to have served Adam: as a training
ground, They were to use it as a base of operations in a world-
wide program of conquest — conquest by ethics. Jonah’s ministry was
the great Old Testament example. But the Hebrews failed in this
task of world-wide evangelism. Satan remained the squatter-
inheritor of the world,

When Jesus announced the beginning of His public ministry
by proclaiming the fulfillment of the jubilee year, He was thereby
announcing the transfer of title: from the deceased Adam to God's incar-
nate Son. What was listed on that deed? The whole world. Jesus was
claiming His inheritance as God’s legitimate son. Satan the squat-
ter was put on notice: the heir has come.

Jesus went out and fulfilled the terms of Isaiah 61: He com-
forted the brokenhearted, healed the sick, and set spiritual cap-
tives free. Then He died on the cross,

The Old Testament laws of inheritance named the brothers of
the deceased as the lawful heirs, if he left behind neither a wife nor
children (Numbers 27:9). Who are Jesus’ lawful heirs? His ethical
followers. “And He stretched out His hand toward His disciples
and said, ‘Here are My mother and My brothers!’” (Matthew
12:49). Those who obey the moral laws of Jesus are the heirs of His
inheritance.

This means that Christians have legally inherited the world. This is
why Jesus commanded His followers to go out and disciple (disci-
pline and rule) the nations (Matthew 28:18). They are to claim their
inheritance in His name.

The basis today for Christians’ collecting their lawful inherit-
ance is hard work, moral faithfulness to God and man, and build-
ing up their families’ savings. They are to earn their inheritance in
the same way that Adam was supposed to earn it: by managing

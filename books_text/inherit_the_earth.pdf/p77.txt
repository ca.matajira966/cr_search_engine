Inheriting the World 69

The important point is this: the parent has an economic incen-
tive to make good administrators and good businessmen out of his
children. If he believes that the State will intervene and take away
the money, he has far less incentive to teach his children the skills of
business. If anything, he will teach his children the skills of politics.

An Illegitimate Heir

What the State is actually saying when it legislates inheritance
taxes is that the State is in principle the legitimate heir of all families. The
politicians are saying that modern sons and daughters of the rich
don’t have the right to inherit, since they aren’t the ones who will
be the ultimate supporters of the parents, meaning “parents in
general.” Why should “parents in general” object if the State, as
their future benefactor and supporter, should inherit the family
fortune?

The modern socialist State promises to take care of everybody,
if necessary, womb to tomb. For example, the idea is being aban-
doned which insists that parents are responsible for the education
and support of their children. State officials assert that they are re-
sponsible for the education and welfare of the children.

Why should children object when the State inherits? After all,
the true parent is the State. The State educated them, protected
them, and now promises to guarantee lifetime employment to
them. (Lifetime employment is the law of the land in the United
States: the Full Employment Act of 1946. It says that the govern-
ment has a legal responsibility for creating conditions of full em-
ployment, meaning mass inflation, if necessary.)

Since the State claims authority over the children, this tends to
make the parents much more short run in their perspective. They
realize that they are not the ones who have the primary responsi-
bility for the education and training of the children, They realize
that they have transferred responsibility to another agent, the
State. The State understands this also, and politicians assert the
new doctrine: inheritance passes to the State.

The State promises that the parents will retire on Social
Security welfare payments, or other State-managed (ha!) capital.

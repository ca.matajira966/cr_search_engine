Inheriting the World a

The Biblical Response to the Welfare State

The modern State promises to support its citizens from womb
to tomb. It educates the children, cares for the aged, and steadily
transfers power to the government officials by taking on new re-
sponsibilities. It taxes our labor, it taxes our profits, and it taxes our
children’s inheritance. It has become a substitute parent for young
children, and it has become a substitute child for aged parents. It
has taken over the economic responsibilities that each generation
is supposed to bear, It therefore insists that it is the lawful heir.

In fact, the modern “Savior State” is a bastard pretender, It is
one more example of Satan’s efforts to maintain control of the in-
heritance he extracted from Adam. He still keeps control by lur-
ing men into sin. In this case, it is the sin of family irresponsibil-
ity. It is also the sin of worshipping the State.

How should Christians attempt to recapture the power and
authority that the State has taken away? The starting point has to
be that parents and family members reassert their responsible role
as God’s designated institutional source of their own social wel-
fare, The family is the primary agency of social welfare in every civilization.
State officials may deny this, and they may attempt to transfer to
themselves the authority for welfare that the family rightfully
holds, but there is no way that the State can completely enforce
this transfer of responsibility. Nevertheless, it may go bankrupt
trying. It may drive its citizens into bankruptcy, too. The State
always extracts wealth from the family in this unjust attempt to
become the rightful heir.

“Charity begins at home.” This is a famous phrase in Ameri-
can life. It is an accurate phrase. This is precisely where charity
must begin. This doesn’t mean that charity is limited only to the
home. On the contrary, charity only begins at home; it isn't sup-
posed to end there (2 Corinthians 8).

Children must learn the basics of charity, and charity must
flow from one home to other households. It's the family which is
the primary agency of welfare, and in a community where strong
families exist, there will be less and less politically perceived ne-
cessity for immoral forms of State-administered welfare.

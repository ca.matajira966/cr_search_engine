‘No Trespassing!” 83

men, Wherever socialism is widely believed by the people, the
church is persecuted, or at least discriminated against. Socialism
is inherently anti-Christian, and Christianity is inherently anti-
socialist.

Exclusion and Dominion

The model of adoption is basic to the model of property own-
ership. If God establishes His eternal claims of ownership over
mankind, it should not surprise us that He also holds men respon-
sible for the administration of His property.

The administration of property is a training ground for do-
minion. This means that certain people must be made legally and
economically accountable before God and other men. They need
the right to exclude other people from their lawful property if they
are to become wise managers of that property. They also need this
protection as an encouragement to make the heavy sacrifices nec-
essary to make any project pay off. The sacrifices of owners in in-
creasing their property are similar to the sacrifices of adoptive
parents. Adoptive parents insist on (and need) the assurance that
their position as parents will be upheld by civil law. So do prop-
erty owners.

Property ownership is not to become some monopoly of an _
elite corps of State-appointed or State-elected officials, any more
than parenthood is. It is not simply some distant bureaucracy
which is to possess the exclusive right of keeping others from
“State” property (meaning, ultimately, property controlled by the
administrators). Every man is to be encouraged to become a
property owner—a responsible steward before God. Decentral-
ized property ownership takes advantage of the Biblical principle
of the division of labor.

It is interesting that in communist societies, from Plato’s uto-
pian (nowhere) “Republic” to modern Soviet society, State officials
have demanded parental rights over children. They set up day
care centers and require working mothers to leave their children
under State care. Furthermore, compulsory education in State-
licensed schools is a universal aspect of the modern Savior State.

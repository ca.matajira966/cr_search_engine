Chapter IX

DOWNWARD PRICE FLEXIBILITY
AND ECONOMIC GROWTH

[The concept of downward price flexibility as the result of a
free market in monetary relations and expanded production
obviously is opposed to virtually all modern schools of eco-
nomic thought—Keynesian, University of Chicago, the old
Irving Fisher Plan, Social Credit schemes, “greenbackism,”
Silvio Gesell’s program. Only the Austrian School, led in
this century by Ludwig von Mises, approaches the problem
of monetary theory as a problem to be solved by the forces
of the free market. Among free market analysts, the Chicago
School, led by Milton Friedman, has been the major group
advocating continuous expansion by the government of the
money supply. The refutation of this line of thought can be
found in Hans Sennholz’s essay in the Festschrift to Mises,
Toward Liberty (Menlo Park, Calif.: Institute for Humane
Studies, 1971): “Chicago Moneiary Tradition in the Light of
Austrian Theory,” and in Murray N. Rothbard's essay, “The
Great Inflationary Recession Issue: ‘Nixonomics’ Explained,”
The Individualist (June, 1970).]

It would appear that the reasons commonly advanced as a proof
that the quantity of the circulating medium should vary as pro-
duction increases or decreases are entirely unfounded. It would
appear also that the fall of prices proportionate to the increase in
productivity, which necessarily follows when, the amount of mon-
ey remaining the same, production increases, is not only entirely
harmless, but in fact the ouly means of avoiding misdirections of
production.
F. A. Hayek
Prices and Production, p. 105

Economic growth is one of the chief fetishés of modern Life.
Hardly anyone would challenge the contemporary commitment to
the aggregate expansion of goods and services, This is true of social-
ists, interventionists, and free enterprise advocates; if it is a question
of “more” as opposed to “Jess,” the demonstrated preference of the

vast bulk of humanity is in favor of the former.

To keep the idea of growth from becoming the modern equivalent

93

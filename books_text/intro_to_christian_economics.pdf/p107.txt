Downward Price Flexibility and Economie Growth 95

free market economists is the issue of monetary policy, The ma-
jority of those calling themselves free market economists believe that.
monetary policy should not be the autonomous creation of voluntary
market agreements. Instead, they favor various governmental or
quasi-governmental policies that would oversee the creation of money
and credit on a national, centralized scale. Monetary policy in this
perspective is an “exogenous factor” in the marketplace—something
that the market must respond to rather than an internally produced,
“endogenous factor” that stems from the market itself. The money
supply is therefore only indirectly related to market processes; it is
controlled by the central governments acting through the central
tank, or else it is the automatic creation of a central bank on a fixed
percentage increase per day and therefore not subject to “fine-tuning”
operations of the political authorities,

A smaller number of free market advocates (myself among them)
are convinced that such monopoly powers of money creation are go-
ing to be used. Power is never neutral; it is exercised according to the
value standards of those who possess itt Money is power, for it
enables the bearer to purchase the tools of power, whether guns or
votes, Governments have an almost insatiable lust for power, or at
least for the right to exercise power. If they are granted the right to fi-
nance political expenditures through deficits in the visible tax sched-
ules, they are empowered fo redistribute wealth in the direction of
the State through the invisible tax of inflation.®

Money, given this fear of the political monopoly of the State,
should ideally be the creation of market forces, Whatever scarce
economic goods that men voluntarily use as a means of facilitating
market exchanges—goods that are durable, divisible, transportable,
and above all scarce—are sufficient to allow men to cooperate in
econamic production, Money came into existence this way; the State
only sanctioned an already prevalent practice,? Generally, the two
gaods that have functioned best as money have been gold and silver:
they both possess great historic value, though not intrinsic value
(since no commodity possesses intrinsic value}.7

Banking, of course, also provides for the creation of new money.

4. F, A. Hayek, The Road to Serfdom (University of Chicago, 1944), is by
far the best treatment of the unneutra} nature of state planning boards.

3, Murray N. Rothbard, “Money, the State, and Modern Mercantilism,” in
Helmut Schoeck and James Wiggens (eds.), Central Planning and Neomercan-
ilism (Princeton: Van Nostrand, 1964), pp. 140-143.

6. Ludwig von Mises, The Theory of Money and Credit (New Haven,
Conn.: Yale University Press, 1953; reprinted 1971 by the Foundation for
Economic Education}, pp. 97-123.

7. Gary North, “The Fallacy of ‘Intrinsic Value," The Freeman (June,
1969) [chap. 7, above].

Downward Price Flexibility and Economic Growth 101

The logic of economic expansion should be clear enough: if it
takes place within a relatively fixed monetary structure, either the
velocity of money will increase (and there are limits here) or else
prices in the aggregate will have to fall. If prices are not permitted to
fall, then many factors of production will be found to be uneconomical
and therefore unemployable, The evidence in favor of this law of
economics is found every time a depression comes around (and they
come around just as regularly as the government-sponsored monetary
expansions that invariably precede them**), Few people interpret
the evidence intelligently.

Labor union Jeaders do not like unemployed members. They do
not care very much about unemployed nonmembers, since these men
are unemployed in order to permit the higher wages of those within
the union. Business owners and managers do not like to see unem-
ployed capital, but they want high rates of return on their capital
investments even if it should mean bankruptcy for competitors. So
when falling prices appear necessary for a marginal firm to stay
competitive, but when it is not efficient enough to compete in terms
of the new lower prices for its products, the appeal goes out to the
State for “protection,” Protection is needed from nasty customers
who are going to spend their hard-earned cash or credit elsewhere.
Each group resists lower returns on its investment—labor or financial
—even in the face of the biggest risk of all: total unemployment, And
if the State intervenes to protect these vested interests, it is forced to
take steps to insure the continued operation of the firms.

Tt does so through the means of an expansion of the money supply.
It steps in to set up price and wage floors; for example, the work of
the NRA in the early years of the Roosevelt administration. Then
the inflation of the money supply raises aggregate prices (or at least
keeps them from falling), lowers the real income from the fixed
money returns, and therefore “saves” business and labor. This was
the “genius” of the Keynesian recovery, only it took the psychological
inducement of total war to allow the governments to inflate the
currencies sufficiently to reduce real wages sufficiently to keep all
employed, while simultaneously creating an atmosphere favoring the
imposition of price and wage controls in order to “repress” the visible
signs of the inflation, ie., even higher moncy prices. So prices no
longer allocated efficiently; ration stamps, priority slips, and other
“hunting licenses” took the place of an integrated market pricing sys-
tem. So did the black market.

13. Mises, Human Action, chap. 20. For a survey of the literature generated
by Mises’ theory, see Gary North, “Repressed Depression,” The Freeman
(April 1969) [chap. 6, above].

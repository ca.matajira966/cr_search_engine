 

402 An Introduction to Christian Economics

Postwar inflationary pressures have prevented us from falling into
reality, Citizens will not face the possibility that the depression of
the 1930's is being repressed through the expansion of the money
supply, an expansion which is now threatening to become exponen-
tial! No, we seem to prefer the blight of inflation to the necessity
of an orderly, generally predictable downward drift of aggregate
prices,

Most people resist change, in spite of the hopes and footnoted
articles by liberal sociologists who enjoy the security of tenure. Those
people who do welcome change have in mind familiar change, po-
tentially controllable change, change that does not rush in with de-
struction. Stability, law, order: these are the catchwords even in our
own culture, a culture that has thrived on change so extensive that
nothing in the history of man can compare with it, It should not be
surprising that the siren’s slogan of “a stable price level” should have
lured so many into the racks of economic inflexibility and mone-
tary inflation.

Yet a stable price level requires, logically, stable conditions: static
tastes, static technology, static resources, static population. In short,
stable prices demand the end of history. The same people who de-
mand stable prices, whether socialist, interventionist, or monetarist,
simultaneously call for increased economic production. What they
want is the fulfillment of that vision restricted to the drunken of the
Old Testament: “. . . tomorrow shall be as this day, and much
more abundant” (Isa. 56:12). The fantasy is still fantasy: tomorrow
will not be as today, and neither will tomorrow’s price structure.

Fantasy in economic affairs can lead to present euphoria and ulti-
mate miscalculation. Prices change. Tastes change. Productivity
changes. To interfere with those changes is to reduce the efficiency of
the market; only if your goal is to reduce market efficiency would the
imposition of controls be rational. To argue that upward prices, down-
ward prices, or stable prices should be the proper arrangement for
any industry over time is to argue nonsense. An official price can
be imposed for a time, of course, but the result is the misallocation of
scarce resources, a misallocation that is mitigated by the creation of
a black market.

There is one sense in which the concept of stable prices has validity.
Prices on a free market ought to change in a stable, generally pre~
dictable, continuous manner, Price (or quality) changes should be
continual (since economic conditions change) and hopefully con-
tinuous (as distinguished from discontinuous, radical) in nature.

14. North, “Theology,” op. cif.

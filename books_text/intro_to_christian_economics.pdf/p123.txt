Fixed Exchange Rates and Monetary Crises iti

American gold stock rose once agam, and the President finally es-
caped on August 15, 1971—or hoped that he had. He cut the
doliar’s official tie to gold in international payments and left it free
to float on the international markets. Of course, this act was a viola-
tion of International Monetary Fund rules, to which the United States
is a party (or was). As Lenin said, treaties are made to be broken,

For a time, fixed exchange rates seem to restrain policies of
domestic monetary inflation, But for how long? Franz Pick’s
report lists devaluations every year, and there are a lot of them. They
are international violations of contract—violations that call into
question the whole structure of international trade. The honoring of
contracts is the very foundation of free exchange. Apart from this,
economic prediction becomes exceedingly difficult and productivity
suffers. Thus writes Alfred Malabre:

International currency exchanges can transpire in various ways.
One is through a system where Currency A can indefinitely be
exchanged at a fixed rate for Currency B. This is the system that
allegedly prevailed through most of the post-World War II era
and to which most Western leaders now wish to return, Ideally,
it’s a magnificent system, because it promises to eliminate uncer-
tainty from international financial dealings. The widget maker
knows, when he gets an order from abroad, that the money he
will receive will be worth as much to him in the future as at
present,

In practice, however, fixed-rate arrangements provide anything
but certainty, Between 1944, when the present fixed-rate system
was conceived at Bretton Woods, N. H., and mid-August [1971],
when the system finally collapsed, 45 countries changed the inter-
national rates for their currencies. In some instances, changes
were repeated many times, so that in all 74 currency-rate changes
oceurred.®

The problem with such devaluations, as Mises has shown, is that
they create incentives for retaliatory devaluations on the part of other
governments. “At the end of this competition is the complete de-
struction of all the nations’ monetary systems.”® If there were no
fixed exchange rates in the first place, there would be no need for
these governmentally imposed economic discontinuities.

8 Alfred L, Malabre, Jr, “ds It Really Time for Monetary Cheer?” Wall
Street Journal (December 2, 1971). Malabre’s estimate of the number of de-
valuations is far too low. Franz Pick, in the introduction to the second edition
of All the Monies of the World (1971), reports that at least 418 partial or
full devaluations took place in 108 countries between 1954 and the end of
1970, 1973 saw an additional 99 devaluations: Barron's (January 3, 1972),
p. 9. This, in spite of the so-called stabilizing influences of the International
Monetary Fund, the organization drawn up at the Bretton Woods Conference
in July, 1944, officially established on December 27, 1945, and put into opera-
tion on March 1, 1947,

9. Mises, Human Action, p. 791.

Gertrude Coogan and the Myth of Social Credit 125

system the only true “Christian” one. This is why I believe
that it iy mandatory to consider extensively the basic teach-
ings of the movement with respect to monetary affairs, testing
their claims in terms of the biblical prohibition on currency
debasement.

So many of Gertrude Coogan’s ideas seem popular today in
the circles in which I have travelled that the reader owes it
to himself to test his own ideas in terms of this essay. If he
is a secret Social Credit advocate, at least he should be aware
of the fact.]

The Douglas Hiterature is distressingly vague, hut at no point is
it so shifty and uncertain as it is in the attempt to expound the
flaw in the financial system, No single consistent explanation is
given, as one might have a right to expect from the repeated as-
sertion that there is an inherent, mathematically demonstrable,
defect in the financial system whereby it is simply impossible that
the community’s purchasing power could be adequate to take off
the goods society can produce, Indeed I have found nine differ-
ent explanations for the alleged shortage of purchasing power,
and there may be more.

Alvin B, Hansen

Full Recovery or Stagnation (1938), p. 96

Conservatives should be aware of the fact that their organizations
ate constantly subject to subversion by anti-conservative forces.
This is a fact of life. Sadly, many conservatives are not aware of the
fact that it is as easy, perhaps easier, for the opposition to paralyze
conservative action by means of fallacious ideas. Subversion need
not always be personal; it can often be intellectual. The tendency
of conservatives to personalize their enemies makes intellectual
subversion even more likely. All that needs to be done is to import
the alien ideas through ostensibly conservative individuals. Unless
the ideas are recognized for what they are, and not just in terms of
who is advocating them, the take-over will be complete, without
a shot being fired or a subversive elected.

in our day, the economist who has become the symbol of Liberal
ideology is John Maynard Keynes, and rightly so. The influence
of Keynesian ideas has been most profound, especiatly in the uni-
versities.1 This is well known by most informed conservatives.
His system of economics is at odds with the idea of monetary

 

1. “John Maynard Keynes has been the twentieth century's most influential
economist. In fact, it is necessary to go back to Alfred Marshall to find an
economist equally effective with professional colleagues, and to David Ri-
cardo for an illustration of equal impact upon public policy.” Robert Lekach-
man, A History of Economic ideas (New York: Harper & Row, 1959), p. 331.

126 An Introduction to Christian Economics

stability? The ironic thing is that the very policies recommended by
Keynes, the same rationalizations for the increase of State controls
on the economy, are dear to the hearts of muny supposedly anti-
Keynesians. Not having read Keynes thoroughly, not having digested
the ideas of any serious economist, unsuspecting conservatives are
frequently misled into advocating crude, but nonetheless dangerous,
Keynesian-type economic policies. If this should become widespread,
then the whole conservative movement could be easily sidetracked
and turned into its opposite.

For decades, there have been several small, almost underground
publishing houses in America that are remarkably consistent in their
support of inflationary monetary theories, yet they supply most of
the conservative book stores. One of them was the Forum Publishing
Company in Boston, now defunct, but the more. important one is
Omni Publications, Hawthorne, California. It distributes the Forum
books, along with tracts from the Sound Money Press and the Social
Credit movement.

These various studies, which can best be termed “Social Credit
economics,” are a composite of many elements, They resemble the
old Populist agrarian reform movement of the 19th century, with
the same cry for easy money and the same attacks against the
“moneyed interests.” In addition, a bit of Roman Catholic Scholastic
economics is added, most notably Aquinas’ doctrine of the “just
price” and the “just wage.” All of these books are nationalistic in
outlook, put forth in the name of enlightened patriotism. An ex-
ception might be Frederick Soddy’s books, but his disciple, Gertrude

2. Milton Friedman's oft-quoted statement, “We are all Keynesians now,”
apparently paraphrasing the Prince of Wales (later Edward VIL) that “We are
all Socialists now-a-days” (1895), indicates the weakness of the methodological
positivism of the Chicago School. That Friedman could admit that the tools
of analysis (though not the conclusions, except in the area of monetary theory)
of the Chicago School and the Keynesians are the same is a sign of what has
happened to academic economies in this century. In methodology and in their
opposition to a full gold coin standard, the Keynesians and Chicago School stand
arm-in-arm, Well, not quite arm-in-arm, They do agree, however, that Mises and
Hayek are “economists made obsolete by the Keynesian revolution.” The words
are Karl Brunner’s, a Chicago School economist, who resents the fact that
Nicholas Kaldor, a ‘Keynesian, Jinked their names to Friedman’s. See Brunner,
“The Monetarist View of Keynesian Ideas,” Lloyds Bank Review (October,
1971), p, 37, The Keynesians and Chicagoans debate the exceedingly fine
points of their respective positions, ignoring the one man whose monetary
theories are rational—-Mises! Cf. J. H. Wood, “Money and Output: Keynes and
Friedman in Historical Perspective,” “Business Review of the Federal Reserve
Bank of Philadelphia (Sept., 1972). On the failure of both Keynesian and Chi-
cagoan inflationary programs to work, see Murray N. Rothbard, “The Great
Inflationary Recession Issue: ‘Nixonomics’ Explained,” The Individualist, IL
(June, 1970). (This is the American journal, not the English one; both use the
same tide.) For a theoretical refutation of Friedman’s monetary views, see
Hans F. Sennhoiz, “Chicago Monetary Tradition in the Light of ‘Austrian

Theory,” in Toward Liber ty (Menio Park, Calif.: Institute for Humane Studies,
1971), IT, 347-366. This essay appéared also in Reason (October, 1971).

    

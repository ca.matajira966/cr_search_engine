Gertrude Coogan and the Myth of Social Credit 129

has been a standard free enterprise text since it was first published
jn 1912." He is used, not because he is infallible, but because his
jdeas seem most logical and most consistent with the facts of money,
especially in those areas in which Miss Coogan has gone astray. In
Mises’ view, money is a commodity, an economic good, which his-
torically has been used as a medium of exchange. Originally, any
object which presently circulates as a medium of exchange had an-
other use, usually artistic or ornamental or even religious. Occasion-
ally, as in the case of salt, it may even have been a consumption
good. But the main point is that the particular good was once
valued for some service other than its exchangeability. Mises has
said specifically that “no good can be employed for the function of
a medium of exchange which at the very beginning of its use for
this purpose did not have exchange value on account of other em-
ployments,"4 Because certain goods had definite properties—dura-
bility, easy divisibility, portability, and especially scarcity—properties
which other goods lacked to the same degree, they became easier to
exchange than other goods. The more that people realized how easy
it was to exchange these certain goods—usually gold, silver, and
precious gems-—the more these goods became desired purely as
exchange media tather than as ornaments. This type of exchange
media is known as “commodity money.” Its chief mark of dis-
tinction is that it, unlike all other goods, is not valued for its ultimate
usé in consumption, but primarily as an exchange device which many
people trust and similarly value. These goods can also be used for
ornament or industry, as they were originally, but then they are no
longer money.*

Money, then, is not a legal demand claim to all goods, nor a store-
house of labor, but merely a useful commodity that is usually, but
not necessarily always, accepted by others in exchange for consump-
tion or production goads. Money is merely the most marketable good
available, due to the special physical properties it has and to the
historically developed acceptance of it as a medium of exchange.
Jt is really quite simple.

Paper money derives its value from the fact that it originally
represented certain quantities of the money commodities, normally
gold and silver. A paper bill was originally a demand claim, not to

3. Ludwig von Mises, Phe Theory of Money and Credit (Foundation for
Economic Education, [1912] 1971}.

4, Mises, Human Action (3rd ed.; Chicago: Regnery, 1966), p. 410.

5. The value of gold or silver as'a medium of exchange increases its
Tespective value over what it would have been worth for ornamental or in-
dustrial purposes alone. Mises, Maney and Credit, pp. 105-106, especially the
citation from John Law, of which Mises approves, p. 106n.

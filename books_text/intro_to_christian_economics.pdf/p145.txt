Gertrude Coogan and the Myth of Social Credit 133

“the only unit of measure of value is the whole sum of the circulating
money... .” This is equivalent, using her own illustration, to the
statement that the only measure of distance is the sum total of all
yardsticks. The question of value is the most fundamental question
jn economic science, yet Coogan dismisses it with this meaningless
yerbosity. In doing so, she declares to the world that she has no
economic theory, that as far as she is concerned, cconomic theory
is not a matter of importance.

Value stems from the fact that individual men have varying indi-
yidual needs, and they are able to satisfy some of these needs through
the employment of certain means. Boehm-Bawerk writes that the
value of goods, therefore, “is determined by that gain in a subject’s
well-being which is dependent on his power of disposal over these
goods .. . the difference between the degree of well-being attainable
with and the degree attainable without the goods to be valued.”*
Value is subjectively determined by acting, calculating, economizing
man, according to his own personal desires and needs. Because value
js sudjective, ‘Acts of valuation are not susceptible of any kind of
measurement.” We can only say that “subjective valuation, which
is the pivot of all economic activity, only arranges commodities in
order of their significance; it does not measure that significance.”?
The only things that are measurable are prices, the exchange ratios be-
tween commodities, and these exchange ratios are not founded upon
any inherent value of the commodities themselves, but instead
they “are based upon the value-scales of individuals dealing in the
market.”14

This means that the State is not the creator of economic value.
Second, it means that money cannot measure values, because all
values are subjectively determined, psychological entities. Value is
based on the desires of individual men who have individual talents and
individual callings. All we can say is that if an exchange takes place
between two men, the first giving up commodity A to receive com-
modity B, and the other giving up commodity B to obtain commodity
A, the first man desires commodity B more than he does commodity
A, and the reverse is true of the second man. We cannot say how
much one man values a good over another, but only that he values it
enough to make the exchange. Thus, Coogan’s statement that “it
is the total number of coins (denominations) which measures value”
(No. 5) is totally false. It is as impossible to measure the subjective,

8. Eugen von Boehm-Bawerk, The Positive Theory of Capital (4th ed;
South Holland, Ill: Libertarian Press, 1921] 1959}, p. 181.

9. Mises, Money and Credit, p. 39.

10. Ibid,

li, Ibid., p. 40,

 

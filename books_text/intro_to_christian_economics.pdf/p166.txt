154 An Inireduction to Christian Economics

visible, budgeted, congressionally controlled taxation, yes, but not
in fieu of taxation as such. Someone has to pay. When she says
that “the purchasing power created at the original source benefits
all,” she is deceiving the reader.®* It benefits only those who receive
the counterfeit, fiat money first; it hurts those who do not have
immediate access to the newly created money. But State officials
know that few people understand that monetary expansion is really
a form of indirect taxation, and so they can increase State expendi-
tures without having to declare unpopular new tax confiscations,
This is why Hans Sennholz writes in American Opinion (Sept.,
1964) that “Many of the foes of the Federal Reserve System are
rabid collectivists.” They support statist monetary inflation as if it
were a golden goose egg. It offers the State something for nothing.
Somebody has to pay, but since the taxes are hidden, few people
scream. If someone does, then Keynes can come forward and pacify the
Liberals, while Coogan steps out to silence the conservatives, It is a
nice arrangement for the statists, A more unjust tax could hardly be
devised, of course. It is not predictable; people cannot plan very easily
for it, and they do not know which prices will be raised. But the idea of
something for nothing is too tempting to the State; if a little inflation is
good, why not more? The State can buy more and more, if it just keeps
printing money faster than prices are rising, until the monetary
system is destroyed, By permitting the State to print money which
has no gold or silver backing, the people have committed economic
suicide, and Coogan stands in the galleries and shouts her encour-
agement. In fact, the whole Social Credit movement is there, pro-
claiming that gold is old fashioned, a tool of the international
bankers. Those who listen to their dreams will reap the whirlwind.

Coogan actually applauds the idea of something for nothing.
In fact, she says it must always be this way: “This is what has been
done and what has to be done in order to have money, a man made
instrumentality.”** Fiat money has to exist; “all money is fiat mon-
ey.”*6 But this is just not true. Money is not created by fiat, but by
the voluntary use of certain goods by people involved in commerce.
Mining costs, in the long run, are no higher or lower than in any
other business, and the profits are no greater, But States can print
bills cheaply, bills which are never used for ornament and industrial
use, a8 money metals sometimes can be. Coogan’s proposals would
destroy the best safeguard we have against the ever-hungry State:
the use of money metals in commeree.87

84, Ibid., p. 262. 86, Ibid., p. 283.
85. Ibid., p. 300. 87. North, “Gold’s Dust,” op, cit.

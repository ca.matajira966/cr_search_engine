174 An Introduction to Christian Economics

benefit. Knowledge does not come easily; it will not be sold cheaply,

This has led me to an odd conclusion (for a college instructor).
The young man who has skills mechanically would be wise to stay out
of college, He would be far wiser to get into a trade school, especially
if he should have a skili that would not require highly specialized ma-
chinery. With some 8,000,000 young men and women on the college
campuses today, there will be a glut of people holding college degrees.
In fact, the glut is already quite visible. The automatic job for the
man with the bachelor’s degree is not automatic any longer, at least
not at the older higher wage. The skilled craftsman is about to have
bis day. The man who can produce a thing of beauty or of use through
his own genius, with simple tools and common materials, should find
the coming decade exceptionally profitable. If he must sacrifice the
false prestige of a college diploma in order to get such skills, the sacri-
fice ought to pay off in the future—perhaps the very near future.

The 1970’s and the 1980’s may well destroy the whole economic

structure that is based on the “organization man”-—the drone who
substitutes activity for production, a glib tongue for knowledge. The
days of the instant success through college are numbered; there are
simply too many people in college for any monopolistic reward to be
maintained by holders of the college degree. Men with skills and
knowledge will continue to be paid well, but the skills and knowledge
required for economic survival may not be those imparted by formal
college instruction. And the riot conditions are not helping the situa-
tion, either. Prestige will come once more to the man who can build
with his own hands, the creative person, the man who possesses
operational knowledge of how simple things work. For that kind of
man, the bonanza is about to begin.
[1973 NOTE: the reader may detect a contradiction between this
essay and the previous one. In 1969 I wrote that trade unions, having
political power, would escape the controls. So far, this has generally
been the case: some unions have had raises as high as 33 percent (the
supposed maximum under controls is 5 percent per annum). How-
ever, my 1971 statement may also prove true: after 1975, an anti-
ustion backlash should appear, and controls will be applied vigorously.
Trade unionism is in trouble.)

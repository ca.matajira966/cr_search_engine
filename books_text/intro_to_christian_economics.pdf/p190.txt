178 An Introduction to Christian Economics

transaction, Naturally the actual exchanges are in the millions of
ounces, but the principle is the same. The Treasury officials in the
bimetallic country soon find that their stores of silver are being rapidly
depleted and that the Treasury is overstocked with the overvalued
gold. Government price-fixing will do this every time: there is a glut of
the overvalued good and shortage of the undervalued good. This
process puts pressure on the bimetallic nation to revise its fixed ratio of
16 to one back toward the true market price of 15 to one; if this is
not done, the Treasury will eventually run out of silver, and its guar-
antee to redeem gold with silver will no longer be meaningful.

The late Knut Wicksell, the Swedish economist, has summarized
the whole question quite well:

Tt can be readily seen that Gresham’s Law, which in the opinion
of many economists would make a fixed value ratio impossible
between two metals both used as standards, is chiefly important
as between different countries. If there is active commercial
intercourse across the frontiers of two neighboring countries and
if different ratios are established in them between full weight gold
and silver coins, it is inevitable that each of the metals will sooner
or later find its way to the country in which its value is relatively
higher,

Bimetallism has never existed in any country. What has been
the case is something more accurately described as a trimonetary
standard: gold, silver, and paper government or bank notes that
may or may not be fully redeemable in gold. or silver. With the
advent of paper money, the whole picture has been. made far more
complex, Credit devices and checking accounts have created a
multiple monetary structure so confusing that only high speed com-
puters seem capable of sorting things out, and even they occasionally
break down in the face of the data. The principles remain the same,
however; they will be the same even if the sea of paper is replaced
by an avalanche of blips on computers.

Since 1934, American citizens have been prohibited from using gold
coins as a means of exchange on the open market, The status of this
executive order has been questionable legally, but it is a fact that
gold no longer circulates as an official means of domestic exchange.
This at least removes from. the domestic scene the problem of
fluctuating exchange ratio between gold and silver, But what we
face now is more insidious than bimetallism, Our problem is that the
federal government has asserted its self-proclaimed right to exercise
monopolistic control over the nation’s money mechanism. It dele-
gates a part of this right to the quasi-national Federal Reserve
System, but this is still within the framework of a legal monopoly.

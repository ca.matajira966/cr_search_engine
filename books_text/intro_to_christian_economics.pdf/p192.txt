180 An Introduction to Christian Economics

equal to the face value of the coin, one dollar. When the price of
silver climbed over $1.29 in 1964, the silver dollars went out of
circulation. This was the result of Gresham’s Law: why should a
person spend a coin worth over a dollar in silver in order to purchase
some item worth a doliar? He spent the paper dollar instead, and he
hoarded (saved) the coin. The artificially overvalued paper drove
out the artificially undervalued silver dollar.

It may be objected that most people were not aware of the fact
that the silver dollar’s silver content was worth more than a dollar.
True enough, but a few did know: the coin collectors. They began
buying up the coins from the banks. Even smaller denomination coins
that would not become full-bodied until silver climbed to $1.38 per
ounce began to disappear, as collectors anticipated a further rise in
prices. A quick glance at the Readers’ Guide to Periodical Literature
under “coins” discloses the 1964 coin shortage crisis. The rush
was On.

The depletion of our silver coinage has progressed steadily since
1964. The sandwich coins have replaced the silver coins. First
the silver dollars went, followed by the half-dollars. Then the
quarters disappeared, and by now few silver dimes are seen. When
the Treasury ceased supporting the price of silver in the summer
of 1967, the price soared from $1.38 to as high as $2.40 (the
price has hovered around $1.90 in recent moriths, but this could
change at any time). The Treasury’s storehouse of pure silver is
now part of history; their silver stock is gone.

The silver coin shortage is the domestic side of the international
gold crisis. The policies of State inflation underlie both phenomena.
In 1934, the government called in the gold coins before the policies
of inflation began. (Actually, only a fraction of the coins were turned
in: $9 billion of the $14 billion worth of coins stayed outside the
Treasury's vaults, This, at least, is the unofficial estimate of leading
men in the coin collecting world.) Though the public cannot use gold
coins in exchange or own gold bullion, foreign governments can, The
United States is still under the rule of gold, at least to some degree.*
This is one of the few remaining restraints ta domestic mass inflation:
the government of this country does not want to be drained of its gold
reserves by foreign governments and central banks, so some restraint
is exercised in producing more unbacked, fiat money. Nevertheless,
more and more central banks are finding it preferable to cash in their
American stocks and bonds, exchanging their cash for gold at $35 per
ounce, The establishment in March of 1968 of a “two-tier” gold

*This is no longer true, given August 15, 1971.

The Ethics of Monetary Hoarding 207

in the latter days of the French Revolution, the hostility was nothing
short of savage. As Andrew Dickson White reports in his Fiut
Money Inflation in France, any shopkeeper who was found charging
two prices, one in terms of gold and the other in terms of the inflated
assignats, could be sentenced to six months in prison. If he were
caught a second time, the penalty was immediate execution.

The attack on those who hoard specie metals is based on a phi-
losophy which sets the State’s authority above all other authorities.
State inflation is by definition supremely wise, supremely moral. Any
deviation from the State’s plan is, also by definition, immoral. The
worst sort of deviation is hoarding; therefore, the hoarder of money
metals is supremely immoral. This kind of hoarding seems immoral,
in short, only to those who attribute absolute infallibility to the State
planners and absolute morality to their inflationary policies,

Conclusion

The Bible, in contrast to the statist philosophy, teaches that the
central religious and social unit is the family. The church is im-
portant, and so is the civil authority, but the family is primary. For
this reason, the Apostle Paul required that any church leader had
first to be a competent guide of his family. Candidates for bishop or
deacon had to demonstrate that they ruled their families and pro-
vided for them adequately, leading them in obedience to God’s family
laws (I Tim. 3). Any man who claimed to be moral, or who did
good deeds, without caring for his family, was considered worse than
an infidel (I Tim. 5:8),

My point should be clear: the hoarding of money metals in times
of heavy inflation is far from being immoral. Assuming the accuracy
of the analysis of the present world economy which is outlined in
this book, it becomes imperative for men to provide for the future.
Such preparation involves, at Jeast in part, the hoarding of money
metals (silver and gold). Those who acknowledge the biblical re-
quirements over their lives must therefore deny the State’s claims
to virtual divinity in monetary affairs. They must be willing to
hoard gold and silver as a religious duty. If they refuse, then they
and their families face an already dismal future without any possi-
bility of defense against total economic, social, and ultimately re-
ligious servitude.

 

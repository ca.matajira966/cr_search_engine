Family, State, and Market 245

tions, gangs, or any number of other voluntary associations. With-
out the emotional stability pravided by these associations—which
Tocqueville said were so fundamental in American life in the 1830’s
——men are left to find meaning and purpose as social atoms. The
“anomie” of modern industrial life, as Durkheim called it at the
end of the last century, results from just this kind of social fragmenta-
tion and atomization. The alienation of mankind which so. appalled
the young Marx—an alienation, within the framework of the Chris-
tian philosophy of the West, which stems from the ultimate alienation
between God and man—flourished far more easily in the milieu of
industrial Europe than it had in the more personalistic culture which
had preceded it.

It has been a hallmark of totalitarian parties that ultimate sov-
ereignty has been ascribed to the leader, for it is he who is the in-
carnation of the spirit of universal meaning (Volkgeist, the prole-
tarian class, the forces of history, etc.). The leader is the sole
source of temporal meaning, the fountain of power, the source of
legitimate change, the touchstone of community. Men participate
in community, thus bringing purpose into otherwise autonomous,
contingent lives, through the leader and the party. Totalitarian sys-
tems deny the validity of alternative institutional sovereignties, for
these operate as buffers against central political power. At best,
such competing institutions are regarded as derivative sovereignties,
drawing legitimacy, power, and meaning from the party and the
party’s State. ‘Thus, the premise of absolute totalitarianism is the
simultaneous existence of radical individualism (i.c., social atomism)
and the total integration of each human personality into the over-
arching sovereignty of the leader and his party.’ From Rousseau to
Stalin, a man is defined only as citizen or comrade; no other member-
ship has any legitimacy. As Koestler has put it, in Darkness at Noon,
a man is defined as one million men divided by one million—a pure
social atom.

Christians rest the case for human freedom on the existence of
legitimate multiple sovereignties, each with the authority to express
itself by means of establishing institutional restraints on members
and on each other. Men can be a part of several of them at any
point in time, and each wil! impart a degree of meaning and stability
into his life. No one human institution can legitimately claim full
sovereignty, for men are sinful, rebellious creatures, and not creator

i, Cf. Hannah Arendt, The Origins of Totalitarianism (New York: Har-
court, Brace, & World, [1951] 1966); Robert A. Nisbet, The Quest for Com-
munity (New York: Oxford, [1953] 1970); J. L. Talmon, The Origins of To-
talitarian Democracy (New York: Praeger, 1961).

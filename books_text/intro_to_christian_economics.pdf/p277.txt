The Crisis in Soviet Economic Planning 265

concerned with the costs of production, decides on the plan for
gross output. A third department or subdepartment, proceeding
from. the principle that costs must also decline and labor produc-
tivity increase, plans costs, wages fund and labor on the basis of
past performance. Material allocations and components are
planned by numerous other departments. Not a single depart-
mevt of Gosplan is responsible for the consistency of these
plans.’6

In short, too many blind cooks are spoiling the soup. All of these
problems are compounded by the constant meddling of Communist
Party officials at ali levels of the economy. This interference naturally
jeads to irrationality in planning, “The problem is not, of course,
new; it is inhérent in the separate existence of party and state hier-
archies.”"*® The Soviet planning system, in the words of Wiles and
Smolinski, is “a crazy quilt of agencies organized according to sev-
eral principles.”*" It should not be surprising that the economic
puzzle in the aggregate never seems to fit together in the particulars.

The preceding discussion has been based on the presupposition
that the choices of the planners, if only they could be coordinated,
would be rational. That assumption in itself is highly suspect. Social-
ists would have us believe so, of course. Peter Wiles has asked
whether or not we should believe them. “The possibility of the
private consumer being irrational is of course an accepted cliché
of Western economies. But none of this makes planners’ preferences
rational. It is astonishing that people with an intimate knowledge of
how the Soviet system works should consider the possibility of op-
erating on the assumption that planners’ preferences are in fact ra-
tional in a Communist country.”2! Those who would construct such
a system of production with as many built-in irrationalities as the
Soviet system contains can certainly be questioned with regard to their
overall rationality.

The almost incredible bureaucratization of Soviet planning is evi-
denced by twe frequently encountered cxamples. In one case, a plan
for the production of ball bearings had to go through so many agen-
cies for approval that a staggering (literally) total of 430 pounds of
documents was generated.*2 In another instance, one “autonomous”

1988), Aleg dNove, The Soviet Economy: An Introduction (New York: Praeger,

19 Rice Nove, “Revamping the Economy,” Problems of Communism, XI
(Jan.-Feb., 1963), p. 15.

20. Wiles and ‘Smnolinski,

21. Peter Wiles, “Rationality, the Market, Decentralization, and the Terri-
torial Principle,” in Grossman (ed.), Value and Pian, pp. 186-187. Cf.
Gerschenkron, Economic Backwardness, pp. 287-288.

22, Abram Bergson, The Economics of Soviet Planning (New Haven,
Conn: Yale University Press, 1964), p. 150.

 

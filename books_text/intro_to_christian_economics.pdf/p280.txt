268 An Introduction to Christian Economics

create a science of socialist economics. It is indicative of the political
attitude toward economics in the Soviet Union that from 1928 until
1954~—-the years of Stalin’s planning and the years of the country’s
most rapid economic growth—it was found necessary to cease teach-
ing all classes in economics in the institutions of higher cducation, and
not one general textbook in political economy appeared!*¢

How they propose to solve these problems remains to be seen,
Tt seems clear that without a decentralization based upon the rational
coordination of a flexible price mechanism, and without the advent
of @ consumer economy based upon the private ownership of the
means of production, the basic issues will remain unsolved. The so-
called Liberman reforms have not yet fundamentally altcred the
structure of the Soviet economy, and the limited decentralization and
production-for-profit techniques of those reforms have been restricted
to Jess than 150 industries, carrying small weight in the aggregate
economy.*? Jf these reforms should become basic to the Soviet sys-
tem, then the entire structure of ownership and control of the firm
will have to be revamped in order to permit entrepreneurs to gain
access to their share of the total value of output.*? It is unlikely
that such a restructuring will take place; it would be impossible
within the framework of a traditional socialist ideology. Therefore,
we can expect the Soviet economy to shift back and forth between
centralized planning and local autarky ‘mediated primarily by a
black market supply system, and growing more and more irrational
as the complexity of the plaoning task grows ever greater. The
system, in good Marxian terminology, contains the seeds of its own
destruction.

30. Nove, The Soviet Economy, p. 282.

31. Marshall I. Goldman, “Economic Controversy in the Soviet Union,”
Foreign Affairs, XLI (1963), in Bornstein and Fusfeld, op. cit., pp. 339-351.
Cf. Ludwig von Mises, “Observations on the Russian Reform Movement,” The
Freeman (May, 1966).

32. Svetozar Pejovich, “Liberman’s Reforms and Property Rights in the
Soviet Union,” The Journal of Law and Economics, XIV (April, 1969),
pp. 155-162.

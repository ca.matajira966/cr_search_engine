274 An Introduction to Christian Economics

at a loss to see how a wartime bomb project or a trip to the moon
indicate anything except the amazing capacity for spending that
governments possess.

Barbara Ward, one of the most respected Establishment thinkers
in Britain, and former editor of The Economist, has taken Buck-
minster Fuller’s spaceship analogy and has tumed it into an effective
neo-Fabian propaganda device: “The most rational way of considering
the whole race today is to see it as the ship’s crew of a single spaceship
on which all of us, with a remarkable combination of security and vul-
nerability, are making our pilgrimage through infinity.”* This assumes,
of course, a chain of command, a previously agreed upon destination,
and some shared faith in the way one goes about getting there. But what
are a few assumptions among rational men, especially planners? Now,
fellow crewmen, “Think what could happen if somebody were to get
mad or drunk in a submarine and run for the controls. If some mem-
ber of the human race gets dead drunk on our spaceship, we are all
in trouble. This is how we have to think of ourselves. We are a
ship’s company on a small ship. Rational behavior is the condition
of survival.” Clearly, as she points out, “Rational rules of behavior
are what we largely lack.”® All is not lost, however. Our divisions
are based on divisions of power, wealth, and ideology, but these can
be overcome through reason. There is a universal means of instant
communication—tcechnology—which brings us together.\° “Quite
apart from common tools and methods, we also have mental attitudes
that do not vary from culture to culture and are common to a single
world civilization.”*2_ What these common bonds are, she fails to
mention; nevertheless, “in short, we have become a single human
community.’”"22

The problem with all of this “spaceship reasoning” is that it as-
sumes as solved those fundamental problems that need solving in
order to make possible the spaceship analogy. The thing which
strikes me as ironic. is that the language of the spaceship involves a
chain of command. approach to the solution of human problems.
These humanitarian intellectuals who decry the petty military dic-
tatorships in underdeveloped nations want to impose a massive system
of command over the whole earth. That is what the call to world
government implies.? The spaceship analogy necessarily views so-
ciety as a vast army. Yet for some reason, Hayek’s identical con-

8. Barbara Ward, Spaceship Earth (New York: Columbia University Press,

1966), p. 15.
9. fhid. 12. ibid., p. 14.
10. Ibid., p. 4. 43, Ibid., p. 17.
WW. Ibid,

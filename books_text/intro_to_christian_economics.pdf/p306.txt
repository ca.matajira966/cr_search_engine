294 An Introduction to Christian Economics

State-supported higher education was pioneered in the nineteenth
century. It makes very little sense today. As Robert Nisbet has ar~
gued in his iconoclastic and reasonable essay on our Permanent
Professors, no one is fired for mental or moral incompetence any
longer, the two chief ways of dismissing tenured men. The excep-
tional mobility of modern teachers removes any serious threat to aca-
demic freedom, since institutions are varied enough to let men find
a platform to teach almost anything. The very guild structure pro-
motes a basic uniformity of methodology today, insuring general
agreement within most academic departments—or so we found until
the mid-1960’s, Finally, Nisbet argues, if academic freedom is
teally the issue, why limit it? Why not let junior members have it?
“On what logical grounds, then, do we claim exemption for age and
rank, in certain respects the most feudal of all feudal qualities?!

Tenure, far from protecting men in their expression of controversial
opinions, has enabled men to express no opinions at all. Teaching
has become lethargic as men pursue their academic careers in the
academic journals (100,000 in the world today**) and their annual
meetings. Tenure protects the man without the flair for teaching,
the man who has no controversial opinions to distinguish his lectures,
the man whose very blandness insures his protection from “academic
witch-hunters,” but who has never learned to compete in the world
of student education, Tenure has turned the university over to the
drone, the pedant, the writer of overfootnoted, mindless articles. It
might even be truc to say that the spirited junior teacher with con-
troversial opinions has more to fear from his tenured, spineless, drab
colleagues than from the outside public. And drones, it should be
noted, are not known for their flexibility. Wage scales reflect this,
especially when conditions dictate a downward revision. Institu-
tional inflexibility rewards the inflexible. Nonmarket financing keeps
the structures inflexible.

The Subsidized Product

The discussion above focused on the implications of the demand
side of the equation. We must now turn to the supply side of the
Ph.D. equation. Why are there so many of them being produced?

Many reasons exist. A primary factor was the existence, until
1968, of the graduate school military draft deferment. This func-
tioned as an indirect subsidy to graduate departments. “Canada”

12. Robert A. Nisbet, “The Permanent Professors: A ‘Modest Proposal”
(1965), in Nisbet, Tradition and Revoit (New York: Random House, 1968),
p. 241. Cf. U.S. "News and World Report (Dec. 11, 1972): breakthrough!

13. M. King Hubbert, “Are We Retrogressing in Science?” Geological So-
ciety of America ‘Bulletin, LXXIV (1963), p. 366.

296 An Introduction to Christian Economics

little need for highly specialized humanities students. Thus, depart-
ments get bloated with graduate students, and while the percentage
of those who are awarded the degree may stay low, the absolute
number of awards increases. Jobs open up in other universities which
are also expanding their graduate programs, thus creating demand
for more Ph.D.’s. The entire structure is geared to the growth of
graduate enrollments.

Colleges gain greater prestige by becoming full universities. If
they do, they can gain access to federal research funds, and these have
expanded exponentially since 1950 (the cut-off came in 1968).
Fellowships and teaching assistantships were multiplied, while loan
programs at low interest were made available to those students who
did not become part-time employees of departments. These loans,
especially under the National Defense (the magic budgetary word
in the mid-1950’s) Education Act, could be canceled after five years
of teaching by the recipient.

Graduate students in the humanities do not generally understand
economics. They are not so aware of the employment situation, and,
as Breneman shows, departments are often rewarded by keeping their
students in the dark on this issue, thus encouraging them to stay in
the program, Students without the Ph.D. have few college teaching
employment opportunities, so the opportunity casts of staying in the
program arc lower than, say, an engineer who can take his M.A. and
get a good job in industry (again, before 1968). So the main con-
cern for the student in a state university is the size of his state-sup-
ported subsidy; the number of campus jobs, the size of tuition costs,
the availability of loans.

Graduate education is costly. Obviously, in terms of faculty mem-
bers employed, the Ph.D. student is around three times as costly,
especially if he does not assume any teaching load as an assistant.
There is simply no way of estimating the cost per student per year,
or so I am told by the university budget department. French students
cost less than physics students in applicd physics, and possibly more
than those in theoretical physics or mathematics. But it is possible
to estimate in a crude fashion that it costs, at an average, $3,500 per
student in the University of California; graduate students are more
costly, though by how much it is difficult to say, But tuition, until
1970, covered at best less than 10 percent of this, or $300. For the.
graduate student, the subsidy would be even greater, possibly double.

Subsidize the production of a scarce economic goad, and there will
be an oversupply of that good in terms of true market demand. That
law is as applicable in the Ph.D. market as in that for surplus wheat
or army fatigues. This is the fundamental cause of the oversupply

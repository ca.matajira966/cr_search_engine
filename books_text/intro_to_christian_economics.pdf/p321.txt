Property Taxes: Sovereignty and Assessed Valuation 309

bank at Baltimore of the Bank of the United States, refused to pay.
Marshall and the Supreme Court affirmed the validity of M’Culloch’s
decision: the bank, as an instrument of the federal government,
chartered by Congress, was immune from levies made upon it by
any other government. “If the States may tax one instrument, em-
ployed by the government in the execution of its powers, they may
tax any and every other instrument.” Thus, the sovereignty of one
government may not be challenged by any other. No one govern-
ment has complete sovereignty over all spheres of life. Here is
a basic principle of American political history; Marshall put it
concisely:
In America, the powers of sovereignty are divided between the
government of the Union and those of the States. They are each
sovereign, with respect to the objects committed to it, and neither
sovereign with respect to the objects committed to the other.t
Several important points can be inferred from Marshall’s decision.
First, the power to tax involves the power to destroy, Taxation
involves coercion, and the right of coercion necessarily involves the
power to destroy. Second, the right to lay coercive taxes upon any in-
dividual or institution is an assertion of sovereignty. For example,
the right of eminent domain is an assertion of sovereignty just as
the right of taxation is. An important legal study, Ruling Case Law,
published in this country before our entrance into the First World
War, makes this clear,

Accordingly it is now generally considered that the power of
eminent domain is not a property right or an exercise by the state
of an ultimate ownership in the soil, but that it is based on the
sovereignty of the state. As that sovereignty includes the right
to enact and enforce as law anything not physically impossible
and not forbidden by some clause in the constitution, and the
taking of property within the jurisdiction of the state for public
use on payment of compensation is neither impossible nor pro-
hibited by the constitution, a statute authorizing the exercise of
eminent domain needs no further justification.”

Sovereignty, coercion, taxation: to challenge one is to challenge
the others. It behooves us to think through the implications of this
ptoposition. The tithe, for example, was mandatory in ancient Is-
rael; the priests, who administered the funds for the benefit of private
agencies of culture and charity, had legitimate sovereignty.

1. Extensive excerpts from this decision are found in Henry Steele Com-
mager (ed.), Documents of American History (6th ed.; New York: Appleton-
Century-Crofts, 1958), pp. 213-220.

2. William M. McKinney and Burdett A. Rich (eds.), Ruling Case Law
(Rochester, N. ¥.: Lawyers Co-operative Publishing Co., 1915), vol. 10, p. 13.

 

Property Taxes: Sovereignty and Assessed Valuation 321

represents sovereignty, Counties may not tax land belonging to other
branches of civil government. This justification of tax exemption is
based on the sovereignty of the other branches of civil government, If
true separation of churches and the State is to be maintained, then
the sovereignty issue cannot be avoided. Churches, like hospitals, tax-
exempt foundations, and other agencies of government (not civil
government, but social government nonetheless), should be able to
claim the right of sovereignty. And if this right is not valid, then all
property, without distinctions as to ownership, should be subject to
the property tax. If the agency of the worship of God is not entitled
to an exemption, then other equally religious agencies are not so
entitled: public schools, federal defense areas, state welfare agencies,
and so forth, If all exemptions were eliminated, then the churches
could not complain. It would also force local, state, and federal
agencies to make rational economic decisions as to the location of
their various bureaus, since the market’s assessment of land and
structural values would be in operation. It’ would also permit private
individuals or corporations to bid on government lands, thus forcing
up the particular agency’s tax rate, just as the government could do
(and would continue to do) to land owned by private agencies and
citizens. If no agency is sovercign, then the market-oriented property
tax system of each county would operate in both directions: land and
buildings could flow back toward the private sector of the economy.
Civil governments at all levels would have to take this fact into con-
sideration when selecting sites for their exercise of eminent domain.
The agencies of each branch of civil government would be forced to
take seriously the economics of location, We might begin to see a
more efficient allocation of scaree economic resources as a direct
result.**

If one branch of civil government can legitimately claim exemp-
tion from the taxation power of another branch, then this same ex-

13. The federal government owns about one-third of all the land in the
United. States, mostly in the western states. See the Statistical Abstract of the
United States, 1971, sect. 7. In California, for example, the federal govern-
ment ownes 45 percent of the state: /bid., p. 189. The state and other agencies
of government own an additional 25 percent, for a total of 70 percent gov-
ernment ownership: “Tax Exempt Foundations and the Property Tax,” a sheet
distributed by United Organizations of Taxpayers, Inc., 6431 West Sih St.
Los Angeles. An additional 10 percent is owned by tax-exempt organizations,
whose numbers total 80,000 in the state. The New York Times (May 9, 1972)
reports that at the present time, there are half a million such organizations in
the United States. Between the State and tax-exempt organizations, as Alfred
Balk, the editor of The Free List: Property Without Taxes (New York: Russell
Sage Foundation, 1971), points out, some $600 billion, or one-third of the
U.S. total of all property, is tax exempt. The areas most affected are the core
cities of metropolitan areas. CE. Robert Cassidy, “The Trouble With Property
Taxes,” The New Republic (May 15, 1971).

 

ee ee eee

SHS tena eREe rere acer

 

earmewrees

 

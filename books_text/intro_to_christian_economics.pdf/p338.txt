Chapter XXVIII
OWNERSHIP: FREE BUT NOT CHEAP

[One of the recommendations which is offered concerning a
means of legitimate Christian social action is the establish-
ment of Christian labor unions, This is almost a theological
dogma among Dutch immigrants, who look back fondly to
the mid-nineteenth-century political activities of Groen van
Prinsterer and Abraham Kuyper at the end of the century.
Tf this is to be strictly a voluntary association, it would be
harmless enough, and possibly even helpful in reducing the
impersonality of factory production. But to give this advice
in a nation whieh has compulsory labor union legislation like
the Wagner Act of 1935 is nothing less than suicidal. Such
@ recommendation stems from either ignorance or a radical
economic philosophy that is at odds with the biblical view of
personal responsibility and godly stewardship. Hopefully, the
following essay will explain the fallacies of arguments in favor
of compulsory trade unions, whether “Christian” or more
consistently pagan.)

Ownership of the means of production is not a privilege, but a
social liability. Capitalists and Jandowners are compelled to em-
ploy their property for the hest possible satisfaction of the con-
sumers. If they arc slow and inept in the performance of their
duties, they are penalized by losses. If they do not learn the les-
son and do not reform their conduct of affairs, they lose their
wealth, No investment is safe forever. He who docs not use his
property in serving the consumers in the most efficient way is
doomed to failure. There is na room left for people who would
like to enjoy their fortunes in idleness and thoughtlessness.
Ludwig von Mises?

There is no more fundamental question in the field of political
economy than that of the ownership of property. Marx, no less
than Adam Smith, saw this clearly. Invariably, the question of
ownership must raise the question of sovereignty. It also raises the

question of stewardship.

The roots of Western civilization extend back to the Hebrews.

1. Ludwig von Mises, Human Action (3rd ed.; Chicago: Regnery, 1966),

pp. 311-312.
326

328 An Introduction to Christian Economics

The tendency of Protestant thinkers to allow secular economists to
take the lead in handling such questions has been pronounced since
the seventeenth century. Even the New England Puritans, whose
commitment to the idea of a revelational Old Testament holy
commonwealth was striking in 1650, saw fit to abandon specific
answers to concrete questions of political economy after 1676.5
As a result, Protestant nations have tended to drift along economic
lines laid out by one or another school of secular economics,

The advent of rationalist and outright anti-Christian philosophies
shifted the language of the sovereignty issue, but not the difficulties.
If God were removed from the day-to-day operation of the universe,
then sovereignty would have to be found elsewhere. Who or what
economic institution, in principle, possesses ultimate economic sov-
ereignty? Ts it the nation-state, the local political community, the
individual citizen? To whom or what are individual men responsible
for their economic decisions? Is a man responsible only to himself,
or to a community as expressed in some political unit, or to a
community as represented in a theoretically impersonal, unhampered
market? Highteenth-century rationalists—from Adam Smith to Jean
Jacques Rousseau, trom the Physiocrats to the Jacobins—attempted
to discover where sovereignty lies, in principle, in human affairs, and
their answers concerning the abstract locus of sovereignty determined
the kind of society they hoped to attain through the use of political
action. Obviously, they arrived at very different answers,

Ownership involves responsibility. It therefore involves steward-
ship, Stewardship is an inescapable issue. If God is the owner of
the universe, then we are stewards of God’s property. If the State
is the ultimate owner, then we are stewards of the State. If the
community is the ultimate owner, then we are stewards solely of
the community. In practice, we find ourselves stewards for more than
one, if: (1) the ultimate sovereign delegates limited sovereignty to the
others; or (2) if there is competition for full sovereignty operationally.

It is possible, of course, to imagine full sovereignty apart from

4. Cf. William Letwin, The Origins of Scientific Economics (New York:
Doubleday Anchor, 1965); R. H. Tawney, Religion and the Rise of Capitalism
(New York: Mentor, [1926] 1954}, chaps. 4, 5; Richard Schlatter, The Sacial
Ideas of Religious Leaders, 1660-2688 (London: Oxford University Press,
1940), pp. 221-226.

5. On the collapse of the operational specifics of the holy commonwealth
idea in New England, sce my study, Puritan Economic Thought: Winthrop to
Franklin (forthcoming, 1973), Cf. Richard Morris, Government and Labor in
Early America (New York: Columbia University Press, 1946}, pp. 56 ff, 77.

6. Cf. Robert A. Nisbet, Social Order and History (New York: Oxford
University Press, 1969), chap. 4; Louis I, Bredvold, The Brave New World of
the Enlightenment (Ann Arbor: University of Michigan Press, 1961).

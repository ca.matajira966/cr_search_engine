Social Antinomianism 355

from the powerful grip of the sins of injustice and lovelessness”
{p. 32).

Apparently, there are standards of “injustice and lovelessness.”
What are they, the Christian must ask, and where do we find them?
So far, all that we know is that the Bible cannot provide them, at
least not in the socio-economic realm. Troost reaches an impasse
at this point. He has proclaimed a vague pietism in the name of
Reformed scholarship. Unless he can find concrete standards of
judgment that are somehow self-evident and eternally valid apart
from the Bible, he leaves us without any basis for decision-making.

In spite of the fact that he has eliminated the Bible from the realm
of social affairs, he now refers back to the book of Acts: “These
first Christians did not abolish property, nor yet the means of pro-
duction (¢.g., landed estates), No, they put ownership and property
tights back into the place where they belong, back into their proper
function, ‘Not a man of them claimed any of his possessions as his
own, but everything was held in common’ (Acts 4:32) ...” (p. 33).
Two preliminary observations should be made with regard to the
interpretation of this passage. First, the decision to enter into such
common ownership was yoluntary, and that anyone was permitted
to hold his private property out of the common stock (Acts 5:4).
Peter, in other words, proclaimed the right of private ownership as
a perfectly legitimate Christian practice. Second, it is also relevant
that the Christians in Jerusalem were expecting the fulfilment of the
prophecy of the destruction of Jerusalem (Luke 21:20 ff.), and any
application of the early church’s practice of common ownership
should be interpreted in this light, In times of social catastrophe
(and in times of the confiscation of property by the State), it may be
a wise decision for Christians to hold some common property, es-
pecially property which is mobile and easily hidden. But is it a
general law?

The real issue, however, goes much deeper than either of these
two criticisms. Troost argues from this passage in the following man-
ner: “Thus. did the practice of this church confirm the preaching of
the gospel with signs and powers. Property relations were set free
from their neutral self-willed self-assertion and employed for loving
service of God and neighbor” (p. 33). Now what are we to conclude
from all of this? The Bible, Troost has argued, does not give us any
“data, points of departure or premises from which to draw logical
conclusions relevant to modern society’s socio-economic problems,
including property relations.” Nevertheless, we are now told that
the early Christians “(put ownership and property rights back in the
place where they belong,” and Troost obviously expects us to take

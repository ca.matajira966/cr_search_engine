Financing the Kingdom of God 379

money supply exceeded 90 percent. Prices are rising at an annual
sate of over six percent; this rate will climb much higher in the 1970's
and 1980’s. Only price and wage controls will call a halt to this
visible increase in prices, and the controls will destroy many segments of
our free market economy.*' Infiation has become a way of life for
Americans, both politically and economically.

Usury is a crime against God. Today Christians have become usu-
rers in response to the appeals of their leaders, They are no doubt
sinning in ignorance, The fact remains, however, that they are involved.
in rebellion against God’s law-order, and there is judgment coming.
The miracle of God's universe is its marvelous regularity; its lawfulness
is beyond human comprehension.” Those who have purchased such
usurious contracts have made an economically irrevocable decision.
Their hopes are being eaten away by inflation. Their real income is
steadily dropping, as value of money tumbles, The usurers are being
destroyed by inflators. God will not be mocked.

The American Institute for Economic Research, a respected invest-
ment service, is noted for its conservative attitude towards highly
speculative investments, The Institute has published a study of the
various forms of annuities, evaluating cach separately, The retirement
annuities, whereby a man sets aside a large sum of money, transfers
it to the corporation in question, and waits for, say, 25 years for it
to mature, at which time he receives a fixed payment for life, is evaluated
as follows:

From an investment point of view, the interest earned on the annual
premiums is not especially favorable, because the guaranteed re-
turn over a long period of years is less than that paid by most sav-
ings banks, . . . if there is any substantial improvement in the
average length of life in the future, the option on an annuity may be
valuable. On the other hand, probable inflation and the threat of
another devaluation of the dollar indicate that deferred contracts
of this nature may not be favorable,”*

Recent reports made available to the news media by the federal
government have announced that a marked drop in the average life
expectancy for males over five years old has appeared. The pressures
of industrial life, coupled (one suspects) with the physically degenera-

21. Gary North, “Price-Wage Controls: Effects and Counter Effects," The Com-
mercial and Financial Chronicle, Aug. 21, 1969; North, “Inflation and the Return of
the Craftsman,” The Whole Earth Catalog, Jan., 1971 [chaps. 12, 13 above). .

22. Cf. Eugene P. Wigner, “The Unreasonable Effectiveness of Mathematics
in the Natural Sciences,” Communications on Pure and Applied Mathematics
XIE (1960), pp. 1-13. Wigner is a Nobel Prize winner in physics.

23. AIER, Life Insurance and Annuities from the Buyers Point of View,
Aug., 1969, p. 25. The Institute does not argue that annuities are totally unwise
economic investments, but only that a person should invest but a part of his
assets in them. The greater the rate of inflation, the less should be invested.

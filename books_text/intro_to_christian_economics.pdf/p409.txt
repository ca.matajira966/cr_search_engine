Bibliography 397

Kirzner’s Market Theory and the Price System (Van Nostrand,
1963). Widely read and tolerable is Richard Leftwich, The Price
System and Resource Allocation (Holt, Rinehart, Winston). If aveil-
able in a used bookstore, George Stigler’s Theory of Competitive
Price (Macmillan, 1942) is good—distinctly superior to his later
Theory of Price, which is the one, of course, that is still in print.

Macroeconomics is a frand. Any attempt to explain it from any
other perspective will result in error and even more fraud. Its results
are predictable—bad, Milton Friedman, who once uttered the magic
words, “We are all Keynesians now”—meaning in basic methodology,
although not necessarily in conclusions-~also admitted at the 1971
meeting of the American Economic Association:

We have been driven into a widespread system of arbitrary and
tyrannical control over our economic life, not because “ “eco-
nomic laws are not working the way they used to,” not because
the classical medicine cannot, if properly applied, halt inflation,
but because the public at large has been led to expect standards
of performance that as economists we do not know how to
achieve. Perhaps, as our knowledge advances, we can come
closer to specifying policies that would achieve these high stand-
ards. Perhaps, the random perturbations inherent in the eco-
nomic system make it impossible to achieve such standards, And
perhaps, even if there are policies that would attain them, con-
siderations of political economy will make it impossible for these
policies to be adopted.

But whatever the future may hold in these respects, I belicve that
we economists in recent years have done vast harm—to society at
large and to our profession in particular—by claiming more than
we can deliver. We have thereby encouraged politicians to make
extravagant promises, inculcate unrealistic expectations in the
public at large, and promote discontent with reasonably satis-
factory results because they fall short of the economists’ promised
land.

 

The departure from the basic restraints of the Bible on issues of
monetary theory and the limits of State sovereignty have brought on
the intellectual crisis Friedman deplores. (His statement appears
in The American Economic Review, Papers and Proceedings [May,
1972], pp. 17-18; it was quoted widely in the press in late 1971).
On comparative economic systems, see Morris Bornstein (ed.),
Comparative Economie Systems (Homewood, IIL: Irwin, 1965),
Wayne A. Leeman (ed.), Capitalism, Market Socialism, and Central
Planning (Boston: Houghton Mifflin, 1963). A good book on
Sovict economy is Robert W. Campbell's Soviet Economic Power
(Houghton Mifflin, 1966}. My book on Marx, Marx’s Religion of
Revolution (The Craig Press, 1968), contains two appendices on

 

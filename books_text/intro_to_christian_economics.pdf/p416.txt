404

249, 251ff, 257, 328, 334, 3398,
383; see also ownership

Resting the land, 214

Restitution, 221, "928, 318

Revelation, vii, x, xii, 354; see also
faith

Revohition, 2414

Rising expectations, 89

Risk, 63, 313, 365, 377£

Roman Empire, 21, 105f, 195, 218,
224, 327

Russia; see USSR

Sabbath, 10, 214, 303

Salary, 239f; see also manager

Saving, 24, 49, 63, 65f, 180, 183

Scarcity, vii, viii, x, 47, 87, 89, 167,
199, 238, 271, 291, 305, 330; see
also curse, finitude

Scissors effect, 56, 195

SDRs, 57n; see also IMF, paper gold

Search costs, 299

Security, 240, 243; see also responsi-
bility

Seignorage, 191

Selective controls, 172f; see also price
controls

Selfishness, 253

Sermon on the Mount, 218, 224

Sharing, 253, 255

Shortages, 172, 186, 192, 196; see also
inflation (repressed), rationing

Silver, 4f, 21f, 31, 88, 95, 100, 121f,
129%, 142, 145, 156, 162n, 176ff,
185, 198, 202, 2

Sin, viii, 4; see also oppression

Slavery, 8, 10, 214, 250

Social change: see change

Social Credit, 43n, 93, Tos, 124, 161f

Social Darwinism, 77

Socialism, 17, 32, 54, 78, 80f, 211,
234,238, 248, 251, 256, 305; see
also mixed economy, planning
(state)

Something for nothing, 23, 25, 32, 50,
154, 158, 166; see also curse,
magic, scarcity, stone into bread

Sovereignty, 6f, 96f, 105f, 108, 120,
131, 135, 138, 140f, 205ff, 213,
223, 226, 229, 231, 245, 276, 309,
314, 317, 3244, 324f, 328f, 3671,
383; see also consumer
sovereignty

Spaceship analogy, 274ff

Special Drawing Rights; see SDRs

Speculation, 99, 114, 181f, 193; see
also forecasting

Stable prices; see prices (stable)

Stability, 58, 79, 112, 123, 145, 188,
244; see also continuity

State religion, 320

Statistics, 82, 91, 105, 119, 136n, 1488;
see also inflation (proportional),
methodology

An Introduction to Christian Economics

Status quo, 3

Stewardship, 251, 328, 333, 339, 366ff,
375, 383; see also ownership,
responsibility

Stock market, 97, 184

Stones into bread, 86, 153, 155, 158,
161; see also magic, something
for nothing

Stranger, 215n

Subsidies, 117, 344; see also redistri-
bution, tariffs

Substitution, 279; see also cost

Sunk cost; see cost (sunk)

Supervisors (male), 281

Supply & demand, 19, 21, 36, 61f, 71;
see also allocation, price system

Supreme Court, 14

Switzerland, 117

Tariffs, 101, 160f, 228, 235, 339, 343£
Taxation, 42, 48f, 195, 246, 272, 308i
exemptions, 320
invisible, 7, 21f, 24, 32, 35, 42, 49,
54, 64, 95, 153f, 158, 179, 195
property, 3088
reform, 322, 324
‘Taxpayers’ revolt, 195
Technocracy, 156, 272
Technolagy, 61, 251, 269, 304
Tenure, vii, 240, 293, 294
Theft, 29, 32, 50f, 142, 198
Theocracy, 217, 223
Theory & practice, 107, 119, 275
Thrift, 8, 68; see also fature-
orientation, saving
Time, 34, 73, 363n, 364
Time-preference, 628, 65f, 364f; see
also interest
Tithe, 323,329, 334, 362, 367£
Toleration, 329n
Tolkatchi, 82, 266
Totalitarianism, 226, 228, 246, 267
Tower of Babel, ix, 341
Townsend Pian, 156n
Trade, viii, 26, 48f, 55, 58, 68, 95,
109, 116f, 136, 254, 341, 344
Trade cycle, S9f, 63, 152, 364; see also
boom-bust
Treasury, ix, 31f, 34, 43, 50, 130, 137,
14if, 144, 176ff, 19%f, 197, 356
Trimonetary standard 178
Two-tier system, 180f; see also gold
crisis

Unemployment, 54, 101, 283, 338

UNESCO, 79

Union card, 295f; see also degrees
{academic}

Union of Radical Political
Economists, x

Unions, 33, 34f, 66, 101, 166,
174, 338£

Unitarianism, 329n

United States Notes; see greenbacks

172,

36 An Introduction to Christian Economics

solid, formerly profitable businesses that had been beneficial to both
buyers and owners in the pre-inflation period now begin to lose
money. Costs are tising faster for certain industries than are profits;
capital is being redirected into other industries; Jaborers are moving
imto-areas where higher wages are present. Firms which had just
barely broken even before the inflation (marginal firms) may now
go under and be forced to declare bankruptcy. They are bought out
by the favored industries, and a centralization of production begins,
with the favored industries leading in expansion and growth. The
marginal firms were not destroyed through honest competition, i.c.,
because they were unable to offer services equal to competitors, but
because.some members of the economy have been given access to
counterfeit money and are thus enabled to compete with an unfair
advantage.

Capital—raw materials, human labor, production machinery—has
been redirected, and in terms of the preinflation conditions, mis-
directed, Efficient firms can no longer compete, so they fold up;
as inflation progresses, they fold up even faster. Supplies, initially
stimulated, may begin to fall as the more efficient firms (in terms of
a non-counterfeit currency) collapse. Prices, the guideposts of a
free market, have been distorted by the injection of the new money,
exactly as the addict’s senses are distorted by the drug, and the
economy reels drunkenly. Paper profits appear, followed by rising
costs which may wipe out all the gains. Businessmen are thrown into
confusion, as are laborers, housewives, and professional business
forecasters: where to invest, what is sound, where will rising costs
lag behind increasing profits? Investments go where the profits are,
but the profits are measured by a mixed currency, part specie metals
and part counterfeit promises to pay specie metal. Counterfeit profits
stimulate the creation of “counterfeit industries,” while wiping out
formerly productive enterprises. The redistribution of wealth results in
the destruction of wealth, aud the consuming public is injured: some
have become rich, but the majority pays for its new-found prosperity.

By fouling up the price mechanism, the inflationary drug has helped
to paralyze industry. The economy fares no better than the addict.
By ignoring reality, i.e., the true conditions of supply and demand, the
inflationist economy helps to destroy itself just as surely as the addict
destroys himself by trying to escape the real world. The senses are
dulled, and the organism suffers.

3. The Body Adjusts to the “Junk” and More Is Demanded

The addict’s body eventually adjusts to the drug that has entered
his system, compensates for its destructive effecis, and then attempts

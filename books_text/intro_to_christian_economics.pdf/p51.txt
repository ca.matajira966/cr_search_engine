Inflation: The Economics of Addiction 39

for the sake of the analogy it is treated as such. Continuous inflation
will, however, spell the death of the circulating media that is used.
Eventually, the market will be forced to shift to some new means of
price measurement, to some new device for economic calculation.
If the inflation is permitted ta progress to this point, the social and
economic results can be devastating. Economies do not die, but the
social order can be replaced. The classic example is Germany in
1923.9 The effects upon individual members of the society could
Jead to chaos, leaving large segments of the population spiritually
demoralized.

The habit cannot go on forever. It will either be stopped, or clse
the addict will die, in the case of the human, and the monetary sys-
iem will collapse, in the economy’s case. The thought of the latter
alternatives turns one’s attention to the former one: stopping the
inflation and stopping the drug.

5. Shaking the Habit

Withdrawal——the most frightening word in the addict’s vocabu-
lary. Depression—the most horrible economic thought in the minds
of today’s citizens. Yet both come as the only remedies for the
suicidal policies entered into.

To the addict, withdrawal means a return to the normal functioning
of the body, a return to reality. The path to normalcy is a decidedly
painful avenue. Withdrawal will not restore him to his pre-addiction
condition, for too much has already been lost—-socially, physically,
financially, spiritually, But he can live, he can survive, and he can
make a decent life for himself,

For the inflationist economy, a cancellation, or even a reduction,
of the inflation means depression, in one form or another. This is
inevitable, and absolutely necessary.2! Prices must be permitted to
seek their level, production must rearrange itself, and this will mean
losses to some and gains for others. The inflationary effects of the
monetization of debt, the pyramiding of credit, are then reversed.
The man who deposited the $100 is pressed for payment by creditors,
so he withdraws his money. The banks are faced with either heavy
(and unfulfillable) specie demands, or at least with credit and cur-
rency withdrawals. The bank calls’ in its loans, sells its property,
begins to liquidate. The man who had borrowed the $90 now must

 

10, Cf. Constantino Bresciani-Turtoni, The Economics of Inflation (London:
George Alien and Unwin, 1937).

11, Murray Rothbard, Man, Economy and State (Princeton: Van Nostrand,
1962), Hl, 854%. “One point should be stressed: the depression phase is ac-
tually the recovery phase” (ibid., p. 860). This study has been reprinted by
the Nash Publishing Co., Los Angeles.

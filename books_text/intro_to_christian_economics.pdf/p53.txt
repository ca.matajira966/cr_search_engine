Inflation: The Economics of Addiction 41

jnflation is not just a question of proper economic policy. Above all,
it is a question of morality, If we should permit the State to continue
its fraud of indirect taxation through inflation, then we would have
lite argument against what is clearly the next step, the final removal
of alf natural resistance to inflation through the establishment of a
world banking system, Mises warned half a century ago that the
establishment of a world bank would leave only panic as the last
barrier to total inflation.**

In the realm of practical recommendatians, at least two seem
absolutely imperative. The first is simple: completely free coinage
as a right of private property, with the government acting as a dis-
interested third party ready to step in and prosecute at the first sign
of fraud on the part of the private firms. Any debasement of these
private coins would be prosecuted to the limit of the laws. The private
mints would therefore find it advantageous to keep continual watch
aver each other’s coinage, calling the State’s attention to any sign of
fraud. By climinating the present State monopoly of coinage, private
competition could act as a barrier to monetary fraud. Without
this mutual competition, the State’s monopoly of coinage can con-
tinue, with only minor checks on debasement. Private coinage would
never eradicate personal greed, of course, but mutual avariciousness
would tend to place checks on the fraudulent practice of theft through
debasement,

The second recommendation, free banking, is similar to the first
one of free coinage. The banks must be made to gain their profits
from the charging of storage costs, clearing house operations for
checks, and the investment of private trust funds. When banks
create credit (and the power of credit creation is precisely what de-
fines a bank, as distinguished from a savings and loan company),
they charge interest on loaned funds which have been created by
fiat. There are no gold or silver reserves backing this money, yet the
banks profit by lending it. It involves fraud, and it is therefore im-
moral. ‘The practice must be hindered,

Professor Mises argues that free banking will keep bankers honest.
Mutual competition will tend to destroy banks that are insolvent
because of their heavy speculative policies of credit creation. Bank runs
will tend to drive the less conservative banks out of business. There may
be some credit creation, but very little in comparison to that which
exists today, when the governments support fractional reserve fraud.
He fears a law which would require one hundred percent reserves for
banks, however, for the power of the State to demand one hundred

14. Mises, Money and Credit, p. 291 ff.

Domestic Inflation versus International Solveney 57

Jf higher interest rates are not offered in the United States and in
Britain, then foreign investors and central banks will cash in their
investments and demand payment in gold. On the other hand, if
jnterest rates are permitted to climb higher, the domestic rate of
growth will be drastically affected. Money will be “too tight,” too
expensive for the prospective borrowers. Hence, the “scissors effect.”
There is no simple solution to the problem,

Jn 1964, the United States lost some $385 million in gold; in
1965, the joss tripied, amounting to over $1.1 billion. In the first six
months of 1956, the outflow was almost $300 million.’ The costs of the
war in Viet Nam are increasing the deficit in the budget. In Britain,
Prime Minister Wilson has been forced to declare a price and wage
freeze in order to halt the inflationary rise in prices; this, of course, is re-
pressed inflation—-the hampering of the market by government con-
trols—and not a cure. But at least political leaders in the two nations
have come to the realization that continued deficits and continued
increases in the money supply (apart from increases in gold and
silver} cannot go on much longer without serious repercussions in
the world money market, and hence, in the world’s trading com-
raunity.

Thus, we can understand the frantic search for a nongold inter-
national medium of payment. The economic isolationism which al-
ways results from domestic inflations cannot be permitted to disrupt
the fabric of international integration and trade. Devaluation (charg-
ing more dollars or pounds for a given quantity of gold) could easily
destroy confidence in both currencies, and thus result in international
economic chaos, Mutual distrust would then be the order of the day
in all international transactions. The problem is that no substitute
for gold has yet been discovered (or created) by mankind; aud gold,
because of its resistance to “full employment” inflationary policies,
ig taboo. What is needed, we are told, is something “as good as gold,”
yet which permits domestic inflation, There are numerous suggestions
for such an international money, probably under the control of the
International Monetary Fund, but no single plan has reached even
partial acceptance by the economists and officials of the nations
involved.” A fundamental obstacle to be overcome is the basic

8. Computed from the tables in Mineral Industry Surveys (Washington,
D, C.: Bureau of Mines, Aug., 1966), p.

9. For a summary of these positions, at least of the leading ones, see
Arthur Kemp, The Role of Gold (Washington, D. C.: American Enterprise
Institute, 1963). Even in 1972, the debate over the IMF's “SDR™ system—
special drawing rights—goes on. The. role of gold is just as hot a topic in
international monetary circles as it was in 1967. Statist economists may want
to see a smaller part played by poid, but they all admit that the dollar, as it
depreciates, will play a smaller role in international monetary affairs.

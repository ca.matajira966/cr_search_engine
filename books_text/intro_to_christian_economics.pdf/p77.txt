Repressed Depression 65

keep the interest rate from going higher, and it may even drop
lower, bat only temporarily, i.c., during the boom period. In fact, one
of the signals that the boom is ending is an increase in the rate of
interest. Capitalists misinterpret this low rate of interest; what is
really merely an increase in the availability of money is seen as an
increase in the availability of capital goods and labor services, Tn
reality, savers have not provided the new funds by restricting their
consumption, thereby releasing capital goods that had previously been
used to satisfy consumer demand more direcily, i.e., more rapidly.
Their patterns of time preference have not been altered; they still value
present goods at a higher level than the rate of interest indicates.

Capitalists purchase goods and services with their new funds. The
price of these goods and services will therefore risc in relation to the
price of goods and services in the lower stages of production—those
closer to the immediate production of consumer products, Labor and
capital then move out of the lower stages of production (¢.g., a local
restaurant or a car wash) and into the higher stages of production
(e.g. 4 steel mill’s newly built branch). The process of production
is elongated; as a result, it becomes more capital-intensive. The new
money puts those who have immediate access to it at a competitive
advantage: they can purchase goods with today’s new money at yes-
terday’s lower prices; or, once the prices of producers’ goods begin to
tise, they can afford to purchase these goods, while their competitors
must restrict their purchases because their incomes have not risen
proportionately. Capital goods and labor are redistributed “upward,”
toward the new money. This is the phenomenon of “forced saving.”
Those capitalists at the lower stages of production are forced to for-
feit their use of capital goods to those in the higher stages of pro-~
duction, The saving is not voluntary: it is the result of the inflation.

The result is an economic boom. More factors of production are
employed than before, as capitalists with the new funds scramble to
purchase them. Wages go up, especially wages in the capital goods
industries. More people are hired. The incumbent political party
can take credit for the “good times.” Everybody seems to be pros-
peting from the stimulating effects of the inflation. Profits appear to
be easy, since capital goods seem to be more readily available than
before. More capitalists therefore go to the banks for loans, and the
banks are tempted to permit a new round of fiat credit expansion in
order to avoid raising the interest rate and stifling the boom,

Sooner or later, however, capitalists realize that something is
wrong. The costs of factors of production are rising faster than had
been anticipated, The competition from the lower stages of pro-
duction had slackened only temporarily. Now they compete once

The Fatlacy of “Intrinsic Value’ 73

rarefied philosophical debate concerning metaphysics, but I think
jt is safe to say that gold does have certain intrinsic qualities, It is
highly durabie, easily divisible, transportable, and most of all, it is
scarce, Money must be all of these, to one degree or another, if it
is to function as a means of exchange. It is vital that we get our cate-
gories straight in our minds: it is mot value that is intrinsic to gold,
but only the physical properties that are valued by acting men.
Gold’s physical properties are the product of nature; its value is
the product of acting men.

It would be a terrible mistake, however, to de-emphasize the
historic value of gold and silver merely because they possess no in-
trinsic value. That mistake is the one which the opponcnts of gold
would have us make. They are equally guilty of mixing up the
categories of intrinsic valuc and historic value, only they argue from
the other direction, Conservatives appreciate the fact of gold’s his-
toric value, but they mistakenly argue their case in terms of gold’s
intrinsic value, Their opponents do not appreciate the argument from
history, but they spend their time refuting the conservatives’ erroneous
presentation. They assume that because gold has no intrinsic value
(true), gold’s historic value as a means of exchange is somehow invali-
dated. The two positions are diametrically opposed, yet they focus on a
common ground which is irrelevant to both positions; the conserva-
tives do not help their case for gold by an appeal to intrinsic value,
and gold’s opponents do not refute the case for gold by demonstrating
the error of that appeal.

Gold’s overwhelming acceptance historically by most men in most
societies is a lasting testimony to its value as a means of exchange.
It should not be referred to as “a storehouse of value,” as it is in so
many textbooks. What we should say is that gold is readily market-
able and for that reason a valuable thing to store. This position of
gold in history is a self-perpetuating phenomenon: people tend to
accept gold because they and others have in the past; they assume
that others will be willing to accept gold in exchange for goods in the
future, This assumption of continuity is basic to all goods that func-
tion as money. Continuity is therefore a function of both the physical
properties of gold and of men’s estimations concerning other men’s
future valuations, In short, it involves nature, man, and time. In
estimating the importance of gold for an economic system’s proper
functioning, we must take into consideration all three factors, keeping
each clear in our minds. This is why we need economic analysis;
without it, we wander blindly.

Ignorance in the short run is seldom profitable; in the long rum, it
is invariably disastrous. Fallacious argumentation can too easily

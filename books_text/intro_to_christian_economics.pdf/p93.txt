The Theology of the Exponential Curve 81

cient in providing needed aid; “helpful” bureaucrats from both the
United States and the Soviet bloc have bungled program after pro-
gram, as bags of cement get shipped to Southeast Asian countries
during the rainy season, and mammoth buildings without air-
conditioning get built in African states.° The reports of such
projects are legion; if they were not so expensive they would be
much funnier.

Naum Jasny, in his book, Soviet Industrialization, 1928-1952
(1961), reveals how Stalin used growth figures as propaganda de-
vices, setting goals for the five-year plans (themselves propaganda
devices more than planning tools) so high that no economic system
could have produced the anticipated results. It was growth for the
sake of growth. The actual per capita output of consumer goods did
not significantly increase until the mid-1950's; only in 1952 did wages
teach the level that Czarist Russia had attained in 19131°

Two wars and a revolution obviously were retarding factors in
Russian economic expansion, So were the loss of manpower in the
famine of the 1920’s and the collectivization of the farms in the
late 1920's and early 1930's; at a minimum estimate, five million
persons were either executed or deported during collectivization.'*
A million people starved to death in [933 alone, during the forced
collectivization of the farms, Jasny estimates.!®

The Soviet Union has experienced a high growth rate of its aggre-
gate output of goods; Bergson’s estimates put it at 4.5 percent per
annum, 1928-1960, and 5.2 percent if we exclude the war years.!®
The rates have tapered off significantly since 1960, but enough
people seem to be so impressed that they deliberately ignore the costs
of that industrial growth to the Soviet Union in human lives and hu-
man freedom (which is almost impossible to deal with statistically,
so it is relegated to the background). What did these growth rates
actually reflect? Jan Prybyla’s comments are to the point:

What the Russians have shown is that cockeyed economic growth
at rapid rates can be achieved without economists and without
economic science; but that after the economy outgtows its teen-age
crisis, elusive and subtle problems of resource allocation among

 

15. Victor Lasky, The Ugly Russian (New York: Trident, 1965), presents a
whole book full of ‘such tidbits of bureaucratic bungling.

16. Janet Chapman, Real Wages in Soviet Russia ‘Since 1928 (Cambridge,
Mass.; Harvard University Press, 1963), p

17. Robert W. Campbell, Soviet seondanie Power (2nd ed.; Boston: Hough-
ton Mifflin, 1966), p. 24.

18. Naum Jasny, Soviet Indusiriatization, 1928-1952 (Chicago: University
of Chicaga Press, 1961), p.

19. Abram Rergson, The Ezonomies of Soviet Planning (New Haven: Yale
University Press, 1964), p.

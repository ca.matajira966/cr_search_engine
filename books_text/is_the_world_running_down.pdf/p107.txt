Entropy and Social Theory 71

but to crumble slowly ~as in erosion, decay, and aging.”> This is a
philosophy of self-conscious defeat, a cry of despair. It is also not the
kind of philosophy that anyone would normally choose to use to
challenge the Marxists in Latin America.

The whole idea is wrong-headed. First, the entropic process of
cosmic physical decay takes place in humanist time scales of billions
of years. Such a time scale is irrelevant for social theory, Christian or
pagan. Societies do not survive for billions of years— not so far, any-
way. I shall return to this theme when I deal with Rifkin’s writings.

Second, what does it mean to say “the world will [or will not] im-
prove”? What world? The geophysical world? What does an ethical
or aesthetic term such as “improve” have to do with the physical
world? Scientific evolutionists have been careful to avoid such value-
laden adjectives with respect to historical geology or biology, at least
until man appears on the historical scene and begins to affect his en-
vironment. Without a moral evaluator, there can be no meaning for
the word “improve.”

Christians should be equally careful in their use of language.
The Christian should argue that God evaluates any improvements or
declines of the external world, and therefore men, acting as God’s
subordinates, also make such evaluations. But there is no autono-
mous impersonal standard of “world improvement,” as any evolution-
ist readily admits, So the flyer apparently had as its point of refer-
ence not the geophysical world but rather man’s social world. This im-
mediately raises a crucial question: How do the operating standards
of man’s social world relate to the physical process of entropy? This
is the question that Scientific Creationists have generally avoided,
and when they have on occasion said something about it, they have
sounded like Jeremy Rifkin.

Rifkin's Tactic

Rifkin has fully understood this, He uses it against his funda-
mentalist Christian reader. He appeals directly to premillennial
eschatology in creating the case for social pessimism. He also ap-
peals to the Creation Scientists’ use of the first and second laws of
thermodynamics. In fact, he sounds as though he is paraphrasing
Henry Morris: “Interestingly enough, the creation story directly
parallels the two basic laws of thermodynamics. According to Gen-

5. What's the Difference? Creation/Evotution? {no date), p. 2.

Entropy and Social Theory 5

not a good statement of the second law, as held by conventional scientists; it
does reflect the language of Creation Scientists—G,N.] That ordered state
is continually being eroded by an irreversible natural process. According to
this law of entropy, all matter and energy are constantly and without excep-
tion moving from an ordered to a disordered state.

Many evangelicals have basically embraced this scientific view of matter
and have applied it to society, with an attendant fatalism.®

The key word is fatalism. This is the heart of my critique of the
use of the second law to construct social theory. What I argue is that
this fatalism has in fact been imported from two sources: 1) a popu-
lar (though fading) version of premillennial eschatology — one which
Reid’s article rejects~ and 2) an inapplicable version of nineteenth-
century physical science, one which evolutionists had been unwilling
to apply to social theory until Rifkin appeared on the scene.

“Winding Down”

Let us pursue this “wind up” and “run down” analogy. Clocks
(like galaxies) unquestionably run down over time, but this has
nothing to do with the purposes, good or evil, to which clocks can be
put. Furthermore, men can rewind clocks. Similarly, God can also
“rewind” the universe or any aspect of the universe. He has done so
in the past. If we feel compelled to use mechanistic analogies, we can
call such a “rewinding event” a miracle. When Jesus healed sick peo-
ple, multiplied fishes and loaves, turned water into wine, resurrected
Lazarus from the dead, rose from the dead Himself, and ascended
into heaven, He overcame the “entropy process” (assuming that we
equate the “entropy process” with the curse of Genesis 3:17-19, as
Creation Science usually does). He “rewound” certain aspects of the
unwinding cosmic clock.

Why, then, should Christians cling frantically and fanatically to
the doctrine of entropy as immutable? It clearly is no¢ immutable. It
is only a “most of the time” backdrop to life. One reason for this
adherence to the second law by Christians is that most modern scien-
tists today still hold such a view of the universe as a whole—the
“closed system” of the supposedly uncreated universe. Since edu-
cated Christians want to be thought of as scientific, they adopt this
commonly held entropic view of the long-term fate of the universe. In

12. Tommy Reid, “Understanding ‘Kingdom Now’ Teaching,” Ministries (Sum-
mer 1986), p. 76

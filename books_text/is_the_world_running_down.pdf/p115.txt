Entropy and Social Theory 79

tians account for the growing complexity of social life, any more
than evolutionists can account for the growing complexity of biologi-
cal life? The biblical answer is ethics: God has blessed His holy word
as it has been extended across the West. It is the abandonment of the gos-
pel which now threatens the West, not the “process of physical en-
tropy” as such, Scientific Creationists have another problem if they
are premillennialists. Will the return of Christ overcome the entropy
process? Will the effects of entropy be in some way suspended, at
least with respect to biology? Will people live longer and healthier?
What about reducing “social entropy”? Will communications im-
prove (i.e., will “noise” be reduced)? Will order begin to overcome
randomness? If so, how? If not... what kind of millennium will
that be?

But if the effects of entropy can and will be overcome after Jesus
returns bodily to reign, but before the final judgment and the trans-
formation of the universe, then why can’t we also argue that if men
voluntarily begin to conform themselves to God’s laws before Jesus
returns physically, the cursed effects of entropy will be progressively
overcome just as surely as after Jesus’ premillennial return?
Premillennialists argue that men will not repent in this fashion, but
this begs the question. The question is: Does God promise that the
effects. of His curse on man and nature can progressively be over-
come (though not absolutely) in response to ethical regeneration?
The biblical answer is clear: yes (Deut. 28:1-14).

Entropy is, at most, a backdrop—a kind of measurable down
payment on the death that awaits all life apart from regeneration
and ethical restoration. Entropy’s effects can (and have been) over-
come by righteousness. Miracles are real. So is regeneration. So is
human progress.

II, Jeremy Rifkin'’s Social Pessimism
The same sorts of questions should be directed against Rifkin’s
social speculations. Rifkin has written two widely read and enthusi-
astically reviewed books that popularize a theory of static society,
The Emerging Order: God in the Age of Scarcity (1979) and Entropy: A New
World View (1980). These books are clear, well-written, and present a
consistent, profoundly anti-Christian view of the world. Because of
the consistency of his arguments, and because of his anti-Christian
conclusions — conclusions presented in the name of a “new Christi-

anity”— Rifkin’s books deserve considerable attention.

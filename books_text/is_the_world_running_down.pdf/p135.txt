The Entropy Debate within Humanistic Science 99

Il. Entropy and Guilt

Rifkin’s book is a call to the West to experience a religious con-
version. He wants all men, but especially residents of the industrial
West who rely on the West’s energy-dissipating technology, to stop
aiding and abetting the entropy process. We must stop using so
much energy. “Addiction! There is simply no other way to accurately
describe America’s energy habit.” In other words, he wants us to
take steps to avoid guilt in what must be regarded as man’s cosmic re-
bellion against nature. The social entropists are unconcerned about
man’s ethical rebellion against the Creator, they worry about Western
man’s supposed rebellion against the very mode of being of the creation
(including man himself), which they believe is an entropy-bound
evolutionary process. In short, they worship the creation rather than the
Creator (Rom, 1:18-22),

These are guilt-ridden men, or at least they are politicians who
are trying to make guilt-ridden people out of their readers. They are
residents of the industrial (and originally Christian) West, and they
see themselves as accomplices in a kind of gang rape of the cosmos,
or at least of the “fragile, defenseless” earth. They are seeking relig-
ious atonement by crying out against the supposed metaphysical sin
of the West, which ultimately is the sin of life itself. All huing beings are
an affront to entropy. Life dissipates the energy and order of the cosmos,
speeding up the entropy process by at least a few milliseconds (or
picoseconds) in a 100 or 500 billion-year process of the universe.
What a sin! We must atone! Life must therefore be limited.

The question is: Whose life?

Life and Death

Rifkin is caught in a theological dilemma: he worships all life, as
a good Eastern mystic always does, ® yet he also believes that the en-
tropy process must be honored. On the one hand, he writes: “Every
species must be preserved simply because it has an inherent and
inalienable right to life by virtue of its existence.” On the other
hand, he writes, “even the tiniest plant maintains its own order at the
expense of creating greater disorder in the overall environment.”®

15, Ritkin, Entropy, p. 99.

16. Cf. R. J. Rushdoony, Politics of Guilt and Pity (Fairfax, Virginia: Thoburn
Press, [1970] 1978), ch. 2.

17. Entropy, p. 210.

18. Ibid., p. 53.

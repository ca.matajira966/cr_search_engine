110 IS THE WORLD RUNNING DOWN?

matter. What Rifkin ignores is a fourth possible response to his
book, a self-consciously biblical response.

The Christian accepts the reality of a time-bound, sin-filled, and
God-cursed universe. He understands that God's curse is also a
blessing for mankind, for economic scarcity makes profitable volun-
tary human cooperation.® The Christian also argues that various
degradative processes are governed by man’s ethical response to the
gospel of Jesus Christ, When men respond in obedient faith to God,
God lifts many aspects of the curse. He promises even to extend
drastically the average lifetime of mankind (Isa. 65:20). In short,
ethics is primary, not some entrapy process. The tendency of all things to
wear out is progressively overcome, just as surely as miracles reverse
this tendency. Thus, degradative processes are not uniformitarian
sentences of doom; they are covenantal curses that can be (and have
often been) reversed in human history.

The Christian can therefore accept and promote some of the
scientific conclusions of the present scientific world-and-life view, to
the extent that this view is future-oriented, rational, and based on
the concept of cause and effect. The Christian believes in the possi-
bility of ethics-based dominion, for God promises to bless covenant-
keeping man, and to remove His curse progressively, if men obey
Him. Covenant-keeping man does not seek power as a religious im-
pulse; he seeks first the kingdom of God and His righteousness, and
all things, including power, are then given to him (Matt. 6:33).

The Christian must not accept Darwinism, or pure materialism,
or the idea of the autonomous progress of man, but he can accept the
basic optimism of the Western view of man, for this Western view
was originally Christian. That this cursed world has a temporal end is
specifically a Christian idea. In fact, its end can be expected several
billion years before evolutionists expect the sun to flame out or ex-
plode in a nova. But the end of éhis world is not the end of ée world in
the Christian worldview, unlike the pessimism of the entropists’ view.

Christians must reject the conclusion of the social entropists,
namely, that because this present age is governed by physical en-
tropy, man’s social institutions cannot experience long-term growth.
What the Christian must affirm is that there is always a possibility of
long-term cthical improvement, both for individuals and collectives

13. Gary North, The Dominion Covenant; Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), ch. 10; “Scarcity: Curse and Blessing.”

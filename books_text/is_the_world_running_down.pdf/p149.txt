Christianity os. Social Entropy 13

The war between evolutionism and Christianity must not be
minimized. We see the conflict between rival religions. This war
manifests itself in the debate over the nature of the final judgment.
As Rushdoony says,

The details differ, but every world-view and every faith has its version of
judgment, heaven, and hell. For some, hell is existence, and heaven is nir-
vana and nothingness. But the basic categories remain, The relativists,
nihilists, and existentialists who deny all absolute values and laws demand
judgment on God, law, and morality; hell for them is a world of absolute
values, which they wage war against, and heaven is a world beyond good
and evil.

But to transfer final judgment, heaven, and hell from the eternal order
to time is to absolutize history and to enthrone man as god. It means the de-
struction of liberty, because history ceases to be the realm of liberty and
testing but becomes the place of final trial. Having made the final judgment
temporal, the humanist cannot permit liberty, because liberty is hostile to
finality; liberty presupposes trial and error and the possibility of serious
waywardness when and where man is sinful and imperfect. History cannot
tolerate both trial and error and [also] insist on finality and the end of trial
and error. The humanistic utopias are all prisons, because they insist on a
finality which man does not possess. Accordingly, the socialist utopias de-
mand the “re-education” of man in the post-revolutionary world, in the era
beyond judgment. The “new era” is the new heaven on earth. . . .

But history refuses to terminate on man’s orders, because it runs on
God's time, and not in terms of man’s myths. As a result, the final orders
which men build have an inevitable habit of decay, and the order which
claims to be final ensures its own destruction as the movement of history
crushes it underfoot in its unrelenting march to epistemological self-
consciousness, ®

Conclusion

The appeal of optimism is a kind of “uniformitarian ideological
constant.” Every group that seeks a large audience understands that
it must offer hope. Premillennialists and amillennialists offer hope
beyond the grave; premillennialists also offer hope during the mil-
lennium, when Christ returns physically to rule in power. These are
appeals beyond the declining historical present. They are appeals to dis-
continuity in the midst of cultural and historical despair —a despair

16. R. J. Rushdoony, The Foundations of Social Order: Studies in the Creeds and Councils
of the Early Church (Fairfax, Virginia: Thoburn Press, [1968] 1978), pp. 176-77.

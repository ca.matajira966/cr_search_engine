116 IS THE WORLD RUNNING DOWN?

12. He never mentions Christ's victory at Calvary.

13. There is no hope in the status quo if entropy governs the
world,

14. He calls for a stalemate: Satan’s old lie.

15. History is linear.

16. Scientists cannot find a way for mankind to escape oblivion.

17. Biblical optimism is based on God’s progressive sanctification
of the world.

18. Ethics is primary, not entropy.

19. God brings covenantal blessings on those who are faithful to

20. Christianity must reject humanism’s pessimism.

21. We can see real progress in history,

22. Victory is supposed to be a continuous process in history.
23. Growth is a Christian imperative.

24. All systems have a doctrine of final judgment.

25. Humanism’s is the heat death of the universe.

26, This absolutizes history and the creation.

27, History will end on God's orders, not man’s, and not entropy’s.

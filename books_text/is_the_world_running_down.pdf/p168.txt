132 18 THE WORLD RUNNING DOWN?

alone is God incarnate. Man does not become God through magical
or metaphysical union with God. It is the age-old heresy of Satan
that man can become God.

Because man is restored ethically before God, he has become a
new creation, Paul says. “Therefore, if any man be in Christ, he is a
new creature [creation]: old things have passed away; behold, all
things have become new” (II Cor. 5:17). Alf things: Christians are
supposed to take this seriously. God grants comprehensive redemption to
His people.? Every aspect of their lives is transformed. God sends
the Holy Spirit that they might be comforted and led into all truth
(John 14:16-17). The dominion covenant can be fulfilled through the
efforts of His redeemed people.

The Bible teaches redemption, not mysticism. It teaches domin-
ion, not retreat. [1 teaches reconstruction, not conformity to this
world, It teaches growth, not stagnation and a steady-state economy.
It teaches conquest through time, not mystical transcendence
beyond time.

Rifkin’s Mysticism

Rifkin’s theology is mystical, not Christian. Christianity explains
man’s history in terms of ethics: the rebellion against God which led
to the curse of creation, Rifkin cites the “small is beautiful” guru,
E. F. Schumacher, author of Buddhist Economics: “The most urgent
need of our time is and remains the need for metaphysical recon-
struction. . . .”* But Christianity teaches that it is not metaphysical
reconstruction that is needed; it is ethical reconstruction. The Christian
philosopher Cornelius Van Til has warned against any confusing of
the ethical theology of Christianity with the metaphysical theology of
humanism:

‘We know that sin is an attempt on the part of man to cut himself loose
from God. But this breaking loose from God could, in the nature of the
case, not be metaphysical; if it were, man himself would be destroyed and
God’s purpose with man would be frustrated. Sin is therefore a breaking
loose from God ethically and not metaphysically. Sin is the creature’s enmity
and rebellion against God but is not an escape from creaturchaod.

3. Gary North, “Comprehensive Redemption: A Theology for Social Action,”
Journal of Christian Reconstruction, VIIL (Summer 1981), Reprinted in this book as
"Appendix C

4. Entropy, p. 205.

Preface xvii

man’s relationship with God and nature, and it has therefore altered
God's relationship with man and nature, for man is God’s cove-
nantal representative over the earth (Gen. 1:26-28).

Science and Motivation

You need to keep this question in mind as you read this book:

If the world is inevitably running down, what hope can
Christians legitimately place in their earthy efforts, scientific and
otherwise, to improve life on earth?

If Christians have no legitimate biblical or cosmic hope in
achieving the gospel’s goal of making God-honoring, worldwide im-
provements in the external condition of this world in history (Deut.
28:1-14) before Christ returns physically in judgment, then why
should they sacrifice everything they possess to launch a doomed
frontal assault against Darwinism? There is only one reason:
because they believe that God has told them to do so. But the prob-
lem with this fall-back motivation is that the vast majority of those
who deny that the church can be victorious in history also believe
that God’s law is no longer binding in New Testament times. Thus,
the ethical motivation of obedience is undercut, for Christians do not
know specifically what God requires them to do. Thus, they throw
away their God-given tool of dominion, His law.8 They have lost the
motivation of victory (optimism) and the method of victory (God’s
law). They have therefore lost most of their cultural battles, and they
have been involved in only a few since 1925 (the year of the Scopes
“monkey trial”).

Darwinists obviously control the major institutions of this world,
including most of the churches. The readers of this book know this,
So do the readers of Scientific Creationism’s books. If the world can-
not be comprehensively improved prior to Christ’s second coming,
then why should Christians risk everything they presently possess in
trying to convert, dislodge, and replace the Darwinian princes of this
world? Why not just stand on street corners and pass out gospel
tracts and wait for the inevitable end?

This mental outlook of assured historical defeat creates a major
problem for Scientific Creationism. There is nothing to be gained

8. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Reconstruction, 1988).

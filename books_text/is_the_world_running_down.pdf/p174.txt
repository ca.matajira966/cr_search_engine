138 IS THE WORLD RUNNING DOWN?

like Rifkin, are hostile to biblical law, with its promises of growth
and its vision of responsible progressive dominion. They cannot
refute him, nor even recognize the totality of his underlying hostility
to Christian orthodoxy, because they have themselves abandoned
too many of the fundamental tenets of the Christian faith.

Misrepresenting Christianity

Riikin seeks to overcome the opposition of potential Christian
critics by misrepresenting their views. He may even think that he
can confuse them into silence. First, he offers his total misrepresen-
tation of the biblical view of dominion: ‘‘Dominion, which Chris-
tian theology has for so long used to justify people’s unrestrained
pillage and exploitation of the natural world, has suddenly and dra-
matically been reinterpreted.” This summary of the theology of
dominion is, as we have seen, a total fabrication. Then he continues;
“Now, according to the new definition of dominion, God’s first in-
struction to the human race is to serve as a steward and protector
over all his creation.” This means zero growth: the triumph of the
steady-state society, the victory of the status quo.

Finally, Rifkin brags about the victory of this perverse misrepre-
sentation of Christian theology: “It is interesting to observe that this
most fundamental reconception of God’s first order to his children on
earth has been accepted by Protestant scholars, ministers and practi-
tioners in just a few short years without any significant opposition be-
ing voiced. In fact, one would be hard pressed to find a leading Prot-
estant scholar anywhere today who would openly question this new
interpretation of dominion in the Book of Genesis.” (Quite clearly,
my economic commentary on the Book of Genesis, The Dominion Cove-
nant, openly questions “this new interpretation of dominion.”)

Rifkin will be forgotten soon enough. (So, for that matter, will
the neo-evangelicals.) But his mass-produced paperback books
received a wide hearing in neo-evangelical circles in the early 1980's,
which indicates just how confused the intellectual leadership of late-
twentieth-century Christianity really is, The inability of Christian
intellectuals to recognize the profoundly anti-Christian nature of
Rifkin’s explicitly and defiantly anti-dominion ideology testifies to
the extent to which Christian intellectuals have themselves adopted
the presuppositions and outlook of the new humanism.

24. Rifkin, Hmerging Order, p. xi.

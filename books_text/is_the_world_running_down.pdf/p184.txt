148 Is THE WORLD RUNNING DOWN?

God’s law-order is designed to promote the rapid fulfilling of the
terms of the dominion covenant. God does not desire nature to re-
main governed by the law of the jungle, the desert, or the frozen
wastes. He wants the ethical obedience of mankind. When people give
Him obedience, He promises to extend their rule over nature. The
extension of man’s rule over nature is delayed primarily by ethics,
not by innate “limits to growth” in nature. Individual limits can be .
overcome in a few generations, though not at zero cost.

It was sin and rebellion that thwarted the Hebrews in the attain-
ment of their assigned tasks. They turned to the gods of Canaan—
gods of the chaos festivals, the eternal cycles, and the abolition of
time.* It was not the hypothetical autonomous restraint of biological
“negative feedback” which kept the Hebrews from multiplying and
filling the earth; it was instead their adoption of Canaanitic religions
of cyclical growth and decay. They began to work out the implica-
tions of these rival religions, and God permitted them to sink their
culture into the paralyzing pessimism of pagan faiths. He gave them
their request, but sent leanness into their souls (Ps. 106:15). Then He
scattered them: by the Assyrians, the Babylonians, the Greeks, and
the Romans.

History is directional, It is headed toward final judgment. This
has always been the message of biblical religion. Jewish scholar and
legal theorist Harold J. Berman of Harvard writes:

In contrast to the other Indo-European peoples, including the Greeks,
who believed that time moved in ever recurring cycles, the Hebrew people
conceived of time as continuous, irreversible, and historical, leading to
ultimate redemption at the end. They also believed, however, that time has
periods wichin it, It is not cyclical but may be interrupted or accelerated. It
develops, The Old Testament is a story not merely of change but of
development, of growth, of movement toward the messianic age—very
uneven movement, to be sure, with much backsliding but nevertheless a
movement foward. Christianity, however, added an important clement to
the Judaic concept of time: that of transformation of the old into the new.
The Hebrew Bible became the Old Testament, its meaning transformed by
its fulfillment in the New Testament. In the story of the Resurrection, death
was transformed into a new beginning. The times were not only accelerated
but regenerated. This introduced a new structure of history, in which there
was a fundamental transformation of one age into another. This transfor-
mation, it was believed, could only happen once: the life, death, and resur-

4. Gary North, Moses and Pharaoh, ch, 17,

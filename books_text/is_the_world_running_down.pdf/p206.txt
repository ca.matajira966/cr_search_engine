170 IS THE WORLD RUNNING DOWN?

“natural” environment either rebels against them or becomes prog-
ressively less threatening. What we learn from God’s miraculous
interventions into the affairs of men, blessings and cursings, is sup-
posed to teach us a principle of God’s continuing providential but
“non-miraculous” governing of His creation.

Reducing the Curse’s Degradative Processes

The Darwinians see no link between man’s ethics and nature’s
laws. This includes New Age mystics such as Rifkin, at least when
they are pretending to be scientific. Unfortunately, I see no evidence
in the writings of the Scientific Creationists that they have recognized
this link between ethics and nature. They know that the curse will be
lifted from nature after the final judgment, but they see this as a
totally discontinuous event. They see the arrival of a supposedly
zero-entropy world only after history ends, but by the time that day
arrives, the cosmic effects of entropy will have created even more
noise and confusion in the world than we see today. Creation Scien-
tists do not speak of the possibility of a continuous, progressive reduction
in the rate of man-threatening decay. They do not discuss the possibility
that God promises to reduce His curse of the earth progressively in
response to the progressive ethical sanctification of individuals and
societies.

I contend that at least some of them have refused to discuss this
possibility for three reasons: 1) they have adopted something like
Rifkin’s understanding of entropy; 2) they are eschatological pessi-
mists who have rejected the idea that there can be progressive sancti-
fication in human history; and 3) they are antinomians who have re-
jected the doctrine of the covenant, which implies the continuing
validity of God's revealed covenantal law. In short, they share too
many presuppositions with the Darwinians with respect to their doc-
trine of God, man, and law.

There is a legitimate though unfortunate reason for their silence:
they have not read Rifkin. This may be due to a fourth cause for
their silence: they are not really interested in social theory, But they
should recognize, first, that physical science at any point in time is
shaped by prevailing social theory, including presuppositions about
the proper role of science. Second, physical science in turn shapes
social theory. Science is not conducted in a social and intellectual
vacuum.

We need to distinguish cosmological speculation that parades as

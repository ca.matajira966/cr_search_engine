172 IS THE WORLD RUNNING DOWN?

state economy, and then they begin to preach such nonsense, we will
find the philosophical basis of Western liberty and Western prosperity
under attack. If voters take such speculation seriously, we will see
the collapse of the world economy. That might make Jeremy Rifkin
happy. It should not make Christians happy.

Christians should begin to think about social theory. They need
to think about the implications of such biblical doctrines as the Fall,
the curse of the ground, and the resurrection of Christ. They need to
think carefully about the relationships between obedience to God’s
covenantal law and external prosperity, including biological trans-
formations. They need to take seriously the biblical imperative of
self-government under law.

When they do, they will no longer take seriously Jeremy Rifkin.

Conclusion

Orthodox Christianity preaches the triumph of Jesus at Calvary.
But this triumph must be seen as three-fold: definitive, progressive,
and final. To deny the progressive nature of sanctification for the in-
dividual is to deny the possibility of personal spiritual maturity and
progress. To deny the progressive nature of sanctification for the
church is to deny the improvement of the creeds and to deny the
healing of society by the preaching of the gospel. To deny pro-
gressive sanctification socially is to deny the power of the gospel in
history. It is to affirm the progressive power of Satan’s followers in
history. It is to preach the historic defeat of Christians, the gospel,
and the Holy Spirit. This is the ultimate form of pessimism. It is the
pessimism that Satan wants Christians to believe above all other
kinds of pessimism.

This pessimism has deeply crippled the social vision of six-day
creationists, who should have been the pioneers of a new, anti-
hamanistic worldview. Because their pessimism regarding the future
has dominated their approach to science, they have focused their at-
tention on the Fall of Adam rather than the resurrection of Christ.
They have made the second law of thermodynamics their key argu-
ment, rather than the much better argument that the evolutionists
do not have sufficient time available to have made possible the evolu-
tion of the universe or the species through natural selection.

What we should expect, as creationists, is that God intends to
remove the cursed effects of the Fall as Christians mature in history.

Resurrection and Reconstruction 173

The cause-and-effect relationship between man’s ethics and nature
that brought on God’s curse—by cursing the second law of thermo-
dynamics — still exists. The progressive overcoming of sin in the lives
of a growing number of converts will have its effects in nature: a
progressive overcoming of the cursed aspects of the universe, in-
cluding the second law of thermodynamics.

When six-day creationists at last admit their mistake and begin
to rethink their apologetic strategy, they will begin to produce first-
rate social theorists from among their ranks.

In summary:

1. Christ did not surrender history at His resurrection.
2. The death of Christ opened up the inheritance of the world to
Christians.
3. The idea of progress is a biblical idea.
4. It affirms progressive linear history.
5. Govenantal faithfulness of Christians brings forth God's exter-
nal blessings in history.
6. This positive vision of the future has been stolen by various
humanist groups since the early 1700's.
7. Dallas Seminary’s John Walvoord says that our efforts to
reform society are futile,
8. Christ has been given all power on earth (Matt. 28:18).
9. One of the visible effects of this power will be the triumph of
the gospel in history: the postmillennial faith.
10. The victory of Christ in history involves a progressive con-
quest over the physical effects of God's curse.
11. Creation Scientists admit this with respect to the world beyond
the final judgment.
i2. They implicitly assume this partial triumph during the millen-
nial reign of Christ, but they never talk about it.
13. The postmillennialist sees this conquest over the curse as pro-
gressive.
14. This is a consequence of Christ’s resurrection and His em-
powering of the church in history.
15. Miracles testify to the incomplete reign of entropy.
16. Capitalism’s economic growth also testifies to the ability of
men to overcome entropy’s cursed effects.
17. Rifkin hates capitalism.
18. In voluntary trade, entropy’s cursed effects are overcome: both
people win.
19. Man’s total environment responds positively to a society’s
covenantal faithfulness.

176 IS THE WORLD RUNNING DOWN?

Van Til’s presuppositional apologetic method,’ and optimism about
the long-run earthly future. What is also needed is a new scientific
paradigm based on this new Reformation.

This new Reformation must be institutionally decentralized,
confident about the church’s role in history, orderly, and uncom-
promising. It must have its own agenda, its own blueprints for social
reconstruction,’ and its own financing. It must break with radical
apocalypticism, whether fundamentalist apocalypticism or New Age
apocalypticism. It must be based self-consciously on the biblical con-
cept of continuity in history.® It must be based on the biblical idea of
progress.? It must fight ideas that are incorrect with something much
better: a consistent biblical worldview.

Any concept of science that is self-consciously Christian must
begin with the Creator-creature distinction, Man is made in God’s
image, and therefore he can think God’s thoughts after Him.
Covenant-breaking man’s mind is ethically fallen, but covenant-
keeping people are given the mind of Christ (I Cor, 2:16). This in-
crease in the orderliness of man’s mind is the product of ethical
transformation through God’s grace. It has nothing to do with any
scientific law of entropy. This increase in clarity of vision is not
matched by some increase in confusion elsewhere in the universe.

The creation reflects the orderly character of God the Creator.
The perceived disorder is in part the product of man’s fallen mind
and in part the product of God’s curse of the world. But Christianity
must proclaim the historical reality of Chrisi’s resurrection and
ascension. This resurrection leads in history to the progressive
transformation and liberation of nature from God’s curse, just as
surely as the Fall of Adam led in history to a discontinuous transfor-

5. Cornelius Van Til, The Defense of the Faith (rev. ed.; Phillipsburg, New Jersey:
Presbyterian & Reformed, 1963); in Defense of Biblical Christianity, 6 vols. (Presbyter-
ian & Reformed, 1967-1976).

6. Roderick Campbell, Israel and the New Covenant (Tyler, Texas: Geneva Divinity
School Press, [1954] 198i); David Chilton, Paradise Restored: A Biblical Theology of
Dominion (Ft. Worth, Texas: Dominion Press, 1985); David Chilton, The Days of Ven-
geance: An Exposition of the Book of Revelation (Ft. Worth, Texas: Dominion Press,
1987).

7. See the Biblical Blueprints Series published by Dominion Press, Ft. Worth,
Texas.

8. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler,
Texas: Institute for Christian Economics, 1985), ch. 12.

9. Gary North, Deminion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987).

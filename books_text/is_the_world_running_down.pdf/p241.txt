The Disastrous Quest for Scientific Common Ground 205

They do not believe that the world’s openness upward to God will
have any permanent positive effects, at least not before a future mil-
Jennium in which Christ appears physically to rule the nations,
accompanied by resurrected saints who no longer are under the
curse of entropy, even though everyone else on earth is.

Conclusion

What we find is that both sides appeal to entropy, and both sides
also deny it. Both sides claim to be uniformitarians, and both sides
deny it. The Scientific Creationists want entropy to rule unchallenged,
except when miracles are involved. The Darwinians also want en-
tropy to rule, but they also want an exception —“sunshine evolution.”
But how did the sun get here? In terms of the evolutionists’ own
presuppositions, we need to ask: How did a supposedly impersonal,
undesigned cosmic order that is characterized by increasing random-
ness ever produce an orderly energy source (the sun) that in turn
created pockets of increasing biological order? How long did this pro-
cess take? What probability can be assigned that the sun’s rays made
life emerge from non-life? This is what Scientific Creationists keep
asking? They do not get straight answers.

Discovering an explanation for getting order out of disorder is the
top priority scientific problem for all evolutionary systems, whether
physical science or social science. Discovering a program or process
for preserving order is the second problem. How does society preserve
order if the entropy law is valid? By an appeal to God and His
miracles? By an appeal to sunlight and its temporary escape hatch?
Or as the mystics would have it, by an appeal to a pantheistic god who
is both totally immersed in and yet totally divorced from this world’s
reality, which they see as maya, or unreality?

Why all this technical discussion of cosmology? Because cosmol-
ogies have implications for every aspect of human thought and action.
We find that the doctrine of origins has implications for our doctrine of
finality, which in turn has implications for our doctrine of immediate ethics,
not to mention our doctrine of social reality. We find that creationists and
evolutionists debate questions of social theory by using arguments
remarkably similar to the arguments that they use against each other
in the field of cosmology. This book is concerned with social theory, so
I was forced to devote part of it to cosmology.

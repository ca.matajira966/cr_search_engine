232 IS THE WORLD RUNNING DOWN?

ways, lock themselves in their churches, and ignore political matters
that simply “do not concern them.” It turns out that “Christian poli-
tical concern” does not automatically mean pamphleteering for
Franklin Roosevelt's New Deal. This revelation has shocked the
theological liberals.

Church, State, and Society

The theological and political liberals have generally adopted
some version of humanism. Humanism has, in turn generally
adopted some version of statism. The State, as the most powerful of
the institutions of man, and by far the most centralized, has been re-
garded as the agency of salvation. The messianic State has gained its
faithful worshippers in the pews of liberal churches, or at least in the
pulpits. The liberals believe in salvation by law— humanist law.
They believe (or have believed until recently) fervently in the benefi-
cial nature of social legislation. Politics has been the religion of humanism
since the days of the tower of Babel.

The liberals have tended to propound solutions in terms of State
power. This means that they define the problems of life in terms of
politics. They regard political solutions as the solutions. This
perspective is what has traditionally distinguished liberals from con-
servatives. Edmund Burke, in his Reflections an the Revolution in France
(1790), provided modern conservatism with its statement of faith.
Not politics, but tradition; not social upheaval, but social stability;
not the rule of politicians, but the rule of law-abiding leaders in
many institutions: here was Burke’s manifesto. Society is not to be
subsumed under the State. The State is not society. The State is
simply one aspect of society, namely, the political. It is not a monop-
oly of authority. Churches, families, voluntary associations of all
kinds, local civil governments, educational institutions, and numer-
ous other institutions also have lawful authority. Men are not simply
members of the State; they are members of many organizations, and
they have multiple loyalties and responsibilities. Burke’s perspective
was generally Christian. The horrors of the French Revolution after
1792 had been predicted by Burke, and the Christian West finally

18. R. J. Rushdoony, “The Modern Priestly State: The Sociology of Justification
by Law,” in Politics of Guilt and Pity (Fairfax, Virginia: Thoburn Press, [1970] 1978),
Pt. IV, ch. 2.

19. R. J. Rushdoony, “The Society of Satan” (1964), Biblical Economics Today, U1
(Oct./Nov, 1979). Published by the Institute for Christian Economics.

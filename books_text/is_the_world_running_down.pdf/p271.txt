Comprehensive Redemption: A Theology for Social Action 235

consistent with their philosophy of ultimate relativism, in which no
final truth is possible. They have admitted that they, too, are pro-
moting positions that must alienate others. They have steadily aban-
doned natural law theory and other forms of universalism. But with-
out some universally agreed-upon principles, there can be no neutral
universe of discourse.

The Dying Myth of Neutrality

One of the best examples of a now-dead faith in human reason is
found in a very popular book, How to Read a Book, by Mortimer
Adler. It first appeared in 1939, and it has gone through at least 40
printings. As a book on how to read critically, it is excellent, As a
hook on philosophy, it is naive, It is based on a nineteenth-century
view of human reason. The relativism of men like Karl] Mannheim,®
or the influential book by Thomas Kuhn, The Structure of Scientific
Revolutions (1970), cannot be reconciled with the naive rationalism of
Adler. Here is Adler’s faith:

One is hopeless about the fruitfulness of discussion if one does not rec-
ognize that all rational men can agree. Note that I said “can agree.” I did
not say all rational men do agree. ] am saying that even when they do not
agree, they can. And the point I trying to make is that disagreement is futile
agitation unless it is undertaken with the hope that it may lead to the resolu-
tion of an issue.

These two facts, that men do disagree and can agree, arise from the
complexity of human nature. Men are rational animals, Their rationality is
the source of their power to agree. Their animality, and the imperfections
of their reason which it entails, is the cause of most of the disagreements
that occur. They are creatures of passion and prejudice. The language they
must use to communicate is an imperfect medium, clouded by emotion and
colored by interest as well as inadequately transparent for thought. Yet to
the extent that men are rational, these obstacles to their understanding one
another can be overcome. The sort of disagreement which is only apparent,
resulting from misunderstanding, is certainly curable.

There is, of course, another sort of disagreement, which is due to in-
equalities of knowledge. The ignorant often foolishly disagree with the learned
about matters exceeding their knowledge. The more learned, however,

25. Karl Mannheim, Ideology and Utopia: An Introduction to the Sociology of Knowledge
(New York: Harvest, [1936]).

26. Thomas Kuhn, The Structure of Scientific Revolutions (2nd ed.; Chicago: Univer-
sity of Chicago Press, 1970); 1. Lakatos and A. Musgrave (cds.), Criticism and the
Growth of Knowledge (New York: Cambridge University Press, 1970)

238 IS THE WORLD RUNNING DOWN?

Whose Reforms?

What is the relevance of all this for social reform? Simple: all
reforms are either consistent with the Bible or inconsistent with it.
The Bible is our point of reference, our final court of appeal. The
Bible, not natural law or natural reason, is the basis of evaluating
the applicability or validity of any proposed social reform. When we
lobby to have a law passed, we need not be embarrassed that it is a
specifically Christian law — a law inconsistent with Marxist ideology,
Islamic culture, or the latest findings of a Presidential commission.
We must not allow ourselves to be paralyzed by doubts regarding the
supposed unfairness of a particular law — “unfair law” being defined
as any law that might restrain the self-proclaimed autonomy of man.
There are always valid debates about timing, or the cost of enforce-
ment, or the strategy of getting a law passed, but questions of fair-
ness must not be decided in terms of humanistic law or humanistic
assertions that a particular law “mixes religion and State.” The more
relevant question to be asked of any proposed law is this one: Whose
religion does it promote?

Should the institutional church get involved in politics? The
more relevant question is this: Can any consistent church avoid pol-
itics? Can it avoid discussing the decisions that men, including its
members, make in life? Can a church stay silent in the face of legal-
ized abortion? On a key issue like this, you would think that the
most hardened “separator” would capitulate, but the vast majority of
churches in any city are publicly unconcerned about abortion. They
are not identified as pro-abortion or anti-abortion churches. They do
not preach on the topic. They do not encourage members to get
politically involved in the war against abortion. They stay silent.
They remain impotent. They do not speak up when unborn children
are aborted, and they remain culturally impotent—a fitting punish-
ment, given the nature of the crime that they do not actively oppose.

The problem is not that of remaining outside of politics. If evil is
entrenched in the land, all institutions that do not actively oppose it
are part of that evil process. They become agencies for smoothing
over the evil. Such churches give hope to men, calm the fears of
men, and promote the blindness of men. They are important agents
for the humanists. They perform their task of neutralization and
castration quite well, and the humanists continue to reward such in-
stitutions by allowing them to retain their tax exemption,

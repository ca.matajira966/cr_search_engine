Are Postmillennialists Accomplices of the New Age Movement? 269

their poorly read followers. They are not ignorant men; they can
read. They simply prefer to mislead their followers deliberately, It is
not an intellectual defect on their part; it is a moral defect. Now that
their followers have at last begun to read Christian books and news-
letters written by people outside the dispensational ghetto, dispensa-
tional leaders are in deep, deep trouble. Their troops are deserting.
(See Appendix E.)

Rifkin’s Allies

We have seen in this book that a self-conscious liberal and New
Age politician has used the premillennialists’ own pessimistic escha-
tology, as well as the Creation Science movement’s appeal to the sec-
ond law of thermodynamics, in order to create a working alliance
between Christians and the New Age movement. Rifkin’s explicit
pessimism concerning the future has led him to promote an attitude
of “batten down the hatches,” raise taxes, and create a controlled,
government-directed, zero-growth economy. In short, he has seen
the eschatological weaknesses of traditional premillennialism (evan-
gelicalism and fundamentalism), and he has used these weaknesses
as a kind of intellectual jiu-jitsu. The evangelicals have been silent
about Rifkin for over seven years.

This is why it is annoying, to say the least, to read Walvoord’s at-
tack on postmillennialism as an ally of evolutionary liberalism:

During the last part of the nineteenth century, evolution emerged as an
explanation for why things were getting better. In those days, prophecy
conferences included postmils, amils, and premils, but it became a battle
between the premil view and the evolutionary view that seemed to fit post-
millennialism. So premillennialism became a battle against the evolution-
ist, which ended up as a battle between fundamentalism and liberalism, I’m
afraid the postmillennial position is stiil closely associated with evolution
and liberalism (8-I).

He may be “afraid” of this, but anyone who has even a smat-
tering of knowledge about twentieth-century postmillennialism
needn't be afraid in the slightest. Here is the man who was president
for thirty years of a seminary that has never offered a course defend-
ing the six-literal-day creation. He says that postmillennialism
favors evolutionism, yet it was R. J. Rushdoony, a postmillennialist,
who got Morris and Whitcomb’s Genesis Flood into print with Presby-
terian & Reformed Publishers after dispensationalist Moody Press

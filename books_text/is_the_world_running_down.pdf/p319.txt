The Division within Dispensationalism 283

coward a new dark age. The frightful uprisings among races, the almost unbe-
lievable conquests of Communism, and the growing antireligious philosophy
throughout the world, all spell out the fact that doom is certain. I can see no
bright prospects, through the efforts of man, for the earth and its inhabitants.?

No Solutions; Therefore, No Responsibilities

This utter pessimism concerning the earthly future of the institu-
tional church and Christian civilization is what lies behind the tradi-
tional premillennialists’ lack of any systematic social policies. They
think it is a waste of their time to think about such matters, since
they believe that the Christians will never be in a position to imple-
ment them, Peter Lalonde’s comments are representative of the
mindset of traditional dispensationalism:

Now, P'm thinking about a conversation I had here with a young Chris-
tian brother the other day. And he’s nnuch involved with the Reconstruction
and Christian conservative movement in the U.S. And he’s talking to me in
terms of political change, social change, economic reform—in fact, he was
talking to me in terms of revamping the entire Federal Reserve System in
the United States, And I think he had a valid point, that the Federal
Reserve is one of the most corrupt institutions on the planet, But it came to
me, and the point he was making was this, he said: “Look, we’re both sin-
cere Christians. You're looking for a rapture, and I'm not. If I'm wrong,” he
said, “we will both go in the rapture anyway, but if you're wrong, I'm the
only one of the two of us who has worked to set up the necessary institutions
to run the world as God would have us do.” In other words, what he is say-
ing is, “My belief has nothing to lose; yours does.”

But there’s a couple problems with this, Ir’s not a case of a coin toss, of
who’s right and wrong; it’s what the Word of God says. You just can’t do it
as a 50-50 option, because I don’t believe the Word leaves that option. And
the Word certainly does not teach anything about setting up institutions,
and that’s something we're going to talk more about. But this leads us to the
central question. He’s saying, in his view, his view has nothing to lose,
where ours does. But it does, because the question is what do we do in the
meantime? It’s a question, “Do you polish brass on a sinking ship?” And if
they’re working on setting up new institutions, instead of going out and
winning the lost for Christ, then they’re wasting the most valuable time on
the planet earth right now, and that is the serious problem in his thinking.*

3, Lehman Strauss, “Our Only Hope,” Bibliotheca Sacra, Vol. 120 (April/June
1963), p. 154.

4, Dominion: A Dangerous New Theology, Tape #1 of the three-tape series, Daminion:
The Word and Naw World Order, distributed by the Omega-Letter, Ontario, Canada, 1986.

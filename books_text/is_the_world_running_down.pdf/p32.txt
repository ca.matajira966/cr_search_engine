xxx 18 THE WORLD RUNNING DOWN?

continuation of the strategy outlined by William Peltz, the Midwest
regional coordinator of the Peoples Bicentennial Commission. At a
meeting in Ann Arbor, Peltz argued that conservative Christians can
be turned into promoters of revolutionary politics if you can show
them that the Bible teaches revolution.

Interestingly, he cited Leviticus 25, the chapter that contains the
Jubilee land laws, which required that the ownership of the land of
Israel be returned to the original families every 50 years. This has
subsequently become a popular theme of numerous radical Chris-
tians, including Ron Sider and Sojourners magazine. It has also be-
come a theme in certain fundamentalist groups. They have not
understood that the Jubilee was an aspect of military conquest, an
economic incentive to fight that was given to each Hebrew family
before Israel invaded Canaan.* They also have not recognized that
the Jubilee was fulfilled in principle by Jesus (Luke 4) and abolished
historically when Israel as a nation ceased to exist.4? But most of all,
they have not bothered to tell their followers that if Leviticus 25 is
still morally and legally binding, then lifetime slavery is still morally
and legally valid, for it is only in Leviticus 25 that the Hebrews were
told that they could buy and enslave foreigners for life, and then en-
slave their heirs forever (Lev. 25:44-46). It is time to abandon Leviti-
cus 25 as the basis of social reform,

The Myth of Neutrality

This book deals in great detail with the writings of Jeremy Rifkin
that are aimed specifically at a Christian audience. But it is not sim-
ply to refute Rifkin that I write this book. It is to refute the earthly
pessimism that Rifkin’s thesis promotes. Unfortunately, Bible-
believing Christians have adopted a very similar view of the sup-
posed “natural” decline of this “natural” world, and this has led to a
similar debilitating outlook —a self-conscious effort on the part of
Christians to remove “the supernatural” from scientific discussion, at
least in the “preliminary” stages of discussion.*®

45. The Aliempt to Steal the Bicentennial, p. 36.

46. Gary North, “The Fulfilment of the Jubilee Year,” Biblical Economics Today, V1
(March/April 1983).

47, Ibid.

48. A goud example of such an approach is John N. Moore’s book, How ta Teach
ORIGINS (Without ACLU Interference) (Milford, Michigan: Mott Media, 1983). This
presumes that Christians are being employed by officially neutral but in fact God-
hating, humanist-operated schools, This perpetually incffective strategy of subver-
sion has yet to work, the latest defeat being the U.S. Supreme Court’s decision in
Edwards i. Aguillard (June 19, 1987). See Appendix B in this book.

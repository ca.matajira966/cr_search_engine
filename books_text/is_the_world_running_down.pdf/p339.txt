The Division within Dispensationalism 303

and God's law as the tool of dominion, and so have adopted postmil-
lennialism as a covenantally inescapable corollary.% The primary
issue for Christian Reconstructionists is ethics, not eschatology.

When we preach dominion, we do not narrow our focus, as fun-
damenialists have done for well over a century, to personal sins
alone. These can more readily be steadily overcome (progressive
sanctification). In any case, personal sins are easier to hide from the
public. They can also be hidden away in the subconscious by various
false theologies of perfectionism: the “higher life” type of thinking.3*
But when we talk about the responsibility of Christians to exercise
dominion culturally, our present visible failure cannot be denied. We
cannot cover it up. Thus, we face a decision: to get to work in terms
of biblical law, or to continue our retreat, either into nearly total
defeat culturally (amillennialism) or into a mental world of expec-
tant deliverance (pretribulational dispensationalism). In either case,
the result is the Christian ghetto. It is an ecclesiology of “a safe port
in the storm.” The end result is the Gulag archipelago, where power-
seeking humanists select our ghetto for us. (The first stage of enter-
ing the Gulag archipelago is sending your children to a public
school. Any Christian who willingly does this to his children has in
principle accepted the Gulag as a legitimate institution: compulsory
State re-education in terms of anti-Christian principles.)

A growing minority of Christians have at last begun to see where
Christian ghetto thinking leads, and where the ethics of “spiritual
victories only” leads. They have begun to understand that Christian
freedom must be defended, which means that Christian civilization
must be extended. We must go on the offensive culturally if we are to
avoid the Gulag. This means that we need future-oriented motiva-
tion and Old Testament institutional blueprints — biblical law. Both
have been denied by pietists for over a century. Alcoholics who have
sobered up eventually realize that it is not enough to get sober and
stay sober; they must do something with their sobriety. Christians
must recognize the same thing. It is not enough to seek personal
holiness; they must do something with that holiness. This belated
recognition of a world of God-assigned responsibility beyond per-
sonal behavior is at the heart of the transformation that is going on

38, Greg Bahnsen has tried to separate the two doctrines, but unsuccessfully, See
my rejoinder in North, Dominion and Common Grace: The Biblical Basis of Progress
(Tylex, Texas: Institute for Christian Economics, 1987), pp. 138-45.

39, Frank, Less Than Conguerors, ch. 4.

Abbey, Edward, 160
abortion, 238, 244-45, 284-85
Abraham, 277
Abraham, Larry, 288n
academic freedom, 213-18
Adam, ix, xxii, 125-26, 152
Adler, Mortimer, 235-36
agenda {control of), 295
agnosticism, 30
agreement, 235-36
AIDS, 83, 182, 302, 304
alcohol, 301
Alderson, Wayne, 314
Alice, 120
alternatives to welfare, 254
amateur theologians, 261, 287
amillennialism, 247-48, 263
anarchy, 226, 240
animism, 140
Antichrist, 123, 259, 287-68
antinomianism, 43, 170, 264, 289,
292-93, 300
apes, 21
apocalypticism, 176, 181-82
apologetics
defined, x (note)
creationism, xii, xvi, 189, 193
Darwinists’, xii
neutrality (false), xxxiii
shift in, xx
Van Til's, xxxiii, 195
Arendt, Hannah, 226
arguments, 236
“arsonist,” 257
asceticism, 159

INDEX

Aspect, Alain, 26
authority, 216, 263
autonomy
academic, 215
apologetics &, xxiii
cosmology, 171
Darwinism & New Age, xiv
“fairness,” 238
nature?, 18
power religion, 42
presupposition, xxxiti, 237
religion of, 42
scientific, 84
starting point, xi}
universe, 35, 171
see also closed box
Asimoy, Isaac, 6-7, 27, 53-54, 62, 73
Assyria, 251
astronomy, 40
atheism, 30, 59
automobile, 312

Babel, 232
Bahnsen, Greg, 220-23
Bailey, Alice A., 184
Bakker, Jim, 301-302
balance of nature, 147, 169
Barnhouse, Donald, 293
Baxter, Richard, 316
Bazarov, I. P., 59-60

beer, 247

Bell, John Stewart, 24-26
Bent, Henry, 51

Berkeley, Bishop, 30
Berkeley, Univ. of Calif., 26

329

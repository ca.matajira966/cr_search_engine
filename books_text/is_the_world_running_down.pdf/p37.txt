INTRODUCTION

Thy raiment waxed not old upon thee, netther did thy foot swell, these forty
years (Deut. 8:4).

And ye shall serve the LorD your God, and he shall bless thy bread, and
thy water; and I will take sickness away from the midst of thee. There
shall nothing cast their young, nor be barren, in thy land: the number of
thy days I will fulfil (Ex, 23:25-26).

The first law of thermodynamics states that the total of “matter-
energy” in the universe is constant throughout eternity, or at least
after the “Big Bang” (the evolutionists’ version of creation). The sec-
ond law of thermodynamics states that useful energy is either con-
stant (equilibrium conditions}! or decreasing (kinetic energy) in the
universe as a whole. Useful energy for the universe as a whole therefore
can never increase, (The underlying assumption of the second law is
that the universe itself is a closed system, something like a giant con-
tainer. Without this crucial assumption, the second law of thermo-
dynamics loses its status as a universal law.)

We read: “Thy raiment waxed not old upon thee, neither did thy
foot swell, these forty years” (Deut. 8:4). Thus, any consistent scienti-
Jic application of the second law of thermodynamics to this verse
would have to assert that God must have drawn energy from some
external source in order to produce this /ocal overcoming of entropy’s
effects. Perhaps God in some way rechanneled energy from the sun.
that otherwise would have been dissipated into space, or into the

1, A system is in equilibrium when its components are not moving in any partic-
ular direction, When a gas is in equilibrium in a container, its molecules are bounc-
ing randomly against the walls of the container. In theory, no heat from outside the
container passes through to the gas inside, and no heat from the gas inside passes
through the container’s walls to the outside. The random bouncing of the molecules
of the gas does nat produce measurable changes—~heat, pressure —in the gas, taken
as a unit,

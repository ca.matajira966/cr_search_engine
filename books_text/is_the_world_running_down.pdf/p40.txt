4 18 THE WORLD RUNNING DOWN?

stood as irrelevant to science, and probably a denial of science. The
reason for this rejection is clear enough: miracles could not be ex-
plained in terms of the categories of nineteenth-century science. The
universe was explained as a sort of giant machine that is governed
exclusively by mathematical laws. Miraculous biblical “discontinu-
ities,” if true, deny the universality of these hypothetical, unbreak-
able, mathematical, natural laws. Hence, in order to maintain their
faith in the world of natural science, ninetecnth-century scientists
(and most of their successors) rejected the idea of biblical miracles.
Typical of this outlook was a statement in 1910 by the American
social scientist and progressive educator, G. Stanley Hall:

We have largely evicted superstition from the physical universe, which
used to be the dumping ground of the miraculous. . . . But we have great
ground to rejoice that science is now advancing into this domain more
tapidly than ever before, and that the last few years have seen more pro-
gress than the century that preceded. The mysteries of our psychic being
are bound ere long to be cleared up. Every one of these ghostly phenomena
will be brought under the domain of law, The present recrudescence here of
ancient faiths in the supernatural is very interesting as a psychic atavism, as
the last flashing up of old psychoses soon to become extinct.*

A tiny number of modern humanist scientists might be willing to
accept the historical validity of some biblical miracles, but they
would reject the biblical explanation, namely, a sovereign God who
brought them to pass for His own purposes. These scientists are be-
lievers in the essential randomness of nature. “Anything is possible,”
they say.5 Anything except the sovereign God of the Bible who con-
trols all things in terms of His plan for the ages. That isn’t possible.

We therefore discover a clash: the overwhelming majority of that
minority of scientists who actually discuss epistemology (“what we
can know and how we can know it”) and cosmology (“origins and fate
of the universe”) vs. miracle-believing Christians who also discuss

4. G. Stanley Hall, “Introduction,” Amy Tanner, Studies in Spintism (New York:
Appleton, 1910), p. xxxii.

5, Lyall Watson writes: “Science no longer holds any absolute truths. Even the
discipline of physics, whose laws once went unchallenged, has had to submit to the
indignity of an Uncertainty Principle. In this climate of disbelief, we have begun to
doubt even fundamental propositions, and the old distinction between natural and
supernatural has become meaningless. I find this tremendously exciting.” Lyall
Watson, Supernature: A Natural History of the Supernatural (Garden City, New York:
Anchor Press/Doubleday, 1973), p. ix.

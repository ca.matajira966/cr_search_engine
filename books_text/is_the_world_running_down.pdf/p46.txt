10 I8 THE WORLD RUNNING DOWN?

Always, furthermore, entropy tends to increase. Everywhere in the
physical universe there is an inexorable downhill trend toward ultimate
complete randomness, utter meaninglessness, and absolute stillness.

This account of the second law of thermodynamics is not funda-
mentally different from what appears in many popular books written
by humanistic scientists. Certamly, Jeremy Rifkin’s interpretation of
the second law conforms to this interpretation. But the Scientific
Creationist always has an exception to the second law ready and
waiting: “except when God miraculously intervenes to overcome the
effects of entropy.” This exception is rejected by modern science.

We usually think of a miracle as a one-time intervention by God
in history. But what if the miracle of Christ's resurrection offers to
God’s people a way of steadily overcoming the cursed effects of the sec-
ond law of thermodynamics? What if Christians’ progressive (though
incomplete) conformity to God’s law through the empowering of the
Holy Spirit were to result in a partial overcoming of the historic
curse which God imposed on Adam and the earth? In short, why
must occasional and unpredictable miracles by God be the only way
to offset the world-deteriorating effects of entropy? Why can’t en-
tropy’s unwanted effects—death at an early age, for example—
be partially overcome through covenantal faithfulness (Ex. 20:12)?
Isn’t this possibility the primary potential social legacy of Christ's
resurrection?

What if the greatest miracle of all—Jesus Christ’s bodily resur-
rection from the dead—was only the first step in a new world order,
Meaning a God-transformed, Bible-based, gospel-inaugurated new world
order, in contrast to the humanists’ version of a central planning elite-
transformed, legislation-based, politics-inaugurated new world
order? If it was, then Christians have been given a momentous re-
sponsibility: to preach Christ’s gospel of comprehensive redemption.
(See Appendix C.) Because millions of Christians suffer from an in-
feriority complex in the face of temporarily triumphant humanism,
they resist the idea that they have been given such comprehensive re-
sponsibility. They therefore resist the idea that God intends that His
people work to establish His kingdom on earth in history. They
prefer historical pessimism to historical optimism, for there is vastly
more personal and institutional responsibility for Christians in a

21. Henry Morris, The Troubled Waters of Evolution (San Diego, California: C.L.P.
Publishers, [1974] 1980), p. 128.

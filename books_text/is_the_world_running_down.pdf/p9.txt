PREFACE

But if there be no resurrection of the dead, then is Christ not risen. and if
Christ be not risen, then is our preaching vain, and your faith is also vain.
Yea, and we are found false witnesses of God; because we have testified of
Ged that he raised up Christ: whom he raised not up, if so be that the dead
rise not. For if the dead rise not, then is not Christ raised: and if Christ be
not raised, your faith is vain; ye are yet in your sins (I Cor, 15:13-17),

This book asks and then attempts to answer one question: Which
is more important, Adam’s Fall or the bodily resurrection of Jesus
Christ? It seems like such a simple question for a Christian to
answer. The answer seems so easy. Obviously, the resurrection is
more important, now and in eternity. If there had been no resurrec-
tion of Christ, our faith would be vain. But this answer immediately
raises a second question: Which is more important, the effects of
Ghrist’s resurrection in history or the effects of Adam's Fall (God's
curse of the ground) in history? My answer to this corollary question
is going to make a lot of very dedicated Christians unhappy. I
answer that the effects of Christ’s resurrection are more important,
as time goes by, than the effects of Adam’s Fall. The implications of
this statement, if believed and put into daily practice, would revolu-
tionize the Christian world, In fact, they would revolutionize the en-
tire fallen world. I will go farther: the implications wil! revolutionize
the fallen world. Yet this is what most Christians categorically deny
today. They deny it because they have been taught, implicitly and
explicitly, that the effects of Adam’s Fall are overwhelmingly, in-
evitably more powerful in history than Christ’s resurrection. This
book is my answer to this denial,

Scientific Methodology and Eschatology

1 wish this book were not necessary. It will alienate some very
dedicated Christians who have devoted their careers to refuting the
fundamental world-and-life view of our age: Darwinian evolution.

ix

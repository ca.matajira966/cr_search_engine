84 Judges

strong position for defense against an enemy in the valley. This
is undoubtedly true. But what must be remembered in this con-
nection is that Barak with his ten thousand men ascending to the
top of Tabor put everything at stake. From Mount Tabor there
was no escape. Once on the mountain, Barak and his men had
no choice. They had to meet the enemy. They had to fight and
gain the victory, or die. On the other hand, there was the enemy.
They were undoubtedly strong in number, for Deborah speaks
of them as a multitude. Secondly, the men of Sisers were
veterans in battle, used to victory. Finally, they were well armed
and equipped with nine hundred chariots of iron. The two
forces therefore could stand no comparison. From a mere
human point of view it was impossible to expect that Barak
would gain the victory. How he would defeat the enemy was a
thing not to be seen. There was but one power that could sustain
Barak, but one strength in which he could proceed to Tabor: It
was the strength of faith in the name and the word of Jehovah.
Jehovah had spoken, and Jehovah would win the battle.”?

The Lord went before the army. It was really His battle; men’s
actions come afterward simply to mop up. Barak led the human
army. We see here what is pictured in fullest form in Revelation
19:11-16, the Greater Barak leading His army to victory. Barak
responded to the Word from Deborah with unmixed faith this
time, and every single soldier in Sisers’s army was slain. A total
victory, picturing the final victory of Christ and His saints over all
enemies. (After all, this is the famous battle of Megiddo, Jud.
5:19, which is the type of the great Battle of Ar-Megiddon.)

The Lord destroyed Sisers’s army. We are not told how here,
because the focus is on the fact of the victory itself. The Song of
Deborah explains that God brought a rainstorm that turned
the plains into mud, and grounded the chariots. Here the focus
is on the simple fact that God can stop chariots any time He
pleases. Sisers had to run away on foot. Now the contest was
more nearly “equal.”

Jael’s Response

17. Now Sisers fled away on foot to the tent of Jae] the wife
of Heber the Kenite, for there was peace between Jabin the king

2, Hoeksema, Era of the Judges, p. 88.

90 Judges

false names and with faked papers. They were not the first or
the last Christian preachers to deceive tyrants, either. If we have
to deceive and lie to bureaucrats in order to keep our churches
and schools running, we must do so freely and with relish, en-
joying the opportunity to fight for the Lord.

Summary and Conclusion
23, So God subdued on that day Jabin the king of Canaan
before the sons of Israel.
24. And the hand of the sons of Israel pressed heavier and
heavier upon Jabin the king of Canaan, until they had cut off
Jabin the king of Canaan.

It was God Who did the work. We must always give Him the
credit. He is our Deliverer.

It took a while, but Jabin and his culture were destroyed.
This verse points forward into the future, for as a matter of fact
the Canaanites did continue to live in the land of God, though
they never again rose up against Israel as they did under Jabin.
Zechariah 14:21 points to the time when all Canaanites will
finally be gone from the land, the house of God.

The Hebrew verb translated “cut off’ is the word used for
making a covenant and for cutting off the foreskin in circumci-
sion. Circumcision is a picture of castration, just as baptism by
sprinkling is a picture of drowning. The righteous are circum-
cised/sprinkled; the wicked are castrated/drowned (see Gal.
5:12). As we noted above, Satan/Sisers’s design was to capture
the bride and use her to raise up his own seed. Thus it is theolog-
ically fitting that the verb used to describe his destruction is the
verb that is used for castration.*

The evaluation of the Lord is the Song of Deborah, which
we analyze in the next chapter of this study.

4. I have discussed circumcision at length in my book, The Law of the
Covenant; An Exposition of Exodus 21-23 (Tyler, TX: Institute for Christian
Economics, 1984), pp. 78f.; 243ff. A particularly graphic use of “cut off” is
Psalm 58:7, where it refers to arrows which have the heads cut off the ends; the
parallel to castration is obvious.

The Song of Deborah 93

Stanza 1

This section begins with Deborah’s praising the Lord be-
cause the people were willing to volunteer and fight. This was a
confirmation of her own ministry. She next addresses the kings
of the nations, informing them that the Lord is the true God of
the storm, as shown by the storm at Mount Sinai. Then she
describes the situation in Israel: The people had lost control of
the highways, because they had chosen new gods and the Lord
had chastised them; but God had raised up a mother in Israel,
and the people had repented. They had volunteered to destroy
the enemy, and this is the occasion for this Song, which will be
sung at all the watering places in Israel.

2. That long locks of hair hung loose in Israel,
That the people volunteered,
Bless the Lorp!

This is a literal translation. The reference is to the Nazirite
vow, the details of which are found in Numbers 6, and we shall
up take those details when we get to Samson. All of Israel were a
nation of priests, and it is the priests who prosecute holy war.
God Himself had established a parallel between the war camp
and the Tabernacle, both holy places, as can be seen in Deuter-
onomy 23:9-14 (discussed on pages 64ff.) and from the fact that
the number five (the number of military organization) is so
prominent in the architectural design of the house of God. Just
as the people were to avoid sexual relations in the special pres-
ence of the true Divine Husband of Israel (Ex. 19:15), so they
were to avoid sexual relations during holy war (2 Sam, 11:11).
During holy war, the men took the Nazirite vow not to drink
wine or eat grapes or raisins, sacrificing the legitimate pleasures
of common life in order to make time for a temporary special
task. They also vowed to let their hair grow long. This long hair,
a sign of glory and strength, was then dedicated to the Lord at
the end of the vow.

Normally the text of Scripture does not call special attention
to the wartime Nazirite vow. Here in Judges 5:2 attention is
directed to it because of the priestly nature of the entire nar-
rative (the battle was led by a Levite; it involved an extension of
sanctuary; etc.).

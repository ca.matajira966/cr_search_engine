The Song of Deborah 97

war in the gates. And accordingly, before there can be peace
through victory, there must be repentance and a retum to the
“old” God, the true God of Israel, the God Who marched from
Seir (v. 4). Since the people had chosen the gods of the Ca-
naanites, God put them under the culture of the Canaanites.

Verse 8b tells us that although the men of Israel did have a
few weapons (which they used in this battle), yet they dared not
let them be seen. The tyrant’s eyes were everywhere. Just as
modern Baalists want gun registration laws (instead of laws
punishing criminals), the ancient Baalists had shield control
laws, and spear registration laws.

I interpret the number 40,000 to be four times 10,000. As
noted in the previous chapter, a myriad is the number of the
Lord’s host. Four is the number of the land, with its four cor-
ners. Thus, 40,000 is a number representing the Lord’s host as
guardians of the whole land.

9, My heart is [goes out] to the commanders of Israel,
The volunteers among the people;
Bless the Lorp !
10. You who ride on white donkeys, you who sit on carpets,
And you who travel on the road — sing! [or, consider this:]
ha. The voice of the minstrels at the watering places,
There they shall recount the righteous deeds of the Lorp,
lib. The righteous deeds of His peasantry in Israel. [or, The
righteous acts of His iron in Israel.1
Then the people of the Lorp went down to the gates.

All the various classes of Israelite society volunteered to
fight, and all are exhorted to sing the Song of Deborah. (Is it
ever sung in your church?) Verse 9 points out that both leaders
and people volunteered.

Verse 10 refers to the two classes as they travel on the (newly-
restored) highways. The wealthy would ride on white donkeys
or sit On fine rugs (in a carriage, perhaps, or on a donkey), while
the poorer members of society would walk. Both groups are
called upon to do something. The most likely translation is
“sing,” though the word may mean “consider.” If they are being
told to sing, then we find that all are to join in the Song of
Deborah. Those who fight for God, also sing to Him. If the
proper meaning is “consider,” then both groups are being told to

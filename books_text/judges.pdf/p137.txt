120 Judges

put the broth in a pot, and brought them out to Him under the
oak, and presented them.

20. And the angel of God said to him, “Take the meat and
the unleavened bread and lay them on this rock, and pour out
the broth.” And he did so,

21. Then the angel of the Lorp put out the end of the staff
that was in his hand and touched the meat and the unleavened
bread; and the fire sprang up from the rock and consumed the
meat and the unleavened bread. Then the angel of the Lorp
vanished from his sight.

22. When Gideon saw that he was the angel of the LoRD,
Gideon said, “Alas, O Master LoRD! For now I have seen the
angel of the Lorn face to face.”

23. And the Lor said to him, “Peace to you, do not fear;
you shall not die.

24, Then Gideon built an altar thereto the Lorp and named
it The Lorp is Peace. To this day it is still in Ophrah of the
Abiezrites.

God graciously consents to Gideon’s request, for it was
made not in unbelief, but in weak faith. “Lord, I believe; help
me in my unbelief !” was his prayer (see Mark 9:24), It is not a
bare sign Gideon wants. We do not enjoy a meal if someone we
intensely dislike is present with us. In the Orient, the meal is
eaten only with family and friends. To share salt and a meal with
someone is to enter a covenant of communion with them. This
is part of the meaning of the Lord’s Supper. Gideon knows that
if God has restored fellowship with His people, then He will
share a meal with them.

An ephah of flour is no little amount, for an ephah was a
vessel large enough to hold a person (Zech. 5:7). Gideon made a
lot of bread. Considering that bread was hard to come by, in
that one could not make much flour threshing in a winepress,
Gideon’s gift was quite generous. God will consume all of it in
fire, and this will be somewhat of a test for Gideon’s faith. It
was unleavened, the bread of the Exodus. Just as the old leaven
of Egypt was not brought to the land of God, but new leaven
was found there, so Gideon refuses to use the old sinful leaven,
looking to God to insert a new leaven of the Spirit into the
dough of humanity.?

2, On leaven, see the discussion in my book The Law of the Covenant, pp.
186ff.

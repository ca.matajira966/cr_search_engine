Gideon: God’s War Against Baal 135

God suggests to Gideon that he go to the Midianite camp,
and take another man along for moral support, to see how God
has prepared the way for their victory.

Verse 12 piles up imagery to impress on our minds just how
awesome was the host of God’s enemies. Surely this vast sea of
trained warriors knows no fear! It is Gideon who must be afraid.

The barley loaf was the bread of the poor in Israel. As a
result of seven years of invasions, all Israel was poor. The in-
vaders took all the wheat, leaving only barley for the Israelites
to eat. The loaf of barley bread, then, clearly symbolizes Israel.
(And see Lev, 23:10f. + John 20:22, and Lev. 23:15-18 + Acts 2;
and 1 Cor, 10:17.) It is a round loaf, which rolls aggressively into
the camp of Midian. This symbolizes the fact that Israel will
launch the attack.

The tent symbolizes the Midianite host. The fact that it is the
tent rather than @ tent that is spoken of indicates that it is the
commander’s headquarters that is in view. This tent is struck by
the Israelite barley loaf, and turned completely upside down. It
is hard to imagine a tent’s being tumed completely upside down,
but in a dream anything can happen. This clearly means that the
fortunes of the Midianites will be inverted, reversed. The tent
lies flat, abandoned.

The interpretation of the dream, however, is what is most
amazing. Instantly the friend of the dreamer jumps to the con-
clusion that the dream refers to Gideon. Amazing! Who would
have thought that these Midianites had ever even heard of Gid-
eon, let alone know his father’s name? Even more, they obvi-
ously are terrified of Gideon! How could this have come about?

It could only have come about through God’s interference
and initiative. Here we see the other side of God’s gracious i
atives to Israel and Gideon. Here we see God’s wrathful initia-
tive against His enemies, as He acts to strike terror into their
hearts. Gideon and Israel are being delivered from fear, while
Midian is being delivered unto fear. The army of Midian has
heard reports about Gideon, a mysterious man who has suddenly
arisen out of nowhere and who has organized an army over-
night. They respond to this news with an irrational dread. After
all, humanly speaking they have no real cause for alarm: No
matter who this Gideon is, he could not possibly defeat 135,000
men, God, however, is at work.

 

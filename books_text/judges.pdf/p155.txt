138 Judges

were their own comrades, returning from watch. Thus, they
attacked each other.

4, The trumpets and noise and fire would stampede the
camels, causing havoc and killing men.

5. The name of Gideon would strike fear into the hearts of
all who had heard of him.

All the same, we must not speak humanly about this opera-
tion. What we read is what Israel also saw at the Red Sea,
“Stand fast and behold the deliverance of the Lorp which He
will accomplish for you today” (Ex. 14:13 ff.). It was the Lord
Who gave this plan to Gideon, and it was the Lord Who made it
work. It was His psychological operation.

The elements employed show that this was a human imaging
of the coming of God’s glory cloud. The glory cloud is the envi-
ronment around God’s chariot throne, and consists of His host.
We get to see into that cloud in the book of Revelation. When
the cloud appears, it is regularly accompanied by such
phenomena as light (lightning flashing), trumpet sounds, and
the shouts of a multitude. All these elements are here present,
and indicate that this human army is imaging the host of the
Lord.§

First they set down the jars and blew the trumpets. Then,
shattering the jars, they raised the torches in their left hands,
and kept blowing the trumpets and shouting “A sword for the
Lorp and for His Messiah.” The elements of this action are not
unique. On the contrary, the components of this action are
found at other places in God’s plan of history as well. What we
need to do is identify the principles at work in this attack, and
see how these principles continue to operate throughout history.
This is not “spiritualizing,” for we are not saying that there is no
literal meaning to the events, but we are dealing with the
underlying principles. What we have here is nothing less than a
picture of the gospel.

What do we see? A sleeping world is shattered by a trumpet
of judgment, a shining light, and the proclamation of a specific
message about a sword, God, and the Messiah.

1. The sleeping world is a common enough image in Scrip-

6, On the glory cloud and the phenomena which attend it, see Meredith G.
Kline, images of the Spirit (Grand ‘Rapids: Baker, 1980), especially chapter 4.

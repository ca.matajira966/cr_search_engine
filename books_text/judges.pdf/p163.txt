146 Judges

Judgment: Zebah and Zalmunna

18, Then he said to Zebah and Zalmunna, “What kind of
men were they whom you killed at Tabor?” And they said, “They
were like you, each one like the form of the sons of a king.”

19. And he said, “They were my brothers, the sons of my
mother. As the Lorp lives, if only you had let them live, I would
not kill you.”

20. So he said to Jether his first-bom, “Rise, kill them.” But
the youth did not draw his sword, for he was afraid, because he
was still a youth.

21. Then Zebah and Zalmunna said, “Rise up yourself, and
fall on us; for as the man, so is his strength.” So Gideon arose
and killed Zebah and Zalmunna, and took the crescent or-
naments which were on their camels’ necks,

What happens here is ambiguous. Possibly Gideon does
nothing wrong here, but more likely this paragraph indicates a
lapse of true obedience. Let us look at the problem.

Israel’s wars were supposed to be holy wars, against God’s
enemies. Deuteronomy 20:13 specifies that all the men of the
enemy are to be killed. Now, however, suddenly it seems as if
Gideon is treating it as a personal matter. There is nothing
wrong with his asking about his brothers, but there seems to be
something wrong indeed with the statement, “if only you had let
them live, I would not kill you.”

Before condemning Gideon, let us try to put the best possi-
ble construction on his action. Possibly he already knew the an-
swer to his question. Thus, possibly the entire conversation was
designed to make a symbolic or theological point: “As the
anointed one of Israel, it is my task to be the blood avenger for
my brethren. What you have done to Israel as a whole, you have
done in particular to my own brethren; and what I do to you to
avenge the blood of my brethren, is what God does to you to
avenge all His children. If you had left my family alone, you
would also have left all God’s family alone. But when you
attacked my family, that is part and parcel of attacking God’s
family, because as the anointed one, I am closely identified with
the Lord as His agent. Thus, my personal vengeance is also the
Lord’s vengeance.” If we put this positive construction on it, we
see that Gideon’s avenging his family is parallel to Christ’s

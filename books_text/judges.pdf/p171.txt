154 Judges

peculiar form of Baalism mentioned here is “Baal-Berith,” the
Baal of the Covenant. Here we see a mixture of true religion, the
Covenant that God made with His people, with the false idol-
airy of Baalism. This is a peculiarly Israelite form of Baalism,
and so God sells His people into bondage to a segment of Israel-
ite society that epitomizes this half-breed religion. God delivers
Israel this time not by raising up a judge, but by removing His
restraining arm and letting evil self-destruct.

Gideon’s Polygamy

8:29. Then Jerubbaal the son of Joash went and lived in his
own house.

30. Now Gideon had 70 sons who came from his loins, for
he had many wives.

31. And his concubine who was in Shechem also bore him a
son, and he appointed his name Abimelech [My Father is King].

32. And Gideon the son of Joash died at a ripe old age and
was buried in the tomb of his father Joash, in Ophrah of the
Abiezrites.

Gideon is called Jerubbaal in verse 29, because he did not
live in a palace but in his own house, thus eschewing the Baal-
istic tendency to become a statist king. At this point, Gideon
was acting as a Baal Fighter.

The name Gideon is used in verse 30, because here we see the
“natural man” in him acting up. The Biblical position is always
monogamy, because man is to image God in his life, being the
very image of God by creation. The Lord is monogamous; His
bride is the Church, and He has no other. If a man does not
stick with one wife, he does not properly image the Lord in his
life. Since the essence of ethics is human conformity to the very
character of God, any failure to image forth that character is
sin,

Polygamy is forbidden in Leviticus 18:18, which says “You
shall not marry a woman in addition to her sister, to be a rival
while she is alive, to uncover her nakedness.” If Jacob was out
of line marrying two sisters, then surely so also would any other
Israelite. Moreover, any second wife would be a rival (1 Sam. 1),
and any second marriage would expose the first to shame (un-
cover nakedness) because it would advertise to the world that

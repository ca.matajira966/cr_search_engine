156 Judges

called Jerubbaal here!) gave to his son: Abimelech. It means
“My Father is King.” If the boy’s mother had been the one to
give him this name, it would be understandable. That Gideon
gave it shows that his heart had been ensnared to some degree
by the desire to rule over men as a potentate. The ephod had
snared him, and now humanistic kingship had snared him. No
father can harbor such desires without his sons picking it up,
and Abimelech acts out in life what his father had only dreamt
of in his weaker moments. (We see the same principle in the life
of David. David’s several wives become Solomon’s multitude.
David’s taking of Bathsheba becomes Amnon’s rape of Tamar.
Like father, like son.)

As a “youth” Gideon had been presented to us as one who
provided the food of kings, bread and wine, to God’s people.
He was a true servant, a man of real humility, oriented toward
serving others and ruling them in that fashion. Now, however,
he has forgotten this to an extent, and has begun to lord it over
other men to some degree, however slight. Such is the tendency
of the human heart. Let each of us pray that if God gives us do-
minion, we will not lose our servant hearts.

Still, the phrase “Gideon the son of Joash died at a ripe old
age” indicates that he received the blessing of the Lord, despite
his sins and failings. This is encouragement to us. Gideon was
blessed not because he was a perfect man, but because Christ
was the perfect Man in his place.

Israel’s Ingratitude

33. Then it came about, as soon as Gideon was dead, that
the sons of Israel again played the harlot with the Baals, and
made Baal-Berith [The Baal of the Covenant] their god.

34. Thus the sons of Israel did not remember the Lorn their
God, who had delivered them from the hands of all their
enemies on every side;

35. Nor did they show kindness to the household of Jerub-
baal (Gideon), in accord with all the good that he had done to
Israel.

Despite his flaws, Gideon must have restrained Baalism
effectively. After his death, Israel went a-whoring after Baal
again, openly. Baal-Berith, the Baal of the Covenant, was a syn-

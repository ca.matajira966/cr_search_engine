170 Judges

the house of their god, and ate and drank and cursed Abimelech.

28. Then Gaal the son of Ebed said, “Who is Abimelech,
and who is Shechem, that we should serve him? Is he not the son
of Jerubbaal [the Baal Fighter]? And is Zebul not his lieutenant?
Serve the men of Hamor the father of Shechem; but why should
we serve him?

29. “And who will give this people into my hand? Then I
would remove Abimelech!” And he said to Abimelech, “Increase
your army, and come out .“

Abimelech was a halfbreed, and his religion was a halfbreed
religion combining symbols of the true faith with the philosophy
of Baalism to form the religion of Baal-Berith, the Baal of the
Covenant. The man who now appears on the scene is a full-
blooded Canaanite, who advocates returning to whole-hearted
Baalism and rejecting all Israelite influence. The English ver-
sions call him Gaal the son of Ebed, but “ebed” is simply the
Hebrew word for “slave.” Gaal means “Loathsome” in Hebrew.
It may have meant something else in the original Canaanite
tongue, or it maybe a name affixed to him by the writer of this
passage. Just as God gives His people new names for the better,
so He can give His enemies new names for the worse. At any
rate, it is clear that Loathsome Slave-son is no good whatsoever,
and that he is a full Canaanite (slave).

Gaal’s “brothers” came with him. This would include
relatives, and probably his whole motorcycle gang of followers.
He was already a big man, and the Shechemites turned to him to
deliver them from their wonderful, marvelous king, Abimelech.

Some type of harvest festival, a perversion of the Feast of
, Tabernacles, is going on. It is held in the temple of Baai-Berith.
This is the same place where the Shechemites had originally
covenanted with Abimelech. Now they covenant with
Loathsome in the same place.

Gaal uses the very same argument against Abimelech that
Abimelech had used against his seventy brothers. He notes that
Abimelech is the son of Gideon Baal Fighter. He encourages
Shechem to cast out all remnants of Israelite culture and return
wholly to the original Canaanite culture of the city. The city had
been founded by Hamor, who named it for his son Shechem
(Gen. 34:2), Abimelech was king over “Israel” (v. 22), though
this probably only really means the area around Shechem. Zebul

Abimelech: Israet’s First King 171

was his vassal in charge in Shechem. To paraphrase verse 28,
which is somewhat obscure: Then Gaal the son of a Canaanite
slave said: “Who is this Abimelech? Is he not the son of Jerub-
baal, the Baal Fighter? Why should we Baalists serve him? And
who is Shechem, that they should serve Zebul, Abimelech’s
lieutenant? The Shechemites should serve someone who stands
in the honorable line of Hamor and Shechem, true Canaanites.
Why should we serve Abimelech?”

Gaal boasts that if the people were under his authority, and
would join the Hell’s Angels under him, he would remove
Abimelech. In the drunkenness of the feast (v. 27), he calls on
Abimelech to come out and fight.

The Doom Implemented: Gaal

36, And when Zebul the ruler of the city heard the word of
Gaal the son of Ebed, his anger burned.

31. And he sent messengers to Abimelech privately, saying,
“Behold, Gaal the son of Ebed and his brothers have come to
Shechem; and behold, they are stirring up the city against you.

32. “Now therefore, arise by night, you and the people who
are with you, and lie in wait in the field.

33. “And it shall come about in the morning, as soon as the
sun is up, that you shall rise early and rush upon the city; and
behold, when he and the people who are with him come out
against you, you shall do to them whatever you are able [what
your hand finds to dol.”

34. So Abimelech and all the people with him arose by night
and lay in wait against Shechem in four heads [companies].

35. Now Gaal the son of Ebed went out and stood in the en-
trance of the city gate; and Abimelech and the people who were
with him arose from the ambush. .

36. And when Gaal saw the people, he said to Zebul, “Look,
people are coming down from the tops of the mountains.” But
Zebul said to him, “You are seeing the shadow of the mountains
as if they were men.”

37. And Gaal spoke again and said, “Behold, people are
coming down from the highest part of the land, and one head
[company] comes by the way of The Diviner’s Oak.”

38. Then Zebul said to him, “Where is your mouth now,
with which you said, Who is Abimelech that we should serve
him?’ Is this not the people whom you despised? Go out now
and fight with them!”

172 Judges

39. So Gaal went out before the leaders of Shechem and
fought with Abimelech.

40, And Abimelech chased him, and he fled before him; and
many fell wounded up to the entrance of the gate.

41, Then Abimelech remained at Arumah, but Zebul drove
out Gaal and his brothers so that they could not remain in
Shechem.

Zebul found out about Gaal that very night. His advice was
to strike immediately, before Gaal could create a large fighting
force, and while Gaal and his followers were still hungover from
their all-night drunk. Even though Abimelech is himself an evil
man, we still seethe Biblical way of thinking, which puts the vic-
tory at sunrise.

Abimelech divided his forces into four groups (with Zebul in
the city making a fifth column!), four being the number of do-
minion over the land. Gad’s confusion of Abimelech with the
mountain is a humorous twist on the common imagery of the an-
cient world, found also in Scripture, that pictures great nations
as mountains (see for instance Zech. 4:7). The mountains were
indeed falling on him. The Leather Jacket Boys were no match
for the army of The Establishment. Gaal’s bunch was not very
brave, and many died trying to get back into the city (v. 40).

The Doom Implemented: Shechem

42, Now it came about the next day, that the people went out
to the field, and it was told to Abimelech.

43. So he took the people and divided them into three heads
[companies], and lay in wait in the field; when he looked and saw
the people coming out from the city, he arose against them and
smote them.

44. Then Abimelech and the company who was with him
dashed forward and stood in the entrance of the city gate; the
other two companies then dashed against all who were in the
field and smote them.

45. And Abimelech fought against the city all that day, and
he captured the city and killed the people who were in it; then he
razed the city and sowed it with salt.

Abimelech is now a very angry man. His wrath must be
satisfied ! He will be avenged against all these people who have

192 Judges

tragedy more often than not; but they have been too much influ-
enced by the story of Agamemnon and Iphigenia. We shall take
up the argument in detail below.

Recognizing these facts, some other conservative commen-
tators have argued that Jephthah did absolutely nothing wrong.
He was a great hero of the faith. God provoked his vow, and his
vow was good. There is nothing here to criticize.

I think that both sides have missed the overall theology of
Judges, and of this section of Judges in particular. I have tried
to lead into this story of Jephthah by making that overall theol-
ogy clear, so that what Jephthah does will make sense. Yes, he
was a real hero of the faith. Yes, God provoked his vow. But
Jephthah had a weakness, and his weakness was the weakness of
Israel as a whole. Israel thought that they needed a human king
as deliverer, and Jephthah thought so too. Jephthah’s flaw was
his desire to startup a dynasty, doubtless for the good of Israel.
The faithful Lord of Israel acted to prevent this, removing from
Jephthah any possibility of stumbling, and teaching Israel a
lesson.

As we shall see, Jephthah’s vow was not rash. It was well
thought out. He had something definite in mind, for a threshold
sacrifice is connected to the establishment of a house or dynasty.
God moved Jephthah to make this vow, because God wanted to
make it clear that it was His house that was the true house of
the King, and it was His house (the Tabemacle) that would be
built (on the threshold sacrifice of the virginity of the women
who ministered at its doorway). Thus the Lord reproved
Jephthah and Israel for desiring a human king, and reminded
them that He alone was their King, and that it was His house
that had to be built up.

This may seem strange, but I believe it is because we are not
used to thinking as people in ancient Israel thought. My task is
to make this interpretation clear as we now turn to the text.

11:1. Now Jephthah the Gileadite was a mighty man of valor,
but he was the son of a harlot. And Gilead begat Jephthah.

.2, And Gilead’s wife bore him sons; and when his wife’s sons
grew up, they drove Jephthah out and said to him, “You shall
not have an inheritance in our father’s house, for you are the son
of another woman.”

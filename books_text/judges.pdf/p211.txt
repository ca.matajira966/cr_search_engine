Jephthah: Reaching for the Crown 195

The Demands of Jephthah

4, And it came about after a while that the sons of Ammon
fought against Israel.

5. And it happened when the sons of Ammon fought
against Israel that the elders of Gilead went to get Jephthah
from the land of Tob.

6. And they said to Jephthah, “Come and be our chief that
we may fight against the sons of Ammon.”

7. Then Jephthah said to the elders of Gilead, “Did you not
hate me and drive me from my father’s house? So why have you
come to me now when you are in trouble?”

8. And the elders of Gilead said to Jephthah, “For this rea-
son we have now retumed to you, that you may go with us and
fight with the sons of Ammon and become head over all the in-
habitants of Gilead.”

9. So Jephthah said to the elders of Gilead, “If you take me
back to fight against the sons of Ammon, and the Lorn gives
them up before me, will I become your head?”

10. And the elders of Gilead said to Jephthah, “The Lorpis
hearer between us; surely we will do according to your word.”

11. Then Jephthah went with the elders of Gilead, and the
people made him head and chief over them; and Jephthah spoke
all his words before the Lorp at Mizpah.

The elders of Gilead had said that whoever delivered them
from Ammon would become their chief. This was according to
the rule we have mentioned earlier: He who saves you becomes
your ruler. This “rash vow,” however, put the men of Gilead in
an awkward position. It was, first, humiliating to have to offer
this title to Jephthah; these elders had acquiesced when the
brothers expelled Jephthah (or, if by “brothers” we are to under-
stand all of Gilead, then they themselves expelled Jephthah,
which is what verse 7 implies). Second, however, their vow now
put them in a position of violating the law, for Jephthah, being
a bastard, could not possibly become a ruler in Israel in any full
and normal sense. The reason why there are no judges from the
tribe of Judah during this period (curious, isn’t it, that the royal
tribe contributes no judges?) is that virtually the whole tribe
were bastards, as mentioned before. Now, however, the
Gileadites are forced to offer rule to a bastard. Jephthah does
indeed become a judge, exceptional as this is. The bastard judge

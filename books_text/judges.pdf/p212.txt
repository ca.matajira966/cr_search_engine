1% Judges

was a continuing sign to Israel of her spiritual condition — the
best man available was not qualified for the job!

Jephthah’s faith is seen in his expression that the Lord will be
their Deliverer (v. 9). All the same, however, he chooses to ig-
nore thé law excluding bastards, and shows a desire to become
some sort of king. ‘The whole deal is ratified in the presence of
the Lord, which probably means that the Ark of God was in the
field at Mizpah (2 Sam. 11:11).

There is an ambiguity throughout the story of Jephthah,
which causes some commentators to treat him too harshly and
others to treat him too leniently. Jephthah was a mixture, as
Israel was a mixture; and his actions were mixed as well.

The Gospel Offered to Ammon

12, Now Jephthah sent messengers to the king of the sons of
Arnmon, saying, “What is between you and me, that you have
come to me to fight against my land?”

13, And the king of the sons of Ammon said to the
messengers of Jephthah, “Because Israel took away my land
when they came up from Egypt, from the Arnon as far as the
Jabbok and the Jordan; therefore, return them peacably now.”

The fact that Jephthah sends messengers to Ammon is a sign
that he is primarily a man of peace, contrary to the assertions of
some commentators who present him as a rash and violent man.
Also, the fact that the message of the Word of God is sent to
Ammon means that Jephthah is preaching the gospel to them. If
Ammon will recognize that Israel is priest to the nations, and
allow Israel to do her priestly work, then Ammon can be saved.
Samson’s offer of marriage to a Philistine girl is also a sign of the
gospel offered to the gentiles, as we shall see, and this is a tie be-
tween the Jephthah and the Samson stories. Jephthah preached
the Word to Ammon calling on them to repent of their actions
against God’s priestly nation. The opportunity to repent is pro-
vided before judgment falls.

Jephthah has entered into discussion, and has heard Am-
mon’s claim, that Israel stole their land. He now refutes this
claim with a series of arguments. (The fact that Jephthah refers
to Israel as “my land” need not worry us. As the judge, he
represented the Lord in warfare, and was identified with Him,
so that in some sense Israel was Jephthah’s land.)

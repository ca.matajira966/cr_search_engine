Jephthah: Reaching for the Crown 207

this person as a whole burnt sacrifice, we realize that he was “de-
voting” him or her to the Lord, and thus no ransom was possible.

Usually this means the death penalty, but it is also possible to
“devote” something or someone to God in life. Numbers 18:14
says to the priests, “Every devoted thing in Israel shall be yours”
(and see Ezk. 44:29). Thus, the heart and essence of “devotion”
was not destruction, but total dedication to the sanctuary of
God. We see this also from the fact that to be “devoted” was
also to be “most holy.” Except for Jesus Christ, no man is ever
called “most holy” (Dan. 9:24; Mt. 12:6). A concordance study
of “most holy” will show that the food of the various sacrifices,
and the various articles of furniture in the Tabemacle, were
“most holy,” and that only the Aaronic priests might have any-
thing to do with them-ordinary Levites would be killed if they
approached them (Num. 4:19). Thus, because of sin, no man
could be “most holy” or devoted to God; but by eating of the
most holy sacrifices, the Aaronic priests were incorporated into
union with the most holy, just as the Christian is incorporated
into the most holy Son of God by eating of His flesh and drink-
ing of His blood in Holy Communion.

What emerges from this all too brief discussion is that any
person devoted to God as most holy could not be redeemed, but
had to be given totally to the sanctuary, the Tabernacle. Nor-
mally, because of sin, this meant death, so that the only persons
thus “devoted” were persons who needed to be put to death. To
our knowledge, except for Jesus Christ, Jephthah’s daughter is
the only person ever devoted to God in life. It meant that she
could not be redeemed; it also meant that she would spend the
rest of her life at the sanctuary of God. Doubtless she was not
the only person ever thus devoted to God in life, but she is the
only example we know of.

The daughter submits to the inevitable, and only asks that
she be given two months to roam around the mountains to
bewail her virginity. This would make absolutely no sense if she
were about to be put to death, but it makes a lot of sense if she
were about to be consigned to perpetual virginity. The whole
point of having virginity is to lose it in the joys of marriage. The
virgin in the Bible is the one who awaits the bridegroom;
perpetual virginity is pure sterility, and is a Greek, not a
Biblical, ideal (see Ps. 45). Jephthah’s daughter “mourns” her

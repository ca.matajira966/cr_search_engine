Jephthah: Destroying the Envious 219

ment. For such people, their baptism (in the Jordan) will
become their destruction (as it happened to Ephraim). The sign
of unity (baptism) will become a sign of obliteration.

7, And Jephthah judged Israel six years. Then Jephthah the
Gileadite died and was buried in the cities of Gilead.

Perhaps the fact that Jephthah only judged six years is de-
signed to show one more time that man’s rule does not reach to
the sabbath, and thus that a True and Future Judge and King
must come to bring in the everlasting, sabbatical kingdom. We
also see something else here. The other judges are said to be
buried with their fathers, or in their cities. Jephthah’s father was
(almost certainly) unknown, and here we see that his burial site
is also unknown. He emerged from “Gilead,” and he returns to
“Gilead.” He emerged as a man with no family name, and he left
no family name. Thus, he was a sign to Israel that ultimately
their trust for such things must be in the Lord alone. The
bastard and the eunuch (and Jephthah is in some sense both)
might not have been full citizens of the earthly kingdom in the
Old Covenant, but they had a heavenly home, from which they
could never be driven, and which will endure forever.

224 Judges

they found there (Num. 13:20, 23). The grape was a sign, once
again, of the arrival of sabbath blessing, and of the end of the
curse. When the sabbath year came, however, Israel was not
allowed to eat the grapes! This was a sign to them that the sab-
bath had not really come, and was still really in the future. They
were not ready to enter into this privilege. (Similarly, the word
nazir is used in Lev. 22:2 to show that when the priests were
unclean, they were not allowed to eat the holy food.) In the New
Covenant, the highlight of the Lord’s Day is the meal with God,
drinking the juice of the grape. For us, the sabbath has come in
Christ.

Once he had finished his work and had dedicated it to the
Lord, “afterward the Nazirite may drink wine.” Just so, when
Jesus finished His work, He sat down at the Father’s right hand,
and now drinks wine anew with us in the Kingdom (Matt.
26:29).

Long Hair
5, All the days of his vow of separation no razor shall pass
> over his head. He shall be holy until the days are fulfilled for
which he separated himself to the Lorn; he shall let the locks of
hair on his head grow long.

The third separation demanded of the Nazirite is that he not
use a razor during the time of his vow. If he becomes defiled, he
must shave his head, and let the hair grow back, starting over as
it were (Num. 6:9, 11). At the end of his vow:

18. The Nazirite shall then shave his dedicated head at the
doorway of the tent of meeting, and take the dedicated hair of
his head and put it on the fire which is under the sacrifice of
peace offerings.

19, And the priest shall take the ram’s shoulder, boiled, and
one unleavened cake out of the basket, and one unleavened
wafer, and shall put them in the palms of the Nazirite after he
has shaved his Nazir [his “untrimmed”].

Let us note initially three things here. First, the hair is referred
to as the Nazir (v. 19). This connects it up with the untrimmed
grape vines of Leviticus 25. Second, throughout this passage
what is dedicated is the “head,” and what is shaved is the “head.”

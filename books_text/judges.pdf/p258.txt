Samson: The Mighty Bridegroom 243

from people like that. He’s a Nazirite. He’s supposed to keep a
stricter rule than the rest of us .“

Uriah: “Well, I don’t know. Maybe he’s just a man like
everybody else.”

Saul: “Yeah, but that’s compromise, and he’s the one that’s
not supposed to compromise.”

Uriah: “Well, uh, that’s right, but maybe none of us ought
to be compromising. . . .”

We can take one more example:

Abner: “Did you hear about Samson carrying off the gates
of the city?”

Amnon: “Yeah, he left the city wide open. We could have
moved in and conquered it .“

Abner: “That’s true. How come we didn’t?”

A Philistine Wife

14:1. Then Samson went down to Timnah and saw a woman
in Timnah, of the daughters of the Philistine.

2. So he came up and told his father and mother, saying, “I
saw a woman in Timnah, of the daughters of the Philistine;
now therefore, get her for me as a wife.”

3. Then his father and his mother said to him, “Is there no
woman among the daughters of your brothers [relatives], or
among all our people, that you go to take a wife from the uncir-
cumcised Philistines?” But Samson said to his father, “Get her
for me, for she is right in my eyes.”

4. However, his father and mother did not know that it was
of the LoRD, for he [or, He] was seeking an occasion against the
Philistine. Now at that time the Philistine were ruling over
Israel.

Samson goes down to Timnah, and comes back up to his
parents. Israel is living in the hills; Philistia controls the plains.
Timnah was two miles west of Beth-Shemesh, the house of the
Sun God. Samson is God’s true sun, God’s replacement for the
false Canaanite worship of the sun.

Samson’s parents call his attention to the basic Biblical prin-
ciple that believers do not marry unbelievers. It seems that they
are correct, but we are told in verse 4 that God was guiding Sam-
on; indeed, it was not simply “God” superintending Samson by
Divine providence, but it was the Lord, the Covenant God of

Samson: The Mighty Bridegroom 249

would kill him. The classic example of this comes from the story
of the Theban Sphinx. The riddle was, “What has four legs in
the moming, two legs at noon, and three legs in the evening?”
The Sphinx killed travelers who did not know the answer, until
Oedipus solved the riddle.. A greater than the Sphinx is here,
however, for Samson has already slain the lion-guardian. If the
Philistine can solve the riddle, they will have the key to Sam-
on’s secret, and they will have power over him.

The wedding feast always involved freely flowing wine, as at
Cana (John 2:1-11). Did Samson violate his Nazirite vow? We
are not told so, and there is no reason to think so. Since his hair
was long, everybody knew he was “weird” anyway. They would
have known he was under a vow (for the pagans had similar
vows), and he could gracefully have refused alcohol.

Not being a wealthy Philistine, Samson could not provide
the retinue that a wedding feast should have, so the Philistine
provided it. This was a sign of Israel’s weakness and humility in
the face of Philistine domination. Thirty men were provided as
“friends of the groom.” The number 3 and multiples of it occur
throughout this passage, and except for the numbers 7 (14:12,
15, 17) and 1000 (15:15 f£.) no other number occurs:

14:6 “the Spirit of the Lorp rushed upon him mightily” three
times, 14:19; 15:14

14:11 30 companions

14:12 30 linen wraps and 30 changes of clothing

14:13 30 linen wraps and 30 changes of clothing

14:14 3 days

14:19 30 slain Philistine

15:4 300 foxes

15:11 3000 men of Judah

The clue to understanding this use of the number three is in
Judges 14:14. As we have noted before, the third day is the day
of preliminary judgment, which puts men back on the track and
makes it possible for them to do good works and come to the
final judgment of the seventh day. It is precisely such a
preliminary judgment that Samson brings upon Philistia, since
he is only beginning to deliver Israel (Jud. 13:5). Moreover, the
judgment of the third day is an opportunity to repent, before
the final judgment of the seventh day. Thus, Samson’s

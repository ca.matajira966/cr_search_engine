2 Judges

ment against those who had converted to the faith. Here he ex-
ercises his calling properly.

Summary

We have now reached the end of the section that details
Judah’s initial faithfulness. It would be good here to draw
together some conclusions, before proceeding further. First, we
see three fundamental ways in which God deals with His
enemies. They are dispossession (driving the Canaanites out),
retribution (perfect justice, an eye for an eye), and utter sacrifi-
cial destruction (seen at Hormah).

Second, we see a series of types or pictures of what it means
to conquer Canaan. Since the great commission tells the Church
today to “disciple all nations,” we can learn lessons from this.
Each of the conquests is a picture of some central aspect of
wickedness that must be vanquished. Each conquest shows us
what Christ (the royal Person, pictured by the royal tribe) has
already done for us, and what we are to do in applying His work
to our world:

Simeon-— Christ invites those under the curse to join with
Him, and find salvation. That is our task as well, for we also
face world rulers, apostate churches (false Jerusalem), and
giants.

Bezek — Christ destroyed the Prince of this World, and so do
we (Rem, 16:20). He measured out perfect justice, and so must
we,

Jerusalem — Christ destroyed the Old Jerusalem in 70 A.D.,
and established the New Jerusalem, His Church. It is our task to
hold it.

Hill country, south country, lowland — Christ conquered the
whole world, and now we must apply that conquest everywhere.

Hebron (Kiriath-Arba) — Christ conquered the giants, and in
union with Him, no giants can stand against us. He has given us
sanctuary in Him, and we must offer sanctuary to all men.

Debir (Kiriath-Sepher) — Christ cast down all philosophies
and imaginations against Him, and replaced them with His
Word, the true foundation of civilization; and this is our task as
well,

Achsah— Christ has given his Bride a good land. We should

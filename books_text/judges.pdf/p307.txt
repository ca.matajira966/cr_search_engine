294 Judges

the day is declining; spend the night here that your heart maybe
merry. Then tomorrow you may arise early for your journey so
that you may go to your tent [home].”

The story seems to bog down and stretch out here. Why this
emphasis on lingering at the house of the father-in-law? It does
not seem to add any tension to the story, and it does not seem to
make any point.

I should like to suggest an overall pattern into which this sec-
tion of the narrative fits, which I believe explains what we have
here. When the girl goes back to her father’s house, it is as if
Israel had gone back to Egypt or to Ur of the Chaldees. When
Israel played the harlot, she rejected the Lord as husband and
sought the gods she formerly worshiped in Ur and in Egypt
Gosh, 24:2, 14), There is a formal parallel between Israel’s
return to her father’s house (idolatry) and the girl’s return to her
father’s house when she plays the harlot. That is not to say that
her father was himself a faithless man, only that a formal
parallel exists.

We cannot help but notice the way the writer emphasizes
how the father-in-law tried to detain the Levite. We might ex-
pect the writer to say that the girl’s father tried to get the Levite
to prolong his stay, but when we read over and over and over
again that the father-in-law persuaded him to stay a bit longer,
we are alerted to a theme of “detaining.” Now, the Levite was a
man with a calling, a task. He was supposed to be pastor of a
local congregation in the remote part of Ephraim. For him to be
gone too long would result in a neglect of his task. Thus, after
three days, he wants to depart. The girl’s father, however, de-
tains him. Now we are reminded of Laban, how he sought to de-
tain Jacob, and of Pharaoh’s detaining of Israel.2 Once again,
there is no indication of a moral parallel between this father-in-
law and Laban or Pharaoh, but there is a formal parallel. After
three days is indeed the proper time for a definitive break, to be
on one’s way; but the father-in-law persuades him to stay. In all,
the father-in-law is shown trying to get the Levite to stay five
times (v. 4, 5, 6-7, 8, 9).

By doing this, the writer creates in us a sense of urgency.

2, See my book, Law of the Covenant, pp. 32ff., for a study of this “exodus
pattern” in the relationship of Jacob and Laban.

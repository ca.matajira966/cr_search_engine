Conquest, Compromise, Judgment, and Restoration 13

entreat the Father for the water (of the Spirit) needed to make it
fruitful.

Kenites — Christ invites all men to join His New Israel, and
that evangelistic task is ours as well.

Hormah — Christ has utterly destroyed His enemies for all
time, by the application of Divine fire. The tongues of fire on
Pentecost show us that it is the preaching of the gospel which
applies that fire to the world today, burning away the chaff and
purifying the gold.

The Response of Israel:
Judah’s Progressive Failure (1:18-21)

Philistine
18. And Judah took Gaza with its territory, and Ashkelon
with its territory, and Ekron with its territory.

That’s wonderful, but there were five Philistine cities. How
about Gath and Ashdod? Joshua 15:47 makes it plain that all
five of the Philistine cities were part of Judah’s inheritance, but
only three are listed here.

At this point, then, the story of Judah’s conquests takes a sub-
tle tum. Heretofore we have seen nothing but victories, together
with a hint of the restoration of Edenic conditions among the
faithful. Now, however, we begin to detect signs of failure.

Iron Chariots

19. Now the Lorp was with Judah, and they took possession
of the hill country; but they could not dispossess the inhabitants
of the valley because they had iron chariots.

Chariots could not function in the hills, so Judah did not have
to fight them there. Where the iron chariots could function, how-
ever, Judah did not succeed. In fact, all the places listed in Judges
1 are mountain places. God, however, did not limit Judah only to
mountainous regions; in 1:2, God had given all the land into her
hand. Moreover, as Judges 4 and 5 will show, God is fully capable
of dealing with iron chariots. Thus, the problem was not the iron
chariots. The problem was faith, or rather the lack of it.

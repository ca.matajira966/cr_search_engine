310 Judges

said, “Surely they are smitten before us, as in the first battle.”
40. But when the cloud began to rise from the city in a col-

umn of smoke, Benjamin looked behind them; and behold, the

whole burnt sacrifice of the city was going up to heaven.

The Lord struck Benjamin. Naturally this means that He put
fear in their hearts, and strengthened the hearts and arms of
Israel. Also, however, it means something specific. Just when
Benjamin thought once again that they had won another battle,
they turned around and saw Gibeah going up in smoke. But it
was not just ordinary smoke. The Hebrew uses the word for
whole burnt sacrifice. It was fire from God’s altar at Bethel
that had been used to torch the city. Gibeah became a whole
bumt sacrifice to the Lord.

Before looking at this further, let us ask if this action had the
Lord’s approval. It meant that Israel was conducting holy war
against Gibeah, and it meant that all within the city would be
slaughtered, as we shall see. We must say, from the passage, that
this did indeed have the Lord’s approval. First, the Lord had
guaranteed them victory when they consulted Him at the Taber-
nacle. They could not have brought fire from the altar without
the approval of Phineas, and thus of the Lord. Second, the
parallel between the buming of Gibeah and the statement of
verse 35 that it was the Lord Who struck Benjamin, makes it
clear that burning Gibeah was God’s own appointed means of
destroying it.

We took up the sacrifice of a city in our comments on Judges
1:8 and 17. We need to look at it a bit more fully here. The city of
Gibeah was clearly a second Sodom, and in Genesis 19:24 and 28
we read that God rained fire on Sodom, and that Lot saw that
“the smoke of the land ascended like the smoke of a kiln.”
Sodom was the first “hormah,” the first city burned up by fire
from heaven. It is entirely appropriate that Gibeah receive the
same treatment.

There is also a parallel, as we have seen, to the defeat of Ai.
A study of Joshua 8 shows that Ai was also burned up as a
whole burnt sacrifice (Josh. 8:28).

By offering a whole burnt sacrifice on God’s altar, Israel had
confessed their sin and had put their faith in a Substitute (Jud.
20:26). As a result, they were not consumed. Benjamin, how-
ever, had rejected the Substitute, and so they were destroyed.

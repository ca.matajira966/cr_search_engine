The Salvation of Benjamin 321

on pain of death. Eating or not eating of the fruit of fellowship
with the devil was the test of whether or not he would be a true
priestly guardian. This is exactly the same as what is going on
here in Judges 21. It was the duty of each and every “Adam” (city)
in the holy land to guard the “Garden,” God’s new land flowing
with milk and honey. Those who refused to guard showed by
their action that they had been fellowshipping with the devil,
and so they came under the original curse.

With all of this in mind, we need not be troubled at Israel’s
destruction of Jabesh-Gilead. In this situation, in this context,
the oath that Israel swore was right and proper, and the destruc-
tion of Jabesh-Gilead was absolutely necessary.

The Destruction of Jabesh-Gilead

6, And the sons of Israel were sorry for their brother Ben-
jamin and said, “One tribe is cut off from Israel today.

7. “What shall we do for wives for those who are left, since
we have sworn by the Lorp not to give them any of our
daughters in marriage?”

8. And they said, “What one is there of the tribes of Israel
who did not come up to the Lorp at Mizpah?” And behold, no
one had come to the camp from Jabesh-Gilead to the assembly.

9. For when the people were mustered, behold, not one of
the inhabitants of Jabesh-Gilead was there.

10. And the congregation sent 12,000 of the valiant warriors
there, and commanded them, saying, “Go and strike the in-
habitants of Jabesh-Gilead with the edge of the sword, with the
women and the little ones.

11. “And this is the thing that you shall do: You shall utterly
destroy [hormah] every man and every woman who has known
lying with a man.”

12. And they found among the inhabitants of Jabesh-Gilead
400 young virgins who had not known a man by lying with him;
and they brought them to the camp at Shiloh, which is in the
land of Canaan.

13. Then the whole congregation sent word and spoke to the
sons of Benjamin who were at the rock of Rimmon, and pro-
claimed peace to them.

14, And Benjamin retumed at that time, and they gave them
the women whom they had kept alive from the women of
Jabesh-Gilead; yet they were not enough for them,

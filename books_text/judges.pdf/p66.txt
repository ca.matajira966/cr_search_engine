48 Judges

were anointed directly by the Spirit.

This does not mean they were not elected officials. E. C.
Wines has provided a good commentary on this point: “Four
stages may be noted in the proceedings relating to Jephthah; —
the preliminary discussion, the nomination, the presentation to
the people, and the installation (Judges 10:17, 18 and 11:1-11).
The enemy was encamped in Gilead. At this point, the people
and their rulers, assembled in convention on the plain, said to
one another, ‘Who shall be our chief, to lead us against the foe?’
This was the discussion, in which every citizen seems to have
had the right to participate. In the exceedingly brief history of
the affair, it is not expressly stated, but it is necessarily implied,
that Jephthah, of Gilead, a man of distinguished military genius
and reputation, was nominated by the voice of the assembly.
But this able captain had been some years before driven out
from his native city. It was necessary to soothe his irritated
spirit. To this end the elders went in person to seek him, laid be-
fore him the urgent necessities of the state, softened his anger by
promises of preferment, and brought him to Mizpeh. Here,
manifestly, they made a formal presentation of him to the peo-
ple, for it is added, ‘the people made him head and captain over
them.’ That is, they completed the election by giving him their
suffrages, recognizing him as their leader, and installing him in
his office. Here, then, we have, 1. The free discussion of the peo-
ple in a popular assembly conceming the selection of a leader;
2. The nomination of Jephthah by the meeting to be chief;
3. The elders’ presentation of him to the people for their
suffrages; and 4. His inauguration as prince and leader of Israel.
It is to the analysis of such incidental relations as this scattered
here and there through the history, that, in default of a more ex-
act account of the primitive order of things, we are compelled to
resort, in our study of the Hebrew constitution, for much of the
information, which it would be gratifying to find in a more de-
tailed and systematic form.” !

Thus, the judges were not self-appointed men, but were
leaders recognized by the people. This is obvious even if Wines’s
analysis be not convincing in its every detail. There was a regu-

1, B.C. Wines, The Hebrew Republic (originally published as Commentary
on the Laws of the Ancient Hebrews, vol. 2; reprint, Uxbridge, MA: Ameri-
can Presbyterian Press, n.d.}, p. Of.

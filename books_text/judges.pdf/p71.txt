Othniel: Babylon Anticipated 83

bolic weight which makes them typical of God’s continuing war
against Satan, and of Christ’s victory in particular. It is in this
sense that the pregnant term yasha‘’, “savior,” can be used of
them. The book of Judges repeatedly calls the judges “deliverers,
saviors,” and speaks of their work as “deliverance, salvation,” all
translations of yasha‘ in its various forms. (As mentioned in
chapter 2 of this study, yasha‘ is the foundation for the names
Joshua and Jesus.) We should not limit this only to “political
salvation” (though it includes that), for it is a fuller, Spiritual as
well as cultural deliverance that is in view. Apart from Spiritual
repentance and conversion, there would never have been any of
the deliverances recorded in the book of Judges.

The judges were, in the ordinary way of life, civil rulers; but
in Scripture, as their work is recorded for our profit, they are
spoken of as anointed ones — in Hebrew, messiahs — and as sav-
iors. Thus, the book of Judges tells us something about the nor-
mal work of the civil magistrate in a Christian society, but it also
tells us something about the special work of Christ, the final
Messiah or Anointed One, the greater Joshua or Savior.

With these introductory remarks in view, let us now turn to
the story of Othniel, the first judge.

Othniel

3:7. And the sons of Israel did what was evil in the sight of
the LoRD, and forgot the Lorp their God, and served the Baals
and the Asherahs.

8. Then the anger of the Lorp was kindled against Israel,
so that He sold them into the hands of Cushan-Rishathaim king
of Aram-Naharaim; and the sons of Israel served Cushan-
Rishathaim eight years.

9, And when the sons of Israel cried to the LoRD, the Lorn
raised up a deliverer (yasha‘) for the sons of Israel to deliver
(yasha‘) them: Othniel the son of Kenaz, Caleb’s younger brother.

10. And the Spirit of the Lorp came upon him, and he judged
Israel. When he went out to war, the Lorp gave Cushan-
Rishathaim king of Aram-Naharaim into his hand, so that he
prevailed over Cushan-Rishathaim.

11. Then then land had rest forty years. And Othniel the son
of Kens.z died.

After the elders who survived Joshua died, the people began
to worship ‘the Baals (local varieties of Baalism) and the Asherahs.

Ehud: Sodom and Amalek Resurgent 63

constituted a reasonable threat: All men carried swords on their
left thighs. But Eglon suspected nothing, it seems, because
Ehud was left-handed. In many cultures of the world, left-
handedness is regarded as bad, and left-handed children are
forced to become right-handed as they grow up. Israel was more
enlightened than to practise such a stupid and superstitious cus-
tom, but is it entirely possible that the pagan Eglon was from a
culture that suppressed left-handedness. He may never have en-
countered a left-handed adult.

The sword sank fully into Eglon’s flesh, indicating just how
fat he really was. The fat closed completely over the blade, so
that the sword disappeared from view. Later on, when the ser-
vants of Eglon peered through the windows or latticework of
the roof chamber, they saw him sitting on the floor, where he
had fallen, and did not see a sword sticking out of him. Ehud
did not have to try to sneak a bloody sword out of the house.
Eglon’s fat was used by God to buy Ehud time to escape and
raise his army. The symbol of Moab’s power (Eglon’s sheer size)
was used against them in their destruction. Here we see God’s
ironic humor, as he uses the works of men against them.

God used something else as well. The last part of verse 22
says that “it” came out. Commentators have sometimes debated
the question, but there is no doubt as to what “it” was. In death,
the muscles of the colon relax, and sometimes excrement issues
from the body. According to verse 24, Eglon’s courtiers thought
that he was “covering his feet in the cool room.” The expression
“covering the feet” is used for private acts in Scripture. Here it
clearly refers to a bowel movement. They could only have
thought this if they had smelled the odor. Eglon was a very fat
man (v. 17). His excrement would therefore have been copious,
poorly digested, and very noisome. God used this stench to buy
Ehud time to escape.

In all of this we see God using the sins of men against them.
Eglon’s foul manner of life, his gluttony, and his idolatry (stan-
ding up at the mention of what he assumed was his own false
god), all were used against him in the end. God always makes
the punishment fit the crime, and the criminal; and God often
does so humorously (see Ps. 2:4).

Eglon means “calf.” Matthew Henry comments that “he fell
like a fatted calf, by the knife, an acceptable sacrifice to divine

Ehud: Sodom and Amalek Resurgent 67

The Savior Leads His Army to Victory

26. Now Ehud escaped while they were delaying, and he
passed by the idols and escaped to Seirah.

27, And it came about when he had arrived, that he blew the
hom in the hill country of Ephraim; and the sons of Israel went
down with him from the hill country, and he was in front of
them,

28. And he said to them, “Pursue, for the Lorp has given
your enemies the Moabites into your hands.” So they went down
after him and seized the fords of the Jordan opposite Moab, and
did not allow anyone to cross.

29. And they struck down at that time about 10,000
Moabites, all robust and valiant men; and no one escaped.

30. So Moab was subdued that day under the hand of Israel.
And the land was undisturbed for eighty years.

Ehud did have time to escape. First he went to Gilgal, the
place where shame was removed, to rally the detachment he had
left there. Then he went north, and backtracked somewhat to
rally his forces in Ephraim. As the Israelite forces swept down
out of the hills, Ehud was in front of them. They joined their
fellows at Gilgal and took possession of the crossing place of the
Jordan. The Moabites, in disarray at the death of their leader,
and a long way from home, began a hurried retreat toward the
Jordan. All were slain by Ehud’s men.

The Jordan was the boundary into the Promised Land. It
was the place of transition from death to life, the place (later on)
of baptism and judgment. For Israel, the judgment had been
unto life, because circumcision was a bloody sacrifice that took
away their sin. There was a hill of foreskins by the gateway into
Canaan (Josh. 5:3). This formed a bloody “pillar” like a gate
post, which corresponded to the blood on the doorposts of the
houses in Egypt at Passover. God spared them when He saw the
blood. But for Moab, there was no slain lamb’s blood on the
doorpost, and no hill of foreskins at the gate. Thus, for them
the place of judgment was a place of destruction. (This theme of
the Jordan as a place of judgment recurs twice again, in Judges
7:24 and 12:5.)

Numerologically, the 10,000 slain represents a total and com-
plete victory, ten being the number of totality. Just as Moab had

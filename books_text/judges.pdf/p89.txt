5

DEBORAH: A MOTHER FOR ISRAEL
(Judges 4)

The liturgical structure of the events of Judges 4 and 5 is
this:

1, God’s command: be faithful
2. Man’s response: disobedience
3. God’s evaluation: Judgment — Canaanite invasion

4, (Implied Command: repent and be delivered)

5. Man’s response: repentance
6. God’s evaluation: Blessing - Deborah raised up

7. God’s command to Barak: lead the army
8. Barak’s response: not unless Deborah goes along
9, God’s evaluation: a woman will get the glory

10. God’s command to Israel: follow Barak and Deborah
11. Israel’s response: some tribes come, and some do not
12. God’s evaluation: the song of Deborah

The following is an outline of the text:

I Enslavement (Judges 4:1-3)
I God’s command/promise (4:4-7)

I Man’s response (4:8-24)
A. Barak’s response and God’s evaluation (4:8-9)
B. Israel’s response (4:10-16)
C. Jael’s response (4:17-22)
D. Continuing response (4:23-24)

IV. God’s evaluation (Judges 5}
A. Stanza 1: The Situation (5:2-11)
B. Stanza 2: The Battle (5:12-22)
C. Stanza 3: The Aftermath (5:23-31)

7

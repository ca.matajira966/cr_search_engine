Deborah: A Mother for Israel a1

the man who should lead, and the woman who should be right
next to him as a “helper” (Gen. 2:18). It was not humiliating for
Barak, as a child in Israel, to submit to the guardianship of
Deborah, the mother of Israel. Now, however, Barak is grown.
Like Adam, he is supposed to take up a role as guardian of the
Bride/Garden. Though his faith is real, it is also weak, and so
Barak receives the humiliation of having a woman do his job.

This gives us the principle that when the man defaults, the
woman may step in to do the job. This is true in every area of
life except, as we have noted, the central core area of liturgy,
where it requires a man to signify the Groom to the Bride. This
is one of the things going on in the story of Deborah. Because
the men were children, God raised up a mother for them. Even
when the men were grown, they still did not want to take up
their God-ordained role, so God let a woman do the man’s task.

Thus, we have two different, but overlapping principles. The
first is that the woman may rule as a mother, even in the larger
sense of a mother to society at large. The second is that the
woman, as vice president, may rule when the men have de-
faulted. Both principles are valid, but it is the first principle
that has power. The mother raises up a son, who tums around
and saves her, as Jesus did Mary (Luke 1:47; 1 Tim. 2:15).

Israel’s Response

9b. Then Deborah arose and went with Barak to Kedesh.

10. And Barak called Zebulun and Naphtali together to
Kedesh, and 10,000 men went up at his feet. Deborah also went
up with him.

11. Now Heber the Kenite had separated himself from the
Kenites, from the sons of Hobab the brother-in-law of Moses,
and had pitched his tent as far away as the oak in Zaanannim,
which is near Kedesh.

12, Then they told Sisers that Barak the son of Abinoam had
gone up to Mount Tabor.

13. And Sisers called together all his chariots, 900 iron
chariots, and all the people who were with him, from
Harosheth-Hagoyim to the river Kishon.

14, And Deborah said to Barak, “Arise! For this is the day in
which the Lorn has given Sisers into your hands; behold, the
Lorp has gone out before you.” So Barak went down from
Mount Tabor with 10,000 men following him.

KILLER ANGEL

together, those lies comprise the lie. The Big Lie. The
Grand Illusion. The Modern Myth.

Myths, according to theologian J. I. Packer, are
“stories made up to sanctify social patterns. 3 They are
lies, carefully designed to reinforce a particular phi-
losophy or morality within a culture. They are instru-
ments of manipulation and control.

When Jeroboam splintered the nation of Israel after
the death of Solomon, he thought that in order to con-
solidate his rule over the northern faction he would have
to wean the people from their spiritual and emotional
dependence on the Jerusalem temple. So he manufac-
tured myths. He lied:

And Jeroboam said in his heart, “Now the
kingdom will return to the house of David.
If this people go up to offer sacrifices in the
house of the Lorp at Jerusalem, then the
heart of this people will return to their lord,
even to Rehoboam king of Judah; and they
will kill me and return to Rehoboam king of
Judah.” So the king consulted, and made
two golden calves, and he said to them, “It
is too much for you to go up to Jerusalem;
behold your gods, O Israel, that brought you
up from the land of Egypt.” And he set one
in Bethel, and the other he put in Dan. Now
this thing became a sin, for the people went
to worship before the one as far as Dan. And
he made houses on high places, and made
priests from among all the people who were
not of the sons of Levi. And Jeroboam insti-
tuted a feast in the eighth month on the
fifteenth day of the month, like the feast

108

MADONNA

ment where she started a salon modeled after those in
the Palais Royale and Paris’s Left Bank. Her series of
evening gatherings were opportunities for intellectu-
als, radicals, artists, actors, writers, and activists to
meet and greet, aspire and conspire. Each night had
its own theme: sometimes it would be politics, some-
times drama, or perhaps poetry or economics or art
or science, Ideas and liquor flowed freely until mid-
night, when Dodge would usher in a sumptuous meal
of the finest meats, poultry, cheeses, and French pas-
tries.

Margaret’s topic of discussion was always sex. Her
detour into labor activism had done little to dampen
her interest in the subject, When it was her turn to lead
an evening, she held Dodge’s guests spellbound, rav-
aging their imaginations with intoxicating notions of
the aromatic dignity, the unfettered self-expression,
and the innate sacredness of sexual desire.

Free love had been practiced quietly for years by
the avant-garde intellectuals in the Village. Eugene
O'Neill took on one mistress after another, immortal-
izing them in his plays. Edna St. Vincent Millay
hopped gaily from bed to bed and wrote about it in
her poems. Max Eastman, Emma Goldman, Floyd
Dell, Rockwell Kent, Edgar Lee Masters, and many
others had for some time enjoyed unrestrained sex-
ploits.

But no one had championed sexual freedom as
openly and ardently as Margaret. When she spoke, the
others became transfixed. Her innocent girl-next-
door looks belied her bordello motif and gutter talk.

37

ARRESTED DEVELOPMENT

than three hundred dollars a meal—but the pleasures of
Paris could be had on an economy scale nonetheless.

Each day, they would wander over to the Pont Neuf
bridge to explore the wares of the bouquinistes—the
traditional French booksellers who had pioneered their
unique brand of transportable trade early in the seven-
teenth century. They would then visit one of the many
magnificent museums or perhaps eat a picnic lunch in
the Bois de Boulogne, the huge park along the city’s
western ridge. Often, they would end up soaking in the
jubilant carnival atmosphere at the Champs-de-Mars
just below the Eiffel Tower.

Paris is a marvel of vintage sensory delights. And
both Margaret and William drank deeply from its draft.
The staccato sounds of the clicking of saucers in the
Place de la Contrescarpe, the trumpeting of traffic
around the Arc de Trioemph, and the conspiratorial
whispering on benches in the Jardin de Luxembourg
seem to play a jangling Debussy score in the twilight
hours. The nostalgic smells of luxuriant perfumes, wine,
and brandy; the invigorating odors of croissants, es-
presso, and cut lavender; and the acrid fumes of tobacco,
roasted chestnuts, and salon sautés seem to texture a
sweet and subtle Monet upon the canvas of Pentente de
la vie. The dominating sights of the yellow towers of
Notre Dame, the arched bridges cutting across the satin
sheen of the river, and the stately elegance of the Bour-
bon palaces and pavilions scattered about the city like
caches of mercy seem to sculpt a muscular Rodin bronze
on the tabla rasa landscape.

It was almost heaven.

Almost. But not quite.

45

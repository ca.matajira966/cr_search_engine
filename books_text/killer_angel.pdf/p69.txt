PART FOUR

 

The End of Man

The whole point of the Eugenic pseudo-scientifie
theories is that they are to be applied wholesale by
some more sweeping and generalizing money
power than the individual husband or wife or
household, Eugenics asserts that all men must
be so stupid that they cannot manage their own
affairs; and also so clever that they can
manage each other’s.

—G. K. Chesterton!

Sex EDUCATION

leadership. But that would mean a few public relations
adjustments. Margaret would have to tone down her
rabid pro-abortion stance, of course. And she would
have to take charge of her children once again—as
distasteful as that chore would be for her—in an effort
to rehabilitate her image. She would also, he said, have
to distance herself from revolutionary rhetoric. The
scientific and philanthropic-sounding themes of
Malthus and Eugenics would have to replace the politi-
cally charged themes of old-line labor Anarchism and
Socialism.

By the time her year in England was over, Margaret’s
ideas were firmly in place, her strategy was thoroughly
mapped out, and her agenda was carefully outlined. She
set out for America with a demonic determination to
alter the course of Western civilization. Ultimately, she
would succeed, but the course she and Ellis designed was
not without its high hurdles.

Margaret’s first task after crossing the Atlantic, of
course, was to face up to the year-old legal charges still
outstanding against her. Using the skills she had long
before developed in the IWW protests and labor strikes,
she launched a brilliant public relations campaign that
so rallied public support for her cause that the authari-
ties were forced to drop all charges.

She had won her first victory.

Then, in order to capitalize on all the publicity that
her victory had generated, she embarked on a three-
and-a-half month, coast-to-coast speaking tour. She
was a stunning success, drawing large, enthusiastic
crowds and garnering controversial press coverage every-
where she went.

Another victory.

63

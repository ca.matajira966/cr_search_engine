x LEVITICUS: AN ECONOMIC COMMENTARY

put onto a CD-ROM disk with all of my other books and news-
letters. With a lot of work, it will be possible to link all of this
material through hyper-text electronic links. ‘Technology is
wonderful.

So, how much will this pair of plastic disks cost you? As of
1994, one U.S. dollar. Send a photocopy of this page plus one
dollar to the ICE, Write to: Disks, ICE, P.O. Box 8000, Tyler,
TX 75711. We will send you Boundaries and Dominion.

The conclusions that appear at the end of the chapters in
Leviticus: An Economic Commeniary appear exactly as they appear
in Boundaries and Dominion. If the reader is not persuaded that
my conclusions follow from the text in Leviticus, he should
consult the full text of the chapter as it appears in Boundaries
and Dominion.

Ilow do you eat an elephant? One bite at a time. In Bound-
aries and Dominion, 1 include handy summaries at the end of
each chapter: its main points listed in one-sentence bite-sized
portions. Consult these summaries.

Another large benefit: when I want to add new material to
Boundaries and Dominion as I continue to read and think about
the theological issues, I can do so easily. I plan to do this with
my other books, too: continuing revisions as a result of continu-
ing thought. This is the most cost-effective way for a writer to
kcep his published works current. It is also cheap for readers to
keep abreast of his thought. An electronic search engine makes
il easy to check a writer’s latest thoughts regarding problems he
may have addressed initially many years earlier.

Eventually, I will publish Boundaries and Dominion as the fifth
volume in The Dominion Covenant series: The Dominion Covenant:
Genesis (1982), Moses and Pharaoh: Dominion Religion vs. Power
Religion (1985), The Sinai Strategy: Economics and the Ten Com-
mandments (1986), and Tools of Dominion: The Case Laws of Exodus
(1990). But that will have to wait on finances. Until then, this
shortened version, plus the clectronically indexed computer
disks, will be adequate for most readers.

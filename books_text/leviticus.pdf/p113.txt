Sacrifice, Stewardship, and Debt 57

Because of the shift in covenants, people no longer arc re-
quired by God to spend money for, or forfeit income from,
animals or grains offered as sacrifices. We nonctheless are re-
quired to suffer losses when God extends financial mercy
through us to impoverished debtors: the legal right of a poor
man to declare bankruptcy, thereby escaping his obligation to
repay beyond the market value of his assets, which he forfeits.
This is why bankruptcy laws are a legitimate aspect of a Christian
society. That a person in the United States is allowed this God-
granted privilege once every seven years is a dim reflection of
the Mosaic Covenant’s law of sabbatical release (Deut. 15:1-7).
Since the late nineteenth century, there have been no debtors’
prisons in the West. In the United States, if a debtor is willing
to forfeit all his assets except the clothes on his back and the
tools of his trade, he has identified himself as an impoverished
person. He therefore is allowed to escape the demands of his
creditors by declaring bankruptcy. If he is wise, however, he
will later repay his creditors if he can; because he owes so much
to God, he should not seek to profit from the sacrifices borne
by those who willingly extended credit to him.”

Mandatory Sacrifices and Free Markets

Covenant-breaking man instinctively looks to the works of
his own hands as the basis of his redemption. He believes that

19. 1am not speaking here of civil governments. Anyone so unwise as to cxtend
civil governments credit should not complain when these debtors declare bankruptcy
either directly or through mass inflation. Also, any Bible-affirming new administra-
tion in a civil government should feel no moral compunction against declaring the
government's bankruptcy if previous administrations unwiscly pledged the govern-
ment's obligation to repay, Defaults on loans made to governments by foreign
governments or forcign commercial banks are especially productive in this regard.
Periodic bond defaults by civil governments are healthy for capital markets: they
remind creditors not to loan money to institutions that are as wasteful and corrupt
as modern civil governments. Investors should loan their money lo productive
enterprises, not governments, except in emergency situations such as wartime
(maybe). The only other justification for lending to civil governments is in cases
where private debtors are even less reliable.

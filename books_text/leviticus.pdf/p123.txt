Priestly Representation 67

And thou shalt bring the meat [meal] offering that is made of these
things unto the Lorn: and when it is presented unto the priest, he shall
bring it unto the altar. And the priest shall take from the meat offering
a memorial thereof, and shall burn it upon the altar: it is an offering
made by fire, of a sweet savour unto the Lorn. And that which is left of
the meat offering shall be Aaron's and his son:
of the offerings of the Lorp made by fire (Lev.

  

‘The fire on the altar was God’s permanent, day-and-night
testimony of Elis wrath. The animal and agricultural sacrifices
placed on this fire produced a sweet savor for God (Lev. 1:9;
2:2; 3:5; 4:31). God delighted n the ritual burning of represen-
tative animals and meal. This symbolized (represented) God's
delight in the eternal burning of His enemies, angelic and
human (Rev. 20:14-15). This particular delight of God ought to
be the terror of man. The smoke ascending day and night from
God’s altar was to scrve as a reminder to man of what awaits
covenant-breakers in eternity. This was God's testimony in
history to the wrath that awaits covenant-breakers beyond histo-

ry.

The Salt of the Covenant

The meal offering, more than the other sacrifices, was the
sacrifice of the covenant. It was the one sacrifice in which salt
was specifically mentioned: “And every oblation of thy meat
{meal} offering shalt thou season with salt; neither shalt thou
suffer the salt of the covenant of thy God to be lacking from thy
meat offering: with all thine offerings thou shalt offer salt” (Lev.
2:13). This phrase, the sali of the covenant, ties this sacrifice to the
Bible’s system of covenantal subordination.

Why salt? First, it is an agency of incorruption, keeping
things from spoiling.? Second, salt imparts flavor. Third, and

3. Alfred Edersheim, The Temple: Its Ministry and Services As They Wore in the Time
of Jesus Christ (Grand Rapids, Michigan: Kerdmans, [1874] 1983), p. 109.

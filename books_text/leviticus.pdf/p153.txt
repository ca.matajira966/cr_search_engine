Corporate Responsibility 97

Sacrilege is the theft. of God’s property, This was Adam’s sin,
the primary sin in history.”” This sin was essentially priestly: a
sacramental boundary violation. Adam’s priestly sin extended
downward to his heirs, bringing death. In a similar sense judi-
cially, a priest under the Mosaic Covenant possessed delegated
authority, thereby enabling him to place the covenanted com-
munity under God’s condemnation. An unintentional sin com-
mitted by the priest was a greater threat than an unintentional
sin committed by the king. Conclusion: the judicial link between
the priest and the people was more binding covenantally in Israel than
the link between the king and the people.

This is evidence that the church is more fundamental than
the State in the political cconomy of the Bible. The church is
central to society: not the State and not the family.** The fami-
ly and the State have been more universal in time and place;
neither has been central in history. It is the ancient error of
natural law theory that has led pagan and Christian social theo-
rists to assume that the geographical universality of family and
State implies the social centrality of one or the other. On the
contrary, the formal preaching of the gospel and the adminis-
tration of the sacraments — inclusion and exclusion — are central
in history because they are central in eternity. (Note: the word
sacrament is derived from the Latin word sacramentum, a military
oath of cnlistment.” Sacraments are an aspect of point four of
the biblical covenant model: oath-sanctions.**) This does not

27. North, Boundaries and Dominion, Appendix A; “Sacrilege and Sanctions.”

28. The church perseveres institutionally in the resurrected world beyond the
final judgmem (Rew 21, 22). The family surely does not: “For in the resurrection
they neither marry, nor are given in marriage, but are as the angels of God in
heaven” (Matt. 22:30). The State apparently does not, since its judicial function is to
bring uegative sanctions against public evil, Public evil will end at the final judgment.

29, “Sacrament,” in Cyclopaedia of Biblical, Theological, and Ecclesiastical Literature,
edited by John M’Clintock and James Strong, 12 vols. (New York: Harper & Bros.,
1894), IX, p. 212.

80. Sutton, That You May Prosper, ch. 4.

 

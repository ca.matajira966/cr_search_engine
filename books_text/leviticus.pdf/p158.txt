102 LEVITICUS: AN #CONOMIG COMMENTARY

This tight covenantal relationship between sacramental
priesthood and congregation still exists. God expects men to
honor it. Nothing in the New Covenant has changed it. With-
oul specific New Testament revelation to the contrary, there is

judicial continuity [rom the Mosaic covenant to the New cove-

nant: the Ten Commandments, the statutes, and their required.
civil sanctions. This is both the testimony and the offense of
Christian Reconstruction. The New ‘Iestament’s standard for
civil government has to be the same as in Old Covenant law: a
theocratic republic. The biblical concept of civil authority man-
dates republicanism: public consent by representatives of the
nation to certain laws and forms of rulership (Ex. 19). A theo-
cratic republic preceded kingship in Israel. Theocracy — ic.,
rule by God — is established today through a biblically mandato-
ry Trinitarian civil oath. The alternative is cithcr another god’s
theocracy (e.g., Islamic nations and the State of Isracl} or politi-
cal polytheism, i.e., religious pluralism.‘ All ibcrals and most
fundamentalists agree: political polytheism is morally mandato-
ry for every nation except the State of Israel.” ‘his worldview
is a denial of the ideal of Christendom.

The theocratic status of a civid government is also manifested
by the presence of a priesthood. The congregation is a nation
of priests (Ex. 19:6); so is the New Covenant church (I Pet. 2:9).
This broad priesthood is represented before God in the church
by a sacramental priesthood, one which is responsible for ad-
ministering baptism and the Lord’s Supper. The covenantal
faithfulness of this sacramental priesthood is more important
for the preservation of continuity and peace in society than the

41. North, Political Polytheism, ch. 7.

42. Jacob Neusner (b, 193) is a Jewish conservative and the author, trauslatey
or editor of about $00 books. His bibliography fills over 23 single-spaced pages, He
reminds his readers: “We cannot build a decent society on secular foundations. Islam
knows that; Judaism knows that; why should Christians say any less?” Jacab Neusner,
“Who's Afraid of the Religious Right?” National Review (Dec. 27, 1993), p. 37. Yet he,
too, calls for a political alliance among Christians, Jews, and Moslems: political
pluralism.

   
  

  

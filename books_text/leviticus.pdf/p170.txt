114 LEVITICUS: AN ECONOMIC COMMENTARY

costs and rewards of production be rationally calculated by
society.

The Ability to Pay

All sin is an affront to God. ‘The rich man’s sin as well as the
poor man’s sin enrages God. But there is this distinction: the
rich man has sinned in the face of greater blessings from God.
He therefore owes more to God than the poor man does in
absolute terms. Making restitution to God is supposed to hurt,
but one man’s economic pain is another man’s economic de-
struction. Thus, sinners are to make restitution to God in terms
of the proportional benefils they expected to gain from their
sin.

A fundamental biblical principle is invoked at this point: from
him to whom much is given, much is expected. The context of this
rule is the imposition of God’s eternal sanctions (Luke 12:42-
48). If this system of proportional sanctions is true throughout
eternity, then it surely must be true in terms of the restitution
payments in history owed to God by men. Marx’s principle of
expectation and economic remuneration is therefore wrong:
“From each according to his ability, to each according to his
needs!” The first half of the statement is correct; the second
half is truc only in the case of the physically or mentally incom-
petent, or those who in the West were for centuries referred to
as “the deserving poor” ‘he general rule is this: “To each
according to market value of his actual production.” We know
this from the parable: “And the Lord said, Who then is that

Ll. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; ‘Iyler, Texas: Institute for Christian Economics, 1994), ch. 5, section on
“Costs of Ownership.”

12, Karl Marx, The Critique of the Gotha Program (1875); in Karl Marx and Freder-
ick Pngels, Selected Works, 3 vols. (Moscow: Progress Publishers, 1969), IT, p. 19.
Marx stofe this phrase from Morelly’s Code de la Nature (1755-60).

13, Peter Mathias, The First Industrial Nation: An Economic History of Britain, 1700-
1914 (New York: Scribner’s, 1969), p. 26.

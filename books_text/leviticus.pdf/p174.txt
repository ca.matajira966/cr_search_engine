118 LEVITICUS: AN ECONOMIC COMMENTARY

Conclusion

‘The Bible teaches the principle of proportional tithing and
proportional restitution to Ged. The problem with animal
sacrifices in the Old Covenant was that they could not be pre-
cisely proportional: men cannot slay just half an animal. Thus,
God imposed a system of different sacrifices for pcople of vary-
ing wealth.

The priests collected the sacrifices, and they could lawfully
use them personally: “And the priest shall make an atonement
for him as touching his sin that he hath sinned in onc of these,
and it shall be forgiven him: and the remnant shall be the
priest’s, as a meat offering” (Lev. 5:13). But these sacrifices
were not part of a predictable stream of income. These pay-
ments were the result of specific sins. These penalties were not
bascd on income but on the sinner’s total wealth; they were
specific restitution payments. They were the economic equiva-
lent of sin taxes — literal sin taxes to God through His church.
This system enabled men to reduce these sin taxes by sinning
less frequently.

The market value of these sacrifices was limited by the
wealth of the sinner. This was to make certain that every sinner
felt the appropriate pain of economic loss; it would remind him
of the eternal loss to come. There were “different strokes for
different folks” only to make sure that all the folks felt an appro-
priate degree of economic pain. Had the sacrificial system been
strictly a system of fines, the proportionality of the sanctions
would have been easy to maintain. Because a living animal is
not divisible on the same basis as monetary fines, God estab-
lished a system of differing sacrifices for the same transgression,
so that all transgressors were to feel a similar psychological
burden for their transgressions irrespective of their net worth.

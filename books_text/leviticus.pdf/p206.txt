150 LEVITICUS: AN ECONOMIC COMMENTARY

ally identified as a means of cleansing: “For on that day shall
the priest make an atonement for you, to cleanse you, that ye
may be clean from all your sins before the Lorn” (Lev. 16:30).

Ritual cleanliness was mandatory for a nation of priests (Ex.
19:6) that had been set apart (sanctified, madc holy) by God as
His special people. ‘Vhis national separation was the heart of the
Mosaic Covenant. Cleanliness laws were temporal boundary
devices that had a covenantal function for as long as the Mosaic
Covenant was valid, To enforce them, there had to be a priest-
hood for the nation of priests. Like the nation of priests, these
ordained priests had boundaries placed around them as a
separate family (Aaron) in a separate tribe (Levi). It was their
task to identify holiness and unholiness, cleanliness and un-
cleanliness (Lev. 10:10). As we find in the laws governing lepro-
sy, their very physical presence inside the boundary of a house
made unclean a house infected with the discase. It was not
legally unclean until a priest crossed its boundary. This was
analogous to the moral uncleanliness of Canaan, which became
judicially unclean — and subject to God’s corporate negative
sanctions — only after the Israelites had crossed the Jordan river
and entered the land.

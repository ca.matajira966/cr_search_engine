152 LEVITICUS: AN ECONOMIC COMMENTARY

it at the third-year feast. “And thou shalt bestow that money for
whatsoever thy soul lusteth after, for oxen, or for sheep, or for
wine, or for strong drink,! or for whatsoever thy soul desireth:
and thou shalt eat there before the Lorp thy God, and thou
shalt rejoice, thou, and thine household” (Deut. 14:26). Wine is
described in the Bible as a blessing from God (Deut. 7:13;
11:14; II Chron. 31:5). God even goes so far as to say that the
absence of wine is a sign of His covenantal curse against a cove-
nanted nation: “And he shall eat the fruit of thy cattle, and the
fruit of thy land, until thou be destroyed: which also shall not
leave thee either corn, wine, or oil, or the increase of thy kine,
or flocks of thy sheep, until he have destroyed thee” (Deut.
28:51), Why, then, this unique Mosaic Covenant prohibition
for the pricsts? Wenham understands that there is a problem
here. “The commands given to. Aaron, however, are strange.
Why should a ban on drinking alcohol be introduced here, and
then be coupled with instructions about teaching the Israel-
ites?”? He correctly identifics both aspects of the prohibition: 1)
clear-headed officiating over the administration of the sacrifices,
and 2) the teaching function of the priests. But he avoids dis-
cussing a very difficult and all-too-obvious problem: teaching by
the priests that took place outside the boundaries of the taber-
nacle and the temple. Why did the prohibition against winc
cease when the priest left the tabernacle? Wasn’t clear instruc-
tion in the word of God just as important outside the temple’s
boundaries as inside?

The ban did not apply to the Levites, yet they also had a
teaching function. Their office was lower than the priestly
oflice. They did not speak with comparable authority. Was this
additional authority of the priesthood an aspect of the ban?

1. This did not refer to Coca Cola Classic. Fundamentalist Christians and other
anti-alcohol legalists have great exegetical problems with this passage.

2, Gordon J. Wenham, The Book of Leviticus (Grand Rapids, Michigan: Eerdmans,
1979), p. 158.

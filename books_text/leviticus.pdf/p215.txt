Wine as a Boundary Marker 159

of the carth, and plenty of corn and wine: Let people serve
thee, and nations bow down to thee: be lord over thy brethren,
and let thy mother’s sons bow down to thee: cursed be every
one that curseth thee, and blessed be he that blesseth thee”
(Gen, 27:28-29). The boundary of wine for the priests testified
that Israel was under temporary constraints geographically; the
element of worldwide dominion.was not present to the same
extent that it is in the New Covenant.

Denying Expansion

‘The worldview of fundamentalism denies the reality of an
expanding kingdom in history, meaning before Christ returns
in person to set up an earthly knmgdom. The kingdom of God.
is said to be limited to the family and the church. In some
extreme formulations, the kingdom of God is equated only with
the church; even the family is understood te be outside it.
According to this view of history, a millennium of medicval
society was either at bottom religiously neutral or else it was not
really a society. This is the history of Western civilization ac-
cording to Voltaire, Diderot, the Enlightenment generaily, and
the standard American high school world history textbook. It is
fundamentalism’s worldview, too, which is why there is a con-
tinuing operational alliance between pietism and humanism.”

Grape juice is the pietist’s preference: a sweet, red liquid
that looks like wine but has no bite, bubble, or joy to it. Funda-
mentalists do not use wine in any form because wine can be
misused by undisciplined people. (They are not equally wary
about their diets and their weight. It is fermented sugar that
arouses their wrath, not unfermented.)"! The imagery of bro-

10, Gary North, Milennialism and Social Theory (lyler, Texas: Institute for
Christian Economics, 1990), pp. 43-44, 135-36, 144, 147, 151, 179-80, 958, 277-78.
11. Typical of the fumdamentalist mindset is the concordance at. the back of the
Scofield Refererice Bible (Oxford University Press, 1909). If you are trying 10 locate
Deuteronomy 21:20, “And they shall say unto the elders of his city, This our son is
stubborn and rebellious, he will not obey our voice; he is a glutton, and a drunkard,”

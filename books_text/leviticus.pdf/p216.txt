160 LEVITICUS: AN ECONOMIC COMMENTARY

ken wineskins also does not appeal to pietistic fundamentalists.
They want to keep those old wineskins intact. The thought of
cultural wine that breaks the institutional structures of society
is foreign to their thinking. Like wine, cultural and political
power can be abused, so they reject it as a matter of morality.
Thus, Christians are supposed to shun power, influence, and
culture in the same way that they are to shun wince. Culture
means dirty movies and perversion; people who even study
cultural affairs are risking being engulfed by a morally pollut-
ing worldliness.

Pietistic fundamentalists do not have confidence in those
fellow Christians who would exercise public authority in the
name of Christ and in terms of His law. They prefer to be
ruled by pagans. Similarly, they have no faith in culture.

Rendering Good Judgment

It is clear why liquor and justice do not mix. The ruler is
required by God to render judgment in His name. This judg-
ment must apply the general principles of biblical justice to
specific infractions. This work takes considerable skill. A person
who is under the influence of alcohol in this task is to that
degree not under the influence of God’s law. But why should
this not be true in every other instance? Why is the’ decision-
making of civil law so crucial? The answer is this: because the
civil magistrate renders judgment in God’s name.

Whenever good judgment is required for the safety of oth-
ers, equally rigorous standards are required. Pilots of airplanes
are not allowed to drink liquor for hours prior to flights. Were
il not so common for automobile drivers to drink before driv-
ing, thereby making it diflicult for prosecutors to get jurics to

you can find it by looking up the word “drunkard,” but not “glutton.” Similarly with
Proverbs 23:21; “For the drunkard and the glutton shall come to poverty: and
drowsiness shall clothe a man with rags.” There is no reference at all to “glutton” in
the concordance.

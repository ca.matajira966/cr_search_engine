168 LEVITICUS: AN EGONOMIG GOMMENTARY

times. The text actually says that the house was cleansed with
the blood of the dead bird, the running water, the living bird,
the cedar wood, the hyssop, and the scarlet (Lev. 14:52). The
legal status of unclean went from the house to the dead bird,
and from there to the live bird. ‘The priest then was to let the
living bird out of the city into the open ficlds, thereby making
an atonement for the house, in order to make it clean (Lev.
14:53). An unclean thing could not legally remain inside the
city. The bird flew away, carrying the unclean legal status of the
house. Conclusion: the threat was judicial; so was the cure.

Total Infection: Covenantal Inclusion

One of the most remarkable aspects of this plague was the
law governing the degree of affliction. “Then the priest shall
consider: and, behold, if the leprosy have covered all his flesh,
he shall pronounce him clean that hath the plague: it is all
turned white: he is clean” (Lev. 13:13), What this says is that if
an individual was completely covered with leprosy, turning his flesh
entirely whitc, he was then pronounced clean. This means that
he had legal access to the tabernacle or to any other element of
corporate worship in Israe]l. He posed no threat to his neigh-
bors, either ritually or biologically. [ce was not contagious.’ We
would normally think of the leprosy as being an affliction that
required him 1o be totally separate, permanently This is not
the case. A partial affliction of leprosy did require his separation.
So did all of the other sores and discolorations of the flesh that
are described in Leviticus 13. Nevertheless, the individual who
was completely afflicted became legally clean.

When an individual was so completely afflicted by the whit-
ening of his skin, he became like God: pure white (Dan, 7:9; Rev.
1:14). This is why God discusscs man’s sins as scarlet, and pro-
mises that they will be white as snow: “Come now, and let us

3. Gordon J. Wenham, The Book of Leviticus (Grand Rapids, Michigan: Rerdmans,
1979), p. 203.

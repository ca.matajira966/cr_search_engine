174 LEVITICUS: AX ECONOMIC COMMENTARY

Conclusion

Wenham has summarized the Levitical laws of quarantine.
Ile correctly relates them to the Levitical holiness laws.° The
quarantine laws of Leviticus had more to do with quarantining
the people from the presence of God than they did with quar-
antining sick people from healthy people. For example, a blem-
ished priest had to be kept away from God’s presence in the
temple (Lev. 21:17-21). ‘The laws of leprosy were related to the
temple’s laws of purity far more than they were to modern
public health laws, This is why any conclusions that we attempt
to draw from these laws must be done by analogy, not directly.

What can we say with confidence? First, the civil government
did possess lawful authority to remove urban residents from
their homes in order to protect others in the community from
the judgment of God. This judgment came in the form of
plague. The contagion was judicial, but the threat did cxist.

Second, the priest possessed the civil authority to remove
houses and people from a city. His judicial declaration as an
ecclesiastical agent had to be enforced by the civil magistrate.

Third, the victim of the plague had to bear the expenses
associated with the results of the quarantine. Because there was
no command in the Old Testament that the State support quar-
antined individuals, it is not possible to derive from this law any
biblical injunction for State welfare programs. The only legiti-
mate economic conclusion to draw from this law by analogy is
that there is no State welfare function. The job of the civil
government is to protect people from violence, not support
people who have been afflicted, either naturally or judicially.
To argue any other way is to make the State into an agency of
healing rather than an agency of protection. ‘The State is an
agency that is supposed to bring negative sanctions against evil-
doers. ‘here is no biblical warrant for the concept of the State
as a healer. ‘Che job of the State is to prohibit behavior that

9. Wenham, Leviticns, pp. 213, 214.

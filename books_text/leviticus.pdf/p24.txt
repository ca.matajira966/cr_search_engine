xxiv LEVITICUS: AN ECONOMIC COMMENTARY

however, necessarily see quile as clear a meaning in biblical
passages on economic conduct, the burdens of wealth and the
sin of greed.” An intelligent reader knows by now what is com-
ing, and it docs.

Gomes says that four biblical passages are customarily cited:
Deuteronomy 23:17, I Kings 14:24, I Kings 22:46, and II Kings
23:7, He says that these passages refer to prostitution, not
homosexuality. Quite true; this is why these passages are not
customarily cited, contrary to Professor Gomes. ‘The passages
that ave customarily cited are these: “Thou shalt not lie with
mankind, as with womankind: it is abomination” (Lev. 18:22).
“Ifa man also lie with mankind, as he lieth with a woman, both
of them have committed an abomination: they shall surely be
put to death; their blood shall be upon them” (Lev. 20:13).

Leviticus: Automatically Dismissed

These two passages present a problem for Professor Gomes.
He refers to them in his next paragraph. But he has an answer:
they are both in Leviticus, and you know what Ph.D.-holding
Harvard theologians think about Leviticus! These two passages
“are part of what biblical scholars call the Ioliness Code. The
code explicitly bans homosexual acts. But it also prohibits eat-
ing raw meat, planting two different kinds of seed in the same
field and wearing garments with two different kinds of yarn.
Tattoos, adultery and sexual intercourse during a woman's
menstrual period are similarly outlawed.” End of argument. He
then goes to the New Testament. Such is the state of theological
scholarship. today in America’s most honored university, found-
ed in 1636 by Calvinist Puritans.

The holiness code is not taken scriously by Professor Gomes
as a guide to modern behavior. I think it is safe to say that it is
not taken scriously by Harvard University. What is very likely
true is that it is not taken seriously by well over nine-tenths of
the evangelical Christian community. This is the problem that
this commentary seeks to overcome.

188 LEVITICUS: AN ECONOMIC COMMENTARY

‘The metaphor of vomiting symbolized a success[ul military
invasion of the land and its subsequent conquest. After the
exile, God’s people were no longer sovercign over civil affairs
in the land. The threat of invasion by a strange nation was no
longer a covenantal threat to Israel’s civil order, which was not
governed by God's covenantal hierarchy. In this sense, a boun-
dary violation of Israel's borders was no longer a major theo-
logical problem. Being vomited out of the land was no longer
a covenantal threat, except in response to their unsuccessful
rebellion against pagan civil authorities who were already in the
land. With the replacement of the temple by the church as
God’s dwelling place in history, it is Jesus Christ who now
spews His enemies out of [lis presence (Rev. 3:16).

The Replacement of the Promised Land

‘The kingdom of God cannot be confined geographically in
New ‘Testament times. Any nation can lawfully covenant with
God today. Israel was the single covenanted nation of Old
Testament, which alone acknowledged the sanctions of God and
the revealed law of God, and which alone required circumcision
of all its male citizens. Only onc other nation bricfly covenanted
under God, Assyria (under Jonah’s preaching), but this cove-
nant was soon broken.’ Today, however, there is no monopoly
of the Promised Land. All nations are required by God to cove-
nant with Him.* Their law structures are supposed to be bibli-

5. Sce North, ibid., ch. 10, subsection on “Lhe Church as the New Tempte.”

6. Gary DeMar, Ruder of the Nations: Biblical Blueprinis for Government (Ft. Worth,
‘Texas: Dominion Press, 1987).

7. This seems to have been a common grace covenanting process ~ formal public
obedience to the outward civil laws of the Bible — since there were uo covenantal
heirs remaining at the time of Assyria’s conquest of Isracl. Also, there is no indication
that they were circumcised as part of their national repentance.

8. Kenneth L. Gentry, Ju, The Greatness of the Great Commission: The Christian
Enuexprise in a Fallen. World (Iyler, ‘Texas: Institute for Christian Economics, 1990), ch.
10.

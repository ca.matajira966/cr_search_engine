Verbal Bonds and Economic Coordination 213

never intends to do it, and if all the facts were available, it
could be proven that he never intended to do it. The deliberate
writing of a check drawn against insufficient funds would be
such an act of fraud.* So would posting as collateral for a loan
an asset known by the holder to be worth less than what he
insists it is worth. So would gaining multiple loans on the basis
of one piece of collateral: fractional reserves.°

The Problem of Economic Coordination

The economic issuc that must be explained is the problem of
the coordination of individual plans. Tow is this best accom-
plished: By State compulsion or market coordination? It is clear
from both the cighth commandment and the tenth command-
ment that private property must be honored. Men must neither
steal nor covet their neighbor’s property. This means that bibli-
cal economics rests on the ideal of the legitimacy of private
property. “Christian socialism” is an oxymoron.’°

In a market cconomy, individuals make plans about the
future, and then they act in terms of these plans. They buy or
sell or hold in terms of their individual plans. The question
then is: [low are the millions of individual decisions integrated
with each other so that men can participate together in the
division of labor? This is the problem of the revision of eco-
nomic plans. How do people change their plans and expecta-
lions in response to the decisions of other individuals? This is
the problem of feedback: information coupled with sanctions.
In what form does information come to an individual that other
participants in the market approve or disapprove of what he is

8. In most states in the United States, this act constitutes a felony,

9. Gary North, Taals of Dominion: The Case Laws of Exodus (iyler, Texas: Institute
for Christian Economics, 1990), pp. 730-40; North, Honest Money: The Biblical Blue-
print for Money and Banking (Ft. Worth, Texas: Dominion Press; Nashville; Thomas
‘Nelson Sons, 1986), ch. 8.

10. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), Appendix DN.

 

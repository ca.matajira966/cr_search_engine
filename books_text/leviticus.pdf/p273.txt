Verbal Bonds and Economic Coordination 217

— clients ~ have to accept on faith what it is that they have just
signed.

Language has limitations. Every possible condition cannot be
included in a contract; hence, mutual trust is mandatory. This
trust can be abused by one party, So can the contract's lan-
guage. God serves as the final arbitrator in all contracts. He
knows each person’s intentions.

Another limit on contracts is the existence of clogged civil
courts, A contract may specily exactly what the other individual
is supposed to do, but if you cannot get that person into court,
the contract does you very little good. This is why mutual trust
is important. Nevertheless, people who trust each other should
still write contracts in order to settle differences later. Even if
the two parties presently agreeing to act together do not get
involved in a dispute, their heirs may later get involved in a
dispute. Still, we cannot expect contractualism to substitute for
trust and moral responsibility. We should not expect words
apart from intentions to protect us in all situations. We should
not trust the letter of the law to protect us from evil intentions
and the skilled misuse of language.

Mutual Trust

The socicty in which mutual trust is increasing is morc likely
to be a productive society. Men seek others who will deal hon-
estly with them.” The cost of policing contracts is reduced as
mutual trust increases. This is a form of sef- government instead
of civil government which becomes dominant. We have appeals
courts in a society: both church and State. Less pressure is
placed on these courts when mutual trust is increasing. This

13. “And it came lo pass at that time, that Abimelech and Phichol the chief
captain of bis host spake unto Abraham, saying, God is with thee in all that thou
docst: Now therefore swear unto me here by God that thou wilt not deal falsely with
me, nor with my son, nor with my son’s son: but according to the kindness that 1
have done unto thee, thou shalt do unto me, and to the land whercin thou hast
sojourned” (Gen, 21:29-23),

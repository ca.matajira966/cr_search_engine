Protecting the Weakest Party 223

19:15, namely, that God does not respect persons? On the
contrary, this case law affirms that. the deaf and the blind are
entitled to the same protection from cursing and tripping that
anyone is. But because they cannot bring a lawsuit on their own
behalf, a righteous person must do it for them. This upholds
the universal authority of God’s law.

I, Withholding Wages

The previous section of Leviticus 19 deals with theft through
fraud: the deliberately deceptive use of words (vv. 11-12).' The
first half of verse 13 repeats this warning. ‘Ihe second half adds
another form of fraudulent wealth transfer: the withholding of
a worker's wages overnight. This act is specified as fraud, and
it is also specified as robbery. ‘he question is: Why? If the
worker agrees in advance to wait longer than a day for his pay,
why should the law of God prohibit the arrangement? Or does
it?

It is always helpful in understanding a casc law if we can
first identify the theocentric principle that undergirds it. Verse
13 deals: with paying a debt. The employer-employee relation-
ship reflects God’s relationship to man. God provides us with
an arena: life and capital. Similarly, the employer supplies. an
employee with capital that makes the employee more produc-
tive. Man is dependent on God. Similarly, the laborer has
worked for a full day; the employer is required to pay to him at
the end of the work day. The context is clear: rapid payment for
services received. God employs us as His stewards. He gives us
the tools that we need to serve Him and thereby serve our-
selves. He always pays us on time. So should the employer. The
employer who withholds wages from his employees is making a
symbolic statement about God’s relationship to man: God sup-
posedly delays paying man what is rightfully owed to him. This
symbolism is incorrect. It testifies falscly about God’s character.

L. Sce Chapter 12.

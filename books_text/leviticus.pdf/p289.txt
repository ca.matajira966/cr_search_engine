Protecting the Weakest Party 233

As in the case of a crime, God is the primary victim.” The
witness serves as a spokesman for both God and the victim.
This law makes it plain that God expects others in the commu-
nity to take action and serve as covenant agents in the name of
the victims. How else could such public infractions of this law
be prosecuted? A verbal curse is a public act in defiance of
God’s law. Such public acts must be prosecuted, not just be-
cause they are morally wrong - many immoral acts cannot
legitimately be prosecuted under biblical law ~ but because they
are public. The public character of this form of cursing places
the integrity of the society on trial, for the victim cannot hear
and respond as God’s designated agent. If no witness intervenes
to bring formal charges, then God will take action against the
evil-doer and the society that has failed to protect the hand-
icapped victim from his persecutor.

Conclusion

Grammatically, verses 13 and 14 are not linked; ethically and
judicially, they are. The links are: 1) God’s protection of the
weakest members in society; 2) ways to overcome the limits on
men’s knowledge, especially the limits on the judges’ knowl-
edge. So, the judicial cases are different — theft vs. public assault
— but the general prohibition is the same: do not harm the
weakest parties.’

These case laws prohibit the victimization of the poorest and
weakest members of the community. The case law in verse 13
deals with theft from economically weak workers and also (indi-
rectly) the most impoverished workers in the community. The
most impoverished workers are those who cannot afford to
extend credit to their employer. They need to be paid at the

12. North, Tbols of Dominion, pp. 279, 296, 300.
13. North, Boundaries and Dominion, ch. 13, section on “The General Legal
Principle: Protecting the Weakest Party.”

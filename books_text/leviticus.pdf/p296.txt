240 LEVITICUS: 4N ECONOMIG COMMENTARY

The Theology of the Poor; ox Poor Theology

From the late 1960’s through the late 1980's, a movement
known as liberation theology had considerable influence on the
thinking of highly educated — i.e., humanist-certified — North
American evangelical Christians and Latin American Roman
Catholic priests.” This movement developed out of a self-con-
scious attempt by Communists and [ar-Left heretical Christian
groups to fuse Marxist social diagnoses and solutions with bibli-
cal rhetoric.’ his phrase became the rallying point of the
liberationists: “God is on the side of the poor.” Is this phrase
truc? No, and Leviticus 19:15 is the most obvious passage in the
Bible demonstrating the phrase’s [alschood. Hardly less power-
ful in this regard is Psalm 62:9: “Surely men of low degree are
vanity, and men of high degree are a lie: to be laid in the bal-
ance, they are altogether lighter than vanity.” Conclusion:
“Trust not in oppression, and become not vain in robbery: if
riches increase, set not your heart upon them” (Ps. 62:10). In
short, judge rightcously.

Whose Side Is God On?

‘The Bible says specifically that God is on the side of the
righteous. Occasionally, the Bible does say that God identifies
with certain members of the poor. The poor who are poor not
by their own fault, and especially those who are poor because of
oppression by others, become identified with God by Gad’s

7. The major English-language publishing house for liberation theology is Orbis
Books. The major ceclestastical organization is the Roman Catholic Maryknoll order.

8. Introductory books, critical of the movement, are Michael Novak (ed),
Liberation North, Liberation South (Washington, D.C.: American Enterprise Institute,
1980); Gerard Berghoet and Lester DeKoster, Liberation Theology: The Church's Future
Shock (Grand Rapids, Michigan: Christian Library Press, 1984); James V. Schall, S.J.,
Liberation Theology in Latin America (San Francisco: Ignatius Press, 1982); and Ronald
Nash (ed.}, Liberation Theology (Milford, Michigan: Mott Media, 1984). A neo-orthodox
critique is J. Andrew Kirk, Liberation Theology: An Evangelical View from the Third World
(Atlanta, Georgia: John Knox Press, 1979).

 

 

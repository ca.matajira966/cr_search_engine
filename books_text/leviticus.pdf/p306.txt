250 LEVITICUS: AN ECONOMIC COMMENTARY

tified as the binding judicial standard for all civil judgment,
irrespective of the national and covenantal origins of residents
within the land.’ Second, local judicial participation: the law is
given to people in a particular community. Law enforcement is
always to begin with self-judgment. The formal cxercise of cove-
nantal judgment then extends to local covenantal institutions:
church, family, and State. This indicates that jurors and judges
in the first stage of civil court proceedings must be recruited
from the local community. Their attitudes will inescapably be
shaped by that community. Acknowledging both the reality and
the legitimacy of this institutional arrangement, Leviticus 19:15
emphasizes the necessity of righteousness. It is this fusion of
God's universal standards with honest and impartial judgment
according to local customs and circumstances — the one and the
many — that is the basis of the development of the godly civil
order?

Judicial Localism

In a political order that is structured in terms of biblical
standards, politics is inherently local. The reason why this is
true is that politics is an aspect of the civil judicial order. Poli-
tics is an aspect of civil judicial sanctions. It is the means by
which those who are lawfully represented in the civil realm are
given an opportunity periodically to sanction their judicial
representatives: legislators, judges, and governors. ‘This is the
Bible’s authorized means of allocating lawful civil authority.
This is why all politics is inherently a form of the judiciary.’
Politics is an outworking of the civil office of judge.

1, Sce Chapter 14.

2. Gary North, Moses and Pharaoh: Dominion Religion us. Power Religion (yler,
Texas: Institute for Christian Economics, 1985), ch. 14.

3. On the 60-year transformation of the Massachusetts General Court into a
purely legislative body, see Gary North, Boundaries and Doiminion: The Economics of
Leviticus (computer edition; Tyler, Texas: Institute for Christian Economics, 1994),
Appendix F: “The Pig ‘hat Changed American Government.”

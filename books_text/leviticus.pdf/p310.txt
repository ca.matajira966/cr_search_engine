254 LEVITICUS: AN ECONOMIC COMMENTARY

judicial system’s scarcest cconomic resources — those people
who possess progressively better-informed judgment — for the
progressively more difficult cases.

Judicial resources, being scarce, had to be allocated by
means of some principle. This was not the free market princi-
ple of “highest bid wins.” Civil and ecclesiastical justice may not
lawfully be purchased. But without price allocation, there was
only one other alternative means of rationing civil justice: stan-
ding in line. Jethro’s system transformed the single long linc in
front of Moses’ tent into tens of thousands of shorter lines.
Rashi,’ the late eleventh-century French rabbinic commentator,
estimated that in Moses’ day, there would have becn 78,600
judges in four levels.®

The Intellectual Division of Labor

Localism is extremely important for the advancement of
what I call the division of judicial labor. The concept of the
division of labor is basic to the Bible. We see it in a primarily
negative sense in the scattering of familics at the Tower of
Babel.° We sce it in a positive sense in Paul’s injunction that
the simple man or the man of one primary skill not fecl bad
because he does not possess a skill that a more prestigious
individual has. In both I Corinthians 12 and Romans 12, Paul

7. Rabbi Solomon (Shlomo) Yizchaki.

8. His reasoning: 600 at the top — judges of thousands (600,000 men divided by
1,000); 6,000 in the upper middle — judges of hundreds (600,000 men divided by
100); 12,000 in the lower middle — judges of fifties (600,000 men divided by 50); and
60,000 lower court judges ~ judges of tens (600,000 men divided by 10). Chumash
with Targum Onkelas, Haphtaroth and Rashi’s Commentary, A. M. Sibermann and M.
Rosenbaum, translators, § vols, (Jerusalem: Silbermann Family, [1984] 1985 [Jewish
year: 5745), I, p. 95. Rashi served as a rabbinic judge, and difficult cases were
continually sent to him from Germany and France. Heinrich Graetz, History of the
Jews, § vals. (Philadelphia: Jewish Publication Society of America, [1894] 1945), IIT,
p. 287.

9, Not entirely a negative sanction. See my comments in The Dominion Covenant:
Genesis (2nd ed; Tyler, Texas: Institute for Christian Economics, 1987), ch. 15: “The
World Trade Incentive.”

 

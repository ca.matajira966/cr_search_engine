260 LEVITICUS: AN ECONOMIC COMMENTARY

The Biblical Jury

The goal of the biblical jury system is not to create new laws
but rather to apply fixed biblical laws to specific cases. The
function of the jury is to bring a small number of individuals
into court so they can hear the disputes between individuals
who have not been able to settle their disputes outside of the
civil court. This is the principle of the division of labor. Many
minds are focused on the details of a single case. After hearing
both sides, the American jury is sequestered into a private room
where members can discuss the case secure from interference
or the threat of subsequent retaliation against any individual
jery member. Neither the judge nor the agents of the dispu-
tants are allowed to enter this room when the jury is in session.
‘Lhis is a sign of its sovercignty. When the common law rule
against double jeopardy is honored, the American jury becomes
the final court of appeal when it issues a “not guilty” verdict.

The jury publicly announces civil judgment: guilty or inno-
cent. This is the same judicial principle that operates in demo-
cratic balloting. It is a manifestation of point four of the biblical
covenant model: the imposition of sanctions. The Anglo-Ameri-
can institution of the secret jury rests on the legal principle that
no outside agent is authorized to bring pressure of any kind
against the decision-makers who sit on that jury. No kind of
public pressure, no kind of economic pressure, and no kind of
threat is legal to be brought against a jury. Tampering with a
jury is a criminal offense. By sequestering the jury — by placmg
a judicial and physical boundary around the members in their
collective capacity as jurors — the judge pressures the members
of the jury to focus all of their attention on the details of the
particular case, rather than worrying about what their opinions
or decisions will produce in response within the community.

‘This is indirect evidence that the modern political practice of
the secret ballot is analogous to the sequestered jury.” When

 

23. ‘Lhe practice first began in Great Britain in 1662, when the Scottish Parlia-

268 LEVITICUS: AN ECONOMIC COMMENTARY

defense is acting as a lawful agent of God. ‘There are civil laws
governing this God-granted authority to kill another person.
The case law of Exodus 22:2 allows a householder to kill a
burglar if the owner catches him while the intruder is breaking
in. The intruder has no legitimate reason to be inside the
house. The resident has a legitimate role as a defender of his
household’s boundaries. God has delegated this authority to
him. The occupant cannot know for sure why the invader has
entered his home without permission, so he is allowed by God’s
Jaw to assume the worst: the invader is a potential murderer
He can lawtully be killed by the person who resides there. The
mere transgression of the home’s boundary is sufficient to
remove the protection of God’s civil law from the invader. If
caught by the homeowner and threatened with a weapon to
prevent his flight before the police arrive, the invader is not
protected by God’s law from execution should he attack the
homeowner. Those lawfully inside the house are protected by
God’s law; therefore, the invader is not. The thief may be
struck while breaking in. If he attempts to flee, the resident is
not supposed to kill him, for he is no longer breaking in, But
the benefit of the doubt is always with the defender, This exe-
cution of an illegal invader is not an act of personal vengeance;
rather, it is an act that defends a lawful boundary. The defend-
er acts in the name of the State and is authorized by the State
because no policeman is available to enforce the law.

By implication, this case law establishes the judicial plea of
sell-defense. The person who is given cause to belicve that an
assailant is ready to kill him is entitled to kill the assailant. The
civil government is required by God to investigate the reasons
for any killing of a human being.® The judges must examine
the cvidence in order to determine whether a murder trial

9, ‘Lhe passage that establishes this requirement is Deuteronomy 21:1-9, which
requires a special sacrifice when a body is found outside a city, and the elders cannot
discover who committed the crime,

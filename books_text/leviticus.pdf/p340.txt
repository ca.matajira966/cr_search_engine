284 LEVITICUS: AN ECONOMIC COMMENTARY

allowed to become a citizen in Isracl. (The New Testament’s
system of adoption has annulled this law: Acts 8:26-38.)'°

Artificial Barriers

The boundaries separating animals had to be there because
of the normal sexual bonding that takes place among pairs
within a species. So, too, was it normal for members of the
same covenantal conlession to marry. But Mosaic law estab-
lished an artificial harrier between the tribes. This artificial barri-
er was both legal and economic: landed inheritance. lribal sepa-
ration decentralized Israel’s economy and politics. There was to
be continuity of theological and judicial principles, one tribe to
another. Plots of land could not be merged beyond the jubilee.
Kings and Levites - the national enforcers of God’s law — could
not pursue judicial centralization through either land purchase
or intermarriage. This prevented what Pharaoh and the priests
had done under Joseph (Gen. 47:20-22) — a curse on Egypt
consistent with Egypt's theology of the divine Pharaoh.

Thus, the prohibition against the interbreeding of animals
and the mixing of sceds had to do with keeping separate artificially
what is normally mixed. Fenced family fields inside Israel reflected
the nation’s tribal boundaries. Such tribal separation was abnor-
mal, not normal. What is abnormal is the separation of breeds
within a species and the separation of crops within a single
fenced field. What is also abnormal is the separation of a bibli-
cally covenanted people. This abnormality was essential to the
maintenance of the tribal structure in Isracl. Inheritance in the
land was by tribal separation, but only until Shiloh at last ar-
rived. The internal boundaries would disappear once Shiloh
came.'!

10. See below, p. #71.

U1, The absence of tribal membership in Judaism indicates that this prophecy
was fulfilled prior to AD, 70.

304 LEVITICUS: AN EGONOMIC COMMENTARY

fruit of new trees planted in Canaan. The fruit did not belong
to the owner until aller the holy feast of year four. That is, he
took possession of the fruit in year five. This parallels Isracl’s
taking possession of Canaan during decade five. This four-year
prohibition pointed symbolically back to Israel’s rebellion in the
wilderness: four decades of deferred possession.® This seed law
for orchards referred back to the unique historical experience
of the conquest generation: Israel’s seed.

This ban was not symbolic of the separation between Israel-
ites and resident aliens, a separation that did not involve a four-
year wailing period.® This law had nothing to do with the agri-
cultural laws of the Hammurabi Codc.’ Contrary to Rabbinical
interpretations, it had nothing to do with man’s supposedly
animalistic nature, which requires the discipline of' waiting, i-e.,
the phenomenon Ludwig von Mises called time-preference.®

The Economics of Restricted Access

‘There is no doubt that one economic effect of this law was to
force the orchard’s owner to forego three years’ worth of the
orchard’s output before he could celebrate before the Lord in
year four. No doubt this law did pressure obedient men to
count the costs of their decision: planting an orchard vs. plant-
ing something else (or planting nothing). But being required to
count the costs of our actions is not in and of itself an incentive

5. It also pointed forward t the ministry of Jesus Christ. Sce Gary North,
Boundaries and Dominion: The Economics of Leviticus (computer edition; Tyler, Texas:
Institute for Christian Economics, 1994), ch. 18, subsection on “Eschatological
References.”

 

6, The third-generation circumcised Egyptian or Edomite became a citizen
(Deut. 23:8). The third-generation member of these two nations was therefore
regarded as the judicial equal of a circumcised Israclite. (He could not inherit land
in the countryside, however: Leviticus 25.) But the third-year fruit was still uncircum-
cised. So, there is no analogy between these two forms of circumcision.

7. ibid., cb. 18, section on “The Initially Confusing Economics of This Law.”

8. Tbid., ch. 18, section on “Rabbinical Interpretations.” I refer here to Rashi and
8. R. Hirsch.

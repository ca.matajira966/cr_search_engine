314 LEVITICUS: AN ECONOMIC COMMENTARY

another set for selling these goods. He was not allowed to use
one set of weighis for some customers and another sct for other
customers. To do so would have testified to the existence of a
God who imposes IJis law’s standards in a partial manner. That
is, it would have pointed to a God who shows favor to certain
persons: one faw for one group, another law for a different
group. Again and again in Scripture, this is denied emphatical-
ly.° The essence of God’s moral character, and therefore of Iis
character as a judge, is the consistent application of His law.

Accompanying this law was an affirmation of God’s character
as a consistent judge, which also served as an implicit warning to
the nation of Isracl: “I am the Lorp your God, which brought
you out of the land of Egypt.” God had brought negative sanc-
tions against the Egyptians for their unrighteous behavior; He
would do the same to Israel. Ue said this explicitly just before
the next generation entered the land of Canaan: “And it shall
be, if thou do at all forget the Lorv thy God, and walk after
other gods, and serve them, and worship them, I testify against
you this day that ye shall surely perish. As the nations which
the Lorn destroyeth before your face, so shall ye perish; be-
cause ye would not be obedient unto the voice of the Lop your
God” (Deut. 8:19-20),

A State that applies God’s law impartially on all has obeyed
the injunction to love men. It must provide equal access to
justice,’

Just Measures and a Just Society

The familiar Western symbol of justice is the blindfolded
woman holding a balance scale. The blindfold symbolizes the
court’s unwillingness to recognize persons. The scale symbolizes

9. See the citations in Chapter 14, p. 239, footnote 6.

10. North, Boundaries and Dominion, ch. 19, section on “Open Access and [mpar-
tial Justice.”

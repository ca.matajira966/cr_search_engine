318 LEVITICUS: AN ECONOMIC COMMENTARY

chefs without one. Their skills are intuilive, not numerical. This
is true in every field.

‘There are limits to measurement because there are limits to
our perception. There are also limits on our ability to verbalize
or quantify the measurements that we perceive well enough to
act upon. Oskar Morgenstern addressed this problem in the
early paragraphs of his classic book, On the Accuracy of Economic
Observations.* Our economic knowledge is inescapably a mix-
ture of objective and subjective knowledge.”* That is to say, we
think as persons; we are not computers. We do not think digi-
tally. We think analogically, as persons made in God’s image.
We are required to think God’s thoughts afier Him. To do this,
we need standards provided by Cod that are perceptible to man. God
has given us such standards (point three of the biblical covenant
model). We also need to exercise judgment in understanding
and applying them (point four). This judgment is not digital; it
is analogical: thinking God’s thoughts after Him. We are re-
quired by God to assess the performance of others in terms of
Goda’s fixed ethical and judicial standards.

In order to achieve a “fit” between God’s standards and the
behavior of others, we must interpret God’s objective law (a
subjective task), assemble the relevant objective facts (a subjec-
tive task), discard the irrelevant objective facts (a subjective
task), and apply this law to those facts (a subjective task). ‘he
result is a judicially objective decision. At every stage of the
decision-making or judgment-rendering process, there is an

14. Oskar Morgenstern, On the Accuracy of Economic Observations (2nd ed.; Prince-
ton, New Jersey: Princeton University Press, 1963). Morgenstern wrote a book on
game theory with John von Neumann, one of the most giftcd mathematicians of the
twenticth century, Morgenstern was aware of the limits of mathematics as a tool of
economic analysis. A more recent treatment of the problem is Andrew M. Kamarck’s
Economics and the Real World (Philadelphia: University of Pennsylvania Press, 1983).
See also Thomas Mayer, Truih versus Precision in Economics (Hampshire, England:
Flgar, 1993).

18. Morgenstern, Accuracy, pp. 3-4.

 

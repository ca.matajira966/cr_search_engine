338 LEVITICUS: AN ECONOMIC COMMENTARY

task of the expositor to sort out — scparate — these implications
in terms of the biblical principle of holiness.*

This law recapitulates the warning in Leviticus 18:28: if they
commit the cvil acts that the Canaanites committed in the land,
the land will vomit them out. They were required to obey God's
revealed law. I have argued that this threatened negative sanc-
tion was an aspect of the land laws of Israel, confined geo-
graphically to the Promised Land, and annulled in A.D. 70 with
the final annulment of the Old Covenant.” The office of “cov-
enantal vomiter” has been taken by the resurrected Christ (Rev.
3:16). The land no longer acts as a covenantal mediator be-
tween God and man, either in Palestine or elscwherc. It does
not provide covenantally predictable sanctions in the New Covenant
era. But the Promised Land did do this under the Mosaic Cov-
enant.

A Separate Land for a Separate Nation

In this passage, we find four basic themes of the Book of
Leviticus: obedience to God’s revealed law, covenantal separa-
tion, national holiness, and the inheritance of the land. Actual-
ly, the third theme, national holiness, is another way of express-
ing the first two themes. God compares the religious boundary
around the people of Isracl with the geographical boundary
around the land itself, The continuing covenanial separation of
the nation of Israel could be secured only by obedience to
God’s law, not by a strictly military defense of the nation’s
geographical boundaries. Secure geographical boundaries for
Israel would be the product of covenantal faithfulness, not
military strength as such (Ps. 20:7; Isa. 31:1).

The Promised Land’s geographical boundary had formerly
surrounded the nation — singular - that had occupied the land.

1, Rival principles of interpretation have divided me from Rushdoony at this
point: the interpretation of the dietary laws.

2. See above, Chapter 10: “The Promised Land as a Covenantal Agent.”

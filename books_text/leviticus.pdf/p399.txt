Inheritance Through Separation 343

superior results in the lives of covenant-keeping people. Insist-
ing on a Levitical diet would have been an act of religious and
political rebellion: the preservation of a defeated nation’s diet.
‘Vhat was not their point. It was not that the Israelites possessed
a uniquely healthy dict that had to be preserved outside the
land; rather, it was the preservation of their covenantal commit-
ment to the God of Israel, whose sovcreignty extended beyond
the land. While the young men did not request food that was
prohibited by Leviticus, they also did not request the blessings
— “fat” — of the Levitical diet: the best of the land. This should
warn us: the Levitical dietary laws were laws furthering covenanial
separation inside the Promised Land, not universal laws of health. To
misunderstand this is to misunderstand covenant theology. To
deny this is to deny covenant theology and replace it with “taste
not—touch not” religion,

If the captive Isracliles were requircd to honor the Mosaic
dictary laws outside the Promised Land, how did Esther conceal
her identity from her husband and Haman? Or was she in
rebellion? Did God deliver His people from their enemies by
means of a woman who openly defied God’s law? Or is there a
theologically simpler answer, namely, that the Israelites lawfully
ignored the dietary law’s requirements when they were in cap-
tivity outside the land, i.e., under the God-ordained authority
of a rival civilization?

A Temporarily Marked-Off Nation

‘The dietary laws were imposed by God belore the nation
came into the Promised Land but after the Israelites had left
Egypt. ‘hese laws were given early in the wilderness experi-
ence. Throughout the 40 years, the people ate mostly manna.
They were forced to refrain from newly prohibited foods, what-
ever their dietary tastes had been in Egypt. Therefore, these
food laws were preparatory for the invasion. Manna, coupled.
with the food laws, forced the younger gencration to grow up
completely unfamiliar with the taste of covenantally prohibited

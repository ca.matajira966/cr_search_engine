Preface xii

valid and have their place in a comprehensive, integrated bibli-
cal hermeneutic. What I am saying is this: the five-point cove-
nant model is the model for covenants, ie., judicial bonds that
are lawfully established by a self-maledictory oath under God.
There are four ~ and only four - such covenants: personal,
ecclesiastical, family, and civil. The five points of the biblical
covenant model are:

1, Transcendence/immanence/sovereignty
2. Hierarchy/representation/authority

3. Ethics/boundaries/dominion

4, Oath/judgment/sanctions

5. Succession/inheritance/continuity

The acronym in English is ‘"THEOS, the Greek word for God.
I am not alone in my surprise. When I hired David Chilton
Lo write a commentary on the Book of Revelation, neither of us
imagined that his Days of Vengeance (1987) would also wind up
as an eloquent footnote to Sutton’s That You May Prosper (1987),
but it did.’ Prior to Sutton’s discovery, Chilton had been totally
bogged down for over a year, unable to complete the book’s
manuscript. After he heard Sutton present his discovery at a
Wednesday evening Bible study that he and I attended, Chilton
re-structured the manuscript, added some new material, and
completed it within a few months. Seven years after its publica-
tion, critics have not yct attempted to refute Chilton’s book, let
alone Sutton’s. (Note: a brief negative book review is not a
refutation. Rather, it is a public notice of the need for one.)
While I have never been bogged down with any volume in
this sct of economic commentaries, there is no doubt that ‘ols
of Dominion and especially Boundaries and Dominion would have
looked very different if Sutton had not made his discovery, and

3. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987).

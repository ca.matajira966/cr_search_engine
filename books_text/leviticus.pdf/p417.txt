Mutual Self-Interest: Priests and Gleaners 361

tion. While there may be individuals who at times place the
interests of the community, or some segment of the community,
above their own personal self-interest, no society can safcly be
constructed which relies exclusively on the widespread voluntary
suppression of personal self-interest among those who are given
monopolistic authority to impose negative sanctions on others.
Liberty and justice require that the legal order acknowledge the
fact that the personal self-interest of judges must be dealt with
institutionally. Negative sanctions must be brought against those
officials who make decisions that favor their interests at the
expense of segments of the gencral public.

This institutional guideline is true for non-profit organiza-
tions, not just civil governments. I would not go so far as to say
thal it is equally truc of priesthoods, since priesthoods formally
are committed to a doctrine of sanctions beyond physical death,
either a final judgment imposed by a divinity or the judgment
of the impersonally applied moral laws of karma: an extension
of the results of personal behavior through reincarnation. Thus,
a priest may have a concept of personal self-interest that is
longer or more apocalyptic than that adopted by a civil judge,
or even more to the point, by a twentieth-century academic
economist. But even non-profit organizations and priesthoods
must acknowledge the potential conflict of intcrests between the
power to impose negative sanctions and the public interest.
Rewards and punishments must be built into the institutional
system in order to reduce the profitable exploitation of such
conflicts of interest, since the public interest will normally be
sacrificed in these conflicts.

Public Choice Theory

In order to understand and then predict the decisions made
by sanctioning agents, we need to consider the influence of self-
interest. If we want to increase the likelihood that people will
act in a particular way, we must see to it that they are rewarded
for performing in the preferred way and punished for deviat-

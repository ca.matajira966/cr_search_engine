INTRODUCTION TO PART 5

And ye shall hallow the fiftieth year, and proclaim liberty throughout
al the land unto all the inhabitants thereof: it shall be a jubile unte you;
and ye shall return every man unto his possession, and ye shall return
every man unto his family (Lev. 25:10).

The fifth section of Leviticus begins with Chapter 25, which
lists the laws governing the jubilee year: inheritance inside the
land’s boundaries. The remainder of Leviticus deals with inher-
itance.

Modern evangelical theologians remain totally hostile to the
theonomists’ principle of biblical interpretation: any Mosaic law
not annulled by the New Covenant is still judicially binding on
church, State, or family. Nevertheless, prominent evangelical
social commentators — though not the theologians — of the far
right and the far left remain fascinated with the jubilee laws of
Leviticus 25.

This is a very curious phenomenon. The jubilee laws were
explicitly ticd to the Promised Land. They were laws governing
the sale of real estate and people. They were not revealed by
God prior to the exodus, and they applied to no region on
earth prior to the conquest of Canaan. Why should evangelical
Protestant social commentators who denounce theonomy’s
hermeneutic of judicial continuity also proclaim the benefits of
the jubilee laws? Is there some hidden agenda at work here?

Boundaries of the Jubilee Land Laws 419

marry, which is why the replacement rate is not 2.0 children.)
So, there must have been about 2.4 million people at the time
of the exodus: two adult parents and about two children per
family.”

They entered a land of about six and a half million acres.*
This meant that the average [amily, had there been no cities,
would have owned about 11 acres.** Not all of this land was
arable. Some of it was taken up by cities, where the Levites
lived. Over time, the number of acres per “nuclear” family
unit” would have declined as population rose. If Israc]l had
remained faithful to God’s law, miscarriages would have ceased.
The early Egypt-era rate of growth of Israelite nuclear familics
would have resumed. No nuclear family could have inherited
more than a declining number of acres as time went on. Even-
tually, no farm would have been large enough to support all
the heirs.

Consider a rate of population growth of 3 percent per an-
num, which has been sustained by many agricultural nations in
the twentieth century. This rate of increase would have doubled
the size of the population in a quarter of a century.”° By the
first jubilee, the average farm would have been down to just
under three acres (11 divided by 4). By the second jubilee, the
average farm would have becn under .7 acre. And so on.

This would have forced the creation of extended family
agricultural corporations, with one or two nuclear families (or

22. North, Moses and Pharaoh, ch. 1

23. ‘The land was no more than 10,330 square miles. Barry J. Beitzel, The Moody
Alas of Bible Lands (Chicago: Moody Press, 1985), p. 25. There are 640 acres per
square mile. This means 6,861,200 acres.

24, 6,661,200 acres divided by 601,730 families = 10.98 acres per family. This
was comparable to the 4 to 15 acres owned by the average Roman farmer around
200 B.G. “Agriculture, history of,” Software Toolworks Hustrated Encyclopedia (1990).
‘Lhis is Grolier’s Encyclopedia on a CD-ROM disk.

25. Contrasted with the extended family.

26. This is based on the “law of 73”: divide the annual rate of growth into 73 in
order to discover the doubling period.

 

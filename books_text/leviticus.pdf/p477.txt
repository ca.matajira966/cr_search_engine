Boundaries of the Jubilee Land Laws 421

Declining Per Capita Farm Income

The jubilee mheritance law was a way to guarantee every
head of household a small and declining share of income from
a family farm. Most heirs would have become urban residents
in Israel or emigrants to other nations. The promise of God
regarding population growth — being fruitful and multiplying
— was a guarantee that covenantal faithfulness would lower the
proportion of per capita family income derived from farming.
The law made it plain to cveryone except modern Bible com-
mentators that if the nation’s numbers grew as a result of God’s
blessing, Israelites could place little hope in the possibility of
supporting themselves financially as farmers. Far from being a
guarantor of egalitarianism, the jubilee inheritance law was a law
forcing covenant-keeping people into the cities or out of the nation.

Real estate located inside walled cities did not come under
this law. Neither did property owned or leased outside the
boundaries of Israel. This law warned them that @ covenanially
faithful nation would become an urbanized nation and/or a nation of
emigrants. The law made it plain that their lives as farmers could
continue only if they were not faithful to God’s law. If the
nation remained primarily agricultural, this was God's visible
curse against them.

The jubilee land inhcritance law was designed to force the
Israelites to plan for a very. different future. They were to be-
come city dwellers as a people within the Promised Land, and
traders, bankers, and skilled manufacturers outside the land.
There could be no legitimate hope in remaining farmers in the
Promised Land. The boundaries of the land were fixed; their
population size was not. There would eventually have to be
expansion beyond the: boundaries of Israel, and there would
have to bea concentration of population in Isracl’s cities. Like
the garden east of Eden, the family-owned gardens of Israel
would be temporary dwelling places of preliminary training for
worldwide dominion. The faster the population grew, the faster
their life as farmers and animal herders would disappear. What

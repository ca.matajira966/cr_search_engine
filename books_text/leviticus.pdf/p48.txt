xlviii LEVITICUS: AN EGONOMIG COMMENTARY

ing judgment of wandcring in the wilderness for forty
years.”® Furthermore, “Israel as a nation is in its infancy at
the outset of this book, only thirteen months afier the exodus
from Egypt. In Numbers, the book of divine discipline, it be-
comes necessary for the nation to go through the painful pro-
cess of testing and maturation. God must teach His people the
consequences of irresponsible decisions. The forty ycars of
wilderness experience transforms them from a rabble of ex-
slaves into a nation ready to take the Promised Land. Numbers
begins with the old generation (1:1-10:10), moves through a
tragic transitional period (10:11-25:18), and ends with the new
gencration (26-36) at the doorway to the land of Canaan,”*°

Deuteronomy

Deuteronomy is the book of Isracl’s inheritance, point five of
the biblical covenant model. “It is addressed to the new genera-
tion destined to possess the land of promise — those who sur-
vived the forty ycars of wilderness wandcring.”*! The children
of the generation of the cxodus renewed their covenant with
God and inherited Canaan on this basis. Moses blessed the
tribes (Deut. 33), a traditional sign of inheritance in the Old
Testament (Gen. 27; 49). Moses died outside the land, but
before he died, God allowed him to look from Mt. Nebo into
the promised land (Deut. 34:4). He saw the inheritance. The
book closes with the elevation of Joshua to leadership, the
transitional event of inheritance or succession (Deut. 34:9-12).

‘Those who reject Sutton’s thesis need to present an alterna-
tive model of the Pentateuch, onc which fits it better, and one
which also fits the Ten Commandments better, since they are
also structured in terms of the five-point model: 1-5 and 6-10.
Critics need to pay attention that old political aphorism: “You

19. Open Bible, p. 127.
20, sbid., p. 128.
21. Ibid, p. 171.

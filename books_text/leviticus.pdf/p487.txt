Boundaries of the Jubilee Land Laws 431

to be worshipped publicly in Israel. Thus, the gods of the land
had to be removed from public view. To achieve this initially,
the Israelites were told by God to exterminate or drive out
every person dwelling in the land. Only Rahab and her family
would be allowed by God to escape this judgment, for she had
established a pre-invasion covenant with God.”

Third, His holiness was to be defended by enforcing a law
that kept post-conquest immigrants from ever owning property
in Isracl except inside Isracl’s walled cities. The families of the
conquest received an inheritable lease that could not be alienat-
ed beyond 49 years. Later immigrants could sublease rural
property if they were sufficiently productive, but they could not
leave an inheritance beyond the jubilee year.

Fourth, God established a law that removed from the majori-
ty of the population any legitimate hope of remaining farmers
in Israel if His blessings were forthcoming in response to their
covenantal faithfulness as a nation. They. surely knew that, as
Israel’s population expanded, no branch of any extended fami-
ly could retain economic control over of a particular plot of
rural land apart from the compliance of all the other members
of the family, except perhaps as a small recreational property (a
consumer good). If they wanted income from the land, they
could attain it only through its productivity. Small, isolated
plots are not very productive. If they wanted to maximize their
passive income from their portion of the extended family’s
land, they would have to cooperate with other members of the
extended family in selecting representative managers, either
from within the extended family or from outside its legal boun-
daries. If any nuclear family unit wanted to farm all of the

87, Itis worth noting that members of Rahab’s family never formally voiced their
individeal support of this covenant, but by remaining silent before she made it, when
the civil authorities had questioned her regarding the spies (Josh. 2:3), they became
lawful residents of Israel through their adherence to the external demands of Rah-
ab’s covenant, If they remained inside their section of the wall, despite the collapse
of the remainder of the wall, they could remain in the Promised Land (2:19).

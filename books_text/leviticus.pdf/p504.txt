27

FOOD MIRACLES AND
COVENANTAL PREDICTABILITY

Wherefore ye shail do my statutes, and keep my judgments, and do
them; and ye shall dwell in the land in safety. And the land shall yield
her fruit, and ye shall eat your fill, and dwell therein in safety. And if ye
shall say, What shall we eat the seventh year? behold, we shall not sow,
nor gather in our tnevease: Then I will command my blessing upon you
in the sixth yea and it shail bring forth fruit for three years. And ye
shall sow the eighth yeax and eat yet of old fruit wntil the ninth year;
until her fruits come in ye shall eat of the old store (Lev. 25:18-22).

The theocentric meaning of this passage is that God sustains
His people, and more than sustains them. He offers them plen-
ty. They are required to acknowledge this fact by trusting His
promises. They display this trust through their obedience to
Tis law.

This passage begins with a re-statement of the familiar cause-
and-effect relationship between corporate external obedicnce to
God’s covenant law and corporate external blessings. We know
that the frame of reference is corporate blessings because of the
use of the first person plural: “What shall we eat the seventh
year? behold, we shail not sow, nor gather in our increase.” In
this case, the text focuses on two blessings: peace and food.

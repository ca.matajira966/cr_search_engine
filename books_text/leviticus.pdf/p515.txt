Food Miracles and Covenantal Predictability 459

marked by widespread obedience to the external laws of God,
they will experience widespread external blessings, among
which are peace and food.

To prove that this promise can be trusted, God on occasion
has established miracles of feeding. The first time was in the
wilderness period: the manna. When they centered the Prom-
ised Land, they initially lived off the crops of their defeated
enemies. Then, as they began to plant and reap, they were to
become thrifty: saving, not for a rainy day, but for the sabbati-
cal year. But in the seventh cycle of sabbatical years, God prom-
ised to give them a miracle: the triple crop of the sixth year.”

The triple crop was also to remind them that God’s blessings
are predictable in history. I would remind them that the
source of their prosperity was not thrift as such, but thrift with-
in the framework of God’s covenant. They were warned not to
draw a false conclusion, one based on the humanist presupposi-
tion of the autonomy of man: “And thou say in thine heart, My
power and the might of mine hand hath gotten me this wealth.
But thou shalt remember the Lorp thy God: for it is he that
giveth thee power to get wealth, that he may establish his cove-
nant which he sware unto thy fathers, as it is this day” (Deut.
8:17-18). Covenantal blessings are given to confirm the cove-
nant.

Conclusion

The miracle of the tripic crop was promised to Israel in
order to confirm visibly: 1) the sovereignty of God over nature;
2) the predictability of God’s covenant-based blessings in histo-
ry The Israelites were not to capitulate to the temptation of
worshipping another god, either a god of nature or a god of

10. ‘Lhe prophets used the miracle of feeding on numerous occasions. The
pagan widow of Zerephath had two coutainers that filled daily, one with oil and the
other with meal, when Elijah lived in her home (I Ki. 17:14-15). In the New Testa-
ment, Jesus used the miracle of feeding on at least two occasions.

The Right of Redemption 473

In wailed cities, the kinds of people who would have wound
up as owners of urban housing would have been the same
kinds of people who own urban property today, Richer people
would have been dominant home owners. That is, those who
were the most productive people in the economy would have
been most likely to buy a home and retain a stake in society.
‘This property right, irrespective of family creed and ritual, to
buy and inherit housing in Mosaic Israel’s walled citics was an
important way for Israel to attract and keep very productive
people from abroad. It would have made Israel’s walled citics
centers of entrepreneurship and trade.

Turnover of ownership would initially have been much more
rapid in walled cities than in rural settings or in unwalled cities.
Nineteenth-century American capitalism’s story of “poor man to
rich man to poor man” i three gencrations would have been
much more common in Israel’s walled cities than outside them,
at least until population growth shrank the size of the average
farm."

The walled cities of the Canaanite era became the walled
cities of Israel. Which citics would have been the walled cities of
Canaan? First, cities that housed cultures with military aspira-
tions: city-state empires. Second, cities with wealth to protect
from invasion: urade centers. Third, cities with unique religious
icons or practices that served the needs of a particular region:
religious centers. Walled cities would have tended to be cities
on the crossroads of trade. Their architecture, water systems,
and similar “infrastructure” would have becn suited to trading
centers. Thus, their character as crossroad cities would not have
been radically altered by Israelite civilization. This means that
walled cities would have become cosmopolitan: world (cosmos)

 

resident, he said, the hated Windsor family inherits thé castle.

14. Tocqueville commented on the United States in the 1830's: “But wealth
circulates there with incredible rapidity, and experience shows that two successive
generations seldom enjoy its favors.” Alexis de Tocqueville, Democracy in America,

edited by J. B. Mayer (Garden City, New York: Doubleday, [1835] 1966), 1:3, p, 54

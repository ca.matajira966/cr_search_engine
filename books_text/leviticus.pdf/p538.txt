482 LEVITICUS; AN FCONOMIC COMMENTARY

What was prohibited under Mosaic law was interest taken from
a poor brother in the faith or a poor resident alien who had
subordinated himself to the civil covenant, presumably by sub-
mitting to circumcision. The lender, then as now, was not to
take advantage of certain poor people: those who had. submit-
ted themselves to the terms of the covenant. He was required
by God to make a charitable loan. He would thereby forfeit the
interest he might have earned from a business loan. Forfeited
Interest was the charitable component of his act. If interest were
universally prohibited, then all legal loans would be charitable. There
would then be no economic distinction between charity loans
and business loans, or between dominion by restoring the cove-
nant-keeping poor and dominion by subordinating the cove-
nant-breaking poor. ‘lhe Bible teaches otherwise.

Charity: Conditional vs. Unconditional

Charitable loans are part of God's program to provide help
to honest, covenant-kecping people who have fallen on hard
times. These loans are not supposed to subsidize sloth or evil.
God does not want us to subsidize evil with the money or assets
that He has provided for us.’ In this sense, biblical charity is
necessarily morally conditional.* Biblical charity is never a judicial-

and Economic Activity in the Middle Ages (New York: St. Martin's, 1969), p. 155. This
prohibition was gradually extended by the thcologians after 800. Ibid., p. 63. The
Second Lateran Council (1189) was especially hostile, going so far as to prohibit
usurers from being granted Christian burial, ibid., p. 165. The Council at Vienna
(1811-12) mandated the excommunication of civil rulers who permitted usury within
their jurisdictions. Ibid., p. 206. Gilchrist’s excellent book did not receive the audience
that it should have, It includes transfations of the texts of the general councils, ‘This
makes it invaluable.

8. R. J. Rushdoony, “Subsidizing Kvil,” in Rushdoony Bread Upon the Waters
(Nutley, New Jersey: Craig Press, 1969), ch. 8

4, Ray R. Sutton, “Whose Conditions for Charity? in Theonomy: An Informed
Response, edited by Gary North (Tyler, Texas: Institute for Christian Economics,
1991), ch. 9. Sutton is responding to Timothy J. Keller, “Theonomy and the Poor:
Some Reflections,” in Theonomy: A Reformed Critique, edited by William S. Barker and
W. Robert Godfrey (Grand Rapids, Michigan: Zondervan Academie, 1990}, ch. 12.

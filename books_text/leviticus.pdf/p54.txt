liv LEVITICUS: AN ECONOMIC COMMENTARY

5. The Reparation Offering (Lev. 5:14-6:7)"

This is called the trespass offering in the King James Version.
‘This sacrifice was required in cases of theft: an illegal appropri-
ation of another man’s inheritance, a violation of the tenth
commandment (point five). A man uses deception to gain own-
ership of another man’s goods. hen he lies to the victim and
the civil authorities. ‘fo restore the legal relationship after the
criminal voluntarily confesses the crime and the two false oaths,
he must pay the victim the value of the item stolen plus a 20
percent penalty (Lev. 6:5)."4 He also has to offer a ram as a
trespass offering to make atonement (Lev. 6:6-7).

‘There should be no confusion about what is involved in the
sacrifice. First, the lost inheritance is restored to the victim, plus
an extra one-fifth. he judicial relationship between the victim
and the criminal is thereby restored, making it possible to gain
the advantages of social cooperation. Second, God is repaid
because of the criminal’s false oath in civil court. ‘The criminal
avoids being cut off by God: disinheritance. ‘he goal is conti-
nuity: survival and covenantal prosperity in history. This is
point five of the biblical covenant model: succession.

Conclusion

The requirement that God’s people be holy is still in force.
‘Vhere will never be an escape from this requirement. It is
eternal. ‘lo understand at least some of the implications of this
ethical requirement -~ point three of the biblical covenant model
- Christians need to understand the Book of Leviticus. They
need to understand that it is a very practical book, many of
whose laws still have valid applications in modern society. We
ignore this book at our peril.

33. ibid., pp. 72-86.

34. Had he not confessed, and had he been convicted, the penalty was at least
two-fold restitution.

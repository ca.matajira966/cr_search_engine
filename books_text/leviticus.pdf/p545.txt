Poverty and Usury 489

lend money rather than goods. He would then reccive an im-
plicit interest return on the loan: added capital (purchasing
power) despite the numerical equality of the monetary units
repaid. ‘The Bible allows this. In times of falling prices, an
astute borrower will prefer a loan in goods rather than money,
but he is not in a position to demand such a. loan. “Beggars
can’t be chooscrs,” as the saying goes. [lowever, in most periods
in history, this added return on money loans is very low, since
prices rarely fall rapidly except following a period of high
monetary inflation. Economic output grows slowly most of the
time; prices therefore fall slowly.”

‘There is no question that the lender's decision to loan in
money or in goods is heavily dependent on the civil govern-
ment’s monetary policies. Because monetary policy cannot be
economically neutral,* to some extent there will always be
profits and losses in debt arrangements. Either the lender loses
or the debtor loses, depending on the terms of the contract and
monetary policy. The key judicial issue, however, is that in a
covenanted Trinitarian nation, the contract for a charitable loan
must not impose an explicit interest payment.

Conclusion

Usury from the poor brother is prohibited by the Bible. In
the Mosaic Covenant, this poor person had to be willing to risk
going into bondservice for up to six years if he defaulted on
such a zero-interest loan. The civil courts were required to
enforce this provision of a charitable loan. This bondservice
provision was assumed in every zero-interest loan, which the
court could safely assume was a charity loan. It was this willing-
ness on the poor person's part to risk bondservice that identi-

17, This is not true of computer chips, whose speed doubles every 18 to 24
months: Moore's Law.

18. Murray N. Rothbard, Man, Economy, and State: A Treatise on Reonomic Princi-
ples (Auburn, Alabama: Mises Institute, [1962] 1993), p. 715.

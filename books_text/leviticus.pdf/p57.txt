INTRODUCTION

And the Lorp spake unto Moses, saying, Speak unto all the congre-
gation of the children of Israel, and say unto them, Ye shall be holy: for
I the Lorp your God am holy. % shall fear every man his mother, and
his father, and keep my sabbaths: I am the Lorp your God. Turn ye not
unto idols, nor make to yourselves molten gods: I am the Lorp your God
(Lev. 19:1-4),

The book of Leviticus is the Bible’s premicr book of holiness.
The biblical meaning of “holy” is “set apart by God.” It is relat-
ed conceptually to “sanctify,” “sanction,” and “saint.” It refers to
any person, place, or thing with a God-ordained covenantal
boundary around it. Everything inside such a boundary is
sacrosanct. For example, we correctly speak of marriage as
holy. This does not mean that every marriage is Christian. It
means that God has placed a special judicial boundary around
every marriage.

The book of Leviticus is the Bible’s premicr book of bound-
aries. There is an element of separation in every boundary, just
as there is in holiness: separation by sanctions.’ The Book of
Numbers is the Pentateuchal book of sanctions, but the civil
sanctions of Leviticus have alienated Christians and have out-
raged pagans. Thai certain sexual acts are forbidden in Leviti-

1. See “Holiness,” A Dictionary of the Bible, edited by James Hastings, 5 vols.
(Edinburgh: T. & T. Clark, 1900), IT, p. 395.

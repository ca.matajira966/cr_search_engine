Slaves and Freemen 527

What I am arguing is simple: if is nol chattel slavery as such that
appails most covenant-breakers and their Christian ideological accompli-
ces, rather, it is the doctrine of eternal punishment. The denial of the
New Testament doctrine of eternal punishment, above all other
denials, is the touchstone of modern humanism. It is this doc-
trine, above all others, that humanists reject. They stand,
clenched fists waving in the air, and shout their defiance to
God, “You have no authority over us!” But He does. They
proclaim, “There is no hell!” But there is. And the lake of fire
will be even worse.

For all his protests, modern man nevertheless still accepts
the legitimacy of slavery. Humanists understand implicitly that
the right to enslave others is an attribute of God’s sovereignty.
They declare the State as the true God of humanity, and then
they proclaim the right of the State to enslave men. They
have created the modern penal system, with its heavy reliance
on imprisonment, yet have rejected the criminal’s obligation to
make restitution to the victim. They allow murderers to go free
aficr a few years of imprisonment or incarceration in a mental
institution, to murder again, for humanists are unwilling to
allow the State to turn the murderer’s soul over to God as
rapidly as possible, so that God may deal with him eternally.
They regard man as the sovercign judge, not God. They have
invented the slave-master institution of the modern prison,
while they have stcadily rejected the legitimacy of capital pun-
ishment. Bettcr to let murderers go free, humanists assert, than
to acknowledge covenantaily and symbolically that the State has
a heavenly judge above it, and that God requires human judges
to turn murdercrs over to Him for His immediate judgment,
once the earthly courts have declared them guilty as charged.

The humanist abolitionist tries to put God in the dock. He
tries to put the State on the judgment throne of God. What he

14. Libertarian anarchists are exceptions to this rule, since they do not acknowl-
edge the legitimacy of the State.

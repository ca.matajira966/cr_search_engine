6 LEVILIGUS: AN ECONOMIC COMMENTARY

and the people as a special province of God. The land of Israel
became like the garden of Eden: a temporary residence unique-
ly under God’s revealed law and uniquely under His historical
sanctions. ‘Lhe land of Israel, like the garden of Eden, was to
serve as a training area for covenant-keeping men. It was also
to serve as an example for covenant-breaking men (Deut. 4:4-
8). It was to serve both as God’s boot camp and as His general
headquarters for worldwide evangelism and cultural conquest.
The laws of Leviticus were designed to keep the leaven of
evil outside of the land of Isracl, but they were also designed to
push the leaven of righteousness into the world around Israel.
Levitical law was both defensive and offensive. One problem
with virtually all commentaries on Leviticus is that they empha-
size the defensive aspects of the. Levitical laws: separation and
exclusion. In this book, I do my best to point out the inclusive
aspects of some of these laws. There were laws of inclusion, at
least to the extent of placing the gentile world under the Ten
Commandments and therefore inside the zone of predictable
external blessings: positive sanctions in history. This was Jo-
nah’s message to Nineveh: God's covenant lawsuit. Had all of
God’s revealed laws been solely exclusionary, Jonah would not
have been sent by God on his missionary journey. As I argue in
this commentary, some of the Mosaic laws were cross-boundary
laws that governed other nations, and are still valid today.

The Book of Priestly Holiness

Behind Jonah’s prophetic ministry was a nation of priests. As
Jacob Milgrom points out in the introduction to the first vol-
ume of his extraordinarily learned, extraordinarily large, and
extraordinarily unreadable commentary on Leviticus, Leviticus
is not about the tribe of Levi. It is about the priesthood. ‘The
Book of Numbers rather than Leviticus deals in detail with the
laws governing the Levites. The reason why the book is called
Leviticus is because in Hellenic times, when the Greek version

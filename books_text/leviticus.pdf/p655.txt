The Redemption Price System 599

They were not prohibited from subleasing these sanctified
fields. These fields would have provided them with a stream of
income. Within a predominantly rural economy, this stream of
income might have been significant, depending on the size and
productivity of the dedicatcd plots.

A Righteous Bribe to Unrighteous Priests

The jubilee law’s restriction on Levitical ownership of rural
land was not primarily economic. The jubilee law itself was not
primarily economic; it was judicial: a mark of freeman status for
the heirs of the conquest. But there were economic incentives
tied to the preservation of political freedom. A small but rele-
vant aspect of these incentives was the law of the unredeemed
field. Priests could in rare instances become permanent owners
of rural land when an owner or his heirs failed to redecm a
reclaimed dedicated plot. But in order for this transfer of title
to take place, the jubilee year first had to be declared publicly
throughout the nation. “And if he will not redcem the field, or
if he have sold the field to another man, it shall not be re-
decmed any more. But the field, when it goeth out in the jub-
ile, shall be holy unto the Lor, as a field devoted; the posses-
sion thereof shall be the priest’s” (Lev. 27:20-21).

The existence of a law that tied the jubilee year 10 a perma-
nent transfer of rural land to priestly members of the tribe of
Levi delivered an important tool of influence into the hands of
covenant-keeping rural land owners. If covenant-kceping men
suspected that the civil authorities and the priests had con-
spired to avoid proclaiming the approaching jubilee year, they
had a way to encourage the ecclesiastical authorities to proclaim
the jubilee year on time. Ail the land owners had to do was
dedicate some fields to the priests and then reclaim the fields
for themselves, refusing to redeem these fields with cash plus a
20 percent payment. To inherit these ficlds at the jubilee, the
priests would have to proclaim the jubilee year. The Mosaic law
therefore provided the other tribes with a legal way to bribe

614 LEVITICUS: AN ECONOMIC COMMENTARY

by the inescapable phenomenon of interest. ‘The present value of
future goods is less than the present value of identical present
goods. This discount is called the rate of interest.’ ‘he priest
could lawfully demand an immediate cash payment of all the
shekels remaining to be paid until the jubilee. But the present
valuc of the moncy to be accrued in the future is less than the
present value of the same number of monetary units paid today
in cash. So, the lessee paid a penalty to the priest: the differ-
ence between the present value of the cash shekels and the
present value of those shekels to come. This was not the 20
percent penalty, but it was nonetheless a penalty.”

The fact is, however, the law provided no explicit earthly
negative sanctions for a priest to impose on a lessee who re-
claimed previously dedicated land. The priest had to rely on
the conscience of the lessee not to reclaim it. We see here that
the long-term sanctity of the land as inheritance judicially out-
weighed the short-term sanctity of the and in priestly dedication.
Only original owners could bring this unique sanction of disin-
heritance on their heirs.

A Judicial Price: Fixed by Law

Why not use a free market price in establishing the redemp-
tion price of dedicated land? Why did the text specify a specific
price and a specific crop? Samson Raphael Hirsch, the carly
nineteenth-century Orthodox Jewish commentator, offered this
explanation: this case “was the one unique case, standing quite
by itself, where a field could be sold and the purchase ultimate-
ly become permanent. Hence for buying back, for the redemp-
tion of such a field which could eventually become a permanent

19. Ludwig von Mises, Human Action: A Treatise on Economics (New Haven,
Connecticut: Yale University Press, 1949), ch. 19.

20. Those who deny the universal phenomenouof time-preference (interest) will
have to seek for another explanation of how the priests were protected from disrup-
tions in their plans: forfeited vows by lessccs.

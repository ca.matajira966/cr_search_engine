Tithing: The Benefit of the Doubt 621

This law reveals that God gave the benefit of the doubt to
the herd owner. An old beast that had diced could lawfully be
replaced by a young beast without the payment of a tithe. Pre-
sumably, this exchange would have benelited the owner, since
the newborn animal would have had many years of productivity
ahead of it. There would have been an increase of net produc-
tivity for tne herd but not a net increase in the size of the herd.
In some cases, however, the older beast would have been more
valuable, especially a prize animal used for breeding or a
trained work animal. God, as sovereign over life and death,
imposes net losses or gains on a herd’s productivity, irrespective
of the number of beasts in the herd.

What was nol tolerated by God was any atlempt by the own-
er to pick and choose from among the newborns. The owner
could not lawfully select the best of the newborns to replace the
dead animals, using the less desirable newborns to pay his tithe,
thereby cheating God. Presumably, the birth order of the new-
borns would govern the replacement of any dead beasts. ‘The
first newborn after the death of another member of the herd
would have been segregated immediately from the other new-
borns as not being eligible for the tithe.

Under the Rod

Those newborn beasts that remained alter the owner had
replaced any dead animals constituted the net increase of the
herd. In this case law, the herd owner lined up the newborns,
probably in a pen, and drove them onc by one past the Levite.
Each beast passed under a rod. Every tenth beast was taken by
the Levite. The herd owner was not allowed to walk the beasts
under the rod in any pre-planned order. ‘The same law that
governed the voluntary sanctification of beasts governed the
involuntary sanctification of beasts: “He shall not alter it, nor
change it, a good for a bad, or a bad for a good: and if he shall
at all change beast for beast, then it and the exchange thereof
shall be holy* (Lev. 27:10). The owner was allowed to buy back

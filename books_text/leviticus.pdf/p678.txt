622 LEVITICUS: AN ECONOMIC COMMENTARY

any sanctified beast, but only by paying the redemption price
commission.

The herd owner was given the benefit of the doubt at the
end of the line. Only the tenth beast was holy. If as many as
nine of the final group of beasts passed under the rod, the herd
owner owed no tithe on these nine beasts. Where the product
could not be divided without destroying the life or valuc-of the
item, the tithe applied only to discrete items. All those animals
that passed under the rod after the final group of 10 had becn
counted escaped the sanctification process,

Because God gave the benefit of the doubt to the tithe-payer,
it was especially evil for him to arrange in advance the collec-
tion of the tithe, with or without the collusion of the Levite.
‘The assembling process was to be humanly random. Neither
the tithe-payer nor the Levite was to manipulate the crop or
the herd to his own advantage, or to the other’s advantage.
God owned the tenth; He alone was authorized to arrange the
collection process. Any attempt by man to arrange the process
was not only theft from God, it was an assertion of man’s auton-
omy. It was an allcmpt to manipulate the created order in a
way prohibited by God.

The Ban

What if a tithe-payer defied Cod and manipulated the tithe-
collection process? ‘The tithed items came under the ban: “if he
change it at all, then both it and the change thereof shall be
holy; it shall not be redeemed.” ‘he tithed item became hormah:
devoted to God. ‘This degree of sanctification was absolute; once
within the boundaries of God’s possession, it could not lawfully
be removed.

Why would a person manipulate the outcome of the collec-
tion process? Because he was trying to cheat God. He was un-
willing to risk paying the 20 percent commission that would be
imposed if he subsequently wanted to buy back a specific item.
What was the penalty for this act of theft? Permanent loss. The

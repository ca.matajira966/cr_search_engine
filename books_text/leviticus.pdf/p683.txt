CONCLUSION

Thrice in the year shall all your men children [males] appear before
the Lord Gon, the God of Israel. For I will cast out the nations before
thee, and enlarge thy borders: neither shall any man desire thy land,
when thou shalt go up to appear before the Lorn thy God thrice in the
year (Ex, 34:23-24).

‘This was God's ultimate visible evidence of His covenantally
predictable defense of Israel. he very boundaries of the land
would become sacrosanct — sacred and set apart by God - dur-
ing the three mandated annual festivals. God promised that
during the Israelites’ numerous corporate journcys to Jcrusa-
lem, which was the only authorized place of sacrifice on earth,
their enemics would not even want to invade the land. In their
times of greatest military vulnerability, when the unarmed army
of the Lord was marching to Jerusalem, the nation would be
sheltered by the divine intervention of God. Potential invaders
would not even want to take advantage of them in these seasons
of formal worship. The nation was holy: set apart by God. ‘This
included the land itself. The sacrilege of military invasion dur-
ing the mandatory feasts could not take place for as long as
God maintained His covenant with Mosaic Israel. Israel would
not be profaned. The sign of God’s rejection of Israel would be
a military invasion during a feast, especially Passover.

 

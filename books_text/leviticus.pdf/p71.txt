Introduction 15

people arriving. Or perhaps 10 million if the population grew.
This did not happen, for God withheld the blessing of popula-
tion growth, but until the sacrificial system changed, this long
walk was required. Then they all walked home.

Mothers today complain about the trouble involved in plan-
ning a day’s drive or a plane ride plus a week’s visit in a motel.
Think about organizing a family for a week’s walk, a week’s
stay, probably camping out in a tent. Bear in mind, there was
no running water, no indoor plumbing, no toilet paper, no
disposable diapers, and no fast food restaurants. This was no
picnic. Then, after a week of jammed masses of humanity and
assembly-line sacrifices, they walked home. Less than two
months after arriving home, all the men walked back to cele-
prate another feast, which we call Pentecost: the firstfruits offer-
ing. This was Israel’s celebration the anniversary of God’s giv-
ing of the Ten Gommandments.*

During Pentecost (“weeks”) and ‘labernacles (“booths” - the
{cast of ingathcring), those eligible to serve in God’s holy army
arrived in the central place of sacrifice in order to offer their
individual sacrifices. The feasts’ celebrations were family-cen-
tered, with each family inviting in Levites and strangers to
share in the festivities (Deut. 16:13-17). During ‘labernacles,
the altar was used the whole week during the daytime for man-
datory national sacrifices (Num. 29:13-34). Pentecost’® was
different; the festival's formal sacrifices were completed on one
day — day 50 after Passover (Lev. 23:16). So, the special five
sacrifices of Leviticus 1-7 could have been conducted after

  

left Egypt (Num. 1:46; 3:43), which meant they had experienced zero population
growth. Stable population growth requires a litle over Wo children per family: 2.1
children — one male, one female on average (in monogamous societies). This means
that Israel had a national population of about 2.4 million people at the time of the
conquest, See Gary North, Moses and Pharavh: Dominion Religion us. Power Religion
(Gyler, Texas: Institute for Christian Economics, 1985), pp. 22-25.

15, Alfred Edersheim, The Temple: Lis Minisiry and Services As They Were in the Time
of Jesus Christ (Grand Rapids, Michigan: Kerdmans, [1874] 1983), p. 261.

16. Penickoste is Greck for fiftieth.

670 LEV{TICUS: AN ECONOMIC COMMENTARY

bondage to God, 540

boundary violation, 7,
97, 121, 126

covenant, 83, 631-32

covenantal sin, 126

cross-examined by God,
136-37

fall of, 97, 220

general covenant law, 631-
33

guardian, 139

hierarchy, 90-91, 97, 182

immediate payoff, 301

inheritance, 84

law restricting, 272

legal status, 282

Levite, 139

naming: mark of author-
ity, 380

No Exit (slavery & hell),
521

permission to act, 272

priest, 97, 128, 139

profane, 48, 130

representative, 549

sacred space, 126

sacrilege, 97

Satan’s subordinate, 83-
84

tree, 126

trespass, 48, 121

“Wait!”, 306

adoption

Abraham, 507

aliens, 417, 425, 470-72,
506-7, 519-20, 522-23

annulment, 587-88

baptism as a mark, 514

centrality of, 519
Christ’s gospel, 520-21
citizenship, 390, 425,
450n, 470-72, 507,
510-11, 522
cost in land value, 509
covenantal, 591
criminal, 508
cross-family, 510-13
ear lobe, 169-70
inclusion (covenant), 631
inheritance, 509, 507
Israel’s, 519-20, 612
kinsman-redeemer, 390,
612
liberty, 390
marriage, 584
military service, 505, 507
New Covenant, 639
population growth, 418
priesthood, 585, 587-88,
642
rural land &, 428
slavery, 520, 642
spiritual, 291
uribal, 507, 592 (also see
Uriah)
types (2), 507
adultery, 5, 42, 64, 101, 197,
326, 379
adulthood, 504, 585
advertising, 437-38
Africa, 559
agriculture
comparative advantage,
22
curse, 421
feasts & 16, 18

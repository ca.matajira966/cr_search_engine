final, 547-48

hierarchical, 90-91, 95, 182,
336n

judge, 258

jury, 259 (also see jury)

local, 253

locus of, 94, 99, 106

naming &, 380

officers, 99

parental, 328

people’s, 94-95, 98-99, 106,
336n

politics &, 258

power &, 258

purification offering, 648

representation, 88-90,
95, 555

resistance to, 590

Satan’s, 113, 221, 274 (also
see Satan)

sharecropping, 113

stoning, $36n

autonomy

alliance: pietist-humanist,
578-79

anti-tithe, 33

Babel, 275

blasphemy, 376

blessings vs, 459

Christians’ quest, 33

divine state, 58

economic theory, 411

God put in bondage, 528

humanist law, 36

“innocence” defensc, 90

liberation vs., 411

man’s limits, 562

sacrifices &, 52, 58

673

society, 578-79

Babel, 254, 275

Babylon
Assyria destroyed by, 543
balance imagery, 317
diet in, 342
military sanctions, 185
sabbath rest &, 312, 397,

501, 576

Bahnsen, Greg, xxxiv, $11,
645-46, 650

balances, 308, 310, 314-15,
317, 321

ban, 584, 619, 622-23 (also see

   

hormah)
Banfield, Edward, 305
bankruptcy, 55, 57, G1, 140,

206, 487, 540
baptism, 290, 292, 347, 512,
514, 639n
Bar Kochba, 23, 32, 611
barley, 600-6
barrier (sce boundaries)
Basques, 30-31
bastards, 483, 507
Bates, E. 8., xxxvii
battery, 235 (also see assault)
Baxter, Richard, 526
beasts
breeding, 285-88, 641
clean-unclean, 639
dedicated, 594
jubilee year, 399
removed, 541
sacrificial, 596-97
sanctified, 587
sanctions-bringers, 574

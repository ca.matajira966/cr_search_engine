682 LEVITICUS: AN ECONOMIC COMMENTARY

vow, 582

also sce discontinuity
contracts, 216-17, 221, 226,

229-30, 354, 465
cooking, 316-17
cooperation, 214-16, 255-56
coordination, 213-20
corruption, 316-17, 321
costs (sunk), 438n
countryside, 170-71, 180, 202
courts

agent (victim), 139-40

appeals, 253-54, 257, 271,

274, 277

atheists in, 142

atonement, 140, 141

clogged, 217

expenses, 139-40

God's, 137, 267

interrogation, 136-37

judges & juries, 256-61

kingdom, 274

kingly justice, 140-41

local, 251

nationalism, 275

representative, 140

ritual payment &, 140

warlordism, 273-74

world, 275
covenant

adoption, 591

Assyria, 188

blessings, 448-50, 553-54,

570

bond, 211

citizenship, 332, 424-25, 468

civil, 93, 460, 638

conditional, 450, 570

continuity, 279

creation &, 59, 401

death, 170, 171, 583-84

discontinuity, 279

dominion, 34, 85, 132, 394,
401, 422

economics, 218-20, 569

femininity, 93

geographical, 8, 184, 185

gospel, 189

grain offering, 46

hierarchy, 88, 90-91

Israel as a battlefield, 185

Jordan on, 10-11

land, 541

lawsuit, xx, 6, 317, 325,
377, 391

limits, 463

masculinity, 93

military, 185

model, Preface, 10-11

national, 188, 259, 457

oath, 102, 211, 220, 457,
630-31

peace offering, 81

Pentateuch, xlii-xlix

point three, 10

population, 558 (also see
population)

predictability, 454-58, 458,
460-61

priesthood &, 359

promise comes first, 10

protection of Israel, 627

renewal, 42, 45, 81-82, 99,
353

representation, 545-46, 554-
55

686 LEVITICUS: AN ECONOMIC COMMENTARY

embassy, 634n
emigration, 422
empire, 410-11
employer, 201, 223-29
Enlightenment, 52, 100-1, 105-
6, 159, 274, 516
entitlement, 208, 483-84
entrepreneur, 219, 412, 439,
535
entropy, 133, 568
environment, 561-62
envy, 380-81
equality (legal)
biblical standard, 249-50,
308, 368
crucifixion as example, 237
economic inequality &, 238-
39
golden rule, 308
Hayek’s view, 241
origins of, 237
sanctions &, 238-39, 241
strangers in land, 368, 375
two kinds, 241-43
weights & measures, ch. 19
Western civilization, 237
Esau, 64 .
eschatology, 461, 568, 572
Esther, 343
Estrada, Emilio, 27
eternity
debt repayment, 55, 56
history & (sanctions), 378
leprosy as symbolic of hell,
164-65
negative sanctions denied,
137
oath &, 457

restitution to God, 375
sacraments &, 457
separatuon, 292
slavery as model of hell, 526
ethics
civil justice, 249
covenant renewal, 42
dispensational, xxix-xxxvii
geography &, 186
maintaining God’s grant, 9
sacrifice &, 47
separation, 180
tribalism, 17
eliquetic, 371
eunuch, 283, 471
evangelism, 422
Eve, 119, 121, 212
evolution, xxix, 27, 460
exclusion, 295, 334-35, 630
(also see inclusion/exclusion)
excommunication
inheritance lost, 120, 450n,
509, 522
judicial circumcision, 340
Levitical authority, 477
prohibited offering, 41,
rare today, 635, 636
risks of, 389-90
execution, 267, 354, 370, 374
exile
disinheritance, 407
ended Davidic kingdom,
137
Europe, 334-35
inheritance, 205n
jubilee slave laws, 407
land as agent, 192
land Taw, 429

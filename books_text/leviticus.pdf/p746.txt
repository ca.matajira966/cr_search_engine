690 LEVITICUS: AN ECONOMIC COMMENTARY

unemployment insurance,
199
urbanization &, 201
glutton, 159n-160n, 163
goat, 92-93, 101
God
absentee landlord, 394
adultery &, 64-65
agent of oppressed, 235
agents of (see God’s
agents)
author of life, 556
authority, 94, 221 (also see
authority)
barbaric?, 323-24, 336n
blasphemy of (scc blasphe-
ny)
blessings, 232, 252, 459,
(also sec blessings)
blood avenger, 464
bondservice to, 492-93
boot camp, 6
city of, 569
civil rights, 376
commandments, 120-21
court, 137, 144
covenant lawsuit, 377
Creator, xxi-xxii

  

Creator/creature distinction,
4

crime, 196, 512

curses by, 164-65, 232

debt to, 9, 11, 51, 53-57,
226, 512

deceiving &, 135-37

delegates authority, 94,
98-99

deliverance, 79, 149, 308

departure of, 100

dependence, 81, 410,
451-52

disinherited Israel, 611

doctrine of, xlix

dwelling place, 430, 462

dwells judicially, 394

economy, 219

ethical unity with, 4

extra mile, 81

fat & blood, 76

fatherhood, 323

fear of, 480, 573

field, 195

fire, 1, 32, 67, 79, 323-24

food sacrifices, 47

forgetful?, 137

forgiveness, 53-57

grant, 9-10, 396, 426

hates sinners, xxix

headquarters, 6

heavenly court, 267, 374,
379

High Pricst, 137, 141

holy, 348

holy thing, 120-21

homophobe, xxix

image, 196

information &, 566 (also
see information)

Israel’s defender, 627

jealous, 323

judge, 314, 320,

justice, 137, 139, 312

King, 137,199

kingly authority, xliv

King’s sanctions, 96

Kinsman-Redeemer, 464

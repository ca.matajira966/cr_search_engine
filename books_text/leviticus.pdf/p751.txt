tithe, 619
urbanization, 425
vow, 581
walk, 11-18
withdrawal?, 2
holiness code, xxiv
holocaust, 1
holy commonwealth, 95
Holy Communion
Calvin, 82
corporate, 83
family prayer &, 124
judicial, 82
leaven,.79
mandatory, 82
oath-bound, 82
Passover &, 82
peace offering &, 81-83
perjurer, 141
priest, 157
Rome, 157
sacrament, 125
weekly, 82
wine, 157, 163
also see Lord’s Supper
holy ground, 192
holy of holies, 123, 347
holy things, 119, 121-26, 129,
131
home, 268, 330, 465-67, 473
(also see houses)
homosexuality
Aristotle on, xxvi
Judaism vs., xxi-xxii
Leviticus, xxiv, XXvi
natural law, xxvi
no fear of God, xxii
Socrates on, Xxv-xxvi

695

honey, 78

Hong Kong, 418

hormah, 595, 622-23

hornets, 297

horses, 13, 258

host, 560, 571

Tlouse, Wayne H., xxxiii, 370n

houses, 166-67, 268, 465-68,
473, 478-79

humanism, 132, 484, 927-28

hunger, 557-58, 875

Ilunt, Dave, xxx

Huyghe, Patrick, 27, 28

hymns, 3

Ice, Thomas, xxxiii, 370n
idolatry, 200, 326, 371
illiterate, 231
immigrant (walled cities), 432
immigration, 320, 333-34, 341,
427-28, 432, 475
imports, 21-23
impurity, 7
imputation, 438
indusion/exclusion
boundaries, 8, 630
citizenship, 634
covenant people, 295, 630
employment, 229
gleaning, 207
Levitical law, 6
oath, 631
sacraments, 97
meome, 439-41
incorporation: legal issue, 296,
298-99
Indians, 29
individualism, 218, 272

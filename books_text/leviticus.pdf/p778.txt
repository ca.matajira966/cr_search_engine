722 LEVITICUS: AN EGONOMIC COMMENTARY

profaning, 327
sobriety &, 153-55
strangers, 309
temple, 154-55
temple-land, 187
walled city, 425
whole world, 309
Satan
Adam &, 83-84
bureaucracy, 221
confusion of, 565
deceives Eve, 212
disinheritance, 62
finitude, 565
growth threatens him, 565-
66
hicrarchy, 221, 274
imperialist, 274
juggler, 566
leascholder, 84
leaven, 77
limits, 565
lying, 221
monitors, 565
ownership, 84
plan coordination, 221, 566
political economy, 62
representation, 565
representatives, 378, 458
sanctions, 375
society of, 378
squatter, 84
Saul, 257
scale, 315, 321
Scandinavia, 30
scarcity, 242, 254, 255
scepter, 638

Schlossberg, Herbert, 484
Scofield, C. 1., 243n
scarch costs, 230
Second Amendment, 269
secret ballot, 260-61
securities markets, 221
security (price of), 499
seed
Abraham, 8
barley, 601
Christ, 514, 557
future, 328
hermeneutic, 280
inheritor, 521
laws, 285, 290-92, 324, 556-
57, 637-42, 641-42
multiplication, 645
New Covenant, 347
promise, 521, 647 (also
sec Shiloh)
prophecy, 17, 285-86
separation, 281
Shiloh, 284, 556
trade-off: land vs. Promised
Seed, 285
tribes, 281-84, 556
secd corn, 110
seed laws
breeding, ch. 17
circumcised fruit, ch. 18,
64142
context of, 556-57
hermeneutics &, 280, 653
jurisdiction, 290-91
Moloch worship, 324, 335
separation principle, 293-94
tribes &, 282,

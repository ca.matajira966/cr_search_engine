26 LEVITICUS: AN ECONOMIC COMMENTARY

town of Los Lunas.” The script (alphabet) dates from the
twelfth century B.C. Professor Robert Pfeiffer of Harvard
University’s Semitic Museum first translated the inscription in
1948.3! A more recent translation than Pfeiffer’s reads:

I [am] Yahve your God who brought you out of the land of the two
Egypts out of the house of bondages. You shall not have other [foreign]
gods in place of [me]. You shall not make for yourself molded or
carved idols. You shall not lift up your voice to connect the name of
Yahve in hate. Remember you [the] day Sabbath to make it holy. Hon-
or your father and your mother to make long your existence upon the
land which Yahve your God gave to you. You shall not murder. You
shall not commit adultery or idolatry. You shail not steal or deceive.
You shail not bear witness against your neighbor testimony for a bribe.
‘You shall not covet [the] wife of your neighbor and all which belongs
to your neighbor?

It mentions two Egypts, an obvious reference to the two
regions of Egypt, upper (close to the head of the Nile) and
lower (close to the Mediterranean). As to when the inscrip-
tion was made, George Morehouse, a mining engincer, has
estimated that this could have taken place as recently as 500
years ago and as far back as two millennia.“ A “revisionist”
who has studied the inscription in detail believes that the text
may be from the era of the Septuagint, ie., over a century
before the birth of Jesus - surely no comfort for conventional

29. David Allen Deal, Discovery of Ancient America (Irvine, California: Kherem La
Yah, 1984), ch. 1.

30. Barry Fell, “Ancient Punctuation and the Los Lunas Text,” Epigraphic Society
Occasional Papers, XIII (Aug. 1985), p. 35.

31. A photocopy of Pfeiffer’s translation appears in Deal, Discovery, p. 10.

82. L. Lyle Underwood,“The I.os Lunas Inscription,” Epigraphic Society Occasion-
al Papers, X:1 (Oct. 1982), p. 58.

38. New Bible Dictionary (Qnd ed.; Wheaton, Illinois: Tyndale House, 1982), p.
302.

34. George E. Morehouse, “The Los Lunas Inscriptions[:] A Geological Study,”
Epigraphic Society Occasional Papers, XIII (Aug. 1985), p. 49.

28 LEVITICUS: AN ECONOMIC COMMENTARY

lower jaws), the dragon, and the use of jade — which overlap-
ped cach other from the fifteenth to the twelfth centuries,
B.C.? Why were the implements and techniques used by the
Mayans to make bark paper five centuries before Christ so
similar to the implements and techniques used by the Chou
dynasty in the same era? Of 121 individual traits, the two sys-
tems shared 91, half of which were non-essential, and the other
half, while essential, had alternative approaches available.”
Why didn’t the Mesoamerican techniques match papermaking
techniques used by cultures in other parts of America?” Why
do Mayan stone art works after 500 B.C. shift from earlier
forms to match Asian art forms of the same era??

Meanwhile, at the other end of the hemisphere, slate tech-
nologics have been discovered in burial sites of the ancient Red
Paint (red ochre) People in Maine and Labrador. These arti-
facts match slatc technologics in Scandinavia. The era of con-
junction was some 4,000 years ago. Huyghe writes: “The
principal deterrent to the notion of historical contact is the
widespread belief that ancient man was incapable of making
ocean voyages in primitive boats. But there is certainly no
doubt that Europeans had oceangoing watercraft quite early.
Bronze Age rock carvings in Europe show plank-built ships
were sailing Atlantic coastal waters more than 4,000 years
ago.”*

How many people know that the Carthaginians were send-
ing trading ships to North America in the late fourth century
B.C.? Throughout the eastern United States, Carthaginian coins

40. Ibid., p. 84.

41, Ibid., pp. 86-87. See Paul Tolstoy, “Paper Route,” Natural History June 1991).

42. fbid., p. 87.

43. Ibid., pp. 87-91. See Gunnar Thompson, Nu Sun (Vresno, California: Pioneer,
1989). Thompson is direclor of the American Discovery Project at California State
University, Fresno.

44. Ibid., pp. 52-54.

45. Ibid., p. 54.

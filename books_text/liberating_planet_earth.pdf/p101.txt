The Liberaiton of the Church 93

Persecution

The church comes under immediate attack and control the
day that the Communist allies of the liberation theologians attain
political or military power. The church is seen as one of the two
enemy institutions to the state, the other being the family. But
churches are far easier to control than families. There are fewer of
them, their property is visible, and their leaders can be threatened.

‘There is little question that there are two major reasons why
churches are not yet being persecuted in non-communist coun-
tries. First, because existing political rulers do not want to take
the risk yet of pressuring the churches. Second, the messages that
most pastors are delivering from the pulpits are no particular
threat to the political rulers. In short, there are high costs and low
benefits for political rulers who attempt to silence the churches.
Most of them have already adopted self-policing methods. They
avoid controversy.

Nevertheless, the existence of legally independent sources of
authority within society is always a threat in principle to humanist
civil rulers. Churches that preach the whole counsel of God can
expect trouble from the civil magistrates eventually. What should
they do to prepare for this day?

First, they must make full use of prayer. Prayer gives Chris-
tians access to God’s holy sanctuary. Corporate prayer through
the church offers added power: this is God’s ordained monopoly of
corporate worship. Paul tells us to pray for the civil government,
that the church might have peace (1 Timothy 2:1-3).

Second, the Psalms offer examples of wrathful prayers. These
are sometimes called imprecatory psalms. They pray down God's
wrath on His enemies, who are the enemies of the church, Psalm
83 is a good example. Psalm 74 is another. These are supposed to
be public prayers, meaning official prayers of the church. If the
rulers refuse to do what is righteous, and they become a threat to
public peace and public good, then they are the legitimate targets of
imprecatory prayers. We ask God to reform them or remove them.

In other words, the prayers of the church are to offer rulers
both rewards and punishments, carrots and sticks, The rulers

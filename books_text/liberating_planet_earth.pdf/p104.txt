96 Liberating Planet Earth

what weekly communion is supposed to accomplish: to weed out
(Matthew 13:7, 22) those who resent God’s discipline over them.
Weekly communion forces them to deal with this moral problem
every week.

Elders must begin to serve as appeals courts (1 Corinthians 6).
They must be ready to put a halt to all rumors and gossip. Con-
tinued tale-bearing is a sin (Leviticus 19:16), Every time a bad
report comes to a church elder, he should ask the tale-bearer to
repeat the accusation because the problem may get to the trial
stage. It would be wise for the officer to carry a little note pad for
this purpose. Just take it out, note the date, write down the tale-
bearer’s name, and then ask him to repeat the accusation slowly.
This will reduce tale-bearing drastically. It forces the tale-bearer
to speak precisely. The cost of becoming a false witness (perjurer)
is to have the penalty that would have been imposed on the victim
imposed on the false witness (Deuteronomy 19:19).

By keeping disputes within the church out of the secular
courts, the state is kept at a greater distance. The state will be
more likely to recognize the church as an independent jurisdic-
tion, which God says it is. To submit to the law of a rival god (hu-
manism), church members have violated the covenant.

Any church member who appeals a decision of the church to
the state should be instantly excommunicated. He is thereby call-
ing in the state to judge the church. This is an act of defiance.
Also, once he is excommunicated and is no longer to be regarded
as a Christian for institutional decision-making purposes {1 Corin-
thians 5), church members can then go into the secular courts to
challenge him and defend the church from his public accusations.
(The obvious example is a dispute concerning a church-granted
divorce, especially the accusation of adultery or other major sin
against one of the spouses. When the announcement of the
reasons for the excommunication and divorce are made public,
the guilty party may seek vengeance against the church.)

As is the case in every covenantal institution, judicial author-
ity is lodged at the top (the principle of representation or pres-
ence), but the chain of command is to be imposed in a bottom-up

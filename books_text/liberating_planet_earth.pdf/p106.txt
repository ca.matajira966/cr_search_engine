98 Liberating Planet Earth

alties against them personally or against the church’s assets if the
church’s authority was misused by the officers to commit a crime,
but these penalties are not the obligations of the members.

Hf churches set up schools, these schools must be legally inde-
pendent of all state regulation except normal safety and health
regulations that are applied equally to all public buildings, espe-
cially buildings owned by the state. There must be no interference
with the curriculum materials of the church-run schools.

Summary

The church is structured along the lines of God’s covenant. It
is a separate legal jurisdiction in the eyes of God. It possesses the
monopoly of the sacraments. Its authority to excommunicate is its
chief power.

It speaks the whole counsel of God. Nothing that the Bible ad-
dresses is outside the preaching jurisdiction of the church. Any attempt to
limit this preaching function represents an assault by God’s ene-
mies against the Word of God.

For society to receive the full blessings of God, individuals and
other institutions must respect the lawful jurisdiction of the
church. If the state hampers the church in its preaching, then God
will bring judgment against the civil magistrates involved. If the
whole population agrees that the state should hamper the church,
then this spiritual sodomy will be punished by God covenantally,
meaning collectively. The equivalent of fire and brimstone is immi-
nent (Genesis 19). Individuals who wish to avoid such collective
judgment would be wise to take up the church’s cause.

In summary:

1. The king in ancient Israel was not allowed to exercise the
priest’s prerogative of making sacrifice to God.

2. The ministry of the sword is different from the ministry of
the sacraments.

3. The church’s covenantal structure parallels the general cov-
enant structure.

4. Satan establishes a rival ecclesiastical structure through the
state.

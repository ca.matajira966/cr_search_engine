The Liberation of the Church

5. The satanic state seeks out false prophets.

6. General apostasy is always accompanied by tyranny.

7. Modern Christians believe that God’s laws in the Old Testa-
ment would promote tyranny today.

8. Humanists argue the same way.

9. In the Old Testament, all other law structures promoted
tyranny.

10. Established humanist civil governments, revolutionaries,
and escape religionists all resent the church’s preaching of the
whole Bible.

11. Pastors fear adding to their responsibilities.

12. Preaching the whole counsel of God necessarily involves be-
coming familiar with many aspects of thought and culture.

13. The church comes under persecution when systematic, ded-
icated humanists come inte political power.

14. The churches must pray: peace prayers and judgment prayers.

15. The church is the protector of the family.

16. The church is a secondary welfare agency, filling in where
families fail.

17. Self-government is basic to a Biblical church.

18. Self-government is invoked before the Lord’s Supper (self-
examination, confession of sin).

19. Weekly communion meals increase the frequency of the
need of formal self-government.

20. Church elders are to serve as appeals court judges.

21. Formal trials will reduce gossip in the church,

22. The church is a separate jurisdiction from the state.

23. This is why it needs its own autonomous courts under God.

24. Judicial authority is at the top, but the system is activated
bottom-up.

25. The satanic structure is bureaucratic, with action initiated
at the top and imposed on the lower rungs of the institution.

26. The church has basic prerogatives as a separate jurisdiction:
freedom from interference by the state.

99

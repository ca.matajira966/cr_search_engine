104 Liberating Planet Earth

this required screening by the lower courts. Jethro understood
that the limitations on Moses’ time were paralyzing the justice
system. Obviously, if every case considered by the lower courts
eventually wound up in front of Moses, the hierarchy of courts
would have provided no respite for Moses. The screening feature
of the court system was fundamental to its success. This meant
that the majority of litigants had to content themselves with some-
thing less than perfect justice.

Jethro understood that endless litigation threatens the survival of the
system of justice. Losers in a case clearly have an incentive to ap-
peal, if the possibility of overturning the decision of the lower
court judge offers hope. So there has to be restraint on the part of
higher court judges to refrain from constant overturning of lower
court decisions. Furthermore, a society composed of people who
always are going to court against each other will suffer from clogged.
courts and delayed justice. A society, in short, which is not gov-
erned by self-restrained people, and which does not provide other
means of settling disputes besides the civil government—church
courts, arbitration panels, mediation boards, industry-wide
courts, and so forth —will find itself paralyzed.

Breakdown of the U.S. Court System

Macklin Fleming is a justice of the California Court of Ap-
peal. His book, The Price of Perfect Justice (Basic Books, 1974),
documents the increasing paralysis of the jegal system in the
United States. It is this quest for earthly perfection a messianic,
God-imitating quest—that has been the U.S. legal system’s
undoing.

The fuel that powers the modern legal engine is the ideal of
perfectability—the concept that with the expenditure of sufficient
time, patience, energy, and money it is possible eventually to
achieve perfect justice in all legal process. For the past twenty
years this ideal has dominated legal thought, and the ideal has
been widely translated into legal action. Yet a look at almost any
specific area of the judicial process will disclose that the noble ideal
has consistently spawned results that can only be described as

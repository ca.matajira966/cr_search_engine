Introduction 7

creeds, but instead seek to escape from it: by drunkenness,
escape, or corruption.

At this stage in Marxism’s history, the Communists must find
new sources of ethical motivation and vision. The Bible gives
them the rhetoric of moral vision that they need.

3. Quest for New Allies

Tn 1965, there was a revolt by Communist forces in Indonesia
against the tottering socialist government of President Sukarno.
This revolt failed. In the wave of Muslim wrath that followed, at
least 200,000 Communists and their suspected supporters were
slaughtered. It may have been as many as a million.

The Communists learned an important lesson: it is risky to try
to impose an alien atheist religion on deeply religious people.
They adopted a new strategy: a far more open, less clandestine
cooperation between Communism and religion. From 1965 on,
the Communists began to promote a “Marxist-Christian dialogue.”
These were one-sided affairs; the Communists conceded nothing,
and the people who dealt with them — humanists who called them-
selves Christians— conceded everything. The most widely known
Communist in this dialogue was French scholar Roger Garaudy.
When he opposed publicly the Soviet invasion of Czechoslovakia
in 1968, he was expelled from the French Communist Party. So
much for “dialogue” and “the mutual sharing of views.”

Conservative Christian journalist and historian Otto Scott has
commented on the importance of the Indonesian slaughter for in-
ternational Communist strategy:

Turning toward Central and South America, they realized that
a revolution could not achieve success in those regions unless it in-
cluded religious elements. Consequently, their Hispanic campaign
moved beyond the intellectuals (a corrupt element available to any
purchaser at any time) and into traditional Roman Catholic cir-
cles. Liberation theology was their vehicle. . . . The same argu-
ments moved smoothly into main-line religious circles in the
United States.5

5, Otto Scott, “Fhe Conservative Counter-Revolution,” Modern Age (Summer
1985), pp. 207-8.

CONCLUSION

All things have been delivered to Me by the Father, and no one
knows the Son except through the Father. Nor does anyone know the
Father except through the Son, and he to whom the Son wills to reveal
Him. Come to Me, all you who labor and are heavy laden, and I
will give you rest. Take My yoke upon you and learn from Me, for I
am gentle and lowly in heart, and you will find rest for your souls.
For My yoke is easy and My burden light (Matthew 11:29-30).

There is only one way to find true liberation: to be in subjection
to Jesus Christ. There is only one alternative to being in subjection
to Jesus Christ: being in subjection to Satan. Therefore, there is
only one alternative to liberation: slavery.

We are always under the domination of someone who is more
powerful than we are. We live under hierarchy. We are either
under Christ or under Satan. There is no autonomy for man.
There is no self-law. We do not build civilizations all by ourselves.
We are either under Christ or under Satan. We work to build a
Christian civilization or a satanic civilization; we cannot legiti-
mately hope to build man’s autonomous civilization. No such civi-
lization is possible.

If we as individuals can find true liberty only under Jesus
Christ, then why should we expect to find a society that provides
true liberty that is not under Jesus Christ? If true freedom is only
available to us as individuals under God and God’s law, then why
should we expect to find civil liberty under Satan and Satan’s law
(which is in fact lawlessness)? There is no “natural liberty” under
“natural law” enforced by “natural man.”

149

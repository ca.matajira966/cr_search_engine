Introduction 9

first, the kingdoms of this world must be steadily transformed into
the kingdom of Christ. In principle, Jesus Christ is now king of all
of mankind’s kingdoms. He gained this authority by His death
and resurrection, His triumph over Satan and sin:

Then Jesus came and spoke to them, saying, “All authority has
been given to Me in heaven and on earth” (Matthew 28:18).

The kingdoms of this world have become the kingdoms of our
Lord and of His Christ, and He shall reign forever and ever (Rev-
elation 11:15).

But it is through the work of His faithful people on earth that
this Aistorical transfer of kingdom ownership to Christ is to be
made manifest in history.

Go therefore and make disciples of all the nations, baptizing
them in the name of the Father and of the Son and of the Holy
Spirit, teaching them to observe all things that I have commanded
you... (Matthew 28:19-20a).

Step by step, person by person, nation by nation, Christians
are to disciple the nations. ‘This means that they are to bring men
under the discipline of the legal terms of God’s covenant. This hap-
pens in history. This is what history is all about. All authority has
already been transferred to Christ, in heaven and in earth. He has
already in principle transferred this authority to His people. They
are to exercise this God-given authority progressively in history.

Then comes the end, when He delivers the kingdom to God the
Father, when He puts an end to all rule and all authority and power.
For He must reign till He has put all enemies under His feet. The
last enemy that will be destroyed is death (1 Corinthians 15:24-26).

Christianity Has a Positive Program
There is an old saying in politics: “You can’t fight something
with nothing.” What this world does not need is another little book
refuting the errors of the Marxist movement known as liberation.
theology. What this world does need is a comprehensive, Bible-
based, God-blessed program for building a better world in the

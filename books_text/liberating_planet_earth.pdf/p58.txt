4
THE COVENANT OF LIBERATION

And you shall remember the Lorp your God, for it is He who
gives you power to get wealth, that He may establish His covenant
which He swore to your fathers, as it is this day. Then shall it be, if
you by any means forget the Lorp your God, and follow other
gods, and serve them and worship them, I testify against you this
day that you shall surely perish. As the nations which the Lorp de-
stroys before you, so shail you perish, because you would not be
obedient to the voice of the Lorp your God (Deuteronomy 8:18-20).

Biblical religion is covenantal religion. Like bondage, the cov-
enant is an inescapable concept. It is never a question of “cove-
nant vs. no covenant.” It is always a question of whose covenant.

The Bible teaches that there are four covenants: personal, fa-
tuilial, ecclesiastical, and civil. Each is marked by what theolog-
ians call a self-maledictory oath. Such an oath calls God's wrath
down on the swearer if he disobeys the covenant. God Himself es-
tablished a covenant with Abraham, cutting animals in pieces and
moving in between them. This signified God’s assurance that He
would be faithful to the terms of His covenant with Abraham, and
with such complete faithfulness that if He violated the covenant,
He would be torn to pieces, like the animals (Genesis 15:7-21).

An individual does not generally make such a covenant directly
before God, except when he makes 2 vow to God (Numbers 30),
but Adam implicitly made such a vow when God placed him
under the covenant of life. He was not to eat of the tree of the
knowledge of good and evil. He did eat, broke the vow, and
placed all his heirs under this same vow. The wrath of God is on

50

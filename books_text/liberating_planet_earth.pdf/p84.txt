76 Liberating Planet Earth

We know that one of the promised blessings for the godly is that
miscarriages will be reduced in a nation which is seeking to con-
form itself to God’s law (Exodus 23:26). The demographic impli-
cation of the Biblical perspective should be obvious: a large and
growing population. When godliness simultaneously increases both
the birth rate and the survival rate, the godly society will experi-
ence a population explosion. What God sets forth in His word is
simple enough, though both Christians and pagans in the late
twentieth century have refused to believe it: one sign of His pleasure
with His people is a population explosion.

Population growth is not a guarantee of His pleasure. Ungodly
societies can temporarily sustain a population explosion, especially
when they have become the recipients of the blessings of God’s law
(for example, Western medical technology) apart from the ethical
foundations that sustain these blessings. Nevertheless, sustained
population growth over many generations is one of God’s external
blessings, and these blessings cannot be sustained long-term apart
from conformity to at least the external, civil, and institutional re-
quirements of God’s law.

Long life is a biological foretaste of eternal life. It is an earthly
“down payment” (earnest) by God. It points to eternal life. It is
also a capital asset which enables men to labor longer in their
assigned task of subduing their portion of the earth to God’s glory.
Long life is an integral part of the dominion covenant.

Since the fulfillment of the dominion covenant involves filling
the earth, it is understandable why long life should be so impor-
tant, It is one critical factor in the population expansion necessary
to fulfill the terms of that covenant, the other being high birth
rates. God has pointed clearly to the importance of the family in
fulfilling the terms of the dominion covenant. The parents receive
the blessing of children (high birth rate), and the children secure
long life by honoring their parents. Or, to put it even more plainly,
a man gains the blessing of long life, including the ability to pro-
duce a large family, by honoring his parents. The way in which
the people of a culture define and practice their family obligations
will determine their ability to approach the earthly fulfillment of

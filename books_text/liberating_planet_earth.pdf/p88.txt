80 Liberating Planet Earth

non continue uninterrupted in any family forever. We also cannot
expect to see rates of growth over 1% for centuries at a time. As I
like to point out, the 4 billion people on earth in 1980 would multi-
ply to over 83 trillion in a thousand years, if the rate of population
growth were 1% per annum. But the fact remains, the longer the
compound growth phenomenon continues, the smaller the annual
percentage increase needs to be in order to produce spectacular
results,

Let us assume that we are dealing with a given monetary unit.
We can call it a alent. A young married man begins with 100
talents. Say that he multiplies this capital base by 2% per annum.
At the end of 50 years, the couple has 269 talents, Let us assume
that the family, and their children’s families, multiply at 1% per
annum, on the average, throughout each subsequent family’s life-
time. After 250 years, if the growth rates both of people and capi-
tal persist, the total family capital base is up to 14,126 talents.
Divided by 24 heirs, each husband or wife now has 589 talents.
This is almost a 6-fold increase per family unit, which is consider-
able. We now have 24 family units, even assuming that each heir
has married someone who has brought no capital into the mar-
riage, with almost six times the wealth that the original family
started out with.

What if the capital base should increase by 3%? At the end of
50 years, the original couple would have 438 talents, over a 4-fold
increase. This is quite impressive. But at the end of 250 years, the
family would possess 161,922 talents, over 1,600 times as large.
Even divided by 24 family units, the per family capital base would
be 6,747 talents, or almost 68 times as great.

Consider the implications of these figures, A future-oriented
man—a man like Abraham —could look forward to his heirs’ pos-
sessing vastly greater wealth than he ever could hope to attain
personally. Yet this is the kind of vision God offers His people, just
as He offered it to Abraham: heirs two or three generations later
who will be numerous and rich. God offers a man the hope of sub-
stantially increased wealth during his own lifetime, in response to
his covenantal faithfulness, hard work, and thrift.

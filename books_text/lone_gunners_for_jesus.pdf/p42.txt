40 LONE GUNNERS FOR JESUS

excommunicate you but only suspended you from the Lord’s
Supper. That is, you were supposedly merely forbidden to take
communion. You stated that in my initial letter, I had misrep-
resented your judicial status. I contacted Rev. Schneider, your
former pastor, who informed me today that you were an ex-
communicate prior to the shootings.

You argue that your original excommunication (May, 1993)
was overturned by the higher court that you insisted should
hear the case. The penalty was reduced to a suspension from
the Lord’s Supper, which is the penalty of excommunication
(“away from the communion table”), though indefinite rather
than permanent. But the court warned you to get this matter
settled. It told you to return to church. You never showed up
again.

As you well know, for a suspended church member to break
away and form his own home church is an act of rebellion
(contumacy). It is an excommunicable offence. The local church
excommunicated you a second time. It cleared this decision
with the higher court, which verbally consented. The pastor
was writing a detailed position paper on your case to submit to
you when you shot the two men. But the church did not send
you the letter and the position paper officially informing you of
its decision, which it presumed you understood. The church
has tried to explain all this to the press, but Presbyterian law is
not readily comprehended even by Presbyterians, let alone by
reporters. The church has said repeatedly that it excommuni-
cated you, and you have never publicly denied this.

Spare me your nit-picking technical objections. Jesus warned
against straining at gnats and swallowing camels (Matt. 23:24).
You are overly concerned about my supposed misrepresenta-
tion of your present church membership status, yet you are
without remorse for having gunned down two unarmed men.

The judicial issue here is your ecclesiastical rebellion, You
were formally and publicly designated as an ecclesiastical rebel
many months before you pulled the trigger. The Trinity Pres-

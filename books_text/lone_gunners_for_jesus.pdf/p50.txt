Does the Bible Really Have the Answers?

“The Bible has the answers to life.”
Well-meaning Christians say this to non-
Christians. But do they really mean it?

It’s one thing to tell people that the
Bible has the answers to al) of life’s
problems. It’s another thing to be able to
provide these answers, and also provide
the biblical evidence.

It’s risky to tell someone that the Bible
hhas all the answers if you don’t know
where to look up the answers in the
Bible. Someone may call your bluff.

Meanwhile, most Christian college
professors tell us: “The Bible isn’t a text-
book on [my academic subject].” In
other words, they deny that the Bible has
answers to life’s questions. They deny
that the Bible tells them what they should
believe in their areas of authority,

Economic Questions

‘What if someone asks a Christian this:
“What's the Bible’s answer to poverty?
To inflation? To unemployment? To tax
policy? To economic depression?” What
should the Christian answer? He will
probably not know what to say.

The Institute for Christian Economics
was established in 1976 to provide bibli-
cal answers for economic questions like
these. The ICE is dedicated to applying
the Bible to economic theory and policy.

The ICE has been publishing detailed
books on economic questions since 1982.
Gary North, the founder of the ICE, has
written five volumes of his Economic
Commentary on the Bible: Genesis
through Leviticus. He has also written
dozens of other books relating to eco-
nomics, history, and theology.

If you want answers, you can find
them in these books, or in one of the
ICE’s newsletters: Biblical Economics
Today, Christian Reconstruction, and
Biblicat Chronology, which are sent to
ICE’s supporters.

Humanism Is Bankrupt

For three centuries, Christians have
lived on the scraps that have fallen from
bumanism’s table. Bat humanisn’s table
has always been filled with food stolen
from the Bible.

Now the humanists’ pantry is almost
bare. As humanists have abandoned
belief in an orderly universe, in fixed
moral law, and moral cause and effect,
they have begun to lose faith in science,
technology, and the free market.

Humanists are afraid of pollution,
afraid of economic growth, afraid of
bogeymen such as the greenhouse effect
(no evidence of workdwiie. warming), the:
gtowing hole in the ozone layer (no
evidence that ozone holes keep growing
or that aerosol sprays cause them), and
other non-existent horrors.

The Way to Turn Things Around

Christians can't beat something with
nothing. If humanism is wrong, then
where are the uniquely biblical answers?
If Christians cannot suggest any, why
should the public pay attention to them?
Christians need biblical answers.

To get answers, sign up for a free six-
month subscription to the ICE newslet-
ters. Send your request to ICE, Box
8000, Tyler, TX 75711. Just say, “Sign
me up for my free subscription.”

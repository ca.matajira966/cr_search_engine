4 LONE GUNNERS FOR JESUS

ordain themselves to ministerial status. Self-ordination under
such circumstances is a form of self-excommunication.

You are accused of having shot to death two men and woun-
ding a woman, You have not denied these accusations. You are
said to have used a shotgun at close range - a weapon not not-
ed for its ability to inflict death on a discriminating basis.

The reports say that you ran. If correct, then you did not act
as a man of courage would have acted. You did not act as one
who believed in some elevated principle. You shot and ran.
This indicates to me that you knew in your heart that your act
was, biblically speaking, an act of murder rather than the God-
authorized defense of a just cause. A man defending a just
cause does not run. He commits his act of civil rebellion in the
name of a higher law and then submits himself to the sanctions
of the state for having violated state law.

‘This is what the people of Operation Rescue do. They stand
in front of an abortion clinic, to be beaten by the police, arrest-
ed, sent to jail, fined, and suffer a loss of their income. They
suffer the consequences of their actions. They are people of
courage.

Murder, Defined Biblically

‘The sixth commandment reads, “Thou shalt not kill” (Ex.
20:13). The God who mandates this is also the God who or-
dered the total annihilation of the Canaanites (Deut. 7:16), so
this verse cannot legitimately be interpreted as a defense of
pacifism.

What is murder, biblically speaking? It is the slaying of a
human being by someone who has not been authorized to do so
as a covenantal agent.

A member of the military can lawfully kill a designated ene-
my during wartime. In Old Covenant Israel, the man eligible
to serve in God’s holy army had to pay blood money to the
priesthood at the time of the army's numbering, just prior to
battle (Ex. 30:12-16). This was atonement money (v. 16). So

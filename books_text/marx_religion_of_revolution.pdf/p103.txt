The Cosmology of Ghaos 29

anity destroyed this conception of a united humanity. Histori-
cally, he was on shaky ground; the distinction which the Greeks
drew between Greeks and Barbarians (all those who did not
speak Greek) scems to testify to the incapacity of men to consider
themselves as a unified whole. Still, his basic point is correct;
Christianity in its orthodox form has a vision of a divided hu-
manity: men are either saved or lost, and the distinction is
permanent throughout eternity. Thus, as a humanist, Feuerbach
was far more consistent than Hegel or Strauss. Hegel originally
hoped to defend religion {though not Christian orthodoxy) by
means of his philosophical speculations. Feuerbach took Hegel’s
presuppositions and extended them into a position of radical
materialism. In 1850, he went so far as to claim that man is
what he eats, but this “vulgar” materialism never had any
influence on Marx.

Feuerbach’s vision of alienated humanity — alienated be-
cause of the perversity of religious beliefs which divide man-
kind — combined with his materialism to cause a metamorphosis
in the minds of the Young Hegelians. Years later, Engels de-
scribed the impact of his ideas: “Then came Feuerbach’s Essence
of Christianity. With one blow it pulverized the contradiction, in
that without circumlocutions it placed materialism on the throne
again. Nature exists independently of all philosophy. It is the
foundation upon which we human beings, ourselves products of
nature, have grown up. Nothing exists outside nature and man,
and the higher beings our religious fantasies have created are
only the fantastic reflection of our own essence. The spell was
broken; the ‘system’ was exploded and cast aside, and the contra-
diction [between nature and the Absolute Idea in Hegel’s sys-
tem], shown to exist only in our imagination, was dissolved. One
must himself have experienced the liberating effect of this book
to get an idea of it. Enthusiasm was general; we all became at
once Feuerbachians,””

20. Frederick Engels, Ludwig Feuerbach and the End of Classical German Philosophy
(1888), in Marx and Engels, Selected Works, 3, p. 344. On the impact of Hegelian

32 Marx’s Religion of Revolution

Marx spelled out his humanism in no uncertain terms: *The
criticism of religion ends with the doctrine that man is the supreme
being for man. It ends, therefore, with the categorical imperative lo
overthrow all those conditions in which man is an abased, enslaved,
abandoned, contemptible being. . . .”*

This revolutionary impulse is visible throughout his writings,
and it characterizes his humanistic perspective. Man is his own
highest good; man is therefore his own “ultimate concern,” to
use theologian Paul Tillich’s phrase ~his own God. As such,
man must be as creative as God, and therefore he must purge
his universe of all that is inhumane and therefore evil and irra-
tional. Man’s universe must give glory to its creator, man, and
it cannot. be permitted to reflect anything that is not humane.
Man, in short, is to be the standard of evaluation for all things,
including himself. Engels later summarized this goal as the crea-
tion of a world in which “man no longer merely proposes, but
also disposes,”®> thus claiming as man’s right that which the
Bible limits to God: “A man’s heart deviseth his way: but the
Lorn directeth his steps” (Prov. 16:9),

In an important chapter on “Socialist Humanism,” the Marx-
ist philosopher Maurice Cornforth has defined Marxist human-
ism, and it indicates the totality of the commitment to man (in
opposition to God) in Marxist thought: “Humanism takes the
view which Plato objected to so strongly when it was first put
forward by Protagoras, that ‘man is the measure of all things’.
Everything else is to be judged in accordance with how it affects
men and can be used by men. Everything men do is to be done
for the sake of men and to be judged by its effects on men. Men
are not to regard themselves as existing for the service of any-

24. Marx, “Contribution to the Critique of Hege!’s Philosophy of Right. Intro-
duction” (1844), in Karl Marx: Early Writings, p. 52, [Collected Works, 3, p. 182.] In
all cases throughout my book, the italics are Marx’s, not mine. In his early writings,
Marx was especially liberal in his use of wide spacing of words~ stress - which
English translators have transformed into italics.

95. Frederick Engels, Herr Fugen Dithring's Revolution in Science [Anti-Dithriig}
(London: Lawrence and Wishart, 1934), p. 348. This was published originally in
1877-78 in Vorwérts, a German radical publication. [Collected Works, 25, p. 302.]

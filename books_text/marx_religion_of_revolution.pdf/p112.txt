38 Marx’s Religion of Revolution

What disturbed Marx was the dehumanizing nature of in-
dustrial production, which he identified exclusively with capital-
istic, privately owned production. Thus, he and Engels were able
to spend hours going through Parliamentary reports and other
documents in their search for appropriate “horror stories” about
urban life under the rule of capitalists. This tradition was carried
on successfully by the Fabian socialists in England in our cen-
tury, a fact which might have bothered (or perhaps even amused)
Marx; that such bourgeois reformers‘as the Fabians should have
carried on his intellectual labors would never have occurred to
him.

The alienation theme appears more often in his earlier writ-
ings than in the later ones, although it never disappears entirely
in the so-called “mature Marx.” For this reason, modern schol-
ars have become fascinated with these early manuscripts. The
alienation which they see in the modern world has focused their
attention on Marx’s handling of the subject. As one commenta-

42. The literature on the “Industrial Revolution” is voluminous. Marx’s chap-
ters in Capital, vol. 1: 10, 15, 25, and Engels, The Condition of the Working Class in
England in 1844 (1845), were very early examples of the “horror story” approach.
Conservative thinkers also looked nostalgically back to rural life and its control by
the landed aristocrats, and they concluded that industrialism was a curse. As Nisbet
has pointed out, “This is why the indictment of capitalism that comes from the
conservatives in the nineteenth century is often more severe than that of the
socialists.” Robert A. Nishet, The Sociological Tradition (New York: Basic Books,
1966), p. 26. Fabian writers, especially J. L. and Barbara Hammond, produced
scmi-popular books describing the “intolerable” conditions of the period. Paul
Mantoux’s The Industrial Revolution in the Eighteenth Century (English translation,
1928) is probably the best of these studies. They tend to de-emphasize such factors
as: (1) the lack of capital and savings in the period; (2} the tremendous impact of
the population explosion in these centuries, which lowered per capita income through-
out Western Europe (especially in those areas in which no industrialization oc-
curred); and (3) the misallocation of scarce resources caused by slate regulations
and prohibitions on private industry. See F. A. Hayek (ed.), Capitalism and the
Historians (University of Chicago Press, 1954), for altcmnalive views of the industri-
alization of Europe. Also see 'T. S. Ashton, The Industrial Revolution (New York:
Oxford University Press, 1964); R. M. Hartwell, “The Rising Standard of Living
in England, 1800-1850,” Economic History Review, Second Series, XUI (1961), pp.
397-416; John U. Nef, “The Industrial Revolution Reconsidered,” Journal of Eco-
nomic History, YET (1943), pp. 1-31; Herbert Heaton, Economic History of Europe (New
York: Harper & Row, 1948), chaps. 21-24,

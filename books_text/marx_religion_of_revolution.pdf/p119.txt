The Cosmology of Chaos 45

less you are, the less you express your own life, the greater is
your alienated tife, the more you have, the greater is the store of
your estranged being.”6! All mankind is being dehumanized
under capitalism, including the capitalist: “Estrangement is mani-
fested not only in the fact that my means of life belong to someone
else, that my desire is the inaccessible possession of another, but
also. in the fact that everything is itself something different from
itself—that my activity is something else and that, finally (and
this applies also to the capitalist), all is under the sway of inhuman
power. ”°*

Here we see one aspect of the inevitable nature-freedom prob-
lem. Man’s own creation, which he had hoped would free him
from an irrational nature governed by scarcity, now turns upon
him and becomes his master, “an inhuman power.” Rather than
putting the responsibility for this where Dooyeweerd has shown
that it belongs — in the antinomies of all secular thought and the
societal relations that are based upon them — Marx proclaimed
that capitalist relations of production are the sole cause of man’s
problem. Industrial production, in short,-is the expression of
alienated mankind in general: “We see how the history of industry
and the established odjective existence of industry are the-open book
of man’s essential powers, the exposure to the senses of human
psychology, . . . We have before us the objectified essential powers of
man in the form of sensuous, alien, useful objects, in the form of
estrangement, displayed in ordinary material in dustry. . . .”®

Some reputable scholars have argued that the alienation
theme, while important to Marx as a young man, did not really
play a very large part in his mature writings. Yet in the posthu-
mously published volume three of Capital, we find the same idea
expressed in even clearer and more forceful language: “Capital
becomes a strange, independent, social power, which stands
opposed to society as a thing, and as the power of capitalists by

61. “Meaning of Human Requirements,” EPM, p. 150. [Collected Works, 3, p.
309]
62. EPM, p. 156. [Collected Works, 3, p. 314.]

63, “Private Property and Communism,” EPM, p. 142, [Collected Works, 3, p.
302]

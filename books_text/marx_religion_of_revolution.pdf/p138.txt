64 Marx’s Religion of Revolution

exercise their influence upon the course of the historical struggles
and in many cases preponderate in determining their form. There
is an interaction of all these elements in which, amid all the
endless host of accidents (that is, of things and events whose
inner connection is so remote or so impossible of proof that we
can regard it as non-existent, as negligible), the economic move-
ment finally asserts itself as necessary. Otherwise the application
of the theory to any period of history would be easier than the
solution of a simple equation of the first degree.”!

Tronically, Marx had claimed for his system exactly this kind
of mathematical precision in his early days. In 1843 [note: over
two years before he collaborated with Engels in writing The
German. Ideology, where his economic materialism took its initial
shape], he had written with regard to the study of political
conditions: “In investigating a situation concerning the state one is
all too easily tempted to overlook the objective nature of the circum-
stances and to explain everything by the will of the persons
concerned. However, there are circumstances which determine the
actions of private persons and individual authorities, and which
are as independent of them as the method of breathing. If from
the outset we adopt this objective standpoint, we shall not as-
sume good or evil will, exclusively on one side or the other, but
we shall see the effect of circumstances where at first glance only
individuals seem to be acting. Once it is proved that a phenome-
non is made necessary by circumstances, it will no longer be
difficult to ascertain the external circumstances in which it must
actually be produced and in those in which it could not be
produced, although the need for it already existed. This can be
established with approximately the same certainty with which
the chemist determines the external conditions under which sub-

104. Engels to J. Bloch, Selected Works, 3, p. 487; Correspondence, p. 475. Cf. Letter
to G. Schmidt, 27 Oct, 1890: Selected Works, 3, p. 492; Correspondence, p. 480. See
Mary's similar statement in Capital, 3, p. 919. (Capital, 3, pp. 791-92.} For a
contemporary analysis of accidents and necessity, see A. P, Chermenina, “The
Concept of Freedom in Marxist-Leninist Ethics,” The Soviet Review, VI (1965), p.
50.

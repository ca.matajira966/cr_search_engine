xvi Marx’s Religion of Revolution

under assault from younger scientists and social philosophers.
Today’s phase of the assault on the rational-phenomenal, began
in the West in philosophy in the late nineteenth century (e.g.,
Nietzsche), in science at the turn of the century (Einstein) and
especially in the 1920°s (quantum physics), and in social philoso-
phy and popular science after 1964.7!

The quest for randomness is systematically pursued by scien-
tists and mathematicians. Randomness is the standard by which
science measures meaningful (i.e, non-random) patterns, yet
scientists have had a difficult time in creating pure randomness
on a rational basis, They are bedeviled by creeping order. “How
to use randomness, how to create it and how to recognize the
real thing have become challenging questions in the computer
era, touching many distant areas of science and philosophy,”
writes James Gleick. “The randomness business is riddled with
pitfalls; creeping nonrandomness has undercut expectations of
many consumers, from state lotteries and tournament bridge
players to drug manufacturers and court systems.” A perfect
shuffle or “riffle shuffle” of playing cards brings the deck back
to its original order in eight shuffles. Thus, if shuffles were
perfect, card playing would cease. But science, like the profes-
sional gambler, seeks perfection. Herein lies a dilemma. Scien-
tists, in order — an interesting phrase! — to rationalize their cho-
sen callings, are searching for ways to produce randomness on a
predictable, mass-produced, “scientific” basis. Gleick points out
that random-number generators keep producing strings of not-
quite-random numbers. “No string of numbers is really random
if it can be produced by a simple computer process.””? And so
the intellectual dilemma goes on, generation after generation,
preliminary software program after preliminary software pro-
gram. (“Today, beta-testing; tomorrow, the world!”)

21. Gary North, Unholy Spirits, Introduction, ch. 1,

22. James Gleick, “Achieving Perfcct Randomness,” New York Times (April 19,
1988).

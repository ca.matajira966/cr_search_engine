88 Marx’s Religion of Revolution

of the social process of production ~ antagonistic not in the sense
of individual antagonism, but of one arising from conditions
surrounding the life of individuals in society; at the same time
the productive forcés developing in the womb of bourgeois soci-
ety create the material conditions for the solution of that antago-
nism, This social formation constitutes, therefore, the
closing chapter of the prehistoric stage of human society,”'®

All earlier societies have been merely “prehistoric.” We may
call our era “history,” but. that is a misnomer. What we call
history must be abolished if man is to survive. Marx, therefore,
appealed to revolutionary chaos to bring an end to this era and
to inaugurate true human history. Man must accomplish this,
for man is Marx’s only god. Eschatology for Marx is the restora-
tion of the society free from alienation and brought into being
by man; the escape from the bondage of present history will be
achieved.

In linking Marx to the ancient cosmology rather than to the
Hebrew-Christian tradition one problem does exist. On the sur-
face, at least, Marx’s conception of history is linear; the ancients
held a cyclical view of history. This is an apparent contradiction.
Marx’s view seems to be closer to the Christian viewpoint:
history does progress in the Marxian system, and historical facts
are important. In this sense, Marx is in the Western tradition;
no one who is a part of that tradition can completely escape the
influence of Augustine’s linear history. It would be odd if Marx
had not shared with ail Western thinkers some of the premises
of Augustine. Nevertheless, in several important respects, Marx’s
history és potentially cyclical in nature. Ef man fell from a primi-
tive society in which there was no alienation, what is to prevent
a similar fall back into alienation after the coming revolution has
produced the Golden Age?. Marx said specifically that private
property did not cause the fall into alienation but rather that the
reverse was true. Though he assumed that a mere reordering of
the social environment would regenerate mankind forever, he

163. Marx, Preface, A Contribution to the Critique of Political Economy, p. 13; cf.
Selected Works, 1, p. 504,

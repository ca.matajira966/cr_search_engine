The Cosmology of Chaos 95

and Engels came to the conclusion that Russia could have its
Revolution apart from the economic foundations required for
such an event. In 1875 Engels wrote that the Russian communal
land system, the mir, could serve as the foundation for a new
society, if only there were a revolution in the West immediately
following.'®° The preface to the Communist Manifesto’s Russian
edition (1882), co-authored by Marx and Engels, asserted the
same thing.!®6 In a letter written by Marx in 1877, he had
announced: “If Russia continues to pursue the path she has
followed since 1861, she will lose the finest chance ever offered
by history to a nation, in order to undergo all the fatal vicissi-
tudes of the capitalist regime.”!87

It is not altogether clear as to why Marx and Engels saw fit
to abandon their theoretical framework in order to make room
for the possibility of a Russian Revolution. It may have been
that Marx was impressed by the fact that the Russian radicals
were very often openly his followers. Russia was the first country
in which a wide distribution of Capital was experienced. (The
Czar’s censors thought that such a large, ponderous volume
would not be read by anyone.) Buber has seen in this admission
by Marx a desire on his part to achieve the kind of communal
society which he had always dreamed of: the mir scemed to be
just this sort of ideal society.!8° Whatever the reason, the “stage
theory” of economic and social evolution was dealt a hard blow
by the founders of the Marxian system,

 

he progressed away from tribal unity. Dobb, “Marx on Pre-Capitalist Economic
Formations,” Science and Society, XXX (1966), pp. 319-25. He is supported in this
interpretation by another Marxist, Eric J. Hobsbawn: Introduction to Marx’s
Pre-Capitalist Economic Formations, p. 36. The reason for all this hedging is that
Marx’s historical schema is not supported by the historical facts, and Russia and
China in the 20th century are the final refutation of the Marxist version of historical
stages.

185. Engels, On Social Relations in Russia (1875), in Selected Works, 2, p. 395.

186. Cf. Marx-Engels, Correspondence, p. 335.

187. Marx to the Editor of the Oljecestuenniye Zapisky, late 1877; ébid., p. 353.

188. Buber, Paths in Utopia, pp. 90-94,

The Cosmology of Ghaos 97

Without the inevitability doctrine, the system loses its force-
fulness; without its universality, the same thing is true. Marx
drew his view of society and history in large, compelling sweeps
of his pen, and as a result, a new religion swept over Europe and
Asia. Yet whenever they found themselves in some intellectual
trap of their own making, Marx and Engels quibbled their
system away in obscure letters to their associates. It was dishon-
est intellectually, for Marx did not really think that his vision of
humanity was less than universal. In 1871 he wrote to Kugel-
mann: “The struggle of the working class against the capitalist
class and its state has entered upon a new phase with the struggle
in Paris, Whatever the immediate results may be, a new point
of departure of world-historic importance has been gained.”!9)
The great Revolution was coming, period. But anything which
competed with that hope was superfluous for Marx; even theo-
retical consistency was not to stand in the way of the Revolution,
any kind of Revolution, even a rural-communal one in backward
Russia. At this point, Marx’s total religious commitment to the
ideal of revolution should be obvious, The Revolution had be-
come a passion for him, a truly holy goal, and everything— family,
wealth, time, and even theoretical consistency — had to be sacri-
ficed to it.

Revolution or Repentance

The Marxist message is, above all, a call to revolution; the
society must be turned over if it is to be made whole again, In
opposition to the Marxian perspective, the traditional Christian
message has been a call to repentance; individual men must turn
around from the path of destruction. Marx, in spite of his appar-
ent moralism— almost a Victorian moralism—always denied

191, Marx to Kugelmann, 17 April 1873: Letters to Kugelmann, p. 125, [Selected
Works, 2, p. 442:] Some kind of total revolution was vital for Marx, but what kind
would it be? Raymond Aron has found at least three wholly different concepts of
the Revolution in Marx: a Blanquist conspiratorial one, an evolutionary one, and
the idea of the Permanent Revolution. Aron, The Opium of the Intellectuals, p. 47,

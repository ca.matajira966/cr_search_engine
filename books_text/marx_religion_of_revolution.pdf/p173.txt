The Cosmology of Chaos 99

to return to the path of peace, after a revolutionary explosion,
with a positive balarice sheet. But revolutionary means remain
on balance contrary to the ends envisaged.”!%

Paradise can be achieved only by total, irrational (or hyper-
rational) destruction,

Paradise

Marx thought of himself as an optimist. However bad the
conditions of life might be under capitalism, and however tyran-
nical things might become under the initial dictatorship of the
proletariat, there is still hope. A new age is coming, Marx be-
lieved, and with it would come a new mankind. He never spelled
out the details of all the elements of life in the “kingdom of
freedom,” but he did give a few hints: “Communism is the positive
abolition of private property, of human selfalienation, and thus the
real appropriation of human nature through and for man. It is,
therefore, the return of man himself as a social, i.¢., really human,
being, a complete and conscious return which assimilates all the
wealth of previous development. Communism as a fully devel-
oped naturalism is humanism and as a fully developed humanism
is naturalism, [tis the definitive resolution of the antagonism
between man and nature, and between man and man. It is the
true solution of the conflict between existence and essence, be-
tween objectification and self-affirmation, between freedom and
necessity, between individual and species. It is the solution of the
riddle of history and knows itself to be this solution.”!97

As we have already scen, the division of labor is to be
abolished in this communist-humanist-naturalist Golden Age.
While Engels may have abandoned this hope, it was certainly a
fundamental tenet of the early Marxist credo. Given this prem-
ise, “the communist revolution, which removes the division of

196. Aron, The Opium of the Intellectuals, pp. 94, 98.

197. Marx, “Private Property and Communism,” EPM, Bottomore translation,
Karl Marx: Early Writings, p. 155. Cf. EPM, p. 135, in the Milligan translation. The
latter contains a key typographical error and is generally unclear, {Collected Works,
3, pp. 296-97.)

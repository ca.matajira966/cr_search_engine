The Cosmology of Chaos 103

Revolution (immediately for Bakunin, gradually ‘withering’ for
Marx), then how is the ‘collective’ to run its property without
becoming an enormous State itself in fact even if not in name?
This was the contradiction which neither the Marxists nor the
Bakuninists were ever able to resolve.”208

The necessity of productive planning implies scarcity: pro-
duction is necessary only because all people do not have every-
thing they want at exactly the moment when they want it. Raw
materials must be fashioned into goods or indirectly into serv-
ices; these goods must be shipped from place to place. These
things involve time {interest on the investment of capital goods}
and labor (wages). Production, in short, demands planning. Society
is never faced with a problem of “to plan or not to plan.” The
issue which confronts society is the question of whose plan to use.
Marx denied the validity of the free market’s planning, since the
market is based upon the private ownership of the means of
production, including the use of money. Money, for Marx, was
the greatest curse of all non-communist societies. It was his
fervent hope to abolish the use of money forever. At the same
time, he denied the possibility of centralized planning by the
state. How could he keep his “association” from becoming a
state? The Fabian writer G. D. H. Cole has seen what the
demand for a classless society necessitates: “But a classless soci-
ety means, in the modern world, a society in which the distribu-
tion of incomes is collectively controlled, as a political function
of society itself. It means further that this controlled distribution
of incomes must be made on such a basis as to allow no room for
the growth of class-differences.”2!° In other words, given the
necessity for a political function in a supposedly state-less world,
how can the Marxists escape the criticism supposedly offered
by Leon Trotsky: “In a country where the sole employer is the

208. Murray N, Rothbard, “Left and Right: The Prospects for Liberty,” Left and
Right, T (1965), p. 8. See the similar remarks of Abram L, Harris, “Utopian
Elements in Marx's Thought,” Ethics, LX (1949-50), pp. 93-94.

209, Marx, “On the Jewish Question,” in Bottomore (ed.), Karl Marx: Early
Writings, pp. 32-40, [Collected Works, 3, pp. 168-74,]

210, Cole, The Meaning of Marzism, p. 249.

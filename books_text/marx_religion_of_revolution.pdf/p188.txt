114 Marx’s Religion of Revolution

“is independent of the amount of labour required to appropri-
ate its useful qualities.”!° A thing can be useful to men, in other
words, even if no one has expended any labor in creating it; a
mountain stream or unimproved land are examples. But ex-
change value, according to Marx, is something else again; ex-
change value is the congealed form of human labor, since human
labor is the only means of creating value. Marx offered the old
Aristotelian argument that for an exchange to take place, there
must be a common element of equal quantity in each of the
exchanged items. “The two things must therefore be equal to a
third, which in itself is neither the one nor the other. Each of
them, so far as it is exchange value, must therefore be reducible
to this third.”!! The common element cannot be use-value, he
hastened to add, because “the exchange of commodities is evi-
dently an act characterized by a total abstraction from use-
value.”!® Use-value makes possible an exchange, since people
will not bother to enter a market in order to exchange useless
goods, but use-value is not the basis of the exchange. Then what
is? He concluded that the exchanged objects each must contain
equal quantities of human labor. It is not a question of any
physical or aesthetic qualities inherent in any particular eco-
nomic good: “. . . there is nothing left but what is common to
them all; all are reduced to one and the same sort of labour,
human labour in the abstract.”

Fundamental to the Marxian economic system is the belief
that things will not be exchanged unless the common element,
human labor, is present in each good to be exchanged. This,
however, is a fallacious concept, and it was dropped by modern
economics after the marginalist-subjectivist schools gained pre-
dominance in the later 19th century. Exchanges take place when

10. Capital (Chicago: Charles H. Kerr, [1867] 1906), 1, p. 42. The Modern
Library edition is a reprint of the Kerr edition, [Capital (New York: International
Publishers, [1967] 1979), 1, p. 36.]

H. Zeid, 1, pp. 43-44. [Zbid. 1, p. 37]

12, Ibid, 1, p. 44, [Ibid 2, p. 87.]

13, Ibid, 1, p. 48, [Zbid. 1, p. 38.]

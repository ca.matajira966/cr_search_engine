The Economics of Revolution 149

The cost price of commodities in example I is 70. The
average rate of profit is 22 percent. Thus, the market price must
be 92: 70 + 22 (.22 x 100). In the preceding chart, it was
demonstrated that the real value of the commodities is 90 (50 c
+ 20v + 208). Thus, the deviation of the actual price of 92 from
the value price of 90 is + 2. If the labor theory of value were
correct, there could be no such deviation,

The “Price of Production” Theory

In explaining this obvious contradiction, Marx appealed to
the idea of a “price of production” theory. This same escape had
been used by both Adam Smith and David Ricardo, although
Marx rejected their use of a similar approach. First, he admit-
ted the problem: “One portion of the commodities is sold in the
same proportion above in which the other is sold below. their
values.”%* This statement is in absolute opposition to his basic
assumption in volume 1: “The creation of surplus-value, and
therefore the conversion of money into capital, can consequently
be explained neither on the assumption that commodities are
sold above their value, nor that they are bought below their
value.”®5 Marx went on: “And it is only their sale.at such prices
which makes it possible that the rate of profit for all five capitals
is uniformly 22%, without regard to the organic composition of
these capitals.” Yet by his own definition, profit can be com-
puted only in terms of the organic composition of capital: s/c+v.
“The prices which arise by drawing the average of the various
rates of profit in the different spheres of production and adding
this average to the cost-prices of the different spheres of produc-
tion, are the prices of production. They are conditioned on the
existence of an average rate of profit, and this, again, rests on the
premise that the rates. of profit in every sphere of production,
considered by itself, have previously been reduced to so many

98. Ibid., 3, pp. 233-34. [Ibid., 8, pp. 198-99.]
94. Iid., 3, p. 185, [Ubid., 3, p. 187]
95, ibid, 1, p. 179. [Fid., 1, p. 161]
96. Hbid., 3, p. 185. [Zbid, 3, p. 157]

150 Marx’s Religion of Revolution

average rates of profit.”% Finally, he defined his terms: “The
price of production of a commodity, then, is equal to its cost-
price plus a percentage of profit apportioned according to the
average rate of profit, or in other words, equal to its cost-price
plus the average profit.” He then used the analogy of a huge
national stockholding company in order to resolve the problem;
this crude aggregate was his basic answer:

While the capitalists in the various spheres of production recover
the value of the capital consumed in the production of their commadi-
ties through the sale of these, they do not secure the surplus-value, and.
consequently the profit, created in their own sphere by the production
of these commodities, but only as much surplus-value, and profit, as
falls to the share of every aliquot part of the total social capital out of
the total social surplus-value, or social profit produced by the total
capital of society in all spheres of production, . . . The various capi-
talists, so far as profits are concerned, are so many stockholders in a
stock company in which the shares of profit are uniformly divided for
every 100 shares of capital, so that profits differ in the case of the
individual capitalists only according to the amount of capital invested
by each one of them in the social enterprise, according to his invest-
ment in social production as a whole, according to his shares.°

But what kind of answer is this? Capitalists, except in the
case of very limited cartels, never act in this fashion. They
compete with each other, receiving their profits or taking their
losses according to the competitive position of their individual
establishments. If capitalists actually did act as if they were
members of a huge stock company, then why should any of them
receive losses? If the company is part of a huge aggregate,
automatically receiving its share of the average rate of profit,
then it should never fail. But one of the main tenets of the
Marxist faith is that capitalists become increasingly competitive,
driving their competitors out of business whenever possible. The

97. lbid., 3, p. 185. [Z6id., 3, p. 157.]
98, Ibid, 3, p. 186, [Zbéd., 3, p. 187.]
99, Ibid., 3, pp. 186-87. [Z6id., 3, p. 158.)

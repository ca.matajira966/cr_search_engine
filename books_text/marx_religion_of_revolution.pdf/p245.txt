Conclusion 171

everything that takes place in history is governed by the absolute
sovercignty of a personal God. Thus, Christians rest their earthly
hope in the providence of God. History is neither random nor
determined by impersonal forces. It is governed by the God who
created the universe.'*

The Bible teaches the doctrine of creation, meaning creation
out of nothing. [t teaches that man rebelled against God, and
both nature and man now labor under God’s historical curse, It
tells of Jesus Christ, the Son of God: His birth, ministry, death,
resurrection, and ascension to heaven to sit at the right hand of
God. It tells of Pentecost, when He sent His Holy Spirit. It tells
us of Christ’s church in history, and of final judgment. There is
direction in history and meaning in life.

Christians are told to believe in “thousands of generations”
as their operating time perspective, This is probably a meta-
phorical expression for history as a whole. Few if any Christians
have taught about a literal 25,000-year period of history (1,000
x 25 years). The point is, the Bible teaches that the kingdom of
God can expand for the whole of history, while Satan’s empires
rise and fall. There is no long-term continuity for Satan’s institu-
tional efforts. He has nothing comparable to the church, Gad’s
monopolistic, perpetual institution that offers each generation
God’s covenantal word, community, and sacraments.

if growth can be compounded over time, a very small capital
base and a very small rate of growth leads to the conquest-of the
world. Growth becomes exponential if it is maintained long
enough. This is the assured basis of Christianity’s long-term
triumph in history. God is faithful. The temporary breaks in the
growth process due to the rebellion of certain generations of
covenanted nations do not call a halt to the expansion of the
kingdom.

The errors, omissions, and narrow focus of any particular
Christian society need not inhibit the progress of Christ’s earthly

14. Gary North, The Dominion Covenant: Genesis (nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), ch, 1: “Cosmic Personalism.”

15. Gary North, The Sinai Strategy: Economies and the Ten Commandments (Tyler,
‘Texas: Institute for Christian Economics, 1986), pp. 101-3.

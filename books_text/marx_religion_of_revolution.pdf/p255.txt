Socialist Economic Calculation 181

all exchange goods on a market, since it is, by definition, the
most exchangeable good. Without:it, economic society as we
know it would not exist.

Marx saw this fact very early in his career. He realized the
interdependence of money and the division of labor, and his
absolute hostility against the division of labor led him to reject
the use of money in his coming post-Revolutionary society. He
expressed his objections to money in his early essay, “On the
Jewish Question,” which appeared in the German-French Yearbooks
in 1844. He characterized the Jew of his day in the worst (for
him) possible terms: the Jew is the ultimate bourgeois figure.
“What is the profane basis of Judaism? Practical need, self-interest.
What is the worldly cult of the Jew? Huckstering. What is his
worldly god? Money.”® Money, for the Jew, has become his
instrument of economic control and social powet: “The Jew has
emancipated himself in a Jewish manner, not only by acquiring
the power of money, but also because money had become, through
him and also apart from him, a world power, while the practical
Jewish spirit has become the practical spirit of the Christian
nations. The Jews have emancipated themselves in so far as the
Christians have become Jews.” Thus, he wrote: “In the final
analysis, the emancipation of the Jews is the emancipation of
mankind from Judaism.”'° In other words, the true freedom of
the Jew can be attained only when the Jews’ source of power is
removed: money. With it, of course, capitalist production must
also be destroyed.

Money and Alienation

Money, for Marx, became a kind of symbol of capitalism.
He saw it as capitalism’s worst feature, “Money is the alienated
essence of man’s work and existence; this essence dominates him

8, Karl Marx, “On the Jewish Question,” in Marx, Early Writings, edited by
T. B. Bottomore (New York: McGraw-Hill, 1964), p. 34. [Collected Works, 3, pp.
169-70,]

9, Ibid, p. 38. [lid %, p. 170.]

10. Did, p. 34. {Ubid., 3, p. 170]

Socialist Economic Calculation : 197

Planning and Production

The argument has been extended by Georg Halm. As he
shows, the whole question of saving and interest rates cannot be
solved by socialism except by arbitrary (and ultimately irra-
tional) declarations by the authorities. The planners must decide
how much of the presently available capital should be devoted
to consumers’ goods and how much to producers’ goods. The
time preference factor is, of course, basic to these calculations: how
much are present goods valued by the public in comparison with
future goods? Under capitalism, the interest rate allocates sav-
ings, and this in turn establishes the quantity of capital available
for investment in productive activities. Such a free capital market
cannot exist under socialism.

Because capital is no longer owned by many private persons, but
by the community, which itself disposes of it directly, a rate of interest
can no longer be determined. A pricing process is always possible only
when demand and supply meet in a market, when the competition of
many offerers and demanders, the mutual out-bidding on the part of
the buyers and under-cutting on the part of the sellers, leads by trial
and error to the gradual emergence of a price, which may be called
normal because it is that price at which the available supply, no more
and no less, can be exactly disposed of. . . . In the socialistic economy
such a process of interest-determination would be impossible. There
can be no demand and no supply when the capital from the outset is
in the possession of its intending user, in this case the socialistic central
authority.

Now it might perhaps be suggested that, since the rate of interest
cannot be determined automatically, it should be fixed by the central
authority. But this likewise would be quite impossible. It is true that
the central authority would know quite well how many capital-goods
of a given kind it possessed or could procure by means of a compulsory
restriction of consumption; it would know the capacity of the existing
plant in the various branches of production; but it would not know how

 

“On the Theory of the Centrally Administered Economy: An Analysis of the
German Experiment,” Economica, XV (May & Aug, 1948); reprinted in Morris
Bornstein (ed.}, Comparative Economic Systems: Models and Cases (Homewood, Mtinois:
Irwin, 1965), pp. 157-97,

196 Marx’s Religion of Revolution

entrepreneur must take inte account if he is to stay in business),
the tastes of the public, the capital available, and a host of other
data must be accomplished by the planning agency. Large-scale
experiments would have to be conducted by the planners
in order to discover the cheapest methods of production. This
is mandatory: “. . . it has no other means of determining 4
priori the technical coefficients most advantageous economically,
and must of nécessity resort to experiments on a large scale in order
to decide afterwards which are the most appropriate organiza-.
tions, which it is advantageous to maintain in existence and to
enlarge to obtain the collective maximum more easily, and which,
on the other hand, it is best to discard as failures.”*2 Barone
ridicules Marx's idea that collectivist planning would somehow
avoid the kinds of decisions made under “anarchistic” capitalism.

Hayek is more pessimistic than Barone, and after surveying
the number of problems which would face the planning board,
he concludes that in an advanced society the decisions to be
made by the board before embarking on any production plan
would “be at least.in the hundreds of thousands.”# Lionel
Robbins regards Hayek’s estimate as overly optimistic. The task
is overwhelming: “It would necessitate the drawing up of mil-
lions of equations on the basis: of millions of statistical tables
based on many more millions of individual computations. By the
time the equations were solved, the information on which they
were based would have become obsolete and they would need
to be calculated anew." But Robbins and Hayek are clearly
pikers in their evaluations of the problem. They are free market
advocates who have never been connected with any major social-
ist planning project.

More Analysts than People

In order to gain an idea of the real problem facing the
planners, we must go to an expert, Victor M. Glushkov, the head

42. Barone, in Hayek (ed.), Callectivist Economic Planning, pp. 288-89.
43, Hayek, ibid,, p. 212.
44, Lionel Robbins, The Great Depression (London: Macmillan, 1934), p. 151,

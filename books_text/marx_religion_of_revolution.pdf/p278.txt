204 Marx’s Religion of Revolution

Naum Jasny, in the opening paragraphs of his monumental
study, Soviet Industrialization, 1928-1952, offers one cogent explana-
tion:

The Bolsheviks came on the scene as fighters for socialism and
against exploitation, for a great improvement in the well-being of
everybody. What they achieved was a great increase in the rate of
exploitation, reducing the people’s share of the national income to an
extent nobody had believed possible. This strangulation of consump-
tion put such large funds in the hands of the state as to permit extensive
industrialization and even greater militarization, despite loss and waste
of every kind caused by wars, internal strife, mismanagement, and so
on,

Tf one looks for figures as evidence of this revolution, there are
probably no better ones than these: While the total personal income
(calculated at constant prices) of the expanded population increased
by about one-third from 1928 to 1952, the reat value of the funds in the
hands of the state for investment, military and other expenses, grew
almost eight-fold. This transformation must be considered a financial,
economic, and social revolution?

Without such a blatant repression of the rights and wants of
the Soviet population, the statistics of industrial output would
never have shown such a phenomenal growth rate. The costs
‘were enormous in human misery. A million people starved in
1933.9 Stalin’s forced collectivization of the farms in the early
1930’s resulted in at least five million people being shot or
deported.!° When the magnitude of such costs are considered,
Rothbard’s question does not seem out of place: “By what right
do you maintain that people should grow faster than they volun-

 

consequence of greater abundance and wider assortment, compared with the acute
‘goods famine’ which prevailed for so many years. Unsold stocks of unsalable goods
ate causing worry to the authorities, The public is becoming more choosy, as
supplies and living standards increase.” Alec Nove, The Soviet Economy: An Introduc-
ion (rev. ed.; New York: Praeger, [1965] 1966), p. 184,

8. Jasny, Soviet Industrialization, pp. 1-2,

9, Iid., p. 73.

10. Robert W. Campbell, Soviet Economic Power (2nd ed.; New York: Houghton

Mifflin, 1966), p. 24.

230 Marx’s Religion of Revolution

been discussed. The reader is referred to some of the easily
available literature on the subject. It is enough to point out
that agriculture and housing are invariably the two weak spots
of any industrialized socialist nation. Poland has become the
most productive farm area in the Soviet bloc; the solution was
simple: Poland decentralized agriculture almost completely. Farm-
ers are relatively free to grow what they want. The Soviet Union
will continue to suffer from low output per farmer until she
follows Poland’s lead.

In the final analysis, the theory of Mises, Hayek, and the
others appears to be justified, or at least hardly disproved, by
Soviet economic theory and practice. Most non-Marxists com-
mentators are willing to admit that in terms of economic effi-
ciency as such — low production costs, higher output, allocation
according to demonstrated consumer preferences — the free mar-
ket economies outperform the Soviet system. It must be borne
in mind, of course, that the goals of the Soviet hierarchy have
seldom been consumer preference oriented; the goal has been the
establishment of political power. Waste was a less important
consideration than the strengthening of the Party and the Soviet
state. There has been growth, to be sure, especially in the areas
of heavy industry and military armaments. In terms of economic
growth as such, Bergson’s restrained conclusion is certainly ac-
curate enough: “As it has turned out, the outstanding example
of socialism that has yet to come into existence has distinguished
itself so far not so much for effective use of resources as for the
novel and strange ends imposed on a great state.”!©° But Jan

99. Oxi the agriculture question, sce Naum Jasny, The Socialized Agriculture of the
USSR (Stanford, California: Stanford University Press, 1949); Lazar Volin, A
Survey of Soviet Russian Agriculture (Washington, D,C,; U.S. Department of Agricul-
ture, 1951); Gregory Grossman, “Soviet Agriculture Since Stalin,” The Annals,
CCCIIT (1956), pp. 62-74; Lazar Volin, “Agricultural Policy of the Soviet Union,”
Comparisons of the United States and Soviet Economnies (Joint Economic Gommittce,
Congress of the United States, 86th Cong., Ist Session, 1959), pt. T; excerpts in
Bornstein and Fusfeld (eds.), The Soviet Economy, pp. 168-201; Nancy Nimiv,
“Agriculture Under Khrushchev: The Lean Years,” Problems of Communism, X1V
(May-June, 1965); reprinted in Bornstein and Fusfeld, itid., pp. 202-15.

108. Bergson, Economics of Soviet Planning, p. 358.

 

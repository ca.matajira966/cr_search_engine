The Myth of Marx’s Poverty 237

As a student at the Universities of Bonn and then Berlin, he
spent prodigious quantities of his father’s money. It was a habit
he was never to break: spending other people’s money. It re-
quired the adoption of a lifetime strategy of begging. In late
December, 1837, a few months before his death, his father wrote
a long, despairing, and critical letter to him. It is obvious that
the father knew his son only too well. He described in detail his
son’s personal habits — habits that remained with him for a
lifetime:

God’s grief!!! Disorderliness, musty excursions into all departments
of knowledge, musty brooding under a gloomy oil-lamp; running wild
in a scholar’s dressing-gown and with unkempt hair instead of running
wild over a glass of beer; unsociable withdrawal with neglect of all
decorum and éven of all consideration for the father... . And is it
here, in this workshop of senseless and inexpedient erudition, that the
fruits are to ripen which will refresh you and your beloved [Jenny von
Westphalen—G.N.], and the harvest garnered which will serve to
fulfill your sacred obligations!?!

 

The desperate dying man then resorted to sarcasm, only too
well deserved, regarding his son’s capacity for spending money:

As if we were men of wealth, my Herr Son disposed in one year of
almost 700 talers contrary to all agreement, contrary to all usage,
whereas the richest spend less than 500. And why? J do him the justice
of saying that he is no rake, no squanderer, But how can a man who
every week or two discovers a new system and has to tear up old works
laboriously arrived at, how can he, I ask, worry about trifles? How can
he submit to the pettiness of order? Everyone dips a hand in his pocket,
and everyone cheats him, so long as he doesn’t disturb him in his
studies, and a new money order is soon written again, of course.!!

 

notoriously boring, and nineteenth-century German university lectures may have
established the modern international world record in the production of student
boredom, Oxford’s lectures were boring, Adam Smith insisted, bur at feast they
were not in German,

10. Herschel Marx to Karl Marx, 9 Dec, 1837; Collected Works, 1, p. 688.

11. Jbid., 1, p. 690.

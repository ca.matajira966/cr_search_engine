244 Marx's Religion of Revolution

a penny. But the money still rolled in. In December of 1844,
he received 1,000 francs for the publication of The Holy Family.3!
Engels also gave him the advance he had received in May for The
Condition of the Working Class in England.** Kéln sent him another
750 francs. He also took advance payment of 1,500 francs for a
book he never got around to writing. The publisher made a
serious financial mistake. After signing an initial contract with
Marx that promised a payment of 1,500 francs upon completion
of the manuscript, and another 1,500 at the time of publication,33
he relented for some reason and sent Marx the initial 1,500 a few
months later. He would spend the next few years demanding the
manuscript or the return of his money, all to no avail. (As a
publisher who has also been sucked in on several occasions by
the pleas and promises of initially enthusiastic, boldly self-
confident, perpetually indebted, and “ideologically pure” authors,
ican sympathize with him. The surest way to bury any book
publishing project is to pay the prospective author in advance.)
He also borrowed 150 francs from his brother-in-law in Novem-
ber of 1847.4 There is no record of any repayment. In an 1847
letter to Engels, Marx brings up the life-long theme. of themes
in his correspondence with Engels: “money.”

We know that Marx received 6,000 francs from his father’s
estate in March of 1848. His father had died in 1838; Marx could
not persuade his mother and his Uncle Lion Philips to give him
the money until 1848." Robert Payne claims, without offering
any substantiating evidence, that Marx immediately spent 5,000

30. Ibid., p. 61.

31. Draper, Chronicle, p. 16.

$2, Raddatz, Kar! Marx, p. 61. I may be double counting here: Raddatz and
Draper do not mention each other’s data on Marx’s book income. Maybe they are
referring to the same payment.

$3, “Contract,” | Feb. 1845, in Collected Works, 4, p. 675.

34. Draper, Chronicle, p. 28.

35, Marx to Engels, 15 May 1847, Collected Works, 38, p. 116.

36. Oscar J. Hammen, The Red “Gers: Karl Marx and Friedrich Engels (New York:
Scribner's, 1969), p. 190.

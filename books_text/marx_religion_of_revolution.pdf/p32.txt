xxxli Marx’s Religion of Revolution

humanist intellectuals, always respectful of those who win major
wars and also respectful of any radical group that conducts a
bloody revolution against traditional, religion-supported author-
ity, haveresurrected Marx’s intellectual reputation posthumously.
In short, had it not been for Lenin, you would never had: heard
about Marx, The library shelves devoted to Marxism would be
devoted to some other topic. (If the Germans had won World
War HH, rest assured that many of these shelves would today be
filled with books praising the creative humanist vision and the
rational economic planning of the Nazis. The fascination that
Nazism had for Western scholars and politicians during the
1930's, including British economist John Maynard Keynes,'® not
to mention U.S. businessmen who traded extensively with the
Nazi State,'9 is a story not found. in today’s textbooks. Why not?
Because Hitler lost.) Scholars want to be on the winning side.
Marx is important for the religion he preached, not the
footnotes he assembled. He is important because he provided
what appeared to be scientific proof for demonic revolution. By
capturing the minds of several generations of bloody revolution-
aries and ideological gangsters, Marx and Engels changed the
history of the world. It was Marx’s vision of an eschatological
apocalypse, not his turgid scholarship, that won the day. He
provided generations of intellectuals with what they have sought
above all: attachment to political victors, either vicariously or
directly in their service. It has also been emotionally convenient

18. Keynes wrote these words in the Foreword to the 1936 German-language
edition of his General Theory of Employment, Interest, and Money. “The theory of
aggregate production, which is the point of the following book, nevertheless can be
touch easier adapted to the conditions of a totalitarian state [eines totalen Staates]
than the theory of production and distribution put forth under conditions of free
competition and a large degree of laissez-faire. This is one of the reasons that
justifies the fact that I call my theory a general theory.” Translated with the German.
original by James J. Martin, Revisionist Viewpoints (Boulder, Colorado: Ralph Myles
Press, 1971), pp. 203, 205. The citation also appears in The Collected Writings of John
Maynard Keynes, vol. 7 (New York: St. Martin’s, 1973), p. xvi.

19. Charles Higham, Trading With the Enemy: An Exposé of the Nazi-American Money
Piot, 1993-1949 (New York: Delacorte, 1983); Antony C. Sutton, Wall Street and the
Rise of Hitler (Suffolk, England: Bloomfield, [1976]), originally published by °76
Press, Seal Beach, California,

270 Marx's Religion of Revolution

Bretonne, Restif de !a, xiii
Brook Farm, xli
Buber, Martin, 92
bureaucracy

LSD &, 23

Satan &, xxxv

USSR, 192-95, 221
bureaucrats, 166
Burroughs, William, 76

Caiillois, Roger, 74-75, 80
calling, 42
Calvin, John, Ix
Calvinism, 54, 57-58
Camus, Albert, xxvi
Capital, lii, liv, lvi, 95
Capital
accumulation of, 125-129
allocation of, 190
concentration of, 127-128
organic composition of, 125n, 146
pricing of, 187-188
two kinds of, 122-123
capitalism
alienation, 44-46
crises, 134-144
dehumanization, 44-46
dynamic, 159
revolution &, xvii
capitalists, 155-156
Capra, Fritjof, x
Carew Hunt, R. N., 65-66
Cartebach, Julius, 16n
Carnival, 75
Cathars, 77

cause and effect, xii, xv, xviii, xexviii, 66

chance, 54, 67-69

change, 189

change vs. law, 19-20, 22-23
(see law & change)

chaos, ix, x, xviii, 87, 162 (see also
revolution)

chaos cults, 2, 73-76, 108

chaos festivals, xviii
Chapman, Janet, 207-209
choice, 177-178, 188
Christian Reconstruction, xxii
Christianity
dialogue, 3
division of labor, 42
Fall of man, 85-86
Feuerbach on, 28-29
Marx’s youth, xxv
repentance vs. revolution, 97
sanctification, 165
Chronos, 75
dass
alienation &, 49
conflict, 47-52
definition, 58-61
inconsistency, 69
philosophy, 52
coercion, 26, 206
Cole, G. D. H., 96, 103, 133, 153
commodoties, 135, 137, 189
Communism
Bakunin on, xxxv-xxxvi
bureaucracy, xxxv
final stage, xviii
ideas or guns?, Ixviii-bxxii
Third World, xxxix
Communist League, 11
Communist Manifesto, 11-12
community of women, 82
competition, 117, 119, 124, 127, 150,
157, 189-190
computers, xii, 197
Condillac, 1t5n
confidence, 168-170
conflict, 51
conquest, 171
consumption, 210-212
cooperatives, 99-93
Cornforth, Maurice, 32-33
cost of production, 149-151
costs, 193-194

274 Marx’s Religion of Revolution

marginalism, 112
Marx, Heinrich, xx-xxi, 236-237
Marx, Karl
accidents, 78
accumulation, 124-129
age 49, xx, li
alienation, |, Ivii, Ixxi, 37-47, 49,
79, 81,99
analytic strategy, 59
anti-communist, 10, 239
anti-economics, li
anti-Semite, xlii, 70, 181
anti-Semitism, 25-16, 182
anti-theory, 96
“anti” writer, xxiii
“appearances maintained,” 251-252
Archimedian point, 35
arrogance, 14-15
association, 100
atheism, 34
atheist, xviii
Augustine &, 88
Austrians &, 141-142
autonomy, 35
Babeuf, 91
Bakunin vs., xxxv-xxxvi, 81
banking, 142
baptized, 7
beggar, 237
blueprints, xxx
Bohm-Bawerk vs., 144-154
bourgeois, xxxiii, xxix, 235-238, 241
Capital, li, liv, Ivi, Ixxi, 44-45
capital, 122-123
capital accumulation, 125-129
capitalism, 44, 46
chance, 60, 67-69
chaos cults, 2, 71, 73-76, 108, 162
Christian, 7
Christian youth, xxv
circular reasoning, 62-63
class, 69
class conflict, 47-52

classes, 58-61, 100

commodities, 117-18, 135, 137

competition, 117, 119, 124, 127,
150-151

conflict is progressive, 51

consciousness, 48

cooperatives, 92-93

creation, 33-37, 108

credit, 139-140, 141-142

criticism, xxix-xxx, so0dii, x0ciy, Iv

cycles, 89

cyclical history, 170

dead ends, xlix, Fi

debt, 142

demoncracy, 91-92

demonism &, xxvi

Demuth, Helene &, xix

designer jeans, xxxiii

dialectics, 43-46

dictatorship, 81-82

dishonest, 97

distribution, xxx, 101

divinization of production, 44

division of labor, 41-43, 46-47, 49, 82

doctoral dissertation, xx, xxxiii, li, 9

Dr. Drudge, xlvii

drudge, xlii

dualism, 62-63, 66

economic determinism, 49-52, 60;

economic fettishism, 115

economic history, 52

economies of scale, 126

empirical ?, 111-H2

enemies of, xix, xlvi, li, 14

Engets &, xliv-xlv

Engels quarrel, 250

England, 73

eschatology, 88

estrangement, 45

exchange, 114, 120

exchange value, 114

exile, 11

 

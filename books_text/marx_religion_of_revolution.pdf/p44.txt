xliv Marx’s Religion of Revolution

Moses Hess: The Forgotten Co-Founder

By writing this book, I became familiar with the major
writings of the two intellectual founders of the most important
secular religion of the modern world, Marx and Engels. By
reading Sidney Hook’s From Hegel to Marx, I also stumbled onto
the existence of the shadowy figure who converted Frederick
Engels to communism in 1842, Moses Hess (1812-1875). He
was the son of a successful Jewish businessman. As an adoles-
cent, he had wanted to join his father in the family business, but
his father insisted that the young man devote his life to the study
of traditional Judaism’s holy books, the Babylonian Talmud,
which young Moses hated. He feli into bad company, young
Jews who were rebelling against their parents’ religion. Hess lost
his faith in Judaism* about a decade prior to Engels’s loss of
faith in Christianity.2° By 1836, Hess was a communist, as
reflected in his anonymously published book, Holy History of
Mankind®! Hess’s second book, The European Triarchy (1841),
predicted that a fusion of French revolutionary socialist political
theory, German revolutionary philosophy, and English social
revolution would produce a new society.

Engels read the second book and was greatly influenced by
it? He met with Hess in late 1842. Seven months later, Hess
described this meeting with Engels: “We talked of questions of
the day. Engels, who was revolutionary to the core when he met
me, left as a passionate Communist.” Engels also met Marx
briefly at this time, but the two did not get along. For one thing,

48, Sidney Hook, From Hegel to Marz: Studies in the Intellectual Development of Karl
Marx (Ann Arbor: University of Michigan Press, [1950] 1962), ch. 6,

49, Shlomo Avineri, Moses Hess: Prophet of Communism and Zionism (New York:
New York University Press, 1985), pp. 10-11.

30, Wurmbrand, Marz and Satan, ch. 3.

51, Avineri, Moses Hass, p. 18.

52. Mareus, Engels, Manchester, and the Working Class, p. 87.

53. Cited by David McLellan, Friedrich Engels (New York: Viking, 1977), p. 21.
This statement was made the following summer: 19 June 1843: Hammen, The Red
“4818, p. 39.

54. Terrell Carver, Engels (New York: Hill and Wang, 1981}, p. 20.

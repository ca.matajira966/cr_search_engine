Ixvi Marx’s Religion of Revolution

the period and still remain confident that you probably under-
stand it, but not if you skip this one. There is nothing else even
remotely like it in terms of its breadth of scholarship and the
revolutionary nature of its thesis. Someday, professional histori-
ans specializing in nineteenth-century Europe may begin to cite
it, or even more astounding, may use it to structure their own
studies of the era. Not in the near future, however, for the book
overturns just about everything that conventional historians
have written about the roots of Marxism and radicalism.

Professional historians got the story basically wrong for well
over a century. Even a man with Billington’s academic creden-
tials has not yet been able to penetrate the historical blackout,
for if what he says is true, then the way the Western world really
has run in the past is very different from the way that profes-
sional historians have said it that it ran. They will not readily
admit this possibility, for it raises that embarrassing, crucial, and
personally dangerous question: “Then is it run similarly today?”
They prefer not to answer that one.!%

After Marx’s Religion of Revolution

Two other books deserve comment. The first is Robert Payne’s
biography, Marx (1968).!°7 It was not well received by the aca-
demic community, but it remains by far the best biography of
Marx.'®8 It was the first book in English that T am aware of to
reveal that Marx fathered a child through his wife’s lifetime
maid, Helene (Lenchen) Demuth, and then got Engels to take
the blame. Fred Demuth was rejected by his true father, whom
he met only once, and he never knew who his father was.!09

106. North, Conspiracy, ch. 6.

107. Robert Payne, Marx (New York: Simon & Schuster, 1968).

108. Fritz Raddatz’s biography is excellent in integrating Marx’s ideas and life,
but Payne’s reads much better, and { think it gives a better view of Marx as a
person,

109. Hbid., p. 265; cf. pp. 582-45. Raddatz credits the publication of extracts of
a relevant letter from Louise Kautsky-Freyberger by Walter Blumenberg in his
monograph, Kar! Marx (Rowohlt Taschenbuch Verlag, 1962): Raddatz, Karl Mars,
p. 293, note 59. Payne actually located Fred Demuth’s birth certificate and offered
extensive evidence, not just an extract from a letter,

ixx Marx’s Religion of Revolution

Socialism: Utopian and Scientific, and if Lenin had never written
State and Revolution and his many other pre-Revolution tracts,
newspapers, pamphlets, and books, would there be any Gommu-
nist-laid land mines? The world has never for a single year been
free of this or that petty dictator strutting across the pages of
some people’s history, but why are we for the first time in man’s
history facing the forced alignment of almost every society on
earth into a pair of armed military camps — nations that will
face the fallout, literally, of any full-scale nuclear war between
these two major superpowers? Why has there been the enormous
appeal of Communism compared to, say, the Mafia itself, or the
Nazis, or any of a dozen other ideological conspiracies? The fact.
that criminal secret societies exist today and have existed from
the dawn of history is not a recent discovery. The fact that an
ideologically identifiable pair of criminal initiatory societies — the
Communist parties of Red China and the Soviet Union — should
now control the lives of a billion and a half people és historically
unprecedented. In short, why do we find so many dedicated men
around the globe who carry AK-47’s, whether manufactured-in
the Soviet Union or Red China? (The answer to this question
may help us to answer a closely related one: Will anti-
Communist freedom fighters wind up the primary users of the
AK-47°s?) 15

There is more involved in the success of Marxism-Leninism
than organizational structure and mass-produced weapons. There
is the appeal of something more than participation in an effective
international terrorist organization or regional tyranny that moti-
vates men to sacrifice everything they are and own for the sake
of Marxism-Leninism. In the typically muddled prose of the
professional sociologist, Henri Lefebvre writes concerning Marx’s
philosophy: “The ‘truth of religion’ ~ what religion really is ~ is
discovered in philosophy. This means that philosophy contrib-
utes a radical criticism of religion, that it lays bare the essence

115, “Pakistani Arms Dealers Hail God and the AK-47,” New York Times (March
8, 1988), The Soviet model sells for $1,400; the Soviet-licensed Red Chinese version
for $1,150. (Gold is at $430/o2.)

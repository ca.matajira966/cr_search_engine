24 Marx’s Religion of Revolution

last attempted on a grand scale by the Prussian-employed Swabian
philosopher G. W, F, Hegel. He created one of the most complex
and total philosophical systems ever constructed.’ In the same
period, the science-ideal was reasserted by the new group known
as the positivists, especially St. Simon and Comte. Herbert
Marcuse has summarized the viewpoint of the positivist move-
ment: “The idealistic idea of reason, we recall, had been intrinsi-
cally connected with the idea of freedom and had opposed any
notion of a natural necessity ruling over society, Positive philoso-
phy tended instead to equate the study of society with the study
of nature, so that natural science, particularly biology, became
the archetype of social theory. Social study was to be a science
seeking social laws, the validity of which was to be analogous to
that.of physical laws. Social practice, especially in the matter of
changing the social system, was herewith throttled by the inexo-
rable. Society was viewed as governed by rational laws that
moved with a natural necessity. This position directly contra-
dicted the view held by the dialectical social theory, that society
is irrational precisely in that it is governed by natural laws. . . .
‘The positivist repudiation of metaphysics was thus coupled with
a repudiation of man’s claim to alter and reorganize his social
institutions in accordance with his rational wiil.”® It is in Marx’s
work that we find the next great attempt to unify the various
strands of thought, and it is this attempted synthesis that estab-
Jishes Marx as a major figure in 19th-century intellectual history.

Before looking at Marx’s system, however, it is necessary to
consider briefly Hegel’s contribution. He tried to unify the Kant-
ian dualism of human freedom and mechanistic science into an
overall philosophy of history. Flux was inserted into the law
sphere, while historical factuality became infused with. philo-

7. A standard introduction to Hegel’s thought is W. T. Stace, The Philosophy of
Hegel (New York: Dover, [1923] 1958).

8, Herbert Marcuse, Reason and Revolution: Hegel and the Rise of Social Theory
(Boston: Beacon Press, {1954] 1960), pp. 343-44. For an important introduction te
the positivists, see F. A. Hayck, The Counter-Revolution of Science (Glencoe, Illinois:
Free Press, [1952] 1955). [Reprinted by Liberty Press, Indianapolis, Indiana, in
1979,]

Pessimillennialism 87

of Revelation becomes an enigma: “The Apocalypse intends to
show you, dear believer, that things are not what they seem!” If
this seems confusing, it is because amillennialism is confusing.
Either Christians will reign on earth and in history (i.e., pre-
Second Coming) or they will not. If neither we nor our cove-
nantal successors will ever be able in history to apply the Bible-
specified sanctions of the heavenly King whom we represent on
earth, then Christians cannot be said ever to reign in history.
The language of “reigning” would then be both misleading and
inappropriate. The issue here is simple: Christians’ possession of
the judicial authority to impose negative civil sanctions or the private
economic power to impose both positive and negative cultural sanctions.
Amillennialists categorically deny that Christians will ever exer-
cise such widespread authority or influence. Thus, amillennial-
ists have yet to explain this eschatologically crucial biblical text
from Revelation. It speaks of Christians who reign on earth.

Ulopianism Without Earthly Hope

It does even less good to encourage the optimism of your
readers by proclaiming, as Raymond Zorn does, that “to the
extent that the world is christianized by the Church’s efforts, it
exhibits to that degree at Jeast, the all-inclusive power of Christ
by which His victory since Calvary is brought to actualiza-
tion,”” if you do not really believe that the Church ever can
achieve the Christianization of the world in history. Yet this is
standard theological fare among amillennialists. A few pages
after the ingeniously qualified phrase “to the extent that” was
added hy the author to a sentence displaying considerable ver-
bal optimism, he added: “. . . Jesus came to found a kingdom
that was not of this world.” (The implicit move toward an
inner, “higher,” “victorious” mysticism here should be obvious,
although it has never been obvious to Calvinistic amillennialists,
unlike medieval Catholic amillennialists.) Zorn then tells us

52. Raymond 0. Zorn, Church and Kingdom (Philadelphia: Presbyterian & Reformed,
1962), p. 146.
53. Tbid., p. 180.

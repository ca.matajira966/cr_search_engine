90 MILLENNIALISM AND SOCIAL THEORY

created by Christ's reign are completely a part of this dispensa-
tion, while the light of his reign will remain dim to the end.”**
Christ, the shadow in history! Then what does this make Satan?
Berkhof and Hoekema are far too astute to raise this thorny but
obvious question. Hockema then goes on to defend a funda-
mental assertion of amillennialism: the ambiguity of history.

Here again we see the ambiguity of history. History does not
reveal a simple triumph of good over evil, nor a total victory of evil
over good. Evil and good continue to exist side by side. Conflict
between the two continues during the present age, but since Christ
has won the victory, the ultimate outcome of the conflict is never in
doubt. The enemy is fighting a losing battle.”

Notice the ambiguity of his phrasing of the issue. The good
does not “simply” triumph over evil. Why simply? Evil does not
“totally” triumph over good. Why totally? This is not clear.
There is some kind of hidden eschatological agenda underlying
such peculiar phrasing. What is very clear, however, is that his
final sentence is misleading. The enemy, given the amillennial
outlook, is indecd fighting a lost cause, but he is surely not fight-
ing a losing battle. He is fighting at the very cast a long-term
historical stalemate, and probably an overall cultural victory.
Under the most favorable possible interpretation, Hoekema is
promoting a version of the stalemate religion.” This is the
religion of cultural cease-fires rather than Christian victory.

Hoekema understands what this means for history: the ab-
sence of meaningful progress. “Can we say that history reveals
genuine progress? Again we face the problem of the ambiguity
of history. For every advance, it would seem, there is a corres-
ponding retreat. The invention of the automobile has brought
with it air pollution and a frightful increase in highway acci-

59. Anthony A Hoekema, The Bible and the Future (Grand Rapids, Michigan: Eerd-
mans, 1979), p. 35.

60. Hem.

G1. Gary North, Backward, Christian Soldiers? An Action Manual for Christian Reconstruc-
tion (Tyler, Texas: Institute for Christian Economics, 1984), ch. 11: “The Stalemate
Mentality.”

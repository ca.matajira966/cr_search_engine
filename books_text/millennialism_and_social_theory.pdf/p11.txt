Preface xi

and will be done by God to overcome this seemingly unsolvable
problem: getting the gospel to these people in time. And by the
words “in time,” I mean in history. 1 mean in my generation.

A Question of Time

It is common in evangelical circles to say that “this world is
running out of time.” This another way of saying, “The lost are
running out of time.” Yes, time is indeed running out, just as
fossil fuels arc running out, but when? In ten years? A thousand
years? Ten thousand years? It makes a big difference. The ques-
tion that we need to get answered is this: “Is time running out
for the people alive today?” And the answer is categorically yes.
The average life span for people living in industrial nations is
about 75 years. If a child gets by his first five years, this figure
goes above 80 years. In underdeveloped nations, the life span is
less, especially for newborn children. So, I can confidently say
that time is running out for these people, and my answer docs
not depend on any theory regarding the timing of Jesus’ Second
Coming.

The Church of Jesus Christ now faces a major problem. It is
the same old problem that it has always faced, but today the
stakes are far higher because the number of souls on the line is
so much larger. These people are spiritually dead. If they do
not respond favorably in history to the gospel of Jesus Christ,
they will remain spiritually dead for all eternity. They will not
die spiritually; they are already dead spiritually. They enter
history with God's declaration of “Guilty as charged!” against
them. This is their legacy from Adam, their spiritual birthwrong.

I contend in this book that the concern of most evangelical
Christians is misplaced today. For over a century, their primary
theological concern has been the dating of the Second Coming
of Christ. Speculation regarding this event, and Christians’
appropriate response to it, has governed both the worldview
and actual strategies of the majority of those denominations
(mostly headquartered in the United States) that call themselves
evangelical. In short, various theories of the Second Coming of
Christ, especially its dating (supposedly imminent), have over-
shadowed the uncontestable fact of world population growth and

116 MILLENNIALISM AND SOCIAL THEORY

the Christians who live and labor prior to Jesus’ physical Second
Coming, the so-called Church Age. All their good works will be
swallowed up during the great tribulation period, either imme-
diatcly before Jesus returns — the post-tribulation position — or
in the seven-year period which follows the so-called ‘secret Rap-
ture’: pre-tribulationism.) Postmillennialism is an inescapable concept.
It is never a question of cultural triumph vs. no cultural tri-
umph prior to Jesus’ Second Coming; it is a question of which
kingdom’s cultural tiumph.”* The amillennialist has identified
the victorious kingdom in history: Satan’s. What, then, is the
rational response of the Christian, if this amillennial vision is
correct? What is to be donc?

Defeatism: Active or Passive?

The optimistic vision of covenantal victory that is found in the
Book of Isaiah is not taken seriously by common grace amillen-
nialists. They see a continuous expansion of Satan’s kingdom
(ie., civilization) in history. Amillennialism’s view of history is
clear: “Things are going to get a lot worse before they get
worse.” Van Riessen is consistent in this respect: he offers Chris-
tians no earthly hope for positive cultural transformation.

On the one hand, Dutch common grace amillennialists insist
that there are uniquely Christian ways to explain the world and
even to suggest to the lost as biblical alternatives. On the other
hand, because they deny the continuing validity of Old Testa-
ment law, they never get around to describing precisely what
these specific reforms are, or how these reforms are uniquely
biblical, ie., how the Bible compels us morally to accept them.

The implications of this outlook for the construction of a
Christian social theory are devastating. The common grace
amillennialists deny God’s covenant-guaranteed sanctions in
history. Worse; they offer a perverse view of these sanctions:
God rewards covenant-breakers and penalizes covenant-keepers.
There is no neutrality in history, Historical sanctions are an
inescapable concept. The question is: Who imposes them, Christ

34. North, Political Polytheism, p. 139.

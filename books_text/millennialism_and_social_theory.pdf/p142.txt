126 MILLENNIALISM AND SOCIAL THEORY

pluralism)" or else (2) Christ’s Second Coming.

Amilliennial theologians would do well to follow the example
of the king of Nineveh. He believed in the ethical conditionality
of God’s prophecies, and responded accordingly. It can happen
again in history. God is sovereign, not covenant-breaking man.

50. North, Political Polytheism,

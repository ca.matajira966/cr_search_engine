Time Enough 133

apart from God’s widespread negative corporate sanctions, there
have been few (if any) known cases of permanent Christian
social transformation.

Christianity’s positive sanctions in New Covenant history tend
to be continuous rather than discontinuous. Even in the rare
cascs of mass revival — discontinuous positive moves by the Holy
Spirit — these events have virtually never been followed by wide-
spread cultural transformation (i.e., cultural continuity). God’s
enemies have inherited.’ The reasons why: mass revivals have
not been accompanied by (1) God’s comprehensive negative
sanctions or by (2) a comprehensive reform of civil law, (Note:
Iam speaking here of history, not prophesying.)

The Protestant Reformation is an example of institutional
transformation, but one reason why it succeeded was that the
Turks were almost at the gates of Vienna; the Pope and the
Emperor had other pressing concerns besides Luther and Cal-
vin. Also, it led to the Thirty Years War (1618-48), and the
resolution of that war was the establishment of Erastianism: the
king’s religion became the religion of the people. This was
hardly a long-term cultural solution. The humanists inherited.

Covenantal Displacement

The process of covenantal displacement is a war over cultural
and judicial standards. It is a war over law. It is therefore a war
to determine the god of the culture, for the source of Iaw in any
culture is its god. The enemies of God very seldom surrender
peacefully. They correctly perceive that they are fighting to the
death covenantally, both personally and institutionally. This is
what the Bible teaches: either the old Adam dies spiritually
through the new birth in history or else God will publicly exe-
cute him eternally on judgment day. Covenant-breakers clearly
perceive the life-and-death nature of the struggle for civilization;
covenant-keepers seldom do. Christians prefer religious and

Vindiciae Contra Fyrannos (1579), the Defense of Liberty Against Trans.

5. See Chapter 11.

6. R, J. Rushdoony, The Institutes of Biblical Law (Nutlcy, New Jersey: Craig Press,
1978), p. 5.

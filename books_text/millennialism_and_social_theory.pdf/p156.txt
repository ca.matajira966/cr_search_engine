140 MILLENNIALISM AND SOCIAL THEORY

the commandments, and the statutes, and the judgments, which I
command thee this day, to do them (Deut. 7:7-11). (emphasis added)

At this point, dispensational commentators will affirm their
occasional commitment to symbolism in biblical interpretation:
waiting another 56,600 years for the Rapture (60,000 minus
3,400 = 56,600) is a bit too much for them to handle.

What is my point? Simple: the Bible sometimes uses symbolic
language as a rhetorical means of driving home an important
theological point. So can we. Bul we must do this fairly and
honestly. This ethical requirement is not always honored.

Reconstructionist Rhetoric

I come now to a consideration of a recent debate over biblical
interpretation. That the Bible does use symbolic language in
order to emphasize important truths was a point made by David
Chilton — and made rhetorically — when he spoke of the sup-
posed 36,600-year millennial era: not a literal 36,600 years but
rather the Bible’s use of symbolic language to describe God’s
long-sullering patience with rebellious mankind and His bless-
ings to covenant-keepers. Chilton was quite clear: “Similarly, the
thousand years of Revelation 20 represent a vast, undefined
period of time. . . .” He cited Milton Terry, the respected com-
mentator and master of biblical hermeneutics: “It may require
a million years.”"? This means simply that the designation of
one thousand years must not be taken exclusively in a literal
sense. In Paradise Restored, citing Deuteronomy 7:9, Chilton
writes of a long era of millennial blessings:

The God of the Covenant told His people that he would bless them to
the thousandth generation of their descendants. That promise was
made (in round figures) about 3,400 ycars ago. If we figure the Bibli-
cal generation at about 40 years, a thousand generations is forty thou-
sand years. We've got 36,600 years to go before this promise is
fulfilled!

12, Chilton, Days of Vengeance, p. 507.
18. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth, Texas:
Dominion Press, 1985), p. 221.

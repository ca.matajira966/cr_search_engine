Denying God’s Predictable Sanctions in History 157

What readers may not immediately recognize is that such an
argument is a cover for a very different ethical conclusion,
namely, that historical sanctions should therefore be imposed in terms
of some rival system of social theory. There must always be sanctions
in society, imposed by the State, the family, the market, and nu-
merous other associations. The five covenantal questions are: (1)
Who establishes these sanctions? (2) What agent or agency
enforces them? (3) What is the mora! foundation of these sanc-
tions? (4) What sanctions apply to which acts? (5) Does the soci-
ety prosper and expand its influence when these sanctions are
enforced? To say that the Bible does not provide this covenant
order in the New Testament era is to say that some other covenant
is legitimate for society. But the opponents of biblical covenant
social order never dare to admit this. They hide their implicit
call for the establishment of some other covenantal standard in the
language of ethical neutrality or judicial randomness. But there
1s no cthical ncutrality. So, arc God’s sanctions in history really
random, covenantally speaking?

In order to get a proper perspective on this question, let us
consider the teachings of two of the most significant theologians
(and culture-transformers) in history: Augustine and Calvin.

Augustine

Augustine’s City of God discusses God's historical sanctions
against individuals exactly where such a discussion should ap-
pear, in Chapter XX, on the last judgment. He therefore relates
sanctions to general eschatology. He begins with a summary of the
traditional creed, “that Christ shall come from heaven to judge
quick and dead. . . .”° He asserts that men and devils are pun-
ished in this life and the next. He then limits himself to a dis-
cussion of the final judgment, “because in it there shall be no
room for the ignorant questioning why this wicked person is
happy and that righteous man unhappy.”*

What ignorant questioning docs he have in mind? He begins
with a presupposition regarding individuals in history: “For we

3. Augustine, The City of Gad, XX:1 (Modern Library), p. 710.
4, Bid. XX, p. 711.

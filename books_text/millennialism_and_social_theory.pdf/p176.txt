160 MILLENNIALISM AND SOCIAL THEORY

up their evil, so that He can punish them even more. This
procedure is true in both the Old Covenant and the New Cove-
nant. Paul favorably cites Proverbs 25:22: “Therefore if thine
enemy hunger, feed him; if he thirst, give him drink: for in so
doing thou shalt heap coals of fire on his head” (Rom 12:20).
God places covenant-breakers on slippery places. Their prosper-
ity in history is temporary. He who fails to understand this, the
Psalmist says, is foolish, ignorant, and like a beast. (Rhetoric!)

The emphasis of the Old Testament is the transition from
wrath to grace in history. This is not to say that it completely
ignores God's negative sanctions on the day of judgment, but
surely this is not emphasized. Thus, to interpret Psalm 73 as if
it were a psalm about the inscrutable prosperity of the wicked
throughout history, with the emphasis on God's post-historical
judgment, misses the point. ‘he psalmist is describing history.
On average, covenant-breakers are eventually brought under
God's negative sanctions in history. But we must not expect to
see instant sanctions. God is sometimes merciful to sinners, not
giving them what they descrve. He sometimes also allows them
extra time to fill their historical cup of wrath to the brim, just as
He did with the Amorites (Gen. 15:16).

Is Augustine’s view the biblical view of God's historical sanc-
tions? What of the supposed inscrutability of God’s historical
sanctions? Doesn't the Bible affirm this inscrutability? With re-
spect to any given individual, sometimes. With respect to cove-
nanted corporate groups, no. What Augustine failed to consider
in this section on God’s final judgment is what his entire book
is about: God’s negative historical sanctions against the city of Rome
because of Rome’s paganism and moral debauchery. It should be clear
why he focused on individuals in Chapter XX: he was explicitly
dealing with God's final judgment against individuals, not God’s
historic judgments against corporate entitics.

With respect to covenantal history, Augustine’s City of God
has served as the most important document in Church history.
He makes it clear that God brings negative historical sanctions
against all imitations of Christ’s kingdom in history. They can-
not survive in their rebellion against God. The city of God is
not brought under comparable negative judgments in history.

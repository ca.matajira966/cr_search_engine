Denying God’s Predictable Sanctions in History 181

years ago; only an expurgated version remains. In any case, it
is doubtful that many scholars ever relied heavily on the eight-
volume original. We find very few references to it in the books
written by other dispensational authors. Dispensationalism has
relied on books written in the early 1950's to defend the posi-
tion, such as Pentecost’s Things to Come. This is indicative of a
stagnant intellectual system. Dispensationalism is in transition.
It is unlikely that whatever emerges will resemble Scofield’s
original system, with its sharp discontinuity between the Old
and New Covenants and its total rejection of biblical law.
Furthermore, even these older works systematically refrained
from applying any of their theological principles to real-world
problems. This is a theological tradition stretching back to the
end of the seventeenth century. Both Protestant and Roman
Catholic casuistry died as a field of Christian ethics after 1700.
Thus, those who oppose Christian Reconstruction do not pos-
sess a developed body of materials to offer as an alternative.
‘They are in the position of fighting something with nothing.
Yet they give the illusion of possessing a published heritage
behind them that serves as the foundation of a comprehensive
challenge to both theonomy and secular humanism. The leaders
all know they are holding an empty book bag, but they never
admit this to their followers. But word is getting out among
those who read: there is no published, Protestant alternative to the
comprehensive worldview of Christian Reconstruction. This is why our
critics keep losing their brightest students to our camp. The
pessimillennialists cannot beat something with nothing.

Conclusion

What prompted Muether’s article? The theological errors of
certain “utopians,” he says — unidentified Christian “utopians”
who promote “a reconstructed republic patterned after the civil
law of Old Testament Israel. . . ."* (Can you guess who these
authors might be?) These error-promoting people recommend
that “Christians pick up the sword to achieve a political goal.”

41. Muether, p. 14.

Historical Sanctions: An Inescapable Concept 189

To be taken without consent from my home and friends; to lose
iny liberty; to undergo all those assaults on my personality which
modern psychoytherapy knows how to deliver; to be re-made after
some pattern of “normality” hatched in a Viennese laboratory to
which I never professed allegiance; to know that this process will
never end antil either my captors have succeeded or I grown wise
enough to cheat them with apparent success — who cares whether
this is called Punishment or not? That it includes most of the ele-
ments for which any punishment is feared — shame, exile, bondage,
and years eaten by the locust — is obvious. Only enormous ill-desert
could justify it; but ill-desert is the very conception which the Hu-
manitarian theory has thrown overboard.”

In his novel, That Hideous Strength, Lewis has a weak-willed
sociologist write a justification for the imposition of corrupt and
total police rule in a local community. The ruling organization
will come, wrote the young sociologist, “in the gracious role of
a rescuer — a rescuer who can remove the criminal from the
harsh sphere of punishment to that of remedial treatment.”*
It is just such a concept of State tyranny that would result
from the judicial prescriptions of Calvinist philosopher Robert
Knudsen. He denies in the New Testament era any legitimacy
of the formal requirements of the Old Covenant legal order. He
insists: “In all their relationships New Testament believers do
not have less responsibility than their Old Testament counter-
parts for obeying God's will as expresscd in his law; in fact, they
have greater responsibility, because it is not legally stipulated
exactly what they should and should not do.”* Our responsibili-
‘ties to each other are open-ended, Yet in any covenant, there
are legal aspects, as Knudson admits.’° These laws restrain the
legitimate demands made by one person on another. It is sig-

7. ©.S. Lewis, “The Humanitarian Theory of Punishment,” in Lewis, God in the Dock:
Essays on Theology and Ethics, edited by Walter Hooper (Grand Rapids, Michigan: Eerd-
mans, 1972), pp. 290-91,

8. C.S, Lewis, That Hideous Strength: A Modem Fairy-Tale for Grown-Ups (New York:
Macmillan, 1946), pp. 132-33.

9. Robert D. Knudsen, “May We Use the Term ‘Theonomy’ for Our Application of
Biblical Law?” in William 8. Barker and W. Robert Godfrey (eds.), Theonomy: A Reformed
Critique (Grand Rapids, Michigan: Zondervan Academic, 1990), p. 34.

10. Ibid, pp. 32-33,

 

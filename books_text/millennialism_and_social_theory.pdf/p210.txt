194 MILLENNIALISM AND SOCIAL THEORY

of history, which was its explicit topic.

The reader may be thinking to himself: “But how did Alva
McClain get away with this? Why did the editor publish such a
piece? How could anyone be fooled this badly?” The answer is
simple: because they wanted to be fooled. They stilt do. They want
to escape personal responsibility for the cultural success of the
gospel of salvation in history. Anything that seems to further
this end they are willing to accept uncritically. We are not wit-
nessing an intellectual failure only; we are witnessing a moral
failure. It has been going on for well over a century. McClain’s
essay was only one small example of a way of life and a way of
thinking about the gospel in history. The good news of Jesus
Christ for a comparative handful of individuals thus far in
Church history is interpreted as bad news for the Church’s
efforts in the “Church Age” as a whole. There can be no fulfill-
ment of the Great Commission in history. This is trend-tending
with a vengeance, but not God’s vengeance.

A Missed Opportunity

Consider when that article appeared, It was at the peak of
the post-World War ¥ period of American supremacy. Eisen-
hower was President. Khrushchev had only barely consolidated
his power in the Sovict Union. His famous 1956 “secret speech”
on Stalin’s “cult of personality” had shaken the American Com-
munist Party to the core, with many resignations as a result. In
America, rock and roll was still in its Fats Domino-Bill Haley-
Buddy Holly-early Elvis Presley phase. There was no conserva-
tive movement. William F. Buckley’s National Review magazine
was only a year old. No one outside of Arizona had heard of
Barry Goldwater, whose run for the Presidency came in 1964,

Religiously, Dallas Seminary and Grace Seminary possessed
something of a monopoly in fundamentalism. The neo-evangeli-
cal movement was less than a decade old. Billy Graham helped
to start Christianity Today in 1956, but it had not yet begun its
visible drift to its present middle-to-left position, Graham had
not yet begun his cooperation at his crusades with locat congre-
gations that belong to the National Council of Churches. He
was a strong anti-Communist in 1956, and the USSR had not

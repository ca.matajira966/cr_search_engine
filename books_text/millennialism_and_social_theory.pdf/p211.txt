Historical Sanctions: An Inescapable Concept 195

yet started inviting him to give crusades that were followed by
press conferences telling the world how there was no religious
persecution in the Soviet Union. (Solzhenitsyn, was politely
scathing in 1983 in his denunciation of Rev. Graham’s services
to the Soviet Union’s tyrants in this regard.)’® There was no
six-day creationist movement to speak of; Morris and Whit-
comb’s Genesis Flood was five years away, (Grace Seminary offici-
ally affirms the six-day creation; Dallas Seminary never has.)
Fuller Seminary was a small, struggling school that had not yet
begun its drift into liberalism, ecumenism, and influence.
Over the next fifteen years, Dallas Seminary, Grace Semi-
nary, and the newly created Talbot Seminary in California sat
immobile and culturally silent while the rest of the country
went through enormous changes. The conservative political
movement got rolling visibly in 1960. Liberalism consolidated
itself in the Kennedy and early Johnson years, but it was blown
apart ethically during the Vietnam War era. Kennedy was
assassinated in November of 1963, and the myth of Camelot
was retroactively created. The Beatles appeared on the Ed Sul-
livan Show the next February. Then all hell broke loose.
Black ghettos rioted, beginning in Harlem in the summer of
1964. Watts went up in flames in 1965. The Rolling Stones had
their first big hit in 1966. ‘he youth movement went crazy,
1965-70. It seemed that everyone under age 25 was asking the
old social order to defend itself morally (it couldn't), and mil-
lions of people were asking many fundamental questions about
life and society.” Throughout all of this, nothing was heard

19, In his 1983 speech to the Templeton Foundation, which had granted him its
enormous award (close to $200,000) for a lifetime of religious service, Solchenitsyn re-
marked toward the end of his presentation on the horrors of Communism: It if with
profound regret that I must note here something which I cannot pass over in silence. My
predecessor in receipt of this prize last year — in the very months that the award was
made — lent public support to communist lies by his deplorable statement that he had not
noticed the persecution of religion in the USSR. Before the multitude of those who have
perished and who are oppressed today, may God be his judge.” His predecessor was Billy
Graham. Solzhenitsyn, The Pompleton Prize, 1983 (Grand Cayman Islands: Lismore Press,
1983), na page numbers.

20. Gary North, Unholy Spirits: Occultism and New Age Humanism (Ft. Worth, Texas:
Dominion Press, £986), pp. 4-11.

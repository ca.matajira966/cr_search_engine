208 MILLENNIALISM AND SOGIAL THEORY

a high price: the operational abandonment of the dispensational
system. This will eventually lead to a quiet, unnanounced, but
nonetheless effective theological abandonment of the system.

The Impossible Dream

‘The pessimillennialists want Christianity to be relevant in
history, yet they have publicly denied the theological founda-
tions of historical relevance: (1) the continuing New Covenant
relevance of God's Old Covenant social and civil laws; (2) God’s
historical sanctions applied in terms of these laws; and (3) his-
torical continuity between the present and the prophesied era
of millennial blessings that will take place on earth and in history.
How can they sensibly expect their followers to take seriously
their assertion of Christianity’s historical relevance, let alone the
historical relevance of their own efforts? C. S. Lewis described
about a similar problem in his 1947 essay, “Men Without
Chests”:

In a sort of ghastly simplicity we remove the organ and demand the
function. We make men without chests and expect of them virtue
and enterprise. We laugh at honour and are shocked to find traitors
in our midst. We castrate and bid the geldings be fruitful”

Conclusion

Social theory requires a unified, authoritative concept of
good and bad, right and wrong, efficient and inefficient. To be
consistent, it must affirm the cxistence of known or at least
knowable standards, and it must also affirm that there is a
sanctioning process that rewards the good (or the efficient) and
penalizes the bad (or the inefficient ). If the standards are
affirmed without also afirming appropriate sanctions, then
there is no way for society to insure justice. There is also no
way for it to insure progress.

Modern Christian theology has denied both biblical law (the
standards) and God's historical sanctions. It has therefore
sought the standards of socicty elsewhere. Occasionally, Chris-

39. C. 8. Lewis, The Abolition of Man (New York: Macmillan, 1947), p. 35.

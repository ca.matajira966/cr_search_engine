The Sociology of Suffering 227

while, and the wicked shall not be: yea, thou shalt diligently consid-
er his place, and it shall not be. But the meek shall inherit the earth; and
shall delight themselves in the abundance of peace, The wicked plotteth
against the just, and gnasheth upon him with his teeth. The Lord
shall laugh at him: for he seeth that his day is coming. The wicked
have drawn out the sword, and have bent their bow, to cast down
the poor and needy, and to slay such as be of upright conversation.
Their sword shall enter into their own heart, and their bows shal?
be broken. A little that a righteous man hath is better than the
riches of many wicked. For the arms of the wicked shall be broken:
but the Lorp upholdeth the righteous. The Lorp knoweth the days
of the upright: and their inheritance shall be for ever, They shall
not be ashamed in the cvil time: and in the days of famine they
shall be satisfied. But the wicked shall perish, and the enemies of
the Lorn shall be as the fat of lambs: they shall consume; into
smoke shall they consume away. The wicked borroweth, and payeth
not again: but the righteous sheweth mercy, and giveth. For such as
be blessed of him shail inherit the earth; and they that be cursed of
him shall be cut off (Psa. 37:1-22). (emphasis added)

This is why the amillennialist has his heart turned against
the Old Testament. It allows us to understand the covenantal
foundation of Christ’s teachings, such as this one: “Blessed are
the meek: for they shall inherit the earth” (Matt. 5:5). Those
who are meek before God and therefore active toward His creation
shall exercise dominion in history, if they obey His laws. God’s
promise of victory to His Church is tied to His covenant. This
cannot be understood apart from His covenantal sanctions in
history, both positive and negative.

Resurrection, Then Crucifixion

Gaffin insists that the Bible’s “eschatology of victory is an
eschatology of suffering. . . .” Then he adds what he regards as
his coup d’grace: “Until Jesus comes again, the church ‘wins’ by
‘losing.’ "He then asks a rhetorical question: “What has hap-
pened to this theology of the cross in .much of contemporary
postmillennialism?”” I shali provide him with the answer: #
has been modified by the theology of resurrection and ascension.

17, Mid., p. 216.

250 MILLENNIALISM AND SOCIAL THEORY

the prophesied events take place. Their goal is ethical. Their
goal is to deflect Christians from the Great Commission, as defi-
ned by the Bible rather than by the gospel tract association.
They wish to substitute a very narrow plan of salvation for the
Bible’s comprehensive plan. Pessimillennialism is basic to this
strategy of deflection.

Narrowing One’s Ethical Vision

Marvin Rosenthal has written a book that defends what
seems to be a mid-tribulational view of the Rapture, although
he wants to avoid the term “tribulation.” In the Introduction to
his book, he makes this statement: “The importance of under-
standing still unfulfilled prophecy for contemporary Christian
living cannot be overstated.”* He then spends three hundred
pages (no subject index) describing his new interpretation of the
seventieth week of Daniel, etc. But there is nothing in the book
that tells us what difference, specifically, any of this makes for
contemporary Christian living. He never bothers to discuss
contemporary Christian living. (Prophecy books never do.)
Chapter 20 is titled, “The Prewrath Rapture: Catalyst for Holy
Living.” Yet there is not one word on any specifically ethical
application of his thesis. He tells us only to be godly, for Jesus
is coming soon. The chapter is basically a plea to the reader to
accept the author’s thesis. He offers us his thesis of the seven
churches of the Book of Revelation: more discussion of the
timing the Rapture. That is all. End of chapter. End of book.

He tells us — as four or more generations of dispensational
preachers have assured us — that “At the present moment of
history, the planet Earth is in grave crises. This celestial ball is
on a collision course with its Creator. Man has pushed the self-
destruct button.”* What is the Church to do? He does not say.
But he assures us, once again, that “Jesus is coming again. That
is the blessed (living) hope. At his coming (perousia), He will
raise the dead and rapture the righteous. Then He will pour

3. Marvin Rosenthal, The Pre-Wrath Rapture of the Church (Nashville, Tennessee:
Nelson, 1990), p. xi-
4. Ibid., p, 295.

Will God Disinherit Christ’s Church? 255

The whole dominion/reconstruction movement is too wedded to
an ongoing earthly process stretching into the indeterminate future
to be truly faithful to the totality of what Scripture says about being
sufficiently disengaged from this world to leave it at a moment’s
notice.”

As is true of premillennialisis generally, Hunt does under-
stand the significance of a Christian's personal unwillingness to
disengage from this world before the time that God calls his soul
home to heaven because his work is completed (Phil. 1:20-24). Hunt
understands that we are supposed to want to finish our tasks.
So, in order to make it easy for us to end all such concerns, he
proclaims the doctrine of the Rapture and the subsequent Great
Tribulation: events that will bury all of the Church's work in
history. He calls this an end to history. But, unlike the amil-
lennial view of Christ's Second Coming, the premillennialist
says that Christ has at least a thousand years of work ahead of
Him. But this work will be utterly discontinuous from anything
the Church achieves in history. This view is wrong.

Our work does have great significance in history. We are
building up God’s kingdom in history, an idea that Hunt denics
in his recent books. He recognizes that we Reconstructionists
have a world-transforming vision. He wants to cut that vision
short. “Look up!” he shouts. This means, “Stop looking forward
to society here on earth through future generations.”

Comic Book Theology

Like those plantation slaves in the American South back in
1850, we Christians are told to look up for our deliverance.
This means that in the meantime, we should say to the human-
ists who dominate society, “Yes, massa. We be good, massa. We
jus’ keep lookin’ up, massa. We don’ cause you no problems,
massa. Jus’ don’ use that whip, massa!”

And deep down inside, we may think, “Someday, Jesus
gonna whip yo’ --! And maybe I will, too.” Hunt does:

10. Hunt, p. 15.

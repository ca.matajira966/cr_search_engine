14 MILLENNIALISM AND SOCIAL THEORY

does not believe that this loss of faith will continue, assuming
that Western Civilization survives. But if it does persist, Western
civilization as we know it today will not survive.

T think he is incorrect about the Greeks’ commitment to the
idea of historical progress, as I have explained elsewhere.* He
is eminently correct with respect to the post-Reformation West.
The fundamental ideas undergirding a doctrine of historical
progress are these: (1) a sovereign predestinating agent who (or
impersonal force that) guarantees the linearity of history (anti-
cyclicalism); (2) cultural and social authority based on a repres-
entative’s publicly acknowledged legitimacy, which in turn is
derived from his (or their) access to (3) the wisdom revealed by
the sovereign agent or force, meaning detailed knowledge of
permanent standards of evaluation (“Progress compared to
what?”), (4) culture-wide cause-and-effect relationships (chal-
lenge and successful response); and (5) compound growth over
long periods of time (technical knowledge, tools, and the divi-
sion of labor). If any one of these five premises is abandoned,
the entire system collapses theoretically, and will therefore
eventually collapse historically.* Today, all except linear history
are being called into question. In short, point one — historical
linearity — does not necessarily imply historical progress. There
can be linearity downward into the void. Modern physical sci-
ence, for example, points directly to such a decline.

3. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler, Texas:
Institute for Christian Economics, 1985), ch. 17. In many of his works, the Benedictine
historian of science Stanley Jaki [YAHkee] has made the case far more persuasively than
T have. Jaki has focused on the importance of the idea of linear history for the develop-
ment of science, though not the idea of progress as such. See especially Jaki, Science and
Creation: From eternal cycles to an oscillating universe (London: Scottish Academic Press,
1974),

4. This assertion of the unity of theory and practice is itself a fundamental aspect of
‘Western social theory.

5. Ifscientists announce the discovery of physical evidence of a coming “big collapse”
of the cosmos that will someday complement the “big bang” of creation, producing yet
another “big bang,” cyclicalism will receive a scientific shot in the arm. At present, few
scientists believe that there is sufficient matter in the universe to produce a “big contrac-
tion,” The lineatity of history is therefore still presumed scientifically: the heat death of
the universe (entropy’s effect). This does not produce optimism; on the contrary, it
affirms an ultimate cosmic pessimism. Gary North, Is the World Running Down? Crisis in the
Christian Worldview (Tyler, Texas: Institute for Christian Economics, 1988), ch. 2.

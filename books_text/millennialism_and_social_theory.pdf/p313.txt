Our Blessed Earthly Hope in History 297

Notice the placement of the harvest. Covenant-breakers and
covenant-keepers will work in society together until the final
harvest. When will this be? At the end of the world. There is no
possibility that there can ever be a Rapture that is separate
from the final judgment.

The prophesied disinheritance of covenant-breakers there-
fore must take place in history. Why? Because covenant-keepers
must inherit the earth from those who possess it today. “What
man is he that feareth the Lorp? him shall he teach in the way
that he shall choose. His soul shall dwell at ease; and his seed
shall inherit the earth” (Psa. 25:12-13). But this inheritance
cannot be relegated exclusively to the post-judgment world, as
amillennialists would insist that it must be. What value would
such an cursed inheritance be in that perfect world? Some value
(there is continuity between this world and the next), but not
very much. The main continuity is ethical — lessons learned in
history ~ not economic. So, title must transfer before the eternal
disinheritance of the lost.

The problem today is that the Church is totally unprepared
to inherit, let alone administer this inheritance. Christians have
becn taught that covenant-breakers lawfully disinherited cove-
nant-keepers from the day of Adam’s fall, and Jesus’ death,
resurrection, and ascension of have not altered this covenantal
disinheritance. Neither has the arrival of the Holy Spirit at
Pentecost. The covenantal message of Noah’s Flood — a vast
disinheritance of covenant-breakers and an opportunity for
comprehensive reconstruction by covenant-keepers — does not
register in their thinking. Neither do Christ’s words: “All power
is given unto me in heaven and in earth” (Matt. 28:18).

We should pray for the conversion of covenant-breakers.
This is the most glorious means of their disinheritance. The old
man in Adam will die. This will enable God’s earthly kingdom
to inherit the whole of their wealth when they repent, including
their personal skills, which are far more valuable than their
material possessions, But this will not happen in history, the
pessimillennialists assure us.

Here is the cultural message of pessimillennialism: the New
Covenant is actually worse than the Old Covenant was with respect to

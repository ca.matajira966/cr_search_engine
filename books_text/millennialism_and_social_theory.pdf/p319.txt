What Is to Be Done? 303

Two Practical Questions

In 1902, Lenin wrote a book with a question for its title: What
Is to Be Done? Forty years earlier, that same title had been cho-
sen by the radical Cherneshevsky for a novel that he began
writing upon his imprisonment in the Peter and Paul Fortress.
His book had become a favorite of Lenin’s revolutionary older
brother, who attempted to assassinate the Czar, and was lawfully
executed.? It then became a favorite of Lenin’s. But Lenin did
not write a novel; he wrote a revolutionary manifesto. This was
to become the most important of his works. It was subtitled,
Burning Questions of Our Movement. Things burned for Lenin. His
newspaper was called The Spark (Iskra). As James Billington says,
there was fire in the minds of men.*

In 1976, Francis Schaeffer also wrote a book with a question
for a title: How Should We Then Live? This is the burning qucs-
tion of our movement: Christianity. his burning is different
from the radical’s burning: ours is fire initiated by the Spirit of
God, man’s only alternative to the consuming fire of final judg-
ment (Rev. 20:14-15).

Schaeffer did not answer his own question. His book does not
even attempt to do so; it is merely a brief popular history of
Western culture. It surveys the rise of authoritarianism, tells us
that we must spcak out against it, warns us that we may well be
executed for doing so, and offers no alternative political pro-
gram. He suggested no specific, concrete, Bible-bascd, compre-
hensive ethical system. Lenin, in contrast, did answer his ques-
tion, but his answers are now exposed to all mankind as produc-
tive of political tyranny and economic poverty. Leninism is just
one more version of the power religion, one more attempt to
build Babylon’s cmpire: the god that [ails.

So, what is to be done by all those who call themselves Chris-
tians? What do they do as individuals, as I hope to show, mat-

2. Robert Payne, The Life and Death of Lenin (New York: Simon & Schuster, 1964), pp.
65-72.

&. Ii, was not noticeably Marxist; it was elitist-terrorist: #id., pp. 148-54.

4, James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith (New
York: Basic Books, 1980),

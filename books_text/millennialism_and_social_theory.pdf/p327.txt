What Is to Be Done? 311

some point in history. He is saying, in no uncertain terms: “To
hell with the whole world. I’m in the Book of Life, and that’s
what counts for me.” It is a bad attitude, but it underlies all
pessimillennial Calvinism. The Arminian pessimillennialists have
an excuse: they do not believe in God’s irresistible grace. But
the Calvinist who thinks in pessimillennial terms has necessarily
adopted an elitist altitude: a world in which he assumes, and
sometimes even says publicly, that “God will not fill up heaven
with the people of my generation. But I've got mine!”

My attitude is different, f think: “Oh, God, if you were willing
to let me in, why don’t you let billions in? It’s no more difficult
for you to let five billion more in than to let me in.” I can pray
in confidence that God might do this in my day because I know
he will do it someday. Pessimillenialists do not pray for the
conversion of the world with my degree of confidence, even
those Presbyterian elders who take a public oath that they do
believe in the Westminster Larger Catechism (Answer 191),

Deflecting Evangelism

The Rapture could solve one aspect of the evangelism prob-
lem, of course. The post-Rapture millennium would get the
message of personal salvation to the lost, though it would not
necessarily get them saved. (A top-down political bureaucracy
run by Christians is hardly the equivalent of widespread, per-
sonal regeneration.) Most dispensationalists say that they believe
the Rapture is imminent. Fair cnough, but then they should not
keep predicting the imminent revival. The imminent Rapture is
the alternative to the imminent revival, not its means. If Jesus is
coming to set up an earthly kingdom, then the revival, if it
actually occurs, will be a post-“Church Age” phenomenon. The
Christians who are on earth today will not be here to see it, pro-
mote it, or respond to it. We will be in heaven, or wherever it is
the raptured Church will be sent for cosmic rest and recreation.

Institutionally, theologically, and emotionally, an appeal to
the imminence of the Rapture is the removal from the Church
of any responsibility for preparing for a great revival. Such a
revival cannot be prepared for today; it is a post-Rapture event.
If you wonder what I have most against premillennialism, this is

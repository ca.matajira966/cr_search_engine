What Is to Be Done? 323
Bishops

The process of church-planting is repeated until each of the
city’s quadrants is saturated. The initial goal is to create clusters
of four local congregations per city, each with its own pastor.
Above each cluster of four is a senior pastor, or bishop (presby-
ter), but always under the authority of the whole denomination.
Bishops must not be legally independent of the denomination’s
general assembly, which must include laymen.

I know, the word “bishop” terrifies independents and Presby-
terians. They equate the office with sacerdotalism. The fact is,
the office of bishop is inescapable — as inescapable as a single
captain of a ship. In the independent churches, the local pastor
is first among equals. Any Baptist pastor who is not strong
enough to give primary direction to his deacons is a soon-to-be-
dismissed pastor. A new man will be brought in, and this man
becomes first among equals. (For want of a better name, let us
call this the Spurgeon Effect.)

The office of bishop is inevitable in Presbyterian churches,
too; it is just not called by that loathed name. Three-office Pres-
byterianism — teaching elder, ruling elder, and deacon ~ makes
this office inescapable: ruling elders are not allowed to offer the
sacraments apart from the teaching elders. A Presbyterian pastor
answers to the presbytery, nat to the local congregation. He has
a seminary degree; his elders do not. The disciplinary system is
more academic and bureaucratic than pastoral, but it is surely
hierarchical. The Presbyterian pastor becomes a bishop opera-
tionally. But instead of answering to another bishop, he answers
toa series of committees. He is @ pastor without a pastor. Above the
local congregation, Presbyterian rule becomes impersonal, and
speedy justice is institutionally unobtainable. If Presbyterian
committees would each appoint a representative foreman who
could make decisions in the name of the committee, yet remain
subject to oversight by the committee, this complaint would not
be valid. But these foremen would then be bishops.

The goal is to create a judicial system in which quick decisions
by pastors is encouraged — the goal of speedy justice (Ex. 18) —
but always with the denomination’s general assembly, which
includes laymen, in the wings to hear appeals. No individual

326 MILLENNIALISM AND SOCIAL THEORY

oath, which only the institutions of the family and civil govern-
ment lawfully share. The covenant oath is always self-maledictory:
the individual promiscs to uphold the terms of the covenant,
and if he fails to do so, he calls down upon himself the negative
sanctions of God, including those lawfully administered through.
lJawful human government. God grants this authority to invoke
and demand an oath only to families, churches, and civil gov-
ernments. The sanctions of each institution are different: fami-
lies apply the rod (corporal punishment short of execution), civil
governments apply the sword (corporal punishment, including
execution), and churches restrict access to the communion table
(excommunication).

This Church oath involves the visible sign of baptism. Because
it possesses the lawful authority to cut people off from the
Lord’s Supper, it is a government. Because it is a government,
it possesses an institutional system for adjudicating disputes
among local members and between members and members in
other congregations and denominations. Here is where the
breakdown in Church order has become obvious, and has been
obvious for centuries. But churches pay no attention.

The gangs of Los Angeles do much better.

Conclusion

We have a tremendous opportunity today. We are seeing the
death of a major faith, salvation through politics.” While the
rhetoric of the imminent, transnational New World Order is
escalating, the economic vulnerability of all government welfare
programs becomes more and more visible. The reality of mod-
ern political life does not match the reality, any more than the
reality of Roman political life in the third century A.D. matched
the messianic announcements on the coinage. Reality will
soon triumph. Humanism as a rival religion is breaking down
even as it asserts the apotheosis of the New Humanity.

Something must be put in its place. There is no neutrality.
There can be no covenantal vacuums, The gangs of Los Angeles

17. Peter F. Drucker, The New Realities (New York: Harper & Row, 1989), ch. 2.
18, Ethelbert Stauffer, Christ and the Caesars (Philadelphia: Westminster Press, 1955).

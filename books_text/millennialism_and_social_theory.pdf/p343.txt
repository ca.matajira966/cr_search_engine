What is to Be Done? 327

testify loudly to this. The Church, however, is not equally
confident about this. Christians look at the religion of humanism
as if it were unbeatable. They have forgotten what God does
each time in history when covenant-breaking men begin build-
ing the latest Tower of Babcl.'? They no longer believe in
God’s negative corporatc sanctions in history.

Churches today are not prepared for the coming of mass
revival: theologically, institutionally, financially, educationally,
or morally. If we get a mass revival, new converts will inevitably
ask: “How Should We Then Live” Tf this new lite in Christ is
defined as “meet, eat, retreat, and hand out a gospel tract,” the
revival will leave one more egg on the face of God’s Church.

None of this is perceived by the churches, which are not ready
for revival. Yet revival may come nonetheless. If it does, we will
see the most remarkable example of on-the-job-training since
the early Church gathered in Jerusalem to meet, eat, and wait
for the Holy Spirit to put them to work. They were waiting to
receive power (Acts 1:8); today’s Church is waiting for late-night
reruns of “Ozzie and Harriet.”

If revival comes, millions of new converts will ask: “Now what
should we do?” What will pastors tell them? “Pray while you’re
plowing the fields”? Hardly. Ours is not a fronticr wilderness.
The division of international labor is the most developed in
mankind's history. Platitudes will not suffice. Yet platitudes are
all that Biblc-believing Christians have offered mankind for a
century. Christians have rejected biblical law, so all they can do
is baptize the prevailing humanism. But baptized humanism will
not suffice next time; humanism is too clearly bankrupt.

What is to be donc? Solomon told us three millennia ago:
“Let us hear the conclusion of the whole matter: Fear God, and
keep his commandments: for this is the whole duty of man”
(Eccl. 12:13-14). Jesus told us two millennia ago: “If ye keep my
commandments, ye shall abide in my love; even as I have kept
my Father's commandments, and abide in his love” (John
15:10). The churches have not listened. They need to, soon.

19. C. S. Lewis, That Hideous Strength: A Modern Fairy-Tale for Groum-Ups (New York:
Macmillan, 1946), ch. 16.

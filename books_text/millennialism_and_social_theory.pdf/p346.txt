330 MILLENNIALISM AND SOCIAL THEORY

This is the vision 1 shared with President Gorbachev in Helsinki.
He, and other leaders from Europe, the gulf and around the world,
understand how we manage this crisis today could shape the future
for generations to come.*

He speaks of a hundred generations. This takes us back to
the era of Abraham or thereabouts, in the days when Egypt
rocked the cradle of civilization. From Egypt to 1990: a lengthy
gestation period. I think Mr. Bush was not deliberayely exag-
gerating, as messianic as his extended timetable may initially
appear. The model of Egypt is always the covenant-breaker’s
preferred alternative to decentralized biblical civilization. It is
time to recall the words of the great German sociologist Max
Weber, in a speech he delivered in 1909:

To this day there has never existed a bureaucracy which could
compare with that of Egypt. This is known to everyone who knows
the social history of ancient times; and it is equally apparent that to-
day we are proceeding towards an evolution which resembles that
system in every detail, except that it is built on other foundations,
on technically more perfect, more rationalized, and therefore more
mechanical foundations. The problem which besets us now is not:
how can this evolution be changed? — for that is impossible, but:
what is to come of it?

Our generation is about to get the answer to Weber's question.
We now face the looming threat of Egypt revisited. This is far
more of a threat to the enemies of Christ than to the Church.
“Thus saith the Lorp; They also that uphold Egypt shall fall;
and the pride of her power shall come down: from the tower of
Syene shall they fall in it by the sword, saith the Lord Gop”
(Ezek. 30:6). The towers of this world shall crumble, and those
who trust in them shall fall.

A. “Text of President Bush's Address to Joint Session of Congress,” New York Times
(Sept. 12, 1990).

5, Max Weber, “Speech to the Verein fir Soxialpolitik” (1909); reprinted in J. P,
Meyer, Max Weber and German Politics (London: Faber & Faber, 1956), p. 127. Cf. Gary
North, “Max Weber: Rationalism, Irrationalism, and the Bureaucratic Cage,” in North
(ed.}, Foundations of Christian Scholarship: Essays in the Van Tit Perspective (Vallecito, Califor-
nia: Ross House Books, 1976), ch. 8.

336 MILLENNIALISM AND SOCIAL THEORY

The Bible teaches the doctrine of creation, meaning creation
out of nothing. It teaches that man rebelled against God, and
both nature and man now labor under God’s historical curse. It
tells of Jesus Christ, the Son of God: His birth, ministry, death,
resurrection, and ascension to heaven to sit at the right hand of
God, It tells of Pentecost, when He sent His Holy Spirit. It tells
us of Christ’s Church in history, and of final judgment. There
is direction in history and meaning in life.

Christians are told to believe in “thousands of generations”
as their operating time perspective. This is probably a meta-
phorical expression for history as a whole. Few if any Christians
have taught about a literal 60,000-year period of history (2,000
times 30 years). The point is, the Bible teaches that the king-
dom of God can expand for the whole of history, while Satan’s
empires rise and fall. There is no long-term continuity for
Satan’s institutional efforts. He has nothing comparable to the
Church, God's monopolistic, perpetual institution that offers
each generation God’s covcnantal sacraments.

If growth can be compounded over time, a very small capital
base and a very small rate of growth jeads to the conquest of
the world. Growth becomes exponential if it is maintained long
enough.” This is the assured basis of the Christianity’s long-
term triumph in history. God is faithful. The temporary breaks
in the growth process duc to the rebellion of certain genera-
tions of covenanted nations do not call a halt to the expansion
of the kingdom.

‘The errors, omissions, and narrow focus of any particular
Christian society need not inhibit the progress of Christ’s earth-
ly kingdom. These limitations can be dealt with covenantally.
The international Church can combine its members’ particular
skills and perspectives into a world-transforming world and life
view (Rom. 12; I Cor. 12). Modern telecommunications and
modern airborne transport are now making this possible.

Christianity has in principle a far more potent view of time

Christian Econontics, 1987), ch. 1: “Cosmic Personatism.”
13. Gary North, The Sinai Strategy: Econcmirs and the Ten Commandments (Tyler, Texas:
Institute for Christian Economics, 1986), pp. 101-3.

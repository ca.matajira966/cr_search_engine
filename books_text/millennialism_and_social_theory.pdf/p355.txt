Conclusion 339

If so, who will inherit the rotting hulk: Christianity, humanist
democracy, international bureaucracy, or Islam?

Conclusion

The Bible teaches that God deals covenantally with nations,
even at the final judgment and beyond. Thus, nations are under
the terms of the covenant, either explicitly (ancient Israel) or
implicitly (all nations under God as Judge). The covenant pro-
cess of blessings and cursings is therefore called into operation
in the history of nations. National continuity and discontinuity
must be viewed as an outworking of this fourth point of the
biblical covenant.

History has seen the rise of empires. They have all failed.
They are satanic imitations of the definitively (though not his-
torically) unified kingdom of Christ on earth. The tendency of
Christ’s kingdom is toward expansion. This leavening process is
also a feature of Satan’s imitation kingdom. But his kingdom
has been on the defensive since Calvary. Whenever Christian
nations remain faithful to the terms of God’s covenant, they
experience blessings leading to victory over time. Whenever
they have apostatized, they have faced judgment and have had
their inheritance transferred to other nations, either through
military defeat or economic defeat.

The West now faces its greatest challenge since the fall of the
Roman Empire, The formerly Christian West has abandoned
the concept of the covenant, and with it, Christianity’s vision of
victory in history. The humanists cling to a waning worldview,
announcing their New World Order even as the moral and
intellectual foundations of such confidence are lost. Like the
coins issued by Roman emperors, one after another, that kept
announcing the dawn of a new age, a new political salvation,
the dross is replacing the silver.'’ This is a religious crisis, and
it has become visible in every area of life.

Peter Drucker has identified both the nature of this crisis and
the opportunity: “The death of the belief in salvation by society,

formation (New York: Dodd, Mead, 1984).
17, Ethelbert Stauffer, Christ and the Caesars (Philadelphia: Westminster Press, 1955),

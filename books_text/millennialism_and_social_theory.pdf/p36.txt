20 MILLENNIALISM AND SOCIAL THEORY

City of God has been so overwhelming that his earlier writings,
especially his six volumes of commentaries on the Psalms, have
been neglected. It is in his exposition of Psalm 110 that we see
his vision of world dominion by Christians. His less precise,
more symbolic references to time and eschatology in City of God
prevailed in Western Christianity throughout the medieval
period. Calvin’s writings suffer from a similar ambiguity. There
are both postmillennial and amillennial passages in his writings,
but the influence of the Institutes of the Christian Religion, which
is comprehensive but less detailed on matters of eschatology,
has led even the Calvinists to neglect his Bible commentaries
and other theological works in which his historic optimism is
readily apparent.”

Postmillenniatism

Postmillennialism has features of both premillennialism and
amillennialism. It shares with amillennialism a commitment to
historical continuity. Both views insist that Jesus will not return
to earth physically to establish a millennial kingdom. It shares
with premillennialism a commitment to the earthly fulfillment
of many of the Old Testament’s kingdom prophecies. Postmil-
lennialism’s hermeneutic (principles of biblical interpretation) is
neither exclusively literalistic nor exclusively symbolic. (For that
matter, this is also true of dispensationalism’s hermeneutic; it
only appears to be literalistic.)"*

Postmillennialism had its greatest historical impact from the
early Puritan era through the North American religious revival
known as the First Great Awakening (1735-55).* It dominated
conservative American Presbyterian theology, North and South,

 

17. Gary North, “The Economic Thought of Luther and Calvin,” Journat of Christian
Reconstruction, U (Summer 1975), pp. 104-6. Greg L. Bahnsen has catalogued many of
Calvin’s postmillennial statements: Bahnsen, “The Prima Fecie Acceptability of Postmillen-
nialism,” ibid., II} (Winter 1976-77), pp. 69-76.

18. Kenneth L. Gentry, Jr, “Consistent Literalism Tested,” Dispensationalism in
Transition, 111 (Sept. 1990), published by the Institute for Christian Economics.

19. Symposium on Puritanism and Progress, Journal of Christian Reconstruction, VI
(Summer 1978); Jain Murray, The Puritan Hope: Revival ond the futerpretation of Prophecy
(London: Banner of Truth Trust, 1971).

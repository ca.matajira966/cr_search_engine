348 MILLENNIALISM AND SOCIAL THEORY

You: But what about those who have been paying?

Smurd: ‘They are taken to heaven.

You: And then what happens to us?

Smurd: The trust document does not say exactly. But you
will be taken care of, I assure you.

You: And what about those left on earth?

Smurd: They get hammered.

You: Well, I certainly don’t want to get hammered.

Smurd: Of course not. Of course, they don’t get hammered
forever.

You: What do you mean?

Smurd: They get hammered for seven years. Or possibly
three and a half years. There’s some debate over this.

You: And then what?

Smurd: They inherit the earth.

You: You mean they get all of the trust’s intermediate distri-
butions?

Smurd: That is correct.

You: What about assessments?

Smurd: Only the evil-docrs pay.

You: But that’s the way it ought to be now!

Smurd: You aren’t the first person to say that.

You: You mean others have objected?

Smurd: They used to.

You: Why not any longer?

Smurd: They all died off during World War I.

You: Who were these people?

Smurd: Postmillennialists.

You: And they left no heirs?

Smurd: Only a handful.

You: Who are these people?

Smurd: Christian Reconstructionists.

You: Aren’t they legalists?

Smord: Horrible: lawyers who belicve in permanent law.

You: We're under grace, not law!

Smurd: Not exactly. You’re under lawyers.

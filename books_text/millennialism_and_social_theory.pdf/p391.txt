premillennialism &, 92-94
prophesies, 104, 124
realized eschatology, 212-13
recruiting, 89
resurrection and, 225
sanctions, 101-2, 177
schizophrenic, 92, 230-31
scholarship, 76
shared perspective, 92-94
social theory, 169
soft underbelly, 213
trend-tending, 125
“ultimate,” 91, 111
utopian, 88, 92
victory, 106, 111-16, 230,
233
view of progress, 230-31,
236
Westminster Seminary, 237
Amish, 269
amoeba, 322
Amorites, 160
Anabaptism, 49, 229, 268-69
Anderson, J. Kerby, 206
annihilation, 17
Antichrist, 249, 254, 271
antinomianism, 72, 156, 242, 247
apocalypticism
active and passive, 268-69
anti-growth, 268
Calvinism, 274
millennial views, 269-73
three-stage, 272
utopianism &, 89
apprenticeship, 316-17, 321
Armageddon, 19, 22, 95, 204,
271-72
Arminianism, 249, 274, 298, 308,
311, 315
Atlas, Charles, 152, 256
Augustine, 19-20, 25, 157-61,
172-73
Augustine’s Revenge, 322

375

Augustinianism, 15
authority

Bible, xiv-xv

Church's, 305

cultural, 138

kingdom of God, 275-77

progressive, 198

social theory, xv
autonomy, 58, 60, 285

Babylon, 101, 104, 107, 128-29
Bad News, 22, 80, 101, 113, 115,
124, 166, 211
Bahnsen, Greg, 73
bait and switch, 91-92
Barth, Karl, 112-13
Becket, Thomas, 305
Belshazzar, 128
Berkhof, Louis, 180, 188n
Bible
authority of, xiv-xv
doctrines, 336
message of (6 words), 2-3
relevance of, 169
social theory, xv
biblical theology, 259
bishop, 329-23
blessed hope, 22-23
blessings, 12, 21, 23, 54, 107
blindness, 143-44
blueprints, 1, 258, 260-61
Boettner, Loraine, 243-47
bubonic plague, 299, 322, 289
bureaucracy, 151, 205, 316-17,
330, 331
Bush, George, 329-30

Calvin, John
amitlennial?, 19-20
followers, 164
sanctions, 163-64
Sermons on Deuteronomy,
161-62

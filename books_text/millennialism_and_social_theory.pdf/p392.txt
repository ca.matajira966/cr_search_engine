376 MILLENNIALISM AND SOCIAL THEORY

Calvin Theological Seminary,
170

Calvinism, 79, 84, 167, 311

Campbell, Roderick, 7!

capita, 54

Captain Marvel, 256

case laws, 72

casuistry, 69, 181

Chafer, Lewis Sperry, 180-81

change, 127

chastening, 69

chastisement, 294

China, 40

Chilton, David, 140-44

Christian Reconstruction
activism, 72
critics, 470
critics of, 180, 257
endless, 69
intellectual, 73
no alternative, 181
otherworldly?, 30
relevance, 169
two-front war, 73

Christian schools, 270

Christianity, 45-49, 184

Church.
agreement (little), 2, 70
amillennial “victory,” 88,

106, 112-13

amoeba analogy, 322
authority, 190, 316, 325-26
authority and, 275
bishop, 322-23
chastisement, 294
continuity, 258
covenant lawsuit, 211-12
creeds, 27-28, 220-21
decentralization, 299, 323-26
deliverance, 137
discipline, 325
disinherited?, 294-98
distinct, 275

division of labor, 336
eschatology, 17
excommunication, $05
exile, 171, 261
exile of, 165-67
general eschatology, 17
ghetto, 115, 117
government, 325-26
immigrant, 270
imprecatory psalms, 293
inertia, 285-87, 299
inheritance, 297
Jerusalem, 98
kingdom and, 274-77
limited success, 153
loses?, 227
loyalty to, 305
main problem, xi
maturation, 221
megachurch, 321-22
Militant, 236
models, 287
negative sanctions, 211
oath, 325-26
oppressed, 107
origins, 275
peaceful program, 134-35
primary, 304, 306-7
progress of, 172-73
rescue mission, 300
responsibility, 307
roiten wood, 274
sanictification, 219
social theory, 302
strategies, 290-91
‘Triumphant, 236
unprepared, 297
victory, 227
Wimpetant, 236
work buried, 255
work of, 269

Church Militant, 86

Charch Triumphant, 86

churls, 96, 105
citizenship, 156, 236
City of God, 160
city on a hill, 117
civil religion, 24
Civil War, 814
civilization, 28, 83 (sce also king-
dom)
class, 40
Colson, Charles, 173
Columbus, C., 289
comic book theology, 255-56
commandments, xiii, 4, 52 (sce
also law of God)
common grace
Adam, 8
continuity, 137
historical relevance, 167
millennialism, 16
oscillating, 172
questions, 183-84
temporary, external, 135
time (gift of), 137-38
Van Til’s version, 79-82
Communism, 337-38 (sce also
Marx, Marxism, Lenin)
compound growth, 53-54, 148-49
284
conditionality, 120, 124
consistency, 45
contractualism, 35-36, 39
continuity
amillennial, 123
battle with sin, 67-68
blessings, 5, 7
common grace, 137
covenant-breakers, 153
discontinuity and, 154, 258
dispensationalism vs., 193
downward, 123
enlightenment faith, 15
ethical, 139
evangelism, 288

Index

377

final judgement and, 139
forgetfulness, 5
hell, 9
Hoekema affirms, 170
inevitable, 115
institutional, 25
life and afterlife, 7-16, 138,
167-72
millennium, 22-26
New Heaven, 6
postjudgment world, 297
redemptive history, 69
sin (subsidy to}, 5
social reform, 132
social theory, 94
time & eternity, 94, 167-72
wheat and tares, 296
conversion, ix, 246
costs, 65
covenant
amillennialism vs., 169
conquest in history, 288
death, 53
disasters, 234-35
displacement, 133-37
dominion, 318, 335
evangelism, 317-19
faithfulness, 174
kingdom &, 284
lawsuit, 211-12
model, 78-79, 186-87, 318
Pratt on, 77-78
predictability, 212
ratified, 4
renewal, 53
separation, 267
silence, 179
social sanctions, 157
social theory, 33
struggles, 42-43, 284
success, 53
trivium &, 78-79
undefined, 40

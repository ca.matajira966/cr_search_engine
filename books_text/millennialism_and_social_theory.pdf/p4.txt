Copyright, Gary North, 1990

 

Library of Congress Cataloguing-in-Publication Data

North, Gary.
Millennialism and Social Theory / Gary North
p- cm.
Includes bibliographical references and index.
ISBN 0-930464-49-4 : $14.95

1. Millennialism. 2. Sociology, Christian. 3. Eschatology.
4. Dominion theology. I. Title.
BT891.N67 1990
236'.9-dc20 90-47609

CIP

 

Institute for Christian Economics
P. O. Box 8000
Tyler, Texas 75711

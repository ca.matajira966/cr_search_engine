48 MILLENNIALISM AND SOCIAL THEORY

jar eschatology but then deny its social implications? Even more
painfully, what if the denomination specifically insists on eschat-
ological neutrality in the ordination of its officers? Third, it
raises psychological questions: “I believe in ¢his eschatology.
Must I also believe in éhaé social theory if I am to remain theo-
logically consistent? Can I even develop a Bible-based social
theory that is consistent with my eschatology?” These ques-
tions disturb amillennialists and premillennialists alike, for both
groups systematically shun social theory. Above all, these ques-
tions disturb that handful of dispensational premillennialists
who are also social activists: the problem of consistency.*
Christian responses have been mixed to Christian Reconstruc-
tionism’s claims in favor of an explicitly biblical social theory.
Some of these critics insist that the traditional categorics of Stoic
and medieval natural law theory are universally valid, and
therefore constitute the only legitimate foundation of social
theory in a post-Resurrection world — a pluralist, humanist-
dominated world. Others take the seemingly less controversial
approach, professing ignorance, but implicitly proclaiming
ignorance as universally binding. They proclaim a kind of spiri-
tual agnosticism. They thereby deny the possibility of Christian
social theory, let alone its necessity. For example, English Cal-
vinist Baptist Errol Hulse writes: “Who among us is adequately
equipped to know which political philosophy most accords with
biblical principles?" (Implication: certainly not the theonom-

47. For a discussion of these sorts of problems, see D. Clair Davis, “A Challenge to
Theonomy,” in William S, Barker and W. Robert Godfrey (eds.), Theonomy: A Reformed
Critique (Grand Rapids, Michigan: Zondervan Academie, 1990), ch. 16.

48. Aclassic example is Professor H. Wayne House, who in 1990 feft Dallas Theologi-
cal Seminary for Western Baptist College in Oregon. He is both a social activist and a
dispensalionalist. See Richard A. Fowler and H. Wayne House, The Christian Confronts His
Culture (Chicago: Moody Press, 1983), His failure to respoud in print to Bahnsea and
Gentry's detailed refutation of House and Ice’s Dominion Theology: Blessing or Curse?
(Portland, Oregon: Multnomah Press, 1988), is indicative of his problem. His silence gives
the appearance of the short-circuiting of his worldview, especially since he has now
resigned from seminary teaching, See Greg L. Bahnsen and Kenneth L. Gentry, House
Divided: The Break-Up of Dispensational Theology (Iyler, Texas: Institute for Christian
Economics, 1989). For a truly pathetic attempt by a dispensational theologian to respond
to House Divided, see John Walvoord’s review in Bidliotheca Sacra (july-Sept. 1990). For my
response, see “First, the Brain Goes Soft,” Dispensationalism in Transition, INI (August 1990).

49. Errol Hulse, “Reconstructionism, Restorationism or Puritanism,” Reformation

70 MILLENNIALISM AND SOCIAL THEORY

son, despise not thou the chastening of the Lord, nor faint when thou art
rebuked of him: For whom the Lord loveth he chasteneth, and scourgeth
every son whom he receiveth. If ye endure chastening, God dealeth with
you as with sons; for what son is he whom the father chasteneth
hot? But if ye be without chastisement, whereo/ all are partakers,
then are ye bastards, and not sons. Furthermore we have had fa-
thers of our flesh which corrected us, and we gave them reverence:
shall we not much rather be in subjection unto the Father of spirits,
and live? For they verily for a few days chastened us after their own
pleasure; but he for our profit, that we might be partakers of his
holiness (Heb. 12:1-10). (emphasis added)

Conclusion

Basic to biblical social theory is the idea of cultural progress.
God brings His positive and negative sanctions in history in
terms of His fixed moral standards, which are revealed clearly
only in the Bible. There is a concrete universal in history who
has met these moral and judicial standards: Jesus Christ. Perfec-
tion has been attained once (and only once) in history. This
perfection is imputed to redeemed people at the point of their
salvation (definitive sanctification). Nevertheless, perfection is
not reached in history. It is a lifelong process (progressive sanct-
ification).

What is true of the individual is also true of covenantal and
non-covenantal corporate units. Their members are required to
strive all through history to reach perfection as a corporate
limit, by means of self-government, Church government, family
government, and civil government. Each form of government
involves the application of appropriate sanctions. When their
representatives refuse to apply them, God will apply His appro-
priate negative corporate sanctions in history.

This only states what the social ideal is. The question then
arises: Can social progress be realized in history? Is it like individual
sanctification: attainable progressively in history? On the answer
to, this question, the Church has long been divided. Generally,
the answer has been that there is no covenantally meaningful
social progress. This has had crucial implications for Christian
social theory, or the absence thereof.

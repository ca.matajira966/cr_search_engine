78 MILLENNIALISM AND SOCIAL THEORY

nearly 500-page book on the Old Covenant. He outlines the
five-point covenant structure on page 286, but does nothing
with it, He does not offer a single example of how this model
was used in any Old Covenant narrative, nor does he mention
the prophets’ use of it. He suggests no application of the cove-
nant model in society, either in the Old Testament or the New
Testament. His only other reference to the covenant — but not
its five points — is a brief mention of “major covenant events in
the days of Adam, Noah, Abraham, Moses, and David.”* He
does not say what these events were. He then implicitly aban-
dons covenant theology for what he calls the “organic model”**
and “organic developments.”* But when stripped of its judicial
(ie., covenantal) foundations, as Pratt clearly insists that it
should be,” social organicism becomes the worldview of Ro-
man Catholicism, philosophical realism, and traditional secular
conservatism. Organicism is an alternative to covenant theology,
not an application of it.” Yet all of this is presented to the
reader as an exercise in biblical hermeneutics by a Calvinist
covenant theologian. Calvinistic, yes; covenantal, no.

Yes, God did give us many stories. The questions are: What
kind of stories? With what moral? With what structure? Leading
to what action, bath personal and corporate? Pratt docs not say.
He is subtly attempting to substitute a symbolic-literary inter-
pretation of the Bible for the traditional grammatical-historical
approach. The problem is, neither of these approaches alone
tells us what the theology of the Bible is. The war between the
two camps goes on, when it could be resolved by settling the
neglected issue: the éheological structure governing the narratives.
What is this structure? The biblical covenant model.*

The medieval trivium reflected the tripartite division of

 

28, Dbid., p. 837.

24, Tbid., p. B41.

25. Idid., p. 844.

26. Ibid., pp. 343-44,

27. On the misuse of the organic model, see Gary North, Moses and Pharaoh: Domin-
ion Religion us. Power Religion (Tyler, Texas: Institute for Christian Economics, 1985), ch.
17: “The Metaphor of Growth: Ethics.”

28, Sutton, That You May Prosper.

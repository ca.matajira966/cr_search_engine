Pessimillennialism 83

outline of such a theology has never been offered. Any amillen-
nial scheme must proclaim one of two positions: either linear
history downward into public evil (Van Til’s view) or ethically
random historical change in a world presently controlled by
covenant-breakers (Kline’s view). Perhaps some energetic and
creative amillennialist will make the exegetical attempt some-
day, but so far, optimistic amillennialism is simply soft-core
postmillennialism for amillennialists still in transition.
Despite the structural cultural pessimism built into all amil-
lennialism, common grace amillennialists often insist that God’s
kingdom also develops and even expands in history. But how
can both kingdoms expand simultaneously? It is covenantally
inconsistent to argue, as these amillennialists do argue, either
implicitly or explicitly, that Satan’s visible kingdom expands in
history, while only God's invisible kingdom expands. The biblical
concept of these rival kingdoms is this: rival civilizations. Each is
both natural and supernatural. Each is a covenantal unit. If
Satan’s kingdom has both a spiritual and an institutional side,
then so must God’s. How can one kingdom (civilization) expand
if it does not progressively push the other out of history’s cul-
tural loaf? Yet common grace amillennialists insist that each
kingdom expands. Evil “leaven” wins, despite Matthew 13:33.
They can defend this two-leavens perspective only by playing
games with language. They belatedly admit in the back pages of
their books that Satan’s earthly covenantal representatives will
progressively impose their negative sanctions against Christians
as history advances. This admission makes ludicrous the idea of
God's kingdom in history. Amillennialism proclaims @ kingdom
whose designated representatives cannot bring the King’s sanctions in
history, which is a denial of any judicial connection between
God's kingdom-civilization and history. Yet it is precisely this
that amillennialists proclaim: a kingdom whose only predictable,
institutional, covenantal sanctions in history are ecclesiastical
and familial, never civil. This makes God the Lord of a declin-
ing percentage of churches and families, but not of history. As

43, See Chapter 7, below.
44. [have never heard of anyone who claims to be an optimistic premillennialist.

Preface xi

spend so much space dealing with such dead issues?” we ask our-
selves. The answer is simple: because when the books were written,
those issues were not dead. Similarly, a hundred years from now,
any readers who may stumble across this book will skim over most of
its extended quotations. Few works of scholarship in one generation
survive into the next, and the writers I cite or refute will be long-
forgatten for the most part. Indeed, many of them are not well-
known today. I am not devoting time simply to refute every erroneous
idea in sight; I am using these citations as examples, as springboards
to introduce explicitly biblical interpretations. The scholars I cite are
very often foils for me; I want readcrs to know that such ideas exist
and need refuting or reinterpreting.

The most important thing is how well I integrate such humanistic
insights into my biblical reconstruction of economics, without 1) los-
ing the importance of these insights or 2) becoming a slave of the hu-
manist presuppositions which officially undergird such insights. But
this is the most important task in any field. Every Christian faces this
problem. We buy and sell with pagans in many marketplaces, and
one of these marketplaces is the marketplace for ideas. We must use
their best ideas against them, and we must expose their worst ideas
in order to undermine men’s confidence in them. In short, in God's
universe, it is a question of “heads, we win, tails, they lose.”

The Outrage of the Christian Classroom Compromisers

It is important to understand from the beginning that the per-
spective expounded in this book is unpopular in academic Christian
circles. Two economic ideas dominate the thinking of the twentieth
century: the idea of central economic planning, and the idea of the
“mixed economy,” meaning interventionism by the civil government
into the economy: Keynesianism, fascism, or the corporate state.
Men have had great confidence in the economic wisdom of the State,
at least until the 1970's. Most Christian academics in the social
sciences still go along enthusiastically with some variant of this
statist ideology. Thus, when they are confronted with what the Bible
really teaches in the ficld of political economy, they react in horror.

Most amusingly, one of these interventionists has accused me of
holding Enlightenment ideas,? not realizing that he and his associ-

2. Ronald Sider, Rich Christians in an Age of Hunger (rev. ed.; Downers Grove,
Hinois: Inter-Varsity Press, 1984), p. 102.

92 MOSES AND PHARAOH

The nature of the punishment testifies to Pharaoh’s understand-
ing of the issues involved: theological, political, and economic. He
also grasped at least some of the implications of the law of the op-
timum mix of production factors, This law of the optimum produc-
tion mix is a corollary to the law of diminishing returns. Perhaps it
would be more accurate to say that the law of the optimum production
mix is really another way of stating the law of diminishing returns,® The
existence of the optimum factor mix is the conclusion we reach when
we acknowledge the existence of the law of diminishing returns,
What do economists mean when they speak of the law of diminishing
returns?

Complementary Factors of Production

We note that when two or more complementary factors of pro-
duction are combined in the production process, output per unit of
resource input rises initially as we add the “sub-optimum” factor of
production, and then output begins to fall after the formerly “sub-
optimum” resource passes the point of optimality. This is an eco-
nomic law which might better be described as the law of variable pro-
portions. One person, for example, cannot efficiently farm 800 acres,
unless he has specialized equipment. Facing the task of farming 800
acres, he adds additional labor. He hires another man to help him
work the land. By combining the labor of two workers, the worker-
owner sees the value of his farm’s total agricultural output increase
by more than the cost of hiring the extra laborer. So he adds more
laborers and/or equipment. Output value increases even faster than
input costs increase, mitially But at some point, the law of
diminishing returns (variable proportions) tells us, the increases in
output value will begin to lag behind the increases in factor input
costs. There will be too many workers an the fixed supply of 800
acres, They will get in each other’s way, or spend too much time
loafing, or fighting, or whatever. Output will actually decline if the
owner adds still more laborers without buying or leasing more land.
In other words, it will pay him to dismiss some workers. He will

5. Writes Murray Rothbard: “Lhe law of retums states that with the quantity of com-
plementary factors held constant, there always exists some optimum amount of the varying factor.”
Rothbard, Man, Economy, and State (New York: New York University Press, [1962]
1975), p. 30.

6. Joseph Schumipeter, History of Economic Analysis (New York: Oxford University
Press, 1954), p. 260.

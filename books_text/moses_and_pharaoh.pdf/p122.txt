8
COVENANTAL JUDGMENT

Then the magicians said unto Pharaoh, This is the finger of God: and
Pharaoh's heart was hardened, and he hearkened not unto them; as the
Lop had said (Ex. 8:19).

The supernatural contest between the representatives of God
and the magicians of Egypt escalated from the beginning. In the first
confrontation, Aaron cast down the rod that God had presented to
Moses. It became a serpent. The magicians matched this display of
supernatural transformation, but then Aaron’s serpent swallowed
the serpents of the Egyptians. This presumably did not help the
magicians to persuade Pharaoh that their magic was stronger than
the supernatural power available to these two Hebrews (Ex.
7:10-12). The second display of power involved the Nile’s water.
Aaron stretched out the rod, and the waters of Egypt turned to
blood. The magicians matched this supernatural act (Ex. 7:19-22).
In this instance, their magic seemed comparable to Aaron's. How-
ever, this was no consolation to Pharaoh; now he had even more
bloody water than he had before the contest. This same “victory” of
Egyptian magic was matched in the coming of the frogs (Ex. 8:5-7).
We can almost imagine Pharaoh’s consternation, how he must have
said to himself, “No, you idiots, not more frogs; fewer frogs! Show me
how powerful you are by removing these frogs.” His magicians were
determined to prove their mastery of the black arts by matching
Aaron, plague for plague, thereby reinforcing God's judgment on Egypt.
But there is no indication that they brought additiona] blood and
frogs to the land of Goshen, whcre the Hebrews lived, They simply
multiplied the burdens of Egypt. The Egyptians were worse off as a
result of the nation’s magicians. The arrogance of the magicians had
made things even less bearable.

It should be understood, however, that the first three plagues did

104

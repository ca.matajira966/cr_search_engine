Covenantal Judgment 113

possible. In order to explain its becoming and its evolution it is not
necessary to have recourse to a doctrine, certainly offensive to a truly
religious mind, according to which the original creation was so
defective that reiterated superhuman intervention is needed to pre-
vent its failure.”? In other words, “a truly religious mind” is the mind
of an eighteenth-century Continental European deist, whose silent,
distant God is sufficiently irrelevant to human affairs to satisfy a
generous and broad-minded humanistic economist (assuming he is
not a follower of Ayn Rand’s atheistic “objectivism’—and few
economists are).1°

For Mises, a confirmed utilitarian, “Law and legality, the moral
code and social institutions are no longer revered as unfathomable
decrees of Heaven, They are of human origin, and the only yard-
stick that must be applied to them is that of expediency with regard
to human welfare.”44 His methodological individualism is grounded
in human expediency, which somehow (he cannot say how) is under-
stood by all men, or at feast all reasonable men who recognize the
value of free market economics and economic growth, In short,
economists — fue economists, meaning defenders of the free market
—“have repeatedly emphasized that they deal with socialism and in-
terventionism from the point of view of the generally accepted values
of Western civilization.”*? Even a methodological individualist some-
times finds collectives —“the values of Western civilization” — episte-
mologically indispensable. Sadly, Mises never admitted what
should have been obvious, specifically, that he was dependent upon
the epistemological holism which he officially and vocifcrously re-
jected.'3

The idea of representative civil government was basic to
nineteenth-century liberalism. The defenders of classical liberalism

9. Lbid., pp. 145-47.

10. Rothbard once remarked concerning the Randians: “They hate God more
than they hate the State.” For a comprehensive critique of Rand’s thought, see John
Robbins, Answer to Ayn Rand: A Critique of the Philosophy of Objectivisin (Washington,
D. C.: Mt. Vernon Pub. Go., 1974), distributed by the Trinity Foundation, P.O.
Box 169, Jefferson, Maryland 21755.

11. Mises, Human Action, p. 147.

12, Mises, Theory and History: An Interpretation of Social and Economic Evolution (New
Rochelle, New York: Arlington House, 1969), p. 33, Published originally by Yale
University Press in 1957,

13. Gary North, ‘Feonomics: From Reason to Intuition,” in North (cd.), Founda-
tions of Christian Scholarship: Essays in the Van Til Perspective (Vallccito, California: Ross
House, 1976), pp. 87-96.

 

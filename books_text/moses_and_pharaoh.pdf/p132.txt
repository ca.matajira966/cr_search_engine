114 MOSES AND PHARAOH

wanted limited civil government and a free market economy. But
there are few strict defenders of the old faith today. Mises and Hayek
have few followers. Their intellectual heirs are either Christians or
outright anarchists. Neither group (a holistic noun) accepts the view-
point of nineteenth-century classical liberalism.'t Christians base
their views on the Bible, and the anarchists want no civil government
certainly not one which is supported by compulsory taxation.
The empirical or “positive economics” of the Chicago School
defends the limited-government viewpoint. These scholars do not
appeal to hypothetically universal rights of man that are based on
natural law. They explain economics strictly in terms of economic
self-interest, and they usé scientific tools of “empirical,” value-free
economic analysis, especially mathematics, which implies sore sort
of holism (economic aggregates). Mises categorically rejected such
holism as a valid tool for understanding human action. Therefore,
the old classical liberalism, with its strict commitment to methodo-
logical individualism, is today a shadow of its former moral self.

Conclusion

The Pharaoh’s court magicians warned him. They told him that
he was facing God almighty. He did not accept their evaluation, or
at least he chose to challenge the God of Moses anyway. Did this
protect the families of the magicians? Did they avoid the plagues?
Did they escape the death of their firstborn? Not without the blood
on the doorposts: Not without an outward covenantal sign in-
dicating that they had placed thermselves under the sovereignty of

 

14. It is one of the ironies of recent history that the two main groups that continue
to read and quote Mises both reject his utilitarianism. The anarcho-capitalists, led
by Rothbard, are defenders of natural law theory, and they explicitly reject
utilitarianism as a legitimate foundation of social and economic theory: Rothbard,
For a New Liberty, p. 16. They are anarchists, and Mises explicitly rejected anar-
chism, He cven said that a military draft is sometimes legitimate: “He who in our
age opposes armaments and conscription is, perhaps unbeknown to himself, an
abettor of those aiming at the enslavement of all.” Human Action, p. 282. The
anarcho-capitalists seldom go into print against Mises by name, since they are self-
professed followers of Mises and Austrian economic theory, but they have abandoned
much of his epistemology (he was a self-conscious Kantian dualist, as well as a
utilitarian) and his philosophy of litnited (rather than zero) civil government. The
other group that uses Mises’ economic arguments is the Christian Reconstruction
movement, whose members reject his humanism-agnosticism and his methodologi-
cal individualism. There are virtually no strict followers of Mises—Kantians,
ntilitarians, non-anarchists— under 60 years of age who are still writing in the early
1980's.

   

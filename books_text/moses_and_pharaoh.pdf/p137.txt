Original Ownership 119

cluding the souls of men. Private ownership, not State ownership, is
the foundation of the parable. 0 challenge the legitimacy of God's
delegated sovereignty of private ownership of the means of production is to
challenge the doctrine of the original sovereignty of God. Socialism is therefore
an innately demonic and evil doctrine. It is not surprising that the rise of
socialism and statism in the West was also accompanied by the rise
of philosophies hostile to the sovereignty of God.?

Pharaoh’s Assertion

Pharaoh recognized the implications of this doctrine of God’s ab-
solute ownership, and he rejected it. He refused to humble himself
before the God of the Israelites. He had no fear of this God, despite
God’s contro} over the forces of nature. He rejected the doctrine of
God’s sovereignty over the affairs of Egypt. He, as a legitimate god,
according to Egyptian theology, possessed at least some degree of au-
tonomous sovereignty. He was entitled to the lives and labors of
these Hebrews, Any attempt by their God to impose non-negotiable
demands on Egypt had to be resisted. To capitulate on this issue
would have implied a moral obligation on Pharaoh’s part to conform
his economic decisions to the law of God. If God owns the whole
earth, then each man is merely a steward of God's property, and is
therefore morally obligated to administer the original owner's prop-
erty according to His instructions. Pharaoh would have had to
acknowledge his position as a subordinate ruler under God, a prince
rather than aking. He would not do it.

As Moses had predicted, as soon as the hail and thunder ceased,
Pharaoh and his servants resumed their resistance (Ex. 9:34).

Their theology acknowledged the possibility of the appearance of
a powerful god to challenge Egypt. Any war between states in the
ancient world was believed to involve the gods of both nations. The
idea that God might own fart of the world, or might legitimately

2, On the rise of revolutionary socialism and equally revolutionary nationalism
=both philosophies of State sovereignty—as religious and extensively occult
phenomena, see James Billington, Fire in the Minds of Men: Origins of the Revolutionary
Faith (New York: Basic Books, 1980). I regard this as the single most significant work
of historical scholarship to be published in the United States in the post-World War
Il era. (Solzhenitsyn’s Gulag Archipelago is the most important work overall, in terms
of its impact on the thinking of Western intellectuals, forcing them to confront the
reality of Soviet civilization. It is not, strictly speaking, a work of historical scholar-
ship, since it foregoes the historian’s paraphernalia of foatnotes and extensive
documentation, although I regard the work as accurate historically. The author has
subtitled this work, “An Experiment in Literary Investigation.”)

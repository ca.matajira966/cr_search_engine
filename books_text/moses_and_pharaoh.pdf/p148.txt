130 MOSES AND PHARAOH

No Compromise

God refused to grant Pharaoh anything. He rejected the seem-
ingly reasonable compromises offered to Him by this self-proclaimed
god, this ruler of a supposedly divine State. God hardened the
Pharaoh’s heart, so that he might not capitulate and let the Israelites
journey into the wilderness to sacrifice to God. Pharaoh is the great
historical example in Scripture of Proverbs 21:1: “The king’s heart is
in the hand of the Lorp, as the rivers of water: he turneth it whither-
soever he will.” Yet Pharaoh was totally responsible for his acts. In
great wrath, he challenged God, and God killed the firstborn of
Egypt. Then the Egyptians expelled the Israclites in great fear,
allowing them not merely a week of freedom to sacrifice to God, but
permanent freedom to sacrifice to God. The Egyptians fulfilled
God’s promise to Moses, that the whole nation of Isracl would go
free and claim the land of Canaan (Ex. 3:8). God gave them total
victory, yet Moses had officially requested only, at most, a week's
freedom to sacrifice. However, this sacrifice was comprehensive: every
man, woman, child, and beast had to go three days’ journcy into the
wilderness.

‘The Pharaoh, unwilling to acknowledge the validity of this claim
upon the whole of Israel, because it would have denied absolute sov-
ereignty on his part and on the part of the Egyptian State, resisted
unto death. He could not allow Israel a week of freedom, a week of
rest, a week in which no tokens of subservience to Egypt's: gods
would be adhered to by the Israelites. He realized that all the claims
of absolute authority on the part of Egypt would be refuted by such a
capitulation on his part. Therefore, he hardened his heart, hoping to
preserve the theology of Egypt and the autonomous sovereignty of
man. When the Israelites left triumphantly, he lashed out against
them, in a suicidal attempt to destroy them or drag them back. God
had not compromised with him; the all-or-nothing claim of God
could not be sacrificed by some token acknowledgment of lawful
Egyptian sovereignty.

Pharaoh recognized the nature of God’s total rejection of his
claims to divinity, the absolute denial of the continuity of being. God
had cut away his claim to divinity in front of his people and the kings
of the earth, just as He had promised (Ex. 9:16), Pharaoh preferred
to risk his own death, the destruction of his army, and the captivity
of Egypt, rather than submit meekly to the triumph of the Israelites.
He bet and lost. As such, Pharaoh's experience is archetypal for all

 

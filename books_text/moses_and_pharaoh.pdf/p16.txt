xvi MOSES AND PHARAOH

chipelago? As a form of judgment, this is possible. God used Assyria
and Babylon as rods of iron to bring Israel to repentance. Or will it
be the steady grinding down of freedom by the West’s massive bu-
reaucracies? This was Max Weber’s vision of the future of the West,
and it is not a pretty picture.!? It has also come progressively true
since he wrote his warnings from 1905 to 1920, Or will it be a new so-
ciety based on a religious revival? Nisbet has seen this as a real possi-
bility: “Much more probable, I believe, is the appearance of yet
another full-blown ‘awakening,’ even a major religious reformation.
For some time now we have been witnessing what might properly be
called the beginnings of such a transformation, beginnings which
range from popular to scholarly, from eruptions of fundamentalism,
pentecostalism—and, even within the Jewish, Roman Catholic and
Protestant establishments, millennialism— all the way to what has to
be regarded as a true efflorescence of formal theology.”!3

The time has come for a program of Christian reconstruction.
Something new must replace humanism, from the bottom up, in
every sphere of human existence. The dominion religion must
replace the power religion. Humanism’s world is collapsing, both in-
tellectually and institutionally, and it will drag the compromised
Christian academic world into the abyss with it. That is where they
both belong. Weep not for their passing. And if you happen to spot
some aspect of humanism which is beginning to wobble, take an ap-
propriate action. Push it.

Liberation from the State

The liberation theologians keep appealing to the Book of Exodus
as their very special book. Michael Walzer’s study of Exodus calls
this assertion into question. Walzer’s earlier studies of the Puritan
revolution established him as an authority in the field. His study of
Exodus argues that this story has affected politics in the West,
especially radical politics, for many centuries. But it is a story which
does not fit the model used by liberation theologians, whose enemy is
the free market social order. As he says, the Israelites “were not the
victims of the market but of the state, the absolute monarchy of the
pharaohs. Hence, Samuel’s warning to the elders of Israel against

12. Gary North, “Max Weber: Rationalism, Irrationalism, and the Bureaucratic
Cage,” in North (ed.), Foundations of Christian Scholarship: Essays in the Van Til Perspec-
tive (Vallecito, California: Ross House, 1976).

13. Nisbet, op. cié., p. 357.

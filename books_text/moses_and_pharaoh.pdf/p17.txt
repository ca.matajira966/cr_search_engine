Preface xvii

choosing aking. . . . Egyptian bondage was the bondage of a people
to the arbitrary power of the state.”!+

The misuse of the Exodus story by liberation theologians is
another example of the misuse of the Bible generally to promote
anti-biblical social, political, and economic views. This is why prac-
tical commentaries dealing with specific disciplines are needed. The
Bible still commands great authority, and this public perception of
the Bible’s authority is increasing, especially regarding social issues.
This willingness on the part of social critics to appeal to the Bible is
itself a major break with the recent past, yet a return to a more dis-
tant past,

Prior to 1660, it was common for conservatives and radicals to
appeal to the Bible to defend their visions of a righteous social order.
Almost overnight, in 1660, this appeal to the Bible ended. Defenders
of the free market appealed to logic or experience rather than “debat-
able” religious or moral views.!5 Socialists and reformers also drop-
ped their appeal to the Bible after 1660, again, almost overnight.
Shafarevich writes: “The development of socialist ideas did not
cease, of course. On the contrary, in the seventeenth and eighteenth
centuries, socialist writings literally flooded Europe. But these ideas
were produced by different circumstances and by men of a different
mentality. The preacher and the wandering Apostle gave way to a
publicist and philosopher. Religious exaltation and references to
revelation were replaced by appeals to reason. The literature of so-
cialism acquired a purely secular and rationalistic character; new
means of poularization were devised: works on this theme now fre-
quently appear under the guise of voyages to unknown lands, inter-
larded with frivolous episodes.”1¢

The Exodus was a time of liberation — liberation from the statist
social order that had been created by adherents of the power
religion. The spiritual heirs of those statist Egyptians are now com-
ing before the spiritual heirs of the Israelites with a new claim: the
need to be liberated from the institutions of the once-Christian West.
They offer chains in the name of liberation, bureaucracy in the name
of individual freedom, and central economic planning in the name of

14. Michael Walzer, Exodus and Revolution (New York: Basic Books, 1985), p. 30.

15. William Letwin, The Origins of Scientific Economics (Garden City, New York:
Anchor, 1965), ch. 6.

16. Igor Shafarevich, The Socialist Phenomenon (New York: Harper & Row, [1975]
1980), pp. 80-81.

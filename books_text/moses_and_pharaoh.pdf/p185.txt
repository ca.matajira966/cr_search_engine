Continuity and Revolution 167

against God’s church (Rev. 20:9). The very essence of leaven, its ability to
spread through the dough, will be removed from Satan, He will be like un-
leavened dough, fit only for burning, cut down in God's final discon-
tinuous-event, just as Pharaoh was cut down. The “leaven of Egypt”
will be purged out, finally, at the end of time. It will be leaven which
can no longer do its work. It is finally made useless, like savorless
salt, fit only for being ground underfoot (Matt. 5:13). Satan’s leaven
is purged at the end of time when the leaven of God's finished loaf
has fully matured, Christ’s leaven will have done its work. The fire of the
last day bakes this bread, for it is ready for the oven, but Satan’s par-
tial leaven’ is left on the altar forever, never fit for consumption,
never fit for God’s blessed communion feast. Satan’s cultural leaven
never fully rises, in time and on earth, since his leaven eventually is
replaced by God’s leaven.

2. Living Sacrifices

There may have been an additional reason for prohibiting leaven
from God’s altar. The leaven, until baked, was a living thing. No liv-
ing thing was ever sacrificed on the altar. Animals were killed at the
door of the tabernacle (Lev. 1:3). Then they were brought to the altar
for burning. In the case of Satan and his followers, they will be
placed in the lake of fire only after they have been slain (Rev, 20:14).
This burning is referred to as the second.death, No living being was
to be burned on the altar, according to Old Testament law. ©

The one legitimate exception in history was Jesus Christ. As a
perfect creation, a perfect human who had fulfilled the terms of the
law, Christ was allowed to become a living sacrifice. God accepted
this living sacrifice as a substitute, No other living being was suitable. All
other beings are subject to death. Christ was not, yet He gave up His
life for His friends (John 15:13). The sacrificial animals were cut
down in the prime of life, but they all faced death eventually. Jesus
Christ was cut down in the prime of life when, in terms of His per-
fect fulfilling of the law, He had not been faced with death.

Christ was a leavened offering — an ethically fully developed offer-
ing—on the symbolic altar, the cross. Christ was a living sacrifice,
too. In neither case was He violating the laws of the offering. He was
instead fulfilling them. The leaven offering (unbaked leavened
loaves) and the final baked leavened loaves were not to be burned,
but Christ, as a Living man, and as a fully developed perfect humanity, did
die on God’s altar, Christ, being perfect, was God’s own leavened, liv-

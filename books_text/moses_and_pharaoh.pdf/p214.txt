196 MOSES AND PHARAOH

The universality of God’s law implies the universal responsibility of all
men to obey His law. Both principles are based on the idea of the total
sovereignty of God. By establishing a unified law code for all residents
of Israel, God thereby announced His kingdom. No one was ex-
empt. It was not a person's participation in family rites, as in Greece
and Rome, which opened the courts of law to him—courts to which
strangers could therefore not appeal with hope of receiving justice.
The principle of justice for all is based on the principle of God’s final
judgment on all. This was scen first within the geographical confines of
Israel, and all nations were to stand in awe of the Icgal system built
on the principle of justice for all (Deut. 4:5-8). The biblical principle
is clear: one God, one law-order, Deny the universality of a single law-
order, and you thereby deny a universal God. This is precisely what
ancient paganism denied.

Pagan Citizenship

One God, one law-order: here is a principle that stood in stark
contrast to the law structures of the ancient world. The pagan
kingdoms of the ancient world made it exceedingly difficult for
foreigners to gain citizenship, for this meant the right to participate
i ious rites of the city. Religious exclusion meant political exclu-
sion. It also meant exclusion from courts of law. It meant, ulti-
mately, exclusion from justice. Polytheislic societics recognized the
biblical principle in its reverse form: many gods, many law-orders. They
understood that they could have no legal standing in another city’s
courts, for the same reason thal forcigners could possess no Icgal
standing in theirs: they worshipped different gods.

Let us consider the “democratic” city-states of the classical world,
since they represent the “best case” of ancient pagan politics. Fustel
de Coulanges’ book, The Ancient City (1864), remains the classic in the
field. He wrote about the link between classical (Greek and Roman)
religion and politics. Religion and politics were inseparably linked.
Because classical religion was essentially initiatory and mystery-
oriented, politics was equally based on secrecy and participation in
closed rites. Unlike the closed rite of the Passover, however, classical
religion and politics were closed rites based on blood lines, meaning
family lines. “As law was a part of religion, it participated in the
mysterious character of all this religion of the cities. The legal for-
mulas, like those of religion, were kept secret. They were concealed
from the stranger, and even from the plebeian. This was not because

 

 

      

 

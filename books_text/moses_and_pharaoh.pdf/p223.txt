The Rule of Law 205

parasitic, uncreative, and stifling. It destroys self-government.

The biblical form of government is a system’ of multiple
sovereignties (“authorities” we read in Romans 33:1), with multiple
hierarchies (the appeals court structures), and with none universally
sovereign over all other human institutions. This is a system of
decentralized government, competing institutional sovercignties,
and limited civil government. It is a system of government which re-
jects absolute human sovereignty. It recognizes the implicit total de-
pravity of man—the definitive depravity of man apart from God’s
common or special grace— and therefore the explicit total depravity of
any absolutely sovereign human institution—assuming that any in-
stitution could ever be free of God’s restraints, which is not possible.

The basic and absolutely indispensable form of social discipline is
the preaching of the whole counsel af God. The church must do this, the
civil government must proclaim biblical civil law, and the fathers in
all the families should proclaim it. Zt is self-government under biblical law
which is the primary means of attaining social order, This does not deny the
need for appeals courts, but it places such courts in their proper
perspective. The individual has the greatest responsibility for con-
forming to God's law, since the individual must give an account of
bis actions, thoughts, and words on the day of judgment (Matt.
12:36; Rom. 14:12). God polices everything and judges everything.
He provides perfect justice and perfect punishment. There is no
escape. Since the punishment is individual (Luke 12:47-48), and the
rewards are individual (T Gor, 3:11-15), the primary agent of earthly
law enforcement is the individual. No one else has comparable
knowledge of his own actions. No other earthly authority has com-
parable incentives to conform a man’s actions to the standards
presented in God’s law. The incentive system described by God in His
word makes it plain that the most important agency of government is the in-
dividual.

For the individual to exercise self-government as required by
biblical law, he must be aware of the terms of the law. He must un-
derstand what his responsibilities are before God. This is why every
human institution ought to proclaim biblical law as it applics to that
particular institution. Men should be openly confronted with
biblical law, from morning to night to morning, in every sphere of
life, for each man is responsible to God, and in some cases to his
fellow men, from morning to night to morning, in every sphere of
life. There is no neutral zone which is free from the requirements of God's law,

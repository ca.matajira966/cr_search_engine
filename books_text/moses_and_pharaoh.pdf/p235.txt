The Rule of Law 217

nomic freedom? Tyrrell’s answer is incisive: both the socialist and
the reactionary conservative share a hatred for the present. “Today’s so-
cialist is not greatly different, in truth, from the reactionary. The lat-
ter idealizes a past that never was. The former idealizes a future that
never will be. Both have an unscotchable and irrational yearning to
escape the present or to destroy it.”#* Equally incisive is Clarence
Carson’s observation that European conservatives and socialists
share a common view of the State, that of a substitute father. The
American tradition was originally very different. Not only did the
Founding Fathers scparate church from State at the Federal level,
they also separated parenthood from State.*®

“Ethical” Critiques of the Market

The standard—indeed, nearly inevitable—criticism of the free
market which is made by socialist and reactionary critics is that
while the free market provides us with inexpensive goods and ser-
vices, it nonetheless caters to those who have money to spend. Origi-
nally, socialists claimed that socialist economic planning is more
efficient than decentralized, individualistic market planning. From
Marx to the Fabians in Britain, this was their belief. As late as 1949,
a British promoter of socialist planning could write of the British ex-
periment in nationalization of industry: “Here at last a practical test
of two vast and so far unproven assumptions is taking place. The
first is that a planned socialist system is economically more efficient
than a private-enterprise system; the second is that within demo-
cratic socialist planning the individual can be given broader social
justice, greater security, and more complete freedom than under
capitalism.”* By the 1970's, the proponents of democratic socialism
had abandoned the first assumption as erroneous, or at the very
least, still unproven. The socialist economies had all failed the test of
efficiency in the post-War world. Socialist scholars have grudgingly

 

short-run objectives; the Social Democratic Party continued to grow. In 1890, when
Bismarek proposed the desperation policy of abrogating the constitution, shrinking
the franchise, and driving the Social Democrats out of existence, the new emperor,
William II, threw Bismarck out of office: Geoffrey Barraclough, The Origins of
Modern Germany (New York: Capricorn, {1946] 1963), pp. 426-27.

44. R. Emmett Tyrrell, Jr., The Liberal Crack-Up (New York: Simon & Schuster,
1984), p. 2. :

45, Clarence B. Carson, The World in the Grip of an Idea (New Rochelle, New
York: Arlington House, 1979), p. 289.

46. Francis Williams, Sucialist Britain (New York: Viking, 1949), p. 5.

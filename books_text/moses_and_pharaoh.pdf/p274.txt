256 MOSES AND PIIARAOH

recruits.) The King James Version says that they went up “har-
nessed” out of the land of Egypt, but a better translation is provided
by the Berkeley Version: “In martial order the Israelites went up out
of the land of Egypt” (Ex. 13:18b). Cassuto translates it: “in orderly
array.”! James Jordan comments: “When Israel marched out of
Egypt, she marched five in a rank, The term translated ‘battle array’
in Exodus 13:18 actually means ‘five in a rank’ (cf. also Josh. 1:14;
4:12; Jud. 7:11; Num. 32:17).”2 God was treating them as recruits are
treated in basic training: marching them im an orderly fashion,
under the leadership of a drill instructor. ‘They were to learn the art
of war through the discipline of enforced marching, visible leader-
ship, and a carefully staged “obstacle course.” That obstacle course
was the Red Sea. Later, their obstacle course was the wilderness.

Three Types of Slavery

Earthly slavery, as manifested clearly in the history of the Ex-
odus, involves at least three factors: slavery to food, slavery to the
past, and slavery to the present. The Hebrews cursed Moses, for he
had served them as a deliverer, He had enabled them to cast off the
chains of bondage. They looked to the uncertainty that lay before
them (the Red Sea) and the chariots behind them, and they wailed.
They had lost what they regarded as external security in Egypt, a
welfare State existence, and they resented Moses’ leadership. They
had no confidence in God and God’s promised future. Looking
resolutely over their shoulders, they stumbled at the very border of
freedom, the Red Sea. Fearing death in freedom, this generation
died in the wilderness. Better to serve in Egypt than die in the
desert, they proclaimed. God then proved to all but Caleb and
Joshua, who never believed in this slave slogan, that it was much
better to die in the wilderness than to serve in Egypt. Best of all, God
showed the next generation, was the conquest of Canaan. But the
Israelites of Moses’ day could not sec that far into the future; they
saw only the past and the present—a present bounded, they be-
lieved, by Pharaoh’s army and the Red Sea. What they saw as the
end of their lives was in fact only an obstacle course.

Slaves to food. Once they had been delivered from their Egyptian
enemies, they were determined to keep their heads riveted to the

 

 

1. U. Gassuto, A Commentary on the Book of Exodus, trans, Israel Abrahams (Jeru-
salem: Magnes Press, Hebrew University, [1951] 1974), pp. 156-57.
2. James Jordan, The Geneva Papers, No. 25 (Feb. 1984), [p. 3].

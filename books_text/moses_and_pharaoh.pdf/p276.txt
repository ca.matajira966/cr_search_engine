258 MOSES AND PHARAOH

Manna was a special kind of food. It was provided for them
supernaturally, It was scheduled to appear predictably. It was also
exceedingly plain—the kind of food which is provided to an army in the
field, In World War II, the all-to-familiar canned C-rations became
the objects of scorn and jokes, just so long as they were delivered on
time. The U.S. Army was the best-fed army in history up to that
time, but the boring food, especially the canned food that was the
common fare when men were in the battlefield, was resented by the
troops, unless a crisis occurred which cut them off from new sup-
plies.* Israel.resented manna, for the Israelites did not see them-
selves as an army on a battlefield. Their memories of special occa-
sional meals in Egypt chased away their memories of bondage. They
had developed a taste for slavery, literally.

Slaves to the past. How could they have forgotten the grinding
bondage of their former state? Could they learn nothing from
history? In a word, no. Historiography is an intellectual reconstruc-
tion which man attempts, and he hopes that his reconstruction
resembles the actual events of the past, But being an intellectual re-
construction, it is dependent upon a philosophical foundation for ac-
curate interpretation. The events of history teach nothing to those
who are blind to God's sovereignty over these events. Or more ac-
curately, their interpretation teaches something that is inaccurate.>

The Israelites told Moses that death in Egypt would have been
preferable to the responsibilities of godly liberty. The pain of dying
would have been over by now, they complained. Wouldn’t the graves
of Egypt have been preferable (Ex. 14:11)? They faced the responsibili-
ties of life, and they grumbled. They were blinded by a false vision of
the past. They preferred to live in terms of historical fantasy rather
than face the realities of life. They preferred to remember their bond-
age in Egypt as a golden age by comparison, thereby trading the op-
portunities of the present for the imaginary comforts of the past. Is it
any wonder that God imposed upon them the ritual observance of the
Passover? Mouthing words that they did not really understand, the
fathers of that generation transmitted a true vision of the past to their
children. The mandatory bitter herbs were educational.

4. The U.S. soldier was also the most heavily laden foot soldier in history: 84
pounds of equipment. William Manchester, The Glory and the Dream (Boston: Little,
Brown, 1974), p. 281.

5, C. Gregg Singer, “The Problem of Historical Interpretation,” in Gary North
(ed.), Foundations of Christian Scholarship: Essays in the Van Til Perspective (Vallecito,
California: Ross House Bouks, 1976), ch. 4.

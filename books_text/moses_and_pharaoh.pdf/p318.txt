300 MOSES AND PHARAOH

40 years (I Ki. 11:42). Thus, Solomon came to the throne around 971
s.c.’ He therefore began construction of the temple around 967/66,
8.c. the fourth year of his reign.®

The date of the Exodus therefore must be set within the narrow
framework of about 1500 3.c, to 1440 B.c., according to I Kings 6:1.
There is no escape from this biblically, unless the chronology of the
later Hebrew kings is somehow shortened through more accurate
research. Such research has not been provided by anyone, nor is it
likely to appear. Nevertheless, the mid-fifteenth century dating of
the Exodus is frequently rejected by conservative Bible scholars.
Why? Because they key their dating of the Exodus to the known (and
exceptionally vague) historical records of Egypt. Why do they do
this? Because secular scholars do, and the Christians have followed
their lead.

Conservatives and Compromise

A representative summary of the dating problem is found in the
International Standard Bible Encyclopedia (1982): “The date of the Ex-
odus is one of the most debated topics in OT studies because of the
ambiguous nature of the evidence. Although the biblical texts seem
to require a date in the middle of the 15th cent. a.c., archeological
evidence seems to point to a date in the 13th cent. B.c.”° The author,
W. H. Shea, then goes on for eight two-column, small-print pages
summarizing bits and pieces of conventional Egyptian chronology
and archaeology. He wants to hold to the fifteenth-century dating,
but his defense is weakened because of his presuppositions concern-
ing methodology. His methodology is based on comparative
chronology and comparative archaeology. This, we are supposed to
believe, is the objective, neutral scholarship we need in order to
make sense out of the Bible.

He affirms that the mid-fifteenth century is “the only date given
for it in the Bible.” But consider his reliance upon the category of
pragmatism in defending the conservative view: “While it is possible
that these [biblical] data could have been corrupted in transmission,

7, Merrill F. Unger, Unger Bible Handbook (Chicago, Illinois: Moody Press,
1967), p. 12.

8, Cf. Merrill C. Tenney (ed,), The Zondervan Pictorial Encyclopedia of the Bible, 5
vols. (Grand Rapids; Michigan: Zondervan, 1975), V, p. 627.

9, “Exodus, Date of the,” Intemational Standard Bible Encyclopedia, 4 vols. (Grand
Rapids, Michigan: Eerdmans, 1982), II, p. 230.

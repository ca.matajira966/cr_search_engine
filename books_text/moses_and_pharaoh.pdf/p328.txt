310

Exodus 7:21 And the fish that was
in the river died.

Exodus 10:15 . . . there remained
not any green thing in the trees,
or in the herbs of the fields,
through all the land of Egypt.

Exodus 9:3 . . . the hand of the
Lord is upon thy cattle which is in
the field . . . there shall be a very
grievous murrain.

Exodus 9:19 . . . gather thy cattle,
and all that thou hast in the field.
Exodus 9:21 And he that regarded
not the word of the Lord left his
servants and his cattle in the field.

Exodus 10:22 . . . and there was
a thick darkness in all the land of

Egypt.

Exodus 12:29 And it came to
pass, that at midnight the Lord
smote all the firstborn in the land
of Egypt, from the firstborn of
Pharoah that sat on his throne un-
to the firstborn of the captive that
was in the dungeon.

Exodus 12:30 . . . there was not a
house where there was not one
dead.

Exodus 12:30. . . there was a
great cry in Egypt.

Exodus 13:21... by day ina
pillar of a cloud, to lead them the
way; and by night in a pillar of
fire, to give them light; to go by
day and night.

MOSES AND PHARAOH

Papyrus 10:3-6 Lower Egypt weeps
. .. The entire palace is without
revenues. To it belong (by right)
wheat and barley, geese and fish.

Papyrus 6:3 Forsooth, grain has
perished on every side.

Papyrus 5:12 Forsooth, that has
perished which yesterday was seen.
The land is left over to its
weariness like the cutting of flax.

Papyrus 5:5 All animals, their
hearts weep, Cattle moan, . .

Papyrus 9:2-3 Behold, cattle are
left to stray, and there is none to
gather them together. Each man

fetches for himself those that are

branded with his name.

Papyrus 9:11 The land is not
light... .

Papyrus 5:3; 5:6 Forsooth, the
children of princes are dashed
against the walls.

Papyrus 6:12 Forsooth, the chil-
dren of princes are cast out in the
streets.

 

Papyrus 2:13 He who places his
brother in the ground is
everywhere.

Papyrus 3:14 It is groaning that
is throughout the land, mingled
with lamentations.

Papyrus 7:4 Behold, the fire has
mounted up on high. Its burning
goes forth against the enemies of
the land.

 

From the King James Version

From A. Gardiner’ translation of the Leiden
Papyrus. He did not observe the similarities.

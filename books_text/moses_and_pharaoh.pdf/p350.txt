332 MOSES AND PHARAOH

The Lrreplaceable Resource: Time

Simon is a humanist —a very smart humanist — who is seeking to
escape the curse-induced limits to growth. He is attempting to escape the
logic of all growth, for it points to a coming judgment and the end of time. A
one per cent per year expansion of today’s human population will
produce over 80 trillion people in a thousand years. There are, in
short, limits to growth. There is finitude. We are not God; we are
limited creatures. Our creativity is the creativity of creatures, a kind
of “re-creativity.”

The Bible says that the primary limit in the post-Fall world is
time: a final judgment is coming. Simon categorically and foolishly
denies this. Speaking of the increase of total resources over time —
the product of superior insight, better technology, and capital accu-
mulation —he writes: “But, you ask, how long can this go on? Surely
it can’t go on forever, can it? In fact there is no logical or physical
reason why the process cannot do just that, go on forever.”!* In this
sense, Simon is “whisding past the graveyard” — the entropy-bound
cosmic graveyard, The process cannot go on forever, or anything
like forever. The universe is bounded. Furthermore, this earth is
bounded, and even 1% per annum growth in the world’s population
will press against these limits within a few generations. Eventually,
population growth will end, thereby fulfilling one aspect of the do-
minion covenant. Other forms of growth will also end,

We have to recognize that Simon’s book is an intellectual over-
reaction. Nevertheless, his arguments are correct within the God-
imposed and (humanly speaking) indefinite limitations of the creation. We do
not live in an infinite environment, but we do live in an indginitely
kimited environment. It is not infinite, but its boundaries cannot be
known by a government committee. There are limits on men’s crea-
tivity, but men do not know where these limits are. God does know,
and therefore it is incorrect to deny the limitations of finitude. On
the other hand, a State bureaucracy does not know, and therefore it
is misleading (and State-enhancing) to speak of the need for limiting
growth by political action.

 

Biblical Ethics vs. Stagnation

The answer, then, is to allow men’s creativity to flow, and to
allow profit-seeking investors to seek out previously undetected op-

14, Ibid., p. 217.

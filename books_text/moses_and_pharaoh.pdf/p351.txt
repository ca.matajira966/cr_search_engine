The Demographics of Decline 333

portunities. It is this fusion of inventive genius and private capital
accumulation and investment which is basic to the institutional
framework of the growth proccss. But most important of all is the
ethical framework, which in turn is the source of ihe institutional
framework, Christian economic and social analysis must postulate a
relatively close relationship in history between ethics and economic
performance. First, there is a relationship between external righteous-
ness and external blessings. Vhis includes population growth. Sec-
ond, we must never forget the relationship between redellion and
stagnation or even “negative income,” as the economists like to put it,
i,e., between evil acts and falling per capita income for a society.
What we should arguc, contrary to Simon, is that there are several
“ultimate resources”: 1) God’s gift of life in the creation; 2) His gift to
the creation of an assistant made in His image, man, who is subor-
dinately creative (Simon’s “ultimate resource”); 3) His gift of land
(natural resources) — the creation itself, 4) His gift of time; 5) His gift
of law; and 6) His gift of regeneration and sanctification to fallen
humanity,

Simon's thesis, therefore, is flawed by his humanism. Never-
theless, his thesis is not nearly so fawed as his humanist opponents’

 

 

theory, that is, that compound economic growth is not the proper
standard, but is instead some sort of cosmic hubris on the part of
man, his defiance of the laws of an entropic cosmos,'5 They assume
that finitude is primary rather than ethics, that entropy is the fun-
damental reality rather than regeneration, sanctification, and bless
ing. His opponents assume that capitalism is evil because it provides
the legal framework for long-term economic growth, and thereby en-
courages such growth. Capitalism does precisely that, of course, but
the Christian response should be that this is one of the reasons why
capitalism is a God-ordained and Cod-required form of economic
organization, Il is not capitalism which is innately evil, but rather
the zero-growth ideology.

 

   

 

 

The Legacy of Malthus

Some have termed the fear of population growth “neo-
Malthusianism.”!© Thomas. Malthus, a late-eighteenth-century

15. Jeremy Rifkin, Entropy: A New World View (New York: Bantam, 1980).
16. For example, B. Bruce-Briggs, “Against the Neo-Malthusians,” Commentary
(July 1974)

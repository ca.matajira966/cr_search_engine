The Demographics of Decline 335

totally at odds with his.2+

The influence of Malthus in discussions of population theory has
been enormous. Independently, both Darwin and A. R. Wallace
came to their theory of “evolution through natura} selection” by
reading Malthus’ insight that populations are constantly pressing
against the means of subsistence.?5 In economics, with the exception
of Marx,?6 the Malthusian perspective led to the “dismal science” (as
Carlyle called it), Classical economic theory of the first half of the
nineteenth century was firmly grounded on the so-called “iron law of
wages,” a corollary to the law of diminishing returns — the belief that
the most productive land would be put into production first, and that
the demand for food created by an increasing population would be
satisfied only at greater and greater cost, as less and less productive
land was brought into production.?” This was Ricardo’s intellectual
legacy, built on Malthus’ earlier population theory.

Readers should be aware that really scientific studies of popula-
tion came only in the late nineteenth century.” The topic was almost
never mentioned in English-language history textbooks until after
World War II, if then, and really not until the mid-1950's.2? The
scientific study of historical population trends is equally recent. The
French have been the pioneers here, yet the discipline of historical
demography began no earlier than the carly 1950’s.4°

Since the period after 1960, the neo-Malthusians have dominated
the popular press and media. This, too, shall pass. Public opinion
concerning the appropriate population growth rate, like the growth

24, Petersen, pp. 150-52.

25. Charles Darwin and Alired Russel Wallace, The Journal of the Linnean Society
(1858); reprinted in Philip Appleman (ed.), Darwin: A Norton Gritical Edition (New
York: Norton, 1970), p. 83; Wallace, “Note on the passages of Malthus’s ‘Principles
of Population’ which suggested the idea of natural selection to Darwin and myself,”
The Darwin and Wallace Celebration held on Thursday, 1 July 1908 ky the Linnean Sociely of
London (London, 1908), pp. 111-18, cited by Sir Gavin de Beer, ibid., p. Tin. See also
Wallace's reminiscences at age 75 in The Wonderful Century; cited by Arnold C.
Brackman, A Delicate Arrangement: The Strange Case of Charles Darwin and Alfred Russel
Wallace (New York: Times Books, 1980), p. 199. For extracts’ of the writings of both
Darwin and Wallace concerning Malthus’ impact on their thinking, see Flew, op.
cit., pp. 49-51.

26. See Flew’s extracts from Marx and Engels, op. cit., pp. 51-52.

27. E. P. Hutchinson, The Population Debate: The Development of Conflicting Theories
up to 1800 (Boston: Houghton Mifflin, 1967), esp. p. 395.

28. David Landes, “The Treatment of Population in History Textbooks,”
Daedalus (Spring 1968), p. 364. This issue was titled, Historical Population Studies,

29. Ibid., pp. 372-78.

30, Louis Henry, “Historical Demography,” itid., pp. 390-91.

 

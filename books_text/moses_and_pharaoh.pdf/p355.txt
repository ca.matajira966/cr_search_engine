The Demographics of Decline 337

the latter decades of the twentieth century. Sider’s book adopts the
reigning “Malthusianism” and proclaims it in the name of Jesus.

I discuss the book at some length here, but not because it is:im-
portant as a work of scholarship. On the contrary, there is nothing
scholarly about it. David Chilton has disposed of it, line by line.3?
The important fact is its popularity; it has been a very effective piece
of poorly documented propaganda. It has become a kind of “Bible”
for “concerned” Christians and especially college-age Christians. It
has gone through numerous reprints since its publication in 1977. Its
wide acceptance indicates just how intellectually defenseless evangel-
ical Christians have become, especially to charges of “exploitation”
and “insensitivity.” This Eastern Baptist Theological Seminary pro-
fessor is one of the most sought-after speakers on the seminary cir-
cuit. He has even debated the nuclear freeze issue with fundamen-
talist celebrity Jerry Falwell on Cable News Network (CNN).33 He
is, in short, a Big Name Theologian.

The book calls for a new “food policy.” (In the twentieth century,
whenever you read the word “policy,” you should mentally substitute
the words “political program.”} North America is criticized for having
produced food in abundance. This agricultural productivity is dis-
cussed as if it were some sort of crime against humanity. And criminals
must make restitution. North Americans eat lots of food. Third World
residents sometimes starve. The connection is obvious to Sider. Anyone
should be able to see this. In effect, North Americans are cannibals. We are
eating our global neighbors, taking food right out of their mouths, con-
suming them in a carniverous orgy of protein consumption. Writes
Sider: “The U.S. Department of Agriculture reports that when the total
life of the animal is considered, each pound of edible beef represents
seven pounds of grain. That means that in addition to all the grass,
hay and other food involved, it also took seven pounds of grain to
produce a typical pound of beef purchased in the supermarket. For-
tunately, the conversion rates for chicken and pork are lower: two or
three to one for chicken and three or four to one for pork. Beef is the
cadillac of meat products. Should we move to compacts?”3+

32. Productive Christians in an Age of Cuilt-Manipulators (3rd ed.; Tyler, Texas: In-
stitute for Christian Economics, 1985).

33. This debate appeared on the Sandy Freeman Show. Miss Freeman is proba-
bly the most thoughtful and effective interviewer on American television. For a cri-
tique of Sider and others on the nuclear disarmament issue, see Franky Schaeffer,
Bad News for Moden Man, ch. 3: “Peace Now!”

34, Sider, Rick Christians (1977), p. 43; (1984), pp. 34-35.

344 MOSES AND PHARAOH

nations have also erected immigration barriers against newcomers
who might at least be able to increase the size of the national popula-
tions sufficiently to maintain them in the long run.

In 1973, about a decade after the “population dilemma” propa-
ganda began,°* the Supreme Court of the United States overturned
all state laws that outlawed “abortion on demand” in the Ree v. Wade
decision. Within a few years, between a million and a million and a
half now-legal abortions were being performed in the United States.
Pro-abortionists offer a counter-argument: that there were as many
as a million illegal abortions in 1960.57 Another estimate of the com-
bined legal and illegal abortions in the U.S. in 1972, a year before
Rae v. Wade, is 1.25 million.*8 A less radical estimate is 587,000 abor-
tions in 1972.59 The number, obviously, was high. But the number of
abortions increased after Roe v. Wade, The Ree v. Wade decision, how-
ever, has led not only to a vast number of abortions but also to a
mobilization of Christians and conservatives in opposition. By
1976, the number of legal abortions performed by physicians in the
United States exceeded the number of tonsillectomies as the most

 

had fallen to 1.8. See Population Estimates and Projections: Estimates of the Poputation of the
United States and Components of Change: 1930-1975, Series P-25, No. 632 (July 1976), p.
2; published by the U.S. Department of Commerce, Bureau of the Census.

56. Philip M. Hauser, The Population Dilemma (Englewood Cliffs, New Jersey:
Prentice-Hall, 1963), copyright by the American Assembly, for whom it was compiled.
‘Trustees of the Assembly included former President Dwight Eisenhower, former
Federal Reserve Board Chairman (under President Roosevelt) Mariner S. Eccles,
W. Averill Harriman, and Henry M. Wriston (later president of the second-largest
bank holding company in the U.S., Citicorp).

57. Garrett Hardin, Mandatory Motherhood, p. 11.

58. Helen Dudar, “Abortion for the Asking,” Saturday Review (April 1973), p. 34.
The “teaser” copy which introduces the article reads: “Ie still not the same as having
a tooth pulled, yet few tears are shed?

59. ‘Another Storm Brewing Over Abortion,” U.S. News and World Report (July
24, 1978).

60. Cf. Franky Schaeffer, A Time for Anger: The Myth of Neutrality (Westchester, 2
linois: Crossway Books, 1982), ch. 6. The success of Francis Schaeffer’s A Christian
Manifesto (Crossway, 1980) and the moderate success of Franky Schaeffer's anti-
abortion movie and his father’s book, co-authored by Dr. C. Everett Koop, both
bearing the title, What Ever Happened to the Human Race? (Old Tappan, New Jersey:
Revell, 1976), led to the appointment in 1981 of Dr. Koop as Surgeon General of the
United States. This symbolic appointment demonstrated that the Christians had at-
tained at least some degree of influence in national politics by means of this topic, It
was the only major appointment during President Reagan’s first term that the Chris-
tians received. An anti-abortion book bearing President Reagan’s name, Abortion and
the Conscience of the Nation, was released in early 1984, a Presidential election year, by
Thomas Nelson Sons, a religious publisher.

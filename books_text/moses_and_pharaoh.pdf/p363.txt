The Demographics of Decline 345

frequently performed surgical procedure.* The number of abor-
tions performed annually finally peaked (possibly only briefly) in
1982.

The speed of the transformation of people’s thinking was
remarkable. In the carly 1960's, the American public favored the
right of a woman to elect to have an abortion if the unborn baby was
known to be “defective.” Still, only ten percent approved of abortion.
on demand simply on request of the woman.® By 1972, a Gallup
Poll showed that 65% of Protestants and 56% of Roman Catholics
answered “yes” to this question: “Do you agree that the decision to
have an abortion should be made solely by a woman and her physi-
cian?”6?

A grim reminder of the judgment which may be in store for
today’s aborting societies is the fact that Germany was the first mod-
ern Western nation to maintain a policy of mass abortions. This cam-
paign to legalize abortion began prior to the coming to power of the
Nazis, but it was under the Nazis that a full-scale policy of legalized
abortion began. The parailels between Nazi Germany's disrespect for
life and the West's disregard for the unborn are chronicled by William
Brennan in two books, neither of which is pleasant to read.

Abortion in the Soviet Union

It is also interesting that in 1965—precisely the same time that
the “population explosion” propaganda began in the West —a debate
began on this topic within the Soyict Union. The official Soviet
Marxist line had been that there could never be overpopulation in a
Marxist uation. [t was the West which worricd about overpopulation
because of the inability of capitalism to produce sufficient food and
consumer goods. Soviet Premier Khrushchev had stated the “hard
line” in a 1955 speech: “The more people we have, the stronger our
country will be. Bourgeois ideologists have invented many can-
nibalistic theories, including the theory of overpopulation. They
think about how to reduce the birth rate and the growth of popula-

61, Sullivan, Ticize, and Dryfoos, “Legal Abortion in the United States,
1975-76," Family Planning (May/June 1977), p. 316; cited by William Brennan,
Medical Holocausts (New York: Nordland, 1980), I, p. 322.

62. Hardin, Mandatory Motherhood, p. 71

63. ibid, p. 7.

64. Brennan, Medica Holocausts, of. cit., and The Abortion Holocaust, Today's Final
Solution (St. Louis, Missouri: Landmark Press, 1983).

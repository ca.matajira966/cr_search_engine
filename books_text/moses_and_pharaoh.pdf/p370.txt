352 MOSES AND PHARAOH

In a remarkable abandonment of both logic and morality, he
then accuses the Roman Catholic position of irresponsibility, He
summarizes the implications of the position: “A fetus, once conceived,
has the right to develop; this is an expression of natural forces and is a
duty allotted to the mother by nature. Taken to its logical conclusion,
this leaves no reom for human responsibility. Instead, the erratic and
impersonal forces of the natural environment are allowed sway. I do
not consider this accords with the biblical emphasis on the responsibil-
ity God has bestowed upon mankind to control our environment,”!¢

Raising the issue of natural law theory at this point in his argu-
ment is a verbal smoke screen, a cheap debate trick. By misdirecting
the attention of his readers to a false issue, “impersonal natural law,”
he would have them overlook the obvious issue, namely, the prohibi-
fon against murder in biblical law. Jones spends page after page in
Chapter 1 to demonstrate that the Bible teachcs that all created real-
ity is intensely personal, because God, the Creator, is personal. He
denies any impersonality in the universe. “The world God has made
is intrinsically personal.”!°> Then he uses a weakness in Roman
Catholic epistemology—natural law theory, which is ultimately
impersonal—to undermine a great strength in Roman Catholic
ethics: the defense of human life.

He argues, incredibly, that by teaching people never to abort the
unborn, the Roman Catholic Church has removed the question of
abortion from the realm of ethics. On the contrary, the Church has
reaffirmed the ethical decision. It is not a question of “to abort or not
to abort under which complex, difficult, dilemma-filled situations?”
It is a question of “to abort or not to abort, under any situation?” The
Church has quite properly called abominable the position defended
by D. Gareth Jones, Ph.D., and he feels the heat.

He says that “Abortion for therapeutic reasons demands a serious
response by those professing to follow Christ.”!9° Indeed, it does.
The serious response is: “Don’t.” The serious reason is: “God says
not to.”

The “Potential for Personkood”

He says that “each fetus is a human life, representing a potential
for personhood from very early in its development. From this early

104. Idem.
105. Ibid., p. 20.
106. Ibid., p. 183

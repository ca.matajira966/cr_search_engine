The Demographics of Decline 355

He is just not certain.

Then what are the grounds of decision-making? By what measure
do we “weigh” the issue of abortion? Not the Bible. Not God’s inscrip-
turated word. No; the key issue is that slickest of slick slogans of the
late 1960's: the quality of life, He tells us this at the beginning of the
book. “Biology is power over the living world, and biomedicine is
power over human nature. ‘There are numerous consequences of this,
and they are already the subject of daily decision-making. These deci-
sions revolve around one crucial issuc, namely, the quality of life we
demand for the populations of technologically-advanced societies and
for individuals within those societies. All other issues, whether at the
commencement of life or at the end of life, revolve around this critical
fulcrum.”!!5 Therefore, “In making the decision, a balance needs to be
attained between the pursuit of biological quality and the potential
that a deformed child within’a family holds out for that family to be
bumanized and to grow as a loving, human unit.”!!¢ In’ short, more lov-
ing through chemical abortion. Or to reverse a 1960's advertising slogan of
the Monsanto Chemical Company, “Better Dying Through
Chemistry.” As Jones concludes: “Unfortunately some families cannot
cope with such a challenge, and a compromise must be reluctantly
adopted, namely, termination of the pregnancy.”!!7

Such is the quality of life when it is not defined by the Bible, in
terms of the ethics of the Bible. Such is the acsthetics of the self-
professed autonomous man.

Humanism’s Ethics of Sentimentality

The compromise must be adopted, the ethicist tells us, just so
jong as the decision is made reluctantly. This is the ethics of sentimen-
tality, as Schlossberg has calledit. “If good and evil are purely a mat-
ter of sentiment, then no action can be judged, since sentiments re-
main opaque to outside certification. Only the motives counts, not
the action. In this way sentiment, not reason or law, is déterminative
of right. and wrong.”!!8 Schlossberg has identified the source of the

 

ethical system allow such specificity, Henry actually had endorsed this book prior to
publication. Known as a Christian conservative, Henry's continual hostility to the
legitimacy of biblical law in New Testament times has finally led him into the picof
confusion and compromise with evil.

115. Jones, p. 10,

16. Thid., p. 179.

117. Idem.

118. Herbert Schlossberg, Idols for Destruction: Christian Faith and Tis Confrontation
With American Society (Nashville, Tennessee: Thomas Nelson Sons, 1983), p. 44.

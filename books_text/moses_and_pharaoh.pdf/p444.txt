426 MOSES AND PHARAQH

We cannot expect to raise money by emotional appeals. We have
no photographs of starving children, no orphanages in Asia. We
generate ideas. There ts always a very limited market for ideas, which is why
some of them have to be subsidized by people who understand the power of
ideas —a limited group, to be sure. John Maynard Keynes, the most in-
fluential economist of this century (which speaks poorly of this cen-
tury), spoke the truth in the final paragraph of his General Theory of
Employment, Interest, and Money (1936):

. .. the ideas of economists and political philosophers, both when they
are right and when they are wrong, are more powerful than is commonly
understood. Indeed, the world is ruled by little else. Practical men, who
believe themselves to be quite exempt from any intellectual influences, are
usually the slaves of some defunct economist. Madmen’‘in authority, who
hear voices in the air, are distilling their frenzy from some academic scrib-
bler of a few years back. I am sure that the power of vested interests is
vastly exaggerated compared with the gradual encroachment of ideas. Not,
indeed, immediately, but after a certain interval; for in the field of eco-
nomic and political philosophy there are not many who are influenced by
new theories after they are twenty-five or thirty years of age, so that the
ideas which civil servants and politicians and even agitators apply to cur-
rent events are not likely to be the newest. But, soon or late, it is ideas, not
vested interests, which are dangerous for good or evil.

Do you believe this? If so, then the program of long-term educa-
tion which the ICE has created should be of considerable interest to
you. What we need are people with a vested interest in ideas, a commit-
ment to principle rather than class position.

There will be few short-term, visible successes for the ICE's pro-
gram. There will be new and interesting books, There will be a con-
stant stream of newsletters. There will be educational audio and
video tapes. But the world is not likely to beat a path to ICE's door,
as long as today’s policies of high taxes and statism have not yet pro-
duced a catastrophe. We are investing in the future, for the far side
of humanism’s economic failure. Thzs is a long-term investment in in-
tellectual capital, Contact us at: ICE, Box 8000, Tyler, TX 75711.

32 MOSES AND PHARAOH

mankind, but he was not of mankind, This is perhaps the most
fitting picture of the good Egyptian ruler, that he was the herdsman
for his people... . The herdsman is primarily the pastor, the
feeder, and a first responsibility of the state was to see that the peo-
ple were fed, Thus the king of Egypt was the god who brought fertil-
ity to Egypt, produced the life-giving waters, and presented the gods
with the sheaf of grain which symbolized abundant food. Indeed, an
essential function of his kingship was that of a medicine man, whose
magic insured good crops. In one of the ceremonials of kingship, the
pharaoh encircled a field four times as a rite of conferring fertility
upon the land.”!+

God blessed Sesostris I through Joseph. The arrogance of power
led Sesostris III, his great-grandson, to enslave the heirs of Joseph.'5
Within a century, Egypt was in ruins, under the domination of
foreign invaders, the Hyksos (Amaiekites). In the light of all this, we
can better appreciate God’s words to the (probable) Pharaoh of the
Exodus, Koncharis: “For now I will stretch out my hand, that I may
smite thee and thy people with pestilence; and thou shalt be cut off
from the earth. And in very deed for this cause have I raised thee up,
for to shew in thee my power; and that my name may be declared
throughout all ‘the earth” (Ex. 9:15-16),

Slavery

The Pharaoh of the enslavement followed a pattern which had
become familiar in the lives of the Hebrews. Like Laban in his deal-
ings with Jacob, and Potiphar in his dealings with Joseph, the
Pharaoh recognized the economic value of the Hebrews. At the same
time, he resented certain concomitant aspects of Hebrew produc-
tivity, in this case, their fertility.'6 Yet he was unwilling to take the
obvious defensive step, namely, to remove them from the land. He
wanted to expropriate their productivity, to compel their service. It
was not enough that they were in Egypt, bringing the land under do-
minion, filling the nation with productive workers. Their productiv-

14, Ibid, pp. 78, 79-80

15. Courville, Exodus Problem, 1, p. 218.

16. “The terms fecundity and fertility, originally used synonymously, were
differentiated from one another only gradually, In 1934 the Population Association
of America officially endorsed the distinction between fecundity, the physiological
ability to reproduce, and fertility, the realization of this potential, the actual birth
performance as measured by the number of offspring.” William Peterson, Population
(and ed.; New York: Macmillan, 1969), p. 173.

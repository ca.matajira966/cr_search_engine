34 MOSES AND PHARAOH

The Pharaoh of Joseph’s day acknowledged Joseph’s access to ac-
curate secret knowledge, and he honored him and his family,
transferring the sovereignty of the State to Joseph. He placed his
own ring on Joseph’s hand, arrayed him in fine linen and gold, and
placed him in the second chariot after his own (Gen, 41:42-43). That
Pharaoh bowed to God’s sovereignty and to God’s dream-mediated
Word, and his kingdom was blessed by Gad.

In contrast, the Pharaoh of the oppression wanted Jacob's heirs
to produce on Egypt’s terms, without the transfer of any of the king’s
sovereignty, but he expected to be able to control and even reduce
that fertility, while appropriating the fruits of their labor. He was
wrong; their fertility continued, and he was forced to attempt the
murder of all the male infants in order to stop this Hebrew popula-
tion explosion (Ex. 1:15-19). He, like the Pharaoh of the Exodus,
found that he could not control God through His people. Laban had
discovered the same thing in his dealings with Jacob.

God’s plan was sovereign over Egyptian history, not the plans of
the pharaohs. The Pharaoh of Joseph’s day had recognized this, and
Egypt had prospered because he was wise enough to transfer the
symbols and prerogatives of State sovereignty to Joseph. His suc-
cessors sought to reassert their self-proclaimed divine sovereignty
over the Hebrews, and the Pharaoh of the Exodus saw Egypt’s
wealth and military power swallowed up.

The Bureaucratic Megamachine

It was not just the Hebrews who were enslaved. Sesostris HI
recentralized the Egyptian social and political order. He began to
construct treasure cities, indicating that he had begun to use tax
revenues in order to strengthen the visible sovereignty of the central
government. Centuries earlier, pharaohs had used State revenues to
construct the giant pyramids— monuments to a theology of death
and resurrection for the Pharaoh (and later, of the nobility) —but the
Pharaoh of the oppression settled for less grandiose displays of his
immediate sovereignty.

By Joseph’s day, the pharaohs no longer built pyramids. The
total centralization of the Pyramid Age had disintegrated. Never-
theless, the theology of the continuity of being was still basic to
Egyptian theology, and the lure of centralized power in the person of
the Pharaoh was still ready to find its political expression. Although
it is true that Joseph had bought all the land of Egypt, excepting only

40 MOSES AND PHARAOH

of men to the care of the gods’ and spirits.”?? In short, Budge implies,
they were all basically Deists when it came to formal theism, and
polytheists when it came to ritual. But ritual was the heart and soul
of Egyptian religion.

Ethics vs. ritual: here is the heart of the difference between the
Egyptians’ religion of death and resurrection and the Hebrews’
religion of death and resurrection. Biblical religion places ethics
above ritual. In the Book of Micah, we read: “Wherewith shall I
come before the Lorn, and bow myself before the high God? Shall I
come before him with burnt offerings, with calves of a year old? Will
the Lorp be pleased with thousands of rams, or with ten thousands
of rivers of oil? Shall I give my firstborn for my transgression, the
fruit of my body for the sin of my soul? He hath shewed thee, O
man, what is good; and what doth the Lorp require of thee, but to
do justly, and to love mercy, and to walk humbly with thy God?”
(Micah 6:6-8). In contrast, consider Budge’s summary of the Egypt-
ians’ concern over resurrection, and their attempt to achieve this ex-
alted state through the manipulation of physical means. Theirs was
a world filled with demons that could be controlled only by magic,
especially word magic. They were obsessed with the physical signis of
death. He writes of the dynastic-era Egyptians that they

attached supreme importance to the preservation and integrity of the dead
body, and they adopted every means known to them to prevent its
dismemberment and decay. They cleansed it and embalmed it with drugs,
spices and balsams; they annointed it with aromatic oils and preservative
fluids; they swathed it in hundreds of yards of linen bandages; and then
they sealed it up in a coffin or sarcophagus, which they laid in a chamber
hewn in the bowels of the mountain. All these things were done to protect
the physical body against damp, dry rot and decay, and against the attacks
af moth, beetles, worms and wild animals. But these were not the only
enemies of the dead against which precautions had to be taken, for both the
mummified body and the spiritual elements which had inhabited it upon
earth had to be protected from a multitude of devils and fiends, and from
the powers of darkness generally. These powers of evil had hideous and ter-
rifying shapes and forms, and their haunts were well known, for they in-
fested the region through which the road of the dead lay when passing from
this world to the Kingdom of Osiris, The “great gods” were afraid of them,
and were obliged to protect themselves by the use of spells and magical
names, and words of power, which were composed and written down by

33. Ibid., pp. xxvili-xxix,

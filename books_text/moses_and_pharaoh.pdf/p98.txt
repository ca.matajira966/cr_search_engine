80 MOSES AND PHARAOH

They feared the Israelites, for the God of these slaves was too power-
ful and dangerous. The Egyptians were not converted to this God;
they did not choose to worship Him. They chose instead to remove
His people from their midst, to put the fearful arm of God into a
different nation. They hoped to escape the earthly wrath of God by
encouraging the Israelites to leave as rapidly as possible, even
though the Israelites possessed the “borrowed” wealth. Pharaoh did
not even wait until morning to call Moses and Aaron before him
(12:31). The favor of the Egyptians was a fear-induced favor,

The second peculiarity of language is the use of the word
translated by the King James translators as “borrow.” It clearly
means “ask,” but here it implies “to extract under threat of violence.”
It meant tribute—in this case, tribute to a departing army rather than
to an invading one. The Israelites had not tried to invade Egypt
militarily, but the Egyptians had created a hostile nation within the
boundaries of Egypt by having placed the Israelites in bondage.

Slavery

It is not clear just how long the Israclites had been in bondage.
By the year of the Exadus, they had been slaves for at least 80 years,
since Moses was horn during the reign of the Pharaoh of the infan-
ticide edict, and he led the nation out of Egypt when he was 80 (Ex.
7:7; he died at age 120 [Deut. 34:7], and Israel spent 40 years in the
wilderness [Num. 14:33-34]). Courville’s reconstruction indicates
that the daughter of Pharaoh who brought up Moses was the
daughter of Amenemhet III, who succeeded Sesostris III,? Sesostris
III, concludes Courville, was the Pharaoh who first enslaved the
Israelites.?. Therefore, the Israclites were in actual bondage for
perhaps a century, possibly onc or two decades longer, I believe they
were in bondage for 80 years: two 40-year periods. It would depend
on the point of time in the reign of Sesostris III that he placed the
Israelites in bondage. If Courville’s reconstruction is incorrect, or if
he has not accurately identified the proper kings of Egypt (assuming
his dating is basically correct, i-e., 1445 8.c. for the Exodus), then all
we can say in confidence is that the enslavement was at least 80
years. This conclusion rests on the additional assumption that the
Pharaoh of the enslavement was the same as the Pharaoh of the in-

2. Donovan Courville, The Exodus Problem and Jts Ramifications (Loma Linda,
California: Challenge Books, 1971), I, p. 157.
3. Ibid., I, pp. 147-48.

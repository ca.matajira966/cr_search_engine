84 NO OTHER STANDARD

Paul emphatically denied that the law was against the promises
(Galatians 3:21). As Mediator of the New Covenant, Jesus pro-
nounced a curse upon anyone who would dare break even the
least of the commandments in the Law and the Prophets (Mat-
thew 5:19). Thus the continuity between the various covenants
from Abraham, through Moses, to Christ is a crucial point of
Biblical theology. When Gentiles become Christians, they are
made sons of Abraham by faith and heirs of the promise (Gala-
tians 3:29) —the promise around which all the Jewish covenants
were unified (Ephesians 2:12). God’s grace —His merciful re-
sponse to violations of His unbending law —is the singular core
around which the covenants of the Old Testament were estab-
lished. The New Covenant brings these former “covenants of the
promise” to full force and realization (e.g., 2 Cor. 1:20), thereby
demonstrating the continuity which characterizes the relationship
of God’s covenants to each other and warranting the concept of
a covenant of grace,

A Christo-Centric Ethic

Zens is jealous to insist that in our age the law “is solely in
the hands of Christ, not Moses” (as quoted above). He says, “To
refer the believer to the details of a terminated economy, and not
to the new and living way of grace and truth in Christ. . . is
manifestly retrogressive.”'° According to him, the “primary norm”
for the theonomist is not the commandments of Christ, but those
of Moses (an unwarranted slander); theonomy does not, he says,
do justice to “the superiority of God’s speaking in Christ.”

In an article entitled “This is My Beloved Son . . . Hear
Him,”"' Zens argued that the Old Testament law was a unity
which has passed away with the covenant which embodied it.
Throughout the article, Zens keeps stressing that we must be
Christ-centered, rather than law-centered. He claims that “the
whole Mosaic arrangement” has been abrogated, in which case
theonomists are wrong to assert simplistically that what was sinful

10. Jon Zens, Baptist Reférmation Review (Fourth Quarter, 1979}, p. 17.
11, Jon Zens, Bapiist Reformation Review, vol. 7 (Winter, 1978), pp. 15

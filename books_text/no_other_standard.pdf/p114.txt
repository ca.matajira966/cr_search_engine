98 NO OTHER STANDARD

Gentiles (eg., the prohibition on eating animals that died of
themselves, Deut. 14:21)? Whether they used the verbal labels of
“moral (civil)” and “ceremonial” (as we do) is beside the point.

Indeed, if the Israelites of old could not tell the difference
between moral laws (defining moral obligation) and ceremonial
laws (defining redemption for those who sin against the moral
Jaws) — and if we today do not draw that distinction — then the
purity of the gospel has been compromised. Both kinds of law
pertain to redemption, but in very different ways.'? We are not
saved by our righteous behavior, but according to God’s gracious
payment of our penalty (ceremonial law) and unto righteous be-
havior (moral law) — Eph. 2:8-10; Rom. 3:28; 8:4. I fear that
many critics of theonomic ethics veer dangerously close to error
by their ambiguous language and fuzzy thinking about the law
here. To eschew theological equivocation, theonomists concur with
the Reformed heritage in discriminating between Jaws which dis-
play the way of redemption (ceremonial) from laws which define
the righteousness of God (moral, civil) to be emulated as an effect
of redemption.

Seeking Similar Treatment for Judicial Laws

Having said this, we are criticized by others for not going on
to say more —for not treating the judicial Jaws (or civil, or case
laws) of the Old Testament in exactly the same way. This cate-
gory of Old Testament commands includes specific illustrations
of how the ten commandments (expressed more broadly or cate-
gorically) are to be applied, often to judicial or civil matters.

13. Schrotenboer equivocably states that al! Old Testament laws are “redemp-
tive” in character, since salvation is as wide as creation (pp. 57-58), and Robertson
claims ail the Old Testament laws had a cultic dimension. C£ the comment by Vern
Poythress: “In a broad sense every law has sedemptive purpose” (Theonomy: A
Reformed Critique, ed. William S, Barker and W, Robert Godfrey [Grand Rapids:
Zondervan Publishing House, 1990], p. 108). It is similarly misleading to assert “all
the verses point forward to Christ” and “have a unique redemptive-historical color-
ing” (p. 158). They certainly do not point to Him in the same way and with the
same teaching content, The prohibition of theft points to His honesty, but the
sacrificial system points to His redemptive work for thieves, Poythress and I are
probably not that far apart, though, since he grants that “as @ matter of degree” we
may still distinguish laws as “primarily” moral or ceremonial in character.

Categories of Old Testament Law 101

to a particular setting in history and in geographical place: “I
am Jehovah your God who brought you out of the land of Egypt.”
Moreover, the wording of the ten commandments themselves
contains temporal/spatial specifications (as do the judicial laws):
“in the land which Jehovah your God gives you” (Ex. 20:12),
stranger within the gates (v. 10), cattle, oxen and donkeys (wv.
10, 17). Such references scuttle Waltke’s notion that the “unre-
stricted extension” of the ten commandments sets them apart
from the judicial laws. (According to Deut. 4:5-8, the “extension”
of those judicial laws was rather unrestricted too!) One could
easily parody Waltke’s argument to dismiss the judicial laws by
making a case that the ten commandments also were “meant for
a specific situation and therefore are mutable” (p. 80). If you look
only at the literary presentation of either the judicial laws or the
ten commandments (in local context only), you could just as
easily argue that even the ten commandments “were meant for
Israel as long as they were in the Promised Land” — indeed, cf:
Deut. 4:1, 9-12, 22b-23, 26, 33, 40, 44 which all introduce a
restatement of the ten commandments (Deut. 5:1-22)!'6

Advocates of theonomic ethics readily grant that the deca-
Jogue has many distinctive features about it. But that premise
does not lead to the conclusion that only the decalogue is morally
binding after the establishment of the New Covenant. It does not
prove—and the critics never attempt to show how it could
prove — that the decalogue is uniquely obligatory. Think about this
for a moment. The unique features of the decalogue were true of
it prior to the establishment of the New Covenant. Do the critics
conclude, therefore, that only the decalogue was binding at that
time, during the Old Covenant? Why, then, would those features
prove that the decalogue alone is binding with the coming of the
New Covenant? This reasoning makes no sense.

According to theonomic ethics (and a long line of standard
Biblical interpretation), the judicial or case laws of the Old Testa-
ment explain and apply the meaning of the decalogue to the

16. Bruce Waltke, “Theonomy in Relation to Dispensational and Covenant The-
ologies,” Theonomy: A Reformed Critique, ed. William S. Barker and W. Robert Godfrey
(Grand Rapids: Zondervan Publishing House, 1990), pp. 70-72.

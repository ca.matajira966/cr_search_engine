Categories of Old Testament Law 103

ing particularly to do with the Old Testament case (judicial) laws,
though. Colossians 2:14 deals with the condemning function of the
law, while Ephesians 2:15 refers in particular to the ceremonial
category of laws which erected a wall of separation between Jews
and Gentiles. Chantry* appeals to Ephesians 2:15 as evidence
that the judicial laws “shut out the rest of the world from faith,”
but this is extravagant. The requirement of a rooftop railing and
the prohibition of rape (just to take two examples) did absolutely
nothing to bar the Gentiles from coming to faith. It is not the civil
regulations of the Jewish commonwealth which built a wall of
partition between Jews and Gentiles. After all, Gentile aliens
existed within the land of Israel and even came to saving faith.
Moreover, the laws revealed by Moses for the commonwealth
were intended to be a model for surrounding Gentile nations
(Deut. 4:5-8). Chantry is thus wrong to think Paul was alluding
to the judicial laws in Ephesians 2:15. Paul speaks of “the law of
commandments in ordinances” which erect a wall between Jews
and Gentiles.

God had revealed to the Jews the way of salvation, found in the
foreshadows of the ceremonial law (sacrifice, temple, etc.). That law
also contained outward signs of separation from the unbelieving
world, such as the dietary separation of clean from unclean meats
(Lev. 20:22-26). With the coming of Christ, these ceremonial
means of redemption have been made inoperative (Heb. 8:13),
and the symbols of separation have been laid aside (Acts 10:11-
15). It was the self-sacrifice of Christ which removed these laws
that placed a partition between Jews and Gentiles (Eph. 2:14-15),
thus bringing both groups into one saved body on an equal foot-
ing. Paul’s teaching in Ephesians 2:15 has nothing whatsoever to
do with Israel as a political body or with the judicial laws of the
Old Testament.

In the second place, if the critics were correct in their appeal
to these passages, and if these passages pertain to the law itself
(without discrimination), then the critics would be reduced to
absurdity — proving far, far more than they intended. Such pas-

20. God's Righteous Kingdom (Edinburgh: Banner of Truth Trust, 1980), pp. 117,
18, 121,

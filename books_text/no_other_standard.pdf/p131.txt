Israel’s Theocratic Uniqueness 5

from the Covenant people. There is nothing in this which “lifts”
them “right out of their historical context” (as Fowler groundlessly
asserts). It is just because of the historical context of these
laws ~ revealed throughout history to all nations by nature and
conscience, enforced by God’s historical judgments on nations, and
given testimony by the historical example of Israel herself— that
Israel’s laws may not correctly be deemed merely a racial or tribal
standard of justice. If laws revealed in the context of the covenant
do not bind those outside of God’s saving covenant, then not even
the decalogue (which was the epitome of the covenant with Moses)
remains to convict unbelievers of sin—in which case they do not
need a saving covenant anyway.

From a logical standpoint, the error most readily committed
by critics who appeal to Israel’s theocratic uniqueness is that they
demonstrate no ethical relevance between (or necessary connec-
tion between) the unique features of Israel and the moral validity
of the law. Plenty of things were unique about Israel, but Scrip-
ture does not teach that God predicated the justice or obligation
of His commandments upon those features. Yes, Israel alone
received the “ceremonial law” for her salvation.” However, this
redemptive blessing was not the reason (or only reason) that rape
called for the death penalty and theft called for restitution in
Israel. Likewise, Israel was given special instructions for holy war
against the Canaanites. But there is not one text of Scripture
which suggests that the penal sanctions of the Mosaic law were
merely an extension of such holy war provisions.’ The argument
which is heard most frequently is that Israel was unique as a
society where there was no separation of church and state—a
misleading and mischievous claim to which I have given a sepa-
rate chapter in this book.

The Argument From Israel
as a Holy Nation and Its Typology

After Theonomy in Christian Ethics was first published, the editor

7. R, Laitd Hartis “Theonomy in Christian Ethics: a Review of Greg L, Bahnsen’s
Book,” Preibyterion V (July, 1979}, p. 12.

8. Contrary to Neilson, p. 33.

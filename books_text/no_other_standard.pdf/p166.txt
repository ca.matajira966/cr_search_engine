150 NO OTHER STANDARD

otherwise. Thus his general remark is somewhat overstated. More-
over, he does not offer us a biblical principle by which he would
discriminate between laws which are for our good to enforce today
and ones which are not. It seems to me that Scripture does not
offer such a principle just because ai! of God’s laws were given for
our good (Deut. 6:24; 10:13), and to follow them is to the advan-
tage of any nation (Deut. 4:4, 8; Prov. 14:34; cf Matt. 6:33; I
Tim. 1:8-10; 4:8; Rom. 13: 1-4).

(d) If the above assertion by Robertson infers that theonomic
ethics promotes the imposition of God’s law on a recalcitrant
society by means of the sword, then I would need to disagree
further with it. The church does not utilize any sword but the
sword of the Spirit, according to Theonomy.!? When that converting
sword has done its work, then a society — being taught to observe
whatsoever Christ has commanded (Matt. 28:18-20) — will adopt
the law of God as the law of its land. It will be followed by choice,
not by enforced threats, (Of course criminal elements will always
have the law—any law which they violate— “enforced” upon
them if they are punished for disobedience. That is true for any
view of civil ethics. The question, then, is which law should we
enforce, God’s inspired one or man’s humanistic one?) This is as
true of the laws in Deut. 13 as those in Ex. 21.

(e} One of Robertson’s arguments against the theonomic con-
ception of the church-state relation leads him to insist that the
Qid Testament state was indeed “an agent of evangelism” — after
he carefully redefines ‘evangelism’ in a broad enough fashion to
include any subduing of an area of life to the revealed will of
God, thereby advancing God’s kingdom. I have no problem with
this understanding of evangelism, if he wishes to use it. But if he
does, then he must be consistent with it and thereby grant that
states today (as also doctors, lawyers, plumbers, etc.) — even as
the Old Testament state — are agents of “evangelism.”

Conclusion

Isaac Watts put it well in his hymn, “Joy to the World”: “He
comes to make His blessings flow far as the curse is found.” If the

12. Theonomy, pp. 414

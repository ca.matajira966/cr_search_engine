156 NO OTHER STANDARD

concern regarding political abuses of power! If Harris does not
want the rigor and restraints of God's written law to be the
standard for a civil magistrate to follow—for fear that it will
breed excesses and mistakes —then he should pause to consider
what that same mistaken and excessive ruler might do if released
to follow nothing else but his private conscience!

More important than any of these prudential considerations,
however, is the question of what God’s own word requires of civil
rulers. And whether we trust the provisions He has made for
safeguarding the justice and wellbeing of society or not, the fact
would remain that such laws are our duty to uphold and (where
applicable) to follow. Until a Biblical case can be made against
the “civil” portion of the Old Testament law, we must presume
that it continues to be binding today.

The Appeal to Common Grace

In a short essay written against theonomy, David Neilands
has claimed that “theonomy has no place for common grace in
the realm of government.”* However following this claim, Neilands
quotes a statement from my book in which I directly and explic-
itly contradict what he says about the theonomic position: “this
restraint of evil by means of human governments is a sure exam-
ple of God’s common grace” (Theonomy, p. 584). Theonomy surely
does have a place for common grace in the realm of civil govern-
ment. But Neilands retorts—just this briefly “This is hardly
the general view of common grace. [Bahnsen’s] common grace
includes God’s law which is special revelation.” It appears from
this remark that Neilands does not wish to take account of the
extensive debates over the meaning of and Biblical justification
for the various concepts represented by the expression “common
grace,” for otherwise he would not thoughtlessly allude to “the
general view” of it—as though some kind of consensus could be
taken for granted. If there is anything approaching “the general
view” which could be clearly defined and justified, the theonomic

4. “Theonomy and Common Grace” (privately distributed, in connection with
a study committee on theonomy in the Presbytery of Northern California, O.P.C.,
1982).

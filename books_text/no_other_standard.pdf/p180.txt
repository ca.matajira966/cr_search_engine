164 NO OTHER STANDARD

discussion, that Neilson finally allows all of the theonomic argu-
mentation and supporting passages to be considered all together
for the overall impression they themselves supply. But just here his
argument stops and he says, “So each must choose.” He never
faces up to refuting the case for theonomic ethics when all of its
major premises, insights, and Scripture supports are working in
tandem; he simply bails out, saying that “to me” they are not
convincing,

This may seem to the reader not worth our attention as a
serious critique of theonomic politics. I have taken the time to
describe Neilson’s method of attacking theonomy, though, merely
because he has been overt and explicit about his line of thinking.
He is by no means alone in reasoning in the fashion described
above. Indeed the vast majority of what I have heard and read.
against the theonomic view of the civil magistrate is very much
in line with Neilson’s approach. He has simply been transparent
enough to let the weakness of this line of thinking be openly seen.

Are There Actual, Further Qualifications
Just Because There Might Be?

He has also been good enough to explicate for us another
common line of fallacious reasoning against the theonomic posi-
tion. Neilson’s basic way of counteracting the key passages which
appear to support the theonomic viewpoint (regarding civil sanc-
tions for certain “religious” offenses) is anything but persuasive.
Matthew 5:17-19 seems to teach that all of the Old Testament
commandments are binding today, and Romans 13:1-7 appears
to teach that the civil magistrate is to punish evil as defined by
God’s law. This much is granted. However, says Neilson, in both
cases qualifications are obviously necessary when we examine the rest
of Scripture. Not all of the Old Testament commandments are
observed today since the ceremonial law has been fulfilled in
Christ, and the magistrate is not to punish every form of evil but
only outward offenses; consequently, both passages need to be
limited in the scope of their application. Since some restrictions
need to be placed upon the unqualified universality of Matthew
5:17-19 (every commandment) and upon the unqualified scope of

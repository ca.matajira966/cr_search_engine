176 NO OTHER STANDARD

Would Not Such Considerations Prove Too Much?

Remember that Neilson has not delineated any adequate and
consistent principle for distinguishing “religious” crimes from “man
to man” crimes. In light of that, one can easily go back through
his alleged disconfirmations of the theonomic thesis (regarding
civil sanctions for “religious” offenses) and apply his line of rea~
soning to other crimes as well, showing thereby that his disconfir-
mations prove —if anything —too much, and thus nothing after
all. There is no reason to think that his considerations apply
uniquely to “religious” offenses, exempting “man to man” offenses
from their disconfirming force (if any).

For instance, the crime of rape is something which few people
would think should be permitted to go unpunished by the civil
magistrate, However, as Neilson has suggested, the New Testa-
ment has a greater emphasis on inwardness, compassion, and an
attitude which does not long for premature judgment on unbeliev-
ers; rapists, of course, will receive their ultimate judgment in hell,
according to the Bible. If anyone should observe that in the Old
Testament rapists were to be executed, Neilson could note that
individual cases of capital punishment were commuted in the Old
Testament, and that the death penalty for rape was typical of the
coming, final judgment anyway. Furthermore, there is no mention
of civil punishment for rape before Sinai, such a civil sanction is
never mentioned in connection with Gentile rulers, and the magis-
trate’s punishing of rape is not prescribed or specifically men-
tioned in the New Testament. Would these considerations reason-
ably lead us to conclude that God has placed a restraint upon any
civil sanction against rape in this age of spiritual warfare? Not at
all. And unless Neilson can offer a Biblically based principle for
distinguishing between perpetual and temporary penal sanctions
from the Old Testament, whatever argument he uses to defend
(or fault) the validity of civil penalties for the selected “religious”
offenses with which his monograph is concerned will apply to aif.

 

executing Christians for failure to offer emperor worship is not supportive of this
claim, cf. Rev. 13), hardly normative for a Bibtical conception of socio-political
morality.

Autonomous Penology, Arbitrary Penology 219

Testament.!? In this way many critics hope to be able to presume
that the Mosaic penal code has now been abrogated, and yet to
maintain the validity of the death penalty for murder, since it was
stipulated prior to Moses, deals with the image of God, and has
not been invalidated by the New Testament. This line of reason-
ing is overburdened with arbitrariness, false assumptions and
fallacious inferences (many of which were rehearsed in Theonomy,
pp. 458-466, and which critics have yet to answer).

By endorsing and restricting modern states to the penal jus-
tice of the Noahic revelation, do critics also insist on the (equally
Noahic) prohibition on eating meat in its blood (Genesis 9:4)?
On what basis is this dietary restriction ignored, while capital
punishment for murder is endorsed? If pre-Mosaic stipulations
are as suck universal and perpetual in their validity, the dietary
requirement should not be set aside. On the other hand, if only
pre-Mosaic stipulations are universal and perpetual standards for
modern states, it would seem that we must rule out the state’s
right to tax its citizens (which is not mentioned in the Noahic
covenant, although countenanced by Paul in Romans 13:6-7).
Moreover, if the non-Noahic provisions of the Mosaic civil code
are really no longer valid today, then non-theonomists would be
compelled to set aside the distinction between manslaughter and
pre-meditated murder which is revealed by Moses, but absent
from the Noahic covenant. If states today are limited to punishing
infractions as defined by the Noahic revelation, there would be
precious little protection left to citizens — against such common
crimes as theft, fraud, rape, kidnapping, perjury, violation of con-
tracts, compensation for damages, etc.

The fact is that theonomic critics are anything but clear and
consistent in their restriction of contemporary social justice to the

12, With minor variations we find this line of thought set forth by Henry, “The
Christian and Political Duty,” God, Revelation and Authority, vol. 6, p. 447; Raymond
©. Zorn, “Theonomy in Christian Ethics,” Vox Reformaia (May, 1982), pp. 17-18}
©. Palmer Robertson, Tapes: “Analysis of Theonomy” (available from Mt. Olive
Tape Library, Box 42, Mt. Olive, MS 39119), tape# ORIO7A1, A2, B3; H. Wayne
House and Thomas Ice, Dominion Theology: Blessing or Curse? (Portland, Oregon:
Multnomah Press, 1988), pp. 86, 119, 127, 130, 135, 187, 339.

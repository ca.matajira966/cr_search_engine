242 NO OTHER STANDARD

namely that Paul could not have expected Festus to understand
the complexities of the Torah (why not? are we question-
begging?). But Paul’s appeal does not presuppose that Festus even
needs to understand the Torah. Paul simply cries out that he is
not guilty of those things charged against him by the Jews — and
if he were, he would not refuse to submit to the appropriate
penalty of death. To turn the tables on Johnson, I would suggest
that it would be straining credulity to think that the Jewish an-
tagonists who “came down from Jerusalem” and “brought against
[Paul] many and grievous charges” (v. 7) were debating points of
Roman jurisprudence! The many serious crimes which would
have been on Jewish minds would have been offenses against their
own law—in which they prided themselves sinfully (cf Rom.
2:18, 20, 23).

Johnson observes Paul’s use in 1 Corinthians 5:13 of the
expulsion language found in some Old Testament penal passages,
“Put away the wicked from among yourselves,” and he notes that
we find there that excommunication is the ecclesiastical expression
of that objective. Is this in any way contrary to what one would
expect based upon a theonomic perspective on ethics, though?
Not at all; it rather is exactly what the position would entail. The
church — the covenant community —is unquestionably supposed
to seek to remove wickedness from its midst (as the law requires),
and the manner in which that is accomplished within the church
is excommunication. This tells us nothing of Paul’s views (posi-
tively or negatively) about whether, or the way in which, the civil
state should seek that objective. Johnson recognizes that it does
“not necessarily” follow from this passage that excommunication
has replaced the Old Testament civil sanction, and he is correct.?

 

is worthy of death. On Johnson’s narrow interpretation, what ordinance would this
be? The argument that not all sins listed in verses 29-31 are capital offenses is not
on point. Those said to be worthy of death in verse 32 are the hoitines at the beginning
of the verse—reférring back to the homosexuals who have heen “given up to a
reprobate mind to do improper things, being filled with every kind of evil [such
as. . J (wv. 28-29),

9. Johnson, p. 181. He says Paul’s ending of his discussion with the use of this
formula is “noteworthy” (and it is), but one suspects Johnson is hinting at more than

Introduction to the Debate i

says about society and civil government. This sacred/secular stance
is a theologically unwarranted and socially dangerous curtailing
of the scope of the Bible’s truth and authority (Ps. 119:160; Isa.
40:8; 45:19; John 17:17; Deut. 4:2; Matt. 5:18-19).

We beseech men not to be conformed to this world, but
transformed by the renewing and reconciling work of Jesus Christ
so as to prove the good, acceptable and perfect will of God in their
lives (2 Cor. 5:20-21; Rom. 12:1-2). We call on them to be deliv-
ered out of darkness into the kingdom of God’s Son, who was
taiscd from the dead in order to have pre-eminence in aif things
(Col. 1:13-18). We must “cast down reasonings and every high
thing which is exalted against the knowledge of God, bringing
every thought into captivity to the obedience of Christ” (2 Cor.
10:5) in whom “all the treasures of wisdom and knowledge are
deposited” (Col. 2:3). Thus believers are exhorted to be holy in
all manner of living (I Peter 1:15), and to do whatever they do for
the glory of God (I Cor. 10:31). To do so will require adherence
to the written word of God, since our faith does not stand in the
wisdom of men but rather in the work and teaching of God’s
Holy Spirit (1 Cor. 2:5, 13; cf. I Thes. 2:13; Num. 15:39; Jer.
23:16). That teaching, infallibly recorded in “every scripture” of
the Old and New Testaments, is able to equip us “for every good
work” (2 Tim. 3:16-17} — thus even in public, community life.

For these reasons theonomists are committed to the ¢ransforma-
tion (reconstruction) of every area of life, including the institutions
and affairs of the socio-political realm, according to the holy
principles of God’s revealed word (theonomy). It is toward this
end that the human community must strive if it is to enjoy true
justice and peace. Because space will not allow a full elaboration,
with extensive qualifications and applications, of the theonomic
position here, it may prove helpful to begin with a systematic
overview and basic summary of the theonomic conception of the
role of civil government in terms of Christ’s rule as King and of
His inscripturated laws:

1. The Scriptures of the Old and New Testaments are, in part
and in whole, a verbal revelation from God through the words of
men, being infallibly true regarding all that they teach on any subject.

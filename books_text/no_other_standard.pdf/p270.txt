254 NO OTHER STANDARD

law’s penal sanctions.? They were not to swerve to the right or
the left from the law (Deut. 17:18-20), and justice is perverted
when the law is slackened (Hab. 1:4). God declared “Thine eye
shall not pity” when criminals are to be executed (Deut. 19:13,
21), for that would make the magistrate guilty of being a respecter
of persons (Prov. 24:23). “The evil man shall not be unpunished”
(Prov. 11:21). Mercy on the part of the civil magistrate in apply-
ing the penal sanctions of the law against criminals is strictly
forbidden. “A man who had violated Moses’ law died without
compassion on the word of two or three witnesses” (Heb.
10:28) - the justice of which procedure is foundational to our
conviction that God is just in condemning apostates for eternity
(v. 29)! Those who wish to keep God’s law, therefore, will defi-
nitely contend with the wicked (Prov. 28:4). Not even a trespass
offering before the priest (Lev. 6:4-7; Num. 5:5-8) or clinging to
God's merciful altar (Ex. 21:14) could relieve the criminal of his
punishment —even as the sacrificial death of Christ upon the
altar for our salvation does not relieve criminals today. Inflexibility
regarding the law’s penal sanctions is a virlwe, not a drawback.
Such inflexibility was Paul’s attitude (Acts 25:11), and without it
the good citizens of a society can have no confidence because the

sword held over the criminal elements in society is carried in vain
(Rom. 13:3-4).

Accommodation and Flexibility

The report of the Special Committee to study the theonomic
question in Evangel Presbytery (P.C.A.) evidenced a concern that
theonomists recognize the need for a flexible application of God’s
law in our day. Its leading question to thconomy was whether it
adequately recognizes the distinction between the Decalogue and
the cultural applications of it in the judicial laws of the Old

2. Tam speaking here of the necessity of punishing the criminal, rather than the
necessity of the particular punishment inflicted. It is another question whether the
law of God itseif grants discretion (in some cases) to civil judges in wisely choosing
the specific penalty that will be applied in a specific case. On the issue of flexibility
within the law of God itself regarding civil penalties, see the discussion below in
response to Dr. Kaiser.

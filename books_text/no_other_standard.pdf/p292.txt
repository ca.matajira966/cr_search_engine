276 NO OTHER STANDARD

to verse 19 (and thereby the overall thrust of the passage), and
shifted the crux of his argument away from exegesis to a theological
discussion of the “judicial laws” and the New Testament form of
God’s kingdom. The changes made by Fowler are significant
ones. Between his first and second efforts to criticize theonomy,
he has silently conceded the overall debate, hanging on now
merely to differences of detail.

Although his second paper needed to soften his criticism of
my treatment of Matthew 5:17, the most important change from
Fowler’s first effort to criticize the theonomic position to his sec-
ond effort has been his complete turn-around regarding the teach-
ing of Matthew 5:19. So dramatic has been his shift that Fowler
has unwittingly come around to the very position taken by my
book, when all is said and done! In his first paper, Fowler sug-
gested that it was only “at first glance” that it “might appear”
that this verse teaches the need for “meticulous observance by
Kingdom citizens of the least details of the Old Testament law.”
In an attempt to overcome this initial appearance, Fowler began
to question what “the content” of “these least commandments”
would be, indicating that it had to be some thing “deeper and
more profound” than the Mosaic laws which the Pharisees
kept—and which Jesus went on to modify in this passage. If
Fowler granted the initial theonomic impression of Matthew 5:19,
his case would be lost, and so he struggled to find a way around
it. Turning away from literary exegesis, he hoped that a look at
the (alleged) history of Reformed theological treatment of the law
would offer an understanding of the content of “the least com-
mandments” in Matthew 5:19 which would not be theonomic.

What is most significant now is that with the distribution of
his second effort, Dr. Fowler has completely revised his approach to
Matthew 5:19. The original three pages dealing with this verse
have been completely dropped out of sight, and ten completely
new pages have replaced them. “The issue” regarding Matthew

 

others secking a refutation of my book. I subsequently distributed a thorough analy-
sis and rebuttal of that paper. Then in February, 1980, Dr. Fowler privately pub-
fished a revised paper entitled “God’s Law Free from Legalism” (distributed from
Reformed Theological Seminary, Jackson, Mississippi).

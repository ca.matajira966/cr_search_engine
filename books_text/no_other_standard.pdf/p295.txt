The Exegesis of Matthew 5 279

Fowler misconceives my position when he states that it is
“critical” to my interpretation of Matthew 5:17 that Jesus is not
speaking of His life. If Jesus meant to state there that His life
would be in utter conformity to the Old Testament, this would
still imply the law’s continuing validity, and thus the point is not
critical for my thesis at all. Further, the simple use of the Greek
verb althon does nothing (contrary to Fowler) to indicate the
purpose of the “coming” which the verb expresses. In his discus-
sion, Fowler commits the fallacy which is often found in Kittel-
type word studies: namely, assuming that every instance of a
word carries with it the accumulated load of its connotations
everywhere else it is used. The issue is the precise use of the word.
in the particular text before us. Fowler’s assertion that Jesus’
teaching and life cannot be divorced is beside the point, The fact
is that they can be distinguished (even as the heads and tails of a
coin cannot be divorced but distinguished) and thus referred to
separately. Anyone can look at the context of Matthew 5:17 and
see that it is not pointedly about the life and behavior of Christ,
but rather a discourse (His doctrine} about the lifestyle of His
followers.

The debate between Fowler and myself over the 18th verse
of Matthew 5 would be with respect to the closing panta genetai
clause. He mistakenly states my position when he claims that I
make the two eos clauses “read as one” and that I would translate
genetai as “become invalid.” In fact, what I see in Matthew 5:18
is the common device of parallelism, the two eos clauses being
functionally equivalent to each other (viz., referring to the end of
the world). What is wrong with interpreting “until heaven and
earth pass away” as parallel to “until everything comes to pass”?
Fowler points to the change of main verb from one feos clause to
the other as showing that something new is being said, but this
is a non sequitur, It may simply be that the same thing is being said
in a new way. Fowler would prefer to parallel “come to pass” in
Matthew 5:18 with “fulfill” in 5:17, but this is merely a personal
preference without argument. The syntactical parallelism within
verse 18 (rather than between verses 17 and 18) is surely more
obvious.

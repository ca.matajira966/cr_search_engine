300 NO OTHER STANDARD

penchant for appealing to vague “motifs” in Biblical passages and
then telling us (without exegetical basis) that they are suggestive
of some theological “connection” or “relation” (without definition).
To deal with broad and ambiguous allusions is not precise enough
to demonstrate any specific conclusion; because there are no con-
trol principles or predictability in how such vague notions will be
taken, the door is left open too wide for the interpreter’s subjective
creativity. And simply to assert that X is (somehow) “related”
or “connected” to Y is trivial —not very informative. (Everything
is related in some way to everything else, after all.) These vague
connections play a determinative role where Poythress wants to
draw significant theological conclusions. For example, in the midst
of explaining how one should not fall into theonomic misapplica-
tion of the law, Poythress calls for interpreters to take account of
the contexts of the law — and includes this entirely vague observa-
tion: “the context of the tabernacle is also relevant because the
tabernacle and the law are closely related” (p. 341). But being
“closely related” could mean any number of different things, in
which case no pro-theonomic or anti-theonomic inferences follow
from this remark. The implications of the law’s “close relation-
ship” to the tabernacle are left to the interpreter to fill in.

About his style of Biblical interpretation Poythress offers his
own commentary: “all these associations are of a vague, sugges-
tive, allusive kind . . . suggesting a multitude of relationships”
(p. 29). Westerners, he claims, minimize “the personal depth
dimensions of human living” (whatever that means); by contrast,
“to appreciate a symbol, we must let our imaginations play a
little” (pp. 38, 39). Just what we need in controlled theological
argument: a playful imagination! We are always left wondering
whether the results of such Biblical interpretation reflect the mind
of God or merely that of the interpreter.

In this mode of communication, some of what Poythress says
is nearly unintelligible. He can speak of a “framework of corre-
spondences” between the tabernacle and promised land, declaring
that the land “shares in the multiple symbolic relations” of the
tabernacle “at least indirectly,” or “at a reduced level of intensity”
{p. 70). What exactly is he claiming (or denying) by these enig-

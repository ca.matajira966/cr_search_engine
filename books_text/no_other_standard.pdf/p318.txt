302 NO OTHER STANDARD

(p. 97)! Apparently, since both the legislation about uncleanness
and the account of creation involve distinctions — despite their be-
ing of completely different kinds—they must be somehow con-
nected to cach other. Accordingly, the separations made by God
at creation are also to be taken as “analogous to the separations
[of rooms, furniture] within the tabernacle” (p. 80), This is really
strained.

The key to drawing artful “connections” everywhere in the
Bible, of course, is to make your categories broad and vague
enough to include just about anything. This is what Poythress
does when he makes the law out to be organized according to the
theme of articulating God’s “order” (pp. 80ff.) or expressing “life”
(pp. 83if.). There are so many senses and kinds of “order” — so
many ways to think about or allude to “life” — that this explana-
tory device gains maximum flexibility and coverage at the cost of
minimum teaching value or equivocation. (Even so, Poythress still
struggles to make some commandments fit into his scheme; theft
is arbitrarily seen as “disordering” human ownership [p. 89]
when more accurately it simply reorders ownership). The same
vagueness and ambiguity is evident when he ties together the
law, the tabernacle and the promised land because they all were
“reflecting the orderliness” of God (p. 109).

Lack of Adequate Logical, Textual Controls

But now then, what does Poythress do when he encounters
competing symbolic explanations — divergent broad “themes” or
“connections” which might be proposed to explain something in
the Biblical text, such as the classification of things as clean or
unclean? This question gives us insight into the “logic” of his
method for Biblical interpretation. It turns out that in such a case
as this the vague and open-ended approach of Poythress allows
him to say about competing schemes of symbolic interpretation:
“in a sense it does not matter”! Why not? Because all we need to
say is that “the two themes . . . are in fact complementary” (p.
82; cf. also p. 68 on competing conceptual subdivisions). Presto!
Nearly everything and anything that comes to mind can thus be
accommodated to this imaginative approach to Biblical interpre-

312 NO OTHER STANDARD

other times does not appcal to those same principles, or at yet
other times appeals to conflicting principles, there is no predict-
ability to the conclusions which are reached (or can be reached)
in terms of his style of interpreting and applying the Old Testa-
ment law. The reasoning can be pressed to prove that a certain
Old Tcstament command is to be applied by civil magistrates
today — or, equally, that the same command is not to be applied
in modern states. A methodology which is this random or arbi-
trary is simply lacking in cogency and bears no authority for
drawing theological conclusions.

Poythress wants to say that there is continuity between the
Old and New Testaments regarding the law of God (or some
particular commandment), and yet there is also discontinuity. And
from that generalized, dialectical platform it then becomes unpre-
dictable (and unjustifiable) whether Poythress will personally come
down upon the continuity emphasis or the discontinuity emphasis
in the conclusion he draws from case to case about the applicabil-
ity of any law today. He portrays the law as having an authority
which cannot pass away, “but in another sense” it did not have
ultimacy and fades away (p. 98). “The new covenant thus contin-
ues the standards of righteousness of the Mosaic covenant. Yet
radical transformation is also in view” (p. 116). The Israelite law
teaches us about the concrete embodiment of justice in an imper-
fect world, Poythress teaches, and then in the next breath he
asserts: “but” that law was meant to foreshadow Christ and thus
be temporary in use (p. 161). Repeatedly it is “yes and no.” And
from this abstract principle’ that there is always continuity-but-
discontinuity Poythress appears to approach individual cases of

24. The problem is not that Poythress believes that there are particular points of
continuity as well as particular points of discontinuity between the Old and New
Testament regarding particular commandments; theonomists teach that such a combi-
nation of particular continuities and particular discontinuities characterizes the rela~
tion between the Testaments. The problem enters when we move away from dealing
with exegetically-based particulars (whether continuities or discontinuities) and her-
mencutically rely simply upon an abstract generalization of continuity-but-discontinuity-
also when interpreting a specific commandment. We are not permitted to define the
continuity or discontinuity for any law on our own, guided by the abstract principle,
but can do so only on the basis of exegesis of the given texts of Scripture.

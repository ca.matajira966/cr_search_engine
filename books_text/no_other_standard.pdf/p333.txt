Poythress as a Theonomist 317

the law. On the other hand, absence of any textual allusion to the
holiness of Israel as the appropriate context or rationale for an
Old Testament penal sanction does not, for Poythress, mean that
we may not dismiss the civil use of that penal sanction today on
the basis of Israel's special holiness! He cautions the reader on
page 215 that “we cannot tell for certain whether the special
holiness of Israel has an influence on the nature and severity of
the penalty” for adultery. That is, Poythress is open to applying
the argument from Israel’s special holiness against the continuing
civil use of an Old Testament penal sanction, even when the text
gives no warrant for doing so. {In logic this is known as the fallacy
of “arguing from ignorance”: viz., the mitigating principle of Is-
rael’s holiness can be applied because “we cannot tell for certain”
that it does not.) But now notice the utter arbitrariness with which
Poythress is left in his reasoning from Israel’s special holiness.
The argumentative force of that feature about Israel (for setting
aside the continuing validity of an Old Testament penal sanction)
might not come into play when the feature is mentioned by Scrip-
ture, but it might come into play when the feature is not mentioned
by Scripture! When we examine the argumentation in Poythress’
book, it all appears to be guided by the subjective feelings of the
interpreter and whatever conclusion he desires to reach. The
danger, therefore, is that we end up reading our own thoughts
into the Bible, rather than reliably exegeting it to find God’s
thoughts,

Now that we have traversed the preceding explanations and
illustrations, let me go back and restate the debilitating
methodological problem which the careful reader sees throughout
Poythress’ book: when your principles of Biblical interpretation
and theological argument are so vague and are used so dialecti-
cally that you can prove anything by means of them (depending
upon your predilection), then those principles are as good as

“proving” nothing.
4, “Fulfillment” of the Law in Matthew 5:17

In the last chapter (pp. 251-286) as well as final appendix to
his book (pp. 363-377), Poythress takes issue with me for arguing

Paythress as a Theonomist 323

importing prophetic and typological objects of “fulfillment” is
astounding, obvious to any simple reader. Jesus there deals with
ethical directives and lifestyle among His followers. We must go
elsewhere in Matthew’s writing to find the typological and pro-
phetic emphasis upon which Poythress chooses to focus.

What “more” does Poythress want to add to interpreting
“fulfill” in Matthew 5:17 as meaning confirming the law in full
measure? Here his discussion degenerates into nearly complete
fuzziness, ambiguity and poetic connotation —sidestepping any
helpful clarity and analysis. He insists that Jesus “fulfills” the law
not simply by “static continuation,” “static maintenance,” mere
“reiteration,” “flat, prosaic, purely unimaginative and strictly straight-
forward reading,” treating the law as an “abstracted word” and
“dusty legal specification,” or hastily reading immediate moral
applications “off of the surface of its text” (pp. 265, 266, 281).
We don’t want any of that “static, abstract, flat or dusty” stuff!
Poythress has more glowing, emotive words for what “fulfilling”
the law means. It involves something “dramatic and spectacular,”
“dynamic advance,” “realization,” “accomplishment,” “a step
forward,” something “new and climactic” which “bursts the
bounds,” something involving “depths and richness” ~ indeed,
“radical” and “profound transformation” (pp. 265, 267, 272, 282).
All gushiness aside, the word “fulfill” does not ordinarily mean
to “change” something (radically transform it). And the word
simply cannot in Matthew 5:17 have the practical effect of annul-

35. The examples offered by Poythress (pp. 258-262) for Jesus “intensifying” or
“going beyond” the law of Moses are quite problematic. He claims Matthew 5:22
intensifies the Old Testament punishments, but this overlooks the fact that the threat
of hell was already taught in the Old Testament. He claims Jesus taught that divorce
is morally evil — which atrociously implies that God practices evil (Jer. 3:8). Poythress
says that Jesus abolishes oaths altogether, even though they are used by God and
men in the New Testament. He portrays the “golden rule” as a new notion which
transforms the dex talionis— even though that rule is not unique to Jesus or the New
Testament. He sees Jesus advancing upon the Old Testament by teaching the
importance of heart intentions in religious practices, even though that message can
already be found in Moses and the prophets at many places,

36. Elsewhere he says “we must expect radical transformation of the texture of
the law and radical reinterpretation in the light of the accomplishments of Christ”
(p. 336). OF the “texture” of the law?

General Index 337

Exodus, old and new, 86

Experts, legal, 137f
Extrascriptural standards, 60, 198
Ezra, 120, 167

“Faith communities”, 191

Faith, privatized, 270

Faith and obedience, 77

Fallacious reasoning of critics, 57ff, 95,
108, 116f, 118f, 122, 165, 175, 203,
227, 229, 248, 2556 263, 281
see: begging the question,

equivocation, silence, reification

False antithesis, 172

Family threatened, 249

Feelings and impressions as standard,
174, 229

Feinberg, Louis, 8

Feinberg, Paul, 8, 102

Festus, 175, 242

Final judgment, 178f, 225f, 241,
247, 313

First amendment, 170, 183, 186

First great commandment, and second,
205

First table of decafogue, unenforced,
2026F

Flexibility regarding penal code, 251

Flexibility within penal code, 256ff

“Forcing” the law on society, 39

Forgiveness enjoined, 173f

Fowler, Paul, 96, 100, 105, 113, 115,
134, 275

Frame, John, 18, 26, 27f, 44, 45, 53,
94, 116, 217, 263, 266

Freedom from dominion of law, 81

Freedom of conscience, 185

Freedom of religion, 200

“Fulfill”, 275, 277ff, 285, 288, 3176

Fuller, Daniel, 27

Functional equivalence in translation,
Q77f

Fusion of church and state, 134ff

Fuzzy picture desired, 28

Gaffin, Richard, 52
Gallup poll, 198
Geesink, Willem, 19
Geisler, Norman, 8, 39
General revelation
see: natural revelation
Geneva College, 7
Gentile nations
as covenant-breakers, 243f
Gentile crimes as insulting to God,
237f
magistrates, responsible to law, 203f
not under the law, 80f
Gentry, Kenneth, Jr., 5, 7, 24, 39
God

absolute justice, 253
character reflected by law, 95
essential character/eternal purposes,
45f, 253
holiness of, 61, 238
sce: immutability
inflexibility of, 251
mercy of, 253
presence of, 2374, 268
prerogatives, tampered with by
critics, 100
God and Politics: Four Views, 5, 7,9
Godfrey, Robert, 8, 20, 36, 47, 53
Goring ox, 261
Gospel deemed foolish, 230
Goudzwaard, Bob, 10
Great Commission, 53

Harris, Laird, 39, 62, 63, 96, 115, 136f,
138, 155f, 1668, 182F

Heart, sins offsins of hand, 240f

Heightened responsibility, 237f

Hall, punishment of, 174, 178, 239, 242,
254, 259

Henry, Carl F. H., 214, 219

Heresy not punished by civil
magistrate, 63, 182

Hermencutical control principle, 50,
3028 325

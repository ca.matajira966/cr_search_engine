Theological and Logical Fallacies 71

ments, not the silences. That is simply to say, where the New Testa-
ment is “silent” about an Old Testament precept, we may not
assume its applicability. If the New Testament does not “state”
the precise nature of some precept’s continuity, then we may not
conclude for its continuity. Johnson expects New Testament restate-
ment or 7-interpretation anyway, or else silence implies discontinu-
ity. This proposal is a definite choice of one operating presump-
tion (or way to handle New Testament silences) over the
other—and it is, sadly, not the covenantal one.!5 Nor is it a
choice with which Johnson is really willing to be theologically
consistent.'6

Now then, we must add that the choice between the herme-
neutical presumption of continuity and that of discontinuity is not
an abstract theological dispute which we may settle “outside of”
Scripture or in terms of our “accustomed way of reading” Scrip-
ture. It is not a question, moreover, on which Scripture itself is
silent. The disappointing thing about Johnson’s discussion of “the
burden of proof” is that he does not address the exegetically based
answer which is readily available in the teaching of the Bible
about how we should see the coming of Christ affecting the gen-
eral operating question of continuity or discontinuity with the Old
Testament law (Matt. 5:17-19). Where the Lord of the covenant
Himself answers that question —and it was precisely that issue
which He was raising {or sensing from His audience) — Biblical
theologians are not free to overlook the answer or adopt another

15. Johnson disguises this fact from himself by thinking that he is simply talking
about “the character” of the continuity or discontinuity between the Testaments
(pp. 175, 190}—when in actuality he has adopted a hermeneutical rule absut conti-
nuity or discontinuity in cases of New Testament silence.

16. Does Johnson sincerely adopt the pattern of reasoning he sets before his
readers? Whal does he theologically conclude from the fact that infant baptism “is
not explicitly addressed in the New Testament”? or the regulative principle of
worship? or a specific sabbalh-keeping requirement? or a prohibition of searching
into the secret things of God? or a prohibition of bestiality? We could continue with
such questions. We can also be relatively certain that Dr. Johnson (and many
theonomic critics like him) would not treat issucs like these in the way that he has
treated the issue of Mosaic penal sanctions. The appeal to silence as a tool of
abrogation is selective and arbitrary. And for that reason alone it is theologically
illicit.

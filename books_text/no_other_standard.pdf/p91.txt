5
CHANGE OF DISPENSATION OR COVENANT

There are those who oppose the theonomic position because
they believe that Christians are no longer under the dispensation
of law (revealed by Moses), but rather the dispensation of grace
inaugurated by Christ. There are also people who oppose the
theonomic position because they believe Christians are no longer
under the Old Covenant (especially as revealed by Moses), but
rather under the New Covenant instituted by Christ. Whether
speaking of dispensations or covenants, such critics believe that
theonomic ethics is perpetuating something (the law) which God’s
redemptive plan in history has made outdated.

Dispensational Reasoning About
the Nature of the Law

If anything should be obvious when one studies the theonomic
position, it is that theonomy stands diametrically opposed to the
theological school of dispensationalism and its method of ap-
proaching the Old Testament scriptures. As Lightner says, “As
systems of theology, dispensational theology and covenant theol-
ogy stand in sharp contrast. This contrast begins with a different
hermeneutic employed by each.”! He later notes: “Dispensation-
alists believe the Law of Moses in its entirety has been done away
as a tule of life. This strikes at the very heart of theonomy in
particular and of covenant Reformed theology in general.”? More

1, Robert P. Lightner, “Theonomy and Dispensationalism,” Bibliotheca Sacra,
vol. 143 (Jan-March, 1986), p. 32.

2. Lightner, “A Dispensational Response to Theonomy,” Bibliotheca Sacra, vol.
143 (July-Sept., 1986), p. 235, In dispensational terminology, Lightner says the law

75

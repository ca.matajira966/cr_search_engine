PARTI

AN ESCHATOLOGY OF DOMINION

Here shalt thou sit incarnate, here shalt reign

Both God and Man, Son both of God and Man,
Anointed universal King; all power

I give thee, reign forever, and assume

Thy merits; under thee as Head Supreme

Thrones, Princedoms, Powers, Dominions I reduce:
All knees to thee shall bow, of them that bide

In Heaven, or Earth, or under Earth in Hell.

John Milton, Peradise Lost [3.315-22]

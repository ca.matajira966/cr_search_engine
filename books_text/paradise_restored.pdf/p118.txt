108 Paradise Restored

Paul also foresaw that heresy would infect the churches of
Asia Minor. Calling together the elders of Ephesus, he exhorted
them to “be on guard for yourselves and for all the flock,”
because “I know that after my departure savage wolves will
come in among you, not sparing the flock; and from among
your own selves men will arise, speaking perverse things, to
draw away the disciples after them” (Acts 20:28-30). Just as
Paul predicted, false doctrine became an issue of enormous pro-
portions in these churches. By the time the Book of Revelation
was written, some of them had become almost completely ruined
through the progress of heretical teachings and the resulting
apostasy (Rev. 2:2, 6, 14-16, 20-24; 3:1-4, 15-18).

But the problem of heresy was not limited to any geographi-
cal or cultural area. It was widespread, and became an increas-
ing subject of apostolic counsel and pastoral oversight as the age
progressed. Some heretics taught that the final Resurrection had
already taken place (2 Tim. 2:18), while others claimed that
resurrection was impossible (1 Cor. 15:12); some taught strange
doctrines of asceticism and angel-worship (Col. 2:8, 18-23; 1
Tim. 4:1-3), while others advocated all kinds of immorality and
rebellion in the name of “liberty” (2 Pet. 2:1-3, 10-22; Jude 4, 8,
10-13, 16). Again and again the apostles found themselves issu-
ing stern warnings against tolerating false teachers and “false
apostles” (Rom, 16:17-18; 2 Cor. 11:3-4, 12-15; Phil. 3:18-19; 1
Tim. 1:3-7; 2 Tim. 4:2-5), for these had been the cause of
massive departures from the faith, and the extent of apostasy
was increasing as the era progressed (1 Tim. 1:19-20; 6:20-21; 2
Tim. 2:16-18; 3:1-9, 13; 4:10, 14-16). One of the last letters of the
New Testament, the Book of Hebrews, was written to an entire
Christian community on the very brink of wholesale abandon-
ment of Christianity. The Christian church of the first genera-
tion was not only characterized by faith and miracles; it was also
characterized by increasing lawlessness, rebellion, and heresy
from within the Christian community itself—just as Jesus had
foretold in Matthew 24,

The Antichrist

The Christians had a specific term for this apostasy. They
called it antichrist. Many popular writers have speculated about
this term, usually failing to regard its usage in Scripture. In the

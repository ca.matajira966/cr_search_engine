When did people begin to abandon the worship of
idols, unless it were since the very Word of God came
among men? When have oracles ceased and become
void of meaning, among the Greeks and everywhere,
except since the Saviour has revealed Himself on
earth? When did those whom the poets call gods and
heroes begin to be adjudged as mere mortals, except
when the Lord took the spoils of death and preserved
incorruptible the body He had taken, raising it from
among the dead? Or when did the deceitfulness and
madness of daemons fall under contempt, save when
the Word, the Power of God, the Master of all these
as well, condescended on account of the weakness of
mankind and appeared on earth? When did the prac-
tice and theory of magic begin ta be spurned under
foot, if not at the manifestation of the Divine Word
to men? In a word, when did the wisdom of the
Greeks become foolish, save when the true Wisdom
of God revealed Himself on earth? In old times the
whole world and every place in it was led astray by the
worship of idols, and men thought the idols were the
only gods that were. But now all over the world men
are forsaking the fear of idols and taking refuge with
Christ; and by worshipping Him as God, they come
through Him to know the Father also, Whom formerly
they did not know.

St. Athanasius, On the Incarnation [46]

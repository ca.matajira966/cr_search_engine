120 Paradise Restored

anything to be written to you. For you yourselves know that the
Day of the Lord will come like a thief in the night. While they are
saying, “Peace and safety!” then destruction will come upon
them suddenly like birth pangs upon a woman with child; and
they shall not escape. But you, brethren, are not in darkness,
that the day should overtake you like a thief; for you are all sons
of light and sons of day... . For God has not destined us for
wrath, but for obtaining salvation through our Lord Jesus
Christ (1 Thess. 5:1-5, 9).

Paul expanded upon this in his second letter to the same
church:

For after all it is a righteous thing for God to repay with tribula-
tion those who trouble you, and to give relief to you who are
troubled and to us as well when the Lord Jesus shall be revealed
from heaven with His mighty angels, in flaming fire dealing out
verigeance to those who do not know God and to those who do
not obey the gospel of our Lord Jesus Christ. These shall be
punished with everlasting destruction, away from the presence
of the Lord and from the glory of His power, when He comes to
be glorified in His saints on that day, and to be marveled at
among all who have believed (2 Thess. 1:6-10).

Clearly, Paul is not talking about Christ’s final coming at the
end of the world, for the coming “tribulation” and “vengeance”
were specifically aimed at thase who were persecuting the
Thessalonian Christians of the first generation. The coming day
of judgment was not something thousands of years away. It was
near —so near that they could see it coming. Most of the “signs
of the end” were in existence already, and the inspired apostles
encouraged the Church to expect the End at any moment. Paul
urged the Christians in Rome to persevere in godly living,
“knowing the time, that it is already the hour for you to awaken
from sleep; for now our salvation is nearer than when we first
believed. The night is almost gone, and the day is at hand. Let
us therefore lay aside the deeds of darkness and put on the ar-
mor of light” (Rom. 13:11-12). As the old age had been charac-
terized by sin, despair, and bondage to Satan, the new age
would be increasingly characterized by righteousness and the
universal reign of the Kingdom. For the period of the “last days”
was also the time when the Kingdom of heaven was inaugurated

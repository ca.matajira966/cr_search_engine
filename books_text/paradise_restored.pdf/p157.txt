The Consummation of the Kingdom 147

tule to the farthest corners of the earth. Finally, the Day will
come when Christ’s actual conquest of the world is complete,
when all enemies have been abolished. This will be the End,
when “at the name of Jesus every knee shall bow . . . and every
tongue shall confess that Jesus Christ is Lord, to the glory of
God the Father” (Phil. 2:10-11).

Fifth, underscoring the fact that the Resurrection occurs at
the end of the Millennium, Paui says that “the last enemy that
will be abolished is death.” Christ’s present reign will witness the
gradual abolition of all enemies, the progressive defeat of every
remnant of Adam’s rebellion, until only one thing remains to be
destroyed: Death. At that moment Christ will return in glary to
raise the dead and to transform the bodies of His people into the
perfection of the completed new Creation. Later in this passage,
Paul elaborates on this fact:

Behold, I tell you a mystery: we shall not all sleep, but we shall
all be changed, in a moment, in the twinkling of an eye, at the
last trumpet; for the trumpet will sound, and the dead will be
raised imperishable, and we shall be changed. For this perishable
Must put on the imperishable, and this mortal must put on im-
mortality. But when this perishable will have put on the im-
perishable, and this mortal will have put on immortality, then
will come about the saying that is written, “Death is swallowed
up in victory” (1 Cor. 15:51-54),

This is paralleled by Paul’s other great statement on the
Resurrection:

For if we believe that Jesus died and rose again, even so God will
bring with Him those who have fallen asleep in Jesus. For this
we say to you by the word of the Lord, that we who are alive,
who remain until the coming of the Lord, shail not precede those
who have fallen asleep. For the Lord Himself will descend from
heaven with a shout, with the voice of the archangel, and with
the trumpet of God; and the dead in Christ shall rise first. Then
we who are alive, who remain, shall be caught up together with
them in the clouds to meet the Lord in the air, and thus we shall
always be with the Lord (1 Thess, 4:14-17).

As Paul’s words so clearly state, the events of the Last Day
include the Second Coming, the Resurrection, and the “Rap-

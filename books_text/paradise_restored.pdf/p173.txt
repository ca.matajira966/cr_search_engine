The Time fs at Hand 163

fully, with all its implications. Faith in Jesus Christ requires ab-
solute submission to His Lordship, at every point, with no com-
promise. The confession of Christ meant conflict with statism,
particularly in the provinces where official worship of Caesar
was required for the transaction of everyday affairs. Failure to
acknowledge the claims of the State would result in economic
hardship and ruin, and often imprisonment, torture and death.

Some Christians compromised: “Sure, Jesus is God. I wor-
ship Him at church and in private devotions. But I can still keep
my job and my union status, even though they require me to
give technical homage to pagan deities. It’s a mere detail: after
all, [ still believe in Jesus in my heart... .” But Christ’s Lord-
ship is universal, and the Bible makes no distinction between
heart and conduct. Jesus is Lord of all. To acknowledge Him
truly as Lord, we must serve Him everywhere. This is the
primary message of the Revelation, and that which the Chris-
tians in Asia desperately needed to hear. They lived in the very
heart of Satan’s throne, the seat of Emperor-worship; John
wrote to remind them of their true King, of their position with
Him as kings and priests, and of the necessity to persevere in
terms of His sovereign Word.

Subject

The purpose of the Revelation was to reveal Christ as Lord
to a suffering Church. Because they were being persecuted, the
early Christians could be tempted to fear that the world was get-
ting out of hand—that Jesus, who had claimed “all authority

. in heaven and on earth” (Matt. 28:18), was not really in
control at all. The apostles often warned against this man-
centered error, reminding the people that God’s sovereignty is
over all of history (including our particular tribulations). This
was the basis for some of the most beautiful passages of com-
fort in the New Testament (e.g. Rom. 8:28-39; 2 Cor. 1:3-7;
4:7-15).

John’s primary concern in writing the Book of Revelation
was just this very thing: to strengthen the Christian community
in the faith of Jesus Christ’s Lordship, to make them aware that
the persecutions they suffered were integrally involved in the
great war of history. The Lord of glory had ascended His
throne, and the ungodly rulers were now resisting His authority

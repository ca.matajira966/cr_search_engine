For the Lord touched all parts of creation, and
freed and undeceived them all from every deceit. As
St. Paul says, “Having put off from Himself the prin-
cipalities and the powers, He triumphed on the cross”
[Col. 2:15], so that no one could possibly be any
longer deceived, but everywhere might find the very
Word of God.

St. Athanasius, On the Incarnation [45]

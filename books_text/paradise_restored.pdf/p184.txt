Therefore blessed Moses of old time ordained the
great feast of the Passover, and our celebration of it,
because, namely, Pharaoh was killed, and the people
were delivered from bondage. For in those times it
was especially, when those who tyrannized over the
people had been slain, that temporal feasts and
holidays were observed in Judea.

Now, however, that the devil, that tyrant against
the whole world, is slain, we do not approach a tem-
poral feast, my beloved, but an eternal and heavenly,
Not in shadows do we show it forth, but we come to it
in truth. For they being filled with the flesh of a dumb
lamb, accomplished the feast, and having anointed
their door-posts with the blood, implored aid against
the destroyer. But now we, eating of the Word of the
Father, and having the lintels of our hearts sealed with
the blood of the New Testament, acknowledge the
grace given us from the Saviour, Who said, “Behold, I
have given unto you to tread upon serpents and scor-
pions, and over all the power of the enemy” [Luke
10:19]. For no more does death reign; but instead of
death henceforth is life, since our Lord said, “I am the
life” [John 14:6]; so that everything is filled with joy
and gladness; as it is written, “The Lord reigneth, let
the earth rejoice” [Ps, 97:1].

St. Athanasius, Letters [iv]

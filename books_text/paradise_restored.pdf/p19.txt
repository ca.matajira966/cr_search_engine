The Hope

incessantly call us to do battle against the forces of evil, and
they promise us that we shall inherit the earth. When the
Church sang the Psalms—not just little snatches of them, but
comprehensively, through the whole Psalter —she was strong,
healthy, aggressive, and could not be stopped. That is why the
devil has sought to keep us from singing the Psalms, to rob us of
our inheritance. If we are to recapture the eschatology of do-
minion, we must reform the Church; and a crucial aspect of that
reformation should be a return to the singing of Psalms. Listen

to the historic hymns of the victorious Church:

All the ends of the earth will remember and turn to the Lorp,
And all the families of the nations will worship before Thee.
(Ps. 22:27)

For evildoers will be cut off,

But those who wait for the Lorn, they will inherit the earth.
Yet a little while, and the wicked man will be no more;

And you will look carefully for his place, and he will not be.
But the meek will inherit the earth,

And will delight themselves in abundant prosperity.

(Ps. 37:9-11)

Come, behold the works of the Lory,

Who has wrought desolations in the earth.

He makes wars to cease to the end of the earth;
He breaks the bow and cuts the spear in two;
He burns the chariots with fire.

Cease striving and know that I am God;

i will be exalted among the nations,

I will be exalted in the earth. (Ps. 46:8-10)

O clap your hands, all peoples;

Shout to God with the voice of joy.

For the Lora Most High is to be feared,
A great King over all the earth.

He subdues peoples under us,

And nations under our feet. (Ps, 47:1-3)

All the earth will worship Thee,
And will sing praises to Thee;
They will sing praises to Thy name. (Ps. 66:4)

He will rule from sea to sea,
And from the River to the ends of the earth.

The Saviour works mightily every day, drawing
men to religion, persuading them to virtue, teaching
them about immortality, quickening their thirst for
heavenly things, revealing the knowledge of the
Father, inspiring strength in the face of death, mani-
festing Himself to each, and displacing the irreligion
of idols; while the gods and evil spirits of the unbe-
lievers can do none of these things, but rather become
dead at Christ’s presence, all their ostentation barren
and void. By the sign of the cross, on the contrary, all
magic is stayed, all sorcery confounded, all the idols
are abandoned and deserted, and all senseless pleas-
ure ceases, as the eye of faith looks up from earth to
heaven.

St. Athanasius, On the Incarnation [311

2
HOW TO READ PROPHECY

I began my personal journey toward the eschatology of do-
minion one evening in church, about a dozen years ago. The
pastor, a preacher famous for his expository method of Bible
teaching, had just begun a series on prophecy. As he eloquently
defended his eschatology of defeat, I was struck by the fact that
he seemed utterly unable to develop his views organically from
the Bible. Oh, he quoted some Scripture—a verse here, a verse
there. But he was never able to show that his explanation of the
future fit in with the overall pattern of the Bible. In other words,
he was very adept at imposing his views of reality upon the
Biblical text, making sure his verses were shuffled together in the
proper order. But he could not show how his doctrines flowed
out of Scripture; his eschatology did not seem to be an organic
part of the Story which the Bible tells.

What I began to realize that night was that the way to
recover the Biblical eschatology must be through an under-
standing of the Biblical Story. Instead of trying to fit the Bible
into a prearranged pattern, we must try to discover the patterns
that are already there. We must allow the Bible’s own structure
to arise from the text itself, to impose itself upon our own un-
derstanding. We must become accustomed to the Biblical
vocabulary and modes of expression, seeking to shape our own
thinking in terms of Scriptural categories.

This perspective sheds valuable light on the old debate about
“literal” versus “symbolic” interpretations. To a great degree,
that debate is beside the point; for the fact is that all interpreters
are “literalists” on some points and “symbolists” on others.

For example, I am looking at a recent commentary on Reve-
Jation, written by a well-known evangelical scholar. The back
cover boldly proclaims: This may be the most literal exposition

i3

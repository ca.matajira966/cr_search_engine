242 Paradise Restored

Scythopolis, and preferred their own safety before their relation to us;
they fought against their own countrymen; nay, their alacrity was so
very great that those of Scythopolis suspected them. These were afraid
therefore, lest they should make an assault upon the city in the night-
time, and to their great misfortune, should thereby make an apology
for themselves to their own people for their revolt from them. So they
commanded them, that in case they would confirm their agreement
and demonstrate their fidelity to them, who were of a different nation,
they should go out of the city, with their families, to a neighboring
grove: and when they had done as they were commanded, without
suspecting anything, the people of Scythopolis lay still for the interval
of two days, to tempt them to be secure, but on the third night they
watched their opportunity, and cut all their throats, some of them as
they lay unguarded, and some as they lay asleep. The number that was
slain was above thirteen thousand, and then they plundered them of
all that they had.

4. lt will deserve our relation what befell Simon: he was the son of
one Saul, a man of reputation among the Jews. This man was
distinguished from the rest by the strength of his body, and the
baldness of his conduct, although he abused them both to the
mischieving of his countrymen: for he came every day and slew a great
many of the Jews of Scythopolis, and he frequently put them to flight,
and became himself alone the cause of his army’s conquering. But a
just punishment overtook him for the murders he had committed
upon those of the same nation with him: for when the people of
Scythopolis threw their darts at them in the grove, he drew his sword,
but did not attack any of the enemy; for he saw that he could do
nothing against such a multitude; but he cried out, after a very moving
manner and said, —“O you people of Scythopolis, I have deservedly
suffered for what I have done with relation to you, when I gave you
such security of my fidelity to you, by slaying so many of those that
were related to me. Wherefore we very justly experience the perfidi-
ousness of foreigners while we acted after a most wicked manner
against our own nation. I will therefore die, polluted wretch as I am,
by mine own hands: for it is nat fit I should die by the hands of our
enemies; and tet the same action be to me both a punishment for my
great crimes, and a testimony of my courage to my commendation,
that so no one of our enemies may have it to brag of, that he it was
that slew me; and no one may insult upon me as I fall.”

Now when he had said this, he looked round about him upon his
family with eyes of commiseration, and of rage (that family consisted
of a wife and children, and his aged parents); so, in the first place he
caught his father by his gray hairs, and ran his sword through him

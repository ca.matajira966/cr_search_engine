Josephus on the Fall of Jerusalem 247

although they were surprised at their onset, and at their good order,
made resistance against their attacks for a little while; but when they
were pricked with their long poles, and overborne by the violent noise
of the horsemen, they came to be trampled under their feet; many also
of them were slain on every side, which made them disperse them-
selves and run to the city, as fast as every one of them was able. So
Titus pressed upon the hindmost, and slew them; and of the rest, some
he fell upon as they stood on heaps, and some he prevented, and met
them in the mouth, and run them through; many also he leaped upon
as they fell one upon another, and trod them down, and cut off all the
retreat they had to the wall, and turned them back into the plain, till at
last they forced a passage by their multitude, and got away, and ran
into the city.

The Sea of Galilee ‘‘Full of Dead Bodies”?
Giisx:9)

9. . . . Now those which were driven into the lake could neither fly
to the land, where all was in their enemies hand, and in war against
them, nor could they fight upon the level by the sea, for their ships
were small and fitted only for piracy; they were too weak to fight with
Vespasian’s vessels, and the mariners that were in them were so few,
that they were afraid to come near the Romans, who attacked them in
great numbers. However, as they sailed round about the vessels, and
sometimes as they came near them, they threw stones at the Romans
when they were a good way off, or came closer and fought them; yet
did they receive the greatest harm themselves in both cases. As for the
stones they threw at the Romans they only made a sound one after
another, for they threw them against such as were in their armor, while
the Roman darts could reach the Jews themselves; and when they ven-
tured to come near the Romans, they became sufferers themselves be-
fore they could do any harm to the other, and were drowned, they and
their ships together.

As for those that endeavored to come to an actual fight, the
Romans ran many of them through with their long poles. Sometimes
the Romans leaped into their ships, with swords in their hands, and
slew them; but when some of them met the vessels, the Romans caught
them by the middle, and destroyed at once their ships and themselves
who were taken in them. And for such as were drowning in the sea, if
they lifted their heads up above the water they were either killed by
darts, or caught by the vessels; but if, in the desperate case they were
in, they attempted to swim, to their enemies, the Romans cut off either
their heads or their hands; and indeed they were destroyed after
various manners everywhere, till the rest, being put to flight, were

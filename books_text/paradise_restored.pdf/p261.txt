Josephus on the Fail of Jerusalem 251

expose themselves to danger, would do il in the day-time: and there
were twelve thousand of the better sort who perished in this manner,

4, And now these zealots and Idumeans were quite weary of barely
killing men, so they had the impudence of setting up fictitious
tribunals and judicatures for that purpose; and as they intended to
have Zacharias,’* the son of Baruch, one of the most eminent of the
citizens, slain, —so what provoked them against him was, that hatred
of wickedness and love of liberty which were so eminent in him: he
was also a rich man, so that by taking him off, they did not only hope
to seize his effects, but also to get rid of a man that had great power to
destroy them. So they called together, by a public proclamation,
seventy of the principal men of the populace, for a show, as if they
were real judges, while they had no proper authority. Before these was
Zacharias accused of a design to betray their polity to the Romans,
and having traitorously sent to Vespasian for that purpose. Now there
appeared no proof or sign of what he was accused; but they affirmed
themselves that they were well persuaded that so it was, and desired
that such their affirmation might be taken for sufficient evidence.

Now when Zacharias clearly saw that there was no way remaining
for his escape from them, as having been treacherously called before
them, and then put in prison, but not with any intention of a legal
trial, he took great liberty of speech in that despair of life he was
under. Accordingly he stood up, and laughed at their pretended ac-
cusation, and in a few words confuted the crimes laid to his charge;
after which he turned his speech to his accusers, and went over dis-
tinctly all their transgressions of the law, and made heavy lamenta-
tions upon the confusion they had brought public affairs to: in the
mean time the zealots grew tumultuous, and had much ado to abstain
from drawing their swords, although they designed to preserve the ap-
pearance and show of judicature to the end. They were also desirous,
on other accounts, to try the judges, whether they would be mindful
of what was just at their own peril.

Now the seventy judges brought in their verdict, that the person
accused was not guilty,—as choosing rather to die themselves with
him, than to have his death Jaid at their doors; hereupon there rose a
great clamor of the zealots upon his acquittal, and they all had in-
dignation at the judges, for not understanding that the authority that
was given them was but in jest. So two of the boldest of them fell upon
Zacharias in the middle of the temple, and slew him; and as he fell
down dead they bantered him, and said, “Thou hast also our verdict,
and this will prove a more sure acquittal to thee than the other.” They
also threw him down out of the temple immediately into the valley
beneath it. Moreover, they struck the judges with the backs of their

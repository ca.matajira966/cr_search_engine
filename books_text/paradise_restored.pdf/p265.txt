Josephus on the Fall of Jerusalem 255

zealots, and drove the rest before them into that royal palace that was
built by Grapte, who was a relation of Izates, the king of Adiabene;
the Idumeans fell in with them and drove the zealots out thence into
the temple, and betook themselves to plunder John’s effects; for both
he himself was, in that palace, and therein had he laid up the spoils he
had acquired by his tyranny. In the mean time the multitude of those
zealots that were dispersed over the city ran together to the temple unto
those that had fled thither, and John prepared to bring them down
against the people and the Idumeans, who were not so much afraid of
being attacked by them, (because they were themselves better soldiers
than they), as at their madness, lest they should privately sally out of
the temple and get among them, and not only destroy them, but set the
city on fire also. So they assembled themselves together, and the high-
priests with them, and took counsel after what manner they should
avoid their assault.

Now it was God who turned their opinions to the worst advice, and
thence they devised such a remedy to get themselves free, as was worse
than the disease itself. Accordingly, in order to overthrow John, they
determined to admit Simon, and earnestly to desire the introduction
of a second tyrant into the city: which resolution they brought to
perfection, and sent Matthias, the high-priest, to beseech this Simon
to come into them, of whom they had so often been afraid. Those also
that had fied from the zealots in Jerusalem joined in this request to
him, out of the desire they had of preserving their houses and their
effects. Accordingly he, in an arrogant manner, granted them his lordly
protection, and came into the city, in order to deliver it from the zeal-
ots. The people also made joyful acclamations to him, as their saviour
and their preserver; but when he was come in with his army, he took
care to secure his own authority, and looked upon those that invited
him to be no less his enemies than those against whom the invitation
was intended,

Lakes of Blood in the Temple
(vsi:3-5)

3. But now the tyrant Simon, the son of Gioras, whom the people
had invited in, out of the hopes they had of his assistance in the great
distresses they were in, having in his power the upper city, and a great.
part of the lower, did now make more vehement assaults upon John
and his party, because they were fought against from above also; yet
was he beneath their situation, when he attacked them, as they were
beneath the attacks of the others above them. Whereby it came to
pass, that John did both receive and inflict great damage, and that
easily, as he was fought against on both sides; and the same advantage

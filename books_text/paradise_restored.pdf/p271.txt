Josephus on the Fall of Jerusalem 261

called Epiphanes, lay before this city, and had been guilty of many in-
dignities against God, and our forefathers met him in arms, they then
were Slain in the battle, this city was plundered by our enemies, and
our sanctuary made desolate for three years and six months.?%

“And what need I bring any more examples! Indeed, what can it be
that hath stirred up an army of the Romans against our nation? Is it
not the impiety of the inhabitants? Whence did our servitude com-
Mence? Was it not derived from the seditions that were among our
forefathers, when the madness of Aristobulus and Hyrcanus, and our
mutual quarrels, brought Pompey upon this city, and when God re-
duced those under subjection to the Romans, who were unworthy of
the liberty they enjoyed? After a siege, therefore of three months, they
were forced to surrender themselves, although they had been guilty of
such offences with regard to our sanctuary and our laws, as you have,
and this while they had much greater advantages to go to war than you
have. Do not we know what end Antigonus, the son of Aristobulus,
came to, under whose reign God provided that this city should be
taken again upon account of the people’s offences? When Herod, the
son of Antipater, brought upon us Sosius, and Sosius brought upon us
the Roman army, they were then encompassed and besieged for six
months, till, as a punishment for their sins, they were taken, and the
city was plundered by the enemy.

“Thus it appears, that arms were never given to our nation but that
we are always given up to be fought against, and to be taken; for I sup-
pose, that such as inhabit this holy place ought to commit the disposal
of all things to God, and then only to disregard the assistance of men
when they resign themselves up to their arbitrator, who is above. As for
you, what have you done of those things that are recommended by our
legislator! and what have you not done of those things that he hath con-
demned! How much more impious are you than those who were so
quickly taken! You have not avoided so much as those sins which are
usually done in secret; I mean thefts, and treacherous plots against men,
and adulteries. You are quarrelling about rapines and murders, and in-
vent strange ways of wickedness. Nay, the temple itself has become the
receptacle of all, and this divine place is polluted by the hands of thase
of our own country; which place hath yet been reverenced by the
Romans when it was at a distance from them, when they have suffered
many of their own customs to give place to our law.3¢

“And, after all this, do you expect Him whom you have so im-
piously abused to be your supporter? To be sure then you have a right
to be a petitioners, and to call upon Him to assist you, so pure are
your hands! Did your king [Hezekiah] lift up such hands in prayer to
God against the king of Assyria, when he destroyed that great army in

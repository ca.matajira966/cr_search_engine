270 Paradise Restored

I suppose, that had the Romans made any longer delay in coming
against these villains, the city would either have been swallowed up by
the ground opening upon them, or been overflowed by water, or else
been destroyed by such thunder as the country of Sodom perished by,
for it had brought forth a generation of men much more atheistical
than were those that suffered such punishments; for by their madness
it was that all the people came to be destroyed.

Jerusalem Becomes a Wilderness
Git)

1. Thus did the miseries of Jerusalem grow worse and worse every
day, and the seditious were still more irritated by the calamities they
were under, even while the famine preyed upon themselves, after it
had preyed upon the people. And indeed the multitude of carcases
that lay in heaps one upon another, was a horribie sight, and produced
a pestilential stench, which was a hindrance to those that would make
sallies out of the city and fight the enemy: but as those were to go in
battle-array, who had been already used to ten thousand murders, and
must tread upon those dead bodies as they marched along, so were not
they terrified, nor did they pity men as they marched over them; nor
did they deem this affront offered to the deceased to be any ill omen to
themselves; but as they had their right hands already polluted with the
murders of their own countrymen, and in that condition ran out to
fight with foreigners, they seem to me to have cast a reproach upon
God himself, as if he were too slow in punishing them; for the war was
not now gone on with as if they had any hope of victory; for they
gloried after a brutish manner in that despair of deliverance they were
already in.

And now the Romans, although they were greatly distressed in get-
ting together their materials, raised their banks in one-and-twenty
days, after they had cut down all the trees that were in the country that
adjoined to the city, and that for ninety furlongs round about, as I
have already related. And, truly, the very view itself of the country
was a melancholy thing; for those places which were before adorned
with trees and pleasant gardens were now become a desolate country
every way, and its trees were all cut down: nor could any foreigner that
had formerly seen Judea and the most beautiful suburbs of the city,
and now saw it as a desert, but lament and mourn sadly at so great a
change; for the war had laid all signs of beauty quite waste; nor, if any
one that had known the place before, had come on a sudden to it now,
would he have known it again; but though he were at the city itself, yet
would he have inquired fer it notwithstanding.

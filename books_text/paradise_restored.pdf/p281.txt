Josephus on the Fall of Jerusalem 271

“dt Is God Himself Who Is Bringing This Fire’’
(visiiz1)

1. And now Titus gave orders to his soldiers that were with him to
dig up the foundations of the tower of Antonia, and make him a ready
passage for his army to come up; while he himself had Josephus
brought to him (for he had been informed that on that very day, which
was the seventeenth day of Panemus [Tamuz], the sacrifice called “the
Daily Sacrifice” had failed, and had not been offered to God for want
of men to offer it, and that the people were grievously troubled at it)
and commanded him to say the same things to John that he had said
before, that if he had any malicious inclination for fighting, he might
come out with as many of his men as he pleased, in order to fight,
without the danger of destroying either his city or temple; but that he
desired he would not defile the temple, nor thereby offend against
God. That he might, if he pleased, offer the sacrifices which were now
discontinued, by any of the Jews whom he should pitch upon. Upon
this, Josephus stood in such a place where he might be heard, not by
John only, but by many more, and then declared to them what Caesar
had given him in charge, and this in the Hebrew language. So he
earnestly prayed them to spare their own city, and to prevent that fire
that was just ready to seize upon the temple, and to offer their usual
sacrifices to God therein. At these words of his a great sadness and
silence were observed among the people. But the tyrant himself cast
many reproaches upon Josephus, with imprecations besides; and at
last added this withal, that he did never fear the taking of the city,
because it was God’s own city.

In answer to which, Josephus said thus, with a loud voice: —“To be
sure, thou hast kept this city wonderfully pure for God’s sake! the
temple also continues entirely unpolluted! Nor hast thou been guilty
of any impiety against him, for whose assistance thou hopest! He still
receives his accustomed sacrifices! Vile wretch that thou art! if any one
should deprive thee of thy daily food, thou wouldst esteem him to be
an enemy to thee; but thou hopest to have that God for thy supporter
in this war whom thou hast deprived of his everlasting worship! and
thou imputest those sins to the Romans, who to this very time take
care to have our laws observed, and almost compel these sacrifices to
be still offered to God, which have by thy means been intermitted!
Who is there that can avoid groans and lamentations at the amazing
change that is made in this city? since very foreigners and enemies do
now correct thal impiety which thou hast occasioned; while thou, who
art a Jew, and was educated in our laws, art become a greater enemy to
them than the others! Bur still, John, it is never dishonorable to re-

 

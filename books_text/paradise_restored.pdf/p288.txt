278 Paradise Restored

outer [court of the] temple, whither the women and children, and a
great mixed multitude of the people fled, in number about six thou-
sand. But before Caesar had determined anything about these people,
or given the commanders any orders relating to them, the soldiers
were jn such a rage, that they set the cloister on fire; by which means it
came to pass that some of these were destroyed by throwing them-
selves down headlong, and some were burnt in the cloisters them-
selves. Nor did any one of them escape with his life.

A false prophet was the occasion of these people's destruction,
who had made a public proclamation in the city that very day, that
God commanded them to get up upon the temple, and that there they
should receive miraculous signs of their deliverance, Now, there was
then a great number of false prophets suborned by the tyrants to im-
pose upon the people, who denounced this to them, that they should
wait for deliverance from God: and this was in order to keep them
from deserting, and that they might be buoyed up above fear and care
by such hopes. Now, a man that is in adversity does easily comply with
such promises; for when such a seducer makes him believe that he
shall be delivered from those miseries which oppress him, then it is
that the patient is full of hopes of such deliverance.

Chariots in the Clouds
(virv:3)

3, Thus were the miserable people persuaded by these deceivers,
and such as belied God himself; while they did not attend, nor give
credit, to the signs that were so evident and did so plainly foretell their
future desolation; but, like men infatuated, without either eyes to see,
or minds to consider, did not regard the denunciations that God made
to them. Thus there was a star resembling a sword, which stood over
the city, and a comet, that continued a whole year. Thus also, before
the Jews’ rebellion, and before those commotions which preceded the
war, when the people were come in great crowds to the feast of
unleavened bread, on the eighth day of the month Xanthicus [Nisan],
and at the ninth hour of the night, so great a fight shone round the
altar and the holy house, that it appeared to be bright day time; which
fight lasted for half an hour. This light seemed to be a good sign to the
unskilful, but was so interpreted by the sacred scribes, as to portend
those events that followed immediately upon it. At the same festival
also, a heifer, as she was led by the high-priest to be sacrificed, brought
forth a lamb in the midst of the temple.

Moreover, the eastern gate of the inner [court of the] temple,
which was of brass, and vastly heavy, and had been with difficulty shut
by twenty men, and rested upon a basis armed with iron, and had

288 Paradise Restored

7. In the spring of a.p. 67, after Nero had received news of the Judean
revolt, he appointed Vespasian commander of the Roman forces to subdue the
rebellion, (Vespasian became the Roman Emperor in a.p. 69, after the turmoil
following Nero’s death.) The event described here refers to Vespasian’s
reprisals against an attempt by Jewish forces to capture the city of Sepphoris
from the Romans.

8. This important stronghold of the Jewish rebels was destroyed by the
Romans on 20 July, 4.0. 67.

9. See Note 7.

10. According to Greek mythology, Andromeda was the daughter of
Kepheus, king of Ethioy She had been bound to a rock in order to be
devoured by a sea monster, but was rescued in the nick of time by the hero
Perseus.

1. The city was another imporiant rebel base, Tarichaeae (also called
Magdala, the home of Mary Magdalene).

12. Titus was the older son of Vespasian, who assisted his father in the
Jewish War. Later, when Vespasian became emperor, Titus took over the direc-
tion of the campaign.

13. The Sea of Galilee was also called Lake Gennesareth. The massacre
recorded here resulted from the Romans’ attempt to catch and destroy the
Jews who were trying to cscape from Tarichacac. This battle took place in late
September 67.

i4. During the civil strife in Jerusalem, the rebellious Zealots barricaded
themselves in the Temple against the pro-Roman citizens, who surrounded the
Temple with armed guards. A few Zealots escaped in the night and made their
way to the camp of the Edomites (idumeans) who had surrounded the city
with 20,000 men, By telling them (falsely) that the priests were planning to sur-
render the city to the Romans, the Zealots persuaded the Edomites to liberate
their comrades from the Temple and then to attack the rest of the city. That
night, before the Edomites went on the rampage, was the last opportunity for
people to escape from the city with safety.

1S. The Edomites were descendants of Esau, Jacob’s brother (Gen. 25:30;
46:8-43), and thus related to the Israelites.

16. Ananus was the High Priest.

17. Jesus, son of Gamalus, was a chief priest, second under Ananus.

18. Cf. Matt. 23:35.

19, Realizing they had been tricked, most of the Edomites left the city.
Meanwhile, the Romans continued their slow advance through Judea, coun-
ting on the internal warfare in Jerusalem to weaken the rebellion. Many Jews
tried to escape from the coming holocaust; most were unsuccessful.

20, Simon was the leader of a powerful faction of rebels in Jerusalem, in
competition with the Zealots Ied by Eleazar, son of Gion, and the Galilean fol-
Jowers of John of Gischala.

21. As Josephus mentions several times in his narrative, people all over the
world knew of the Temple in Jerusalem: it was “esteemed holy by all
mankind.”

22. Cf. Luke 13:1-9.

 

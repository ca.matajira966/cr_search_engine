326 Paradise Restored

He joined the Chalcedon staff part-time around 1978. He
later became a full-time staff member. But the inevitable hap-
pened to him, just as it had happened to so many bright young
men before him. He left Chalcedon as he had come: fired with
enthusiasm.’ I hired him to produce a monthly newsletter pub-
lished by my Institute for Christian Economics in Tyler, Texas,
The Biblical Educator, in the fall of 1979. It ran for almost three
years.

In the fall of 1980, I accepted an assignment to debate Ron-
ald J. Sider (Rich Christians in an Age of Hunger) at Gordon-
Conwell Divinity School in Massachusetts. I wanted to take
along a book refuting Sider. Normally, a debater does not ex-
pect his epponent to publish a book just for an evening’s
debate, so I decided to present Sider with a unique surprise. I
called Chilton and asked him to research and write in three
months a manuscript refuting Sider, so that I could get it printed
by the following April. He did it, and we got delivery of the
books one day before the debate. I titled the book, Productive
Christians in an Age of Guilt-Manipulators, and it is now in its
fourth edition.* It has become the most successful and widely
read book that the Institute for Christian Economics has pub-
lished. Sider was surprised, to say the least. Frankly, I think he
never got over it. I hope so, anyway.

Chilton Comes to Tyler

A little over a year after Productive Christians appeared, I
hired Chilton to come to Tyler and work for my publishing com-
pany.* I wanted him to do several writing projects that I knew
he was capable of, but after an ill-fated beginning on a book
about agriculture, we both agreed that the topic should be

7. The list of names of former employees of Chalcedon is a Who’s Who in
Christian Reconstruction (also, in some cases, a “Who’s Not”): Gary North,
Greg Bahnsen, David Chilton, James Jordan, Edward Powell, Douglas Kelly,
and Kevin Craig. Now that David Chilton is back in central California, per-
haps he will become the first of us to be re-hired. The book-writing experience
and theological training that he received in Tyler makes him the “hottest theo-
logical property” in the West. If he joins Chalcedon again, those of us in Tyler
will be able to proclaim: “Tyler Boy Makes Good (Again)!”

8. Tyler, Texas: Institute for Christian Economics, 1986.

9. The American Bureau of Economic Research, the profit-seeking side of
my publishing operations. It owns Dominion Press.

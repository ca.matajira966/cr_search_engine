334 Paradise Restored

the wrong voice one day. The bottom line on dominion without
covenant is that we don’t get the blessing of God.”

eee

If I, as the publisher of Paradise Restored and The Days of
Vengeance, did not warn readers against making too much of
the idea of dominion, as such, I would be abusing my authority.
As Christians, we need to preach dominion, but dominion must
always be on God’s terms, not man’s. Dominion is always by
covenant. I should have written this in an earlier edition, but
Sutton had not yet worked out the details of his thesis on the
covenant. Now he has. If you take seriously the optimistic
Christianity expressed in Paradise Restored, then you need also
to read Sutton’s book, That You May Prosper: Dominion By
Covenant ($12.50). The book is published by the non-profit In-
stitute for Christian Economics, of which I am the unpaid presi-
dent. I do not make a penny on the book. I am recommending it
only because I truly believe its message. Order from:

Institute for Christian Economics
P.O. Box 8000
Tyler, Texas 75711

21. Covenant Renewal, Vol. 1, No, 1 January 1987). Published by the Insti-
tute for Christian Economics.

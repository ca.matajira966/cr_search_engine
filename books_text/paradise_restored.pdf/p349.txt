What Is the ICE? 339

reconstruction of ancient history.

The purpose of the ICE is to relate biblical ethics to Christian
activities in the field of economics. ‘Io cite the title of Francis
Schaeffer's book, “How should we then live?” How should we
apply biblical wisdom in the field of economics to our lives, our
culture, our civil government, and our businesses and callings?

If God calls men to responsible decision-making, then He
must have standards of righteousness that guide men in their
decision-making. It is the work of the ICE to discover, illumi-
nate, explain, and suggest applications of these guidelines in the
ficld of economics. We publish the results of our findings in the
newsleuers.

The ICE sends oud the newsletters free of charge. Anyone can sign
up for six months to receive them. This gives the reader the
opportunity of seeing “what we’re up to.” At the end of six
months, he or she can renew for another six months.

Donors receive a one-year subscription. This reduces the
extra trouble associated with sending out rencwal notices, and
it also means less trouble for the subscriber.

There are also donors who pledge to pay $15 a month. They
are members of the ICE’s “Reconstruction Committee.” They help
to provide a predictable stream of income which finances the
day-to-day operations of the ICE. Then the donations from
others can finance special projects, such as the publication of a
new buok,

The basic service that ICE offers is education. We are
presenting ideas and approaches to Christian ethical bchavior
that few other organizations even suspect are major problem
areas. The Christian world has for too long acted us though we were
not responsible citizens on earth, as well as citizens of heaven. (“For
our conyersation [citizenship] is in heaven” [Philippians 3:20a].)
We must be godly stewards of all our assets, which includes our lives,
minds, and skills.

Because economics affects every sphere of life, the ICE’s
reports and surveys are relevant to all areas of life. Because
Scarcity affects every area, the whole world necds to be governed
by biblical requirements for honest stewardship of the earth's
resources. The various publications are wide-ranging, since the
effects of the curse of the ground (Genesis 3:17-19) are wide-
ranging.

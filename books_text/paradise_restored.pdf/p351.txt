What Is the ICE? 341

politicians and even agitators apply to current events are
not likely to be the newest. But, soon or late, it is ideas,
not vested interests, which are dangerous for good or evil.

Do you believe this? If so, then the program of long-term
education which the ICE has created should be of considerable
interest to you. What we need are people with a vested interest in
ideas, a commitment to principle rather than class position.

There will be few short-term, visible successes for the ICE’s
program. There will be new and interesting books. There will
be a constant stream of newsletters. There will cducational
audio and video tapes. But the world is not likely to beat a path
to ICE’s door, as long as today’s policies of high taxes and
statism have not yet produced a catastrophe. We are investing
in the future, for the far side of humanism’s economic failure.
This is a long-term investment in intellectual capital. Contact us at:
ICE, Box 8000, Tyier, TX 75711.

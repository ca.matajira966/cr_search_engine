5
THE GARDEN OF THE LORD

Animals in the Garden

In Eden, before the Fall, there was no death (Rom. 5:12),
Animals were not “wild,” and Adam was able to name {.e.,
classify) the animals without fear (Gen. 2:19-20). But man’s re-
bellion resulted in terrible changes throughout the world. The
nature of animals was altered, so that they became a threat to
the peace and safety of man, The dominion over them that
Adam had exercised was lost.

In Christ, however, man’s dominion has been restored (Ps.
8:5-8 with Heb. 2:6-9). Thus, when God saved His people, this
effect of the Curse began to be reversed. He led them through a
dangerous wilderness, protecting them from the snakes and
scorpions (Deut. 8:15), and He promised them that their life in
the Promised Land would be Eden-like in its freedom from the
ravages of wild animals: “I shall also grant peace in the land, so
that you may lie down with no one making you tremble. I shall
also eliminate harmful beasts from the land” (Lev. 26:6). In fact,
this is why God did not allow Israel to exterminate the Canaanites
all at once: the heathen served as a buffer between the covenant
people and the wild animats (Ex. 23:29-30; Deut. 7:22).

Accordingly, when the prophets foretold the coming salva-
tion in Christ, they described it in the same terms of Edenic
blessing: “I will make a covenant of peace with them and elimin-
ate harmful beasts from the land, so that they may live securely
in the wilderness and sleep in the woods” (Ezek. 34:25). “No
lion will be there, nor will any vicious beast go up on it; these
will not be found there. But the redeemed will walk there” (Isa.
35:9). In fact, the Bible goes so far as to say that through the
Gospel’s permeation of the world the wild nature of the animals
will be transformed into its original, Edenic condition:

39

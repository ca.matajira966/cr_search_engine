The Caming of the Kingdom wal

The Apostle Peter understood that the meaning of the
Ascension was Christ’s enthronement in heaven. Citing a proph-
ecy of King David, Peter said:

And so, because he was a prophet, and knew that God had
sworn to him with an oath that of the fruit of his body, accord-
ing to the flesh, He would raise up the Christ to sit on his throne;
he, foreseeing this, spoke concerning the resurrection of the
Christ, that His soul was not left in Hades, nor did His flesh see
corruption. This Jesus God has raised up, of which we are all
witnesses. Therefore being exalted to the right hand of God, and
having received from the Father the promise of the Holy Spirit,
He poured out this which you now see and hear. For David did
not ascend into the heavens, but he says himself: “The Lord said
to my Lord, ‘Sit at My right hand, until 1 make Your enemies
Your footstool” ” Therefore let all the house of Israel know
assuredly that God has made this Jesus, whom you crucified,
both Lord and Christ (Acts 2:30-36),

It is crucial to understand the Bible’s own interpretation of
the throne of Christ. According to the inspired Apostle Peter,
David's prophecy of Christ being seated on a throne was not a
prophecy of some earthly throne in Jerusalem (as some today
mistakenly insist). David was prophesying about Christ's throne
in heaven. \t is the heavenly enthronement that King David
foretold, Peter told his audience on the Day of Pentecost. From
His throne in heaven, Christ is already ruling the world.

The Apostle Paul agreed: at Christ’s Ascension, he wrote,
God “seated Him at His right hand in the heavenly places, far
above all rule and authority and power and dominion, and every
name that is named, not only in this age, but also in the one to
come. And He put all things in subjection under His feet” (Eph.
1:20-22). Now, if Christ is seated now above aif rule and author-
ity and power and dominion, if aif things are now under His
feet, why are some Christians waiting for His Kingdom to
begin? According to Paul, God “delivered us from the domain
of darkness, and transferred us into the Kingdom of His beloved
Son” (Col. 1:13), The Bible says the Kingdom Aas arrived; some
modern theologians say it Aasn’t. Is there really any question
about whom we should believe?

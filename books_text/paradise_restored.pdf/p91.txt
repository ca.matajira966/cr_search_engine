The Rejection of Israel 81

wrote that “the Jews . . . killed both the Lord and their own
prophets, and have persecuted us; they do not please God and
are contrary to all men, forbidding us to speak to the Gentiles
that they may be saved, so as always to fill up the measure of
their sins; but wrath has come upon them to the uttermost”
(1 Thess. 2:14-16).

As a nation, Israel had become apostate, a spiritual harlot in
rebellion against her Husband (cf. Ezek. 16). The fearful words
of Hebrews 6:4-8 were literally applicable to the covenant na-
tion, which had forfeited its birthright:

For it is impossible for those who were once enlightened, and
have tasted of the heavenly gift, and have become partakers of
the Holy Spirit, and have tasted the good word of God and the
powers of the age to come, if they fall away, to renew them again
to repentance, since they crucify for themselves the Son of God,
and put Him to an open shame. For the earth which drinks in the
rain that often comes upon it, and bears herbs useful for those
by whom it is cultivated, receives blessing from God; but if it
bears thorns and briars, it is rejected and near to being cursed,
whose end is to be burned.

The same multitude which welcomed Jesus into Jerusalem.
with hosannas was screaming for His blood in less than a week.
Like all slaves, their attitude was fickle; but ultimately, their atti-
tude was summed up in another of Jesus’ parables: “We will not
have this Man to reign over us!” (Luke 19:14). The chief priests
revealed the faith of the nation when they vehemently denied
the lordship of Christ and affirmed, “We have no king but
Caesar!” (John 19:15),

So the covenant people inherited the Curse. They had waved
their branches toward the Owner’s Son when he entered their
vineyard, seeming to welcome Him to His rightful property; but
when He came closer and inspected the branches, He found no
fruit—just leaves. In keeping with the pattern we have seen in
our study of the Garden of Eden, Israel was ripe for becoming
judged, disinherited, and cast out of the Vineyard.

But they had not only the examples of Eden, the Flood,
Babel, and other historical judgments as warnings. God had
specifically stated, through Moses, that the Curse would fall
upon them if they apostatized from the true faith. We would do

The Great Tribulation 89

coming when neither in this mountain, nor in Jerusalem, shall
you worship the Father. . . . But an hour is coming, and now is,
when the true worshipers shall worship the Father in Spirit and
truth” (John 4:21-23). Now Jesus was making it clear that the
new age was about to be permanently established upon the ashes
of the old. The disciples urgently asked: “When will these things
be, and what will be the sign of your coming, and of the end of
the age?”

Some have attempted to read this as two or three entirely
separate questions, so that the disciples would be asking first
about the destruction of the Temple, and ven about the signs of
the end of the world. This hardly seems credible. The concern of
the immediate context (Jesus’ recent sermon) is on the fate of
this generation. The disciples, in consternation, had pointed out
the beauties of the Temple, as if to argue that such a magnificent
spectacle should not be ruined; they had just been silenced with
Jesus’ categorical declaration that not one stone there would be
left upon another. There is nothing whatsoever to indicate that
they should suddenly change subjects and ask about the end of
the material universe. (The translation “end of the world” in the
King James Version is misleading, for the meaning of the
English word world has changed in the last few centuries. The
Greek word here is not cosmos {world/, but aion, meaning eon
or age.) The disciples had one concern, and their questions
tevolved around one single issue: the fact that their own genera-
tion would witness the close of the pre-Christian era and the
coming of the new age promised by the prophets. All they
wanted to know was when it would come, and what signs they
should look for, in order to be fully prepared.

Signs of the End

Jesus responded by giving the disciples not one, but seven
signs of the end. (We must remember that “the end” in this
passage is not the end of the world, but rather the end of the
age, the end of the Temple, the sacrificial system, the covenant
nation of Israel, and the last remnants of the pre-Christian era),
It is notable that there is a progression in this list: the signs seem
to become more specific and pronounced until we reach the
final, immediate precursor of the end. The list begins with cer-
tain events which would occur merely as “the beginning of birth

Sanctuary and Suffrage 17

Gods of the underworld were sometimes elevated to Olympian
status, but they retained many of their underworld features. The
snake, for example, was linked with the guardian genius (spirit) of
the city of Athens, a vehicle of the wrath of the goddess Athena.**
Jane Harrison is correct: “Whatever may have been the view of the
unthinking public, the educated man, as well as the barbarous Per-
sian, knew that in past days the Grecks themselves had worshipped
nature-powers.”® The Olympian gods were essentially inventions;
they served both intellectual and political purposes.

‘Two centuries before the birth of Christ, the Dea Roma cult in
Smyrna had elevated the people of Rome and the city to divine
status, The Roman Empire later elevated the “genius of the Em-
peror’” to the status of divinity. In the Emperor was the personifica-
tion of the divine State.?° Rushdoony has summarized the implica-
tions for the Church of this development:

The conflict of Christianity with Rome was thus political from the
Roman perspective, although religious from the Christian perspective. The
Christians were never asked to worship Rome’s pagan gods; they were
merely asked to recognize the religious primacy of the state... . The issue,
then, was this: should the emperor's law, state law, govern both the state
and the church, or were both state and church, emperor and bishop alike,
under God’s law? Who represented true and ultimate order, God or Rome,
eternity or time? The Roman answer was Rome and time, and hence
Christianity constituted a treasonable faith and a menace to political order.
The Roman answer to the problem of man was political, not religious. This
meant, first, that man’s basic problem was not sin but lack of political order. This
Rome sought to supply, religiously and earnestly, Second, Rome answered
the problern of the one and the many in favor of oneness, the unity of all
things in terms of the state, Rome. Hence, over-organization, undue sim-
plification, and centralization increasingly characterized Rome.”

This was political religion, invented for the sake of politics and
the polis. This was political polytheism, a supernatural plurality of

24. Harrison, Prolegomena, p, 305.

25, Harrison, in Epilogomena to the Study of Greek Religion and Themis: A Study of the
Social Origins of Greek Religion. (2nd ed.; New Hyde Park, New York: University
Books, [1927] 1962), p. 446. ©

26. Cyrus Bailey, Phases in the Religion of Ancient Rome (Berkeley: University of
California Press,"1932), p. 52; cited by R. J. Rushdoony, The One and the Many:
Studies in the Philosophy of Order and Ultimacy (Fairfax, Virginia: Thoburn Press, [1971]
1978), p. 93.

27. Rushdoony, One and Many, p. 94.

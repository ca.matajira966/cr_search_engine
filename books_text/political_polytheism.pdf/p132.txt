108 POLITICAL POLYTHEISM

The Original Grandfather Clause

To answer this, the Puritans of New England devised a complex
theology known as the halfway covenant. They had to depart from
the old norm: that only the children of full church members could be
baptized. The churches baptized the grandchildren of that first gener-
ation, but they refused to serve them communion. Writes historian
Edmund Morgan: “Given both infant baptism and the restriction of
church membership to visible saints, it was impossible for the Puri-
tans either to evade the questions just posed or to answer them with-
out an elaborate casuistry that bred dissatisfaction and disagree-
ment. The history of New England churches during the seventeenth
and eighteenth centuries was in large measure a history of these dis-
satisfactions and disagreements.”®> The Congregational system
never recovered from this confusion of status. It was replaced in the
first four decades of the eighteenth century by revivalism and sectar-
ianism, The political and theological foundations of the holy com-
monwealths of New England collapsed under the resulting social
strain when the third generation came to maturity.%

Thus, one seemingly minor theological error early in New Eng-
land Puritan history—the requirement of an extra-biblical experi-
ence of conversion—led step by step to the destruction of the Puritan
experiment in covenantal order. This error was not the only cause of
the failure of that experiment, but it was a major one. It was a theo-
logical failure, and the New England Puritans, Calvinists to the bit-
ter end, took theology very seriously.

Lhave adopted the term halfway covenant in this book for a reason:
to show how seemingly minor errors in an otherwise consistent Cal-
vinist theological system can and have led to a serious breakdown of
Calvinist theology. Because of the ethical nature of these disastrous
and fundamental errors— the denial of four of the five points of the
biblical covenant-they strike at the heart of all forms of Bible-
believing Christianity. These errors are widely shared, but Calvin-
ists, being more theologically inclined, and possessing a more detail-
ed and rigorous theological system, have had the greatest difficulty
in dealing with these discrepancies. These discrepancies strike at the
heart of covenant theology, and Calvinists are self-consciously the

95. fbid., pp. 128-29.
96. Richard Bushman, From Puritan To Yankee: Character and the Social Order in Con-
necticut, 1690-1765 (New York: Norton, [1967] 1970), Part 4.

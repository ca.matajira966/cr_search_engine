Sanctuary and Suffrage 109

developers and defenders of covenant theology. These discrepancies
therefore appear far more radical in Calvinist theology than in other
systems: They have had catastrophic repercussions’ far beyond the
narrow debates of professional theologians.

Conclusion

This book begins with the assumption that God’s Old Testament
law is still valid in New Testament times, unless a particular law has
been annulled specifically or by clear implication by the New Testa-
ment, I do not want to defend this thesis here. It has been defended
by others elsewhere.” I have explained at considerable length (and
expense!) how Old Testament laws apply to the field of economics.%*

This chailenge to the humanist social order has been delayed for
three centuries, Christian casuistry—the application of God's fixed
laws to the changing realm of history —as a separate discipline went
into eclipse in the final quarter of the seventeenth century, and only
re-emerged three cenuries later. As recently as 1948, Kenneth Kirk
could write: “But in general it may be said that the lack of a continu-
ous and authoritative tradition, the pressure of other interests, the
growth of philosophical individualism, with the consequent decline
of the sense of loyalty . . . all combined to sterilise the Reformed
casuistry. From the beginning of the eighteenth century you may
look in vain for anything approaching a systematic grasp of the par-
ticular problems of morality. It is for the historian of modern Christi-
anity to say how far this fact has been the cause of that impotence of
the Churches which is so often deplored,”® In 1948, there was no
change onthe visible horizon. Now there is,

What I want the reader to understand is that he cannot legiti-
mately avoid the religious problems associated with politics. Whose

97. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973); Greg L. Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg,
New Jersey: Presbyterian & Reforraed, 1984); Greg L, Bahnsen, By This Standard:
The Authority of God’s Law Teday (Tyler, Texas: Institute for Christian Economics,
1985),

98. North, Dominion Covenant: Genesis; North, Moses and Pharaoh: Dominion Religion
us. Power Religion (Tyler, Texas: Institute for Christian Economics, 1985); North, The
Sinai Strategy: Economics and the Ten Commandments (Tyler, Texas: Institute for Chris-
tian Economics, 1986); North, Tools of Dominion,

99. Kenneth E. Kirk, Conscience and His Problems: An Introduction to Casuisiry (new
ed.; London: Longmans, Green, 1948), pp. 206-7.

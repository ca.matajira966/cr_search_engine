Sanctuary and Suffrage 1

sonalities. These personalities work at cross purposes. So do political
schizophrenics. 12

This book is fairly lengthy. I know of no way to overcome about
1,900 years of error in, say, 185 large-print pages. So, I have concen-
trated on reaching “the best and the brightest” in order to refute the
most representative, most intelligently argued defenders of halfway
covenant religion, I have devoted a lot of space to summarizing their
arguments, so that no one can say in response, “You quoted them
out of context.” This means I had to give the reader a lot of context.

The Choices Before Us

There are three choices politically: life under God’s civil cove-
nant, life under a rival sovereign’s civil covenant, and life in a tempor-
ary civil order in which there are rival civil covenant principles com-
peting for allegiance, where the outcome of the competition is still an
dpen question in the minds of most political participants. This last
system is generally called political pluralism. Iis defenders think this
system can be made permanent. They are incorrect. There are too
many people around like I am who eventually topple the system.
Political pluralism is a halfway house system that lasts only as long
as no group or major coalition can take over politically and an-
nounce a specific new civil covenant. It is a stand-off, a temporary
stalemate, a temporary cease-fire. [1 is defended today as if it were a
permanent condition, as if pluralism were a permanent political phi-
losophy, It is not. It is the political philosophy of the temporary
cease-fire.

Today, the West lives officially under the civil covenant of politi-
cal pluralism, but the balance of political power in this century has
shifted to a humanist theocracy. The humanist theocrats hide what
has happened in the language of moral and judicial neutrality. This
has been a very successful camouflage operation. Until the abortion
question blew a hole in the rhetoric of the myth of neutrality, the vast
majority of Christians believed in political pluralism as a philosophy.
That majority is still vast, but less so than before. But vast, three-
quarters vast, or even less, it is clinging to a philosophically and
morally dead system. Its defenders have yet to present a systematic
defense of its claims to universality. They have not been able to dem-
onstrate how pluralism is compatible with the Bible, the writings of

102. See especially Chapter 4, below, pp. 198-206, on C. Everett Koop.

There goes many a ship to sea, with many hundred souls in one
ship, whose weal and woe is common, and is a true picture of a com-
monwealth, or a human combination or society. It hath fallen out
sometimes, that both papists and protestants, Jews and Turks, may
be embarked in one ship; upon which supposal I affirm, that all the
liberty of conscience, that ever I pleaded for, turns upon these two
hinges—that none of the papists, protestants, Jews, or Turks, be
forced to come to the ship’s prayers or worship, nor compelled from
their own particular prayers or worship, if they practice any. I fur-
ther add, that I never denied, that notwithstanding this liberty, the
commander of this ship ought to command the ship’s course, yea,
and also command that justice, peace and sobriety, be kept and
practiced, both among the seamen and all the passengers. If any of
the seamen refuse to perform their services, or passengers to pay
their freight; if any refuse to help, in person or purse, towards the
common charges or defence; if any refuse to obey the common laws
and orders of the ship, concerning their common peace of preserva-
tion; if any shall mutiny and rise up against their commanders and
officers; if any should preach or write that there ought to be no com-
manders or officers, because all are equal in Christ, therefore no
masters nor officers, no laws nor ofders, nor corrections nor punish-
ments; —I say, I never denied, but in such cases, whatever is pre-
tended, the commander or commanders may judge, resist, compel
and punish such transgressors, according to their deserts and merits.

Roger Williams (1654)*

*Letters of Roger Williams, 1632-1682, Naxraganset Club Publications, 1st ser., vol.
IIE, pp. 3-4. Cited in Ansen Phelps Stokes, Church and State in the United States (New
York: Harper & Row, [1950] 1964), p. 15.

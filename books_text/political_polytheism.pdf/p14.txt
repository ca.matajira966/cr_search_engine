xvi POLITIGAL POLYTIIEISM

* theology who belonged te various denominations: Presbyterian, Congrega-
tionalist, Baptist, Dutch Reformed, German Reformed, and other smaller
Calvinistic communions.*

Second, he correctly observes that “the new American nation, as
it emerged in the late eighteenth century, was built upon an eclectic
foundation.”> He identifies three cultural streams in this eclectic
political river: radical Whiggism, Enlightenment thought (several
European traditions), and “the Judeo-Christian tradition.” (His
summary rests on the implied distinction between radical Whiggism
and the Enlightenment—a false distinction®— and the implied con-
nection between the ethics of Judaism and Christianity —a false con-
nection.”)

Third, he observes: “During the Revolutionary era Christians
failed to develop a distinctly biblical understanding of political
thought that differed sharply with Enlightenment rationalism.”® If
he is speaking of Scottish Enlightenment thought— the right wing of
the European Enlightenment— which I presume that he is, then he
is correct.

Fourth, he acknowledges the inherent incompatibility of the two
systems: “Christian and Enlightenment world views, though resting
upon very different presuppositions, combined to furnish principles
that guided the development of the American political system. . . .
American society has continued to blend or amalgamate Judeo-
Christian and Enlightenment principles to the present day, and this
ideological synthesis still molds our political consciousness and con-
cerns.”® Note his phrase, “ideological synthesis.”

What is remarkable—no, what is utterly astonishing —is his con-
tinuing commitment to the operational validity and moral integrity
of this “ideological synthesis.” We have no choice in the matter, he

4. Gary Scott Smith, Introduction, Smith (ed.), Ged and Politics, Four Views on the
Reformation of Civil Government (Phillipsburg, New Jersey: Presbyterian & Reformed,
1989), p. 4.

5, Idem,

6. See Chapter 6, below.

7. That there has never been a Judeo-Christian tradition is one of those obstinate
facts of history that do not faze Christian social commentators, as well as humanistic
commentators. Jews know better. See Arthur Cohen, The Myth of the Judso- Christian
Tradition (New. York: Schocken, 1971). See also Gary North, The Judeo-Christian Tradi-
tion: A Guide for the Perplexed (Tyler, Texas: Institute for Christian Economics, 1989).

8. Smith, God and Palitics, p. 5.

9. Idem.

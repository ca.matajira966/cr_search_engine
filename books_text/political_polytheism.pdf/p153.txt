Halfway Covenant Ethics 129

it is in the Bible and we believe that which is in-the Bible to be the
only defensible philosophical position.”® He begins his book on epis-
temology — what he calls revelational epistemology — with an affirmation
of God’s temporal creation of the universe.®

Another key feature of Van Til’s system was his denial of any
neutral common-ground reasoning between the covenant-keeper
and the covenant-breaker. The only common ground between them,
he insisted, is their shared image of God." This was the basis of his
denial of all forms of natural law theory, the unique feature of his
apologetic system. “It will not do to ignore the difference between
Christians and non-Christians and speak of reason in general. Such a
thing does not exist in practice.”"" His radical, uncompromising re-
jection of all forms of natural law philosophy separated him from all
previous Christian apologists. He was aware of this, and was sys-
tematic in his criticisms of all other Christian apologists for their
compromises with natural law, common-ground intellectual strategies.

The image of God in man was important for another aspect of
his system of epistemology: the link between human minds, and
between God’s mind and man’s. Epistemology asks: “What can man
know, and on what basis does he know it?” Van Til taught that man
is supposed to think his thoughts after God as a creature made in
God’s image. This is clearly a system of hierarchical reasoning. Man is
the subordinate. “Human reason is not a simple linear extension
of divine reasoning. The human activity or interpretation always
runs alongside of and is subordinate to the main plan and purpose
of God.” 2 God is the primary thinker; man is secondary. God thinks
creatively; man thinks re-creatively. “The natural man wants to
be creatively constructive instead of receptively reconstructive.”
To this extent, Van Til was faithful to point two of the covenant:
hierarchy.

8. Van Til, A Survey of Christian Epistemology, vol. I of In Defense of Biblical Christi-
anity (den Dulk Foundation, 1969), pp. 18-19. Distributed by Presbyterian & Re-
formed Pub. Co., Phillipsburg, New Jersey.

9. [bid., p. 1.

10. Van Til, The Defense of the Faith (rev, ed; Philadelphia: Presbyterian & Re-
formed, 1963), ch. 8: “Common Grace and Scholasticism.”

LL. Van Til, An Introduction to Systematic Theology, vol. V of In Defense of Biblical
Christianity (Phillipsburg, New Jersey: Presbyterian & Reformed, [1961] 1978), p. 25.

12, Ibid., p. 28.

13. Tbid., p. 16.

174 POLITICAL POLYTHEISM

or explicitly, for over three centuries, Christian social theorists chose
to defend the myth of neutrality.

This was also true of the Schaefferites, but in a different way.
Schaeffer and his followers kept saying that secular neutrality is the
myth, but when push came to shove in the early 1980's, they finally
decided that political pluralism is the lesser of two evils, and that
theocracy is the really dangerous myth, so they publicly re-adopted
neutrality. This decision had been implicit from the beginning in
Francis Schaeffer’s apologetics. It took a decade, 1975-85, for Franky
Schaeffer and Dr. Koop to become consistent with that original
apologetic method. Francis, Sr. had died with the dilemma still
unresolved, but his successors could no longer sustain his system's
inherent intellectual and ethical schizophrenia.

Schaeffer's followers were systematically misled throughout his
public, published career regarding what he really believed. I think it
is safe to say that it was not an oversight on Schaeffer’s part that he
neglected to reprint his 1976 pamphlet defending infant baptism in
his misleadingly titled five-volume set, Complete Works (1982). This
pamphlet had been published by an obscure local publisher long after
he had become the nation’s best-selling evangelical philosopher-
critic, a very peculiar publishing arrangement,°° As I will show in
this chapter, there were other significant elements of self-conscious
deception in his later, post-1967 career. His odd combination of theo-
logical doctrines inevitably led him into trouble after he began pub-
lishing in 1968, trouble that eventually destroyed the institutional
remains of his intellectual legacy.

Schaeffer & Son

Francis Schaeffer was the dominant influence in the intellectual
revival among American Protestant evangelicals, 1968-84. By 1979,
his books had sold over two million copies.* As he correctly ob-
served regarding his own influence, “I don’t know anyone who has
really taken a clear Christian position who has been more widely ac-
cepted in the secular area.”5?

30. Francis Schaeffer, Baptism (Wilmington, Delaware: Trimark Publishers,
1976).

31. Philip Yancey, “Francis Schaeffer: A Prophet for Our Time?” Christianity To-
day (March 23, 1979), p. 16

32. “Schaeffer on Schaeffer, Part 1,” Christianity Today (March 23, 1979), p. 20.

178 POLITICAL POLYTHEISM

that “works” eternally, is not the only possible source of the best sys-
tem of economics in man’s history? Could it be that there is no fixed
relationship between covenantal faithfulness and external blessings?
Could it be that there is no covenantal relationship between Christi-
anity and economic freedom, which is only produced by free market
capitalism? Could Christianity be thaf irrelevant socially?

Franky and his authors did not ask any of the questions, because
{select one): 1) most of them are not Christians; 2) none of them be-
lieves in the biblical covenant sanctions for society (Deut. 28); 3)
they have no answers to these questions; 4) they forgot the answers;
5) they are scared to death of appearing to be theonomists; 6) all of
the above.

Franky Schaeffer came to this embarrassing position because
both he and his father systematically refused to defend the idea of an
explicitly Christian economics—an economic system that is self-
consciously based on Old Testament law, He called for a study of the
economic evidence, as if such evidence were neutral, as if religious
presuppositions do not control every person’s interpretation of evi-
dence, as if he had forgotten the subtitle of his book, A Time for Anger:
The Myth of Neutrality (Crossway, 1982). Once again, he was the intel-
lectual heir of his father’s hostility to Van Til’s rigorously presupposi-
tional apologetic method. Franky wrote that “we must then look
about us and decide on the basis of evidence, not ideology, what eco-
nomic systems presently and realistically available best provide for
the needs of most people.”3? When he said “ideology,” he also meant
“theology.” It was the same argument used by the pro-abortionists:
“Let us just look as neutral observers at the evidence to see whether
this or that unborn child should be murdered. Let us appeal to evi-
dence, not ideology.” The proper Christian response must always be:
“Let us appeal to the law of God, not to the evidence selected by hu-
manist economists in terms of the religious presuppositions of secu-
Jar humanists.” In the book’s bibliography, there is no mention of
me, Rushdoony, E, L, Hebden Taylor,#9 or any other person even
remotely connected with Christian Reconstruction.

39, Franky Schaeffer, Introduction, in Franky Schaeffer (ed.), £s Capitalism Christian?
(Westchester, Illinois: Crossway Books, 1985), p. xxvii.

40. Author of Economics Money and Banking (Nutley, New Jersey: Craig Press,
1978).

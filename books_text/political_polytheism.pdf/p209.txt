Halfway Covenant Social Criticism 185

either a forgotten remnant of pre-Darwinian natural law theory or
else a systematic denial of permanent law (existentialism), They
have adopted political pluralism because they do not believe that Christians
can ever win in history. They are trying to buy time by leasing out the
crown rights of King Jesus to the highest bidder. They are willing to
rely on judicial decisions made by the second-rate humanists of the
democratic West in preference to proclaiming the legitimacy of bibli-
cal theocracy, yet these Western humanists are busy selling out the
West to the first-rate humanists behind the Iron Curtain.5+

The Myth of Neutrality

By denying the biblical legitimacy of the idea of the national cov-
enant, Schaeffer was necessarily proclaiming the myth of neutrality,
the myth of natural law, and the myth of permanent political
pluralism. Ultimately, this is a single myth: the myth of equal time for
Satan. There is no logical escape from this conclusion. If neutrality is
a myth, then there is a full-scale war going on between Jesus Christ
and Satan, between Jesus’ kingdom and Satan’s empire, between
Jesus’ law and Satan’s counterfeit laws. This is why Schaeffer,
despite occasional language to the contrary, never broke with Greek
natural law theory. This is why his published followers still publicly
proclaim the ideal of political pluralism through natural law,
although they refuse to use the phrase “natural law.” The deception
continues.

The Cancer of Relativism

Francis Schaeffer hated the inevitable result of political plural-
ism: relativism. What he refused to admit anywhere in his writings
was that this relativism has been implicit in pluralism from the be-
ginning. The following extract from A Christian Manifesto reveals his
problem. The transition from what is described in the first paragraph
to what is described in the second was not the result of a discontinu-
ous historical break; it was the result of a continuous philosophical
development:

54. Joan Francois Revel, How Democracies Perish (Garden City, New York:
Doubleday, 1984); Aleksandr Solzhenitsyn, “A World Split Apart” (1978), Solzhenitsyn
at Harvard (Washington, D.C.: Ethics and Public Policy Center, 1980); Solzhenitsyn,
“Misconceptions about Russia Are a Threat to America,” Foreign Affairs (Spring
1980). Cif. North, Healer of the Nations, ch. 8.

  

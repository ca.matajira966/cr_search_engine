Halfway Covenant Social Criticism 189

characteristic of modern Christian classroom defenders of political
pluralism, as we shall again see in Chapter 5.

Wells and his politically liberal campus colleagues ignore many
things besides the historical facts. They ignore the enormous prob-
lem of evangelism that nineteenth-century American Christians
faced, a battle for the heart of the United States and thereby for
Western civilization. Secular Historians are sometimes far more sen-
sitive to this problem, Historian Paul Kleppner calls attention to a
Methodist conference statement in 1891 which described the problem
well: “Unsaved millions of foreigners are coming to our shores, forc-
ing upon us one of the greatest missionary questions of modern
evangelism, viz.:—shall America be unamericanized, or shall mil-
lions of North American citizens be brought into sympathy with our
Christian institutions through the church of Jesus Christ?”®

It also should be noted that during the cra in which Christianity
was dominant politically in the United States, there was wide-open
immigration. It was only after modern democratic humanism gained
total power at the national level — after World War I— that the immi-
gration law of 1924 closed the door of sanctuary to most foreigners.
So, the problem of political pluralism is in fact international in both
scope and theory, an inescapable fact that all the defenders of democratic
political pluralism refuse even to mention, let alone deal with. Today’s
democratic pluralists in every nation use coercive immigration bar-
riers to keep out the “great foreign unwashed” who might not fit into
national humanist plans. Today, the humanists screen primarily by
means of race, although this is hidden because of the language of an-
nual national immigration quotas. This racial screening was quite
openly admitted in the 1920’s, when racial Darwinism and especially
eugenics (genetic racial selection) were dominant ideas. Immigra-
tion barriers, sterilization of retarded people and sometimes even the
poor, and national intelligence tests all appeared at once in the
United States, and all were promoted in the name of scientific
racism—humanist planned, financed, and delivered.®' Eugenics

64, Minutes of the Maine Conference of the Methadist Episcopal Church, 1891. Cited by
Paul Kieppner, The Third Electoral System, 1853-1892: Parties, Voters, and Political Cul-
tures (Chapel Hill: University of North Carolina Press, 1979), p. 198.

65. Allan Chase, The Legacy of Malthus: The Social Cost of the New Scientific Racism
(Urbana: University of Illinois Press, 1980), chaps. 10-13, 18-20. For a thought-
provoking: reconsideration of the scientific rigor of the scientists who testified for
Scopes, see Burton W. Folsom, “Scientific Blunders and the Scopes Trial,” The World
& I (June 1989), pp. 583-97.

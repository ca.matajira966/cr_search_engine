Halfway Covenant Social Criticism 215

There is another reason for this digression, however: Christians
need to understand that natural law philosophy and the ideal of per-
manent political pluralism are closely linked, and they have and
always must lead to a dead end for Christians. The Hebrews of Jere-
miah’s day were warned by God not to put their trust in polytheistic
Egypt in their confrontation with the polytheistic Chaldeans (Jer.
42:19); neither is there any help against today’s “Chaldeans” of Com-
munism, socialism, and sodomism in the pluralism of “Egypt.” The
halfway house Christianity that is tied to natural law philosophy and
permanent political pluralism is just that: halfway. Choose this day
whom you will serve: God or Baal, Jesus or Aristotle.

The Many Sides of Francis Schaeffer’s Ministry

Francis Schaeffer’s ministry had several sides. One side was
evangelical yet personal: he attracted to his chalet the “up and out”
children of the upper middle class, especially in the late 1960’s, when
they wandered aimlessly around Europe in search of adventure,
meaning, and purpose. He Iced many of them to Christ.

Another side was intellectual: he became a self-educated critic of
humanism, especially the arts, He taught himself a great deal about
many fields, although he did not meet the intellectual standards of
tenured neo-evangelical professors who teach all of 9 to 12 hours
a week, earn fat salaries, take four months of paid vacation each

yyear, conduct no evangelism programs, counsel no wandering “riff-
raff,” and have lots of time to read obscure journal articles written by
specialists.

He wrote books aimed at literate Christians. He surprised every-
one, especially his publisher, InterVarsity Press, when he found that
market far larger than anyone had imagined. How he accomplished
this is as much a mystery as how the Beatles did what they did or Hal
Lindsey did what he did. All of a sudden, there were buyers of rather
sophisticated books on Bible-believing Christianity and modern cul-
ture, and Schaeffer's reputation as a scholar began to spread. He be-
came a scholar through the back door. This sometimes showed, but

it rarely mattered.

135. Lam not, ler it be said, arguing that political pluralism cannot be defended
as a temporary system during which all sides are mobilizing to capture the system
permanently. There is nothing inherently wrong with a temporary cease-fire

  

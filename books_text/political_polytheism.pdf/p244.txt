220 POLITIGAL POLYTHEISM

In short, Francis Schaeffer's ministry ran head-on into the hard
reality of the old political slogan: “You can’t beat something with
nothing.” In a time of intellectual and social disintegration, which
this century surely is, those clinging desperately to the collapsing
center cannot be expected to do much more than hold on for dear
life. The collapsing center surcly cannot pave the way to a better
temporal world when it is unable to hold things together. Without
hope for the earthly future, Francis Schaeffer could not persuade the
bulk of his followers to move off-center, away from pietism: neo-
evangelical, Reformed, or fundamentalist. There were only three
ways out of pietism when he died: liberation theology, theocracy,
and apostasy. Schaeffer rejected all three. He died before the center
collapsed, It is unlikely that his spiritual heirs will escape that easily.

Things could have been worse, however, Francis Schaeffer might
have been a political liberal. He might even have been a trained
historian. If you think Schaeffer sounds confused, or even theologi-
cally schizophrenic, wait until you discover the theories of professors
Noll, Hatch, and Marsden in Chapter 5.

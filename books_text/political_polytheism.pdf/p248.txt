224 POLITICAL POLYTHEISM

return to Old Testament laws? Have they become theonomists in
principle, though not in practice? The term “biblical principles” is
like a blank check. Whether it bounces or not depends on how large
the account is compared with the amount of the check. As we shall
see, both the account and the check are remarkably small. In fact, by
the time you finish this chapter, you probably will expect to see their
check returned to you, with the grim notation: “insufficient funds.”

Two other bothersome questions may also intrude into the mind
of a thoughtful Christian. First, the U.S. Constitution permits
amendments. This allows for peaceful political continuity. What if a
large majority of voters in the United States should someday convert
to Jesus Christ as Lord and Savior, and then, over several decades or
more, repeatedly amend the Constitution in order to reflect the pro-
gressive transformation of the nation into a bottom-up, decentral-
ized, theocratic republic? I am not yet raising the question of the
wisdom of such a political act, though of course I would favor it
under the historical conditions I have specified. I am merely raising
the question of the legality of such a program of Constitutional revi-
sion. If it is legal, why not do it? If it is not legal, on what basis is it
declared illegal?

There is no doubt, as the authors correctly observe, that the
United States is legally a pluralistic nation today. This is not the pri-
mary point. The far more important question is: Is today’s political
pluralism necessarily a judicially and politically permanent condition?
Even more to the point: Is it Constitutionally permanent? At what
point in history may a majority of voters legitimately begin to amend
the Constitution to make it conform to “the will of the People,” if
those voters have become theonomists? If “We, the People” should
self-consciously become “We, God’s covenanted people,” what legal
or moral principle can the pluralist offer which would prohibit the
peaceful, voluntary, and majoritarian abandonment of the doctrine
of political pluralism, both in word and in deed?

The Myth of Neutrality (Again)

So, the U.S. Constitution can be lawfully amended to revoke
political pluralism. A second question occurs, On the one hand, they

3. The U.S. Constitution is divided into five parts. These parts happen to corre-
spond to Ray Sutton’s description of the Bibie’s five-point covenant model, although
not in the same order. ‘The fifth point of the covenant, continuity, corresponds to the
fifth section of the Constitution: the amending process. See Chapter 10.

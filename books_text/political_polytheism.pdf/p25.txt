INTRODUCTION
(to be read)

For from the least of them even unto the greatest of them every one is
given to covetousness; and from the prophet even unto the priest every one
deateth falsely They have healed also the hurt of the daughter of my peo~
ple slightly, saying, Peace, peace; when there is no peace. Were they
ashamed when they had committed abomination? nay, they were not at
all ashamed, neither could they blush: therefore they shall fall among
them that fall: at the time that I visit them they shall be cast down, satth
the Lorn, Thus saith the Lorp, Stand ye in the ways, and see, and ask
for the old paths, where is the good way, and watk therein, and ye shall
find rest for your souls. But they said, We will not watk therein. Also I
set watchmen over you, saying, Hearken to the sound of the trumpet. But
they said, We will not hearken (Jer. 6-13-17).

Men want peace. Peace is a gift from God. Political peace is also
a promise of Satan, who cannot deliver on this promise, but it is be-
lieved by professionally naive Christians, generation after genera-
tion. Moses warned the Israelites against any attempt to gain peace
apart from faithfulness to the stipulations of God’s covenant (Deut.
29:18-28). Any other peace is a false peace. It cannot last. It does not
last, as the bloody twentieth century reveals.1 But humanism’s false
peace is supported politically by its targeted victims, the Christians.

The Threat of God’s Negative Sanctions
For a little over three centuries, Protestant Christians have not
taken Moses’ warning seriously, Throughout the Western world,
men have substituted other gods and other explanations of the origin
and destiny of man—not ancient gods of wood and stone, but mod-
ern gods of their own imagination. God’s warning to His covenanted

1. Gil Elliot, ‘tventieth Century Book of the Dead (New York: Scribner's, 1972).
1

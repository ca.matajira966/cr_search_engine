Halfway Covenant Historiography 229

case laws. The case laws are too radical religiously, too biblically
theocratic. The present Western theocracies of autonomous man will
not tolerate such opposition.

This poses a problem for the vast majority of academically em-
ployed Christians. Their academic peers are liberals. Or Marxists.
So, they are in no position to defend publicly, let alone promote, the
legislative enactment of Old Testament law. Nevertheless, they
claim to be Christians. There is only one acceptable tactic: to deny
that the case laws are binding in New Testament times. (They share
this viewpoint with 99.9 percent of all covenant-breakers, excluding
only a few cults.) But without the case laws, there is no way to distin-
guish Christian legislation from non-Christian legislation. Chris-
tians in academic positions understand this, so they conclude that
there can be no legitimate Christian civil legislation. Then they set
out to prove it, Any other conclusion would be regarded by their
present employers as an unacceptable narrowing of the Christian
worldview, and it would also lead rapidly to a drastic narrowing of
their career opportunities.

It is the contention of theonomists that the Old Testament is not
“the Word of God (emeritus).” We take its laws seriously. This is why
we are “persona non grata” in every Christian college faculty in the
land, Our three authors do not make explicit their theological objec-
tions to theonomy. They ignore theology as such. Instead, they put
on the protective clothing of professional academic specialization.
They have written a lengthy tract against Old Testament law and
have disguised it as a survey of American political history.

Humanism’s Arrogant Humility

They make a series of assertions regarding the American past.
They understand that people who take the whole Bible seriously
tend to be political conservatives. They themselves are not political
conservatives. They must reject the notions that 1) they themselves
are not Christians and 2) they themselves are not consistent Chris-
tians. To do this, they must assert the only logical alternative, namely,
that the New Testament is inherently ambiguous — politically, economically,
and in every other way, This is exactly what our trio of historians an-
nounce regarding politics, but only toward the end of their book,
They are representative of the whole neo-evangelical academic
world. “The Bible is not a political handbook.”!? (What stick man

12. Phe Search, p. 137.

Halfway Covenant Historiography oat

What he is saying is simple: There can never be an authentically Chris-
lian reconstruction of any society this side of God’s final judgment. This is his
theological starting point, but it is carefully disguised as an historical
conclusion.

There is an even more fundamental conclusion lurking in the
shadows: let Christians rest content with political pluralism, it is the best we
can hope for in history. This is the presupposition our trio begins with as
their operating assumption. Their other conclusions follow.

Van Til is correct: finite man must reason in circles, either
vicious circles or covenantal circles. What man assumes determines
what he concludes. It would save us all a lot of time and trouble if
scholars admitted this in their prefaces or introductions. Readers
would know in advance where a book is headed.

Proving What You Have Already Assumed.

Marsden’s 1982 essay set forth the basic themes of The Search for
Christian America. As J said before, this book is an ideologically moti-
vated tract in favor of political pluralism, but is presented as a work
of historical scholarship. The authors of The Search for Christian Amer-
ica claim that they did not find Christian America. Since they very
clearly embarked on their academic journey in order to avoid finding
it, this is not surprising. They of course do not tell the reader that the
absence in history of a Christian America was thcir intellectual starting
point. On the contrary, they insist that it was “a study of the facts”
that led them to this conclusion. “We feel that a careful study of the
facts of history shows that early America does not deserve to be con-
sidered uniquely, distinctly or even predominantly Christian, if we
mean by the word ‘Christian’ a state of society reflecting the ideals
presented in Scripture. There is no lost golden age to which Ameri-
can Christians may return,”5? Furthermore, “the idea of a ‘Christian
nation’ is a very ambiguous concept . . . .”5% (Lost Golden Age?
This is part of pagan man’s theory of cyclical history, the myth of the
eternal return. The Church was in a revolt against such a view of
history from the beginning, culminating with Augustine’s City of
God.®? Why do they keep using this utterly misleading rhetoric in a

57. Bbid., p. 17.

58. Idem,

59. Charles Norris Cochrane, Christianity and Classical Culture: A Study tn Thought
and sain from Augustus to Augustine (New York: Oxford University Press, [1944]
1957}.

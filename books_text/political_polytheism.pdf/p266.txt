242 POLITICAL POLYTHEISM

supposedly scholarly historical study? Yes, yes, I know; you don’t
have to tell me: for rhetorical purposes.)

As we read the book, we find that if we used the same criteria
which they employ to define and identify a Christian nation in his-
tory to define a church, family, or individual, we would just as surely
find that “early [whatever] does not deserve to be considered
uniquely, distinctly or even predominantly Christian, if we mean by
the word ‘Christian’ a state of [whatever] reflecting the ideals pres-
ented in Scripture.” This is because “the idea of [Christian what-
ever)’ is a very ambiguous concept.”

New England as a Test Case

They fully understand, as Marsden did in his essay, that if they
are to maintain thcir thesis successfully, the hardest historical nut for
them to crack is Puritan New England. It was self-consciously Cal-
vinistic. It self-consciously proclaimed the three biblical covenants:
Church, family, and above all (for the authors’ thesis) civil. Indeed,
although the authors do not refer to it, the civil covenant known as
the Fundamental Orders of Connecticut (1639) is generally recog-
nized by scholars as the very first written constitution to launch a
new political unit in history.

They do discuss the Massachusetts Body of Liberties of 1641, in
which every Old Testament capital crime except sabbath-breaking®*
was brought under the civil sanction of execution. Predictably, they
are hostile to such a covenant document. They begin their discussion
of the Body of Liberties with this dubious statement: “But these
positive accomplishments were offset by more dubious practical con-
sequences. Old Testament law was directly if not exclusively incor-
porated into the legal systems of New England.”® (The word dubious
is Marsden’s; we find it in the 1982 essay.) In other words, these
Christian professors of American history are morally disturbed by

60. The anthors’ subhead (p. 31)

61, McLaughlin writes: “But we cannot pass over unnoticed the organization of
the river towns of Connecticut, where, it has been said, was formed the first written
constitution in history.” Andrew C. McLaughlin, The Foundations of American Constitu-
tionalism (Greenwich, Gonnecticut: Fawcett, [1932] 1961), p. 33.

62. As in the case of all churches in history, they implicitly recognized a shift from
the Old Testaments sanction to something different in the New Testament era. On
this shift, see Gary North, The Sinai Strategy: Economics and the Ten Commandmenis
(Tyler, Texas: Institute for Christian Economics, 1986), ch. 4, Appendix A.

63. The Search, p. 35.

 

 

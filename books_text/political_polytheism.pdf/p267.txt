Halfway Covenant Historiography 243

the obvious historical fact which refutes their thesis: the presence of
biblical case laws and their sanctions in the written civil covenant of
Puritan Massachusetts, In short, the best single piece of evidence
that New England was self-consciously, covenantally Christian be-
comes, in their pluralist hands, evidence that it was not Christian:
Because New England looked back to Old Testament Israel as their
judicial model, New England was being untrue to Christ.

A New Israel

In the section “A New Israel,” the authors cite the presence of bib-
lical case laws as a major problem —not for their thesis, but rather
for New England’s claim to be Christian. “The central problem,
however, immediately presented itself when Winthrop, the civil gov-
ernor, attempted to apply the summary of the law to the entire soci-
ety of Massachusetts Bay. While he made the distinction between
justice, which should be expected in any society, and mercy, to be
found in Christian associations, he clearly considered the entire
Massachusetts society as such to be essentially Christian.”** Notice
carefully the direction in which their argument is headed. “Most
ironically, probably the principal factor turning the Puritan cultural
achievement into a highly ambiguous one was the very concept that
is the central theme of this chapter—the idea that one can create a
truly Christian culture.” In short, because Winthrop regarded
Massachusetts Bay as a Christian culture, we know that it could not
possibly have been a “truly” Christian culture.

Truly, theirs is a strange argument. Think about it. Does it
sound strange to you? They are arguing that precisely because the
New England Puritans sought the establishment of a self-consciously
biblical civil government, it could not therefore have been truly
Christian. If you think this sounds weird, then you have already
concluded that it must not be inherently anti-Christian (or “not ‘truly’
Christian”) for a society to pursue the establishment of a Bible-based
covenant which embraces the fundamental principle of biblical law,
including Old Testament law. You may not approve of this as the
only possible Christian approach to politics, but you understand that
it is not inherently anti-Christian, i.e., a society which establishes
such a covenant cannot therefore be dismissed automatically as not

64. Ibid., pp. 33-34.
65. Ibid,, p. 31.

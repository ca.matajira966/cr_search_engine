Halfway Covenant Historiography 249

from the compact theory of the state: that government is a man-
made institution, that it rests on consent, and that it is founded on
the assumed equality of the subjects.”** What Parrington did not
mention, but which Perry Miller, Edmund Morgan, and most mod-
ern historians of Puritan thought do admit, is that the Puritan con-
cept of the civil covenant also rested on the consent of the governed.
Those who did not consent could leave, which Williams did, rapidly,
when he learned that he was about to be deported back to England.
What alienated Williams, and what continues to alienate his spiri-
tual and political heirs, is that the Puritans assumed that only those
who professed faith in Christ and who were members of a church
would be allowed by civil law to interpret and apply the civil laws of
God. Why did they assume this? Because they understood the gov-
erning principle of representation in a biblical holy commonwealth:
if anyone refuses to affirm publicly that he ts under God's historical and eternal
sanctions, he has no lawful right to enforce God’s civil laws on others. This
was a recognized principle in American courts until the twentieth
century: atheists, not believing in God’s final judgment, were not
allowed to testify in many state courts. In short, the Purttans held to
covenant theology: a sovereign God who rules hierarchically through
human representatives in terms of His revealed law. The rulers are
required by God to bring sanctions in God’s name against individu-
als who violate God's civil law. Why? In order to keep God from
bringing far worse sanctions on the whole society. But modern com-
mentators, including our trio, deny either explicitly or implicitly that
God brings such corporate sanctions in history. Thus, hypothetically
neutral political pluralism seems safe and sound rather than full of
sound and fury —fury against God’s law.

Pluratism and Natural Law Theory

Perry Miller and most modern historians fault Parrington for
viewing Williams as a political theorist rather than as a theologian,”®
but this is not to say that Parrington’s assessment of the Williams’
operational legacy was incorrect. Williams is still remembered by
most Americans even today, if he is remembered at all, as the perse-
cuted saint who challenged the dour political theocracy of the Mas-
sachusetts Bay Colony. This is still the Roger Williams of the high
school textbooks, Miller’s detailed and subtle revisionism netwith-

78. Idem.
79. Miller, Roger Williams, pp. 27-32.

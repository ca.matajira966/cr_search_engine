250 POLITIGAL POLYTHEISM

standing.*° Williams’ challenge to the Puritan theocracy was founded
on a specific alternative political philosophy: pluralism and natural
law. Williams understood far better than his modern pluralist dis-
ciples that you cannot beat something with nothing. He left a unique
political legacy. Deny its presuppositions — natural law theory and
the myth of neutrality—and you must logically reject his political
philosophy.

But logic is not always a man’s dominant concern. In this transi-
tional era, Christian social philosophers—e.g., Van Til, Schaeffer,
and our trio— cannot bring themselves to go beyond the first stage of
reconstruction: the denial of Williams’ first principles, natural law
and the ncutrality doctrine. ‘Thus, they remain intellectually schizo-
phrenic and dualistic—hcart-mind dualism, law-grace dualism,
spiritual-secular dualism, politics-religion dualism, heaven-earth
dualism—or remain silent on real-world issues, or disappear from
the scene, or else decide to accept the myth of neutrality after all. But
what they categorically refuse to do is to publish a detailed, docu-
mented, Bible-based, cogently argued presentation of the hoped-for,
longed-for, and implicitly promised reconciliation of inevitably non-
neutral Christianity and political pluralism. Williams could not
write it, and this inability was not simply because, in the words of
Perry Miller, “we may well doubt that he could ever construct a sus-
tained logical argument of his own . . . .”#! It was because the two
worldviews are theologically irreconcilable. Pluralism is the philoso-
phy of the temporary cease-fire; in contrast, Christianity is the reli-
gion of escalating historical confrontation, to be followed by God’s
eternal sanctions. Humanism is also a religion of escalating confron-
tation. The debate between Christianity and humanism is over
which side will win this confrontation.

Williams vs. Covenantal Hierarchies

What the high school textbooks neglect to mention is that this
bright young man, who fled Massachusetts at about age 33, was a
professional emigrant. He ‘could not settle down, intellectually or

80. A classic study on Williams is Dexter's 1876 book, As to Roger Williams, and his
“Banishment” from the Massachusetts Plantation... . , published by the Congregational
Publishing Society. Excerpts are reprinted in Christianity and Civilization 1 (1982).
Another version appears in Roger Williams and the Massachusetts Magistrates, edited by
Theodore P, Greene (Lexington, Massachusetts: Heath, 1964).

81. Miller, Roger Williams, p. 102

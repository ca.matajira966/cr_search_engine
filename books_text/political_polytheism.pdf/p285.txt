Halfway Covenant Historiography 261

ments regarding this requirement of a conversion experience, “It
may seem curious that the Puritans of the Bay carried the search for
purity farther than the Puritans of Plymouth.”!!6 To admit this,
however, the trio would be compelled to identify the Puritans of
Massachusetts Bay as even more rigorous in intent about their
Christianity than The Search admits, so they do not mention it.
The authors have completely, utterly misrepresented the Puritan
political experiment as somehow not being truly Christian, when it
was probably the most self-conscious experiment in building a com-
prehensive Christian civil order in the midst of a wilderness that any
group has come close to achieving in history. Why did these profes-
sional historians so completely misrepresent Puritanism? The an-
swer is clear: they reject, above all, the idea of a biblically mandated
civil covenant. Nothing is allowed to stand in the way of their
diatribe against the whole idea of a biblical civil covenant. I think
that this was the number-one purpose of their book, both from their
point of view and the publisher’s. In rejecting the idea of a biblical
civil covenant, they lead their readers into the swamp of relativism.
“(T]he idea of a ‘Christian nation’ is a very ambiguous concept. . . .”"!7
It is not surprising, then, that they appeal here for academic support
to one of the modern apostles of relativism, H. Richard Niebuhr—
not someone who can be regarded as an unimpeachable source.'1®

Thomas Jefferson, Prophet?

The authors try to persuade the reader of a second unusual hypo-
thesis. Their first hypothesis is that the Puritans, being systematic
covenant theologians, could therefore not be “truly” Christian in
their worldview, because they believed that the civil covenant under
God is still a valid concept. Second, they argue that the Constitution’s
Framers, who were at best nominal Christians, invented a pluralist
civil covenant which is supposedly the universal standard for a
Christian society. Thus, the most self-conscious Christians in history
(New England Puritans) were not truly Christians in their political
views, while a group of essentially non-Christian pluralist politicians
in 1787 devised the only legitimate civil foundation for American
Christians to accept, now and forever more, amen.

16. Robert Middlekauff, The Mathers: Three Generations of Puritan Intellectuals,
1596-1728 (New York: Oxford University Press, 1971), pp. 48-49.

117. The Search, p. 17.

118. Sce Appendix A: “The Authority of H. Richard Niebuhr.”

Halfway Covenant Historiography 263

strongly the Bible or other revelation informs our political views, for the purpuses
of civic debate and legislation we will not appeal simply to religious authority, !?2

This statement appears in their section, “A Middle Way.” A mid-
dle way to where? Or what?

I think it is safe to say that The Search for Christian America, if ex-
amined critically, turns out to be a second-rate piece of historiography
in search of a third-rate thesis: the theology of pluralism as the high-
est political goal of the Christian. Such shoddy scholarship from a
group of previously competent scholars testifies to a deep and
abiding intellectual schizophrenia on their part, as well as a task too
difficult for the best available minds, They found it impossible to be
true to both Jefferson and the Old Testament in maiters civil, so they
sided with Jefferson in the name of “true” Christianity. This is the
basis of their rejection of what they disparagingly refer to as “the
myth of America’s ‘Christian’ origins.”!?3

Frankly, T am not impressed.

Pluralism: Moral Deception and Judicial Deferral

Our authors write: “The complexity and irony of history blast all
our cherished notions and our pet theories.”!?+ Indeed? T should
have thought that God’s sanctions in history are what blast them.
These historical sanctions certainly have blasted the self-confidence
of Amcrican political liberalism during the last quarter century. No
one seriously proclaims “the end of ideology” the way that sociologist
Daniel Bell did before Kennedy and Camelot disappeared, to be re-
placed by Johnson and Vietnam.’ But if ideology has not ended,
then what is the future of political pluralism? If ideology escalates,
what is pluralism’s future? Noll, Hatch, and Marsden have bet their
pooled reputations on a first-place finish by Common Sense Plural-

122. The Search, p. 134.

123, dbid., p. 43.

124. Ibid., p. 154.

125. Daniel Bell, The End of Ideology: On the Exhaustion of Political Ideas in the Fifties
(rev. ed.; New York: Free Press, 1962). By 1968, the thesis was passé: Chaim I.
Waxman (ed.), The End of Ideology Debate (New York: Funk & Wagnalls, 1968). The
second book was dedicated to the memory of Martin Luther King, Jr., which seems
appropriate: the ideological debates had escalated sharply. This symposium ap-
peared seven years after Bells first edition (1961). Usually such symposia appear
shortly thereafter, not seven years. The whole world had changed, 1964-65. On this
era as a watershed period, see Gary North, Unholy Spirits: Occultism and New Age
Humanism (Ft. Worth, Texas: Dominion Press, 1986), Introduction. CL. pp. 22-25.

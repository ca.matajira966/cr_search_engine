Halfway Covenant Historiography 267

To cite our trio once again, “when bringing these decisions to bear
on civic debate and legislation we must agree to the rules of the civic
game, which under the American constitution are pluralistic. That
means that no matter how strongly the Bible or other revelation in-
forms our political views, for the purposes of civic debate and legislation we
will not appeal siraply to religious authority.”!53

This is the widely held view of most voters today. Why, then,
should we be surprised to discover that Christians in the electorate
do not know where to turn for righteous solutions to the problems of
the day? Their scholarly leaders to whom they have entrusted the
training of their children have told them Christians have a moral
and political obligation to shut their Bibles when they enter the pub-
lic arena. Today's Christian voters have faith in no authoritative
judicial standards by which moral and political order can be restored
in the midst of the real and now widely perceived moral dissolution
of our era. Thus, political solutions to crises come only when some
secular humanist moral scene ts coerctvely imposed by a power elite to solve the
problem. There is only one politically acceptable alternative to this
process: moral conflicts are deferred for a season by political or
bureaucratic tinkering, plus an additional infusion of government
money.

Tactic or Permanent Philosophy?

Once we understand that political pluralism is a process of public
moral deception and judicial deferral, the only reasonable defense of this
philosophy becomes pure pragmatism: to reduce visible conflicts tempor-
arily, One can of course attempt to defend pluralism as a permanent
philosophy by arguing that Christianity in politics tends to promote
violent conflict, so let us choose pluralism for the sake of social
peace. This is exactly what the authors argue. “In political life, if
every party is certain its position is backed by the sure authority of
God, the likelihood of violence increases vastly.”!°* They are obvi-
ously worried about the terrors of ideological politics, a fear shared
by other liberal historians.!35 So, they conclude, let us choose in-
stead pluralism. But for how long?

133. The Search, p. 134.

134, L6id., p. 137.

135. Marian J. Morton, The Terrors of Ideological Politics: Liberal Historians in a Con-
servative Mood (Cleveland, Obio: Gase Western Reserve University, 1972). Included
in this survey is a chapter on Marsden’s teacher, Edmund $. Morgan.

INTRODUCTION, PART 3

¥e have seen what I did unto the Egyptians, and how I bare you on
cagles’ wings, and brought you unto myself. Now therefore, if ye will
obey my voice indeed, and keep my covenant, then ye shall be a peculiar
treasure unto me above all people: for all the earth ts mine: And ye shall
be unto me a kingdom of priests, and an holy nation. ‘hese are the words
which thou shalt speak unto the children of Israel. And Moses came and
called for the elders of the people, and laid before their faces all these
words which the LorD commanded him. And all the people answered
together, and said, All that the Lorp hath spoken we will do, And
Moses returned the words of the people unto the Lorp (Ex. 19:4-8).

In the fall of 1965, I took a graduate seminar on the American
Revolution. The instructor was a visiting professor from a nearby
college, Douglass Adair. I had not heard of him when I began that
seminar; I have heard about him many times ever since. That semi-
nar was a marvelous academic experience in a world of infrequent
marvelous experiences. The most memorable aspect of it was the
day he asked a pair of questions that have been in the back of my
mind—and occasionally at the front —ever since, The first question
was: “Who taught the tutors of the members of the Virginia dynasty?”
And the second question was like unto it: “What books did the mem-
bers of that dynasty read?”

He did not answer these questions in great detail, but the general
answers he suggested were these: the tutors, more often than not,
were taught in some Scottish university, and the books they assigned
to their students were the books of the Scottish Enlightenment.
Right or wrong, these are the sorts of questions that historians ought
to be asking.

Who Were the “Founders”?

But there is a more fundamental question, one that I am asking
here: Who were these, and what, exactly, did they found?

307

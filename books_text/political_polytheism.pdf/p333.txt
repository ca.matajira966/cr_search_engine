Introduction, Part 3 309

do leaders encourage self-government, since the consent of the gov-
erned is always necessary.

Finally, the question of succession or continuity. This is the ques-
tion of rulership and citizenship. What is the legal basis of transi-
tion, ruler to ruler, citizen to citizen? Birth? Legal adoption? Elec-
tion? Naturalization? People are born and they die. They move.
‘They change allegiances. Societies and civil governments must deal
with these facts of life and death. To do so, they create judicially
binding public events, events that are best understood as acts of cove-
nant renewal. An election is an act of covenant renewal. So is swear-
ing an oath of office. Especially swearing an oath of office, for the oath
explicitly or implicitly calls down the negative sanctions of the cove-
nant, should the swearer break the legal terms of the covenant.

Covenantalism: An Inescapable Concept

This section of Political Polytheism deals primarily with the politi-
cal and judicial implications of point four of the biblical covenant
model: oaths/sanctions. This is not to say that none of the other
points is involved. A covenant is presented to men as a unit, and it
is either accepted or rejected as a unit, When we deal with any of
God’s covenant institutions, we must consider all five aspects of the
biblical covenant model. Following Ray Sutton’s model,' I divide up
the covenant into these five points:

Transcendence (sovereignty), yet immanence (presence)
Hierarchy/authority/representation
Ethics/law/dominion

Oath/judgment/sanctions (blessings, cursings)
Succession/continuity/inheritance

All three of the authorized corporate covenant institutions—
Church government, family government, and civil government —
must bear the institutional marks of these five points. There is no
escape. All five are basic to each of the covenant institutions. The
covenant may identify a god different from the God of the Bible, but
the covenant structure itself is inescapable. There can be no govern-
ment apart from this structure. The covenant is an inescapable con-
cept. It is never a question of “covenant vs. no covenant.” It is

1. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: Insti-
tute for Christian Economics, 1987).

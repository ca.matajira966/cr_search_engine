310 POLITICAL POLYTHEISM

always a question of which covenant. More to the point, it is a ques-
tion of which sovereign master,

Because Western Protestantism ever since the late seventeenth
century has cooperated with the forces of rationalism in abandoning
the original covenantal foundations of Western civilization, we still
face a 300-year-old dilemma. It is most acute in the United States,
where vestiges of the older covenantal Christianity still remain, and
where the conflict between covenant-breakers and covenant-keepers
has visibly escalated since about 1975. American Church historian
Sidney Mead stated the nature of the intellectual problem, which
has now begun to assert itself as a cultural and political problem—an
ancient one in American history. Writing in 1953, he observed:

But the great item of unfinished intellectual business confronting the
Protestant denominations was and is the problem of religious freedom. And
here the situation is almost as desperate as increasingly it becomes clear that
the problem cannot be solved simply by maligning the character of those
who question the American practice.

Is it not passing strange that American Protestantism has never devel-
oped any sound theoretical justification of or theological orientation for its
most distinctive practice? Today we should probably have to agree with the
writer of 1876 who said that “we seern to have made no advance whatever in
harmonizing (on a theoretical level) the relations of religious sects among
themselves, or in defining their common relation to the Civil power.”?

Part 3 of Political Polytheism asks the question: To what extent is
the U.S. Constitution a covenant document? If I can show that it is a
covenant document, then a second question arises: What kind of
covenant, Christian or secular humanist? To answer these two ques-
tions, I shall present a considerable quantity of historical material,
much of it unfamiliar even to professional historians unless they are
specialists in colonial American history and eighteenth-century reli-
gious controversies. I was trained professionally in the former field,
yet what I discuss in this section was never mentioned in any gradu-
ate seminar I ever took or any book I ever read in the 1960's. The

2. Sidney E. Mead, “American Protestantism During the Revolutionary Epoch,”
Church History, XXIL (1953); reprinted in Religion in American History: Interpretive
Essays, edited by John M. Mulder and John F. Wilson (Englewood Cliffs, New Jer-
sey: Prentice-Hall, 1978), p. 175; citing J. L. Diman, “Religion in America,
1776-1876," North American Review, CXXII (Jan. 1876), p. 42, Mead also cites the
views of Wilhelm Pauck, “Theology in the Life of Contemporary American Protes-
tantism,” Shane Quarterly, XIII (April 1952), pp. 37-56.

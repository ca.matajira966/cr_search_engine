312 POLITICAL POLYTHEISM

American nation, yet it is seldom discussed, even by specialists in
Constitutional theory. The quiet revolution which this provision ac-
complished is still equally quiet, two centuries after the revolution
began. As Garet Garett said, speaking of Franklin Roosevelt’s New
Deal of the 1930's and early 1940's, “the revolution was,”

Historiography

There is no neutrality. One's presuppositions about the nature of
God, man, law, and time* shape one’s interpretation of all facts.
There is no brute factuality, as Van Til always insisted; there is only
interpreted factuality,

The history of the origins of the U.S. Constitution in the twentieth
century has been a debate between the old Whig view — an instrument
written by men who sought to increase human liberty — and the eco-
nomic-Marxist-Beardian view: a document written by a particular
economic class of men who were seeking economic advantage. There
has also been a modified Tory view, represented by the “Imperial”
histories written by men like Charles M, Andrews and Lawrence H,
Gipson, who argue that things really were not so bad, 1763-75, and
that the disputes could have been worked out between the colonies
and Great Britain within the framework of the imperial system. The
Whig view, however, has predominated. This view goes back to the
very era of the Constitution itself, to South Carolinian David
Ramsey. There have been wide variations within this tradition, re-
flecting the divisions within the Constitutional Convention: big gov-
ernment (Hamiltonian Federalist), limited government ( Jefferso-
nian republican), and states’ rights. To put it bluntly, the winners
write the history books, and even the losers (e.g., Alexander H.
Stephens’ A Constitutional View of the Late War Between the Staies) wind
up siding with some faction within the camp of earlier winners.

This study of the Constitution is an exception to the rule. I am
writing from the perspective of the real losers, the ones whose case is
virtually never even considered, let alone defended. I am arguing
the case from the point of view of the Founders of America, the
Puritan’s. It was they who steadily lost the battle, beginning with
the restoration of Charles II to the throne in 1660. It took over a cen-
tury for this defeat to be consummated by the ratification of the U.S.

4. Gary North, Unconditional Surrender:. Gads Program for Victory (3rd ©
Texas: Institute for Christian Economics, 1987), Part 1.

 

Tyler,

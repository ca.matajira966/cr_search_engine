Introduction, Part 3 313

Constitution, They had basically lost the war by 1684, marked by the
revocation of the Massachusetts charter under Charles H, who died
in 1685. After the Glorious Revolution against James II—a Whig
revolution — of 1688-89, Massachusetts was granted a new royal
charter (1691), but one which was no longer Puritan in origin. Voting
henceforth was regulated strictly in terms of property ownership, not
religion. Covenantally speaking, the lawyers and the merchants in-
herited the Puritan commonwealth.

The Rhode Island Experiment

Theologically and even covenantally, this was not the beginning
of the battle; this was the beginning of the end. The first skirmish in
the struggle to create the modern world was in the. winter of 1636,
when Roger Williams fled Massachusetts and headed into the wil-
derness of what was to become Rhode Island. Williams successfully
created a new colony, but it was far more than a new colony; it was a
new concept of civil government. It was a concept that has become
dominant today—the very distinguishing mark of modernism. He
founded a colony that was openly secular; there would be no Church-
State connection, or even a religion-State connection.

In 1642, the General Court of Rhode Island organized a new
government. It required an oath of office from magistrates to “walk
faithfully’ and taken “in the presence of God.”5 There was no other
mention of religion. The colony’s civil government was formally rec-
ognized as “a democracy, or popular government.”® In March of
1644 (old calendar, 1643), Charles I granted a charter to the Provi-
dence Plantations. In response, in 1647, acts and orders were agreed
upon. The colony was again identified as “democratical,” meaning “a
government held by the free and voluntary consent of all, or the
greater part of the free inhabitants.”’ It admitted the existence of
“our different consciences touching the truth as it is in Jesus,” and
affirmed “each man’s peaceable and quiet enjoyment of his lawful
right and liberty. . . .”8 They enacted civil laws and sanctions for
various crimes, including murder, rebellion, misbehavior, witch-

5. “Organization of the Government of Rhode Island, March 16-19, 1641/42,” in
W. Keith Kavenaugh (ed.), Foundations of Colonial America: A Documentary History, 3
vols. (New York: Chelsea House, 1973), L, p. 343.

6, Idem,

7. Ibid., I, p. 347,

8, Idem.

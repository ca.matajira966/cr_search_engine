The Theological Origins of the U.S. Constitution 347

deistic flights, Plato was pantheistic still. Deism and Pantheism are
at bottom one.”!?9 The same was true of Newtonianism.

Newtonianism was officially Deistic. The “establishment” New-
tonians, including Newton, had no use for pantheism, They did not
want a revival of Giordano Bruno’s magic or his speculations regard-
ing a world soul. Nevertheless, pantheism could not be successfully
overcome by the Newtonian moderate Whigs, given the reality of
Newton’s heavy Socinian emphasis on the absolute transcendence of
God. The unsolved theological problem for Newton was immanence,
Where is God's personal presence in this world?

The Puritans possessed a consistent answer to this problem based
on the doctrine of the Trinity. First and foremost, God is transcendently
in control of all things the doctrine of covenantal providence. !°°
This same God is also present with His people in the Person of the
Holy Spirit, who dwells in the hearts of regenerate men and who en-
ables both regenerate and unregenerate to perform good works.1°!
He gives His people new hearts. “Those who are once effectually
called, and regenerated, having a new heart, and a new spirit created
in them, are further sanctified, really and personally, through the
virtue of Christ’s death and resurrection, by His Word and Spirit
dwelling in them: . . .”!3? God interacts with mankind in history, for
He had been a man in history, and in His perfect manhood, He now
sits at the right hand of God the Father (Mark 16:3).!®* God is pres-
ent representatively in the Bible, the revealed Word of God in his-
tory, and also in His Church.

In contrast to the Puritans’ concept of cosmic personalism stands
Newton’s cosmic impersonalism. His was a halfway covenant cos-
mology: relying on the intellectual residue of Puritanism, he denied
the power thereof. Newton was not a Trinitarian. His cosmology did
not allow for much interaction between God and man, and even his
peers resented his discussion of God’s cosmic interventions to shore
up the rusting clock. 13+

129. Cornelius Van Til, A Survey of Christian Epistemology, vol. 2 of In Defense of Bib-
kcal Christianity (den Dulk Foundation, 1969), pp. 54-35. Cf. pp. 42-43.

130. Westminster Confession of Faith, V, “Providence.”

131, Ibid., XVI, “Of Good Works.”

132, Ibid., XIE, “Sanctification,”

133. [bid., VIII:4, “Christ the Mediator.”

134, Jacob, Radical Enlighienment, pp. 59-60.

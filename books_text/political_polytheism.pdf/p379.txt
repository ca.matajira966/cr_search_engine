The Theological Origins of the U.S. Constitution 355

he now is a preserver and repairer of cosmic order. This was a transi-
tional concept of God.'®® Hume’s skepticism undermined faith in
this Tory god from man’s perspective, and advancing science sys-
tematically found ways of removing the need for this god by finding
ways of autonomously shoring up nature’s friction-bound autono-
mous order. Nevertheless, the idea of an orderly system of nature
under the rule of mathematics remained (and remains) a powerful
motivating idea for men in their quest to master nature — including
man’s own nature and society—by means of rigorous investigation
and the application of practical science to the environment. Like the
doctrine of predestination, faith in which supposedly should make
fatalists and passivists out of Calvinists, who subsequently turn out
to be a dynamic social force, so was Newtonian mathematical law. It
delivered practical knowledge to man, and in doing so, offered him
the possibility of dominion and power.

What was needed to infuse Newtonianism with power was a new
dynamic, What was needed was a view of the possibility of man’s
ethical transformation, which could then produce social transforma-
tion. What was needed was a doctrine of the new man. Rousseau pro-
vided one version of this doctrine of human transformation; the
American revivalists provided another. Both views rested on a doc-
trine of man as being more than—transcendent to—the mechanical
laws of matter in motion. Both views therefore rested on a program of
personal and social change that was beyond the boundaries of reason.

The Great Awakening

The shift from rationalism to emotionalism in the life of colonial
America can best be seen in the writings of Jonathan Edwards, He
began with his youthful speculations on science: “. . . it is self-evident
I believe to every man, that Space is necessary, eternal, infinite and
omnipresent, But I had as good speak plain: I have already said as
much as, that Space is God. And it is indeed clear to me, that all the
Space there is, not proper to the body, all the Space there is without
the bounds of Creation, all the Space there was before the Creation,
is God himself; . . .”!6° Yet he was to write that lengthy defense of
“sweet” emotionalism, the Treatise Concerning the Religious Affections

168. Burtt, Metaphysical Foundations, pp. 301-2.
169. “Notes on Natural Science. Of Being.” Cited by Rushdoony, This Indgpendent
Republic, p. 6

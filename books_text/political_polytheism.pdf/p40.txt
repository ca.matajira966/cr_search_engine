16 POLITICAL POLYTHEISM

There is a reason for their opposition to both six-day creationism
and political pluralism: /iberalism, both theological and political.
These people want the fruits of Christianity without its Bible-
revealed limits and responsibilities. They proclaim God’s creation in
general, hoping thereby 1) to retain donors’ support and a stream. of
young Christian students to capture; and 2) to avoid the radical
despair of modern humanist science.“ By refusing to affirm six-day
creationism, they hope to accomplish at least three things: 1) retain
their accreditation from humanist accrediting agencies; 2) retain the
approval of other neo-evangelical academicians, who have long since
abandoned orthodoxy for Barthianism or liberation theology; and 3)
leave the door open to just about any kind of theoretical compromise
with the “latest finding of moderm science,” meaning the scientific
views of a decade ago that are now working their way into the text-
books. They want the benefits of orthodoxy with none of the liabilities.

Similarly, they affirm something called God's “creation or-
dinances.”46 Sometimes, they call this a “creation ethic.”4?7 This
sounds so very biblical. But they simultaneously deny that Old Tes-
tament law in any way provides the definitions, specific content, and
intellectual limits of these ordinances. Their goals here are much the
same as with their affirmation of creationism, but not six-day cre-
ationism, First, they can still sound as though they affirm the Bible,

44. North, Foreword; Hodge, Baptized Inflation, pp. xx-xxii; “The Academic
Seduction of the Innocent.”

45. Gary North, Is the World Running Down? Crisis in the Christian Worldview (Tyler,
Texas: Institute for Christian Economics, 1988), ch. 2: “The Pessimism of the Scien-
tists.”

46. “Natural law theories are essentially secularized versions of the idea of crea-
tion ordinances.” Gordon Spykman, “The Principled Pluralist Position,” in God and
Politics, p. 91. Ironically, Spykman rejects natural law, yet he also denies that Old
Testament laws provide the required content of these definitionless “creation or-
dinances,” This feaves everything conveniently open-ended. ‘That is the heart and
soul of neo-evangelicalism: intellectually and morally open-ended. T. M. Moore is
correct: “Ultimately, Spykman exalts Gods revelation in nature above the Bible. He
insists that the meaning of Scripture cah only be unlocked by first understanding the
meaning of God's word inherent in the creational norms around us.” Moore, “The
Christian America Response to Principled Pluralism,” ibid., p. 110.

47. See A. Troost’s response to my criticism of his antinomian call to medieval
guild socialism in the name of the Bible. Troost titled, his essay, “A Plea for a Crea-
tion Ethic,” International Reformed Bulletin (Oct. 1967). This journal was edited at che
time by Paul Schrotenboer, who algo has an essay in God and Politics, pp. 54-60. For
my response to Troost, see Narth, The Sinai Strategy: Economics and the Ten Command-
ments (Tyler, Texas: Institute for Christian Economics, 1986), Appendix C: “Social
Antinomianism.”

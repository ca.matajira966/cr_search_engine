Renewed Covenant or Broken Covenant? 379

radicalism. But of all the intellectual problems the colonists faced,
one was absolutely crucial: in the last analysis it was over this issue
that the Revolution was fought.”"?

The solution to this intellectual problem was settled in a prelimi-
nary way in 1788, with the ratification of the Constitution; it was set-
tled more decisively on the battlefields of 1861-65. But it is still not
settled in the United States. It will not be settled historically in any
nation until the whole world formally affirms the crown rights of
King Jesus.*”

What I assume in this scction of Political Polytheism is that the Ar-
ticles of Confederation served as a halfway national covenant. This
chapter is about the Constitution, but the Constitution was the cove-
nantal successor of the Articles. The Articles did not explicitly deny
that the God of the Bible is Lord over all governments, nor did they
affirm it. Several of the state constitutions did affirm this. Thus, the
national civil government was a covenantal mixture, for the national
government prior to 1788 was a confederation, not a unitary state. It
was a halfway covenant. As we shail see, the U.S. Constitution is far
more consistent. What the Articles did not positively affirm, the
Constitution positively denies: the legitimacy of religious test oaths
as a screening device for officers of the national civil government. It
is this shift that marks the transition from the older Trinitarian state
covenants to what became, over decades, apostate state covenants.
This transition at the national level did not occur overnight; there
was an intermediary step: the Articles of Confederation. Yet when
the next-to-the last step was taken—the Constitutional Convention
—those who took it ignored the original by-laws of the Articles and
appealed forward to the People. The Framers publicly ignored the
Declaration of Independence, which had formally incorporated the
national government, for they were interested in upholding the myth
of the sovereign People, and the Declaration had repeatedly men-
tioned God. Thus, the Declaration and the Articles both disap-
peared from the American judicial tradition and its system of legal
precedents, and the Articles disappeared from American political
thought. Two things were retained, however: the national name es-

19, Bernard Bailyn, The Ideological Origins of the American Revolution (Cambridge,
Massachusctts: Harvard University Press, 1967), p. 198.

20. Gary North, Healer of the Nations. Biblical Blueprints for International Retations (Fi.
Worth, Texas: Dominion Press, 1987); Gary DeMar, Ruler of the Nations: Biblical
Blueprints for Government (Bt. Worth, Texas: Dominion Press, 1987).

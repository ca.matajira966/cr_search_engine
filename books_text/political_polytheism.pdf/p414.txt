390 POLITICAL POLYTHEISM

sequences of offending him, either in this or a future world or both,
he will be under the same restraint from perjury as if he had previ-
ously subscribed to a test requiring this belief. If the person in ques-
tion be an unbeliever in these points and would notwithstanding
take an oath, a previous test could have no effect. He would sub-
scribe to it as he would take the oath, without any principle that
could be affected by either.”*! In short, a believer already believes; a
liar will subscribe; so why bother with an oath? This argument was
used by other defenders of the abolition of a religious test oath.*?

But the argument misses a key point: What about Aonest Deists
and Unitarians who would not want to betray their principles by
taking a false oath to a Trinitarian God? A Christian oath would bar
them from ‘serving as covenantal agents of the ultimate sovereign,
the God of the Bible. By removing the requirement of the oath, the
Convention’s delegates were in fact opening up the door to federal
office-holding that would otherwise be closed to honest non-Chris-
tians, a point observed by some of the defenders of the removal of
the religious test. It would also open up offices of authority to men
who had taken other binding oaths that were hostile to Christianity ~ men who
had taken these rival oaths in good faith, That possibility was never openly
discussed, but it was a possibility which lay silently in the back-
ground of the closed Convention in Philadelphia, By closing the lit-
eral doors in Philadelphia, the delegates were opening the judicial
door to a new group of officials. They were therefore closing the judi-
cial door to the original authorizing Sovereign Agent under whom.
almost all officials had been serving from the very beginning of the
country. The proposal was submitted by Charles Pinckney of South
Carolina. After debate, it was accepted overwhelmingly. North
Carolina opposed it; Maryland was divided.*

Those hostile to Article VI, Clause 3 suspected what might hap-
pen: “. . . if there be no religious test required, pagans, deists, and
Mahometans might obtain offices among us, and that the senators

41. Ibid., IV, p. 639. Gf. Mr. Spencer, North Carolina ratifying convention, in
Jonathan Elliot (ed.), The Debates in the Several State Conventions on the Adoption of the
‘Federal Constitution as Recommended by the General Convention at Philadelphia in 1787, 5
vols. (Philadelphia: Lippincott, [1836] 1907), TV, p. 200.

42. Cf, Mr, Shute in the debate in Massachusetts’ ratifying convention: ibid., IV,
p. 642; Mr. Iredell of North Carolina: Elliot, Debates, IV, p. 193.

43. Trench Coxe, Oliver Ellsworth, Mr. Shute, Edmund Randolph: Founders’
Constitution, IV, pp. 639, 643, 644.

44, Farrand, Records, II, pp. 461, 468.

Renewed Covenant or Broken Covenant? 407

gion of the United States.”' In a letter to Benjamin Watterhouse
that same year, he wrote: “I trust that there is not a young man now
living in the United States who will not die a Unitarian.”! The
Bible is just another history book, he wrote to Peter Carr: “Read
the Bible, then, as you would read Livy or Tacitus.”°? What, then,
becomes of the sanctions of religion that the Framers hoped would
be out in the service of society? As Pangle asks: “Can belief in im-
mortality of the soul or in providential interventions in this life be
divorced from belief in miracles, and can one easily confine theologi-
cal disputation once one encourages the belief in miracles? We search
in vain for answers in Jefferson’s writings, public or private. . . .7103
The same question must be posed regarding the other Framers’ views,
and the same silence is ominous. Many of them based their hopes of
social stability on a religion they had personally rejected. They drew
Jarge drafts on a Trinitarian cultural bank account into which they
made few deposits in their lifetimes.

The Declaration of Independence

The Declaration of Independence announced the creation of a
new nation in 1776, The day it was approved, July 4, 1776, the Con-
gress authorized a committee to create a national seal. A seal is an
aspect of incorporation, just as baptism is. This is why we know that
the Declaration was an incorporating document. The by-laws of the
nation were agreed to in November of 1777, but they were not rati-
fied until 1781: the Articles of Confederation. What very few people
are ever told today is that this was not the full name of the Articles.
The document was called, “Articles of Confederation and perpetual
Union between the States. . . .” It then listed the 13 states by name.
The words “perpetual Union” reveal the nature of the Constitutional
Convention of 1787 and the call for state ratifying conventions: an
initially illegal revocation of the original by-laws of the nation, which
was to have been a perpetual union.

This original union was legally dissolved in 1788 by the ratifica-
tion of the Constitution. A new Deity was identified, “We the
People.” The old Deity of the Declaration, the undefined god of

100. Jefferson to Smith, Dec. 8, 1822; cited in Pangle, Spirit of Modern Republican-
ism, p. 83.

101. Jefferson to Watterhouse, June 26, 1822; idem,

102. Jefferson to Carr, Oct. 31, 1787; sbid., p. 84.

103. Zbid., p. 85.

Renewed Covenant or Broken Covenant? 409

was primarily a foreign policy document aimed at France and
Europe, although it was designed to unify those at home." It ex-
pressed only commonplace sentiments in America. It did not be-
come a well-known document of the history of the Revolution until
decades later. It had not even been a part of Fourth of July
ceremonies in the decade of the 1790’s.!°* Until the Presidential elec-
tion of 1796, when John Adams ran against Thomas Jefferson, the
public had barely heard of the Declaration. Jefferson’s supporters
resurrected it as a symbol of their candidate’s importance, much to
the displeasure of Adams, who was one of the five men on the com-
mittee that was responsible for drafting it. The Federalist Party did
its best to de-emphasize Jefferson’s part in the Declaration’s draft-
ing.1°° But Adams could hardly deny that the language and concepts
were mostly Jefferson’s, 1°

John Witherspoon signed the Declaration and served in the war-
time Congress. He therefore served as the new nation’s baptizing
agent for the American Whig churches. This was the public anoint-
ing that was covenantally needed in all Christian nations prior to the
ratification of the U.S. Constitution. This was, in short, the sanction-
ing of the new revolutionary constitutional order of 1776. This is why
Witherspoon was so important in American history, and why the
Whig churches ever since have praised his actions and designated
him as he ecclesiastical figure in the Revolutionary War era, which
he undoubtedly was, but not for the reasons listed today, He was not
merely a political representative who happened to be an ordained
Presbyterian minister; he was in effect the covenantal representative
agent of the Whig-Patriot churches. The British recognized him as
such, which is why the military immediately bayoneted the man they
believed to be Witherspoon.’ Witherspoon was crucial to the

107, Tbid., p. 143

108. fid., p. 212.

109. Philip F, Detweiler, “The Changing Reputation of the Declaration of Inde-
pendence: The First Fifty Years,” Willian and Mary Quarterly, 3rd. ser., XIX (1962),
pp. 565-66.

110. For my views on the Declaration, see my essay, “The Declaration of Inde-
pendence as a Conservative Document,” Joumal of Christian Reconstruction, 111 (Sum-
mer 1976), pp. 94-115. I did not discuss the character of the Declaration as an incor-
porating document, however, an oversight common to historians and most lawyers.
I was informed of this judicial character in 1985 by a retired president of an obscure
and defunct conservative law school. This theme of the Declaration as an incor-
porating document is now taught at CBN University law school.

111. James Hastings Nichols, “John Witherspoon on Church and State,” Journal
of Presbyterian History, XLVU (Sept. 1964), pp. 166-67.

  

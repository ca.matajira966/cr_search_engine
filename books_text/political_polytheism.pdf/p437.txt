8
THE STRATEGY OF DECEPTION

Before I arrived, a number of rules had been adopted to regulate the pro-
ceedings of the Convention, by one of which, seven states might proceed to
business, and. consequently four states, the majority of that number,
might eventually have agreed upon a system which was to affect the whole
Union. By another, the doors were to be shut, and the whole proceedings
were to be kept secret; and so far did this rule extend, that we were thereby
prevented from corresponding with gentlemen in the different states upon
the subjects under our discussion — a circumstance, str, which I confess I
greatly regretted. I had no idea thai all the wisdom, integrity, and virtue
af this state [Maryland], or of the others, were centred in the Convention.

Luther Martin (1788)!

The U.S. Constitution is a covenantal document that was drawn
up by delegates to an historic Convention. This Convention had
been authorized by Congress in February of 1787 “for the sole and
express purpose of revising the Articles of Confederation, and report-
ing to Congress and the several legislatures such alterations and pro-
visions therein as shall when agreed to in Congress, and confirmed by
the states, render the federal Constitution adequate to the exigencies
of government and the preservation of the Union.” It was on this
explicit legal basis alone that three of the state legislatures sent dele-
gates to Philadelphia: Massachusetts, Connecticut, and New York.’

1. Letter from Luther Martin, Attorney-General of Maryland, to Thomas C.
Deye, Speaker of the House of Delegates of Maryland (Jan. 27, 1788); in Jonathan
Elliot (ed.), The Debates in the Several State Conventions on the Adoption of the Federal Consti-
tution as Recommended by the General Convention at Philadelphia in 1787, 5 vols, (Phila~
delphia: Lippincott, [1836] 1907), I, p. 345.

2, Bid., I, p. 120.

3. The wording of Congress—“for the sole and express purpose of revising the
Articles of Confederation”— was adopted by the formal authorization of the dele-
gates from Massachusetts, Connecticut, and New York, Iézd., I, pp. 126-27, See also
Documents Hustrative of the Formation of the Union of the American States, edited by Charles
C. Tansill (Washington, D,C.: Government Printing Office, 1927), pp. 56-59.

413

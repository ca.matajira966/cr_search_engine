418 POLITICAL POLYTHEISM

those walls, not even in their old age. Why not? In a modern world
filled with “leaks” to the press and everyone else, we can hardly im-
agine what it might have been that persuaded these men to keep
their silence. I have read no history book that has even raised the
question. But of this we can be confident: they all feared some kind
of negative sanctions, either external or internal, for breaking this
oath of secrecy.

When the Convention ended, they took the final step. They
handed all the minutes over to George Washington to take back to
Mount Vernon. They knew that no one in the nation would have the
audacity to tell George Washington that he had to hand over the evi-
dence of what was in fact a coup. Madison’s notes state specifically
that “The president, having asked what the Convention meant should
be done with the Journals, &c., whether copies were to be allowed to
the members, if applied for, it was resolved, nem. con., ‘that he retain
the Journal and other papers, subject to the order of Congress, if
ever formed under the Constitution.’ The members then proceeded
to sign the Constitution. . . .”?° In short, if the coup was successful,
then the new Congress could gain access to the records. If not, no
one would have any written evidence to prove anything except the
untouchable General Washington.?! On that basis, they signed.

Historian Jack Rakove argues that this element of secrecy was
the result of years of near-secrecy by the Continental Congress itself.
To this extent, he implies, it was a fitting end for the old Congress.
This is a strange way to argue; nothing in Congress’ history rivaled
the degree of secrecy in Philadelphia. Rakove is nevertheless correct:
“For the most remarkable aspect of the Convention’s four-month in-
quiry was that it was conducted in virtually absolute secrecy, un-
influenced by external pressures of any kind. . . . Except for the
occasional rumors—many of them inaccurate—that American
newspapers published, the general public knew nothing of the Con-
vention’s deliberations.”2?

Bypassing Congress
Instead of submitting the Constitution to Congress, as originally
agreed to by all the delegates—so much for legal but politically in-

20. Elliot, Debates, V, p. 558.

21, Wouldn’t Richard Nixon have appreciated such rules in 1974!

22. Jack N. Rakove, The Beginnings of National Politics: An Interpretive History of the
Continental Congress (New York: Knopf, 1979), p. 399.

428 POLITICAL POLYTHEISM

Madison was a dedicated man. As we shall see in Chapter 9,
what had long motivated him was his commitment to remove the re-
ligious test oath from Virginia politics and then national politics; He
achieved bath of these goals within a three-year period, 1786-88.

Madison is often called the “Father of the Constitution.” Intellec-
tually speaking, it was John Adams, the American ambassador in
England at the time of the Convention, who was an equally domi-
nant figure at the Convention because of his detailed studies of the
state constitutions, especially his pre-Convention, three-volume
work, Defense of the Constitutions of the Government of the United States.
His model of the “balanced constitution” was an important influence
at Philadelphia.”? Nevertheless, it was surely Madison who was the
father of the Convention, with Washington sitting silently as the
godfather. It was Madison who, more than any other man, broke the
national covenant with God.

Conspiracy

Arrant hypocrisy?, Rushdoony asks rhetorically. Not at all. Arrant
conspiracy, These men were conspirators, The Articles of Gonfedera-
tion had stated clearly that “No two or more states shall enter into
any treaty, confederation or alliance whatever between them, with-
out the consent of the united states in congress assembled, specifying
absolutely the purposes for which the same is to be entered into, and
how long it shall continue” (Article VI), This is why the conspirators
tried to surround the proposed Constitution with an air of legality by
stating in the Preamble: *. .. in Order to form a more perfect
Union, establish Justice,” etc. The specified time limit was perpet-
ual: “. . . to secure the Blessings of Liberty to ourselves and our Pos-
terity. .. .” But Congress had not authorized any such treaty, con-
federation or alliance. The conspirators knew it, especially the man
who made the coup possible, George Washington. “More than most
men,” comments Garry Wills, “he showed an early and unblinking
awareness that the Philadelphia convention would engage in acts not
only ‘irregular’ or extralegal, but very likely illegal. John Jay had
alerted him to this problem as early as January.”” Jay’s fears were

72. Gordon §. Wood, The Creation of the American Republic, 1776-1787 (Williams-
burg, Virginia: Institute of Early American History, published by the University of
North Carolina Press, 1969), ch. 14.

73. Garry Wills, Cincinnatus: George Washingion and the Enlightenment (Garden City,
New York: Doubleday, 1984), p. 154.

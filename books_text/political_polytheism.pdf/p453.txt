The Strategy of Deception 429

only partially allayed when in February, Congress authorized the
Convention, but only to suggest amendments to Congress. On
March 10, Washington wrote to Jay: “In strict propriety a Conven-
tion so holden may not be legal.””* But they proceeded.

They knew the whole thing was illegal, a subversive act of revo-
lution. They were lawyers, and they had read their Blackstone.”
Blackstone had commented on the convention-parliament that had
called William Hi to the throne in 1688-89. It had been legal, he
said, only because James II had abdicated. (Blackstone failed to
mention the less-than-voluntary circumstances of the king’s depar-
ture.) Blackstone wrote: “The vacancy of the throne was precedent
to their meeting without any royal summons, not a consequence of
it. They did not assemble without writ, and then make the throne
vacant; but the throne being previously vacant by the king’s abdica-
tion, they assembled without writ, as they must do if they assembled
at all. Had the throne been full, their meeting would not have been
regular; but, as it was empty, such meeting became absolutely neces-
sary.”76 The “throne” was occupied in 1787; Congress had not abdi-
cated. The Convention had been issued writs; these writs expressly
prohibited the substitution of a new constitutional document. Those
who came to Philadelphia for any other purpose were conspirators.
Yet most of those who came arrived in Philadelphia with the death
sentence in their pockets against the existing Confederation and the
authorizing Congress.

It was this well-organized conspiracy that had control over the
institutional levers that made possible the events of the Revolu-
tionary War era “that transformed the entire political and social
structure of the thirteen colonies in less time than it now takes to
send a First Amendment case from appeal to the Supreme Court.”””

The Masonic Connection

James D. Carter wrote his doctoral dissertation under Professor
Walter Prescott Webb, one of the most distinguished American his-

74, Thid., p. 155.
75. Bernard Bailyn, The Ideological Origins of the American Revolution (Cambridge,
Massachusetts; Belknap Press of Harvard University Press, 1967), p. 31;
McDonald, Novus Ordo Seclorum, p. xii.
76. William Blackstone, Commentaries on the Laws of England, 4 vols. (Chicago:
University of Chicago Press, [1765] 1979), vol. I, Of the Rights of Persons, p. 148.
77. Rutland, “James Madison's Dream,” of. cit., p. 9.

 

9
FROM COUP TO REVOLUTION

The conduct of every popular assembly acting on oath, the strongest of re-
ligtous tres, proves that individuals join without remorse in acts, against
which their consciences would revolt if proposed to them under the like
sanction, separately in their closets. When indeed Religion is kindled
into enthusiasm, its force like that of other passions, is increased by the
sympathy of a multitude. But enthusiasm is only a temporary state of re-
ligion, and while it lasts will hardly be seen with pleasure at the helm of
Government. Besides as religion in its coolest state is not infallible, it
may become a motive to oppression as well as a restraint from injustice.

James Madison (1787)!

James Madison was an angry young man in the spring of 1787.
(He was 36 years old.) He had been angry for a long time. Every-
thing he saw—in the Articles of Confederation, in the state legisla-
tures, in the cconomy-- made him angry. He was determined that
there would soon be a change. This change would have to be politi-
cal and national. He set down his private thoughts in the weeks be-
fore the great convention that he had organized, a convention that
he had begun planning at the meeting at Mount Vernon two years
earlier,

He was also determined to achieve his long-term goal of separat-
ing Christianity from civil government— not just separating Church
from State, but Christianity from civil government. He knew what
had to be done in order to accomplish this goal: the severing of the
binding power of Trinitarian religious oaths that were required of
state officers in several states. Those oaths had to be circumvented.

1. James Madison, “Vices of the Political System of the United States” (April
1787); reprinted in Marvin Myers (ed.), The Mind of the Founder: Sources of the Political
Thought of James Madison (Indianapolis, Indiana: Bobbs-Merril, 1973), p. 90.

443

Introduction 23

The heart of the question of politics, Lenin said, is the question
of “Who, Whom?” Writes Neuhaus: “Every system of government,
no matter what it is called, is a system by which some people rule
over other people. In every political system, political legerdemain,
which is to say political success, requires that people be kept from
recognizing the elementary fact that in any society there are the rul-
ers and there are the ruled.”® The question is: Who rules, and who
is ruled? The question of legitimate rule also is: In whose name, by
whose authority, by what standard, with what sanctions, and for
how long?

Christian political pluralists have promised that they can supply
us with the answers, if only we remain patient. I am hereby calling
their bluff. It is time for Bible-believing Christians to stop comprom-
ising with the humanists who are the beneficiaries of the system, and
stop listening to their paid agents in the Christian college classroom.
We need to make a clean break ideologically, and then patiently
work out the practical implications of this break over many, many
years.

What we have seen for over three centuries is a stream of
compromises; in late seventeenth-century England, late eighteenth-
century America, and late ninetéenth-century Holland. Let us end it
in late twentieth-century America. We are asked generation after
generation to sell our birthright for a mess of pottage. Each genera-
tion complies. We keep selling it cheaper. Let us reclaim our birth-
right from those who have bought it with counterfeit money. This is
the visible legacy of humanism: counterfeit money from privately
owned fractional reserve banks.® It is time for a change—theologi-
cally, politically, and monetarily.®

63. Richard John Neuhaus, The Naked Public Square: Religion and Democracy in
America (Grand Rapids, Michigan: Eerdmans, 1984), p. 30.

64, Elgin Groseclose, Money and Man: A Survey of Monetary Experience (Norman
Oklahoma: University of Oklahoma Press, 1961).

65. Gary North, Honest Money. The Biblical Blueprint for Money and Banking (Ft.
Worth, Texas: Dominion Press, 1986).

454 POLITICAL POLYTHEISM

promised to accomplish: greater freedom for individuals, greater
wealth for nations. Ferguson, as an ordained Presbyterian minister,
at least had a liberal Presbyterian view of God to undergird his social
evolutionism. Smith had a more Deistic view of God as the founda-
tion of morality. He spoke of “the all-seeing Judge of the world,
whose eye can never be deceived, and whose judgments can never be
perverted.”38 He believed in final judgment, including negative sanc-
tions.3° He did not appeal to religion merely as an instrumental
value for national goals. The Framers were much less clear about
such supernatural supports, except insofar as widespread belief in
such a God would strengthen social order.

With their faith in God as the cosmic Architect of the moral
world, by tying the operations of a competitive market order to God's
ultimate design, the Scottish rationalists could offer the suggestion
that men can increase their wealth by trimming away most legisla-
tion. The world works better when politicians remove themselves
from the market. The designing schemes of politicians are the source
of the poverty of nations. While Jefferson may have believed in such
an economic world— Hamilton surely did not—it took a leap of faith
to believe that a Convention could revolutionize civil government by
designing a totally new experiment in national government without
falling into the trap that the Scots said that politicians always fall
into: not seeing the long-term consequences of their actions. The
Scots believed in a Grand Architect, but they were of the opinion
that a wise politician will leave God’s handiwork alone. The Framers
had a different opinion, at least regarding civil government.

In modern times, the collapse of faith in any underlying unity
apart from either coalitions or the outright abolition of rival factions
has destroyed the Madisonian paradigm. Political Unitarianism
has been replaced by relativism and the consequent cacophony of
single-issue politics. The physical world of Newton has been re-
placed by the world of Heisenberg, at least at the subatomic level.
The social world of Newtonianism has been replaced by theories of
pluralism. The individual gods of the pluralist universe are unwill-
ing to take “no” for an answer. Anarchy—that great fear of the
Framers — has once again reared its many heads. They had relied on
a Trinitarian society to preserve their Unitarian settlement, and the

38. Adam Smith, The Theory of Moral Sentiments (Indianapolis, Indiana: Liberty
Classics, [1759] 1976), p. 228.
39. Tbid., pp. 280-81.

From Coup to Revalution 457

Paradigm Shifts and Political Organization

The question remains: How did they do it? How did they organ-
ize the Convention, gain Congress’ grudging acceptance, and then
defeat the Antifederalists in the state ratifying conventions? There is
reasonable evidence that Antifederalist sentiments were held by at
least an equal number of citizens in 1788 as those favoring ratifica-
tion.** Was the victory of the Federalists due to better organization
or a better case philosophically? In my view, it was both.

In a paradigm shift, those who are creating the new paradigm
constantly call to the attention of everyone the fact that the existing
paradigm cannot solve major empirical, factual, real-world prob-
lems. The defenders of the older paradigm cling to the old system,
vainly trying to show that the empirical problems raised by the
critics are really not so threatening and are best solved by using the
familiar terms of the older system. Bui as the incongruities between
the new facts— meaning either newly observed, recently re-discovered,
or newly emphasized facts— and the old paradigm continue to grow,
and as younger men tire of putting up with these anomalies, the next
generation of leaders shifts its allegiance to the newer paradigm.*5

The young men of the Revolution produced this paradigm shift
in 1787, The older political paradigm of the Trinitarian colonial
charters was very nearly dead in 1787. Biblical covenantalism at the
colony level had steadily been replaced after 1776 by halfway cove-
nantalism; halfway national covenantalism at the national level was
then unable to survive the onslaught of apostate national covenant-
alism.*® The Federalists successfully portrayed the problems of the
late 1780's as being of crisis-level proportions, an argument denied

44. Jackson Turner Main, The Anti-Federalists. Critics of the Constitution, 1781-88
(Williamsburg, Virginia: Institute of Early American History and Culture, pub-
lished by the University of North Carolina Press, 1961), Conclusion.

45. Thomas Kuhn, The Structure of Scientific Revolutions (2nd ed.; Chicago: Univer-
sity of Chicago Press, 1970).

46. [expect an analogous replay of this 20-year-old “battle of the paradigms” be-
tween those younger Christian scholars and activists who see clearly what I am get-
ting at in this book and those traditional Christian pietists and natural law defenders
whose theologically compromised systern visibly died with Darwin and the Four-
tcenth Amendment, but who still cling to the Constitutional paradigm as if it were
not secular humanist to its core. Among the older men, only those, such as the Cov-
enanters, who never accepted the theological legitimacy of the Constitutional settle-
ment, are likely to accept my critique of that settlement, and probably not many of
even the Covenanters. They will see how radical my rejection is, even though I am a
strategic gradualist. Halfway covenant thinking is still a way of life for Christians.

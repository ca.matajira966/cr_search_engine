470 POLITICAL POLYTHEISM

especially of an indigenous variety, seemed to offer a solution to the
religious problem bequeathed by the English Revolution. Radicals
in the 1690s who desired a republican version of the constitution,
true religious toleration, social reform, a Parliament ruled by gentle-
men in the interest of the people, had to recognise that those goals
had been rejected in 1660 at the Restoration.”*? They asked them-
selves; Why had the two English Revolutions failed? Religious con-
flict, concluded a radical minority. They concluded that what was
needed was a program of reform based on a new “religious consen-
sus, in a civil and universal religion. . . ."°* Freemasonry was the
eighteenth-century’s institutional culmination of this quest.®*
Masonry’s principles, like its organizational structure, were “por-
table,” to use Dr. Lipson’s term. While I understand that readers
have a tendency to skip over lengthy block quotations, I strongly
suggest that this temptation be resisted at this point. Writes Dr. Lipson:

The first problem on which Freemasonry worked was how a society
with an established church could accommodate both a growing religious di-
versity and the rationalistic universalism that had attended the growth of
the new sciences. The Masonic response was to provide a secret (arcane)
pseudo-religion by developing an elaborate mythology and system of rituals
for teaching moral values that Masons claimed were universal. The leaders
were not unaware of the parallels of Masonry and religion. Churches, how-
ever, required uniformity over a wide range of beliefs and values, from the
immediate to the ultimate, while Masonry only required fidelity to a gener
ally accepted system of moral values related to daily life. As [Wellins] Calcott
reminded his English and American readers [in 1769 and 1772, respectively],
in the implicit anticlericalism that pervaded Freemasonic literature, the
church’s interpretation of history was one of “enmity and cruclty”
Masonry, on the other hand, was a system of morality based on the will of
God and “discoverable to us by the light of reason without the assistance of
revelation.” According to the Constitutions, a Mason was obliged “to obey the
Moral law,” or the Religion in which all men agree, leaving their particular
Opinions to themselves; that is to be good Men and true, or Men of Honour
and Honesty, by whatever Denominations or Persuasions they may be dis-

83. Jacob, Redical Enlightenment, p. 154.

84, Tbid,, p. 155.

85. On the connections of Commonwealthman John Toland’s pantheism, re-
publicanism, and Freemasonry, see Jacob, ibid., p, 153. This connection has been
denied —unconvincingly, in my view—by Roger L. Emerson, “Latitudinarianism
and the English Deists,” in J. A. Leo LeMay (ed.), Deism, Masonry, and the Enlighten-
ment (Newark, New Jersey: University of Delaware Press, 1987), pp. 43-44.

From Coup to Revolution 485

men’s vengeance is confined to the contempt and infamy which the fore-
swearer incurs. !90

Tf the human penalty were merely “contempt and infamy,” then
the perjurer would not fear for his property or life. On the other
hand, oaths that are self-maledictory with respect to men as well as
Ged are doubly fearful. If Masons do take the oaths described by
Graebner, then they have a human sword hanging over them— the
imitation ‘covenantal oath — whenever they are tempted to reveal the
society’s mysteries. The language of the reported oaths is bloody —
covenantally bloody, There is little doubt that Masonic leaders under-
stand what an oath is, as distinguished from a contract, and they re-
gard the verbal oaths of their members as oaths in the same way that
a magistrate of a kingdom regards an oath in one of the kingdom’s
courts of law, An oath places a person under a sovereign, and this sov-
ereign possesses power, at the very least, and presumably a degree of
authority (legitimacy). It is easy to understand why orthodox Chris-
tianity has been hostile to secret societies over the years. A secret soci-
ety sets up a rival kingdom with rival oaths and therefore rival gods.

Conclusion.

Henry Steele Commager has remarked that “The constitutional
convention, which has some claim to be the most original political
institution of modern times, legalized revolution.”!°! This comes
close to the mark, but not dead center, What legalized the revolution
were the mini-conventions at the state level. These individual repre-
sentative plebiscites sanctioned ‘the coup in Philadelphia, and from
that point on, the revolution was secured. Not the American Revo-
lution . . . the lawyers’ revolution.

The problem with exposing the coup in Philadelphia is that it was
such a successful coup, Berman regards the American Revolution as
one of the six successful revolutions in Western history.12? To be a true
revolution, he argues, a revolution must be a revolution in law, and it
must survive more than a generation; otherwise, it is just a coup, '94

130, Encyclopaedia, HL, pp. 555-36,

131, Henry Steele Commager, The Empire of Reason: How Europe Imagined and
America Realized the Enlightenment (New York: Oxford University Press, 1977), p. 182.

132. Harold J. Berman, Law and Revolution: The Formation of the Western Legal
‘Tradition (Cambridge, Massachusetts: Harvard University Press, 1983), pp. 5, 18.

133. Tbid., p. 20.

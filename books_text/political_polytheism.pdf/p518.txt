494 POLITICAL POLYTHEISM

The cighteenth-century world steadily abandoned the earlier view of
the civil covenant: government under God. It became popular to
speak of a social contract between or among the people, as the sovereign
initiators. It is, in Wood’s phrase, “the equation of rulers and ruled.”®
Charles Backus declared in a 1788 sermon: “But in America, the Peo-
ple have had an opportunity of forming a compact betwixt themselves;
from which alone, their rulers derive all their authority to govern.”!°

The heart of the judicial apostasy of the modern world is found
here: the shift from the formal biblical covenant to a State-enforced
contract, so-called. The State, as the highest court of appeal —short
of revolution —became the operational Sovercign of the civil cove-
nant, since it was no longer formally covenanted under God. As the
human agency with the greatest power, the State steadily has
asserted jurisdiction over churches and families. Since the State is
regarded as beyond earthly appeal, no other human covenant sup-
posedly can be said to have a higher court of appeal than the State.

This shift in language—covenant to contract— unleashed the
State from its traditional shackles under God and God’s law. Dar-
winism later completed the process of emancipation from God and
deliverance into the bondage of the State. But Darwinism was sim-
ply a working out in the field of biology of the judicial and cove-
nantal viewpoint of seventeenth-century Whigs —the philosophers of
the voluntary political contract—and the eighteenth-century Scot-
tish Enlightenment thinkers —the philosophers of the voluntary eco-
nomic contract.

Nevertheless, this shift in language is misleading. There is no
escape from covenantalism. Covenants are inescapable concepts.
Many attempts have been made over the last three centuries to con-
vert the three covenantal institutions into contractual ones, but the
biblical fact is, men produce broken covenants when they speak of
Church, State, and family as merely contractual. Men are self-
deceived when they speak this way. There will always be some new

9, Gordon S. Wood, The Creation of the American Republic, 1776-1787 (Williams-
burg, Virginia: Institute of Early American History, published by the University of
North Carolina Press, 1969), p. 600.

10. Charles Backus, Sermon Preached at Long Meadow (1788); cited in #bid., p. 601,

11. On this point, see F, A, Hayek, The Constitution of Liberty (Chicago: Univer-
sity of Chicago Press, 1960), pp. 58-59. For my critique of Hayek’s evolutionism,
see Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), Appendix B: “The Evolutionists’ Defense of the
Market.”

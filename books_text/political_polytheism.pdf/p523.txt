“We the People”: From Vassal to Suzerain to Serf 499

a power to propose a consolidated government instead of a con-
federacy? Were they not deputed by States, and not by the people?
The assent of the people, in their collective capacity, is not necessary
to the formation of a federal government. The people have no right
to enter into leagues, alliances, or confederations: they are not the
proper agents for this purpose: States and sovereign powers are the
only proper agents for this kind of government. Show me an in-
stance where the people have exercised this business: has it not
always gone through the legislatures? . . . This, therefore, ought to
depend on the consent of the legislatures.”

Henry said emphatically of the delegates, “The people gave them
no power to use their name. That they exceeded their power is per-
fectly clear.” He reminded them of the original authorization of the
convention: “The federal convention ought to have amended the old
system; for this purpose they were solely delegated: the object of
their mission extended to no other consideration.” But since the
legislatures authorized the conventions, they in effect had sanctioned
this public transfer of the locus of sovereignty.

Divine Right, Closed Universe

Henry could not overcome Americans’ commitment to a new
theology, the theology of the divine right of the invisible People. This
theology had now replaced the divine right of kings and the divine
right of Parliament. There could ultimately be no appeal beyond the
sovereign will of the voters. The People as a collective unit are best
represented by the voters. The People collectively are originally sov-
ereign; hence, the voters are intermittently sovereign. Men can
build in institutional safeguards against the misuse of this authority
—the Constitution is full of them — but ultimately the voters are sov-
ereign. The People speak through the voters. This was why the Con-
vention appealed to a plebiscite of voters, state by state, not as they
were legally represented in the established legislatures, but in state~
wide conventions — mini-conventions modeled along the lines of the
Philadelphia Convention, and equally controlled by the same na-
tional political faction, The language of political philosophy in 1787

26. Fam using the version in Norine Dickson Campbell, Patrick Henry: Patriot and
Statesman (Old Greenwich, Connecticut: Devin-Adair, 1969), p. 338. This appears in
Jonathan Elliot (ed.), The Debates in the Several State Conventions on the Adoption of the
Federal Constitution as Recommended by the General Convention at Philadelphia in 1787, 5
vols. (Philadelphia: Lippincott, [1836] 1907), III, p. 22

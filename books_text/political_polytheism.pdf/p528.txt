504 POLITICAL POLYTHEISM

It was what John Marshall formally announced concerning the
sovereignty of the Supreme Court, not what the Framers announced
about it, even in The Federalisi, that has determined the history of
civil government in the United States. That the Court under Chief
Justice Earl Warren produced what Professor Alexander Bickel
called a web of subjectivity?’ should surprise no one. This web of
subjectivity is the inevitable product of a combining of two doc-
trines: the biblical doctrine of hierarchical representation and the
Darwinian doctrine of the autonomy of man in a world of ceaseless
flux. The mythical “higher law” of natural law theory was erased
from modern man’s thinking by Darwin, as Rushdoony noted in
1969.8 This left the Voice of the People in control. This voice in the
United States is the latest pronouncement of the civil agency beyond
which there is no judicial appeal: the Supreme Court.

Point Two of the Covenant

Hierarchy is the second point of the biblical covenant model. It is
the section that deals with representation. Some office, agency, or in-
dividual must represent the people before the throne of God and
God before the people. In the church, this is the local pastor or eld-
ers, In denominations, it will be the General Assembly, or in some
cases, the Synods or Presbyteries acting as a constitutional unit. But
the agency, commission, or person with the authority to issue a bind-
ing judgment on disputed cases is the jinal earthly authority for that
sphere of covenantal human government. In the U.S. government,
this clearly is the Supreme Court.

There is no escape from the principle of judicial authority, There
must always be a final earthly court of appeal, It can in theory be a
plural voice, however: legislature, court, and executive combined,
or any two of them. In the twentieth century, the U.S. Supreme
Court has become America’s final court of appeal. Five justices
speak for the invisible People through the judicially flexible words of
the Constitution. The Framers did not recognize this possibility.
They did not even bother to stipulate how many Supreme Court jus-
tices there should be. They did not understand point two of biblical
covenantalism, although the Constitution is structured in terms of

37. Alexander M. Bickel, The Supreme Court and the Idea of Progress (New York:
Harper & Row, 1970), ch. 3: “The Web of Subjectivity.”

38, R. J. Rushdoony, ‘he Biblical Philosophy of History (Nutley, New Jersey: Pres-
byterian and Reformed, 1969), p. 7.

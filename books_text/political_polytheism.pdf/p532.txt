508 POLITICAL POLYTHEISM

cess either of the other two; and that all possible care is requisite to
enable it to defend itself against their attacks." Hamilton was
wrong. At least some of the Antifederalists saw what was coming.
“The weakening of the place of the jury, the provision for a complete
system of national courts, the extensive jurisdiction of the national
judiciary, the provision for appeal to the Supreme Court on ques-
tions of fact as well as law, and the supremacy of the Constitution
and the laws and treaties made thereunder all seemed to give enor-
mous power over the daily concerns of men to a small group of irre-
sponsible judges.”© Storing then cites “Brutus,” whose Antifederalist
writings he regards as the best regarding the federal judiciary.*!
“Brutus” prophesied that “the supreme court under this constitution
would be exalted above all other power in the government, and sub-
ject to no controul.”® He forecast clearly what subsequently has
taken place:

The power of this court is in many cases superior to that of the legisla-
ture. I have shewed, in a former paper, that this court will be authorised to
decide upon the meaning of the constitution, and that, not only according
to the natural and ob{vious] meaning of the words, but also according to
the spirit and intention of it, In the exercise of this power they will not be
subordinate to, but above the legislature. For all the departments of this
government will receive their powers, so far as they are expressed in the
constitution, from the people immediately, who are the source of power.
The legislature can only exercise such powers as arc given them by the con-
stitution, they cannot assume any of the rights annexed to the judicial, for
this plain reason, that the same authority which vested the legislature with
their powers, vested the judicial with theirs—both are derived from the
same source, both therefore are equally valid, and the judicial hold their
powers independently of the legislature, as the legislature do of the
judicial. ~The supreme cort then have a right, independent of the legisla-
ture, to give a construction to the constitution and every part of it, and
there is no power provided in this system to correct their construction or do
it away. If, therefore, the legislature pass any laws, inconsistent with the
sense the judges put upon the constitution, they will declare it void; and
therefore in this respect their power is superior to that of the legislature. In

49, Hamilton, Federalist No. 78: The Federalist, p. 523.

50. Herbert J. Storing, What the Anti-Federalists Were For (Chicago: University of
Ghicago Press, 1981), p. 50.

51. Storing, introductory remarks, “Essays of Brutus,” The Complete Anti-Fed-
eralist, 7 vols. (Chicago: University of Chicago Press, 1981), II, p. 358.

52. Ibid, IL, pp. 437-37.

 

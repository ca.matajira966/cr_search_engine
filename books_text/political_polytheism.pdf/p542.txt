518 POLITICAL POLYTHEISM

The remarkable fact is that this development was foreseen clearly
by “Brutus.” Analyzing the Preamble, he recognized that a vast ex-
pansion of national political power was inevitable:

To discover the spirit of the constitution, it is of the first importance to
attend to the principal ends and designs it has in view. These are expressed
in the preamble, in the following words, viz. “We, the people of the United
States, in order to form a more perfect union, establish justice, insure
domestic tranquility, provide for the common defence, promote the general
welfare, and secure the blessings of liberty to ourselves and our posterity, do
ordain and establish this constitution,” &c. If the end of the government is
to be learned from these words, which are clearly designed to declare it, it is
obvious it has in view every object which is embraced by any government.
The preservation of internal peace—the due administration of justice—and
to provide for the defence of the community, seems to include all the objects
of government; but if they do not, they are certainly comprehended in the
words, “to provide for the general welfare,” If it be further considered, that
this constitution, if it is ratified, will not be a compact entered into by
states, in their corporate capacities, but an agreement of the people of the
United States, as one great body politic, no doubt can remain, but that the
great end of the constitution, if it is to be collected from the preamble, in -
which its end is declared, is to constitute a government which is to extend to
every case for which any government is instituted, whether external or in-
ternal, The courts, therefore, will establish this as a principle in expound-
ing the constitution, and will give every part of it such an explanation, as
will give latitude to every department under it, to take cognizance of every
matter, not only that affects the general and national concerns of the union,
but also of such as relate to the administration of private justice, and to reg-
ulating the internal and local affairs of the different parts.”*

The means of this centralization of power, he predicted, would
be the Supreme Court’s power of judicial review:

Perhaps nothing could have been better conceived to facilitate the aboli-
tion of the state governments than the constitution of the judicial. They will
be able to extend the limits of the general. government gradually, and by in-
sensible degrees, and to accomodatc:themselves to the temper of the people.
Their decisions on the meaning of the constitution will commonly take
place in cases which arise between individuals, with which the public will
not be generally acquainted; one adjudication will form a precedent to the
next, and this to a following one. These cases wil] immediately affect indi-
viduals only; so that a series of determinations will probably take place be-

76. “Brutus,” Complete Antt-Federalist, II, p. 424.

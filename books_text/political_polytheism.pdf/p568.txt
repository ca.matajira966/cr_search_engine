544 POLITICAL POLYTHEISM

Power shifts were taking place within the denomination. Increased
immigration from Scotland was making the church more theologi-
cally conservative, less enthusiastic about the heritage of the Great
Awakening.® At the same time, these immigrants were heading
West, where there were no well-organized presbyteries.*° There was
also a growing reaction against Deism, skepticism, and the increas-
ingly liberal rationalism of the remnants of Jonathan Edwards’ ra-
tionalistic theology, the New Side heritage.

Attendance at the annual synod meetings had declined during
the war and had not recovered. The expanding geography of the
American nation by 1780 had overthrown the theory of a single an-
nual synod meeting that could handle all business not capable of
being handled at the presbytery level.*! Changes were needed. A
committee was appointed in 1785 to draw up a new form of Presby-
terian discipline. Then, later in the day, another overture was sug-
gested: the creation of a General Assembly, along the lines of the
Scottish church, and the creation of three synods. The records do not
indicate who made this overture.”

On the face of it, this overture was highly peculiar, If the institu-
tional problem facing the denomination was geographical, why
would anyone propose the creation of a General Assembly? The an-
swer should have been obvious: to ceniralize the denomination once and for
all. If the regional presbyteries were becoming more distant from the
center, then there would have to be a central representative body as well
as central judicial body that could hold the Church’s governmental sys-
tem together. (This was exactly what Madison had concluded re-
garding American civil government.) The Committee on Overture
took over; a second study on Church government began. As is usual
for Presbyterianism, no official decision was made at that time.
(This was paralleled by the late-March meeting at Mount Vernon at
which Maryland and Virginia commissioners proposed ways of settl-
ing trade disputes. And like the Synod meeting, the records of what
took place are unclear.)*

39. Ibid, pp. 263-64.

40, Tbid., pp. 268-72.

41, Ibid., pp. 281-82.

42. Ibid., p. 283.

43, Writes Burton K. Hendrick: “The gathering attracted little attention at the
time, and has not figured extensively in history since. Yet its outcome, two years
afterward, was the Constitution of the United States.” Hendrick, Bulwark of the
Republic: A Biography of the Constitution (Boston: Little, Brown, 1937}, p. 11.

562 POLITICAL POLYTHRISM

legal immunities from State interference? At some point in time, the
Constitution will become too great a threat to one side or the other:
to covenant-breakers who resent any residue of Constitutional re-
straint or to covenant-keepers who have been pushed to the limits of
their endurance by the culmination of the original apostate cove-
nant. The Constitution’s provisions were written by self-consciously
apostate men and conspiratorial Christian colleagues whose under-
standing of the biblical covenant had been eroded by a lifetime of
Newtonian philosophy and training in the pagan classics. Neverthe-
less, these men were under restraints: philosophical (natural rights
doctrines) and political (a Christian electorate), Both of these re-
straints have almost completely disappeared in the twentieth cen-
tury. Thus, the evils implicit in the ratified national covenant have
grown more evil over time.

Declining Restraints

The first set of restraints on the Framers was philosophical: natu-
val rights philosophy. Officially, the Constitution does not recognize
natural rights. It was from the beginning far more in tune with the
Darwinian world to come than the world of eighteenth-century
Whig moral philosophy. Today, almost no one in a place of intellec-
tual influence or political authority defends the older natural rights
viewpoint. Take the case of the man who is perhaps the most distin-
guished and best-known legal scholar and judge in American conser-
vatism, Robert Bork. Because of his conservative judicial views,
Bork was refused confirmation to the Supreme Court by the U.S.
Senate in 1987, We might expect him to be a defender of natural
rights. Not so. He was the author of a 1971 essay denying the natural
rights foundation of judicial decisions. He denied that moral consi-
derations can properly enter into judicial decisions, except insofar as
the political decision of the legislature has colored a law.? Judges, he
insisted, must remain morally neutral. The older, pre-Darwin moral
framework for American Constitutional law is dead. It was a long
time dying, both philosophically and judicially.?

2. Robert Bork, “Natural Principles and Some First Amendment Problems,”
Indiana Law Journal (Fall 1971). In this essay, Bork called on judges to adopt a princi-
ple of moral neutrality in making judicial decisions. Critical of Bork was Christian
Reconstruction columnist John Lofton, “Soft on Natural Rights?” Washington Times
(July 8, 1987),

3. Gary Jacobsohn, The Supreme Court and the Decline of Constitutional Aspiration
(Totowa, New Jersey: Rowman and Littlefield, 1986).

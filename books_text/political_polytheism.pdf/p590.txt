566 POLITICAL POLYTHEISM

They are incorrect. There is covenantal continuity in the United
States as surely as there was in the Northern Kingdom in Elijah’s
day. It is the continuing presence of people who affirm the gospel that provides
covenantal continuity with the past, as well as with the future, It is this cove-
nantal continuity that will bring forth (and has brought forth) God’s
historic sanctions — sanctions leading either to national oblivion, as
they did in North Africa in the seventh century, or to covenantal res-
toration, Let us pray that it will be the latter.

Jeroboam’s Priesthood

The U.S. Constitution is one step beyond Jeroboam’s golden
calves, but not yet the covenant of Ahab and Jezebel. Today’s politi-
cal leaders are the judicial equivalent of Jeroboam’s priesthood.
They are morally superior to Ahab’s 450 priests of Baal and 450
priests of the groves (Asherah). Christians therefore should defend
the golden calf of the Constitution as a temporary device that gives
us freedom to work for an eventual return to Jerusalem.

We now face the threat of a transition to the Ahab and Jezebel
school of civil religion, if the plans for a new Constitutional Conven-
tion go through. One thing is clear, however: Jeroboam’s halfway
covenant world did not survive. Neither did the Articles of Con-
federation. Jeroboam’s halfway covenant moved forward into Ahab’s
Baalism. We also live under a transitional covenantal settlement.
Either this nation will return to its pre-Constitution orthodoxy or
else it heads into outright paganism. Judicially speaking, the latter is
more likely than the former. We are already judicially pagan.

Covenant Sanctions: Church and Family

The Anglo-Saxon Masons of the eighteenth century ridiculed
atheism, but the atheists have triumphed, just as a handful of Anti-
federalists predicted in state ratifying conventions and pamphlets,
The Masons used the widely accepted doctrine of neutral natural
law to undergird their appeal to religious toleration. They took their
Unitarian Masonic oaths in secret, and they took their secular politi-
cal oaths, and they thought that all was well. It wasn’t. The worst
elements have inherited the supposedly neutral kingdom.

The American Civil Liberties Union has few supporters within
the lodges of the deeply Masonic South. The good old boys with
their fezes and Shriner regalia do not cheer when the ACLU gets a
federal injunction against having a Christmas manger scene on the

The Restoration of Biblical Pluralism 589

correct against Pelagius; Luther was correct against Erasmus; Calvin
was correct against Pighius; and the Puritans were correct against
the latitudinarians. Man’s lack of free will is why God can easily
change the spiritual condition of this world in order to give the
human race freedom.

Second, because we Calvinistic Christian Reconstructionists be-
lieve that the Holy Spirit forces hearts to change — the doctrine of ir-
resistible grace—we also believe that human institutions are not
allowed to seek to coerce men’s hearts and minds. Such coercion of the
Auman will—its transformation prior to the permission of the individ-
ual whose will is being transformed—is @ monopoly that belongs exclu-
sively to God.

Men must recognize that coercion is an inescapable concept in
history, It is never a question of coercion vs. no coercion. It is always
a question of whose coercion. Reconstructionists affirm the power of
the Holy Spirit to change men’s souls — to declare judicially that they
are saved, and therefore possess Christ’s righteousness— and to change
them ethically at the point of their ethical transformation, Those
who deny this exclusive power of the Spirit in transforming the lives
of covenant-breakers instinctively expect to find coercion some-
where else: in human institutions— either humanist or “theocratic-
bureaucratic” —or in a future personal kingdom ruled by Christ in
Person.

Third, because we postmillennialists find it taught in the Bible
that there will be a future outpouring of this soul-transforming Holy
Spirit—the only possible basis of the Bible’s prophesied millennial
blessings—we disagree with premillennialists and amillennialists
concerning the limited extent of the Spirit’s work in the future. The
kingdom will not be brought in by a bureaucratic theocratic regime,
but by the heart-transforming work of the Holy Spirit. We therefore

 

Jacob have Lloved, but Esau have I hated. What shall we say then? Is there unright-
eousness with God? God forbid. For he saith to Moses, I will have mercy on whom 1
will have mercy, and I will have compassion on whom I will have compassion. So
then it is not of him that willeth, nor of him that runneth, but of God that sheweth
mercy. For the scripture saith unto Pharaoh, Even for this same purpose have I raised
thee up, that I might shew my power in thee, and that my name might be declared
throughout all the earth. Therefore hath he mercy on whom he will have mercy, and
whom he will he hardeneth. Thou wilt say then unto me, Why doth he yet find
fault? For who hath resisted his will? Nay but, O man, who art thou that repliest
against God? Shall the thing formed say to him that formed it, Why hast thou made
me thus? Hath not the potter power over the clay, of the same lump to make one
vessel unto honour, and another unto dishonour?” (9:11-21),

592 POLITICAL POLYTHEISM

Gnostic Dualism

Another ancient heresy that is still with us is gnosticism. It be-
came a major threat to the early Church almost from the beginning.
It was also a form of dualism, a theory of a radical split. The gnostics
taught that the split is between evil matter and good spirit. Thus,
their goal was to escape this material world through other-worldly
exercises that punish the body. They believed in retreat from the world
of human conflicts and responsibility. Some of these ideas got into the
Church, and people started doing ridiculous things. So-called “pillar
saints” became temporarily popular in the fifth century, a.p. A
“saint” would sit on a platform on top of a pole for several decades
without coming down. This was considered very spiritual.2> (Who
fed them? Who cleaned up after them?)

Thus, many Christians came to view “the world” as something
permanently outside the kingdom of God. They believed that this
hostile, forever-evil world cannot be redeemed, reformed, and
reconstructed, At best, it can-be subdued by power (maybe). Jesus
did not really die for it, and it cannot be healed. This dualistic view
of the world vs. God’s kingdom narrowly restricted any earthly man-
ifestation of God’s kingdom. Christians who were influenced by
gnosticism concluded that God’s kingdom refers only to the institu-
tional Church. They argued that the institutional Church is the only
manifestation of God’s kingdom.

This led to two opposite and equally evil conclusions. First,
power religionists who accepted this definition of God’s kingdom
tried to put the institutional Church in charge of everything, since it
is supposedly “the only manifestation of God’s kingdom on earth,” To
subdue the supposedly unredeemable world, which is forever outside
the kingdom, the institutional Church has to rule with the sword.
The institutional Church must give orders to the State, and the State
must enforce these orders with the sword.* The institutional Church
must therefore concentrate political and economic power. What then
becomes of liberty?

35. Kenneth Scott Latourette, A History of Christianity (New York: Harper & Row,
1953), pp. 228, 298.

36. The innovator here was Pope Gregory VII in the late eleventh century. He
launched a major revolution in the West. Harold J. Berman, Faw and Revolution: The
Formation of the Western Legal Tradition (Cambridge, Massachusetts: Harvard Univer-
sity Press, 1983), ch. 2. Cf, Walter Ulmann, The Growth of Papal Government in the
Middle Ages: A study in the ideological relation of clerical to lay power (2nd ed.; London:
Methuen, 1962), ch. 9.

The Restoration of Biblical Pluralism 603

omission. They have failed to press the claims of Jesus Christ in
every area of life. They have failed to challenge the sins of this age.
They have refused to tell the world that God really does have specific
answers for every area of life, including economics and politics.
Christians have preferred to comfort themselves as they have sat in
their rocking chairs in the shadows of history, rocking themselves
back and forth, and saying over and over: “I am not a theocrat. I am
not a theocrat.” What this phrase means is simple: God does not govern
the world, so Christians are off the hook,

But what if God does rule? What if He has given us the unchang-
ing laws by which He expects His people to rule? What if He has
given us the tools of dominion, and we have left them in the rain to
rust? What will He do with our generation?

Just what He did with Moses’ generation: He will leave them
behind to die in the wilderness,

Christians in the Protestant era have not wanted to think about
God’s sanctions in history. This has been the number-one ethical,
theological, and institutional problem ever since the Protestant Ref-
ormation: the nature of legitimate sanctions in Church and State.
(The Roman Church abandoned biblical law early in the Middle
Ages; this rejection of point three of the biblical covenant has been
an ecumenical sin of omission ever since.) Because Christians have
self-consciously refused to exercise God’s sanctions in history—in
Church, State, and family —we are now facing an escalating series of
crises in Church, State, and family. Only repentance and reform will
change the modern world’s course toward a head-on collision with
God’s historical sanctions.

Reform must begin with self-government under God’s law.

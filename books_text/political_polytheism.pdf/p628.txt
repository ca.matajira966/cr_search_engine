A world without judgment is a world without values and hence
without meaning. To deny judgment is to deny value and meaning.
Without judgment, there can be no cultural progress, and the only valid
form of judgment is that which is grounded in the word of God. The
general revelation of God to mankind enabled some cultures to prog-
ress to a point, until the relativism inherent in man’s original sin,
his desire to be his own god (Gen. 3:5), subverted judgment in his
society. With relativism came stagnation and deterioration.

Moreover, judgment and salvation cannot be separated from one
another. Those theologies which hold to judgment, while having a
limited view of salvation, as witness premillenial and amillennial
theologies, cut the vital nerve of both doctrines. Only as man has a
total concept of salvation, of victory in time and eternity, can he apply
a total concept of judgment to every sphere of life. Judgment is of ne-
cessity total wherever it is held that every sphere of life must be brought
into captivity to Christ, because every sphere must manifest His sal-
vation as an aspect of His new creation. A doctrine of salvation which
calls for man’s redemption, and limits that redemption to his soul now
and his body in the general resurrection, is defective. The redeemed
man will of necessity, because it is basic to his life, work to bring re-
demption to every sphere and area of life as an aspect of his creation
mandate. The redeemed man’s warfare against the powers of evil is
not “after the flesh,” i.e., does not rely on human resources, but relies
rather on the supernatural power of God. Everything that exalts itself
against the knowledge of God, St. Paul tells us, shall be cast down. In
the words of Arthur Way’s rendering of II Corinthians 10:3-5,

Very human as I am, I do not fight with merely human weapons.
No, the weapons with which I war are not weapons of mere flesh and
blood, but, in the strength of God, they are mighty enough to raze all
strong-holds of our foes, I can batter down bulwarks of human reason,
I can scale every crag-fortress that towers up bidding defiance to the
true knowledge of God. I can make each rebel purpose my prisoner-
of-war, and bow it into submission to Messiah,

R. J. Rushdoony (1983)*

*Rushdoony, Salvation and Godly Rule (Vallecito, California: Ross House Books,
1983), pp. 22-23.

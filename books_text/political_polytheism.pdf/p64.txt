40 POLITICAL POLYTITEISM

3, Ethies/Law/Dominion

“Drop down, ye heavens, from above, and let the skies pour
down righteousness: let the earth open, and let them bring forth sal-
vation, and let righteousness spring up together; I the Lorp have
created it” (Isa. 45:8). The whole cosmos is described here as being
filled with righteousness. Righteousness is the basis of man’s domin-
ion over the earth.

But righteousness must be defined, This is what God’s law does.
It establishes boundaries to our lawful actions. The tree of the knowl-
edge of good and evil was “hedged in” by God’s law. Adam and Eve
were not to eat from it, or as Eve properly interpreted, even touch it
(Gen, 3:3).

These ethical boundaries are not exclusively personal; they are
also corporate. There are biblical laws given by God that are to gov-
ern the actions of families, churches, and civil governments. Auton-
omous man would like to think that God’s law has nothing to do with
his institutions, especially civil government, but autonomous man is
in rebellion. Ged’s law is not restrained by autonomous man’s
preferred boundaries. It is not man who lawfully declares: “Fear ye
not me? saith the Lorn: will ye not tremble at my presence, which
have placed the sand for the bound of the sea by a perpetual decree,
that it cannot pass it: and though the waves thereof toss themselves,
yet can they not prevail; though they roar, yet can they not pass over
it? But this people hath a revolting and a rebellious heart; they are
revolted and gone. Neither say they in their heart, Let us now fear
the Lorn our God, that giveth rain, both the former and the latter,
in his season: he reserveth unto us the appointed weeks of the har-
vest” (Jer. 5:22-24).

Notice the development of God’s argument, which is in fact a cov-
enani lawsuit brought against Judah by His prophet, Jeremiah. God
sets boundaries to the sea, the seasons, and the harvest. The impli-
cation is that He also sets /egal and moral boundaries around people,
both as individuals and nationally. Men are to fear this God who sets
cosmic boundaries. How is this required fear to be acknowledged?
The prophets answered this question over and over, generation after
generation: by obeying God's law.

4, Oath/ Judgment/Sanctions
“I have sworn by myself, the word is gone out of my mouth in
righteousness, and shall not return, That unto me every knee shall

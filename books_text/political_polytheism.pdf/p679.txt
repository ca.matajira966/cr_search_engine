Conclusion 655

judicial roots by covenanting with a new god, the sovereign People.
There would be no other God tolerated: in the new political order.
There would be no appeal beyond this sovereign god,

This collective god, speaking through the federal government,
began its inevitable expansion, predicted by the Antifederalists,
most notably Patrick Henry. The secularization of the. republic
began in earnest. This process has not yet ceased. Brown is correct:
“The most revolutionary aspect of the founding of the United States
was neither independence nor democracy, but the new government’s
official neutrality on religion, which has gradually turned into a mild
to severe antagonism toward religion. Lamentably, this antagonism is
now helping to undermine all the foundations of a humane America,”®!

Nevertheless, this surrender to secular humanism was not an
overnight process. The rise of Unitarian abolitionism,** the coming of
the Civil War, the advent of Darwinism, the expansion of immigra-
tion, the spread of the public school system, the rise of state licensing
and the concomitant growth of university certification, and a host of
other social and political influences have all worked to transform the
interdenominational American civil religion into a religion not fun-
damentally different from the one that Jeroboam set up, so that the
people of the Northern Kingdom might not journey to Jerusalem in
Judah to offer sacrifices (I Ki. 12:26-31). The golden calves may not
be on the hilltops, but the theology is the same: religion exists to
serve the needs of the State, and the State is sovereign over the mater-
ial things of this world. There are many forms of idol worship. The
worship of the U.S. Constitution has been a popular form of this an-
cient practice, especially in conservative Christian circles. It is not
seen as a flawed tool in need of revision but as a holy witness to the
truth of the moral validity of permanent political pluralism.

Pre-Constitutional Covenants

The covenantal sanctions of the pre-Constitutional colonial cove-
nants are still in force. Those oaths were taken in good faith before a
God who does not forget. One cannot break covenantal continuity
with the Great King at zero price. He will bring additional negative
sanctions unless those original covenants are renewed. This, how-

51, Ibid,, p. 134.

52. Christians did yeoman service in the household of their theological enemies.
Cf. Bertram Wyatt-Brown, Lewis Tappan and the Evangelical War Against Slavery
(Cleveland, Ohio: Gase Western Reserve University Press, 1969),

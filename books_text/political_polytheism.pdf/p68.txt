44 POLITICAL POLYTHEISM

God is not sovereign.

You need not obey Him.

His law is not authoritative.

. The promised negative sanction will not come.
. [implicd:] You will keep the inheritance.

ORY N

I choose to analyze the biblical definition of antinomianism in
terms of Satan’s temptation of Eve. This line of satanic reasoning is
the heart of all antinomianism.

4. Transcendence/Immanence

Who is God? Satan was asking Eve to decide. Who lays down the
law? Whose word is authoritative?

Obviously, the Creator is God. Then who is the true creator,
man or God? This is what Satan was asking mankind, God’s chrono-
logical and judicial representative. If man answered anything but “God
is the Creator, and His Word alone is authoritative,” then Satan
would inherit the earth. Man would die unless, of course, God should
later send His Son, the second Adam, to inherit it, but Satan chose
either to ignore this possibility or to act against what he knew would
happen in the future.

The first step in becoming an antinomianism is to deny the abso-
lute sovereignty of God. It usually begins with a denial, implicit or
explicit, that God created the world. This usually begins with a soft-
ening of the doctrine of the six, literal, 24-hour-day creation. This is
how the seeds of Darwinism were sown: denying the literal character
of God’s chronology in Genesis 1.'°

The next step is to deny the obvious implication of the doctrine of
creation: since God created the world, He also controls the world. In
other words, men deny the absolute sovereignty of God and the
providence of God. They deny the doctrine of predestination.17

Why is a denial of predestination inherently antinomian? Be-
cause it means that events in history come to pass outside of God’s
decree. They are therefore random events in terms of His decree,
what philosophers call conéingent events. An element of contingency is
thereby brought into the universe. If A takes place, B may not take

16. North, Dominion Covenant: Genesis, Appendix C: “Cosmologies in Conflict:
Creation vs. Evolution.”

17, Loraine Boettner, The Reformed Doctrine of Predestination (Philadelphia: Presby-
terian & Reformed, [1932] 1965).

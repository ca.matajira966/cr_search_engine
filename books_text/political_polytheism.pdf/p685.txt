Conclusion 661

It is understandable that men do not want to grant to foreigners
the sanction of the suffrage. They believe that if someone has fled
from another place of residence, the refugee should not expect to tell
his hosts how to run their country. It takes generations to learn the
ways of a foreign nation, unless the immigrant has already accepted the reli-
gious views that undergird that nation. If he accepts the religion of the
host nation, then he has made the transition. He is ready for citizen-
ship. If he is willing to come under the sanction of the sacraments,
then he is ready to exercise civil judgment in terms of the law of the
covenant,

Because modern democracies honor only the civil sacrament of
voting, they resist turning over this authority to immigrants who
can, in a mass democracy, vote their way into the pocketbooks and
bank accounts of those who now reside in the host nation. So, the
residents want to limit immigrants to those who have equally large
pocketbooks and bank accounts. Canada welcomes Hong Kongers
who can bring large sums of rnoney with them. Nations allow im-
migrants to buy their way in. Closed borders are the price of political
“open communion”—one man, one vote.

Until Christians understand the principle of closed communion
and excommunication in the Church (point four of the Church cove-
nant), and until they link this by civil law with the exercise of the
franchise (point four of the State covenant), they will not understand
the biblical principle of open borders and sanctuary. It will take a
paradigm shift of historic proportions to accomplish this transforma-
tion, Christians will first have to abandon both Witherspoon and
Madison, and for over two centuries, they have been unwilling to
think about this, let alone do it.

Conclusion

It is not enough to know what to do; it is required that those who
know do it. “Therefore to him that knoweth to do good, and doeth it
not, to him it is sin” (James 4:17), The problem today ~ and for the

 

ain has agreed to deliver Hong Kong to the Chinese Communists in 1997. Hong
Kong’s citizens are being denied access to a legal escape route to Britain, despite the
fact that Hong Kong had been part of the British Empire and still is part of the Com-
monwealth, Britain’s voters are racists, but no politician dares say this in public.
They remember what happened to Enoch Powell, M.P., who did say this, in the
early 1970's. He was attacked in the press by every liberal in England, who today re-
main silent regarding Hong Kong.

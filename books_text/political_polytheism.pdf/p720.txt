696 POLITICAL POLYTHEISM

factions have cancelled themselves out is the common good — the reli-
giously neutral common good.

The fact that Madison did not appeal to an abstract concept of
rationality is irrelevant; the Framers, both individually and as a fac-
tion, always balanced their appeals to abstract rationality with an
appeal to historical experience. This, as Van Til argues, is what cov-
enant-breaking men have done from the beginning. This is the old
Parmenides-Heraclitus dualism. Madison appealed to reason, expe-
rience, common sense, morality, and any other slogan he could get
his hands on. “The free system of government we have established is
so congenial with reason, with common sense, and with universal
feeling, that it must produce approbation and a desire of imitation,
as avenues may be found for truth to the knowledge of nations.”® So
did his colleagues. There men were politicians, first and foremost. If
a slogan would sell the Constitution, good; if an brilliant idea would,
excellent; if a convoluted or improbable argument would, fine. It
was all grist for their Unitarian mill. Christians should not be de-
ceived, especially self-deceived.

James Madison was a covenant-breaking genius, and the heart
and soul of his genius was his commitment to religious neutralism.
He devised a Constitution that for two centuries has fooled even the
most perceptive Christian social philosophers of each generation into
thinking that Madison was not what he was: a Unitarian theocrat
whose goal was to snuff out the civil influence of the Trinitarian
churches whenever they did not support his brainchild. For two cen-
turies, his demonic plan has worked.

Rushdoony’s equating of Enlightenment rationalism with a priori
rationalism, and then his denial that the Americans ever affirmed a
priori rationalism, is at the heart of his general myth that there was
never a serious Enlightenment in colonial America. It is also at the
heart of the traditional conservatives’ myth that Burkean conser-
vatism was not part of the Enlightenment. Both views are myths.
Burke adopted and promoted the social evolutionary worldview of the
Scottish Enlightenment. The Scots were all intellectual colleagues.
They were all members of the right wing of the Enlightenment, just
as F. A, Hayek is. There was no one left on either side of the Atlantic
who was publicly preaching the Puritan view of the covenant, meaning

80. Cited in Adrien Koch, Power, Morals, and the Founding Fathers: Essays in the In-
lerpretation of the American Enlightenment (Ithaca, New York: Cornell University Press,
[1961] 1975), p. 105.

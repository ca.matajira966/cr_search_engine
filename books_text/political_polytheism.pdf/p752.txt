728 POLITICAL POLYTHEISM

Christian, 370
Church crisis, 533
common ground, 522-28
covenant needed, 523-24
eclectic, xvi
Enlightenment, xvi
five sources, 321
grievances, 527
justification of, 524-25, 530
pastors, 525-26
Rush's hopes, 528n
sovereignty, 378-79
state covenants, 526
Unitarian, 524-25
war weariness, 543
“American Whig,” 549
amillennialism
history, 638
inheritance, 42
national confession, 559
no sanctions, 609
“postmillennial,” 139
predestination &, 616
quicksand, 153
sanctions, 141-53, 638
Van Til, 136-39, 141-47
“victory,” 141-46, 153, 606-10
amnesia (covenantal), 294
Anebaptism, 702-3
anarchism, 454, 641-42
Anderson’s Constitutions, 466-69
angels, 7in
animism, 350
Ankerberg, John, 43in
Antifederalists
Bill of Rights, 496
classical references, 461
fear of judiciary, 508
localism, 459
older men, 456
Roman pseudonyms, 327
weak position, 459
Whigs, 366
antinomianism
assumptions, 158
Christian civilization denied, 178,
542 (see also Marsden, political
pluralism, political polytheism)

defined, 43-52
denials, 51
Edwards, 367
five points, 27-28
free will, 45
leaders, 626
legalism, 209
national covenant, 35
Niebuhr, 668-69
pessimillennialism, 51
political pluralism &, 158
predestination &, 44-45
results of, 580
social, 49, 153-54, 186
Van Til, 130-31
see also pro-noraianism
Anti-Revolutionary Party, 680n,
apocalypticism, 138
appeals court, 39, 99-100, 401, 494,
501, 504-5, 509-13, 529
Aquinas, Thomas, 333
Arbelta, 244
architecture, 536
Arianism, 349
Ark of Covenant, 103-4, 342, 63in
Arkansas, 288
Arminianism, 19, 155, 167, 334, 357,
359, 626, 658-59
army (Masonic influences), 423-24,
47
Article VI (oaths), 385-91
central provision, 410
eternal sanctions vs., 403
fear of, 390-81
god of politics, 528
states rights, 392
see also test oath
Articles of Confederation
by-laws, 407, 416, 491, 531
covenant, 531
election, 384
failure of, 379, 384, 571
gone forever, 571
halfway covenant, 379, 458, 460-61
Jensen on, 455
“lag,” 397
less Christian?, 534

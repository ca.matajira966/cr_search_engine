52 POLITICAL POLYTHEISM

away the continuing authority of Deuteronomy 28. I therefore see
the inescapable theological necessity of restoring the biblical defini-
tion of biblical law and therefore anti-law.

I fully realize that my definition of antinomian is not the accepted
usage. This common usage exists primarily because theological anti-
nomians who have rejected one or more of the covenant model's five
points have previously defined the word so that it conforms to their
pessimistic historical outlook: the long-term cultural impotence of
God’s redeemed people in history. They argue that antinomianism is
merely the denial of one’s personal responsibility to obey God’s moral
law (undefined).*? This deliberately restricted definition implicitly
surrenders history to the devil. What I am saying is this: anyone who
denies that there are cause-and-effect relationships in history between
the application of biblical case laws and the success or failure of so-
cial institutions has also inevitably and in principle adopted the idea
that the devil controls and will continue to control this world. Why?
Because the devil’s representatives are said to be able to maintain con-
trol over the social institutions of this world throughout history
(point two of the covenant: representation). It does no good for a
person to answer that he is not an antinomian just because he
respects God’s law in his personal life, family life, and church life.
He is still saying that God’s law is historically impotent in social
affairs, that covenant-keeping or covenant-breaking offers rewards
or curses only to individuals and only after the final judgment.

Yes, I am offering a more comprehensive definition of “anti-
nomian.” My major goal in life is to lay additional foundations for a
theological paradigm shift that has already begun. I am quite self-
conscious about this task. Readers deserve to know this. One inesca-
pable aspect of a new movement or new way of viewing the world is
the creation of new terms (e.g., “theonomy”), and the redefining of
old terms. Einstein, for example, redefined several of the terms used
by Newton.*? Clearly, this is what the Barthians did with the

32. “It refers to the doctrine that the moral law is not binding upon Christians as
a way of life.” Alexander M, Renwick, “Antinomianism,” in Baker's Dictionary of
Theology, edited by Everett F. Harrison, Geoffrey W. Bromiley, and Carl F. H.
Henry (Grand Rapids, Michigan: Baker, 1960), p. 48.

33, Thomas Kuhn, The Structure of Scientific Revolutions (Ind ed.; University of
Chicago Press, 1970), pp. 101-2, 149. Kuhn writes: “Since new paradigms are born of
old ones, they ordinarily incorporate much of the vocabulary and apparatus, both
conceptual and manipulative, that the traditional paradigm had previously em-
ployed, But they seldom employ these borrowed elements in quite the traditional
way.” fbid,, p. 149.

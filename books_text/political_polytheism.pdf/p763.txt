continuity with Constitution (seal),

408
covenant, 524
created equal, 523-24
Deist, 406
foreign policy, 409
god of, 492 (see also nature’s god)
halfway covenant, 408, 492, 523,
528
incorporating document, 407,
525
Masonry downplays, 704
new nation, 523, 525
organic law, 524n
political, 311
Preamble as, 491-93
seal, 405, 407-8.
Singer on, 404
states-established, 495
Unitarian, 406, 531
unknown (1776-1795), 409
Witherspoon, 409-10
Declaratory Act, 525n
decree uf God, 45
definitions, 52-53
definitions (covenantal), 92-96
Deism
Christianity &, 351-54
Declaration of Independence, 406
god of, 38, 344-45
Jefferson, 406
linguistic subversion, 680-81
Masonic alliance, 464
Newtonianism, 347
Plato's, 346-47
test oaths, 390
textbook, 38
triumph, 400
Delaware, 384, 463, 682
democracy, 73-74, 101-2, 647-50, 657
demons, 75-77
Desaguiliers, John T., 369, 467
Descartes, René, 333-34, 356
despair, 613-14
détente, 91-92
dictionaries, 53
dikes, 8, 132-33, 260

index 739

discontinuity
cavenantal, 516
historical, 144
judicial, 506
Puritan revolution, 19
ratification of Constitutian, 419
Van Til on, 144
discrimination, 72
dispensationalism, 212-13 (see also
pessimillennialism)
dissenters, 464 (see also
Commonwealthmen)
divine right, 248, 375-77, 499-502,
541, 699
dominion
biblical law &, 576-77
confidence &, 612-13
Darwinian, 345
doubt vs., 613-14
ethics &, 144
Presbyterians vs., 616-17
righteousness & 40
sanctions &, 50
service &, 636
tools of, 76, 158-59 (see also biblical
law)
Doner, Colonel, 22n
Dooley, 397
doubt vs. dominion, 613-14
down payment (earnest), 41
Dred Scott, 512
drugs, 97, 578
dualism
casuistry, 6
common ground, 535
Constitution, 692-93
eighteenth century, 6
ethical, $35, 542-43
gnostic, 592-93
God, 629
Manichaean, 593
Marcion, 581, 591
pessimillenialism, 662
procedure vs, ethics, 542-43
Rushdoony, 692-95
time & history, 365
two gods, 591

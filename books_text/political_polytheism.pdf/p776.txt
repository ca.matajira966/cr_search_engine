752 POLITICAL POLYTHEISM

Currey review, 427
distrust of judiciary, 503
judicial review, 502n
Newton, 369
public indifference, 415
silence on Masonry, 433
sovereignty, 569n
Washington's stare, 459n
wrong questions, 370

McIntire, Garl, 166-67

Mead, Sidney, 165, 310, 400, 464,
644

mechanism, 350

media, 626

Melanchthon, Philip, 12

Mencken, H. L., 671

merchants, 527, 530

Messiah, 99

Middleman, Udo, 207

Miller, Perry
covenant, 275, 718
covenant theology, 300
influence, 325
Marsden on, 285-88, 297
national covenant, 550-51
Parrington &, 249
Williams, 250

Mills, C. Wright, 265-66

Milton, John, 22

Mises, Ludwig, 460n

missions, 530, 585

Moderns, 362-67

Molnar, Thomas, 337

money, 552-53 (see also gold)

Monkey trial (see Scopes trial)

Monsanto, 339

Montesquieu, 446

Moody, D. L., 632

Moore, T. M., 296

moral crisis, 268

moral law, 48

morals, 266

Moray, Robert, 476

More, Thomas, 104

Morecraft, Joseph, (guess)

Morgan, Edmund S.
convention, 444-45

halfway covenant, 1070, 260
ideological politics, 267n
Madison’s solution, 442
Winthrop, 538

Mormonism, 97, 534-35

Morton, Thomas, 274-75

Moses
conspiracy vs., 650
insufferable, 32
Renaissance &, 477
representative, 374
Rhode Istand?, 22

Mount Vernon, 414-15, 427, 443, 544

Murray, John, 30, 134, 582

music, 344

mytho-history (Rushdoony), 685-87,
697-98, 700

Myers, Ken, 54, 121-22

Napoleon, 680
national Christian covenant
amendment, 411, 557, 617, 653
antinomian, 35
crities of, 650-51
denial of, 2-3, 31-32, 34-35, 47,
565
god of, 522
limiting concept, 34-35
Marsden denies, 241
myth in 1800, 550-51
neutral, 298
no escape from, 564-65
positive feedback, 32-33
postmillennialism, 558-59
re-conquest, 564
rejection of, 256-61
sanctions, 640
Schaeffer vs., 180-85
transitional, 566
Van Til, 130
nationalism, 496, 497, 529-31, 680
natural law
242=4, 353
abandoned, 302, 563
alliance (1787), 333
alternatives to, 302
atheistic, 247

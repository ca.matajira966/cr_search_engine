54 ‘ POLITICAL POLYTHEISM

flect common usage after the paradigm shift. We are preparing for
this shift well in advance.

Theonomy as Utopian

Calvinist Ken Myers, a defender of Christian political pluralism,
has taken a forthright stand against utopianism, as in utopian blue-
prints for society, He stands foresquare for realism. (Or is it
Realpolitik?) He adopts a line of reasoning used by antinomians
everywhere. He contrasts the idea of perfect standards with the real-
ity of this sin-filled world. He does not do this in order to spur men
on.to greater perfection. He does not call on them to invoke this vi-
sion of a perfect blueprint as a means of reconstructing society. On
the contrary, he says that the very perfection of the blueprint may
condemn it as a tool of dominion in history. “It is never enough for us
to concoct notions of perfect government.”

But what if God Himself has “concocted” such notions?

It is God that girdeth me with strength, and maketh my way perfect (Ps.
18:32).

The law of the Lorn is perfect, converting the soul: the testimony of the
Loxp is sure, making wise the simple (Ps. 19:7).

Mark the perfect man, and behold the upright: for the end of that man
is peace (Ps. 37:37).

I will behave myself wisely in a perfect way. O when wilt thou come un-
to me? I will walk within my house with a perfect heart (Ps. 101:2).

Mine eyes shall bé upon the faithful of the land, that they may dwell
with me: he that walketh in a perfect way, he shall serve me (Ps. 101:6).

But whoso looketh into the perfect law of liberty, and continueth
therein, he being not a forgetful hearer, but a doer of the work, this man
shall be blessed in his deed (James 1:25).

This line of reasoning does not impress Mr. Myers greatly. What
he seems to have in mind is a specific kind of utopianism: Christian
Reconstruction, Perhaps not. He may be thinking of something that
just sounds a lot like Christian Reconstruction. But note carefully

39. Myers, “Biblical Obedience and Political Thought,” Bible, Politics and
Democracy, p. 28.

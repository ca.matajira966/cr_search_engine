760 POLITICAL POLYTHEISM

prophets, 41
proof-texting, 28
pro-nomianism, 35-43
prosperity, 31, 58, 140
providence, 44-45, 340-41
Providence (Rhode Island), 313, 538
providences (Newtonian), 351
providentialism, 426
psychology, 4
public evil, 294
public opinion, 419, 503, 648
public schools, 627
“Publius,” 327
Puritan Revolution, 19
Puritans
ambiguous?, 243
capital sanctions, 643
city on a hill, 244
Christian society, 261
cosmic personalism, 347
covenant theology, 249, 300
discarded legacy, 516-17
education, 327-28
English, 647
experience, 368
halfway covenant, 107-9
holy commonwealth, 249, 657
internationalism, 244-45, 529
Indians, 238
limitation on government, 399
losers, 312-13
Madison vs. 531
Marsden on, 235-38
Miller on, 285-87
new Israel, 243-45
Niebuhr vs., 668
Old Testament, 236-37
Old Testament law, 287
paradoxical?, 239
political theory, 255-56, 645
pride, 244
public education, 645
representation, 249
sanctions, 244, 255
strategic error, 647
theocrats, 242-45, 249
top-down, 647

transcendence-immanence, 347
voting, 249

pyramid, 343

Pythagoras, 336

Pythagoreans, 476

quantum physics, x, xxii, 45, 348

Rakove, Jack, 418
Ramm, Bernard, 284n
Ramsey, David, 312, 325
Randolph, Edmund, 387-88, 414, 431
randomness, 44, 142, 144, 148 (see also
chance, chaos)
Rapture, 139
“cat in Philadelphia” (Henry), 415
ratification (Constitution)
50 - 50 split, 456
assumptions by voters, 399
broken covenant, 386, 492
bypassing Congress, 419
Christians, 408-09, 439
confusion, 399
Congress vs., 388
conventions, 492, 499
discontinuity, 419
enthusiastic Christians, 561
faction, 499-500
Henry as primary opponent, 416
legitimacy, 386, 419, 507
new covenant, 386
new god, 492
People, 492, 495
plebiscite, 419, 486
public opinion, 419
renewal, 419, 494
revolution, 485-86
Washington’s role, 421
why unanimous?, 500
Reagan, Ronald, 175, 199, 288-89
Reconstruction, 385n, 512 (see also
Christian Reconstruction)
Red Riding Hood, 124
Red Sea, xxiv
Reformed Presbyterian Church,
Evangelical Synod, 167
Reformed Presbyterian Church of
North America, 711-16

premillennial, 166
presuppositionalism, 171-72, 175,
218
relativism, 170, 185-87, 191
roof analogy, 171
Rushdoony &, 175, 196-97
Rutherford, 195-97
schizophrenia, 175, 218-19
silence, 181
sound theory, 212
spirit of world, 301
successors, 173-74
tension, 183-85
theocracy, 173-74, 180
transitional figure, 175
truth, 171-72, 186
Wells vs., 187-91
Westminster Confession, 168,
470, 172
wilderness, 165
Van Til’s critique, 170-71
Schaeffer, Edith, 169
Schaeffer, Franky, 176-79, 253n
Schaff, Philip, 546, 551-52
schizophrenia
intellectual, xv, 216-19, 250, 263,
293-94
judicial, 91
political, 110-11
providential, 353
theological, 196
Schrotenboer, Paul, 123
scientific revolution, 104-5
Scopes trial, 193n, 211, 216
Scott, Otto, 675-76
screening, 190, 193, 379, 387, 524
seal (see Great Seal)
sealed tomb, 295
secrecy
Convention, 412-15, 417-18
lodges, 564
Masonic, 475
sects, 446-47
secularization, 367, 372
self-government, 6, 67, 532, 534,
535, 585, 590, 649

Index 765

Senate
atheists, 517
legislatures elected, 387
pagan?, 390-91
Rome, 79
separation of powers, 381-82
separatists, 245
sex education, 201-3
Shaftesbury, Earl of, 352
Shays, Daniel, 431, 455
Shimei, 275, 299
Sider, Ron, 213
silence of the “transcendent,” 102-3
Silver, Morris, 416, 553
Simmel, Georg, 475n
Singer, C, Gregg, 405, 677
sinking ship, 580
Skillen, James, 124-25
slavery, 264, 319n, 602
Slosser, Bob, 21in
Smith, Adam, 453-54, 459-60
Smith, Gary Scott, xv-xviii, xxi
Smith, Joseph, 534-35
Smith, Page, 434
snake (Athens), 77
smoking, 203-4
smorgasbord religion, 638
social action, 164, 176
social change, 577
Social Darwinism, 99
social fabric, 563
Social Gospel, 289
social order, 100, 109, 149, 464, 479
social theary
abandonment of, 5
anti-covenantal, 34
Christian Reconstruction, ix, 32,
130
Christianity’s compromises, xv,
253, 461
Masonic, 468-69
no contract, 28
pay as you go, 29
Van Til vs., 130, 133
Socinianism, 345
soldiers, 46

Solomon, 26

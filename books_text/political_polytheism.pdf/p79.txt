What Is Covenant Law? 55

the thrust of his argument: that without assurance that the program
of Christian Reconstruction (or whatever) is possible in the future, it
can become self-defeating to attempt a significant break with the
status quo. If you think I regard the following paragraph as a practical
reason for linking theonomy with postmillennialism, you are correct:

The other variety of utopianism we must avoid might be called the lure
of the “third way.” By this I mean to challenge not the motives but the con-
dusions of all those who set out determined not to embrace any existing sol-
ution but to invent an entirely distinct “biblica?” option. Such an exercise
may be useful for academic purposes, and it is possible that it may some-
times be fruitful to develop a vanguard with an entirely new vision. Fur-
ther, it may be the case that all existing parties or programs are so tainted
with evil that they must be rejected. But sometimes we must take sides with
the lesser of two evils, especially if we have reason to believe that our
tenaciously holding out for the éertium quid will ensure that the greater of two
evils will carry the day and do much greater damage. Political realism re-
quires the recognition that human sin may rule out any hope that our ideal
program can ever be enacted or may ensure that if it is enacted, it will have
no chance of achieving the desired ends. If the best is impossible to achieve,
we should know how we can promote the better,

In short, theonomy is utopian unless postmillennialism is true.
This observation leads me to Mr. Harrington’s lament.

Harrington’s Lament

H. B. Harrington, a supporter of a Constitutional amendment to
name Jesus Christ as the sovereign Lord of the United States, has
protested against the “Iyler Reconstructionist” fusion of theonomy
and postmillennialism. He calls it “frustrating” and a “constant irri-
tant.” What he means is that it is frustrating and irritating to those,
like himself, who have a slightly different agenda for Church reform
and national reform, and who have a more traditional constituency.
The question is: Why should someone who does not share our theo-
logical agenda complain if our eschatology interferes with his agenda,
especially since he is using our materials and arguments to promote
his agenda? Why not just rest content with the fact that each side can
pursue its separate though closely related agendas, cooperating on
an ad hoc basis whenever possible? Christians already do this in the
battle against abortion. Why not also in the battle to impose a na-

40. Idem.

No one could become a citizen at Athens if he was a citizen in
another city; for it was a religious impossibility to be at the same
time a member of two cities, as it also was to be a member of two
families. One could not have two religions at the same time.

The participation in the worship carried with it the possession of
rights. As the citizen might assist in the sacrifice which preceded the
assembly, he could also vote at the assembly. As he could perform
the sacrifices in the name of the city, he might be a prytane and an
archon. Having the religion of the city, he might claim rights under
its laws, and perform all the ceremonies of legal procedure.

The stranger, on the contrary, having no part in the religion, had
none in the law. If he entered the sacred enclosure which the priests
had traced for the assembly, he was punished with death. The laws
of the city did not exist for him. If he had committed a crime, he was
treated as a slave, and punished without process of law, the city
owing him no legal protection. When men arrived at that stage that
they felt the need of having laws for the stranger, it was necessary to
establish an exceptional tribunal. . . .

Neither at Rome nor at Athens could a foreigner be a proprietor.
He could not marry; or, if he married, his marriage was not recog-
nized, and his children were reputed illegitimate. He could not make
a contract with a citizen; at any rate, the law did not recognize such
a contract as valid, At first he could take no part in commerce. The
Roman law forbade him to inherit from a citizen, and even forbade
a citizen to inherit from him. They pushed this principle so far, that
if a foreigner obtained the rights of a citizen without his son, born
before this event, obtaining the same favor, the son became a for-
eigner in regard to his father, and could not inherit from him, The
distinction between citizen and foreigner was stronger than the natu-
ral tie between father and son. .

Fustel de Coulanges (1864)*

*Pustel, The Ancient City: A Study on the Religion, Laws, and Institutions of Greece and
Rome (Garden City, New York: Doubleday Anchor [1864] 1955), pp. 196-97.

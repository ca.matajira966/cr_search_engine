no Productive Christians in an Age of Guilt-Maniputators

Biblical Alternatives to Foreign Aid

The Christian cannot support the unbiblical practices of gov-
ernment aid. But this does not. mean we have nowhere to turn.
There is much that we can do. Firsi, we can support Christian
missionary activity, to bring the gospel to the poor and build bib-
lical principles into the lives of converts, This is the basic and in-
dispensable requirement; without regeneration, true cultural
transformation is ultimately impossible. As these peoples conform
their lives to biblical standards, their societies will experience the
economic growth promised in God’s law. Second, we should op-
pose the really unjust patterns of both trade and aid in our world.
We should abolish trade barriers of ali sorts, and promote biblical
law in all areas of government. Third, we may invest in those
economies which abide by God’s law enough to restrain from “na-
tionalizing” our properties. Private capital, for many reasons, is
more productive than foreign aid. It goes to producers, not gov-
ernments, and thus does not concentrate political power; it is
more personal and local; it is more likely to be handled respon-
sibly, in terms of profitable production rather than prestigious
superfluity; and it is related to specific market conditions, rather
than the political goals of bureaucrats. Fourth, we can give to char-
itable causes for the relief of specific needs. As we saw in Chapter
Two, biblical charity works toward responsibility in the recipient.
And, being individual and uncoerced, it produces responsibility
in the giver as well. Foreign aid is “charity” at gunpoint: our state
officials force us to pay for it, through either taxes or inflation. It
builds no moral character in either the “givers” or the receivers.
Biblical charity is personal, prudent, and responsible. Because it
is morally sound, it is economically sound as well. It genuinely
enables us to “bear one another’s burdens,” and at the same time
teaches the weak to be strong, so that “each one shall bear his own
Toad.”

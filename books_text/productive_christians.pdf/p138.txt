124 Productive Christians in an Age of Guilt-Manipulators

readers may have missed the logical link which ties high profits to
injustice. So for those of us who can’t quite plumb the depths, he
adds a helpful note:

The reader without a degree in economics probably wishes international
economics were less complex or that faithful discipleship in our time had
less to do with such a complicated subject.$

That was the whole problem, dear reader. You don’t have a
degree in economics. You didn’t probe through the excretions of
Keynes and Galbraith. That’s why you don’t understand. So let
Father Sider take you by the hand, and he will lead you skillfully
through the maze of economic theory.

He’s got some competition, though. Solomon said you could
study the Book of Proverbs. . . .

To know wisdom and instruction,

To discern the sayings of understanding,

To receive instruction in wise behavior,

Righteousness, justice and equity;

To give prudence to the naive. . . .

The fear of the Loro is the beginning of knowledge. (Proverbs
1:2-7)

According to Solomon, we can come to a capable understand-
ing of what is fair and just simply by submitting to God’s revela-
tion and studying the biblical implications of God’s law. Of course
they didn’t grant degrees in economics in those days, but even if
they had, the Psalmist exclaimed:

T have more insight than all my teachers,
For Thy testimonies are my meditation. (Psalm 119:99)

The Bible is the standard for every aspect of life. It tells us a
great deal about economics, And it tells us that God’s law alone is
the standard of right and wrong, of justice and injustice. We
might therefore expect that the Bible would have much to say
about how to discern exactly when a profit rises above a “fair”

3. Ibid. [p. 155].

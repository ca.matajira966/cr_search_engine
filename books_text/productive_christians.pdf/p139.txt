The Law and the Projits 125

level. But it does not. From this fact there are two possible conclu-
sions. Either God was waiting for Marx or Sider to come along
and do it for Him, or there is no such thing as an unjust profit. For the
remainder of this chapter, I am assuming that the latter option is
correct,+

The issue of profits centers on the problem of production. How
does the entrepreneur know where to channel investments? Or, to
put it another way, How can the wants of consumers be satisfied?
The entrepreneur attempts to anticipate and meet constantly
changing future consumer demands by directing the various fac~
tors of production in such a way as to make them provide what the
people want in the most efficient and profitable way. If he fails to
forecast the market correctly, he will suffer losses. If his decision is
correct, he will make a profit. The profit is the tangible sign of success in
serving consumer wants. A large profit means the entrepreneur has
been very successful. He has not wasted resources, which are now
released to the market to serve other consumer preferences.

It is commonly assumed that profits are arbitrary ~ that the en-
trepreneur says, “Well, let’s see. Costs of production amounted to
such-and-such; I will add on a profit of X percent and make a lot
of money.” But the entrepreneur cannot control demand. If the
demand for his product is low, he will suffer losses. The en-
trepreneur does not “set” a profit. He does not know the future.
The price (and hence the profit) will be determined by consumer
demand in relation to the supply. If many consumers want the
product, they will bid up the price in order to get the product. The
profit comes from consumers who outbid other consumers in com-
petition for it. The fact that profits are high means that many con~
sumers are bidding, and this means that the entrepreneur has cor-
rectly discerned their needs. He makes a profit in direct relation-
ship to how well he has satisfied their desires. Remember this law:
consumers compete against consumers, while sellers compete

4. For a very helpful explanation of the nature and function of profite, see
Ludwig von Mises, Planning for Freedom (South Holland, IL: Libertarian Press,
fourth ed., 1980), pp. 108i, See also Mises, Human Action: A Treatise on Economics
(Chicago: Henry Regnery Co., third revised ed., 1966), pp. 289-303,

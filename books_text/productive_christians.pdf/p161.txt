Cultural Bone Rot 147

States has “an unjust division of the earth’s food and resources,””2?
and our right to use them is superseded by the “human” rights of
the rest of the world.?5 The poor nations suffer because we use
more fertilizer,2* eat more fish® and beef,?° and generally con-
sume more than other people do.2? White schoolchildren are
guilty of the “terrible sin” of getting a better education and thus
better job opportunities, because their parents pulled them out of
an inner-city school,® We are all guilty of Mexico’s oppression of
its farmers;?9 we all are guilty of the profits made by U.S. com-
panies in other nations—profits which constitute “foreign aid”
from poor people to us.3° Profits from these countries are “unjustly
high.”3! “Every North American benefits from these structural
injustices... . you participate. in unjust structures which con-
tribute directly to the hunger of a billion unhappy neighbors.”3?
“The proper conclusion is that injustice has become embedded in
some of our fundamental. economic institutions’33—so much so
that it is “tmpossible to live in North America and not be involved in unjust
social siructures.”3* If you’re having trouble bearing the weight of
all that guilt by yourself, Sider offers some comfort, hastening to
make it clear that he does not “want to suggest that 214 million
Americans bear sole responsibility for all hunger, starvation and
injustice in today’s world.” No? Of course not: “Adi countries of
the rich Northern Hemisphere are directly involved.”?5 At least
we're not alone. And, generously, Sider includes himself in all

22, Ibid., p. 18,
23. Ibid., p. 210 [p. 194].

 

24. 151.

25. , 136.

26. , 1586, [pp. 1488].

27. , 153, 162 [ef pp. 1448].

28. . 1328. [pp. 176].

29, . 159 [p. 148].

30. . 161.

31. . 162.

32. . 155]. Italics added.

33. . 155).

34. . 148 [cf. p. 137]. Italics added.
35. . 139 [cf p. 124]. Italics added.

166 Productive Christians in an Age of Guilt-Manipulators

description of the “body life” of the Christian assembly in
Romans 12, he tells us that we should be “contributing to the
needs of the saints, practicing hospitality” (v. 13). The word con-
tributing there is the verb form of koinonia; in other words, if we are
to have genuine fellowship in the body of believers, we must not
be content with coffee and donuts on Sunday morning. We must
be “fellowshipping to the needs of the saints,” truly seeking to
help them in their difficulties.

The primary symbol of this sharing ministry among believers is
the communion service, which in modern churches has been
almost entirely stripped of its meaning. Far from being the “pre-
tend” meal of congregations today, the communion service of the
New Testament was the continuation and fulfillment of the Old
Testament festivals (see Matthew 26:19-30; I Corinthians 5:6-8).
It was a “love feast,” a common meal in which the Christians
shared their food with one another. The participants did not get a
little piece of cracker and a thimbleful of grape juice. They sat
down and ate a meal. The danger Paul rebuked at Corinth was
that the believers there were failing to discern the Lord’s body (I
Corinthians 11:29), This doesn’t mean they failed to comprehend
some mystical dogma regarding the precise relationship of
Christ's physical body to the bread and wine. Paul was not rebuk-
ing a lack of intellectual understanding, but rather a lack of moral
discernment. The Corinthians had been behaving sinfully, in-
dulging themselves in gluttony and drunkenness, refusing to
share food with one another (v. 21). The “body” they failed to
discern was Christ’s congregational body, their fellow believers. They
came to have communion, and did not commune together; they
did not share, yet they called it a “sharing service.” Paul accused
them of not eating “the Lord’s supper,” that each one ate “his own
supper” (v. 20-21); with the result that “one is hungry and
another drunk.” And this is a problem with churches today —even
though we've gotten rid of the alcohol in most cases (incidentally,
we're supposed to share the wine, not abolish it). In many modern
“communion” services, the participants receive a token meal,
close their eyes and chew away, thinking spiritual, inward-type

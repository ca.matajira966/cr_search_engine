The Goal of Equality 71

Now, contrast that with his statement that economic sharing
among Christians—the means to his goal of centralized power —is to
be “voluntary,” and that “Legalism is not the answer.” Not,
that is, until it is /egislated. Then, armed officers will come to col-
lect our donations, and the texts exegeted as the basis for it will
come from the Federal Register. If you think Sider was simply
outlining a plan for a wonderful paradise of voluntary sharing
among Christians, you’ve been conned. He has no intention of
stopping with voluntarism: He wants to manipulate the church
with envy and guilt, to provide a model for public policy, and
later, when his proposals are enacted into laws, voluntarism be
damned. You wii share, whether you like it or not. Again he says:

Certainly we should work politically to demand costly concessions from
Washington in international forums working to reshape the Interna-
tional Monetary Fund, as well as new policy in trade negotiations on
tariffs, commodity agreements and the like. Certainly we must ask
whether far more sweeping structural changes are necessary. However, our
attempt to restructure secular society will possess integrity only if our
personal life-styles .  . demonstrate that wé-are already daring to live
what we ask Washington to legislate... . If even one-quarter of the Chris-
tians in the northern hemisphere had the courage to live the . . . vision
of economic equality, the governments of our dangerously divided global
village might also be persuaded to legislate the sweeping changes needed.*

What sort of “sweeping changes” in public policy would Sider
like to see? He suggests for his “model” the church, that it should
be “the norm, rather than the exception, for Christians to. . .
evaluate each other’s income-tax returns and family budgets,
discuss major purchases, and gently nudge each other toward
lifestyles more in keeping with their worship of a God who sides
with the poor.” Thus, using this as a model, a government
officer can sit in on your next family budget-planning session and
discuss major purchases with you. He might have to bring his

13. Sider, Rich Christians, p. 107 [pp. 95f.].
14. Sider, “Sharing The Wealth,” pp. 564f. Italics added.
15. Ibid., p. 564.

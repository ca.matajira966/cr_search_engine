172 Productive Christians in an Age of Guilt-Manipulators

billy club along in case you need a “gentle nudge.”

Actually, Ronald Sider has nothing but contempt for private
charity. He would rather have state-enforced “institutional
change.” Here are two of his reasons:

First, institutional change is often more efective. . . . The cup of cold water
that we give in Christ’s name is often more effective if it is given through the
public health measures of preventive medicine or economic planning.

Second, institutional change is often morally better. Personal charity and
philanthropy still. permit the rich donor to feel superior. And it makes
the recipient feel inferior and dependent. Institutional changes, on the
other hand, give the oppressed rights and power. '®

Now, if only the Lord had thought of that. But it’s too late.
That morally inferior personal charity is encoded into biblical
law, and we're stuck with it until heaven and earth pass away.
Darn! However, at least we have learned something significant
about Sider’s views. The power of the state, restricted by biblical
law, should be aggrandized. God’s law is less effective. State in-
tervention, forbidden by Scripture, is morally better than the
eternal word of God. Personal charity—which we all thought was
the whole point of his book —is really just a model for the omnipotent
state, and the model, morally substandard as it is, will probably be
scrapped once we get the state programs going full blast. There
will be less money for charity after taxes.

The professed goal of economic equality has long been used by
tyrants as a cover for the most brutal kinds of intervention. It is a
fetish, held up before the poor to excite envy, dangled in front of
the rich to induce guilt. Revolution and statist oppression are
facilitated thereby: the envious will rebel, and the guilty will have
been rendered impotent. Sider does not want the biblical idea of
equality before the law—which assumes that there are distinctions
among men, and guarantees justice for all, and freedom to fulfill
one’s calling under God. Sider instead wants a state-enforced

16. Sider, “Ambulance Drivers or Tunnel Builders,” (Philadelphia:
Evangelicals for Social Action, n.d.), p. 4.

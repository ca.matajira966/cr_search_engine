Introduction 5

Yet what they are preaching is the Revolution. It is not pre-
sented so baldly, of course; most Christians would not be so easily
seduced if it were called by its true name. It is therefore altered
into revolution by installments. The results are nevertheless the same.
Expropriation of the wealthy is theft under any name. In every
revolution of the past, words were revolutionized in meaning, and
ordinary people were moved to extraordinary acts, without realiz-
ing that the impressive words had been redefined: justice meant
injustice; freedom meant coercion; humanity ‘meant savagery;
non-violence meant war without end.

The mark of a Christian movement is its willingness to submit
to the demands of Scripture. Not, mind you, merely to “prin-
ciples” abstracted from their context and loaded with new con-
tent; but rather the actual, concrete, explicit statements of God's
word. “You shall not steal,” for instance: that must not be
relativized on the mere excuse that the thief has no bread. It must
not be violated just because someone has found a “principle” that
God would like everyone to have bread, It must not be trans-
gressed with the spurious rationale that the thief should have been
given the bread in the first place. If you want principles, here’s
one: theft ts theft. Easy to remember, uncomplicated and biblical.
The “Christian” who advocates theft in the name of social justice
is in truth calling for the Revolution, whether or not he fully
realizes what he is doing. And we must not allow the lovely
sounds of the words to disguise their meaning. The great Dutch
Christian historian of revolution, Groen van Prinsterer, pointed
out that “wherever the Revolution has been at work it has become
apparent that it considers law to be mere convention, a product of
the human will.”6 We shall see that this is the mark of the “Chris-
tian socialist” movement as well—that its only real principle is the
principle of unbelief:

The principle of unbelief—the sovereignty of reason and the sovereignty
of the people— must end, while proclaiming Liberty, either in radicalism

6. G. Groen van Prinsterer, Unbelief and Revolution: Lecture XI (Amsterdam:
The Groen van Prinsterer Fund, 1973), p. 10.

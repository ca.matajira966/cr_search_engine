13
STATISM

To virtually every problem raised in his writings, Ronald
Sider’s answer is state intervention. Even where he appeals to the
“private sector,” his motive is to have the uncoerced actions of in-
dividuals serve as “models” for coercive actions of government.
As we have seen, he regards the biblical method of charity to be
morally inferior to the biblically forbidden methods of statist ex-
propriation and redistribution. Are prices “unjust”? Have the
state lower them—or raise them, judging solely on the basis of
economic class, siding with the poor, providing for their needs,
creating more and more dependence upon government. Do we
need jobs? Let the state provide them. Are profits too high? Let
the state slice them down to size. Do we eat too much meat? Let
us have a national food policy. What about health care? That is
the business of the state. Is there inequality in the world? Let us
ask our Father in Washington to make us all equal in economics.
Of course, when other inequalities arise—not differences, mind
you, but inequalities—the state will have to level them too. Until—

In the not very distant future, after the Third World War, Justice had
made great strides. Legal Justice, Economic Justice, Social Justice, and
many other forms of justice, of which we do not even know the names,
had been attained; but there still remained spheres of human relation-
ship and activity in which Justice did-not reign.!

So begins L. P. Hartley’s futurist novel, Facial Justice, in which

1. L. P. Hartley, Facial Justice (Garden City, NY: Doubleday & Company,
Inc., 1960), p. 7; see Schoeck, pp. 1498.

177

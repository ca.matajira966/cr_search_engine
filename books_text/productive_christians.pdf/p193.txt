Statism 179

with sin. I may have sin in my heart in refusing to do business
with red-haired people, but it is not a crime legitimately punisha-
ble by the state. God will deal with me. Again, if my prices are
“too high,” the search for bargains will induce people to patronize
the business of a competitor whose prices more closely are in line
with the reality of demand. God’s imposed scarcity (Genesis
3:17-19) induces men to become less wasteful. The free market is
free only with respect to state intervention. It is never free from
the providence of God. And this is as good a point as any to con-
sider Sider’s objections to the free roarket and its alleged inven-
tors, the 18th-century economists:

During the eighteenth century, Western society decided that the scien-
tific method would shape our relationship to reality. Since only quanti-
tative criteria of truth and value were acceptable, more intangible values
such as community, trust and friendship became less important. Unlike
friendship and justice, GNP can be measured. The result is our com-
petitive, growth economy where winning and economic success (and
they are usually the same) are all-important.3

From the perspective of biblical revelation, property owners are not free
to seek their own profit without regard for the needs of their neighbor.
Such an outlook derives from the secular laissez-faire economics of the
deist Adam Smith, not from Scripture.

Smith published a book in 1776 which has profoundly shaped Western
society in the last two centuries. (Since the Keynesian revolution, of
course, Smith’s ideas have shaped Western societies less than previously,
but his fundamental outlook, albeit in somewhat revised form, still pro-
vides the basic ideological framework for many North Americans.)
Smith argues that an invisible hand would guarantee the good of ail if
each person would pursue his or her own economic self-interest in the
context of a competitive society. Supply and demand for goods and ser-
vices must be the sole determinant of prices and wages. If the law of sup-
ply and demand reigns and if all seek their own advantage within a com-
petitive nonmonopolistic economy, the good of society will be served.
Owners of land and capital therefore have not only the right but alsa the
obligation to seek as much profit as possible.

3. Sider, Rich Christians, p. 49 [p. 41].

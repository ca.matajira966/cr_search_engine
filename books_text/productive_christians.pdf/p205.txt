Statism 191

men as men, created in the irnage of God. Socialism is a power
theory, We must not look merely at Ronald Sider’s alleged
ideals—that the poor should have enough to eat, that we all
should be healthy, that we should not covetously spend our
money on every trifle we see advertised, and so on—but we must
look closely at how he intends to accomplish these things. He
wants to see the state empowered to enforce his goals in every area
of life. Let us never forget that this means the use of weapons to
ensure compliance on the part of the people. I am not implying, of
course, that there will be a shootout every time the state moves in
on an offender, The threat, however, always exists. But the only
ultimate reason for asking the state to enforce something is that
the state possesses the legal right of coercion—and practically
speaking, as Chairman Mao said, political power grows out of a
gun barrel. Nor am I implying that the state’s use of violence is
always wrong. But that power of coercion must be used only
where God has given the state the authority to do so. Any other
use of it is abuse. It is the exaltation of the state into the place of
God.

It is conceivable that Sider has not thought of the implications
of his demands. He may be only very ignorant, and not as evil as
Thave suggested. He has, indeed, called for a “nonviolent revolu-
tion,” and seems to deplore the use of physical force.!? We can
hope that he is sincere, but that does not change the fact that his
policies require all sorts of violent intrusions upon liberty. We have
already noted that comprehensive statist planning means nothing
other than the concentration of power. And this concentration of
power is forbidden by the law of God. It is therefore doomed to
failure. It cannot increase resources, capital, or productivity. It
cannot ultimately do anything for the poor, The only thing
statism will ever produce is the judgment of a jealous God upon
its presumption. Sider’s appeal for state controls will result only in
tyranny and destruction.

19. Sider, “A Call for Evangelical Nonviolence”; see also “To See the Cross, To
Find the Tomb, To Change the World,” in theOtheSide (February, 197), pp. 168.

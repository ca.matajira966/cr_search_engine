194 Productive Christians in an Age of Guilt-Manipulators

Meredith Kline has rightly characterized the prophets as prose-
cuting attorneys, messengers from God sent to remind God’s
vassals of their covenantal obligations, and warning the people of
the consequences of disobedience. “The mission of the Old Testa-
ment prophets, those messengers of Yahweh to enforce the cove-
nant mediated to Israel through Moses, is surely to be understood
within the judicial framework of the covenant lawsuit.”®

The prophets therefore took their stand firmly in terms of
biblical law. They did not go beyond it, but simply applied it to
the problems of the society, and commanded the people to repent
of their sins and return to the way of obedience under the law of
God. Everything they said was in complete accordance with the
law, This means that, while they were very concerned over the
oppression of the poor—incidentally, that was not their only
concern--they never advocated statist policies to remedy the
situation, You will never read of an Old Testament prophet call-
ing for rent controls, minimum wage laws, or guaranteed jobs.
They never demanded that the government print more money or
expand credit. They did not plead for foreign aid, national health
care, or restriction of profits, They did not try to institutionalize
envy or legalize theft. Instead, they worked to reestablish the law
of God in every area. This is the fundamental difference between
the prophets and the socialists, While they both speak about the
poor, their content, methodology and goals are completely at
odds.

This could be amply demonstrated by a thorough survey of all
the prophetic literature, but that would itself constitute an. entire
book. So we will narrow our field of inquiry to one prophet, who,
above all others, seems to be a socialist favorite. This is the
prophet Amos. If there was a single man in all of Scripture who
spoke out against the sins of oppressions and economic injustice,
it was Amos. For this reason, socialists seize upon his little book to
justify statist remedies of every kind~—yet, as we shall see, with

2. Meredith G. Kline, By Oath Consigned: A Reinterpretation of the Covenant Signs
of Circumcision and Baptism (Grand Rapids: William B. Eerdmans Publishing Co.,
1968), p. 52.

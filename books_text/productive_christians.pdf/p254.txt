240 Productive Christians in an Age of Guilt-Manipulators

as we ourselves are discipled to God’s law. Only disciples can
make disciples. Hybrids cannot reproduce. But as we ourselves
submit to Christ, the nations will also.

The subduing of the nations to the discipleship of Christ will
take place as we are faithful, That is the crucial issue, and that is
why Christians have lost ground over the recent past. It is not due
to the advance of paganism (remember, gates can’t advance). It is
due only to the refreat of Christians. “That there is still a remnant
of paganism in this world is chiefly the fault of the Church. The
Word of God is just as powerful in this generation as it was during
the early history of the Church. The power of the Gospel is just as
strong in this century as in the days of the Reformation, These
enemies could be completely vanquished if the Christians of this
day and age were as vigorous, as bold, as earnest, as prayerful,
and as faithful as Christians were in the first several centuries and
in the time of the Reformation.”*

Thus we must work diligently and patiently for the kingdom. It
has come and it is still coming. In the meantime, we are not to
envy even the wicked who are in power. They will fall, and the
meek will inherit the earth (Psalm 37). The gradual growth of
Christ’s kingdom was stated beautifully by Benjamin Warfield:
“Through all the years one increasing purpose runs, one increasing
purpose: the kingdoms of the earth become ever more and more
the kingdom of our God and His Christ. The process may be
slow; the progress may appear to our impatient eyes to lag. But it
is God who is building: and under His hands the structure rises as
steadily as it does slowly, and in due time the capstone shall be set
into its place, and to our astonished eyes shall be revealed nothing
Jess than a saved world.”$

The Blessings of National Obedience

We have noted the cultural effects of obedience so often in our
study that it would be superfluous to recount them all here, But

4. Kik, p. 250.
5, Benjamin Breckenridge Warfield, Biblical and Theological Studies (Philadelphia:
The Presbyterian and Reformed Publishing Company, 1968), pp. 518f,

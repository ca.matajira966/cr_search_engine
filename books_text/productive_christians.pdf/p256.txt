242 Productive Christians in an Age of Guili-Manipulators

And never again will they train for war.

And each of them will sit under his vine

And under his fig tree,

With no one to make them afraid,

For the mouth of the Lorp of hosts has spoken.

As obedience to biblical law spreads, capital will be shifted from.
warfare to more productive endeavors; and as productivity rises,
we find each man on his own property, sitting under his vine and
fig tree. This is the direction of history. Men will become more
obedient, hence more responsible, hence more productive, hence
more capitalized.... The Bible shows that poverty will be
abolished through godly productivity and rising real wealth. The
biblical answer is not, as the saying goes, to redistribute the pie,
but to make a bigger pie.

It can happen. Moreover, it will happen. Ultimately, poverty
has no future, except for the ungodly who are dispossessed.
Ezekiel’s vision of the kingdom’s growth throughout the world
(symbolized by the gradually rising stream flowing from the tem-
ple, Ezekiel 47:1-12} showed the blessings of God affecting virtu-
ally everything in life, bringing health and prosperity to the
world. Even the salty Dead Sea, symbol of God’s curse upon
Sodom and Gomorrah, will become fresh—but some few places
will be “left for salt” (v. 8-11), still under the judgment of God.
The Bible looks forward to the time when none of God’s people
will be poor, when by God’s gracious providence the land will be
distributed to all those who are obedient.

This will never come about through ungodly acts of expropria-
tion, It will never happen as long as the church continues to heed
unbiblical philosophies which seek to turn her away from obe-
dience to God’s law. Institutional poverty will never be cured by
socialism and statism. Ungodliness can only extend the Curse.
The conquest of poverty is not really based on the issue of poverty
at all. It is an issue of obedience, of godliness, of submission to the
Lord Christ at all points.

Our nation has a Christian heritage. While they had their
flaws, the Puritans and the leaders of the young United States

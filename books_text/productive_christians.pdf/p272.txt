258 Productive Christians in an Age of Guilt-Manipulators

Those who have noted the discrepancies, however, would naturally
suppose that Sider really has changed his mind on this point. The
professor seems to have mellowed considerably, He no longer
claims that the Jubilee should be implemented today, or that we
should communize all income-producing property to fulfill its re-
quirements .. . right? Yet, in the new edition of his book, in a
later section that has nothing to do with the Jubilee, Professor
Sider informs us that—of alt things~ the Jubilee Law is the an-
swer to the problems of the Third World! All the.land should be
communized, you see, because

the jubilee calis for redistribution of society’s pool of productive
assets. (p. 147)

What are we to think of all this? The professor erased that kind of
language from his section specifically dealing with the Jubilee, but
then reinserted it into a passage on the Third World. So—has he
changed his views or hasn’t he? If not, why did he delete his origi-
nal statement about the Jubilee? If God’s Jubilee Law calls for the
“redistribution of society's pool of productive assets,” shouldn't
Christians still “pool all their stocks, bonds, and income produc-
ing property and businesses and redistribute them equally”? And
yet, on the authority of the professor himself, we can say with
complete confidence that the Jubilee should “certainly not” be ap-
plied in this age . . . except for when it should be applied. One
thing’s for sure: If Dr. Sider deliberately set out to confuse his
readers, it worked.

Another strange new feature of the revised version shows up
on pages 171-72, where the professor lays down “six criteria” to
enable us to determine the kind of lifestyle we should pursue. As
Dr. Sider hastens to point out, he offers these “as suggestions, not
as norms or laws” (his emphasis). In his “suggestions,” however,
he uses these words:

need ought should should not wrong sin

These are not norms or laws, mind you, Far be it from Dr. Sider to
presume to legislate our conduct. No, he is merely suggesting

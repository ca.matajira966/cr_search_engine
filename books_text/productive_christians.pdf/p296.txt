282, Productive Christians in an Age of Guili-Manipulators

“Those fighting for free enterprise and free competition do not
defend the interests of those rich today. They want a free hand left
to unknown men who will be the entrepreneurs of tomorrow and
whose ingenuity will make the life of coming generations more
agreeable. They want the way left open to further economic im-
provements. They are the spokesmen of material progress.”75

In marked contrast to this is the position of Dr. Sider. By his
opposition to the freedom and responsibility of individuals to ad-
minister their God-given wealth, by advocating a socialistic sys-
tem which requires totalitarian controls by a ruthless bureau-
cracy, he has championed the cause of elite planners and dictators
against the masses. By constantly fomenting envy and resentment
against producers, against the creators of wealth, he is opposing a
progressive, rising standard of living for the peoples of the world.
To the extent that Ronald Sider’s policies are followed, the evil
rich will get richer, by theft and extortion; and the poor will get
poorer, The earth will become one grand Soviet empire—the real
payload of his alleged compassion for the poor.

John P. Roche offers some good advice for those who must
deal with a Marxist-Leninist demagogue: “Don't listen to what he
says: watch his hands.”76

From Russia, With Love

To lend some credibility to his allegedly “neutral” position on
the free market vs. the controlled economy, Dr. Sider’s revised
version manages to make a few critical statements about the
Soviets—not, to be sure, without condemning capitalism in the
same breath, but at least he does say that “we should also vigor-
ously condemn the repression, totalitarianism and violation of
human rights perpetuated by the Soviet Union in places like
Afghanistan and Poland” (p. 196; cf. p. 81). I begin to wonder if
Sider has heard about that slight matter of Soviet oppression “in

75, Ibid., pp. 826.

76. John P. Roche, The History and Impact of Marxist-Leninist Organizational
Theory: “Useful Idiots,” ‘Innocents’ Clubs,” and “Transmission Belts” (Washington: Ine
stitute for Foreign Policy Analysis, 1984), p. 7.

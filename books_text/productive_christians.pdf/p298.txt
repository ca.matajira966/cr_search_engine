284 Productive Christians in an Age of Guilt-Manipulators

result in a Soviet invasion” (p. 274). Sider and Taylor both attended
Yale, so it didn’t take them long to figure that one out. Of course,
they don’t intend to take this lying down. They've come up with
what they call a “Civilian-Based Defense.” (You will think I'm
making up the following quotation, but I'm not. With Sider and
Taylor writing great material like this, who needs satire?) It works
like this: as the Soviet troops land, Sider and Taylor organize a
“nonviolent blitzkrieg,” in which thousands of American citizens
would assemble to meet the enemy’s jumbo jets as they land,
Here’s the scenario, straight from Sider and Taylor:

“The landing would be peaceful. No American artillery would
fire; no jets would strafe. Instead of American soldiers crouching
behind tanks and pointing guns at them, the invaders would see
tens of thousands of unarmed people carrying signs with messages
in the invader’s language: Go Home! We Won't Harm You; Don’t
Shoot—We Are Your Brothers and Sisters; Your Life Is Precious;
You Are a Child of God.8?

“Like the Czechs, Hungarians and East Germans during the
Russian invasion of those countries, Americans would climb up
on tanks and try to talk to soldiers: Why have you come? Why are
you invading a peaceful nation that is not threatening you?” Loud-
speakers would explain that the troops are welcome as tourists but
will be opposed as invaders. . . . If members of the crowd were
not able to keep discipline and started to threaten the soldiers,
special U.S. Peace-keeping Teams would move in nonviolently to
restrain the persons who were losing control.”

Moral Majority’s Cal Thomas remarked to me that such a
“defense” strategy just might work; the Soviet soldiers could die

80. It is interesting that Sider regards officially atheistic troops of a hostile
government as his “brothers and sisters,” the “children” of the same god he wor-
ships.

81. Ronald J. Sider and Richard K. Taylor, Nuclear Holocaust and Christian
Hope: A Book for Christian Peacemakers (Downers Grove, IL: InterVarsity Press,
1982), p, 275.

16 Productive Christians in an Age of Guilt-Manipulators

lie; whose end is destruction, The words of Otto Scott are
especially relevant for our examination of Ronald Sider and his
writings:

The figure of the Fool is widely misunderstood. He is neither a jester nor
a clown nor an idiot. He is, instead, the dark side of genius. For if a
genius has the ability to see and make connections beyond the normal
range of vision, the fool is one who can see~and disconnect.

 

29. Otto Scott, James I (New York: Mason/Charter, 1976), p. 13.

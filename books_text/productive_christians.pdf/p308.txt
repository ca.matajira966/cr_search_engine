294 Productive Christians in an Age of Guilt-Manipulators

socialist system is that economic calculation is rendered impossible.112
Without the market system of profit and loss —the mechanism by
which prices are determined, through a multitude of individual
economic decisions — there is absolutely no way to assess the econ-
omy. Where should energy and capital be channelled? How much
do materials and products cost? Apart from the market, there is
simply no way of calculating anything. (If you don’t believe this, try
it. It cannot be done; and this argument by Mises has never been
Tefuted since he first put it forth in 1922. In fact, a society without
market pricing and calculation is totally inconceivable: it cannot
even be imagined— which is one reason why no socialist society has
ever achteved true socialism. The only thing socialism has ever been
able to provide its adherents is the “guaranteed income” of
Romans 6:23.)

P. T. Bauer asks this intriguing question: “How would you
rate the economic prospects of an Asian country which has very
little land (and only eroded hillsides at that), and which is indeed
the most densely populated country in the world; whose popula-
tion has grown rapidly, both through natural increase and large-
scale irnmigration; which imports all its oil and raw materials,
and even most of its water; whose government is not engaged in
planning and operates no exchange controls or restrictions on
capital exports and imports; and which is the only remaining
Western colony of any significance? You would think that this
country must be doomed, unless it received large external dona-
tions. Or rather you would have to believe this if you went by
what politicians of all parties, the United Nations and its
affiliates, prominent economists and the quality press all say
about less developed countries.”#3

The country is, of course, Hong Kong, which has become
such a powerful center of industry that the great Western nations

M2. Ludwig von Mises, Sucialism: An Economic and Sociological Analysis (In-
dianapolis: Liberty Classics, [1936] 1981).

13. P. T. Bauer, Equality, the Third World, and Economic Delusion, p. 185. See also
Alvin Rabushka, Hong Kong: A Study in Economic Freedom (Chicago: University of
Chicago, 1979).

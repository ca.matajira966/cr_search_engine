“TI don’t want to have secular penalties exercised by the state for
people who commit adultery or homosexual sins. People need to
be free to make choices in that area . ...”
(Ronald Sider, The Wittenburg
Door, Oct./Nov, 1979, p. 16)

“Tf there is a man who commits adultery with another man’s wife,
one who commits adultery with his friend’s wife, the adulterer and
the adulteress shall surely be put to death. . . . If there is a man
who lies with a male as those who lie with a woman, both of them
have committed a detestable act; they shall surely be put to
death.”

(Leviticus 20:10, 13)

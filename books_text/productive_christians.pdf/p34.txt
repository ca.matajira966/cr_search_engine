20 Productive Christians in an Age of Guilt-Manipulators

This, of course, means that Sider is free to devise his own blue-
print, while using vague “biblical principles” to justify his thesis
to the Christian community. Sider’s blueprint calls for socialistic
redistribution of wealth and government intervention—a blue-
print not countenanced by Scripture, but which Sider claims to
find in the fact that “biblical revelation tells us that God and his
faithful people are always at work liberating the oppressed, and
also provides some principles apropos of justice in society.”

In plain translation: where the Bible is specific on economic
issues, it is not valid; where the Bible states a general princeple that
can be redefined in terms of “‘liberationist” specifics, it is valid. In
Sider’s hands, the Bible becomes no more than a ventriloquist’s
dummy. Or, to put it another way: “The hands are Esau’s hands,
but the voice is the voice of Jacob.” Sider’s thesis feels biblical, on
the surface; but the voice is the voice of Ronald Sider.

Detailed documentation of this charge will appear in the follow-
ing chapters. For the present, we will examine an outline of the
biblical laws on economics and government. There #s “a com-
prehensive blueprint” for economics in Scripture, but it is not the
kind Sider wishes to implement. Therefore, he has to deny that
such a blueprint exists.

God’s Blueprint: Biblical Law

Christian economics begins with God the Creator, “who cre-
ated heaven and the things in it, and the earth and the things in it,
and the sea and the things in it” (Revelation 10:6). As Creator,
God is supreme Lord of His workmanship. No aspect of reality is
autonomous or neutral: everything is completely subject to His
commands, What we call physical laws (such as gravity, photo-
synthesis and the principles of thermodynamics) are simply the
outworking of God’s eternal decree. and continual providence.
And the same is true of economic laws. You may (as one man did)
write to Congress and request that our legislators repeal the law of
supply and demand, but it is God’s law, not subject to human

3. Ibid. (ef. p. 193].

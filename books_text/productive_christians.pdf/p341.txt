Socialism, the Anabaptist Heresy 327

generous tithes and offerings donated by pacifistic Anabaptists in
the United States). Dolcino’s millennium lasted for three terrible
years, until the orthodox Christians finally captured and executed
him in 1307.16

Dolcino’s heresy has become well-known recently through
Umberto Eco’s best-selling historical novel of the fourteenth cen-
tury, The Name of the Rose.!? Toward the end of the book, Brother
Remigio (a former member of the Apostolic Brethren) tells
Brother William what motivated the movement: “And we burned
and looted because we had proclaimed poverty the universal law,
and we had the right to appropriate the illegitimate riches of
others, and we wanted to strike at the heart of the network of
greed that extended from parish to parish, but we never looted in
order to possess, or killed in order to loot; we killed to punish, to
purify the impure through blood. Perhaps we were driven by an
overweening desire for justice: a man can sin also through over-
weening love of God, through superabundance of perfection. We
were the true spiritual congregation sent by the Lord and destined
for the glory of the last days; we sought our reward in paradise,
hastening the time of your destruction. We alone were the apostles
of Christ, all others had betrayed him, and Gherardo Segarelli
had been a divine plant, planta Dei pullulans in radice fidei; our
Rule came to us directly from God. We had to kill the innocent as
well, in order to kill all of you more quickly. We wanted a better
world, of peace and sweetness and happiness for all, we wanted to
kill the war that you brought on with your greed, because you re-
proached us when, to establish justice and happiness, we had to
shed a little blood. . . . The fact is . . . the fact is that it did not
take much, the hastening, and.it was worth turning the waters of
the Carnasco red that day at Stavello, there was our own blood,
too, we did not spare ourselves, our blood and your blood, much.
of it, at once, immediately, the times of Dolcino’s prophecy were
at hand, we had to hasten the course of events. . . .”18

16. Zid, pp. 29, 46-50.
17. New York: Harcourt Brace Jovanovich, 1983. See pp. 221-33, 382-84.
18. Jbid., p. 384.

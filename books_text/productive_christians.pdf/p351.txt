Socialism, the Anabaptist Heresy 337

More's work was followed for the next few centuries by other
utopian socialist writers, who refined More’s basic outline in terms
of a more consistent, thoroughly paganized, socialist vision. In
general, no practical steps were suggested for alleviating the condi-
tion of the poor; the image of the suffering poor was simply dredged
up in order to incite hatred and envy against the rich. The philoso-
phers were explicit in their insistence upon complete standardiza-
tion: increasingly, equality meant identity They dreamed of the “in-
evitable” approach of the socialist ideal, of total equality under a
total State, when language would become static.and unchanging,
reading (and eventually thinking) would atrophy, all days would be
alike, and even facial appearances would be identical.*?

Inspired by all this literary ferment — and by the brief incarna-
tion of socialist ideals during the French Revolution and the
Reign of Terror—secret societies and conspiracies came into be-
ing, of which Shafarevich makes a very important observation:
“At the moment of their inception, socialist movements often
strike one by their helplessness, their isolation from reality, their
naively adventuristic character and their comic, ‘Gogolian’
features (as Berdyaev put it). One gets the impression that these
hopeless failures haven’t a chance of success, and that in fact they
do everything in their power to compromise the ideas they are
proclaiming. However, they are merely biding their. time. At
some point, almost unexpectedly, these ideas find a broad popular
reception, and become the forces that determine the course of his-
tory, while the leaders of these movements come to rule the
destiny of nations.”®°

State Socialism

Shafarevich turns at this point to a discussion of the nature of
socialism when it gains control of an entire nation, beginning with

49. Thus the thesis of L.P. Hardey’s novel Facial Justice (Garden City, NY:
Doubleday & Company, 1960), cited on p. 177 above, is not as fanciful as it might
seem. “Facial justice,” strangely enough, is a recurring motif in socialist theory:
see Shafarevich, The Socialist Phenomenon, pp. 120, 198, 260, 269,

50. Shafarevich, The Socialist Phenomenon, p. 129.

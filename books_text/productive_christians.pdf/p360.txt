346 Productive Christians in an Age of Guilt-Manipulators

makers were quickly silenced. More people began reading the
Supplement; and the Manual (if it was read at all) was reserved for
reading at funerals, where people talked of the City in the Sky.
And there were many funerals, more than in the old days; but the
Other Siders explained that it was only because they were not de-
stroying the City quickly enough. “Besides,” Dr. Side would say—
quoting one of his mentors —“you can’t make an omelette without
breaking a few eggs.”

So the work went on, as the clouds gathered over their heads.
‘The work went on, as thunder began to roll. The work went on,
until the storm finally broke; until the rain descended, and the
floods came, and the winds blew, and beat upon Anomia; and it
fell: and great was the fall of it, And the Anomians hid themselves
in the dens and in the rocks of the mountains (for by now they
were ali Cave Dwellers); and said to the mountains and rocks,
“Fall on us, and hide us from the face of him that sitteth on the
throne, and from the wrath of the Architect; for the great day of
his wrath is come; and who shall be able ‘to stand?”

But there was one final surprise in store for the Anomians. It
came after the End, when Dr, Side removed his mask.

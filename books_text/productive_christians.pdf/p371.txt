The Background of “Productive Christians” 357

the Woodrow Wilson International Center for Scholars in Wash-
ington, D.C. His academic credentials are impeccable (for those
who care about such matters), and the work is a masterpiece of
historical scholarship. In it, he argues that the two main sources
of revolutionary socialism in the nineteenth century were the oc-
cult underground (secret societies) and journalism. This was a
true faith—a religion of revolution. (I was first informed of the ex-
istence of the book by David Chilton.)

Marxism possesses, for the moment, the three key features that
are necessary for any world-transforming ideology: 1) an histor-
ical dynamic, 2) a doctrine of law, and 3) a doctrine of predestina-
tion. To some extent, this faith is dying inside the nations where
Marxism has been politically triumphant, but in the Third
World, this faith is the driving secular religion of our day. They
believe in historical progress—a stage theory of social development,
the so-called dialectic of history. They believe in a unique law-order
which enables them to identify and promote “Marxist art,” “Marx-
ist social structures,” “Marxist genetics” (e.g., the ill-fated
Lysenko affair of the 1940s through the 1960s),’? and so forth. Fi-
nally, they believe in the inevitability of socialism, with the imper-
sonal forces of history guiding human institutions into the socialist
stage of history. As an intellectual force, the Marxist movement
has been almost irresistible.

Christianity offers an alternative. It does not offer a doctrine of
historical progress based on class conflict, but a theory of history
based on the ethical conflict between Satan and God, and between
Satan’s forces and Gods. It offers a law-order based on the crea-
tive work of God, not on the work of impersonal, undirected
“forces.” It offers a doctrine of God’s providence, not a doctrine of
impersonal historical inevitability. The warfare of this age is between
Christianity and Marxism. Christianity is Marxism’s chief rival.
When Christians affirm all three positions—historical (eschato-
logical) optimism, a revelational law-order (law code), and the
sovereignty of God (providence)—they can successfully challenge

12, David Joravsky, The Lysenko Afoir (Carabridge, Mass.: Harvard Univer-
sity Press, 1970).

384 Productive Christians in an Age of Guilt-Manipulators

to confiscate and destroy. Socialism is institutionalized envy. When
envy dominates a culture, progress is impossible: the envious will
hate those who are successful, and everyone will do his best not to
appear successful, in fear of being envied. When envy is inverted
and turned in upon oneself, it becomes guilt (q.v.). You can sell a
lot of copies of books that encourage guilt in an envy-dominated
culture. Christians who do not tithe constitute a strong potential
market for such books.

Exchange is involved in every human action, for people act in
order to trade a less-desired condition for a more-desired condi-
tion. Within a free market, every morally legitimate exchange will
benefit both parties, since both are exchanging less-desired goods
for more-desired goods. See Profit.

Factors of Production are three: (1) the natural resources of the
earth (land), (2) the human work in developing those natural
resources (labor), and (3) time. These three factors of production
are usually summed up as land, labor and capital. Capital is the
combination of land and labor over time. They receive rent,
wages, and interest (time-preference payment).

The Federal Reserve System is ‘the. central bank of the United
States, created by the Federal Reserve Act of 1913. (Over 100
amendments have since been added to the Act, vastly increasing
the power of the “Fed.”) The Fed controls the supply of money and
credit, in Ime with the general policies of the administration in
power. It creates money out of nothing and buys up federal debt.
Then the government spends this new money. Its actions have
caused the increasing worthlessness of the dollar. See Credit expan-
Sion, Fractional reserve banking and Inflation.

Fiat money comes from the biblical account of creation, when
God said (in the Latin translation), “Fiat lux,” which means “Let
there be light.” The government says, “Fiat bucks! Let there be
money!” This is done by printing paper money or expanding credit
beyond actual gold or silver reserves. It is a modern form of

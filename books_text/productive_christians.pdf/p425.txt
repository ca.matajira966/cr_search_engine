Bibliography 41

Davidson, James Dale. The Squeeze. New York: Summit Books,
1980.

Freeman, Roger A. The Growth of American Government: A Mor-
phology of the Welfare State. Stanford, CA: Hoover Institution
Press, 1975.

Kwiiny, Jonathan. Endless Enemies: The Making of an Unfriendly
World. New York: Congdon & Weed, 1984.

Lambro, Donald. Fat City: How Washington Wastes Your Taxes.
South Bend, IN: Regnery Gateway, 1980.

Morgan, Richard E, Disabling America: The “Rights Industry” of our
Time. New York: Basic Books, 1984.

Murray, Charles. Losing Ground: American Social Policy, 1950-80.
New York: Basic Books, 1984.

O'Shea, Lester. Tampering with the Machinery: Roots of Economic and
Political Malaise. New York: McGraw-Hill Book Company,
1980.

Reynolds, Morgan O. Power and Privilege: Labor Unions in America.
New York: Universe Books, 1984.

Reisman, George. The Government Against the Economy, Thorn-
wood, NY: Caroline House, 1979.

Schuettinger, Robert, and Butler, Eamonn. Forty Centuries of Wage
and Price Controls: How Not to Fight Inflation, Thornwood, NY:
Caroline House, 1979,

Taylor, E. L. Hebden. Economics, Money, and Banking. Nutley, NJ:
The Craig Press, 1978.

Templeton, Kenneth S., ed., The Politicization of Society. In-
dianapolis: Liberty Press, 1979.

Tyrrell, R. Emmett Jr, The Liberal Crack-Up, New York: Simon
and Schuster, 1984.

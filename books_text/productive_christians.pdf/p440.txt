426

Free Spirits. See Brethren of the Free
Spirit

Free trade, 100-05, 385

French Revolution, 3, 6, 74, 2t,
226, 306, 332, 337

Future orientation, 95, 227, 229-45,
385

Gay, Peter, 25in

“Gentle Nudge,” 171-172, 289, 345

Ghandi, Indira, 100, 120

Gilder, George, 148, 269, 275n, 280

Gish, Art, 140n, 219, 310-11, 364

Gladwin, John, 310, 311, 364

Gleaning, 56-57

Global 2000, 256

Gnosticism, 334

Godzilla, 310

Golden Rule, 40

Gordon-Conwell School of Theology,
352, 355-56

Géring, Hermann, 65

Gospel, 74, 93, 95-96, 110, 222-26,
230, 238-42, 386

Gouldner, Alvin, 264n

Graham, Billy, 72, 75

Graham, Gen. Daniel, 285n

Grant, George, 319

“Great Society,” 269-70

Greaves, Percy L.., Jr, 377

Griffin, Bryan F., 254 n.

Griffiths, Brian, 250

Groen van Prinsterer, G. 5-6, 244n

Groppi, “Father,” 349-50, 351

Guilt-manipulation, 9, 12, 52-53, 88,
13, 115, 144, 172, 205-13, 244, 265,
275, 283, 301, 302, 305-06, 319,
329, 384, 386, 396

Guilt vs. freedom, 206-12, 283n

Hagen, Everett, 17
Hahn, Scott, 352, 355
Halverson, Richard, 360

Productive Christians in an Age of Gutli-Manipulators

Hanks, Tom, 288n.

Haroid IL, 207-08

Harrington, Michael, 274

Hartley, L. P., 177, 3370

Hayek, Friedrich A., 66, 281, 293,
296n, 317n

Hazlitt, Henry, 140, 269, 378

Hefner, Hugh, 142

Herbert, Auberon, 50-51, 64

Heresy, 307-09, 321-42

“High Frontier,” 285n

Hinduism, 96-97

Hitler, Adolph, 4, 8, 65, 263, 392

Hong Kong, 294-95

Howarth, David, 207

Humanist Manifestos, 315-16, 335

Humanists, 348-49, 386

Hutterites, 160

Idealism, 325n

Illegal aliens, 47

Immigrants. See Strangers

Immigration and Naturalization
Service, 46, 47

Incas, 338

Incentives, 272-73. See also
Responsibility

Income
guaranteed, 35, 50, 294
given away, 14, 52, 138, 394-95
per capita figures, 91-92, 394
psychic, 92
differences, 91-92, 305-06

India, 96-97, 100, 107-08, 119, 120,
218-19, 226, 255-56

Infallibility, 316

Inflation, 41-42, 197-98, 200, 220,
379-80, 381, 386-87

Institutional unemployment, 187-88,
387, 390, 397

Internal Revenue Service, 354

InterVarsity Press, 261n, 264, 283,
295, 310, 319, 351, 361-62

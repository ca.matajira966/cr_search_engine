60 Productive Christians in an Age of Guilt-Manipulators

think of “slavery,” think of the pre-Civil War South, where cer-
tain aspects of slavery were in violation of biblical law. Thus
many know only of an abused, unbiblical form of slavery. But
since the Bible allows for slavery, it is clearly unbiblical to speak
of slavery as being wrong or sinful. (Even Southern slavery was
not as unbiblical as many have charged. The common conception
of the slavery of that age is quite distorted; the Abolitionists were
often as guilty of transgressing God’s laws as were slave-holders,
-as we shall see in our next chapter.) If slavery were a sin, God
would not have provided for it. Indeed, since God is the Standard
of right and wrong, the fact that He gives rules for the proper
management of slavery shows that to disregard the laws of slavery
is asin. For example, since fornication is a sin, God does not give
directions for the right management of a brothel. Nor does he
offer instructions about successful methods of murder or theft.
Slavery is not a sin, but the violation of God’s slavery laws is.
To understand God’s slavery laws, we must understand a basic
biblical fact: slavery is inescapable~-no culture is without it. Apart
from God’s grace, all men are enslaved to sin. Salvation liberates
us from slavery to sin and makes us slaves of righteousness, obe-
dient to God’s word rather than to Satan’s (Romans 6:16-22). I
am not playing with words here, for this point is central to social
and cultural issues. If men are not slaves of God, they are already
enslaved to sin. As sinners, they abandon their duty of dominion
over the creation, with the result that they become slaves of other
men, worshipping and serving the creature rather than the Crea-
tor (Romans 1:25). The issues of life flow from the heart (Prov-
erbs 4:23), and a man’s relationship to Ged, or lack thereof, has
immediate and long-lasting consequences in every area. Every
culture that has not served the true God has eventually become
enslaved to the state! Ron Sider’s thesis will not “liberate”
anyone in this regard: his solution to the problem of poverty is
merely a plea for increased slavery to the state through radical

13. For a fascinating history of an important aspect of this, see Forty Centuries of
Wage and Price Controls, by Robert Schuettinger and Eamonn Butler (Ottowa, IL:
Caroline House Publishers, Inc., 1979).

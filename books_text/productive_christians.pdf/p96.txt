82 Productive Christians in an Age of ' Cuilt-Manipulators

his people to do the same.”* “There he goes again: “the
oppressed,” “the blind,” “the poor,” as if Christ makes no distinc-
tion. Yet what Jesus says a few verses later is very important with
regard to God’s care for the poor. Jesus was preaching in
Nazareth, His home town, where He was faced with rejection and
unbelief—even, apparently, by “the poor and oppressed.” Our
Lord responded by leaving town, permanently: the poor of
Nazareth were henceforth excluded from His ministry, Before He
left, Jesus explained His actions by reminding them of Elijah and
Elisha’s ministry to the poor and afflicted. Although there were
those in their day who had not bowed to Baal, still it was a time of
rampant, vicious ungodliness. “The poor” of Israel received no
help from God’s prophets; instead, the prophets aided foreigners
who had called on the name of the Lord and were obedient to His
word, Our Lord’s comment stung, and the people of Nazareth felt
it:

“But I say to you in truth, there were many widows in Israel in the days
of Elijah, when the sky was shut up for three years and six months, when
a great famine came over the land (note: God afflicted both the rich and
the poor by withholding rain]; and yet Elijah was sent to none'of them, but
only to Zarephath, in the land of Sidon, to a woman who was a widow.
And there were many lepers in Israel in the time of Elisha the prophet;
and nene was cleansed, but only Naaman the Syrian.” And all in the
synagogue were filled with rage as they heard these things; and they rose
up and cast Him out of the city, and led Him to the brow of the hill on
which their city had been built, in order to throw Him down the cliff.
(Luke 4:25-29)

In this important statement, Jesus declares that God’s concern
for the poor is discriminatory. It is not just “the poor” in some
abstract, general, universal sense who are the objects of God’s
care, Here they are on the same level with the rich: if they reject
Christ, they are themselves rejected by Him, They wanted
benefits, but were ready to murder Him when they discovered
that He practiced discrimination in His welfare plan. There is no

4, Sider, Rich Christians, pp. 66f. [pp. 594].

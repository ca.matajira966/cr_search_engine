16 Puritan Economic Experiments

formed a new community down the road, Marlborough. Not
gaining access to the local commons, they were perfectly willing
to settle for a 24,000-acre plot a few miles away.”*

Factional strife was not a part of the original goals of the
founders of New England. Factionalism was a blight to be avoided;
this opinion remained a touchstone of American political thought
right down to James Madison’s Federalist #10. Yet the quarreling
over the commons was incessant, in direct opposition to the
political and communal ideal of the peaceable kingdom.

“Togetherness”

The town of Sudbury was not to be the only Puritan village
unable to cope successfully with the centrifugal forces created
by the presence of socialized property within the town limits.
The creation of Marlborough, despite the fact that the young
founders also established a town commons, testified to the diffi-
culty of preserving both the old common field tenure system and
social peace in the midst of vast stretches of unoccupied land. It
was too easy to move out, and this feature of New England was
to erode the medievalism of early Puritan thought. The central-
ized social control necessary to enforce such a system of common
land required the existence of widespread land scarcity. Ironi-
cally, it was in the final quarter of the seventeenth century that
such land scarcity appeared —scarcity of the most productive
lands—but by that time the haggling over the administration
of the commons and increasing land values had already provided
the incentives necessary to convince both leaders and average
citizens that the commons should be distributed permanently.

One of the original goals of the founders of New England
was that of social cohesion. The life of each community was to
be religiously based. The church was the center of the town, both
symbolically and very often physically. Men were to live close
to each other, share in each other’s burdens, pray together, and
construct God’s kingdom on earth. But there was a strong eco-

24. Powell, Puritan Village, ch. 9.

24 Puritan Economic Experiments

professional academic economists, and very few men were morc
than pamphleteers, even among the “professionals.” Thus, it is
not surprising that the first two generations of leaders in New
England should have fallen back upon “tricd and true” medieval
economic concepts. One of these was the concept of the just
price.

A “Just” Price

A lot of needless confusion has emerged from discussions of
scholars concerning the just price. From the time of Thomas
Aquinas right up until the mid-seventeenth century, a “just”
price was assumed to be the market price during “normal” times.
No widely read schoolman ever tried to compute some mathe-
matically precise formula on the basis of ethics; indeed, Aquinas
himself had denied that such precision is possible.5 The problem
of justicc arose when there were disruptions in the market—a
war, a famine, a local production monopoly—that made it
appear that justice was being thwarted by greedy exploiters.
Then the standard approach was to assemble a group of distin-
guished, “impartial” leaders in the community, and they were
supposed. to determine the proper prices for various commodi-
ties. The goal, officially, was consumer protection. More often
the result was the creation of an even more monopolistic guild,
for the “just” or “reasonable” price was, in the absence of a
competitive market price, computed on a “cost-plus” basis. As
in World War II, this was more apt to lead to producer protec~
tion = from more competitive producers.®

These restrictions on free entry—to guarantee “quality”
production from “unscrupulous” producers who would offer
shoddy goods at lower prices—were the foundation of the
medieval guild system, Similar restrictions operated in New

5. Thomas Aquinas, Summa Theologica, H-H, Quest. 77.

6. Price competition broadens the scope of the market by making goods and
services available to buyers formerly too poor to enter the market, It was the
fundamental form of competition in the coming of modern capitalism: Max Weber,
General Economic History (New York: Collier [1920] 1961), p. 230.

 

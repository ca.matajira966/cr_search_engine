 

Price Controls 25

England during the first half-ccntury of its existence. Licensing,
municipality-enforced inspections, and self-policing by guild mem-
bers were all features of the medicval city and the New England
town. But the most common feature was a system of price and
wage controls.

Early Controls

Almost from the beginning, the colony of the Massachusetts
Bay Company placed controls on the wages of artisans. The
colony was begun in 1629; in 1630 a law was passed which
established wage ceilings for carpenters, joiners, bricklayers, saw-
yers, and thatchers.? Common laborers were limited to twelve
shillings a day, or six if meat and drink were provided by the
employer. Any artisan violating this statute was to be assessed
a ten shilling fine. The effect of these wage ceilings must have
presented itself almost immediately: an excess of demand for the
services of artisans over the available supply. Under such condi-
tions, it is always difficult to recruit labor. Within six months,
these wage ceilings were repealed, leaving wages “free and at
liberty as men shall reasonable agree.”® The implication was
clear enough, however: if men should again grow unreasonabic,
the controls would be reimposed. They were.

The history of price and wage controls in New England is
an “on again, off again” affair. The year 1633 brought a new sect
of regulations, a law which the magistrates saw fit to repeal in
1635.° The repeal. was of a special nature, however. The civil
government imposed a general profit margin of 33 percent on
any goods sold retail in the colonics if the particular good was
imported. No imported good could therefore be sold for over 33
pereent above the London price. The magistrates then inserted

7. Nathaniel B. Shurteff (ed.), Records of the Governor and Company of the Massa-

chuseits Bay in Naw England, 5 vols. (Boston: William White, Commonwealth Printer,
1853), 1, p. 74. [Cited hereafter as Mass. Col. Recs.]

8. Ibid, I, p. 84.
9. fbid., 1, pp. 104, 159-60.
10. #bid., 1, pp. 159-60: the maximum rate was 4d/s, that is, 4 pence per shilling,
or 4/12, or 33 percent:

 

Price Gontrols 29

wheelwrights. All other handicraft workers were prohibited from
taking more than 18 pence per day for the first half of the year,
nor more than 12 pence during the remainder of the year. Work-
ers were obligated to work an eleven-hour day in the suramer,
in addition to eating and sleeping, and nine hours in the winter.
Price controls were placed on sawed boards.’ This jumbled
mass of legislation was repealed within a decade.'®

New Haven Colony, which was merged with Connecticut in
1662, imposed. controls in 1640, just as the great economic de-
pression of the 1640’s struck New England.'9 No longer would
there be the massive exodus of Puritans from England, for the
Civil War of the Cromwell era had begun, and English Puritans
stayed in the British Isles to fight. Thus, the capital and currency
brought in. by immigrants from 1630-40 was instantly cut off.
Prices collapsed almost overnight. The irony of imposing price
controls in the year of the beginning of a collapse in prices should
be obvious, As in the comparable medieval legislation, a group
of supposedly disinterested men served as the committee which
Judged individual situations in cases involving disputes.”° These
controls do not appear in the 1656 law code of the colony,
indicating that they had abandoned price controls in the inter-
vening period.

The Trial of Captain Keayne, an Untypical Case

Historians are not agreed on the actual effects of such legisla-
tion. Some think that the Puritans were in dead carnest about
enforcing the codes.?! On the other hand, one scholar has argued
that they probably were not that crucial in operation; between

17, J. Hammond Trumball and Charles Hoadly (eds.), The Public Recards of the
Colony of Connecticut, 15 vols. (New York: A.M.S. Press, [1850-90] 1968), T, (1641),
p. 65, [Cited hereafier as Conn. Col. Recs.}

18. 1bid., L, (1650), p. 205.

19, Charles Hoadly (ed.), Records of the Colony and Plantation of Naw Haven 1650-
1649 (Hartford: for the editor, 1857), pp. 35-36, 52, 55.

20. fdid., p. 55
21. Morvis, Gavernment & Labor, p. 72.

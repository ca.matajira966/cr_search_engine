Sumptuary Legislation 57

a tiny, rural colony in an uncharted wilderness to a thriving and
productive component of a newly developed English trade sys-
tem, civil custom was indeed the question. Customs were any-
thing but fixed or universal. After 1680, clerical opinion no
longer carried as much weight in establishing or maintaining
older customs. The very fluidity of fashion, where new styles
could sweep through the community, reflected the lack of fixed
standards, and this fact dismayed the preachers.

Conclusion

Status distinctions were supposed to be respected by mem-
bers of a Holy Commonwealth; this meant that each status
required its appropriate fashions, manners, customs. The prob-
lem which the first generation had never been willing to consider
was to make itself felt in the 1670’s. In a society in which men
are not only free to increase their estates, but in fact have a moral
obligation to do so, should men not be allowed to improve their
social statuses? If Puritan frugality, the rational use of time and
resources, systematic accounting, personal responsibility, and a
future-oriented view of the world are allowed to combine into
an ethos favoring both individual and aggregate economic growth,
then social mobility, upward or downward, should be character-
istic of that society. Yet the Puritan theologians of the second
generation did not reach such a conclusion. Therefore, given
their unwillingness to accept the legitimacy of social mobility
on such a scale, they had an obligation to spell out the nature of
specific legislation, both ecclesiastical and civil, that would dc-
fine the relationship between status and wealth, and between
status and fashion. This was the great stumbling stone for the
Puritan oligarchs, The ministers were never able to agree on
such rules. The sumptuary laws went unenforced, relics of the
first generation’s confidence in status legislation. Fashions con-
tinued to degenerate, and finally, to the horror. of many of the
pastors, Puritan sairits began wearing wigs! As far as the sermons
of the 1670’s are concerned, Worthington C. Ford’s description
holds good: “Massachusetts Bay was becoming degenerate, the

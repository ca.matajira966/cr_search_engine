A Commitment to Cultural Irrelevance 103

speaking, meaning as a coherent system, dispensational theology
is dead. Its brain wave signal has gone flat. It has now assumed
room temperature. RIP It was not killed by its theological
opponents. Its defenders killed it by a thousand qualifications.
They revised it into oblivion.’* Like 4 man peeling an onion,
dispensational theologians kept slicing away the system’s embar-
rassing visible layers until there was nothing left. The last re-
maining layer was removed by H. Wayne House and Thomas
Ice in their 1988 book, Dominion Theology: Blessing or Curse?

As an intellectual system, dispensationalism never had much
of a life. From the beginning, its theological critics had the
better arguments, from George Bush in the 1840's to Oswald T.
Allis’ classic study, Prophecy and the Church, published in 1945.
But the critics never had many followers. Furthermore, the
critics were trained theologians, and dispensationalists have
never paid much attention to trained theologians. Besides,
there were not very many critics, Because dispensationalists had
no self-consciously scholarly theology to defend and no institu-
tions of somewhat higher learning until well into the twentieth
century, their critics thought that they could safely ignore the
dispensational movement. They always aimed their published
analyses at the academic Christian community. They thought
they could call a halt to the rapid spread of dispensationalism
through an appeal to the Scriptures and an appeal to the schol-
arly Christian community. They were wrong. Theirs was a
strategic error; popular mass movements are not directly affec-
ted by such narrow intellectual challenges. Indirectly over time,
yes, but not directly. Few people adopt or abandon their theo-
logical views by reading heavily footnoted and carefully argued
scholarly books. Thus, the appeal of dispensational theology
was not undermined by its theological opponents; instead, it
collapsed of its own weight. Like a former athlete who dies of a
heart attack at age 52 from obesity and lack of exercise, so did

14, See Chapter 8,

146 RAPTURE FEVER

co-author, along with Peter Leithart, of The Reduction of Chris-
tianity (1988), which was a response to Hunt's Beyond Seduction
(1987). Also in 1988, then-Dallas Seminary professor H. Wayne
House and Rev. Ice wrote Dominion Theology: Blessing or Curse?
A year later, the Institute for Christian Economics published a
rebuttal, House Divided: The Break-Up of Dispensational Theology.*
All of this writing and publishing took place within a period of
two years.

House Divided publicly buried an expired theological system.
What is even more significant about this burial is that dispensa-
tionalism’s official defenders have been almost as active in gath-
ering dirt to shovel on the casket as its theonomic critics are.>

The Academic Game of Quiet Revising

House and Ice quietly revised the fundamental doctrines of
traditional dispensational theology. They no longer believe that
the old dispensational theology can be successfully defended, a
suspicion obviously shared by Dallas Theological Seminary
Professor Craig Blaising, as revealed by the citation which
begins this chapter. For example, they (i.e., House) argue that
the death penalty is still valid in New Testament times because
this was part of Noah’s covenant (Gen. 9:5-6)} — a pre-Mosaic
covenant.’ This was Calvinist theologian John Murray’s argu-
ment a generation ago.” It is a bit odd to see dispensationalists
appealing to traditional covenant theology when defending dis-
pensationalism against theonomy. Professor House in this case
has dressed John Murray’s covenant theology in Lewis Sperry

4. Available from the ICE; $25, hardback.

5. See, for example, John MacArthur, Jr, The Gaspel According to Jesus (Grand
Rapids, Michigan: Zondervan Academie, 1988), which documents the antinomianism
of conventional dispensationalism. See Chapter 10, below.

6. House and Ice, Dominion Theology: Blessing or Curse? (Portland, Oregon:
Multnomah Press, 1988), p. 130.

7, John Murray, Principles of Conduct: Aspects of Biblical Eihics (Grand Rapids,
Michigan: Eerdmans, 1987), p. 118.

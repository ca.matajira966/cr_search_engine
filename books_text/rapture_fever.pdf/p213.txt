Dispensationalism us. Sanctification 179

true; but this does not mean that “once confessed, always
saved” is true. The perseverance of the saints is a reality; the
evidence that some men are not saints is that they refuse to
maintain either their confession or the lifestyle which the Bible
mandates for those who confess Christ as savior.

Know ye not that the unrighteous shall not inherit the kingdom
of God? Be not deceived: neither fornicators, nor idolaters, nor
adulterers, nor effeminate, nor abusers of themselves with man-
kind, Nor thieves, nor covetous, nor drunkards, nor revilers, nor
extortioners, shall inherit the kingdom of God (I Cor. 6:9-10).

Conclusion

The dispensationalists now face a major problem: one of
their own has broken publicly with the traditional dispensation-
alism’s antinomianism. Christians bought something in the
range of 100,000 copies of his book - a huge number. Although
he has not shown how, exactly, his view of law can be integrat-
ed into dispensationalism, he has nevertheless unleashed the
power of God’s law within dispensational circles. Once again,
we see that the dispensational system is unraveling, and it is
dispensational authors who are doing this work of deconstruc-
tion. They are revising the system to death. This is producing
another layer of paralysis.

To escape the ethical paralysis of dispensationalism, Mac-
Arthur adopted a view of law and salvation which Calvinists J.
I. Packer and James Boice could enthusiastically endorse. Once
again, in order to become relevant, a dispensationalist has had
to look for theological support from outside dispensationalism’s
camp. This is the fate of any movement that refuses to address
publicly the ethical issues of the day.

186 RAPTURE FEVER

believe that historical forces are on their side, that Marxism-
Leninism provides them with access to the laws of historical
change, and that their movement must succeed. Islam has a
similar faith. In the early modern Christian West, Calvinists and
Puritans had such faith. Social or religious philosophies which
lack any one of these elements are seldom able to compete with
a system that possesses all three. To a great extent, the cultural
successes of modern secular science have been based on a fu-
sion of these three elements: scientific (material) determinism,
the scientists’ knowledge of natural laws, and the inevitable
progress of scientific technique. As faith in all three has waned,
the religious lure of science has also faded, especially since
about 1965, when the counter-culture began to challenge all
three assumptions.

Modern fundamentalism has long since abandoned all three.
The fundamentalists are divided on the question of predestina-
tion, but the majority are committed to Arminian views of God,
man, and law. They believe in man’s limited autonomy, or “free
will.” Furthermore, they have rejected biblical law as a guide-
line for social order. They argue that there is no explicitly
Christian Jaw-order in the era of the church, from Pentecost to
the future Rapture into heaven of the saints. Finally, they are
committed to eschatological pessimism concerning the efforts of
the church, in time and on earth. Without a doctrine of the
comprehensive sovereignty of God, without a doctrine of a
unique biblical law structure that can reconstruct the institu-
tions of society, and without a doctrine of eschatological victory,
in time and on earth, the fundamentalists have been unable to
exercise effective leadership.

The prospects for effective political action have begun to
shake the operational faith of modern fundamentalists ~ not
their official faith, but their operational world-and-life view.
This shift of faith will steadily pressure them to rethink their
traditional theological beliefs. The leaders of the Moral Majority
will come under increasing pressure, both internal and exter-

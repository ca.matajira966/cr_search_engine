200 RAPTURE FEVER

absorbed a fantastic $69 million in donations from six million
people. The Moral Majority was shut down in 1989, the year of
George Bush’s inauguration. (Rev. Falwell had publicly sup-
ported the idea of a Bush Presidency since 1984.) The AP story
commented:

In the blink of an eye, Mr. Falwell went from a central figure
in the nation’s stage to a bit player, burdened by enormous
problems.

How would you feel at age 59 to have your life’s visible
legacy $90 million in debt, having defaulted on the interest
payments, and with no visible means of your ministries’ avoid-
ing bankruptcy before you die? What kind of Christian testimo-
ny would you imagine that this presents? He has only two
hopes: an early death or the Rapture.

So, what do we find? He has been struck down once again with
Rapture fever. In his broadcast of December 27, he said that the
Rapture will probably take place by the year 1999. But then he
added that he also expects to live as long as W. A. Criswell, the
legendary 83-year-old pastor of First Baptist Church of Dallas.
Mixed chronological signals!

Meanwhile, First Baptist Church of Dallas, the nation’s larg-
est Southern Baptist church (28,000 on the rolls), is now $8
million in debt. It has had to cut back its television ministry.
The story has recently been on the front page of the Dallas
Morning News. It has once again lost a promising assistant pas-
tor because Rev. Criswell has once again refused to give up
preaching at the 11 a.m. service. The church keeps hiring the
best replacement pastors it can find, promising them that they
will.soon lead the church. Fat chance. Then they quit. And the
debt builds up.

In ministry after ministry, the story is the same. Debt. Enor-
mous debt. Debt for Jesus’ sake. From red letter edition Bibles
(no creed but Christ, no law but love) to red ink: the story is

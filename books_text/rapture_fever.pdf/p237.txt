Conclusion 203

supposedly based on universal principles of natural law — than
the Church. They have trusted the moral authority of the hu-
manist State far more than they have trusted the moral author-
ity of the Church. And why not? The humanist State is a win-
ner in history. The Church is a loser. So say the theologians.
The result is easily predictable: a Church filled with people who
are unskillful in the word of righteousness, God’s revealed law.

No better statement of this ethical position can be found
than Norman Geisler’s 1992 affirmation. Dr. Geisler received
his Ph.D. in philosophy from a Jesuit university. He defends
neutral natural law. He has devoted a large portion of his aca-
demic career to a public rejection of biblical law. Geisler is a
dispensationalist: formerly a professor at Dallas Theological
Seminary and formerly a professor at Jerry Falwell’s Liberty
University. He writes: “The religious right is at least as dan-
gerous as the secular left. Religions theonomy (divine law) as
the basis for human dignity can be as frightening as secular
anarchy.’ He assures us that “Theonomy is an unworkable
ethical basis for government in a religiously pluralistic society,
whether it be Muslim or Christian in form,”? He insists: “Soon-
er or later the question arises: whose religious book will be the
basis for the civil laws? It is sheer religious bigotry to answer:
‘Mine. ”? Conclusion: it is not sheer religious bigotry for secular
humanists to answer, “Ours.” Dr. Geisler believes in the legitimacy
of secular humanism’s claim that civil law can be religiously
neutral and morally valid. Dr. Geisler is a consistent dispensa-
tionalist: he prefers to live under the civil banner of religious pluralism
rather than under the civil banner of Jesus Christ, So do his colleagues.

1. Norman Geisler, “Human Life,” In Search of a Nattonal Morality: A Manifesto for
Evangelicals and Catholics, edited'by William Bentley Bali (Grand Rapids, Michigan:
Baker Book House, 1992), p. 114. Mr. Ball is a Roman Catholic lawyer who special-
izes in defending Christian schools. Baker Book House is a Protestant publishing firm
with a Calvinist slant. it publishes mainly amillennial books.

2. Idem.

8. Idem.

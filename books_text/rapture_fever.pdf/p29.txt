Preface xxi

biblical languages or some related technical field, but when they
produce their scholarship, these works are rarely explicitly
dispensational. Rarely in our day do they even attempt to
define and defend the broad categories of dispensational theol-
ogy. The classic works of dispensationalism are at least a gener-
ation old and are going out of print.

This is not random. This is the result of a specific view of
time and law. Dispensationalism in the 1990's has become intel-
lectually paralyzed. This book shows why and how this hap-
pened. I believe, though do not attempt to prove here, that this
intellectual paralysis will lead to a more general paralysis within
two decades. To avoid this paralysis, today’s intellectual leaders
within the dispensationalist camp must rethink the categories of
traditional dispensationalism and make the system relevant. I
believe this cannot be done without scrapping dispensationalism
and inventing something new. It may be called dispensation-
alism, but it will not be dispensationalism. It will have aban-
doned every theological distinctive that the founders of the
various dispensational seminaries sacrificed so much to defend.
This abandonment has already begun, as I show in this book.
More than this: this process of abandonment is now in its final
stages. This is the “dirty little secret” that the leaders of dispen-
sationalism have done their best to hide from donors since
1985.

The Silence of the Sacrificial Lambs

This intellectual defection began in 1945. That was the year
that O. T. Allis, America’s premier Old Testament scholar at
the time, wrote Prophecy and the Church. That book was relent-
less and thorough in its refutation of dispensationalism’s escha-
tology, point by point. Academic dispensationalists adopted a
doomed strategy to deal with Allis: a conspiracy of silence. They
played “let’s pretend”: let’s pretend our students will never
read this book, our supporters will never hear of it, and our
critics will never spot the nature of our defensive strategy.

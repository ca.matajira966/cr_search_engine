Preface XXXiil

Biblical Theology of Dominion (1985), Days of Vengeance: An Exposi-
tion of the Book of Revelation (1987) and his smaller book, The
Great Tribulation (1987). In succession came Greg Bahnsen and
Ken Gentry's House Divided: The Break-Up of Dispensational Theol-
ogy (1989), a devastating reply to (then) Dallas Seminary profes-
sor H. Wayne House and his research assistant, Thomas D. Ice.
(House left Dalias soon thereafter.) Then came Gentry’s Before
Jerusalem Fell: Dating the Book of Revelation (1989), The Beast of
Revelation (1989), The Greatness of the Great Commission (1990),
and his massive exposition, He Shall Have Dominion: A Postmillen-
nial Eschatology (1992). Also published in this period were Gary
DeMar and Peter Leithart’s The Reduction of Christianity: A Bibli-
cal Response to Dave Hunt (1988), DeMar’s The Debate Over Chris-
Han Reconstruction (1988), Last Days Madness (1991; not published
by me), and my Millennialism and Social Theory (1990). Above all,
there was That You May Prosper: Dominion By Covenant (1987),
written by a Dallas Seminary Th.M., Ray Sutton. Dr. Sutton
today is the president of Philadelphia Theological Seminary
and the chancellor of education for the Reformed Episcopal
Church. In the face of all of this, Dallas Seminary has remained
silent, except for an occasional brief book review by John Wal-
voord or Robert Lightner.

Only two dispensationalist authors have replied in detail to
Christian Reconstruction: House and Ice. (Dave Hunt never
devoted more than a few pages to us, and the ill-fated attempt
by Hal Lindsey to identify all non-dispensational theologies as
inherently anti-Semitic is representative of neither dispensation-
alism nor scholarship.*) Since his departure from Dallas Semi-
nary, Dr. House has not put anything into print about theon-
omy or Christian Reconstruction, which is not surprising, given
what Dr. Bahnsen did to him in full public view for over 130

6. Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989). For a response,
see Gary DeMar and Peter Leithart, The Legacy of Hatred Continues: A Response to Hal
Lindsey's The Road to Holocaust (Tyler, TX: Institute for Christian Economics, 1989).

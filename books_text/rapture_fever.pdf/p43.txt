Introduction 9

with taxes extracted from Christians. The major institutions of
American humanism have been built with Christians’ money. The
Christians never complained about this publicly until the late
1970's, when they finally began to perceive three things: (1) the
growing failure of humanist institutions to “deliver the goods”;
(2) the size of their own tax bills; and (3) the non-neutrality of
humanism — humanism’s war on the Christian faith.

‘Today, American fundamentalism is sharply divided. There
are many who still hold the old theology and the old worldview
—not in the Big Three seminaries, but in the pulpits and pews.
Their solution is reminiscent of the tactic used by the wagon
trains on the Great Plains in 1870: “Form a circle with the
wagons!” They hope and pray for the imminent arrival of Cal-
vary's cavalry: Captain Jesus and His angelic troops, trumpet
blaring, who will carry them safely to their final destination —
not California; heaven. The problem is, this psychological and
institutional tactic is not a valid strategy, since nobody really
wants to spend his whole life inside a circle of covered wagons,
with a horde of howling savages — many of them with Ph.D.
degrees from prestige universities — attacking the perimeter of
the camp.

More and more of those who are trapped inside fundamen-
talism’s tight little defensive circle are becoming fed up, both
with the savages outside the camp and the leaders inside. They
are becoming ready psychologically to take the war directly to
the enemy. But they don’t know how. They have not been
trained to fight an offensive campaign. At best, they are special-
ists in defense. They have long been denied the weapons need-
ed to conduct an offensive campaign, most notably comprehen-
sive, self-consciously biblical higher education.

Questions that have long been dismissed as irrelevant for
Christians to ask are now being asked by younger fundamental-
ists. The main question is the one that premillennial but non-
dispensational Calvinist Francis Schaeffer asked in 1976: How
should we then live? Schaeffer never offered an answer, but his

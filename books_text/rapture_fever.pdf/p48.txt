i4 RAPTURE FEVER

Where is it said to be a crime in the Bible?

“Should we regard the arrival of AIDS in 1981 as an “ethically
random event,” the way we regard chicken pox?

¢Is bestiality a sin?

*Is bestiality said to be a crime in the Bible?

* Where is it said that bestiality is a crime in the Bible?

If we cannot find New Testament legal standards, could dis-
pensationalism be wrong about Old Testament law?
Has dispensational theology become irrelevant?

These questions are never addressed in print by the older
dispensational theologians. They are rarely addressed by the
younger ones, since they fear losing their jobs. If they make a
wrong answer — a wrong answer being one which clearly breaks
with one of the official tenets of the dispensational system —
they could be fired. Not at Talbot, of course. And there are no
longer any full-time positions remaining at Grace. But at Dallas
you could lose your job. So the wise faculty member at Dallas
Seminary follows Solomon's advice: “A prudent man concealeth
knowledge” (Prov. 12:23a). He keeps his mouth shut and his
published work focused on some topic not inherently dispensa~
tional. So, dispensationalism today has no intellectual leaders.

Traditional dispensational textbooks and theological treatises
are going out of print. The surviving professors who wrote
them, now in their eighties, no longer write new ones. Neither
do the younger men. In this sense, the theological leaders of
dispensationalism have adopted a strategy of prudent deferral.
Like those terrorized pilgrims cowering inside the perimeter of
those forever-circled wagons, they pray that Captain Jesus will
arrive before word gets out that dispensationalism is terminal.

Evidence of a Paradigm Shift

I have already discussed one of the main signs of the shift:
the quiet, unpublicized demise of dispensationalism in the

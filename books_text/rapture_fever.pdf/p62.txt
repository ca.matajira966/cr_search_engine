28 RAPTURE FEVER

State of Israel should, for some suicidal reason, attack Russia,
there is not going to be a Russian-Israeli war. (See Chapter 12.)

The failed coup placed a tombstone on top of a huge pile of
utterly inaccurate prophecies made by the leaders of popular
dispensationalism, a pile of errors that had been growing since
1917. (Actually, long before: John Cumming’s book, The End:
Ox, The Proximate Signs of the Close of This Dispensation, published
in 1855, is evidence. Lecture 7 was: “The Russian and North-
ern Confederacy.”) This tombstone’s inscription reads: “Died of
a Self-Inflicted Wound: Sensationalism.” While a dispensational
theologian today might conceivably be able to speculate about
a Russian invasion of the State of Israel a century from now, or
a millennium from now, the fact remains that the basis of the
popularity of paperback dispensational books on prophecy
(there have been no hardbacks) has always been the doctrine of
the imminent Rapture. The Rapture is just around the corner,
the faithful have been told, because Russia is building up its
military machine, and the State of Israel is simply sitting there.
Defenseless. Waiting to be surrounded by Russia. Now what?

Today, Russia is being surrounded: by seceding republics.
What possible incentive does a military confrontation with the
State of Israel offer anti-Communist Russian leaders today, now
that expansionist Soviet Communism is deader than a doornail?
Even if a military autocracy takes over in what is now the Rus-
sian republic, what threat would this pose to the State of Israel?
What would be the incentive for a military junta to engage in a
distant military confrontation with Israel and the United States?
What would be the payoff? The Soviets in 1990 and 1991 used
Jewish emigration to the State of Israel as a pressure-release
valve. Why would any military junta want to close off this valve?
Why would a junta want to create Jewish resentment within
Russia and worldwide opposition against Russia?

Three and a half years before Russia surrounds the State of
Israel, dispensational laymen have been publicly assured for
over seven decades, the Rapture will pull all Christians out of

Call No Man Your Father 81

ers by securing their homes against theft, developing neighbor-
hood watch groups, and taking courses in self-defense. The “citi-
zen’s arrest” is also an aspect of self-government. A police State is
difficult to generate if the people take responsibility for their ac-
tions and do not look to the State as the only protecting agency.
Big Brother’s influence increases when we fail to watch out for
ourselves and the well-being of our neighbors.

Of Limits

A careful reading of Scripture will show that the State, or civil
government, has almost no authority in the areas of education,
business, welfare, and ecclesiastical affairs. For example, the only
time education of children is taken on by the State is when the
people of God are held captive by a pagan government (Daniel
1:1-7). Jurisdiction of education is given to parents (Deuteronomy
6:4-9). A legitimate educational function of the State would be
military academies.

Caring for widows is another concern. The responsibility first
lies with family members. This is why Paul makes a distinction
between “widows indeed,” or those who have no family or who
have families that refuse to care for them, and widows who have
family members who can help out: “Honor widows who are wid-
ows indeed; but if any widow has children or grandchildren, let
them first learn to practice piety in regard to their own family, and
to make some return to their parents; for this is acceptable in the
sight of God, Now she who is a widow, indeed, and who has been
left alone has fixed her hope on God, and continues in entreaties
and prayers night and day” (1 Timothy 5:3-5). The church is to
step in and help those widows “who are widows indeed.” Their
hope is fixed “on God.” The church is God’s special representative
on earth, God's provision comes through the church in tithes and
offerings of the people. The State can best help widows by not tax-
ing inheritances.

Some of Jesus’ harshest words are for children who put their
parents’ lives in jeopardy. He pronounces the death penalty on
unfaithful and self-righteous children who neglect caring for their

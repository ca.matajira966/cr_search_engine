84

Ruler of the Nations

. A servant to God is a free man spiritually.

. A servant to Satan is a slave spiritually.

. Slavery is Satan’s imitation of servitude to God.

. Modern men are increasingly slaves to the State.

. Sin is the first step toward slavery.

. Samuel warned Israel against the kingly State.

. The State’s job is limited: bearing the sword (vengeance).
. It is to dispense God’s justice: punishing criminals.

. It is to enforce honest weights and measures.

. It is to defend Christianity from public attack.

. It is to defend the nation from invasion.

. It is to impose medical quarantines.

. It is to define and protect private property.

. It has little authority over other spheres of government,
especially in the area of welfare.

86 Ruler of the Nations

Even nations outside Israel were required to follow the law as
it was given to the nation Israel. This is a controversial statement.
If the nations of the Old Testament world were supposed to obey
God’s civil law, then it becomes more difficult to argue that na-
tions of the New Testament world are not under the same obliga-
tion. But what is the evidence that nations in the Old Testament
were to be governed by God’s law? The best evidence is this: God
judged them.

Sodom and Gomorrah were destroyed because they broke the
law of God (Genesis 13:13).

God commanded the prophet Jonah to preach to the Ninevites
(Assyrians) because their wickedness had come up before God
(Jonah 1:2). The reason is clear: “There shall be one standard for
you; it shall be for the stranger as well as the native, for I am the
Lorp your God” (Leviticus 24:22).

The prophet Amos set forth the coming judgment of God to
Damascus, Gaza, Tyre, Edom, Ammon, and Moab. These non-
Israelite nations stood accountable for their transgressions: “For
three transgressions . . . and for four I will not revoke its punish-
ment” (Amos 1:3, 6, 9, 13; 2:1). Non-Israelite nations were to be
judged along with Judah and Israel (Amos 2:4, 6). There is one
law and one Lawgiver. The One who gives the law is the sover-
eign Lord of history—all of history, not just Israel’s history and
not just the church’s history.

The Law Established

The New Testament shows a similar emphasis, as we should
expect, The God of the New Testament is the God of the Old Tes-
tament. We must not adopt a “two gods” view of history, with a
mean, evil, tyrannical god in the Old Testament, and a sweet,
kind, “devil may care, but I don’t” god of the New Testament.
God does not change (Malachi 3:6); therefore, His law does not
change (Matthew 5:17-20),

Though Christians do not make blood sacrifices as remission
for sins, we do keep this Old Testament law in Christ. We take
holy communion, in which the wine becomes the sacrificial equiva-

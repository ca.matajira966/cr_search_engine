Editor's Introduction xi

literally spoke the universe into existence (Genesis 1), men cannot
do this. Their fiat (spoken) word is not powerful. They need tools
to enforce their words—levers, if you will. They need leverage.

How do they get it? By gaining the cooperation of others. How
do they gain it? By many means, but they all boil down to two:
the carrot and the stick. They buy cooperation, or they compel it.
In a free market society, the accent is on buying it; in a socialist
(command) society, the emphasis is placed on forced cooperation.

Mankind lives on, but men die. So how does a system of gov-
ernment survive the founders? How do people on top remain on
top, and how do their successors remain on top? Who chooses
their successors? How do those on the bottom get on top?

These are the basic questions of all organizations, and all gov-
ernments. They are asked over and over in the history of man.
‘The answers vary in details, but there are not many different
kinds of answers.

The Covenant Structure

To get the right answers, we need first to ask the right ques-
tions. For a long, long time, Christians and Jews have had the
right questions right under their noses, but no one paid any atten-
tion. The questions concerning lawful government are organized
in the Bible around a single theme: the covenant.

Most Christians and Jews have heard the word “covenant.”
They regard themselves (and occasionally even each other) as
covenant people. They are taught from their youth about God’s
covenant with Israel, and how this covenant extends (or doesn’t)
to the Christian church. Everyone talks about the covenant, but
until late 1985, nobody did anything about it.

Not in 3,400 years of Bible commentaries.

Is this too strong a statement? It may be. But the fact remains,
if you go to a Christian or a Jew and ask him to outline the basic
features of the Biblical covenant, he will not be able to do it rapidly
or perhaps even believably. Ask two Jews or two Christians who
talk about the covenant, and compare the answers. The answers
will not fit very well.

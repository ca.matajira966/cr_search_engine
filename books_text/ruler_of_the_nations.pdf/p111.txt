Jesus Alone Possesses Divine Rights 2

Jesus Christ is acknowledged as King and its citizens embrace
Him as such personally.

The Bible emphasizes that all people are Christ’s subjects in a
variety of ways: First, Jesus speaks to Nicodemus about the neces-
sity of a “new birth,” a comprehensive transformation of the entire
individual (John 3:5-7), Man is not considered “sick.” He is con-
sidered sinful. The unregenerate sinner is “dead in trespasses and
sins” (Ephesians 2:1). Only the regenerating power of the Holy
Spirit can make a dead man live. The fundamental issue is ethi-
cal, not medical or psychological.

Second, the written Word of God is acknowledged as the only
tule of faith and practice. Law is not found in the vote of the peo-
ple, the decree of the courts or the pronouncement of rulers. The
law of God is Christ the King’s law; therefore, it must be obeyed.
Moreover, being set free in Jesus Christ liberates neither citizen
nor ruler from the guidance, obligations, and benefits of the law
(Romans 3:31), only from the final curse of the law (Galatians
3:13). The consequences of broken law are well established (Deu-
teronomy 28:15-68).

Third, the regenerate mind is renewed (Romans 12:2). Every
individual operates from a particular view of the world in mind,
and evaluates all life from this perspective, meaning his chosen re-
ligious presuppositions. Prior to acknowledging Christ as King,
all life is seen from man’s perspective: “For as he [man] thinks
within himself, so he is” (Proverbs 23:7). The new creature in
Christ should evaluate life from the perspective of the Word of
God, thinking God’s thoughts after Him, “taking every thought
captive to the obedience of Christ” (2 Corinthians 10:5).

Fourth, those who do not want Jesus as King “will wage war
against the Lamb, and the Lamb will overcome them, because He
is Lord of lords and King of kings, and those who are with Him
are the called and chosen and faithful” (Revelation 17:14).

Separate Jurisdictions
The Bible reveals that the jurisdiction of the State and the jur-
isdiction of the church are to be separate, though the separation is

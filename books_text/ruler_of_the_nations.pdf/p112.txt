92 Ruler of the Nations

not absolute. The Word of God transcends any absolute wall of
separation some may seck to erect. For this reason, civil servants
often are given religious titles. The king in Israel was the Lord’s
anointed set apart for a ci task in the same way the priest was set
apart and anointed for his ecclesiastical (religious) task (Numbers 3:3).

David, who was to replace Saul as the Lord’s anointed,
respected the special office of the king: “Far be it from me because
of the Lorn that I should do this thing to my lord [Saul], the
Lorp’s anointed, to stretch out my hand against him, since he is
the Lorp’s anointed” (1 Samuel 24:6).

Those subordinates who served with David also enjoyed relig-
ious titles. His chief officers were called priests (2 Samuel 8:18).
These were not temple priests or Levites; they were comparable
to ministers of Romans 13:4. The reason for the title “priest” is not
immediately evident until we understand that these “priests,” in
their governmental role, were to give counsel to the king. Only the
title of “priest” was significant enough to give these counselors’ role
the importance it deserved: they were to counsel the civil minister
in godly law and actions, The New Testament emphasizes a simi-
lar title for all rulers by designating them as “ministers of God”
(Romans 13:4, 6).

King of All the Nations

Even outside Israel, rulers were given religious titles for civil
functions. Cyrus is given the title of “shepherd.” This title is usually
reserved for God Himself (Isaiah 40:11; cf. John i0) and the rulers
of Israel ( Jeremiah 23:4), but God calls a non-Israelite “My shep-
herd” (Isaiah 44:28): “It is 1 who says of Cyrus, ‘he is My shep-
herd! And he will perform all My desire.” Cyrus is given an even
more significant title: “Thus says the Lorp to Cyrus His anointed,
whom I have taken by the right hand, to subdue nations before
him, and to loose the loins of kings; to open doors before him so
that gates will not be shut” (45:1). The High Priest in Israel and
the King are designated in the same way. Of course, it is the title
for the coming deliverer, the “Messiah.”

Hence, the rulers of the nations are given titles which clearly

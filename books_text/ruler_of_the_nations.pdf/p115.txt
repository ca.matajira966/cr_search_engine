Jesus Alone Possesses Divine Rights 95

The law as given to Israel was a standard for nations sur-
rounding Israel. When other nations heard of the righteous judg-
ments within Israel, these nations remark with wonder: “Surely
this great nation is a wise and understanding people” (Deuteron-
omy 4:6). The psalmist proclaims to the kings and judges of the
earth “to take warning . . . and worship the Lorp with reverence”
and “do homage to the Son” (Psalm 2:10-11). Quite frequently, the
other nations are called upon in the Psalms to honor God. The
prophets insisted that the nations surrounding Israel would re-
spond to His threat of historical judgment. God does not exempt
other nations from the requirements of His righteousness. He
holds them responsible for their sins (Amos 1:3-2:5).

The New Testament Emphasis

The New Testament presupposes the moral order laid down in
what we call the “Old Testament.” John the Baptist used the law of
God to confront Herod in his adulterous affair: “Herod . . . had
John arrested and bound in prison on account of Herodias, the
wife of his brother Philip, because he had married her. For John
had been saying to Herod, ‘It is not lawful for you to have your
brother’s wife’” (Mark 6:17, 18; Leviticus 20:10; Deuteronomy
22:22). This was not mere advice. John lost his own head in the
exchange.

In Romans 13, the civil magistrate is termed a “minister of
God’ who has the responsibility and authority to punish evildoers.
As God’s servants these rulers must rule God’s way. Just as a min-
ister in the church is obligated to implement the law of God as it
touches on ecclesiastical matters, a civil servant must implement
the law of God in civil affairs. The determination of good and evil must
derive from some objective standard. In Hebrews 5:14, the Christian is
instructed to train his senses “to discern good and evil.” In
Romans 13;4 the civil authorities are to wield the sword, punish-
ing evil doers and promoting the good.

God certainly does not intend the standard of good and evil to

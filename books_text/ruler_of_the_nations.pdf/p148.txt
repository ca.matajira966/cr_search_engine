128 Ruler of the Nations

the means to clean up politics (or any other area of human activ-
ity)? If Christians do not, who will? Christians have stayed out of
politics, making its corruption even more pronounced. The an-
swer is not to consign politics to even more corruption by ignoring
its potential as an area for redemption and restoration.

The Bible never condemns political involvement. John the
Baptist does not rebuke Herod for his political position, but for his
sinful actions as ruler. Jesus does not quarrel with Pontius Pilate
over whether he should rule, but only reminds him why he rules
and, implicitly, by what standard he rules. Paul calls rulers God’s
“ministers,” servants in the political sphere (Romans 13:4). Paul
appeals to Caesar, the seat of Roman political power, in order to
gain a hearing.

The desire to retreat from political concerns is recent within
our history. John Witherspoon, a minister in the Presbyterian
church and the President of the College of New Jersey (which
later became Princeton), was a signer of the Declaration of Inde-
pendence. The framers of the Constitution, “with no more than
five exceptions (and perhaps no more than three), . . . were ortho-
dox members of one of the established Christian communions: ap-
proximately twenty-nine Anglicans, sixteen to eighteen Calvinists,
two Methodists, two Lutherans, two Roman Catholics, one lapsed
Quaker and sometime-Anglican, and one open Deist—Dr. [Benja-
min] Franklin, who attended every kind of Christian worship,
called for public prayer, and contributed to all denominations.””

Summary

The eighth basic principle in the Biblicat blueprint for civil
government is that there can be no neutrality or passivity in the
advance of God’s kingdom. It parallels Chapter Three, which
deals with Biblical law and Biblical ethics (point three of the Bibli-
cal covenant),

For many Christians, evil times are evidence that the end is

7. M. E. Bradford, A Worthy Company (Marlborough, New Hampshire: Ply-
mouth Rock Foundation, 1982), p. viii.

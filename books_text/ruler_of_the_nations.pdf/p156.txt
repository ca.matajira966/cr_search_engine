136 Ruler of the Nations

not in all ways parallel to State and church. This is why the family
cannot by itself lawfully resist the State; Biblically, only the
church has been ordained to do so. The family ought to find pro-
tection under the church.

Family-Worshipping Revolutionaries

This is one very good reason to reject the revolutionary radical
segments of the so-called “identity movement.” Members of nu-
merous radical religious groups claim that they have a lawful right
to rebel against civil authority because of their supposed link to
the ten lost tribes of Israel. They are trying to make family or
tribal authority supreme over civil authority. This is anti-Biblical
to the core. The family is not the church, nor is it an alternative to
the church. Paralleling the decline in Christianity in our day has
also been the rise of a new humanist familism which is proposed
as the only lawful alternative to the humanist State. Such a choice
is inherently unlawful and anti-Biblical. The church is the pro-
tector of the family. It is the God-ordained government that alone
can lawfully authorize and therefore legitimize family resistance
to the State.

In short, from a Biblical perspective: no church-no lawful family
resistance. Anyone who teaches otherwise is a covenant-breaker, a
revolutionary, and should be avoided. He will eventually be
smashed by the State, and any association with him will produce
unpleasant consequences. If the church is corrupt, then the State’s
tyranny is simply God’s lawful judgment, just as it was when
Assyria and Babylon carried away the Israelites. Tyranny in such
a case is a step toward repentance, revival, and reform.

What the Church Can Do

First and foremost, the church can preach. It can set forth
God’s permanent standards of right and wrong: for individuals,
families, churches, and civil government. Preaching applies God’s
standards to this world, A church that refuses to preach the whole
counsel of God has become an accomplice of evil in every area of
life not touched upon by its preaching.

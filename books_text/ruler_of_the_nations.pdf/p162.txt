142 Ruler of the Nations

God judges those who tamper with His law. There is a jurisdic-
tional separation between church and State. The Bible defines the
limits of jurisdiction, Church and State were established to cooper-
ate, not compete. Leadership in both church and State is based on
ethical considerations. Church and State are parallel governments
bound by the same law. Leaders in both church and State are given
the title “minister.” The Bible condemns jurisdictional usurpation.

God establishes multiple government jurisdictions, and there-
fore multiple hierarchies, in order to reflect His own plural
nature, but also to restrain the sinfulness of man. He brought
judgment in history against the builders of the Tower of Babel
because they proposed to build a one-world messianic State. They
wanted to give their own name to themselves, defining themselves
without reference to God, and to establish their own jurisdictions,

The criteria for serving as a judge, in church and State, is
morality. Men are not to use their offices to pursue personal eco-
nomic gain or power. They are to execute judgment as God's dele-
gated representatives. This representative character of all civil
and ecclesiastical offices is basic to every human government.

God brings His people freedom. One means to this freedom is
a system of potentially competing delegated sovereignties. When
men sin, and overstep their limits (the meaning of sin), they often
try to extend their authority over others. Parallel governments
help to reduce the extent of such lawless behavior. This is the
meaning of federalism, of checks and balances.

Most Americans are confused when it comes to church/State
relations. If they were asked to choose between the language of
the First Amendment of our Constitution and the language of the
Soviet Constitution, few could tell the difference. The critics of
Christ and His law have done a masterful job in rewriting history.
(See Chapter 13 and the Appendix for details.)

In summary:

1. All authority is delegated from God.

2. Obedience to God-authorized judges is required.

3. There is no single human authority; human authority is
always plural.

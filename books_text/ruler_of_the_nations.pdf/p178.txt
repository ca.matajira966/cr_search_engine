158 Ruler of the Nations

No, if we do our work, we will be blessed. So Satan blinds us.
He tells us we don’t have enough time to get our work done. He
persuades us that Christians’ efforts are not sufficient to please
God. He somehow has convinced a whole generation of Chris-
tians that they will leave nothing to their children and grandchil-
dren because the Church will be “Raptured” out of the world and
out of history before their children and grandchildren can inherit, or
at least put their inheritance to work. Satan has won a temporary
cultural victory by shortening the time perspectives of his enemies.

The best example that I can think of is the one Gary North
always cites. The Communists believe that an overnight revolu-
tion is the only event that can save society from evil capitalism.
But they work patiently for generations, eroding their enemies’
faith in capitalism, subverting every institution, and planning for
revolution, nation by nation.

In contrast, those Christians who have spoken for the church
historically have usually believed in the steady progress of the gos-
pel. They have opposed radical revolution. They have had faith in
the future. Nevertheless, in our day they have dreamed of mira-
cles and prayed for the Rapture to remove them from their mis-
ery. They have adopted Satan’s faith in short-run miracles, na-
tional political miracles (“We must elect a Christian leader who
will throw the rascals out, once and for all!”), and the miracle of
the Rapture. Christians have shortened their time perspective,
and Satan has convinced his followers that they have all the time
in the world.

Satan has been winning for two centuries because of this. It is
time to call a halt to this temporary period of Christian defeat.
The way we do it is to lengthen our time perspectives and then get
to work.

Summary
God tells us that it is our task to subdue the earth to His glory.
This will take the cumulative efforts of all His people. No single
generation will get all the credit. Step by step, line upon line, here
a little and there a little, His kingdom in heaven marches forward,

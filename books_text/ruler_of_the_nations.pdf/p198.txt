178 Ruler of the Nations

believed in something it would find itself in the church’s creed.
This is one of the reasons the Amish have been able, up until re-
cently, to practice their faith without the long-arm of government
interference. On the new Social Security bail-out scheme, the
Amish are exempt. Their creedal formulations and their consist-
ent practice substantiates their claims of seeking no government
aid. Prior to the new tax ruling, nearly 80% of all churches had
voluntarily chosen to pay into the Social Security System.

This is an indication that the “whole purpose of God” is not
preached from the pulpits. These church employees could have
gotten a raise (employees must remember that the employer does
not pay an equal share of Social Security. This is money that
would have gone to the employee in the form of wages. By putting
the money we now pay into private pensions and savings pro-
grams the pay off would be substantially higher than it could ever
be under Social Security. Moreover, this investment capital could
be loaned out for greater economic expansion.)

T've been told that “This is not a creed-writing age.” No age isa
creed writing age until the need for a creed arises and someone
decides to write a creed. The last great creedal formulation was in
1647 when the Westminster Confession of Faith was written. That
was over 300 years ago. A lot has happened since then. We didn’t
have statist schools, the NEA, Social Security, and the IRS. A. A.
Hodge writes about the need for creeds as a preservative from ex-
ternal and internal hostilities: “The Church is forced, therefore,
on the great principle of self-preservation, to forrn such accurate
definitions of every particular doctrine misrepresented as shall in-
clude the whole truth and exclude all error; and to make such
comprehensive exhibitions of the system of revealed truth as a
whole that no one part shall be either unduly diminished or exag-
gerated, but the true proportion of the whole be preserved.”?

The Westminster Confession of Faith, for example, says noth-
ing about Christian education or the tax question, When the State

9, A. A. Hodge, The Confession of Faith (Edinburgh: The Banner of Truth
Trust, [1869] 1978), p. 2.

180 Ruler of the Nations

ers, and no leaders without followers. Men must learn to obey be-
fore they can successfully lead. This is why Paul establishes as the
criterion for church office, elders and deacons, that the candidate
must rule his family well (1 Timothy 3). If he has not learned to do
this, he has not proven his capacity for leadership?

Does this mean that unmarried and never-married men
should not ordinarily be ordained? If we are to take seriously the
words of the apostle, this is what it means. But this also insures
that creative, disciplined men will become leaders. They will
understand the needs and problems of congregation members.
They will be far less bureaucratic in their rule, and far more
familial.

The Roman Catholic Church a thousand years ago made celi-
bacy the basis of ordination, precisely because the leaders recog-
nized that a man who worries about wife and family cannot be a
bureaucrat whose obedience can be commanded. Protestants rec-
ognized early that this mandated celibacy favored a top-down hi-
erarchical church that is unbiblical. What is needed is a bottom-
up appeals court system, with judges who understand the lives of
those under them, This means family men. To the extent that un-
married men, especially young unmarried men, are placed over
congregations, to that extent one of the major insights of Protes-
tantism is lost. If the seminary degree continues to be the major
criterion for access to the pulpit, rather than family leadership,
hospitality, and godly behavior, then the church will continue to
flounder. On this point, the Reformed tradition of an educated
ministry became the bureaucratic emphasis on academic training
and academic degrees. Thus the church has suffered from this im-
proper emphasis for over nine centuries.

Ethics/Law/Dominion

A continuing theme in this book is the idea that people must
learn to govern themselves before they govern others. But self-
government must be in terms of law. What law? Whose law? Un-
til Christians get this clear in their own minds, they will not be
able to exercise dominion in society.

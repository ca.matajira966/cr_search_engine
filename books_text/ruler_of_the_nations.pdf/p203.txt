Reconstructing Church Government 183

When disputes cannot be settled between two individuals or
among family members, the church can (must) operate as a legiti-
mate judicial authority. When individual and group confrontation
fails to bring about a just decision, Jesus commands us to “tell it to
the church” (Matthew 18:17). The church is a gifted body of be-
lievers (1 Corinthians 12-14) who have the Word of God as the final
voice of authority for all matters of faith and practice (2 Timothy
3:16, 17).

If the church is not equipped to handle disputes then what in-
stitution is?

While Paul discouraged lawsuits between believers, he did es-
tablish the principle that the people of God are capable of handl-
ing disputes among the brethren: “Does any one of you, when he
has a case against his neighbor, dare to go to law before the un-
righteous, and not before the saints?” (1 Corinthians 6:1).

Inheritance/Legitimacy/Continuity

Establish an educational center at your church. Most families
cannot afford to build a substantial library. And most Public
Libraries will not carry the books you'll want to read. Start a book
fund to purchase hard-to-find books. A room should be set aside
for study. Tapes, magazines, newsletters, and Bible courses can
be made available.

Your church could work with Christian schools in the area and
set up teacher enrichment programs. Why send your teachers
back to the State schools for indoctrination? Develop your own
program and funnel any tuition money that would have gone to
the colleges back through the church’s study center.

If your church doesn’t have a Christian school, and there isn’t
a good one in the area, help to get one started. When modern-day
church-schools are harassed by the State today, the State sees no
such creedal prescription. There is nothing written that expresses
the convictions of the denomination. It’s interesting to note that
independent churches are most susceptible. Their “No creed but
Christ” rhetoric gets them into trouble. Moreover, their in-
dependency leaves them vulnerable. Do you think the State

190 Ruler of the Nations

our children as He deals with us: “But when we are judged, we are
disciplined by the Lord in order that we may not be condemned
along with the world” (1 Corinthians 11:32). This verse appears in
the middle of the section on self-judgment prior to taking the
Lord’s Supper in church. “But if we judge ourselves rightly, we
should not be judged” (v. 31). Self-judgment should replace exter-
nal judgment as we mature in the faith, It is the substitution of
self-judgment for external judgment that is the mark of maturity,
and therefore the mark of freedom under Christ. This is why
spankings no longer work with older children who have not learned
self-government, and why church officers and even civil officers
need to be called in to judge chronic juvenile delinquents (Deuter-
onomy 21:18). This is why the family is not an independent insti-
tution, but a dependent one. But it was never intended by God
that it be dependent solely on the State.

Inheritance/Legitimacy/Continuity

Fifth, the family is to care for its own members (1 Timothy
5:8). Rita Kramer writes, “We have asked institutions — the gov-
ernment, the schools— to undertake to direct human nature. The
paradox is that as our aims have become increasingly humanitar-
ian, our means have become increasingly controlling.”* If we con-
trol our own families, we must take responsibility for family
affairs and not turn over family government to another family, or
to the church, the school, or the State. Too many families will-
ingly sacrifice their children to such statist institutions as the pub-
lic schools, day care centers, and welfare agencies.

The family must be the child’s first school. The family must
provide basic Christian teaching rather than turning all education
over to the church or the school. This means daily instruction in
the Bible, prayer, and worship. School and church should supple-
ment these family activities, not replace them. Parents must be
overseers of their children’s education. North writes, “[E]ducation
is the moral responsibility of parents, They are the ones who must

4. Rita Kramer, In Defense of the Family (New York: Basic Books, 1983), p. 19.

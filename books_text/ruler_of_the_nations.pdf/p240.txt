220 Ruler of the Nations

53,000 pages of fine print each year. Harvard legal scholar Harold
Berman has written that the rapid escalation of administrative law
in the West since 1900 threatens the West’s whole tradition of law,
and represents a major threat to liberty.?

The agencies have also created a new bureaucratic office
called the “administrative law judge.” It has the ring of authority
about it. In fact, this officer is a paid employee of the particular
federal agency involved in the dispute with citizens or organiza-
tions. This paid hireling of the agency renders his decision con-
cerning the lawfulness of the bureaucracy’s decision. This office
should have its name changed. It should be made legal for any cit-
izen to appeal administrative rulings directly to the civil courts,
without having to take the case first through the time-consuming
bureaucracy.

In effect, government bureaucracies have become legislators,
judges, and executive enforcers. This violates the principle of fed-
eralism: checks and balances. Only one thing will roll back their
power: a drastic reduction in their funding. An increase of agency
funding is a form of judgment, a reward for good service. It is
easier for politicians to increase spending if voters have forgotten
how much government is costing them. Therefore, Gary North
recommends two very simple administrative changes in govern-
ment funding. First, require the taxes for any level of civil govern-
ment to be due the day before elections. Today, due dates for
taxes are deliberately scheduled seven months earlier than voting
(April vs. November). The second simple administrative change
would involve returning to the tax collection system that prevailed
in the United States before World War II: no compulsory with-
holding of anyone’s income during the year by the government.
Each person must pay his taxes out of his own savings on tax day.
He must learn to budget for himself.

The imposition of these two simple administrative changes
would produce the greatest tax revolt since the American Revolu-

2. Harold J. Berman, Law and Revolution: The Formation of the Western Legal
Tradition (Cambridge, Massachusetts: Harvard University Press, 1983), Intro-
duction.

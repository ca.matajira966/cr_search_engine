222 Ruler of the Nations

abolished for the same reason, The State must not tax any
person’s family inheritance.

Because debt longer than seven years is prohibited by the
Bible (Deuteronomy 15), Christians must on principle always vote
against any bond issue that carries a debt repayment schedule
longer than the Biblical limit of seven years. But all bond issues
are long-term debt contracts. This means: vote no on all bond issues,
no matter how “good” the purpose of the bonds. We must not rob
our children’s inheritance.

Register to vote. Voting is the way that Christians can affect
the nature of the continuity of civil government. Your vote
counts. As humanist voters’ apathy increases, Christians have a
better chance of capturing many political offices.

© In 1645, one vote gave Oliver Cromwell control of England.

© In 1776, one vote determined that English, not German,
would be the American language.

© In 1845, one vote brought Texas into the Union.

® In 1923, one vote gave Hitler control of the Nazi Party.

© In 1948 in Texas, Lyndon B. Johnson was elected to the
U.S. Senate by 87 votes out of 988,295 votes cast in 6,000
precincts. That figures out to be 1/69th of a vote per precinct.

© In 1958, six congressmen were re-elected to the U.S. House
of Representatives by less than one vole per precinct.

In 1960, John F. Kennedy defeated Richard Nixon by only
13,000 popular votes— one-half vote per precinct.

In 1980, John East of North Carolina won the U.S. Senate
seat by one vote per precinct.

The Christian community can make a difference if it will only
vote: “According to Sacramento, California political consultant
Wayne Johnson, 56 per cent of the evangelicals gave Jimmy Carter
their votes in 1976, 56 percent gave their vote to Ronald Reagan
in 1980, and 75 to 81 per cent gave their vote to President Reagan
in 1984,”3

3. Candidates Biblical Scorecard, 1986 Edition, p. 27.

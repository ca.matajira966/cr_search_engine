226 Ruler of the Nations

neighborhood, “churches have places for dances and sports events.”
Religion is trivialized. In another textbook, a “Puritan” church is
not described as a center of religious life but rather as a center for
asummer piano festival. For the secularists, religion is evolution-
ary. There was a time when people were religious. But now that
we've come of age, we no longer need religion. Religion, in effect,
is a projection of man’s primitive past. Modern man can do with-
out the superstitions of religious belief. What used to be places of
worship are now nothing more than entertainment centers. The
Enlightenment lives!

The depiction of religious life in America doesn’t seem much
different from that of religious life in the Soviet Union. Some of
the churches in the Soviet Union are used as museums. For the
most part, only the elderly are “religious” and go to church. The
children are indoctrinated to believe that the State is the greatest
good.

The report by Dr. Vitz is a frightening reality. George
Orwell's “Ministry of Truth,” where the past is discarded down the
“memory hole,” is no longer fiction. Orwell understood the impor-
tance of the past. He wrote the following in his prophetic novel
1984: “Who controls the past, controls the future: who controls the
present controls the past.” Our job is to make Christianity the
standard around the world. No discussion ought to take place un-
less the Biblical perspective is first discussed. This will mean gain-
ing a better knowledge of the Bible and the development of skills
to use the Bible. Further, it means gaining a knowledge of our rich
Christian past, a past that is being cleverly and boldly written out
of our history books.

The present educational establishment wants to bury the past
so our children have no way of comparing the past with the secu-
larists’ version of history and their aspirations for the future. This
tactic eliminates discussion and conflict. It’s time that our nation
is reintroduced to the past. As Christians, we must not remain
silent as nearly every vestige of Christianity is being removed from
public life: from the gospel being denied in Public (government)
schools to the removal of signs that the Salvation Army put on
buses in Fresno, California that read: “God bless you.”

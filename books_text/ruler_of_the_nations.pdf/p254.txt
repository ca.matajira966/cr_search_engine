234 Ruler of the Nations

politically. So he covered his tracks. He hid behind a smoke screen
of false concern over religious integrity and a free conscience,

This was a smart tactic. Baptists were not part of any State re-
ligious establishment. They resented the fact that they had to pay
taxes that went to support State churches—a reasonable resent-
ment in retrospect, but not a commonly shared opinion in 1802,
anywhere on earth. So he appealed to their sense of injustice. He
understood their fears. He wrote:

Believing with you that religion is a matter which lies solely
between man and his God, that he owes account to none other for
faith or his worship, that the legislative powers of goverrament
reach actions only, and not opinions, I contemplate with sover-
eign reverence that act of the whole American people which
declared that their legislature should “make no law respecting an
establishment of religion, or prohibiting the free exercise
thereof,” thus building a wall of separation between church and
state.

‘That “wall of separation” language appealed to what was then
a small religious sect that was discriminated against, the Baptists.
Fifty years later, they had become the dominant Protestant group
numerically, as they remain today. Jefferson, the theological infi-
del, wanted nothing more than to get Christians out of his hair
politically. So, in effect, he offered them a political deal: you get
out of my hair politically, and I will get out of your hair ecclesias-
tically.

This deal was repeated again and again in U.S. history. It
rested on a myth, the myth of neutrality. It rested on another
myth, the myth of natural law. It rested on the greatest political
myth in modern history: the separation of God and State. The
infidels spoke of the separation of church and State, but what they
were after was the separation of God and State, the separation of
God!s law and State, and (if they could achieve it), the separation
of Christians and State. They wanted the Christians to disenfran-
chise themselves voluntarily, and to achieve this, they invented a
new slogan, the separation of church and State. It worked, too,

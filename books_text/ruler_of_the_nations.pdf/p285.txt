What Are Biblical Blueprints? 265

The kingdom of God includes every human institution, and every aspect of
life, for all of life is under God and is governed by His unchanging princi-
ples. All of life is under God and God’s principles because God in-
tends to judge all of life in terms of His principles.

In this structure of plural governments, the institutional churches
serve as advisors to the other institutions (the Levitical function),
but the churches can only pressure individual leaders through the
threat of excommunication. As a restraining factor on unwar-
ranted church authority, an unlawful excommunication by one
local church or denomination is always subject to review by the
others if and when the excommunicated person seeks membership
elsewhere. Thus, each of the three covenantal institutions is to be
run under God, as interpreted by its lawfully elected or ordained
leaders, with the advice of the churches, not the compulsion

Majority Rule

Just for the record, the authors aren’t in favor of imposing
some sort of top-down bureaucratic tyranny in the name of
Christ. The kingdom of God requires a bottom-up society. The
bottom-up Christian society rests ultimately on the doctrine of
self-government under God. It’s the humanist view of society that
promotes top-down bureaucratic power.

The authors are in favor of evangelism and missions leading to
a widespread Christian revival, so that the great mass of earth’s
inhabitants will place themselves under Christ’s protection, and
voluntarily use His covenantal principles for self-government.
Christian reconstruction begins with personal conversion to
Christ and self-government under God’s principles, then spreads
to others through revival, and only later brings comprehensive
changes in civil law, when the vast majority of voters voluntarily
agree to live under Biblical blueprints.

Let’s get this straight: Christian reconstruction depends on
majority rule. Of course, the leaders of the Christian reconstruc-
tionist movement expect a majority eventually to accept Christ as
savior. If this doesn’t happen, then Christians must be content
with only partial reconstruction, and only partial blessings from

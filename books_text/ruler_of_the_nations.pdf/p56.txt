36 Ruler of the Nations

judges. Thus, there is liberty at the individual level, but there
is also a court system for achieving peaceful settlements of dis-
putes. This is the same sort of appeals system that Christ estab-
lished for the settling of disputes in the church (Matthew
18:15-20).

God therefore provided a hierarchy for them, but a bottom-up
hierarchy. He gave them His law (Exodus 18:20). He made right-
eousness the fundamental principle of holding office: “Moreover
you shall select from all the people able men, such as fear God,
men of truth, hating covetousness; and place such over them as to
be rulers of thousands, rulers of hundreds, rulers of fifties, and
rulers of tens” (v. 21).

The principle of Biblical representative government stems
from this Old Testament innovation. The primary issue is ethics
—righteousness. Secondary to this is competence, but always sec-
ondary: Paul said that it is better to be judged by the least com-
petent person in the church than by an anti-Christian civil judge
(1 Corinthians 6:4).

The representative is under God and sworn to uphold God’s
law. He represents men before God and God before men. He
speaks in God’s name. This is the meaning of all government.
Someone on earth must speak in God’s name. The Biblical test of
the speaker’s authority is his lawful ordination; the test of his con-
tinuing right to ordination is his faithfulness to God’s Word. If he
fails this test, he is to be removed from office by the people he
serves and judges.

All authority is God-given. God grants original authority to
the ruled to choose who will rule over them. He therefore also
grants authority to the rulers. Ultimately, if people refuse to obey
rulers, they can topple any system of government. The people are
sovereign, but they are not originally sovereign. Only God pos-
sesses original sovereignty. A constitutional republic best reflects
this dual grant of civil authority: from God to men in general and
to specific rulers.

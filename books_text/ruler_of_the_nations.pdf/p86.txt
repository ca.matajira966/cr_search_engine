66 Ruler of the Nations

does not challenge the God of the Bible unless he is suicidal. Man
knows who God is, Paul writes in the first chapter of Romans, but
man rebelliously worships the creature rather than the Creator
(vv. 18-23). So when men act as though God is dead, they call
God’s wrath down on them. They commit suicide. On the day
that Adam ate of the tree of the knowledge of good and evil, he did
surely die, and killed his posterity with him.

If we do not preach to men that God judges individuals,
groups, and nations in Aistory then we are adopting the false doc-
trine that God has abdicated His office and has stepped down
from His throne. He may step back to His throne on judgment
day, or sometime during an earthly millennium in which Jesus
physically returns to rule visibly on earth, but until then, we are
saying, “There is no heavenly ruler of the nations.”

This leaves the office of judge open and the throne empty.
Guess who will rush to the empty throne? Man. More specifically,
power-seeking elitists who want to play God.

There must always be judgment in history. The question is:
Whose? God’s or man’s?

Summary

We have to affirm the Trinity. We have to affirm the equal
ultimacy of the One and the Many. We have to affirm the reality
of individuals and collectives. We therefore have to affirm the
reality of God’s judgments in history against individuals and col-
lectives. This is the message of the covenant.

Sodom and Gomorrah were judged in the midst of their sins,
in the midst of history. God sent no prophet to warn them. ‘They
were responsible before God for their sins, and in the midst of
prosperity, the day of doom came upon them.

God judges collectives. He will sometimes spare an evil collec-
tive for the sake of a few righteous people. But eventually judg-
ment comes in history, and the righteous minority suffers. God
judges collectives as collectives.

‘The nations have broken God’s covenant and have transgressed
His law, Isaiah warned. Thus, they are always ripe for historic

78 Ruler of ihe Nations

and measures to be “violence and destruction” (Ezekiel 45:9),
Men are not permitted even to own false weights and measures:
“You shall not have in your bag differing weights, a large and a
small. You shall not have in your house differing measures, a
large and a small” (Deuteronomy 25:13, 14). Again, a Biblical
monetary system can only work when the people are self-gov-
erned, when they do not cheat, pilfer, falsely advertise, or appeal
to their civil representatives to create laws that favor them in their
business dealings.

When civil governments abandon basic Biblical laws relating
to economics, the people suffer. Inflation of the currency, by aban-
doning gold and silver as the monetary standard and “creating”
money to fund non-Biblical political projects, especially hurts the
poor: “Your silver has become dross, your drink diluted with
water. Your rulers are rebels, and companions of thieves; every one loves a
bribe, and chases after rewards. They do not defend the orphan, nor does the
widow's plea come before them” (Isaiah 1:22, 23).

Defending Christianity from Public Attack

Third, the Christian religion should be protected against those
who would seek to destroy it. The State cannot be neutral toward
the Christian faith. Any obstacle that would jeopardize the
preaching of the Word of God and carrying out the Great
Commission must be removed by civil government. Civil rulers
should have the interests of “godliness” and “dignity” in mind as
they administer justice. This is why Paul instructs Christians to
pray for their rulers, so “that we [Christians] may lead a tranquil
and quiet life in all godliness and dignity” (1 Timothy 2:2).

Many wish to maintain that the State must be religiously and
morally neutral. The Bible makes no such suggestion, Even our
Constitution assumes the protection of the Christian religion. The
First Amendment had the specific purpose of excluding all rivalry
among Christian denominations. Paul expected even the Roman
civil government to protect him from those who threatened the
Christian religion (Acts 23:12-31; ef. 25:11). This means civil gov-
ernment cannot be religiously neutral. If the Christian religion is

6

WHO SHOULD CONTROL
DISTRIBUTION?

And he causeth all, both small and great, rich and poor, free and
bond, to receive a mark in their right hand, or in their fore-
heads: And that no man might buy or sell, save he that had the
mark, or the name of the beast, or the number of his name
{Revelation 13:16-17).

Who should control distribution? This question has divided
economists almost from the beginning. We need to ask our-
selves several questions. Which people should we trust? State
bureaucrats who can rarely be fired if they price things incor-
rectly? Or producers who lose their own money if they price
their output incorrectly? Which group possesses greater power
to coerce consumers, a monopolistic government bureaucracy
or competing producers? Which group poses the greatest threat
to our freedom? Which group is more likely to play God? In
other words, who should be trusted by consumers: profit-seek-
ing producers or Civil Service-protected bureaucrats?

The Unity of Production and. Distribution

One of the most important errors in economics is to imagine
that it is possible to separate free market production from

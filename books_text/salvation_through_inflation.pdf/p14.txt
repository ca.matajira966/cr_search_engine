xiv SALVATION THROUGH iNFLATION

The Lure of the Bizarre

This is why Christians often revel in ideas that members of
the Establishment’s humanist culture regard as utterly bizarre.
Their willingness to embrace the culturally and intellectually
bizarre becomes their point of vulnerability. There are many
bizarre fringe groups within the humanist camp. Leaders inside
the Christian camp can and do import very strange ideas from
these humanist fringes, ideas that have nothing to do with the
Bible but which can be wrapped in the swaddling clothes of
conservative religion. In this form, these imports can and have
deceived many. One such import is Social Credit.

Because the importers are Christians, the typical disciple
does not expect them to be familiar with the details of academ-
ic, scientific, and intellectual matters. Here is the problem:
because the Christian importers are intellectually unable to
recognize the nature of the deception when they see it, they
pass along its errors to their followers in the name of their own
anti-Establishment commitment. They do not perceive that they
have been deceived by humanists, occultists, and charlatans from outside
the camp. They say: “Weren't the originators also persecuted by
the Establishment? Didn't they also suffer for their beliefs?
Well, then, let Christians also warmly receive these new ideas!”
Result: Christians rarely recognize that these ideas are very
often ancient errors that had once been the common coin of
the realm within the humanist camp, but which fell out of favor
when they were found to be incorrect and, more to the point,
totally unworkable.

A growing minority of Christians have grown weary of their
status as political observers and victims. They have become
restless. They want to make a difference in history. But they
have no experience in “worldly” matters. They are aware of no
biblical working models, no Bible-based blueprints for social
transformation. But they are no longer content to serve as
doormats, paying taxes to finance their spiritual enemies’ end-
less State-funded projects. So, they begin to listen to those who

142 SALVATION THROUGH INFLATION

Jesus used the parable to describe how much we owe to God
for His grace to us. Jesus used an economic parable to drive
home His point: on judgment day, God will expect much from
us if He has given much to us. As He said elsewhere: “And that
servant, which knew his lord’s will, and prepared not himself,
neither did according to his will, shall be beaten with many
stripes. But he that knew not, and did commit things worthy of
stripes, shall be beaten with few stripes. For unto whomsoever
much is given, of him shall be much required: and to whom
men have committed much, of him they will ask the more”
(Luke 12:47-48).

Jesus was not opposed to money-lending as such. He was not
opposed to banking and interest. He was not opposed to high
profits. After all, the good servants in the parable had made
100% on their investment of the master’s money (Matt. 25:20,
22). What He was opposed to was servants who do not increase
the talents which God has entrusted to them.

A Bankers’ Conspiracy!

Major Douglas warned that “if the population of this or any
other country is willing to allow the mechanism of money to be
controlled by the few, then, so long as inducement by money is
the basis of credit, so long will the few control the many.”! This
was a major feature of his critique of capitalism: a system of
money creation that places power in the hands of the few. This
diagnosis appeals to those people who want to view history as a
battleground between “the people” and “the conspiracy.”

This view of history is at bottom false. There are always
conspiracies competing for men’s allegiance, but at the heart of
any society is never a conspiracy. The heart of any society is the
religious worldview of the people, whose allegiance is so impor-
tant for conspirators. Some conspiracy or group of conspiracies
may seek to represent the people. A conspiracy for a time may

1. Credit-Power and Democracy (London: Cecil Palmer, 1920), p. 62.

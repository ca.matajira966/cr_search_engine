A False Prescription 157

The allocation of seats is not made by the tickets. The alloca-
tion is made by the auction process for tickets. Contrary to
Douglas, it is the tickets’ money-value (as Douglas put it) which
performs the task of allocating scarce seats for the performance.

A ticket is a legal claim to a specific item. The economic
value of that item is not established by the ticket; it is estab-
lished by supply and demand. Douglas saw each unit of money
as a ticket. This is incorrect: money is not a ticket; rather, it is
the most marketable commodity. A ticket entitles its holder to
whatever the ticket promises. It is a legal claim. Its value is
determined by the value of the thing legally represented by the
ticket. In contrast, fiat money entitles its holder to nothing.
Economically speaking, it is not a claim to anything. Its value is
indeterminate until the seller of some good accepts the buyer’s
money bid. A unit of money is a means of bidding at the auc-
tion, not a legal claim on anything offered for sale.”

I Want Out!

Consider that same auditorium. Instead of the nation’s most
popular entertainer or group, someone ~ not very skilled entre-
preneurially — decides to schedule a debate on the economics of
Social Credit. How much will each of the 5,000 tickets be
worth? Not very much. In fact, to fill all 5,000 seats on the
night of the debate, the sponsoring organization would proba-
bly have to pay people to attend. The fact is, most people
would have to be compensated financially to keep them in their
seats during a debate on Social Credit. They want out.

What is the difference between the value of the tickets in the
first example and their value in the second? The seats are
physically the same. The tickets may be the same color. But
nobody wants to pay for them in the second example. What is
the economic difference? Answer: the general desirability of

23. A warehouse receipt (IOU) can function as money only because the item
against which the receipt is a legal claim is money.

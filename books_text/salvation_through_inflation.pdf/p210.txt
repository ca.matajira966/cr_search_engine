178 SALVATION THROUGH INFLATION

tual agreement. Second, a wage is not a form of servitude. A
wage is legal payment for agreed-upon services rendered. The free
market economist teaches this. So does the Bible.

The Parable of the Righteous Employer

In the parable of the just employer, Jesus defended the
principle of voluntary contracts for labor. A farmer hired men
in the morning to work at an agreed-upon wage. He hired
other laborers later in the day. He paid them all the same daily
(not hourly) wage, which he and they had agreed upon. At the
end of the day, he paid them exactly what they were due. This
payment was a legal entitlement on their part. But those workers
who had worked longer complained. They wanted more than
what they had agreed to in the morning.

But when the first came, they supposed that they should have
received more; and they likewise received every man a penny.
And when they had received it, they murmured against the
goodman of the house, Saying, These last have wrought but one
hour, and thou hast made them equal unto us, which have borne
the burden and heat of the day. But he answered one of them,
and said, Friend, I do thee no wrong: didst not thou agree with
me for a penny? Take that thine is, and go thy way: I will give
unto this last, even as unto thee. Is it not lawful for me to do
what I will with mine own? Is thine eye evil, because I am good?
(Matthew 20:10-15).

The employer's answer was to the point: “Didst not thou
agree with me for a penny? Take that thine is, and go thy way.”
The employer was a free man. He was under no moral or legal
obligation to pay them more. Nevertheless, their pay was theirs.
It belonged by law to them. This was not servitude.

Douglas Confused Dividends With Interest

Having completely misrepresented wages as a form of servi-

182 SALVATION THROUGH INFLATION

goods by creating too few tickets (money): underconsumption.
Meanwhile, capitalism does not actually produce all the con-
sumer goods possible, leaving us to condemn it for underpro-
duction.

If you are a banker, and you have loaned me money, do you
want me to make a lot of money or to become impoverished?
Which people do bankers prefer to make loans to: poor people
who may not be able to repay the loans or successful people
who will repay and then borrow again? The answer is obvious:
bankers usually loan money to successful people who are ex-
pected to do well, not to people who are getting poorer year by
year. Sadly, this motivation of bankers was not discussed by
Major Douglas, as far as his books indicate.

Is capitalism really this paradoxical? Or was Major Douglas
totally confused? I vote for the latter possibility.

Conclusion

Major Douglas confused wages with dividends, and on this
basis predicted that dividends would steadily replace wages as
the source of income: “[T]he dividend is the logical successor to
the wage, carrying with it privileges which the wage never can
have. .. .”” The dividend was his version of the welfare dole.

He also confused dividends with interest, blaming the sup-
posed concentration of power in banking and the trusts with
their control over credit. He did not understand that interest is
the payment for time, wages are the payment for labor, rent is
the payment for land, and profits are a residual that will re-
main if an entrepreneur has correctly forecast future consumer
demand and has organized production on an effective, low-
waste basis. Profits are not automatic; dividends are not auto-
matic; and both are low most of the time. Profits are rarely
above a ten percent return on invested capital most of the time.

Douglas used Marx-like arguments regarding the exploita-

+27. Credit-Power and Democracy, p. 43.

Social Credit Means State Monopoly Credit 187

ysis is this equation: society = State. This is why Social Credit
economics is not conservative,

There are few propositions more hostile to conservatism
than the “society = State” formula. From the days of Edmund
Burke, England’s social philosopher and member of Parliament
in the late eighteenth century, until today, conservatives have
maintained that society is much more than the State, Society is
a vast association which includes families, churches, voluntary
associations of all kinds, businesses, etc. And from the days of
the French Revolution, which Burke so eloquently opposed,
radicals and socialists have insisted that society is the State, that
the State must exercise ever-increasing control over all these
social organizations.

There are very few conservative professors of sociology in
today’s world, but the most famous of them, Robert Nisbet
(under whom I studied), made his position plain: “In the first
place, State and society must be sharply distinguished.” It és the
essence of totalitarianism that the State gains control over the institu-
tions we call social. Any move at this late date in history toward
the expansion of State power in order further to empower the
State at the expense of society is a move away from freedom.
Any attempt to make State bureaucrats the monopolistic eco-
nomic agents of individuals in the quest for a better, more
productive economy is one more step toward the triumph of
the State over individuals and the social groups they belong to.
Yet there are people who call themselves conservatives today
who promote the creation of just such a State monopoly: sup-
porters of Social Credit. Even more astounding, they promote
this in the name of the consumer.

Consumer sovereignty is undermined by almost every trans-
fer of power over the economy to State bureaucrats. Taking
money away from citizens and placing it in the hands of politi-

4. Robert A. Nishet, The Quest for Community (New York: Oxford University Press,
1954), p. 99.

Social Credit Means State Monopoly Credit 191

and a very Machiavellian policy it is, resulting as it does in the
intelligent voter being completely disfranchised.”* In other
words, too many citizens are allowed to vote. In Social Credit, he
wrote that under his proposed reform of society, the voters will
be allowed to vote on general economic policy, but they must
not be allowed to dictate to the elite credit masters exactly how
the nation’s capital shall be allocated ~ a merely procedural
matter. “To submit questions of fiscal procedure, of foreign
affairs, and other cognate matters to the judgment of an elec-
torate is merely to submit matters which are essentially techni-
cal to a community which is essentially non-technical.”

So, when Douglas spoke of consumers’ control over credit,
he meant the elite credit masters’ control over credit in the
name of consumers. Under Social Credit, the representation of
consumers will be political and ultimately bureaucratic. Bankers
will not represent the interests of depositors. An unelected elite
group of central planners will represent consumers and pro-
ducers equally. We know where this system of bureaucratic
representation always leads, on both sides of the now-defunct
Iron Curtain: the elitists who disburse the money will pursue
their own economic interests and, if they should get caught, will
then claim that they were only representing “the People.”

Procedure and Responsibility

Douglas wrote as if the allocation of a nation’s capital were a
procedural matter only, as if the money elite’s crucial decisions
were as economically neutral and simple as drawing up plans to
build a bridge. Yet we know how politically corrupt something
as narrowly constrained as building a bridge can be: who gets
paid how much for the land, who gets awarded the contracts,
etc. Nevertheless, Douglas recommended the creation of a
political elite that would finance thousands of projects every

8, Warning Democracy (2nd ed.; London: Stanley Nott, 1984), p. 8.
9, Social Credit (3rd ed.; London: Eyre & Spottiswoode, 1933), p. 125.

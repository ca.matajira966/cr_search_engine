Social Credit Means State Monopoly Credit 195

Our acceptance or rejection of a reform proposal should not
be based on what it is called. Our decision should be made on
what the specifics of the proposal are.

“Effective Demand”

Major Douglas insisted that “The scarcity of money and the
consequent restriction of effective demand is unquestionably the
most important, and in fact, the vital point on which the future
of the present financial system turns. . . .”* This phrase,
“effective demand,” became important in the writings of anoth-
er economist, one far more influential than Major Douglas, as
we shall see: John Maynard Keynes.

Douglas also insisted that “an enormously increased use of
credit facilities is the only radical solution of the present diffi-
culties. . . .”” He called for consumer credit to match produc-
er credit. This credit should be issued “below cost.” (Below
cost? This is a familiar socialist slogan, not a conservative one.)
He assured his readers that “it is just as feasible to issue this
credit to the consumer by selling ‘below cost’ as it is to issue it
to the producer by anticipating payment. In both cases it is
public credit which is used. . . .”'

Someone or some process has to balance supply and de-
mand. In a free market society, this balancing is done through
flexible pricing. But Douglas did not believe that the voluntary
decisions of buyers and sellers, lenders and debtors, can per-
form this balancing function. He did not believe that freely
fluctuating prices are the best way to balance supply with de-
mand. Balancing must be done through the issue of credit, he
insisted, This means that merely producing goods and services
is not sufficient. There must be effective demand, a phrase which
Keynes was later to adopt. Douglas wrote: “Now, one of the

14. Warning Democracy, p. 100.
15. Credit-Power and Democracy, p. 143.
16, Ihid., p. 143.

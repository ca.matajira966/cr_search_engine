Preface xxiii

sound, both logically and theologically. My goal is to persuade
honest people of this fact - people who are willing to spend
some time examining the actual words of Major Douglas, and
who are willing to think through my arguments and Major
Douglas’ arguments before making a decision.

Nevertheless, we need more than negative goals in life. We
need positive goals. This book is more than a critique of an
obscure and long-forgotten English author of the 1920's and
1930's. It sets forth a positive case for free market economics,
and specifically Christian free market economics. After you read
it, you should have a better understanding of how the economic
world works.

There will be some readers who think, “I don’t care any-
thing about theology or religion.” Others may think, “I don’t
care anything about a lot of technical economics.” The problem
I face is that so many of those who identify themselves as fol-
lowers of Major Douglas do so in the name of Christianity. [ am
a Christian. I am the author of a four-volume (as of early 1993)
economic commentary on the Bible, with another 1,300-page
volume due out later this year. Over the last quarter century, I
have been challenged from time to time by followers of Major
Douglas to answer the good major. While I have discussed his
writings occasionally, I have never published a book-length
refutation of his system. This has not been because my problem
in refuting Social Credit ~ an accusation made in a recent news-
letter published by a Social Credit author (see Appendix B).
No, my previous refusal to write this book was based on the
inescapable reality of scarcity: time. I try to write at least one
serious book every six months, and I did not regard the Social
Credit movement as sufficiently large, sufficiently well-funded,
or sufficiently influential to take time out of my schedule to
write a full-length book on the topic. I wrote a great many
books and published many books written by other authors
before I finally decided to write this book.

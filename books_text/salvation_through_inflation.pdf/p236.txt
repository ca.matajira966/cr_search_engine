204 SALVATION THROUGH INFLATION

what basis can the credit masters decide how much fiat credit to
issue? The answer is obvious: there is no scientific basis.

Major Douglas offered no formula to guide the credit mas-
ters, no mechanism for the public to control the credit masters,
no legal limits to the creation of credit, meaning money. Social
Credit is therefore an open-ended invitation to the creation of
money by the State in an attempt by politicians to attain their
political goals without increasing taxes. This is the traditional
political motivation for every mass inflationist regime in history.
There have been many of them.®

Follow the Money

In his first book, Major Douglas wrote emphatically that “the
State should lend, not borrow, and that in this respect, as in others, the
Capitalist usurps the function of the State.” He never changed
his mind about this usurping of a State function by private
lenders. Everything he ever wrote on economic reform rested
on this fundamental presupposition. This was the heart of his
judicial criticism of capitalism, not his technical A + B Theo-
rem. He made his position clear: “There is no doubt whatever
that the first step towards dealing with the problem is the rec-
ognition of the fact that what is commonly called credit by the
banker is administered by him primarily for the purpose of
private profit, whereas it is most definitely communal proper-
ty” Here it is again: your bank account belongs to society.

I have stated that in seeking answers to questions of opera-
tional authority, we should begin with a plan: follow the money.
Let us do so now. Question: Where does the State obtain assets
(money) to Iend? Ah, there’s the rub! It possesses no wealth of
its own. The State has to take assets from individuals in order

38. Forrest H. Capie (ed.), Major Inflations in History (Brookfield, Vermont: Elgar,
1991).

89. Economic Democracy, p. 125.
40. Ibid., p. 121.

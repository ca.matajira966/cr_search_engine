11

SANCTIONS: FROM ECONOMICS
TO POLITICS

It seems indisputable that no modern economic system can be
based on any theory of rewards and punishments.

C. H. Douglas (1931)!

Behind any mechanism, you always have to have a sanction.
it is the sanction which is the important thing. If you have the
sanction, the mechanism can always be devised.

C. H. Douglas (1937)?

Well, which is it? Are sanctions — blessing and cursing, carrot
and stick — irrelevant to economic theory and economic systems,
or are they crucial? For economics, he said they are irrelevant.
For politics, which was the focus of his 1937 essay, Major Doug-
las said sanctions are inescapable. Yet earlier he had rejected
the long-term legitimacy of economic sanctions. He distin-
guished between political sanctions and economic sanctions.
This is a very important distinction, one which I believe lies at

1. The Monopoly of Credit (London: Chapman & Hall, 1931), p. 86.
2, The Policy of a Philosophy (Liverpool: K.R.B Publications, 1987), p. 7.

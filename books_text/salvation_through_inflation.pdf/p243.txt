Sanctions: From Economics to Politics 211

Darwinian Evolution

Douglas was committed to the doctrine of Darwinian evolu-
tion. In the language of evolutionism, he called his readers
“back to first principles — to an attempt to define the purposes,
conscious or unconscious, which govern humanity in its cease-
less struggle with environment.” This process he called “the
drive of evolution.”* He referred favorably to evolutionist Ben-
jamin Kidd's Science of Power, which defended the survival of
the fittest.? While there are some questions about the term
“fittest,” Douglas said, “it is not of course necessary to question
the soundness of Darwin's theory.”

He observed that “It is even probable that all life on this
planet is compelled by the nature of things thus to change on
to a different plane on pain of extinction.”® All of life evolving
to “a different plane”? This sounds more like New Age mysti-
cism than science. It is not difficult to see why A. R. Orage, a
Theosophist and Eastern mystic, adopted Social Credit so readi-
ly. Eastern mysticism is evolutionistic.

A Rejection of the Bible

As we have seen in Chapter 10, Douglas expected Social
Credit to remove steadily the role of labor as a source of a
person's income." He made this the touchstone of his econ-
omic reform:

1, That the cash credits of the population of any country shali at
any moment be collectively equal to the collective cash prices for
consumable goods for sale in that country, and such cash credits
shall be cancelled on the purchase of goods for consumption.

6, Economic Democracy, p. 4.
7, Tbid., p. 10.

8. fbid., p. 10.

9. Warning Democracy (2nd ed.; London: Stanley Nott, 1934}, p. 79.
10. iBid., pp. 34-86.

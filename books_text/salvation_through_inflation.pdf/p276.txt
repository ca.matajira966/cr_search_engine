244 SALVATION THROUGH INFLATION

different kind of break from the one he hypothesized in his
critique of bank credit. The A + B Theorem has nothing to do
analytically with the credit system.

A Series of Mistakes

We have seen in earlier chapters that Major Douglas’ analyti-
cal error began with his misunderstanding of money and credit.
There is no break in the flow of funds under capitalism. His
assertion that there is such a break rested on at least four fun-
damental misunderstandings of capitalism.

First, money is not a system of tickets; money is the most
marketable commodity. Tickets are legal claims to specific
goods: “one ticket, one item.” Money, on the other hand, is the
means of competitive bidding for goods: “high bid wins.”

Second, money (credit) is not issued to a prospective produc-
er by a bank so that a specific worker can in the future buy
back exactly the good or service which he has produced. Credit
is issued so that the producer can hire land (vend), labor (wages),
and capital goods (land plus labor) over time (interest) in order
to produce consumer goods. The bank lends money to enable
the producer to bring a final product to market.

The bank is equally willing to issue consumer credit to buy-
ers. If anything, the bank is more willing to do this. In the late
1980’s, consumers in the United States were paying interest
rates on bank credit card debt that were twice as high as the
rates which businesses were being charged. But Douglas never
discussed consumer debt, for obvious reasons. The presence of
consumer credit in finance capitalism makes his critique of capitalism
look silly, at least in the eyes of those who have not adopted a cult-like
attitude toward Social Credit.

The producer, if he is to make a profit, must buy these
factors of production less expensively than he can sell them in
their final composition as consumer goods. He makes his profit
(if any) by buying these factor inputs for less than they are
really worth, given the actual conditions of future consumer

256 SALVATION THROUGH INFLATION

 

uw | Re
Kate that ang wees eara-
i Stiaeespectoh ey sige
1 Papen before day ota
ve ameter

E power, which will be require.
gy | ede tly, Profitsare onltted

 

 

 

 

 

 

 

 

 

 

  

But the total money spent at each stage in the production
process will cover all costs of production. He identified materi-
als, labor, and material plus labor (capital). He ignored interest
payments. He could safely ignore profit, for profit is a residual
for correct forecasting; therefore, it may or may not appear in
any given entrepreneur’s accounts. The main issue is this: Will
the total payments by consumers match the payments to land (raw mate-
rials, space), labor, and interest? The answer is clear: yes.

We need to add interest payments to our diagram of the
structure of production. An accurate diagram will reveal that
consumers pay more to sellers than producers have paid to
owners of land and labor. The difference between what con-
sumers pay to sellers and what producers paid for land and
labor is énderest: the payments made by producers to suppliers of
capital during the period of production. Interest does not ap-
pear in Douglas’ diagram. This was a crucial mistake on his

258 SALVATION THROUGH INFLATION

On the same page, Rothbard commented on the meaning of
this diagram: “Now, instead of collecting interest income for
services in one lump sum at the final stage, the capitalist or
capitalists acquire interest income at each stage. If each stage
takes one year, then the entire production process for the good
takes six years. When the stages are all lumped together, or
vertically integrated, then one capitalist (or set of capitalists)
advances the owners of original factors their money six years
ahead of time and then waits for this period to acquire his
revenue.”

As you can see, total payments from consumers at the end of
the production process exactly match the total payments for the
factors of production — land and labor — plus interest. Or, as we
might put it, A + B (payments received by producers and lend-
ers) = A + B (payments made to producers by consumers).
Thus, Major Douglas’ analytical tool of criticism is wrong. To
the extent that Social Credit economics relies on the A + B
Theorem, Social Credit economics is wrong.

What About After the Reform?

For all the attention focused on the A + B Theorem by
Douglas, his followers, and his critics, one fact should not be
ignored: Douglas never once returned to this Theorem to show
mathematically why or how this supposed flaw would cease to
operate in the world of Social Credit. That is, his discussions of
the A + B Theorem always are confined to his discussions of
the inescapable dilemma of capitalism. Never once in his books did
Major Douglas show from his actual A + B formula how this suppos-
edly fatal flaw of capitalist finance will be avoided in the world beyond
his proposed reform.

As we saw in Chapter 4, his proposed reform involved hav-
ing the national government take a census of all the property in

20. Murray N. Rothbard, Man, Economy, and State: A Treatise on Economic Princi-
ples (Princeton, New Jersey: Van Nostrand, 1962), p. 314.

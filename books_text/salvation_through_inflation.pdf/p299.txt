My Challenge to Social Credit Leaders 267

The article was FAXed to me by Mr. Ian Hodge on Novem-
ber 9. That FAX persuaded me to write this book. I completed
the manuscript on December 31. I would have completed it
earlier, but I was in the process of completing two other books.

Why Economists Have Long Ignored Social Credit

T have now written the first scholarly book published since
the late 1930's that refutes the economics of Social Credit. The
jack of such a book prior to mine was not due to the intellectual
inability of many, many economists to respond to the details of
Social Credit’s proposed reforms. It was due to the fact that no
economist thought it was worth his time to respond, and every
publisher agreed with this assessment.

Social Credit as an economic doctrine has always been a
peripheral affair, a part-time pastime of poets, mystics, and
Christian fundamentalists who confuse Major Douglas’ ideas
with biblical wisdom. After the Social Credit government of
Alberta failed to implement Social Credit’s reforms (I hesitate to
call Major Douglas’ almost random suggestions a program) in
the late 1930's, it became obvious to any secular economist that
no book on Social Credit was needed, nor would there be much
demand for one. The public forgot about Major Douglas after
1939, as the good major knew very well before he died. So, the
professional economist asked himself, why spend time, effort,
and publishing capital to beat an obviously dead horse? Social
Credit economics is an exceedingly dead horse.

Were I not a Christian, I would have agreed with this assess-
ment. Had a remnant of God-fearing Christians around the
English-speaking world not been lured into the economic and
political errors of the late Mr. Douglas and his promoters, this
book would not have been worth my effort and ICE’s money.
Those defenders of Social Credit who have come in the name
of Major Douglas alone are simply not worth anyone’s time or
energy to challenge. They are defenders of what is clearly a
long-lost cause. They are intellectually incapable .of writing

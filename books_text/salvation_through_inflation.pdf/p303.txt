My Challenge to Social Credit Leaders 271

Douglas’ A + B Theorem, I showed that this theorem had
nothing to do with bank credit’s effect on the flow of funds. I
also showed that the theorem was incorrect with respect to the
flow of funds.

Seventh, I have shown that profits under the free market are
compatible with gradually falling prices as scarcity is steadily
overcome. I therefore applaud falling prices that fall because of
rising production and a relatively stable money supply. Falling
prices under these conditions is the mark of man’s progressive
overcoming of God’s curses in history. It is because I believe
that it is beneficial for men to work toward the total abolition of
the cursed aspects of scarcity, even though total abolition will
occur only after the Final Judgment, that I applaud falling
prices. Social Credit rejects falling prices.

Finally, I have shown that Major Douglas was an anti-Semite.
While some of his followers are aware of this, and no doubt
have committed themselves to the Social Credit movement
because of it, I think most of the Christians who are associated
with Social Credit would not agree with Douglas on this point.

I have done a great deal more than this, but this brief list
should be sufficient. If I have in fact done what I have listed
here, then Social Credit is revealed as fraudulent: wood, hay,
and stubble. It deserves to be abandoned. I call upon you to
abandon it publicly. You won't, of course. Therefore. .. .

‘What You Must Now Do

Those who have committed themselves to any cause for
many years rarely abandon it. We see this in the case of Chris-
tians who predict the return of Jesus to bring the Rapture on a
certain date. These predictions are always proven wrong on the
predicted day of the so-called secret Rapture." Does this per-

8. Dwight Wilson, Armageddon Now! The Premillennial Response to Russia and Israel
Since 1917 Giyler, Texas: Institute for Christian Economics, [1977] 1991). Dr. Wilson
is premillennial.

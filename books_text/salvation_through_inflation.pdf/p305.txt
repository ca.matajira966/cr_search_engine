My Challenge to Social Credit Leaders 273

followers that not one of you can answer me in an equally
detailed book, keeping your arguments internally coherent,
maintaining the cohesion of your entire reply, then your more
sophisticated followers will catch on: you can't answer me.
Can one of you answer me? In a book? That is the question.

“Douglas Did Not Really Mean This”

1 have filled my book with direct quotations from Major
Douglas’ books. I have let him speak for himself. It will be
difficult for you to persuade your readers that I have quoted
him out of context, since I have quoted from all of his books to
prove my case. Therefore, you will have to show that Douglas
really did not mean what he wrote. That will be a very difficult
task on your part. Also, you may wind up the way theological
liberals have: abandoning the message of the Revealed Word by
completely revising its meaning.

“North Has Misinterpreied Douglas”

This is probably your safest approach. While it will not be
easy, due to my continual word-for-word citations from Doug-
las, perhaps you will be able to confuse at least some of your
readers. After all, so few of them own all of Major Douglas’
books. They have not read them. Maybe you can show that two
or three my interpretations of his words are incorrect, or are at
least open to question, especially on some minor point or other.
Then you can write something like this: “We see that North has
completely misinterpreted Douglas and Social Credit.” You
have this much going for you: Douglas’ language is so often
confusing that your readers may not figure out that I have
interpreted him correctly and you haven't.

1 wonder, though, how you are going to deal with his con-
tinual attacks on the Jews. That will take some doing. He was so
clear on this point — just about the only topic he was clear
about.

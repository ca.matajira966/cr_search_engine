14 SALVATION THROUGH INFLATION

al jargon at least vaguely familiar to academic economists of an
earlier generation. But few laymen read Keynes’ General Theory.
In contrast, Douglas provided a reform program whose slogans,
though not the details of its proposed reform, could be easily
picked up by economically untrained critics of capitalism, critics
of banking, and, after 1929, critics of the Great Depression.

What the reader needs to understand is that Social Credit is
merely one variant of a whole series of proposed monetary
reforms, all claiming scientific validity, all blaming capitalism for
its supposed tendency toward underconsumption, all proclaim-
ing fiat money as the solution, and all suggesting a bankers’
conspiracy. Two decades ago, I identified the fundamental
conceptual and practical flaws in these analyses. I focused on
the writings of Gertrude Coogan, a contemporary of Major
Douglas, but a critic who had become far more popular within
the fringes of America’s right wing than Major Douglas had
ever been.** Her influence has now faded. It is time for me to
devote an entire book to Social Credit, for the influence of
Social Credit in Canada and Australia is still present, especially
among evangelical Christians.

Nevertheless, I deal with Social Credit only as an example.
There were many other fiat money inflationists before Douglas,
contemporary with him, and long after him. As Mises wrote in
1949, virtually the entire economics profession and all govern-
ments have adopted some version of underconsumptionism,
and they have proposed or adopted one or another scheme of
scientific fiat money management by central bankers.” I have
collected three shelves of books by popular inflationists, and I
own only a small fraction of those ever published. Louis Spad-
aro traced these ideas back to Karl Marx’s socialist rival, Pierre-
Joseph Proudhon.* Proudhon (1809-1865) was a socialist

24. Gary North, “Gertrude Coogan and the Myth Of Social Credit,” in North,
Introduction to Christian Economics (Nutley, New Jersey: Craig Press, 1973), ch. 11.
25. Mises, Human Action, p, 186.

26. Spadaro, “Salvation Through Credit Reform,” unpublished Ph.D. disserta-

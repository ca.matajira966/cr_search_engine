28 SALVATION THROUGH INFLATION

defended Social Credit?”

Seventh: “Why have the disciples of Major Douglas not kept
all of his writings in print?”

Eighth: “Have the present-day defenders of Social Credit
addressed the issues Gary North raises in his book?”

Ninth: “How would Social Credit’s primary reform - the
abolition of bank credit — be implemented without destroying
the economy during the transition?”

Tenth: “Why did the Social Credit government of Alberta,
Canada, refuse to impose any of Social Credit’s recommended
reforms in the years of uncontested dominance, 1935-39?”

Eleventh: “If Social Credit is Christian, why were so many of
its original defenders anti-Christian?”

Conclusion

Social philosophies are not mere abstractions. They are calls
to action. A philosophy must produce specific policies. Major
Douglas knew this: “If there is one thing which seems to me
beyond dispute, it is that you cannot have a policy . . . the
policy of a country, policy of a race, or of a nation, without
having a philosophy behind it.” He warned against what he
called the perils of abstractionism.'* He believed that his phi-
losophy was so radical in its policy prescriptions that it was in
fact revolutionary. He offered mankind the possibility of trans-
forming the world in short order:

Our new philosophy will change the run of the universe at
once. It will enable you to have a new conception. So if you can
do that, and in my opinion you can do it systematically, you will,
in an incredibly short time, become the most formidable force
that the world holds, because you will have, in my opinion, the
sounder philosophy, and you would have, in that philosophy, a

13. Philosophy of a Policy, p. 4.
14, Ibid., p. 8.

Social Credit’s Blueprint 59

Yes, the community does create credit. But there are impor-
tant questions that we need to get answered. What community?
How does it create credit? In what way did Douglas believe the
“community” creates credit? In what way should this “commu-
nity” distribute the credit? The answers to these questions are
at the heart of Social Credit’s proposed reforms.

The Community

Let me describe a credit-creating “community.” Let us say
that you have some money that you are willing to lend. I want
to borrow some money. You lend me money. I borrow it,
promising in writing that by an agreed-upon date, I will repay
you your principal plus a rate of interest. (I could also agree to
pay interest quarterly or monthly rather than at the end of the
loan.) This is surely a community action, albeit a rather small
community. Without a borrower there is no credit; without a
lender, there is no credit. As @ community of two, you and I have
created credit. Is there anything sinister in all this? If so, ] do not
see it.

Let me change the details. You want to lend some money. I
want to borrow. You take your money to a bank. You believe
the banker is more knowledgeable than you are regarding risky
loans. I then go to the banker for a loan. He decides that I am
not a high-risk borrower, so he lends me your money. He
charges you a fraction of the interest I pay him. This pays him
for his time and trouble. Is there anything sinister in all this? If
so, I cannot see it.

If he should create money on the basis of your deposit,
lending out more than you deposited, then there would be
something sinister. That would be what is sometimes called
(fractional reserve banking. He would have issued more receipts to
money than money in reserve. That would be an inflationary
act: diluting the value of all the monetary units in the economy

74 SANCTIONS AND DOMINION

possessed was incomplete, that he would have to supply services
to others in order to prosper.

Mobility at Some Price

As men approached the holy of holies, there was increasingly
limited lawful geographical mobility. This limited mobility was
based on the presence of sacred boundaries. Transgression of
these boundaries was not a violation of etiquette; it was a viola-
tion of sacred space: profanity.

Israelite society was not characterized by a fixed hierarchy,
especially a hierarchy of inheritance. The sacred hierarchy was
confined to a single tribe. This opened the possibility of social
mobility for those outside the tribe of Levi. There were no
economic guarantees outside of Levi, but there were also no
significant restraints on what a free man could earn or do with
his capital, including time. There was the jubilee law (Lev. 25),
but this law was to be applied infrequently. Economically speak-
ing, its effects would have become decreasingly significant over
time in a covenantally faithful society, as a result of the decreas-
ing economic relevance of agricultural landed property.

The sacred boundaries for Israelites were limited to the area
close to the tabernacle. Beyond this geographical limit, men
could go where they would. They could rise as high as their
talents would allow when God’s law was enforced. There was no
caste system that specified that a man, family, or tribe had to
perform this or that service. Service was contractual. With the
exception of those services identified by God's law as inherently
immoral, each person could offer his services for sale without
restriction. Because Israelite society was not sacred, service was
not fixed. Sacred boundaries applied only to the sacramental realm of
the sacrifices. Had Mosaic society been universally sacred, these
sacred boundaries would have encased men and their talents
within tight legal and geographical boundaries.

Beyond the sacred space of the tabernacle, and outside of
any sacred services performed by local Levites, men were free

The Hierarchy of Service/Sanctions 75

to move. So, they were free to choose. They could offer to buy
their way into whatever position was for sale, whether they used
their labor or their money as the appropriate currency of ac-
cess. Every hierarchy other than the hierarchy of ecclesiastical
service, which included the king’s office as military priest in
times of holy warfare, was temporary. Because men possessed
the legal right of mobility, upward and downward, the hierar-
chies that existed at any point in time were mobile.

The covenantal hierarchy of the priesthood was sacred: a
matter of life and death. The covenantal hierarchy in the army
was quasi-priestly: control over life and death. The respective
chains of command were necessary for the performance of
oath-bound service. The hierarchical structure of both of these
hierarchies could lawfully be defended by the threat of vio-
lence. Outsiders could not gain lawful access apart from adop-
tion and/or oath. The same is true in a family, another oath-
bound institution. But there is no fourth institution lawfully
established by means of a covenantal oath.

With the annulment of the Mosaic priesthood, neither birth
nor family adoption is necessary to gain access to the mediatori-
al ecclesiastical office of minister. Only a wife’® and a ministe-
rial oath is mandated. Even in the case of the military services,
access to the top positions has generally been open to men of
lower classes during wartime. The man who repeatedly wins
battles “buys” his way into senior military positions normally
closed in times of peace. The currency of upward mobility
during wartime is victory. This is why, in Tocqueville's opinion,
the non-commissioned officer in a democratic army favors
war,” Exemplary service in battle, coupled with the high

18. This is a true saying, if a man desire the office of a bishop, he desireth a
good work. A bishop then must be blameless, the husband of one wife, vigilant,
sober, of good behaviour, given to hospitality apt to teach” (I Tim. $:1-2).

19. Alexis de Tocqueville, Democracy in America, J. P. Meyer, editor, 2 vols.
(Garden City, New York: Anchor, [1966] 1969), II (1840), ch. 23, p. 654.

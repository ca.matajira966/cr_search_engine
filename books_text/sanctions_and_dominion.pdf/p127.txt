The Firstborn and Negative Economic Sanctions 89

As in the case of the Passover, they were to do this as a
means of instructing each generation in the story of their deliv-
erance from Egypt. Firstborn animals were either to be slain or
redeemed with money. Leviticus 27 specified that the firstborn
of clean animals had to be sacrificed: “. .. no man shall sanctify
it; whether it be ox, or sheep: it is the Lorn’s” (v. 26b). That
meant that it had to die. The firstborn of an unclean animal
had to be redeemed by paying their market value plus one-fifth
(wv. 27).

The one exception was the donkey: it had to be slain by
breaking its néck, or else it could be redeemed by the sacrifice
of a lamb. We shall consider the reason for this exception later
in this chapter. The donkey and the horse were unclean ani-
mals. They had hooves, but these hooves were not cloven. To
be a clean beast with hooves, it had to have cloven hooves and
also chew the cud (Lev. 11:3-7). Horses were comparatively
rare in Israel; the donkey was the commonly used beast of
transport for man.

As in the case of the Passover feast (Ex. 12:26-27), sons were
expected to ask what the meaning of this practice was. The
meaning here was the same as the meaning of Passover: 1) the
family’s deliverance (positive sanction) through the shedding of
a lamb’s blood (negative sanction); 2) collecting the inheritance
of Egypt (positive sanction) through the death of Egypt's first-
born (negative sanction).

As already mentioned, unclean animals were not to be killed;
they were mstead redeemed by a payment. “Every thing that
openeth the matrix in all flesh, which they bring unto the
Lorp, whether it be of men or beasts, shall be thine: neverthe-
less the firstborn of man shalt thou surely redeem, and the
firstling of unclean beasts shalt thou redeem” (Num. 18:15). A
money payment had to be substituted for Israelite firstborn
males and unclean animals. Was the link here based on Israel-
ite firstborn as judicially unclean? No, but they, like unclean
beasts, were not eligible to serve as literal sacrifices. So, a substi-

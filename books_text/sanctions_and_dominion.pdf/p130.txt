92 SANCTIONS AND DOMINION

born animals were not given to the priests for their use; on the
contrary, they were either killed or redeemed. Unclean animals
had to be redeemed with money. Why? Because they could not
serve as sacrificial substitutes.

There is nothing in the texts governing the firstborn to
indicate that the firstborn son had some unique claim on priest-
ly service. ‘The Bible. never says that a firstborn son under the
Old Covenant would automatically have become a family priest
in the household of his father. Both Cain and Abel offered
priestly sacrifices, not just Cain (Gen. 4:3-4). By the time a man
reached 30 years old, the age of Mosaic priestly service (Num.
4:47), he would probably have been the head of his own family.

Someone had to serve as a priest, but this office was not said
to be a monopoly of firstborn sons. Prior to the golden calf
incident, Moses used young men of Israel as assistants: “And
Moses wrote all the words of the Lorn, and rose up early in the
morning, and builded an altar under the hill, and twelve pil-
lars, according to the twelve tribes of Israel. And he sent young
men of the children of Israel, which offered burnt offerings,
and sacrificed peace offerings of oxen unto the Lorn” (Ex.
24:4-5), Non-Levites originally had duties associated with offer-
ing sacrifices.

The rabbis are correct about one thing: the firstborn sons
were indeed consecrated by God. They were consecrated in the
same way that firstborn sons in Egypt had been consecrated: as
covenantal representatives of the nation’s future. The mark of their
unique status was their inheritance of a double portion. Their
judicially consecrated status became an enormous threat to
them at the golden calf incident. Because of the rebellion of
their parents, the firstborn sons of Israel became the judicial
equivalent of the firstborn sons of Egypt: under a curse. These
sons were in need of another blood atonement: the judicial
equivalent of Passover lambs.

God substituted others in order to save the firstborn sons:
3,000 of their fathers. The Levites served as the priests in this

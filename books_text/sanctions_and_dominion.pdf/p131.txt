The Firstborn and Negative Economic Sanctions 93

atoning sacrifice. They slew 3,000 men, who became the judicial
equivalent of Passover lambs. The Levites became executioners
because the men had become idolaters, just as the Egyptians
had been. Had the Levites not acted to execute 3,000 represen-
tatives of Israel, God would have slain the firstborn sons. The
firstborn sons of Israel, apart from the bloody service of the
Levites, were as deserving of death as the firstborn sons of
Egypt had been. God substituted the Levites and the redemp-
tion payment system, not for the sake of some hypothetical,
God-consecrated priestly role for Israe!’s firstborn, but for the
sake of the firstborn sons’ judicial status as condemned representatives
of the nation’s future: point four of the biblical covenant — sanc-
tions — in relation to point five, succession.

As in the case of Egypt, Israel’s inheritance would have been
cut off had God imposed this negative sanction. If the Israelites
on the pre-adoption Passover night were in replacement-rate
mode, then the future of the nation would have been complete-
ly cut short apart from a program of adoption. Only daughters
would have remained. They would have had to marry adopted
sons of the mixed multitude. But more to the point covenan-
tally, the destruction of firstborn sons would have left the Ark
of the Covenant undefended in the next generation.

Bloodless Execution

A redemption payment was mandatory for the firstborn sons.
It was also mandatory for unclean animals. The exceptional
case was the donkey. Here, the owner had a choice: break its
neck or redeem it with a slain lamb. This is repeated twice in Exo-
dus. “And every firstling of an ass thou shalt redeem with a
lamb; and if thou wilt not redeem it, then thou shalt break his
neck: and all the firstborn of man among thy children shalt
thou redeem” (Ex. 13:13). “But the firstling of an ass thou shalt
redeem with a lamb: and if thou redeem him not, then shalt
thou break his neck. All the firstborn of thy sons thou shalt
redeem. And none shall appear before me empty” (Ex. 34:20).

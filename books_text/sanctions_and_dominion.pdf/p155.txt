The Altar Offering 117

Equal Tribal Assessments

This offering was the offering for the altar. The earlier offer-
ing had been for the construction of the tabernacle (Ex. 36:3).
Numbers 7 recounts in detail the same story a dozen times: one
day per tribe. It lists what each of the tribal princes placed in
the wagons. Each prince represented one tribe; each offering
was the same.

And his offering was one silver charger, the weight thereof was an
hundred and thirty shekels, one silver bow] of seventy shekels, after the
shekel of the sanctuary; both of them were full of fine flour mingled
with oil for a meat offering: One spoon of ten shekels of gold, full of
incense: One young bullock, one ram, one lamb of the first year, for a
burnt offering: One kid of the goats for a sin offering: And for a sacri-
fice of peace offerings, two oxen, five rams, five he goats, five lambs of
the first year (Num. 7:13-17a).

There was no distinction sacrificially among the 12 tribes in
terms of their wealth or population. They all owed four of the
five sacrifices: whole burnt offering (point one), meal (tribute,
allegiance)* offering (point two), peace offering (point three),
and sin (purification) offering (point four). They did not owe a
guile (reparation) offering (point five), which has no corporate
aspect.’ The Israelites had not sinned against men; they had
sinned against God.

The offering was delivered to the tabernacle by princes, i.e.,
men who served as civil officers. The text does not say how they
had apportioned the required offering among the families. We

3. Gary North, Leviticus: An Economic Commentary (Tyler, Texas: Institute for
Christian Economics, 1994), pp. 63-64.

4. Sacrificial offerings had to be male; personal offerings could be female. The
purification offering had to be female (Lew 4:28, 32). Milgrom, Numbers, p. 363.
Milgrom neglects to mention that the ruler’s reparation offering had to be male (Lev.
4:23), It was the common person's offering that had to be female. Masculinity under
the Mosaic covenant was associated in the civil covenant with rulership, femininity
with subordination. North, Leviticus, p. 93.

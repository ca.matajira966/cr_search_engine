142 SANCTIONS AND DOMINION

plied not just to tithes and voluntary offerings but also to man-
datory offerings to cover sin.

This is additional testimony to the fact that God does not call
upon man to make huge sacrifices. He warns men that they
must not attempt to buy their way out of God's judgment by
means of enormous sacrifices. Man does not possess sufficient
assets to placate God. To act as though he does is itself sinful.?
What God does call man to do is to acknowledge that with
greater blessings from God there is greater responsibility to
God. Their entrance into the land will be a blessing, God an-
nounces in Numbers 15. The people should know that the land
will bring blessings because there will be additional require-
ments for the mandated sacrifices, as well as for the voluntary
peace offering (Num. 15:9-10).

The parents had just rebelled against God and had been
locked out of the Promised Land. What were they going to
miss? A lot, God announced. The new requirements for the
various offerings testified to the fact that life would be filled
with blessings in the land: meal, wine, and oil. The people had
rebelled against God by complaining about the insufficiency of
manna. In the Promised Land, there will be far more than
manna, God was reminding them here.

There will be a new aspect of the offering. “Speak unto the
children of Israel, and say unto them, When ye come into the
Jand whither I bring you, Then it shall be, that, when ye eat of
the bread of the land, ye shall offer up an heave offering unto
the Lorn” (Num. 15:18-19). The heave offering will be of the
threshingfloor (v. 20). Previously, the heave offering by the
people was animal: “And the right shoulder shall ye give unto
the priest for an heave offering of the sacrifices of your peace
offerings” (Lev. 7:32; cf. Lev. 10:15). When they have land,
they will have farms. A small portion of agricultural output

2. Gary North, Leviticus: An Economic Commentary (Tyler, Texas: Institute for
Christian Economics, 1994), ch. 1.

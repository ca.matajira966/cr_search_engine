Evaluation and Sanctions 153

and against Aaron, saying, Ye have killed the people of the
Lorp” (Num. 16:41). They had seen the fire from heaven con-
sume the 250 who had offered sacrifice (v. 35). This did not
make an impression on them. The Israelites interpreted all of
this in terms of their desire for equality with Moses and Aaron.
Their representatives, who had acted to elevate the people, had
been destroyed. It was not God who had done this; it was Mo-
ses and Aaron. God’s immediate response to this accusation was
to send a plague on them (v. 47). Only the active intervention
of Aaron saved the nation, but 14,700 of them died (v. 49).

Chapter 17 is really an extension of chapter 16. God estab-
lished one more test and one more evaluation. He had Moses
instruct the tribes to brmg one rod per tribe. The name of the
prince of the tribe was to be written on the tribe’s rod. These
men were political representatives, not priests. Then He had
Moses write Aaron’s name on Levi’s rod. When these rods were
placed before God in the tabernacle and left overnight, Aaron’s
rod blossomed (v. 8). The other rods did not. Moses brought
each man’s rod back to him (v. 9). “And the Lorp said unto
Moses, Bring Aaron’s rod again before the testimony, to be kept
for a token against the rebels; and thou shalt quite take away
their myrmurings from me, that they die not” (v, 10), “And the
children of Israel spake unto Moses, saying, Behold, we die, we
perish, we all perish. Whosoever cometh any thing near unto
the tabernacle of the Lorp shall die: shall we be consumed with
dying?” (vv. 12-13). This ended the rebellion of the people in
seeking to become priests or to send their agents to become
priests.

The Basis of Evaluation

The people did not suffer from physical blindness; they
suffered from moral blindness. They refused to interpret what
they saw by means of what God told them through Moses. They
were in revolt against God. They were in revolt against Moses.
Moses’ words were automatically rejected by them. Thus, his

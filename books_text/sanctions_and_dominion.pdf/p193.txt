Evaluation and Sanctions 155

escape from the negative sanctions, Abraham said, was a will-
ingness to listen and conform to Moses and the prophets prior
to the imposition of the sanctions. This, the generation of the
wilderness steadfastly refused to do. And so the negative sanc-
tions kept coming.

Conclusion

The people had looked at their wilderness condition, had
compared it with the promise of milk and honey, and had
sanctioned the rebels retroactively. Korah had asked rhetorical-
ly, “Wilt thou put out the eyes of these men?” But these men
could not see. They were judicially blind. Seeing, they would
not see.

The nation saw what they wanted to see. They saw Moses
and Aaron as tyrants, despite the fact that it had been, and
soon would be again, the active intervention of Moses and
Aaron that allowed them to ‘survive. They saw the wilderness,
but did not see liberty. They saw their children, but did not see
the inheritance, which is why they refused to circumcise their
sons (Josh. 5:5), They had seen Egypt’s tyranny, but viewed it
as a land of plenty.

Evaluation is associated with judgment. To evaluate some-
thing is to judge it. The test of what an evaluator says is how
closely his words match what God has decreed. As Moses said
before God's judgment between Moses and Korah, “Hereby ye
shall know that the Lorn hath sent me to do all these works; for
I have not done them of mine own mind. If these men die the
common death of all men, or if they be visited after the visita-
tion of all men; then the Lorn hath not sent me. But if the
Lorn make a new thing, and the earth open her mouth, and
swallow them up, with all that appertain unto them, and they
go down quick into the pit; then ye shall understand that these
men have provoked the Lorn” (vv, 28-30), But the people did
not understand this; they chose not to understand. So the
negative sanctions came on them.

168 SANCTIONS AND DOMINION

services must be paid for. So, the tither is not buying his salva-
tion; he is supporting the representatives whose task it is to
declare the way of salvation and to allocate access to the sacra-
ments. Tithing is therefore a sacramental function; it is a pay-
ment for sacred services rendered.

This does not mean that tithing is mandatory in order to
receive the sacraments. The sacraments are not for sale for
money. They are allocated, however. Those who do not profess
faith in the God of the Bible are not allowed access to the sacra-
ments. Also, those who have been excommunicated because of
their flagrant sinning do not have lawful access. In Israel, access
to Passover was limited to circumcised males and their families.
The phrase “cut off from his people” appears regularly in the
Mosaic law. This was a judicial cutting off. A judicially conse-
crated representative of the church must decide who has lawful
access to the sacraments. His services must be paid for. The
tithe is the mode of payment mandated by God. Those who pay
less fall short of God’s requirements (Mal. 3:8-9).

The Politics of Plunder

God did not designate this system of equal percentage giving
as unfair. Twentieth-century man does. First, his designated
sovercign agency 1s the State, not the church. To cheat the
taxman is to cheat the State: the highest relevant court of ap-
peal in history. Second, modern man points to an equal per-
centage payment system and calls it regressive: an excessive
burden on the poor. The tithe principle is in fact proportionate,
but modern man dismisses proportionate taxation as regressive,
thereby condemning it in the name of a supposedly higher
morality: one opposed to oppressing the poor. Modern man
calls for progressive taxation, i.e., graduated tax brackets. The
rich should pay a greater percentage of their income, we are
told. Modern man docs not identify what percentage of income
taxation constitutes a boundary beyond which the State cannot
lawfully go. Establishing such boundaries is understood as

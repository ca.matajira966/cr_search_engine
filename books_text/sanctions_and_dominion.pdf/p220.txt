182 SANCTIONS AND DOMINION

mark “the center of the world,” the supreme sacred space?*
Could Adam and Eve somehow manipulate these cosmic forces
to gain further knowledge or power? Was the forbidden tree a
microcosm that offered man power over the macrocosm, analo-
gous to the voodoo doll’s supposed power to produce analo-
gous effects in the thing represented by the doll? Could Adam
and. Eve achieve “unity of being” with the universe through
subsequent forbidden feasts? Could they achieve self-transcen-
dence? In short, could they become mini-gods, as the serpent
had promised Eve (Gen. 3:5)?

The Genesis account of their transgression informs us that
immediately after their eyes were opened, the forbidden tree
was no longer the focus of their interest. They did not seek
additional fruit. They did not invoke cosmic forces to protect
them or do their bidding. They paid no further attention to the
tree. They did not act as though they believed the tree pos-
sessed any special properties other than its fruit, which was
admittedly good to view and good to eat. Even the serpent said
nothing further to them. There was no need for him to say
anything. His words and work were over. Adam and Eve had
performed the profane act. It was an act of judicial transgres-
sion: a trespass.

It is clear that their new-found self-awareness was the prod-
uct of seif-judgment: they had evaluated their act of rebellion in

or axis of the world ~ the fine drawn through the earth which points to the pole stax.
it was the link between heaven and earth. See Mircea Eliade, Patierns in Comparative
Religion (New York: Sheed & Ward, 1958), p. 111; cf. 266-67, 271, 273-74. On the
axis mundi, see the extraordinary, complex, and cryptic book on ancient mathematics,
myth, and cosmology, Hamlet's Mill: An essay on myth and the frame of time, by Giorgio
de Santillana and Hertha von Dechend (Boston: Gambit, 1969). It should be obvious
what the source of these cosmic tree and cosmic mountain myths was: the garden of
Eden, itself located on a mountain or raised area, for the river flowing through it
became four rivers (Gen. 2:10).

4, Eliade writes: “The tree came to express the cosmos fully in itself, by embody-
ing, in apparently static form, its ‘force’, its life and its quality of periodic regenera-
tion.” Patterns, p. 274.

184 SANCTIONS AND DOMINLON

cial task to them when He told Adam to guard the, garden.®
Adam’s task was to announce preliminary judgment against
Satan, for Satan had testified falsely regarding the character of
God. “Hath God said?”, the serpent had asked. But Adam and
Eve had served instead as false judges, rendering judgment
implicitly against God and explicitly against God’s word.” Im-
mediately, they recognized that they were wearing no “robes”
— the mark of lawful judicial authority. They were judicially
uncovered before each other. Their perceived dilemma had
nothing further to do with the tree. Now the primary symbol of
their spiritual condition was their own naked flesh. They
sought to cover this revelation with fig leaves.

God was not judicially present in the garden immediately
after their sin. He did not shout a warning to them: “I said not
to touch that!” He gave them time to respond, either as cove-
nant-breakers or covenant-keepers. They responded as cove-
nant-breakers. They knew that His negative sanctions were
coming, but their immediate concern was not their nakedness
in His eyes; it was nakedness in their own eyes. Later, they hid
themselves from God when they heard Him coming; in the
meantime, they felt a compulsive need to hide their flesh from
cach other.

They reacted as though the psychological effects of eating
from a merely symbolic tree - their sense of shame regarding
their own personal nakedness — could be successfully covered
by the leaves of another fruit-bearing tree. A representative of
the plant kingdom had been a crucial aspect of this crisis of
perception, so they covered themselves with leaves. They did
not slay the serpent or some other animal in their quest for a
covering. They dealt with their sin symbolically: the tree had be-
come to them a symbol of their transgression, and so their

6. “And the Lorp God took the man, and put him into the garden of Eden to
dress it and to keep [shawmar: guard] it” (Gen. 2:15).

7. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), Appendix E: “Witnesses and Judges.”

Preface XXV

the history of the covenantal struggle. Thus, there are two
possible choices for building a civilization: Christendom or anti-
Christendom. We now get to the famous bottom line: “He that
is not with me is against me; and he that gathereth not with me
scattereth abroad” (Matt. 12:30). There are Christians who want
to limit this two-fold distinction to individual souls, families, and
churches. They categorically deny that this division applies to
the civil covenant. They do so because they are opposed to the
ideal of Christian civilization. Officially, they affirm the exis-
tence of a freedom-enhancing, creedally neutral civil law. Un-
officially, they either prefer to live under anti-Christian civil
laws rather than biblical law, or else they seek a peace treaty
with humanism because they are convinced that the only alter-
native to this is the open persecution of the church and the
nearly total destruction of all traces of Christian culture. They
believe that their unofficial peace treaty with covenant-breakers
can gain Christians limited zones of neutral freedom under
“mild” anti-Christian civil sanctions. They prefer life in a Chris-
tian cultural and emotional ghetto to the comprehensive re-
sponsibilities associated with the Great Commission.” To put
it in historical terms, their theory of civil government borders
on the Amish view. In this sense, Protestant political theory has
become Anabaptist, beginning with Roger Williams and contin-
uing in Westminster Seminary’s faculty. It relies on some
combination of natural law, natural revelation, natural rights
theory, and common grace to protect Christians from tyranny.

The issue here is sanctions. Anti-Christendom Christians
believe that anti-Christians will not impose harsh civil sanctions
on Christians if Christians agree publicly not to impose any civil
sanctions on anti-Christians. They have adopted as a New Tes-
tament theological doctrine Sam Rayburn’s political dictum:

82. Kenneth L. Gentry, Jr, The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Tyler, Texas: Institute for Christian Economics, 1990).

88. North, Westminster's Confession. See also North, “I've Been Framed!" A Study
in Academic Positioning” (Dec. 1995), published by the ICE.

The Office of Court Prophet 215

thereby establish himself as both a true prophet and a court
prophet.

God did impose sanctions on Israel: 24,000 died in a plague
(Num. 25:9). More would have died had it not been for the
representative judicial action of Phinehas, the son of Eleazar (v.
7), who executed an Israelite man and Midianite woman in
their act of debauchery (v. 8). But Israel’s sin did not break the
covenant permanently God’s grace covered the transgression.
God then called Israel to battle Midian (v. 17). It was Midian
that lost the war, not Israel (Num. 31).

Conclusion

Balaam was the classic court prophet. The court prophet in
Israel served the king, not God. The king paid him to speak
the word of the king in the name of God. Whether he declared
the future accurately was irrelevant to his status as a court
prophet.

Covenant law is intended to lead sinners to repentance.
Balaam did not call Balak to repentance before God. Instead,
he sought a way to lure Israel into sin. He misused his knowl-
edge of God’s covenant. He sought a perverse end by means of
the law itself. In this sense, he adopted Satan's strategy with
Eve in the garden and Jesus in the wilderness: partial citations
of the law in order to undermine the intent of the law, i.e.,
covenantal faithfulness to God. Balaam announced God’s com-
mitment to Israel because of Israel’s righteousness, and then he
devised a strategy to make Israel unrighteous. For this, God
placed him under the negative sanction of execution.

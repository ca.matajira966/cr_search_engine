14

DIVIDING THE INHERITANCE

And the Lonp spake unto Moses, saying, Unto these the land shall be
divided for an inheritance according to the number of names. To many
thou shalt give the more inheritance, and to few thou shalt give the less
inheritance: to every one shall his inheritance be given according to those
that were numbered of him. Notwithstanding the land shall be divided by
lot: according to the names of the tribes of their fathers they shall inherit.
According to the lot shall the possession thereof be divided between many
and few (Num. 26:52-56).

The theocentric focus of this law is the familiar theme of
ownership: God was the owner of the Promised Land. He
delegated to Israelite tribes and then to families inter-genera-
tional stewardship over certain plots of land. He served as the
original agent of distribution by means of the casting of lots.
Tribes were to serve as the secondary agents of distribution:
allocating ownership in terms of family size. The question to be
resolved was ethical: To what extent was the allocation of land
based on considerations of equity — family size and need - and
to what extent on the question of equality of family inheritance?
That is, was the allocation based more on family size or tribal
inheritance rights? This problem has baffled rabbinic commen-
tators for almost two thousand years.

Preface Xxvii

church, we are also told. Only one view of eschatology denies
this with respect to the New Covenant church age: postmillen-
nialism. This view is dismissed as heretical by premillennialists
and amillennialists.

Consider the inflamatory rhetoric of amillennialist Engelsma.
He dismisses “the carnal kingdom of postmillennialism” as
“Gnjurious, if not disastrous.” Postmillennialism raises “practical
nightmares.” He invokes a code word of the pietist-Anabaptist
tradition: “worldly.” He goes on: “Reformed men and churches
make strange, forbidden, wicked alliances in order, by hook or
by crook, to build the earthly kingdom of Christ.”** Christian
Reconstruction introduces the “fundamental heresy of Judaiz-
ing” by calling for “a vast array of Old Testament laws. . . "°°
As for J. Marcellus Kik’s book, An Eschatology of Victory (1971),
it is heretical, as is Christian Reconstructionism. “By heresy, I
mean not only a serious departure from the teaching of the
Scriptures but a grievous corruption of the gospel. The error is
that the spiritual kingdom revealed and realized by the gospel
is changed into a carnal kingdom, and the spiritual triumph of
the exalted Christ in history is changed into an earthly tri-
umph.”*” This is very strong judicial language. Is he correct?

Here I must offer the reader an explanation. I devote the
remainder of this Preface to answering Rev. Engelsma. Yet he
is not a well-known critic of Christian Reconstruction. He is a
leader in a small Dutch-American denomination, the Protestant
Reformed Church.® Then why single him out? First, because

35. David J. Engelsma, ‘Jewish Dreams,” The Standard Beaver (Jan. 15, 1995), pp.
178-74,

36. Ibid., p. 174.

37. Ibid. (March 18, 1995), p. 295.

38. It was founded in 1923 in reaction to the Christian Reformed Ghurch’s
position on common grace, namely, that God shows some degree of favor and love
to all men, The PRG has denied the very existence of God’s common grace, thereby
abandoning Calvin and the entire history of Reformed theology. Their theologians
cannot easily explain | Timothy 4:10: “For therefore we both labour and suffer
reproach, because we trust in the living God, who is the Saviour of all men, specially

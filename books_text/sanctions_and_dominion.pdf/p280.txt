16

OATH AND SANCTIONS

if a man vow a vow unto the Lorp, or swear an oath to bind his
Soul with a band; he shall not break his word, he shall do according to
all that proceedeth out of his mouth (Num. 30:2).

The oath constitutes point four of the biblical covenant mod-
el: oath/sanctions.' Oath and sanctions are linked judicially. A
covenantal oath is a self-maledictory oath. It formally invokes
God’s negative sanctions in advance, should the oath-taker not
perform the details of his oath. Without the threat of God's
negative sanctions, there is no valid covenantal oath. Putting
this another way, where it is not valid to invoke such sanctions,
a biblical oath is not valid. Putting it even more specifically, a
self-maledictory oath is valid only when sworn under the judi-
cial authority of an institution that has been authorized by God
to impose or accept such an oath. There are three such institu-
tions: church, family, and State. This is the reason why secret
societies misuse the power of the oath when they impose initia-
tory oaths of secrecy that invoke negative sanctions. Such oaths
are not valid, nor arc the unsanctioned institutions that require

L. Ray R. Sutton, That You May Prosper: Dominion By Covenant (2nd ed; Tyler,
Texas: Institute for Christian Economics, 1992), ch. 4.

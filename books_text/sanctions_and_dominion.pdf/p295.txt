Oath and Sanctions 257

Scottish Enlightenment was decentralist, emphasizing the bonds
of society before and more comprehensive than the State. The
European Enlightenment tended to reverse this; its focus was
statist, with the State as the organizing principle of society. The
Scottish Enlightenment’s outlook became the social philosophy
of English Whiggism.. Scottish Enlightenment political thought
was a deistic version of seventeenth-century Scottish Presbyter-
ianism’s covenant theology: government as an appeals court
system that intervenes when all other human governments and
associations fail to settle a dispute. Continental Enlightenment
political thought was a deistic version of the Jesuit order’s hier-
archy: civil government as centrally directed, highly disciplined
social order. “Citizen” became for the French Revolutionaries
what “brother” was for the Jesuits. (Among Communists a cen-
tury later, “comrade” replaced “citizen.”)

Nineteenth-century English liberalism abandoned even the
weak deistic elements of Whiggism.'? Nothing substantive re-
mained except the stipulations of the two contracts: private and
civil. The sanctions upholding the stipulations of the contract
were said to be exclusively secular. Nineteenth-century Conti-
nental liberalism also abandoned the doctrine of God. Anti-
clericalism became a new religion.”

Darwinism and the State

Darwinism had a transforming impact on both Anglo-Ameri-
can and Continental social thought. In the two decades after
the publication of The Origin of Species (1859), Darwinism was
used by the disciples of Herbert Spencer to bolster the free
market by asserting the legitimacy of unguided economic com-
petition as the means of the survival of the fittest individuals
and businesses. Over the next two decades, intellectual opinion

19. Owen Chadwick, The Secularization of the European Mind in the 19th Century
(New York: Cambridge University Press, 1975), ch. 2,
20. Ibid., ch. 5.

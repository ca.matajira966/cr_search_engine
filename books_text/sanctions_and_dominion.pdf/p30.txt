XXX SANCTIONS AND DOMINION

He argues that there is supposed to be no trace of the Old
Testament’s legal order in New Testament era civil law. “The
New Testament reality of the nation of Israel, the real kingdom
of God in the world, does not legislate and execute the civil
laws of the Old Testament. It has no use for the civil laws of the
shadow-nation.”** This means one of two things: 1) the real
New Testament kingdom of God has no civil aspect, and hence
does not legislate, or 2) the real New Testament kingdom does
have a civil aspect, but some other source of civil law has been
substituted by God. What other source, he refuses to say.

Reconstructionists ask: “Where should Christians seek accu-
rate definitions of law and crime?” Engelsma prudently remains
silent on this point, except to say where we should not search:
the Old Testament. He and Rev. Hanko have remained silent
on this matter for the last decade and a half in their intermit-
tent attacks on Christian Reconstruction. In this respect, they
share a great deal with all of Reconstructionism’s critics. Recon-
structionists have offered a comprehensive ethical system in the
name of Christ; meanwhile, our critics resort to rhetoric. They
yell, “Heretics!” This is not a legitimate substitute for detailed
biblical exegesis: criticism based on biblical texts. This is why I
have devoted almost a quarter of a century to writing detailed
commentaries on the economics of the Pentateuch. Our critics
have yet to respond with an equally detailed series of commen-
taries on any aspect of the Pentateuch, As time goes on, the
disparity between our commentaries and our critics’ rhetoric
will become more pronounced.

Kingdom Sanctions

Notice that Rev. Engelsma speaks of “the real kingdom of
God in the world.” He does not say exactly what this phrase
means. I need to make two additional observations. First, if he

44. Engelsma, “Jewish Dreams,” op. cit., pp. 174-75.

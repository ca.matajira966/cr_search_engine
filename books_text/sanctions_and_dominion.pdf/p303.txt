17

WAR BRIDES

And they warred against the Midianites, as the Lorp commanded
Moses; and they slew all the males. And they slew the kings of Midian,
beside the vest of them that were slain; namely, Evi, and Rekem, and
Zur, and Hur, and Reba, five kings of Midian: Balaam also the son of
Beor they slew with the sword. And the children of Israel took all the
women of Midian captives, and their little ones, and took the spoil of all
their cattle, and all their flocks, and all their goods. And they burnt all
their cities wherein they dwelt, and all their goodly castles [fortresses],
with fire. And they took all the spoil, and all the prey, both of men and
of beasts (Num. 31:7-11).

Midian had tempted Israel by sending women to marry
them and then jure them into the worship of false gods. “And
the Lorp spake unto Moses, saying, Vex the Midianites, and
smite them: For they vex you with their wiles, wherewith they
have beguiled you in the matter of Peor, and in the matter of
Gozbi, the daughter of a prince of Midian, their sister, which
was slain in the day of the plague for Peor’s sake” (Num.
25:16-18). Yet in this passage, Moses allowed the Israelites to
marry Midianite women. Why the apparent discrepancy?

Marriage is a covenantal institution. It is governed by laws of
the covenant. The Mosaic law established laws for the marrying
of foreign women. These laws were explicit. The only foreign

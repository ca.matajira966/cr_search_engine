The Spoils of Holy Warfare 277

a testimony to the protecting hand of God in battle, but also as
a public acknowledgment that the men who had received God’s
special protection had acknowledged this by a voluntary gift.

Conclusion

The military spoils of Midian were unique. The whole nation
had not been directly involved in the battle. Only a small, cov-
enantally representative force had been chosen to fight. This
would not be the case.in Canaan. This was a preliminary cam-
paign designed to prove to Israel that God was with them in a
special, miraculous way. Not one Israelite died in battle.

The representative character of this force was manifested in
the requirement that the participating warriors forfeit half of
their animate spoils to the nation. The nation made a small
tribute payment to the Levites; the warriors paid a tenth of this
tribute to the Aaronic priests through Eleazar. Those who had
entered the battle at greater risk statistically paid less than those
who remained behind.

The miraculous outcome of the battle - no Israelite deaths —
so impressed the warriors that they voluntarily gave their inani-
mate booty to Moses and Eleazar. They had not in fact been at
risk, so they decided not to keep the booty that was normally
the reward of any soldier who could take it in battle. The risk-
reward ratio of normal combat had been overcome in this battle
by God's intervention. The statistical improbability of the cam-
paign testified to God’s special presence among them. The
combatants publicly acknowledged that there had been no risk
by surrendering their personal rewards to the covenantal repre-
sentatives of the God who had removed the risk.

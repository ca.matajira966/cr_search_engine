284 SANCTIONS AND BOMINION

22). He made no mention of their offer to serve as an advance
guard, which had been their original offer, which demonstrated
that fear of combat was not the issue. He reduced their re-
quired level of commitment. If they broke the terms of this less
rigorous covenant, Moses said, there would be negative sanc-
tions: “But if ye will not do so, behold, ye have sinned against
the Lorn: and be sure your sin will find you out” (v. 23)..We
know this agreement was.a covenant rather than a contract
because of the threat of God’s negative sanctions.

Was their offer sincere? The layout of the nation’s military
formation indicates that it was. Reuben and Gad marched side
by side. Manasseh was on the right flank.®

The Israelite Camp
‘Asher DAN Naphath

Levites
(mera

Benjamin ‘scachar

 

EPHRAIM Tent af Meeting JUDAH

Levites
(Gershon)
sweat

 

 

Manasseh Zebulua

 

evitas
‘kohathy

Gad REUBEN Simeon

If the nation subsequently marched into battle with the
“south” quadrant leading the attack, Gad and Reuben would
take the brunt of the initial resistance. Simeon would also be
exposed. Manasseh would be on the right flank. If half the tribe
of Manasseh joined Simeon’s ranks, Simeon would receive
additional support, thereby compensating for its increased risk.

To accept this offer, Moses would have had to sacrifice Ju-
dah’s role as the “point man” of the nation, the primary sword-
bearer in battle, a position consistent with Jacob’s prophecy:
“The sceptre shall not depart from Judah, nor a lawgiver from

3, James B, Jordan, Through New Eyes: Developing a Biblical View of the World
(Brentwood, Tennessee: Wolgenruth & Hyatt, 1988), p. 205.

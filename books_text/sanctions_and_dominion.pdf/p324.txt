286 SANCTIONS AND DOMINION

Structuring a Persuasive Offer

Moses then assembled representatives of the other tribes to
announce the terms of this covenant (v. 28). He told them the
options. He structured the options in such a way that the other
tribes could hardly refuse. “And Moses said unto them, If the
children of Gad and the children of Reuben will pass with you
over Jordan, every man armed to battle, before the Lorp, and
the land shall be subdued before you; then ye shall give them
the land of Gilead for a possession: But if they will not pass
over with you armed, they shall have possessions among you in
the land of Canaan” (vv. 29-30). If the two tribes fought along-
side the other ten non-Levitical tribes, they would surrender
their share of the Promised Land to the other ten tribes. This
was Clearly a benefit to the other tribes. The other tribes would
trade any future claims on the land of Gilead to the two tribes
and half the tribe of Manasseh (v. 33). On the other hand, if
the two tribes refused to fight, they would still inherit their
share of the land of Canaan.

Was this fair? Why should these two and a half tribes receive
part of the spoils of war if they refused to fight? Because of the
promise to Abraham: his seed would inherit the land. This
promise was qualified in only one way: circumcision. Without
this covenantal mark, this seed and land prophecy would not
come to pass. This is why the sons of Israel had to be circum-
cised after they crossed the Jordan but before they conquered
Jericho (Josh. 5:7). The two and a half tribes could not be kept
out of the land if they agreed to fight. In fact, it seemed impos-
sible to keep them out even if they refused to fight. They were
entitled to their share of the land.

So, the offer was structured in such a way that the positive
sanctions of the two and a half tribes’ participation favored the
other tribes: their forfeiting of any claims to land inside the
Jordan River’s boundaries. Reinforcing this offer was the fact
that a negative sanction would be imposed on the other tribes
if the two and a half tribes refused to cross over: sharing equal

288 SANCTIONS AND DOMINION

share — whatever other participants regard as fair® — why
should anyone participate? If no one participates, the collective
goal cannot be achieved.

This is the logical justification for the imposition of civil
compulsion in certain collective efforts. This free rider argu-
ment lies behind the creation of an analytic economic category
called public goods. Economist Edwin Dolan describes public
goods as “goods or services having the properties that (1) they
cannot be provided to one citizen without being supplied also
to that citizen’s neighbors, and (2) once provided for one citi-
zen, the cost of providing them to others is zero. Perhaps the
best example of a public good is national defense.”” A missile-
defense system protects everyone in the region whether or not
one person pays. But if anyone so protected can legally escape
paying, then the missile-defense system will probably not be
built.

The free market offers a solution in most areas of resource
allocation: no participation in the end result for those who
refused to participate in the effort - “no pain, no gain.” A legal
boundary is placed around the end product, i.e., a property
right is established. The legal system authorizes property own-
ers to exclude free riders. But exclusion from God’s covenant
with Israel was not allowed merely for failure to participate in
military service. Deborah could not legally force Barak to lead
the army against Sisera (Judg. 4:8). Similarly, she could not
force the tribes to participate. This is why she included ridicule
against some non-participating tribes in her song of victory

6. Ina collective effort, there is no scientific way of determining a fair share.
This is another application of a central dilemma for theoretical economics: the
impossibility of making interpersonal comparisons of subjective utility. See Gary
North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: institute for Christian
Economics, 1987), ch. 4; North, Tools of Dominion: The Case Laws of Exodus (Tyler,
Texas: Institute for Christian Economics, 1990), Appendix D; “The Epistemological
Problem of Social Cost.”

7. Edwin G. Dolan, Basie Economics (2nd ed.; Hinsdale, Illinois: Dryden, 1980),

 

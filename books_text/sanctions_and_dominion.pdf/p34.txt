Xxxiv SANCTIONS AXD DOMINION

Only after Jesus comes with His angels to rule the earth in
person, the premillennialist says.

The pessimillennialist, whether premillennial or amillennial,
resists my definition of the kingdom, for it extends long-aban-
dened and long-denied areas of responsibility to Christians. It
announces the need for comprehensive evangelism as part of
God’s mandated program of comprehensive redemption.”
Pessimillennialists seek to escape these added kingdom respon-
sibilities. It is all that the pietistic theologian can do to maintain
biblical relevance inside the narrow confines of the Christian
ghetto. Defending the Bible’s relevance in the frightening world
outside this ghetto is more than he chooses to bear.

Orwellian Newspeak

Rev. Engelsma’s eschatology denies the transforming power
of the gospel in history. It does not present Christianity a
world-transforming, evangelizing, spiritual leaven (Matt. 13:33).
It implies that the presence of the Holy Spirit will not trans-
form our world. He sees Satan's earthly kingdom as possessing
the only comprehensive, world-changing program in history.
His implicitly humanistic social theory and his defeatist escha-
tology justify life in a defensive Christian ghetto.

For Rev. Engelsma and theologians who share his views, the
doctrine of Christ’s bodily ascension in history to the right hand
of God remains an irrelevant doctrine for social theory. In fact,
these men deny the very possibility of Christian social theory,
precisely because of their ghetto eschatology. They spend their
careers re-writing the plain meaning of the Great Commission:
the discipling of all nations.*! The Great Commission cannot
mean this, Rev. Engelsma’s theology implicitly insists; therefore,

50, Gary North, fs the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1988), Appendix C: “Comprehen-
sive Redemption: A Theology for Social Action.”

31. Gentry, The Greatness of the Great Commission.

XXXVI SANCTIONS AND DOMINION

cal world to the devil. This is God’s plan for the ages, he says.
“But Satan does have ‘complete control over the nations of the
world.’ Of course, he is not the almighty sovereign. The triune
God is sovereign, But Satan controls the nations of the world as
to their spiritual condition.” This is not merely a temporary
condition that Christians must work to reverse. On the con-
trary, we must learn to live with it. “Until the personal return
of Christ, the nations under the government of the kings of the
earth make war against Him as He is present in His church by
His Word.”** The old phrase that Ben Franklin recommended
as America’s national slogan — “Resistance to tyrants is obedi-
ence to God!” ~ has been in effect reworked by Rev. Engelsma:
“Resistance to tyrants is disobedience to God!” Our goal is to be
let alone by the humanists in our little ghettos. Otherwise, our
task is to serve as martyrs, There is no legitimate hope in Chris-
tian social transformation. We are little more than sheep for the
slaughter. He calls this theology “victorious.” Indeed, it is. . .
for Satan.

Conclusion

The seventeenth century brought the beginnings of postmil-
lennial optimism to Protestantism, and accompanying this post-
millennialism, for the first time in man’s history, came the ideal
of long-term economic growth, compounded. This economic
growth ideal eventually transformed England; it was in England
that the Industrial Revolution began in the late eighteenth
century. What was first believed to be possible in the seven-
teenth century began to take place a century later: long-term
growth without permanent reversal.

53. Engelsma, “A Defense of (Reformed) Amillennialism. 3, Apostasy and Perse-
cation,” ibid, (May 1, 1995), p. 343.

54, Engelsma, “A Defense of (Reformed) Amillennialism. 2. Revelation 20,” ibid.
(April 15, 1998), p. 366. April 13 is tax-filing day in the United States: appropriate
for Dr. Engelsma’s tirade against all Christian political reform.

322 SANCTIONS AND DOMINION

ble says that we must be slaves to the future.® God rules the
future as absolutely as He rules the present and the past. An
important mark of His people’s faithfulness is their confidence
in the future, for it is governed by God’s eternal decree. This is
seen through faith, since we cannot see the future. But God’s
reliability has been revealed in the Bible through the prophets’
accurate predictions of future events.’ The mark of the true
prophet was two-fold: accurate predictions regarding the future
(Deut. 18:22) and faithful theological testimony regarding the
one true God (Deut. 13:1-5).

A person who is future-oriented is upper class, no matter
what his present income is. His thinking is characterized by
long-range planning, thrift, and a willingness to defer gratifica-
tion. Mises calls this phenomenon low time-preference. The
future-oriented person is willing to lend at comparatively low
rates of interest. He is unwilling to borrow at high rates of
interest in order to fund present consumption.*

The generation of the exodus could not plan for the future
successfully. They were trapped by their own present-orienta-
tion. They could not see beyond the present. They were there-
fore blind to the reality of the past. They kept crying out to
Moses to take them back to Egypt. They remembered the past
in terms of the low-risk immobility of slavery. The past deliver-
ances of God did not persuade them to accept His promise of
future protection because they had no confidence in history.

6. North, Moses and Pharaoh, pp. 259-60.

7. This is why higher critics of the Old ‘Testament invariably conclude that
prophetic passages that demonstrably came true, most notably those in Daniel
forecasting three future empires, were written after the fact. The suggestion that
some men can know the future in detail is an implicit affirmation of teleology: a
future that is fixed, i.e., not open-ended in terms of the present. It affirms predesti-
nation. Only man is allowed by humanists to seek to predestine the future, and even
he is not acknowledged as being capable of achieving this goal.

8. On time-preference and the rate of interest, see Ludwig von Mises, Human
Action: A Treatise on Economics (New Haven, Connecticut: Yale University Press, 1949),
ch, 19,

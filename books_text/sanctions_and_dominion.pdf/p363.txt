Conclusion 325

Canaan was supposed to be disinherited by Israel. This
disinheritance would be the basis of Israel’s inheritance. This
transfer of wealth was based on ethics, not power. God had told
Abraham: “But in the fourth generation they shall come hither
again: for the iniquity of the Amorites is not yet full” (Gen.
15:16). Corporate, national iniquity was the covenantal basis of
the disinheritance of the Amorites. The progressive rebellion of

the Amorites was cumulative. It moved toward eschatological
fulfillment.

Sanctions and Eschatology

Israel had been given an eschatology: guaranteed inheri-
tance through military conquest. The exodus generation had
not believed this eschatology. Or, more to the point, that gener-
ation refused to believe that the eschatological fulfillment of the
promise of a land flowing with milk and honey was in any way
associated with Israel’s prophetic role as a sanctions-bringer in
Canaan. Israel rejected the specified terms of the inheritance:
military conquest. So, the next gencration would inherit. This
meant that each of Israel’s holy warriors would have to accept
both the obligation and threat of personal military sanctions in
battle. Israel’s national inheritance was tied to the presence of
these sanctions.

This leads us to a theological conclusion. Point four of the
biblical covenant model is sanctions. It is as tied judicially to
point five, inheritance, as it is to point three: law. God imposes
historical sanctions, positive and negative, in terms of His cove-
nant law. These sanctions result in inheritance by His covenant
people and the disinheritance of covenant-breakers. This is why
theonomy is inescapably and indissolubly tied to eschatology.
Theonomy is inherently postmillennial because theonomy is
biblical law, and biblical law is indissolubly linked to God’s
covenant sanctions in history. Law without sanctions is mere
opinion. Theonomy without predictable historical sanctions is
mere opinion ~ one not widely shared. Theonomy without post-

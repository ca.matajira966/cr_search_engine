How Large Was Israel’s Population? 339

have been 30 X 70 = 2,100. Of these, 1,050 were sons. Repeat-
ing this performance, Moses’ generation would have totaled
15,750 men (15 X 1,050). Assuming no retarding effects demo-
graphically from the persecution of the Pharaoh, Moses’ gener-
ation would have produced 236,250 sons (15 X 15,750). If ali of
Moses’ generation had been alive at the time of the first num-
bering, and all of Joshua’s generation, the total would have
been 252,000 men, But there were over 600,000 men. Even
with the preposterous assumption of 30 children per family, the
numbers do not add up.

Relating to the second mustering in the Book of Numbers,
we read: “And the name of Amram’s wife was Jochebed, the
daughter of Levi, whom her mother bare to Levi in Egypt: and
she bare unto Amram Aaron and Moses, and Miriam their
sister” (Num. 26:59). It appears that Israelite mothers had far
fewer than 30 children unless Jochebed missed the mark by a
factor of ten to one. Israel’s descent into Egypt took place when
Jacob was an old man: age 130 (Gen. 47:9). Jochebed’s birth
occurred after Levi had come down with his father into Egypt.
There were no other intervening generations. Moses’ genera-
tion was the second after the descent. This left only two until
the conquest of Canaan (Gen. 15:16).”°

Adopted Household Servants

Seventy male family members arrived in Egypt: “All the souls
that came with Jacob into Egypt, which came out of his loins,
besides Jacob’s sons’ wives, all the souls were threescore and six;
And the sons of Joseph, which were born him in Egypt, were
two souls: all the souls of the house of Jacob, which came into
Egypt, were threescore and ten” (Gen, 46:26-27). Jacob plus

 

20. The children of Joshua's generation did have children, and presumably some
of them were over age 20 at the second Numbers mustering. But the leaders were
the sons of Joshua’s generation, and so are regarded as heirs of God's promise to
Abram.

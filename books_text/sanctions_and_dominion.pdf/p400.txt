362 SANCTIONS AND DOMINION

Second, it is possible that they believed the Israelites and put
blood on their doorposts. To this extent, they covenanted with
God: a common-grace, non-adoptive covenanting. Problem:
Where did this many of them get the lambs? Or did they use
some other form of blood, e.g., the blood of human fathers?
Did God accept a substitute form of blood in an emergency? He
might have. He wants obedience, not the blood of animals.
“The sacrifices of God are a broken spirit: a broken and a
contrite heart, O God, thou wilt not despise” (Ps. 51:17). Third,
it is possible that their firstborn died. This possibility seems
unlikely, for the adoption scenario rests on the assumption of
the presence of a large population of non-Israelite males. If
their firstborn sons did die, who subsequently married all of
their firstborn daughters? How did demographic stability occur
in the wilderness era, i.e., one wife, one husband, one son, and
one daughter?

I think the second possibility is most likely. [ reject the first
scenario — grace without bloodshed — because of the compre-
hensive nature of the death of the firstborn in Numbers 8:17:
“For all the firstborn of the children of Israel are mine, both
man and beast: on the day that I smote every firstborn in the
land of Egypt I sanctified them for myself.” The firstborn sons
and animals of the mixed multitude survived because the par-
ents smeared blood on their doorposts. They believed the God
of Israel with respect to the coming sanction. They had seen
the other nine plagues. In smearing their doorposts with blood,
they did not covenant with God in a special grace sense but in
a common grace* sense: a visible acknowledgment that He is
sovereign and the source of visible covenantal sanctions in
history. They broke covenant with the gods of Egypt, but they
did not formally covenant with the God of Israel. They only
acknowledged that in order to avoid the negative sanction of

36. Gary North, Dominion and Common Grace: The Biblical Basis of Progvess (Tyler,
Texas: Institute for Christian Econontics, 1987}.

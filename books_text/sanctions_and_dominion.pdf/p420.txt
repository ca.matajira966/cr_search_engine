INDEX

Aaron

assistant, 62

challenged by Korah, 69

challenged Moses, 132-
33

death, 2, 175, 189, 192

family, 356

golden calf, 56

inner circle, 61

numbering, 29, 35

economics, 353-54

firstborn vs., 352-55, 360

Jacob’s household, 339-43,
357

Joseph's family, 224n

marriage, 313-15

Mosaic law, 315

priestly, 60, 75, 97

tribal, 359

wives, 234, 313

office adultery, xxix, xxi, xxxii-xxxiii,
priesthood, 56 214

robes, 189 Ahab, 110-13, 235

rod, 58, 153 Ai, 198

sons, 62, 64, 102-103,
108-10, 114, 157-59,

allocation (which rule?), 217-19
altar, 71, 118, 160, 249

161, 342 Amalek, 41, 137, 192
tithe, 65 amillennialism, xii—xiv, xxviii,
vows, 252 xix, xxii-xxiii, xxviii-xxix,
Abel, 92, 174 326
Abihu, 62, 109 Amish, xxv
Abiram, 149 Ammon, 198
Abraham, 7-8, 189, 204, 297 Amorites, 199, 217, 325
Abravanel, Isaac, 218-19, 221, Anabaptists, xviii, xix, xxv,
227 XXXVii
Adam, 56, 59, 181-86, 324 animals, 4 (see also sacrifices)
adoption annihilation, 267-68, 272,
central, 366 292-93, 296

city, 237 Aquinas, 256

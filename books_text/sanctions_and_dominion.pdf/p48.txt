10 SANCTIONS AND DOMINION

lence was not to become the basis of wealth creation in Israel.
The military conquest of Canaan was to be a one-time event.

A Fool in His Folly

Martin Noth, who died as he was completing his commen-
tary on Numbers, was (and remains) one of the most respected
academic commentators on the Old Testament. Yet any normal
person who picks up his Numbers commentary and reads two
pages will think to himself: “This book is utterly incoherent. No
one in his right mind would waste his life writing something as
useless as this, Noth must have been a German.” Indeed, he
was. He was a German’s German: enormously learned, enor-
mously liberal, and enormously unreadable. His commentary
on Numbers does not bother with the mundane task of explain-
ing what any passage means. Instead, it goes on and on about
which traditions or late-date authors’ interpolations found
expression in Numbers, producing a definitively chaotic book.
The Book of Numbers is a jumble without any integrating
theme, Noth argued, because of these later insertions. Noth
wrote:

From the point of view of its contents, the book lacks unity, and it is
difficult to see any pattern in its construction.’*

There can be no question of the unity of the book of Numbers, nor of
its originating from the hand of a single author, This is already clear
from the confusion and lack of order in its contents."

Numbers participates only marginally in the great themes of the Penta-
teuchal tradition.’®

14. Martin Noth, Nwnbers: A Commentary (Philadelphia: Westminster Press, 1968),

ph
15. Tbid., p. 4.

16. Hid, p. 5.

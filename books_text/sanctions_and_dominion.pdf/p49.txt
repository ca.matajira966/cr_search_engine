Introduction 11

Martin Noth was a liberal higher critic who denied that the
Pentateuch is the inspired, authoritative, morally binding word
of God. Put more biblically, Martin Noth was a fool. “The wise
in heart will receive commandments: but a prating fool shall
fall” (Prov. 10:8). “He that trusteth in his own heart is a fool:
but whoso walketh wisely, he shall be delivered” (Prov. 28:26).
He adopted and applied the hermeneutic of higher criticism,
namely, that many people wrote the Pentateuch a millennium
after it says it was written. He invoked the evidence offered by
higher criticism: the alleged chaos of the Pentateuchal texts.
Then he assured his readers that Numbers is incoherent and
without unity, But his conclusion had nothing to do with Num-
bers; it had everything to do with Noth’s blindness. Noth and
his academic peers are blind.

And in that day shall the deaf hear the words of the book, and the eyes
of the blind shall see out of obscurity, and out of darkness (Isa. 29:18).

His watchmen are blind: they are all ignorant, they are all dumb dogs,
they cannot bark; sleeping, lying down, loving to slumber (Isa. 56:10).

Let them alone: they be blind leaders of the blind. And if the blind tead
the blind, both shall fall into the ditch (Matt. 15:14).

Contrary to Noth, the Book of Numbers is integral to the
Pentateuch, and its overriding theme reflects this: sanctions.
The book is placed exactly where it should be: book four, which
corresponds to point four of the biblical covenant model. Had
Noth understood the covenant, respected it, and paid attention
to it, he might not have concluded that Numbers possesses no
unity and “participates only marginally in the great themes of
the Pentateuchal tradition.” But Noth was a fool who did not
heed Sclomon’s counsel: “Even a fool, when he holdeth his
peace, is counted wise: and he that shutteth his lips is esteemed
a man of understanding” (Prov. 17:28) He went into print to be

Introduction 13

ed shall not be: yea, thou shalt diligently consider his place, and
it shall not be. But the meek shall inherit the earth; and shall
delight themselves in the abundance of peace” (Ps. 37:9-11).

The New Covenant in no way reverses this structure of
inheritance. On the contrary, the New Covenant reaffirms it.
“Blessed are the meek: for they shall inherit the earth” (Matt.
5:5). The New Covenant was marked by a transfer of inheri-
tance from Israel to the church. “Therefore say I unto you,
The kingdom of God shali be taken from you, and given to a
nation bringing forth the fruits thereof” (Matt. 21:43). This
transfer was visibly imposed by God through Rome’s destruc-
tion of the temple in A.D. 70."

The Book of Numbers has stood as a warning down through
the ages: the basis of covenantal inheritance is corporate coven-
ant-keeping. Numbers calls on men and nations to repent, to
turn back to God in search of the standards of righteousness. If
God was willing to disinherit the exodus generation because of
their constant complaining and their lack of courage, how
much more should the spiritual heirs of the Canaanites take
heed!

19. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987}.

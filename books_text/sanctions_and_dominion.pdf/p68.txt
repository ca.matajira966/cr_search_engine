30 SANGIIONS AND DOMINION

The armies were tribal affairs, and under them, family
affairs. This meant that warfare was intensely personal. If a
section of the army was overrun, whole families would die,
whole communities would be emptied of men. This happened
in the American Civil War (1861-65), where the fighting-age
male population of entire towns sometimes disappeared. Town
regiments signed up as units and were kept as units throughout
the war. This built a closeness of spirit, but it involved great
risk to the community. Enlistment policies were changed in
subsequent wars to prevent this. In World War II, brothers
were allowed to enlist together, but when all five Sullivan bro-
thers died on a sinking ship, the rule was changed. A modern
army is far less personal, with senior officers required to rotate
regularly through various commands. The bureaucratic impulse
has replaced the personal impulse as warfare has become more
rationalized. Such was not the case in Mosaic Isracl. The local-
ism of Mosaic Israel was reflected in their military formations.

The army was a federation. Each tribe supplied warriors. In
mustering the armed forces, a tribe might refuse to participate,
as was the case when Deborah called the army together to fight
Sisera: several tribes refused to send anyone (Judg. 5:16-17).

Mustering and Atonement

There was an cxemption in this tribal mustering: Levi. This
was the priestly tribe. The rules governing tribal mustering
seem to be in conflict. At first, God forbade Moses to number
the Levites. “For the Lorp had spoken unto Moses, saying,
Only thou shalt not number the tribe of Levi, neither take the
sum of them among the children of Israel” (Num. 1:48-49).
“But the Levites were not numbered among the children of
Israel; as the Lor commanded Moses” (Num. 2:33). God later
told Moses to number them (Num. 3:15). “All that were num-
bered of the Levites, which Moses and Aaron numbered at the
commandment of the Lorp, throughout their families, all the
males from a month old and upward, were twenty and two

Military Planning vs. Central Planning 41

missed 22,000 people who admitted that they were afraid to
fight (Judg. 7:3).

In Mosaic Israel prior to the Babylonian captivity, the male
civilian population above the age of 19 was the army (Num.
1:20). The Book of Numbers records the history of the wilder-
ness period in terms of a central theme: military preparedness,
ie., the ability of God's covenant people to impose military
sanctions. God brought the holy nation/army under a series of
sanctions in the wilderness, including military sanctions, in
order to enable Moses to evaluate the military preparedness of
the nation prior to the conquest of Canaan.

Moses began with a rag-tag army of civilians. In every sense
of the word, this was an army of conscripts. They had been
thrown out of Egypt by the Egyptians (Ex. 12:33). Moses had
not taken them out by way of Philistia for fear that they would
turn back toward Egypt in the face of war (Ex. 13:17). They
had never fought a battle prior to the war with Amalek, and
Moses had to stand with his arms above his head for them to
win (Ex. 17:8-13). They had been placed in bondage by the
Pharaoh of the oppression because he was fearful that they
might someday fight a battle alongside Egypt's invading ene-
mies (Ex. 1:10). They had been bullied for a generation by
Egyptian taskmasters, as well as taught submissiveness by their
own civil representatives. In Egypt, they had been fearful of
any confrontation with authority (Ex. 5:20-21), let alone a war.
This Egyptian training had been remarkably successful. Except
for Moses’ slaying of the taskmaster, during the entire era only
the Hebrew midwives had been courageous enough to resist.
Their non-violent actions of self-conscious deception of the civil
authorities had saved the nation (Ex. 1:19-20). They lied to a
false god (Pharaoh) in the name of the true God, and God
blessed them for this, dwelling specially with them (Ex. 1:20a).
Israel had been delivered by women. This had taken place 80
years before the exodus — a distant memory testifying to the
long-term submission of Israel’s males to pagan military power.

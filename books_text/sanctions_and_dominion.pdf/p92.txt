54 SANCTIONS AND DOMINION

omnipresence and, ultimately, its omnipotence. Because Mosaic
Israel was founded on a public repudiation of Egypt’s messianic
State, its legal order made impossible the civil government's
collecting of statistics that were unrelated to the defense of the
nation against covenant-breakers. There is nothing in the New
Covenant that altered the Old Covenant'’s view of the messianic
State. Thus, there is no reason to believe that government data-
gathering is legitimate except in the specified areas of national
defense and crime prevention. It can lawfully collect taxes, but
taxes that mandate the collection of information on private
citizens are inherently suspect. Such data must not be used for
manipulating the economy; they are to be used only for legal
purposes, to prove that someone has or has not paid his taxes.

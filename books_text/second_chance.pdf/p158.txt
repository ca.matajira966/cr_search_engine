136 Second Chance

The use of “hold fast” indicates that “being joined to” a person’s
spouse is more the idea of faithfulness, which is the true meaning of
love, and which is the correct understanding of the third term of
the marriage covenant: the term of faithfulness.

For the person considering remarriage, it means that he
should examine the new spouse to see if there are indicators that
this person will be faithful. He should examine the person’s
morals, standards, and philosophy of life; ideas have conse-
quences, and if the candidate for marriage is committed to a man-
centered system of theology and philosophy, then he will tend to
be selfish. The one investigating the degree of faithfulness should
find out if the individual being evaluated for marriage has been
divorced, and why he or she was divorced; remember, a divorced
person will tend to repeat.

The woman should look at the spouse-to-be’s work record and
habits; if he has not been able to hold a job, he will probably not
be a faithful spouse; and if he has no definite calling, he may not
think he has any definite calling to the marriage commitments
that will be demanded.

Furthermore, the investigator should even take some time to
talk to the people who work around this person, because there are
not many ways to determine faithfulness. Finally, he should look
at the church attendance record, as well as the kind of church
member he has been.

‘The key is to try to measure how effectively this candidate for
marriage will be faithful. It may take some time, but it will be
time well spent. Keep in mind that if a person is unfaithful in one
area, he will probably do the same in another.

Term #4: And they shall become one flesh” (Genesis 2:24c).

The relationship is to be consummated, just as the original cove-
nant with Adam and Eve was to be consummated on the Lord’s Day
by receiving a blessing (Genesis 2:1-3). The term is consumma-
tion through oneness. Again, although it includes the physical,
oneness is more. It is has a positive and a negative side to it, just
as the sanctions of the covenant consist of blessings and cursings.

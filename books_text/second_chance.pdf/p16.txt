xvi Second Chance

bility is to persuade your congregation and any church authorities
over you. If your church does not bring under formal public disci-
pline every member who has ever violated the “no divorce/no re-
marriage” rule, then you must threaten to resign if the congrega-
tion does not take immediate action and reverse its lax standards.
With those members who have remarried, or have married
divorced people, there can be no hesitation: excommunication
without appeal until they make a public admission of guilt and
also make restitution of some kind. The “no divorce/no remar-
riage” view argues that marrying a divorced person constitutes
adultery, and in the Bible, adultery is a capital crime (Leviticus
20:10). If your congregation refuses to excommunicate adulterers
automatically, then it is subsidizing a capital crime. You must
resign, or work so hard to reverse your church’s view that your job
is placed on the line. Persuading Pastor Sutton should be far
down on your list of priorities,

Furthermore, you should also be preaching systematically and
often that the civil government should pass legislation requiring
the execution of all persons who remarry after the appropriate
legislation is enacted. Isn’t adultery a capital crime, biblically
speaking? If such preaching makes you appear to be an extremist,
so be it. The issue is truth, not appearances. Only after you have
taken these steps should you devote time and effort to persuading
Sutton of your position. Your primary responsibility is to your
congregation and the transformation of its spiritual condition, not
straightening out Sutton’s theology.

Now, if your church really does excommunicate all those who
remarry or marry divorced persons, then Pastor Sutton and I
would be interested in hearing from you. You are not just blowing
smoke. Please send us photocopies of the relevant sections of your
church’s book of church order or discipline in which the “no
divorce/no remarriage” position is stated explicitly, which iden-
tifies those who remarry as adulterers, and which outlines pro-
cedures for automatically excommunicating all those who
Temarry, no matter what excuse they might offer. If your church
has such a requirement, then you possess a working model of how

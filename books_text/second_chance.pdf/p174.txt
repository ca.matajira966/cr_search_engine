152 Second Chance

seled the leaders of the city to execute Sheba on behalf of the king
to demonstrate their loyalty and to remove any possibility of
retaliation on the part of Joab. She was successful and the city of
counsel was saved.

This woman demonstrates perhaps the greatest skill of a coun-
selor: the ability to lead people away from the judgment of God,
or the ability to protect from God’s wrath, A counselor should be-
lieve in the reality of the judgment of God first and foremost. If he
doesn’t believe, then he will give counsel with no thought of the
consequences for either his bad counsel, or the wrong actions of
the people he is counseling; everybody loses, better, everybody
gets judged. If he doesn’t believe, then he will not try to lead peo-
ple to Christ, who removed the judgment of God from man. In-
stead, he should beware of the consequences, spiritual and other-
wise, of not obeying God’s Word.

For two people considering a second marriage, protection is
very important. The first failed marriage has already had enough
bad effect, and they probably feel as though there has been
enough judgment on them. They need to be guided away from,
not into, more judgment, so they need to be told frankly whether
or not they should remarry, or whether or not they should be
marrying each other. They need to be made aware of the negative
consequences if they marry unlawfully, and I should add, they
need to realize the consequences of obeying as well. They need to be
shown how to please, and not anger God with their new marriage!

5. Lasting Counsel

When they heard this, they were furious and took counsel to
kill them. Then one in the council stood up, a Pharisee named
Gamaliel, a teacher of the law held in respect by all the people, and
commanded them to put the apostles outside for a little while. And
he said to them: “Men of Israel, take heed to yourselves what you
intend to do regarding these men. For some time ago Theudas rose
up, claiming to be somebody. A number of men, about four hun-
dred, joined him. He was slain, and ail who obeyed him were scat-
tered and came to nothing. After this man, Judas of Galilee rose
up in the days of the census, and drew away many people after

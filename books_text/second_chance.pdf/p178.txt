156 Second Chance

George:

Tve had it. I feel trapped and I want out. . . . I've watched you
go through medical school, and I even helped. [ve tried to raise
our four kids to the best of my ability. But I’m worn out.

I think you ought to know that Richard (an associate of
George's at the hospital) and I have been having an affair for over a
year, I find him very attractive, and most of all, be doesn’t have
any children. I can’t say that I don’t love my children, but I just
don’t want to be around them any more. I’m not going to say that I
don’t ever want to see them again, but I’ve already wasted away
too much of my life in car-pools and sack lunches. Well, all of that
is over now, and I’ve got a lot of catching up to do, and I don’t have
time for them. Maybe after they have their own families and Pve
begun a new life, Pll be interested again.

As the saying goes, I’ve got places to go, and I’ve got people to
see. And, I've decided that Pm going to those places and that 'm
going to see those people with Richard. I hope you will come to see
it my way.

Mary

George was not totally surprised, but he was crushed by
Mary’s letter. The divorce was awful, one where both of them bat-
tled for the estate, except in this case, Mary didn’t want the
children, George got them and because the judge could see the
kind of person he had been married to, he was awarded the house.

But now George faced a real problem: what does a divorced
middle-aged man with four children do for a spouse? Where is he
going to find a woman who is old enough, but not too old, who
would want to marry into the demands of a family his size?
Because George was a Christian, he was not about to go sleeping
around, Besides, that would have provided a bad example for the
kids, So, he prayed and kept his eyes open.

George met Judy, a beautiful middle-aged hair stylist who
worked at, you guessed it, the place where he got his hair cut. He
made all the classic mistakes: not checking to see if she was a
Christian and marrying too soon. But he was convinced that Judy
was the woman for him. She had a son from a previous marriage,

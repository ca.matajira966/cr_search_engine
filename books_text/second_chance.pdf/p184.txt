162 Second Chance

new shoes by which he could have dominion. He was given a new
position.

Third, Abraham was given a new law. God told Abraham, “I
am Almighty God; walk before me and be blameless” (Genesis
17:1). The understanding was that Abraham could become a new
son in God’s household, but he had to be willing to live on the
terms of the head of the household.

Fourth, Abraham was given a sign which reminded him of
God's pledge, and the reception of which became his pledge to
God. Circumcision was a picture of death through the shedding of
blood. It reminded Abraham that his new life could only come
about as a result of his death, in this case his death was symboli-
cally acted out; remember that in the story of the prodigal son, the
father said, “My son is dead but is alive again,” implying that the
adoption had been a death to resurrection process. Finally, since
circumcision involved the removal of the foreskin from the organ
of reproduction, it told him that his new life had not come through
natural birth or bloodline, but that it had come through adoption.

Fifth, Abraham was given a new inheritance. As a result of his
adoption, he was told by God, “I will make you exceedingly fruit-
ful; and I will make nations of you, and kings shall come from you.
.. Also T will give to you and your descendants after you the
land in which you are a stranger, all the land of Canaan, as an
everlasting possession” (Genesis 17:6-8). He was actually consid-
ered an orphan, which is a person with no inheritance, but he
moved from this status to that of an adopted son and he received
an inheritance. I summarized this effect of adoption in That You
May Prosper:

Adoption is covenantal. It allows someone outside the family
line to become an heir. Since the Fall, man has been outside God's
family. His parents are “dead.” He lost thern when Adam sinned.
And along with his parent’s death, he even lost his inheritance.
Covenantally, man is an orphan. The only way that God can become
his parent and he can become God’s son is through adoption. He is
not, never was, nor will he ever be part of God’s essence. He can-
not go rummaging through lost archives to prove that he is a legiti-

192 Second Chance

it into the water. Then the priest shall stand the woman before the
Lord, uncover the woman’s head, and put the offering for remem-
bering in her hands, which is the grain offering of jealousy. And
the priest shall have in his hand the bitter water that brings a curse.
And the priest shall put her under oath, and say to the woman, “If
no man has lain with you, and if you have not gone astray to un-
cleanness while under your husband's authority, be free from this
bitter water that brings a curse. But if you have gone astray while
under your husband’s authority, and if you have defiled yourself
and some man other than your husband has lain with you—then
the priest shall put the woman under the oath of the curse, and he
shall say to the woman—“The Lord make you a curse and an oath
among your people, when the Lord makes your thigh rot and your
belly swell; and may this water that causes the curse go into your
stomach, and make your belly swell and your thigh rot.” Then the
woman shall say, “Amen, so be it.”

Then the priest shall write these curses in a book, and he shall
scrape them off into the bitter water. And he shall make the woman
drink the bitter water that brings a curse, and the water that brings
a curse shall enter her to become bitter. Then the priest shall take
the grain offering of jealousy from the woman's hand, shall wave
the offering before the Lord, and bring it to the altar; and the
priest shall take a handful of the offering, as its memorial portion,
burn it on the altar, and afterward make the woman drink the
water. When he has made her drink the water, then it shall be, if
she has defiled herself and behaved unfaithfully toward her hus-
band, that the water that brings a curse will enter her and become
bitter, and her belly will swell, her thigh will rot, and the woman.
will become a curse among her people. But if the woman has not
defiled herself, and is clean, then she shall be free and may con-
ceive children.

This is the law of jealousy, when a wife, while under her hus-
band’s authority, goes astray and defiles herself, or when the spirit
of jealousy comes upon a man, and he becomes jealous of his wife;
then he shall stand the woman before the Lord, and the priest shall
execute all this law upon her. Then the man shall be free from ini-
quity, but that woman shall bear the guilt (Numbers 5:16-31).

To settle this dispute, the husband was required to go to the

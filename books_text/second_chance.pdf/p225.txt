What the Church Should Do 203

what he had wanted to say. The phrase would have read: “married
[gameo] only once.” That would been clear. But he did not use
gameo; indeed, he was not talking about how often one was married.
Rather, .. . he was concerned ot about how many times a man
had been married, but about how many wives he had!

The phrase “the husband of only one wife,” strictly speaking,
permits only one interpretation: a prospective elder or deacon
[because he must be an example in all things— including marriage
practices] may not be a polygamist, The phrase means “husband of
only one wife” ai any given time. It says nothing whatsoever about
remarriage.?

So I agree with Jay Adams that an officer can be divorced and
remarried.3 But i think a couple of other Biblical lines of counsel
should be kept in mind. First, I referred to the statute of limitations
principle in the seventh chapter. I would apply it to an officer who
has been divorced and remarried to mean that he may have to
take a temporary leave of absence to wait a period of time before
he resumes his responsibilities.

Second, Paul also cites as an officer qualification that he be
“above reproach” (1 Timothy 3:2), and that he “have a good repu-
tation with outsiders” (1 Timothy 3:7). It may be that a divorce
and remarriage might affect his reputation inside and outside of
the church such that he should take a leave of absence until things
cool down. Then he could return to active duty. Given these qual-
ifications, therefore, the Church should be sensitive to the prob-
Jems associated with a new position brought about by remarriage.

3. New Cause and Effect

Conversion brings a new dynamic of cause and effect into the
issue of remarriage. Adams clearly states the matter,

2. Jay Adams, Marriage, Divorce and Remarriage in the Bible (Phillipsburg, New
‘Jersey: Presbyterian and Reformed, 1980), pp. 80-81.

3. In the Old Testament, the priest could not marry a divorced woman. He
could only marry a virgin, because he could not take the risk of any impurity,
since he had to go into the Temple and offer sacrifices (Leviticus 21:7, 13-15; Eze-
kiel 44:22). So the law would not apply to the elder in the New Covenant, strictly
speaking, since he is not offering an atoning sacrifice.

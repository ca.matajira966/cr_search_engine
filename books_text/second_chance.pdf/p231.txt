What the State Should Do 209

nantal divorce lawsuit, and in forming a covenantal adoption
through remarriage. Now we want to consider the State’s lawful
Biblical role in the same processes, as well as examine some very
critical issues that face our civilization. Once again, therefore, we
turn to an illuminating Biblical passage on the issue, the story of
John the Baptist before Herod and his unlawful wife, Herodias.

What is it about? Herod, the king of Israel had committed two
great sins. First, he had permitted unlawful remarriage. He had
unlawfully married his brother’s wife, Herodias, which was clearly
forbidden by Biblical law. He had broken the Levitical law of
affinity, “You shall not uncover the nakedness [A euphemism for,
‘You shall not marry’] of your brother’s wife” (Leviticus 18:16),
which forbids marrying an in-law.

Second, he had permitted unlawful divorce. Because he had
unlawfully married this woman, she was an unlawfully divorced
woman, who was still married to her husband. Consequently, the
State, in this case Israel’s king, was involved in sanctioning un-
Jawful marriages and in creating total disruption for the society,

More importantly, however, Herod was supposed to be a Mes-
sianic king, a king representing the Messiah, the deliverer who
was expected to bring redemption and establish righteousness
(Biblical law) on the earth. He was supposed to represent the
Messiah by applying the Word of God. When he didn't, he faced a
covenantal lawsuit by the prophet, John the Baptist, who himself
embodied the Word of God; when he spoke, his words were direct
revelation. In other words, Herod was having a divorce lawsuit
brought against him and his people by God’s messenger, because
he was permitting unlawful divorces, namely his wife's, and he
was allowing unlawful remarriages, in this case, his marriage to
Herodias.

The point: the family is a reflection of man’s relationship to
God, because the man-to-woman union is given as a picture of the
God-to-man covenant. When the State allows unlawful divorces
and it performs unlawful marriages (remarriages), it participates
in undermining this picture of man’s relationship to God. It enters
a conspiracy to project a sinful and corrupt image of the God-to-

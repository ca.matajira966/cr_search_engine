Union involves mutual consent; the dissolution of a marriage
does not. The most common form of divorce is by death. This could
be not only @ natural death, which is not strictly a divorce, but a
legal execution, which divorced the culprit from life, society, and
spouse. Those who were missionaries for idolatrous cults were
subject to death and therefore divorce (Deut. 13:1-11). The pre-
Mosaic law required death for adultery, as the Tamar incident
shows (Gen. 38:24), David expected it for his own sin (II Sam.
12:5), and it required a word from the Lord, Nathan’s message
“thou shalt not die” (II Sam. 12:13) to avoid that sentence. . . .

Thus, the Scripture, in both Old and New Testaments, has
one law with respect to marriage. The purpose of marriage is not
humanistic; it is covenantal, and therefore the reasons for divorce
cannot be humanistic and must be covenantal.

Unfortunately, divorce laws have been radically altered by
humanism. The answer, however, is not a return to Montanism.
The practice of Calvin in Geneva illustrates that a strict, covenan-
tal view of marriage and divorce is Biblical rather than having
only adultery the grounds for divorce.

The Biblical standards were clearly in force in the American
states for many years. It is interesting to note that many states
amplified the divorce by death aspect to include criminals sen-
tenced to life imprisonment,

R. J. Rushdoony*

*Rushdoony, The Institutes of Biblical Law (Nutley, NJ: Craig Press, 1973), pp.
401, 414.

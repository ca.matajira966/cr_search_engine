4 Second Chance

‘When can I Jawfully remarry? (Chapter 7)

What kind of person should I remarry? (Chapter 8)

How should I handle stepchildren problems? (Chapter 9)

What kind of counsel should I seek? (Chapter 10)

What should the Family do when involved in a divorce?
(Chapter 11) .

What should the Family do when involved in remarriage?
(Chapter 11)

What should the Church do when it faces divorce in the con-
gregation? (Chapter 12)

What should the Church do when it faces remarriage?
(Chapter 12)

What can the State do about divorce? (Ghapter 13)

What can the State do about remarriage? (Chapter 13)

What are the explicitly Biblical answers? Without even know-
ing you, I'll bet you, or someone you know, needs answers to one
or more of these questions about divorce and remarriage.

You shouldn't feel alone. As a pastor and counselor, I’ve been
made aware of people who are frustrated, and who are desperate
to get some answers for the really tough, rough questions. I've
even discovered that a lot of people are at the point of despair
because they think there aren’t any explicitly Biblical answers.
Tve also learned that the few answers that they are getting are so
irrelevant and downright wrong that it’s as though the person do-
ing the answering didn’t hear the original questions!

Why? Why aren't the questions getting answered? And why
are the few answers being given so bad? I think it’s largely because
people are looking in the wrong place for answers.

The Modern State Doesn’t Have Any Biblical Answers
Folks have looked to the State to solve their problems. More
than ever before, the State has been involved over the last fifty
years in solving our social ills, and where has it gotten us? I'll tell
you where it has gotten us.

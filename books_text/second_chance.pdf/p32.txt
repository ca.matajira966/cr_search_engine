10 Second Chance
in my book, That You May Prosper.® They are:

1, Transcendence: God is the Sovereign Creator, and so He is
the originator of all covenants.
2. Hierarchy: God establishes authorities over us in our cove-
nant with Him.
3. Ethics: God demands faithfulness, teaching a cause/effect
relationship between man’s obedience to Him and what happens in
his life.
4. Sanctions: The covenant is entered by receiving and mak-
ing promises under the condition of death.
5. Continuity: Faithfulness to the covenant is rewarded with
inheritance.
If a Biblical covenant has these five parts, so will the marriage
covenant, because marriage is without a doubt one of God’s three
required covenant institutions in history.?

The Lord has been witness between you and the wife of your
youth, with whom you have dealt treacherously; yet she is your
companion and your wife by covenant (Malachi 2:14).

One obvious application of knowing what a covenant consists of,
and knowing that marriage is a covenant, is that we can correctly
understand marriage from a Biblical point of view. But the other
application that is so critical to the concerns of Second Chance is that
we can also find the key to the problems connected with divorce
and remarriage. How so?

Marriage is a picture of the God-to-man covenant: Paul says,
“Just as the Church is subject to Christ, so let the wives be to their
own husbands in everything . . . Husbands, love your wives, just
as Christ has loved the Church” (Ephesians 5:22-25). So all we
need to do is examine how a covenant is made and dissolved at the
God-to-man level, so as to understand the principles of divorce
and remarriage at the marriage level. In other words, making or

8. Ray R, Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987).
9. Ibid,, ch. 8.

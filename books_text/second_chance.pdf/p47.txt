Marriages Are Made in Heaven 25

good. He would impute death in the same way that He had im-
puted life. He would pronounce them dead if they broke the terms of the
covenant in the same way He had pronounced creation good because He had
Julfilled the covenant. So God warned them that the principle of im-
putation was at work both ways.

What happened? Adam and Eve disobeyed God: “So when the
woman saw that the tree was good for food, that it was pleasant to
the eyes, and a tree desirable to make one wise, she took of its
fruit and ate. She also gave to her husband with her, and he ate.
Then the eyes of both of them were opened, and they knew that
they were naked; and they sewed fig leaves together and made
themselves coverings” (Genesis 3:6-7). Adam and Eve disobeyed
God; they sinned; and . . . but wait a minute! The Bible says
that they continued to live after they sinned. Didn’t God say that
they would die if they disoheyed? Did He lie? Yes He said it, and
no, He did not lie. So how did they die without dying physically?

God pronounced them dead on the basis of covenant-breaking, He says
through the Apostle Paul, “Therefore, just as through one man sin
entered the world, and death through sin, and thus death spread to
all men, because all sinned” (Romans 5:11). God allowed Adam
and Eve to remain physically alive, but they were covenantally
dead. He imputed a certain status to them according to their per-
formance, in this case their failure to fulfill the covenant, and
their physical death followed their legal, covenantal status. He
gave them the death penalty for breaking the covenant, and He
laid the foundation for the physical death penalty that appears
later in the Bible. But why did God allow them to live?

Imputation by Redemption

God redeemed man through the same principle of imputation
at the Cross of Jesus Christ. Scripture says, “For if by the one
man’s offense death reigned through the one, much more those
who receive abundance of grace and of the gift of righteousness
will reign in life through the One, Jesus Christ. Therefore, as
through one man’s offense judgment [covenantal death] came to
all men, resulting in condemnation [covenantal severance], even

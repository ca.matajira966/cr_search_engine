Marriages Are Made in Heaven 27

So, at every level of the covenant, the covenant relationship is
created by a legal declaration on the basis of covenant fulfillment,
or pledged fulfillment. It is also dissolved on the same basis. In
this case, it would be a legal declaration made on the basis of
unfaithfulness,

The key is that Moses and Jesus cite specific acts of unfaithful-
ness as the only justifiable causes of divorce. They assume the im-
putation principle when they give these reasons for divorce.
Moses says, “When a man takes a wife and marries her, and it
happens that she finds no favor in his eyes because he has found
some uncleanness in her, and he writes her a certificate of divorce,
puts it in her hand, and sends her out of his house” (Deuteronomy
24:1), and Jesus says, “Whoever divorces his wife for any reason
except sexual immorality causes her to commit adultery” (Matthew
5:32).

How could Moses and Jesus allow for divorce, yet not simul-
taneously violate the second half of the statement referred to
above: “let not man separate”? As I’ve already implied, “let not
man separate” is not the same as “Man is not ab/e to separate.” So,
it doesn’t mean that a man and woman cannot be legally sepa-
rated, that is, it doesn’t mean that a marriage covenant cannot be
dissolved. Rather, it simply means that man is commanded not to
dissolve the marriage covenant, because the only way to dissolve
it is through some particular act of unfaithfulness that falls under the
categories mentioned by Moses and Jesus.

Jesus was careful to make a distinction between the certificate of
divorce and the specific acts leading to the issuing of that certificate.
Consider the whole context of His comments in Matthew 5, The
Pharisees were trying to trap Jesus with Moses’ words, incorrectly
interpreting Moses to be saying, “You can get a divorce any of
time you feel like it for any ol’ reason, if you just purchase a
divorce certificate.” They were basically arguing for no-fault
divorce. And Jesus responded, “It has been said [by the Pharisees
who wrongly interpreted Moses], ‘Whoever divorces his wife, let
him give her a certificate of divorce’ But I say to you that whoever
divorces his wife for any reason except sexual immorality causes her

Playing with Fire Burns Out a Marriage 51

serve all the words of this law that are written in this book. . . .
then the Lord will bring upon you and your descendants extraor-
dinary plagues. . . . Also every sickness and every plague, which
is not written in the book of this law, will the Lord bring upon you
until you are destroyed” (Deuteronomy 28:59, 61).

The basic principle is that God demands the death penalty for
breaking the terms of the covenant. This principle is further sup-
ported by the list of offenses that are specifically punishable by
death or exile (excommunication): idolatry (Deuteronomy 13:10),
infant sacrifice (Leviticus 20:2), witchcraft (Deuteronomy
18:10,11), blasphemy (Leviticus 24:11-23), false prophecy (Deuter-
onomy 18:20-22), Sabbath-breaking (Exodus 31:13-17), contempt
of court (Deuteronomy 17:8-12), murder (Genesis 9:6), adultery
(Leviticus 20:10), homosexuality (Leviticus 18:22,29), bestiality
(Leviticus 18:23), rape of a non-consenting person (Deuteronomy
22:25-27), incest (Leviticus 20:11), kidnapping (Exodus 21:16),
life-threatening perjury (Leviticus 19:19-20), and incorrigibility
toward parents (Deuteronomy 21:18-21). Notice how these infrac-
tions line up with the Ten Commandments,

Commandment #1: Idolatry, blasphemy

Commandment #2: Idolatry, blasphemy

Commandment #3: Witchcraft, false prophecy

Commandment #4: Sabbath breaking

Commandment #5: Incorrigibility toward parents

Commandment #6: Murder, infant sacrifice (abortion)

Commandment #7: Adultery, homosexuality, bestiality,
incest, rape

Commandment #8: Kidnapping

Commandment #9: Perjury, contempt of court

Commandment #10: Can lead to all of the offenses

The relationship between the covenant terms and the Ten
Commandments explains the death penalties. To break a com-
mandment was to break the covenant itself. A capital violation
was a challenge to God and His covenant. An infraction of this or-

Playing with Fire Burns Out a Marriage 55

It has been said, “Whoever divorces his wife, let him give her a
certificate of divorce.” But I say to you that whoever divorces his
wife for any reason except sexual immorality causes her to commit
adultery” (Matthew 5:31-32).

Jesus is dealing with a pharisaical interpretation of Moses that
abused his comments about divorce such as, “When a man takes a
wife and marries her and it happens that she finds no favor in his
eyes because he has found some uncleanness in her, and he writes
her a certificate of divorce” (Deuteronomy 24:1). How did the
Pharisees twist Moses? According to Jesus, they were saying that
all you had to do to get a divorce was write out the certificate;
whereas Moses said that there had to be the “unclean thing,” a
specific offense.

Jesus supported the correct interpretation of Moses when He
said that “fornication” (porno), the proper interpretation, is the
basis for divorce. When He did, however, He reaffirmed the rela-
tionship between marital offense and capital offense. How? Dr.
Greg Bahnsen, former professor at Reformed Theological
Seminary, shows the parallel between Moses’ use of “uncleanness”
and Jesus’ use of “fornication.” Dr. Bahnsen proves that both of
these words generally refer to the same offenses, and furthermore,
that they generally point to the same offenses that resulted in the

death penalty,

A study of the original word for “indecent thing” [along with its
Greek equivalent] and “fornication” is very helpful at this point, for
it discloses that in the biblical literature [viz., Hebrew OT, Greek
LXX and NT] the two terms and their cognates are virtually coex-
tensive in their applications. They both denote generic, ethically
abhorrent misbehavior with the focus on sexual immorality. The
word for “indecent [shameful] thing” is used in referring to naked-
ness [e.g. Exodus 22:27; Isaiah 20:2] and the genital organ [e.g.
Exodus 20:26; I Corinthians 12:23; Revelation 16:15], and thus the
focus of its use on sexual immorality [e.g. “to make naked,” Leviti-
cus 20:18-19] is understandable. It should be observed that the
focus on sexual immorality pertains to a broad understanding of
sexual sins; that is, beyond adultery it could include rape [e.g.

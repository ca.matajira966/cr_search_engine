66 Second Chance

Here is where Nathan, the prophet, enters the picture. The
prophets of the Bible represented the Word of God (Deuteronomy
18), and they brought a covenant lawsuit on behalf of God when
the covenant terms were violated. Do you remember the cause/
effect principle of the last chapter: obedience leads to life, and
disobedience leads to death? In this case, Nathan was the ap-
pointed prosecutor, but he wanted David to condemn himself,
because the king was the supreme judge of the land. Like all pros-
ecutors, he knew that if he got David to judge himself, he would
gain certain conviction, a conviction that was absolutely neces-
sary to preserve order in the land.

Nathan lured David into a judgment on himself by the use of
the parable found at the beginning of this chapter. He told the
story of two men: a rich man who had plenty of flocks and a poor
man who only had one ewe (female) lamb. He described how the
rich man stole the poor man’s lamb when he had a visitor, and
how he refused to slaughter one of his own. He provoked a
response from David at that point. David knew that the action of
the rich man was fatal because the poor man would die without
his only lamb to eat. And so David pronounced the penalty: death
to the rich man or fourfold restitution.

Then Nathan revealed to David that he was the rich man. He
had stolen his neighbor's wife and slaughtered her, because he had
brought the death penalty on her. He had levied his own death pen-
alty on the poor innocent man, Uriah, by entering a relationship
that caused the king to murder Uriah. And now he had judged him-
self; he deserved to die! Out of his own mouth he was condemned.

As the story goes, however, David and Bathsheba were not
killed, nor did they pay restitution directly. Instead, another was
killed for them: their own son.! Nevertheless, why was the death

1, Four-fold restitution is required for stealing and then slaughtering a stolen
sheep (Exodus 22:1), for sheep are representative of vulnerable, innocent people,
David knew this, and so specified four-fold restitution (2 Samuel 12:6). Though
David repented, God exacted His four-fold restitution: the infant died, David's
son Absalom killed his son Amnon, his cousin Joab killed Absalom, and his son
Solomon had his son Adonijah killed.

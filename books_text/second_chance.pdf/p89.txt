Covenantal Execution Protects the Innocent 67

penalty required? In the last chapter we considered the basic
cause/effect principle at work. We established that death results
when the terms of the covenant are violated. But why is such a
strict sanction attached to the covenant? The answer is the princi-
ple of protection that I want to develop in this chapter.

The Principle of Protection

The covenant relationship is protected by the way it is ratified.
It is entered by accepting (trusting in) a promise(s) or oath under
the condition of death, and for this reason the oath is called self-
maledictory (“to speak evil on oneself”). There was a condition of
death in that both parties were bound by the promise as long as
they lived and also in that the guilty party was brought under the
penalty of death should he violate the promises. The promise was
a twofold sanction; it promised life, and it promised death if the
covenant were broken. The imposition of judgment or sanctions is
point four of the Biblical covenant model.?

The concept of entering a covenant by promises under the
condition of death went back to the garden. God promised, “Of
every tree you may freely eat” (Genesis 2:16), but He also prom-
ised, “Of the tree of the knowledge of good and evil you may not
eat for the day you eat of it, you will surely die” (Genesis 2:17).
God fulfilled these promises when Adam and Eve fell, as I have
demonstrated in earlier chapters, when He executed the death
penalty on them in the form of cursing (Genesis 3:14-19),

The same concept of entering a covenant by promise under
the condition of death appeared in the ancient world. A covenant
was commonly made when two people exchanged promises to do
certain things, and then they sealed the covenant arrangement by
walking between separated animals on the ground, burning the
animals after they completed the ceremony. A similar experience
occurred in the Bible, when a man named Abraham was told to
enter covenant with God almost the same way (Genesis 15:1-16).

2. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 4.

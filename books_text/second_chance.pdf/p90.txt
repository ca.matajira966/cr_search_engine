68 Second Chance

The main difference was that when it came time to walk between
the animals, God put Abraham to sleep, and He walked between
the animals. By so doing, God promised judgment on Himself for
man’s sin. This was a great act of mercy, but Abrahams potential
judgment was even greater, because with God’s promise of salva-
tion, he also received God’s promise that he would be judged with
the judgment of God if he ever broke covenant, meaning there
was an intensified version of a condition of death.

Abraham personally received God’s promise at the rite of cir-
cumcision. It was a ritual of cutting off the foreskin of the male
genital organ. It symbolized the promises of God by picturing
God’s judgment on Abraham's seed, the Seed who was eventually
Jesus Christ, and who died on the cross, the judgment of God on
Himself. It dramatically pictured the necessity of death to fulfill
the promise.

Finally, we see in the fourth part of the Biblical covenant in
Deuteronomy that it is ratified by promise under the condition of
death (Deuteronomy 27). The people of Israel gathered before
Moses and they said “amen” to the curses of the covenant. Their
“amen” was a promise in response to God’s promises made to
Abraham, and their promise was under the condition of death in
that they responded to God’s curses. Here is what I said their
“amen” meant in my book, That You May Prosper:

Man enters the covenant by saying “amen” to God’s self-matedictory oath.
In other words, “amen” means, “May God render to me the curse
that He has been willing to take on Himself, should I renege on the
covenant.” What exactly does this mean? Going back to the Abra-
hamic example again—where animals were cut in half and burned
with fire~the one who enters covenant with God is saying that
that would literally happen to Aim; he would be torn in half; the
birds would come and devour him; he would be utterly burned with
fire. Saying “amen” should not be taken lightly!

So, a covenant was entered by receiving promises under the
condition of death, meaning the death penalty would fall on the

3. Ibid., p. 84.

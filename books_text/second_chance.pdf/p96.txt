74 Second Chance

(1 Corinthians 5:6), meaning a person who has entered a rival
covenant brings the innocent party under damaging sanctions;
the fornicator “leavens” the innocent. So, Paul exhorts the Corin-
thians to expel the covenant-breaking fornicators. He could only
give such an exhortation if he reasoned according to the covenant,
and we have every reason to believe he was using the covenantal
rationale, since he was quoting from the covenant of Deuteron-
omy. He was in essence telling them to declare the covenant-
breaker and their covenant with him as dead. He was protecting
the innocent through the condition of death attached to the prom-
ises made upon entrance to the covenant. The fornicators had ac-
cepted God’s promise under the condition of death, and they had
broken the promise.

Again, since the issue is fornication, and Jesus gives “fornica-
tion” as the only justification for divorce, Paul’s words would apply
ta marriage. He refers to a relationship with a harlot as a rival
covenant when he uses the covenant phrase “one flesh.” In the
broader context, he argues that the innocent person who is mar-
ried to such a one in a rival covenant should be protected by the
death penalty which could be applied in many different forms: ex-
pulsion from the covenant community and/or capital punish-
ment. He protects the innocent in marriage with the same lan-
guage and principle that we saw in the Biblical covenant.

Applications of the Principle of Protection

In the Bible, the application of the principle of covenantal pro-
tection is made through various means of applying the death penalty.
That's right, the death penalty is implemented a number of ways,
Why? The death penalty in terms of execution was not mandatory;
it was the maximum penalty. It was only mandatory in the case of
murder. But in the case of adultery, for example, it was not man-
datory because of what the Bible says about Joseph, Mary’s hus-
band: “He, being a just man, was minded to put her away secretly”
{Matthew 1:19). Joseph was “just,” meaning he was right in what
he did, but he did not have to have her put to death. The death
penalty would be applied through several means such as: execu-

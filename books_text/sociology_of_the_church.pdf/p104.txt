90 The Sociology of the Church

mary symbol of the failure of the sanctuary to spread and multiply
is the laver of cleansing, which has no outflow until it is tipped
over in Ezekiel 47 (and compare Zechariah 14).5 Thus, the reason
for the restriction of sacramental worship to one and only one
place during the Old Covenant lies in the sin of man and conse-
quent curse and exclusion, not in the design of creation.

Of course, sin wrecked this original program, but God never
gave it up. Accordingly, throughout the Old Covenant period, we
continue to see a duality between central sanctuary and outlying
world. We have a seed-throne nation of priests, who maintain the
sanctuary; and we have outlying peoples who engage in cultural
tasks “downstream from Eden.” The plan of redemption is always
set out in terms of this duality in the Old Covenant. The differ-
ence is that instead of an outflow there must be an influx: The
peoples must come to the sanctuary, specifically to the gate of the
sanctuary because they are not allowed in, Only in the New Cov-
enant is the outflow reestablished.

When he fell, Adam had already taken up his general or kingly
task, beginning with naming the animals and receiving a helper.
This task continues down through history, though under curse.
Adam had failed to take up his special or priestly task of guarding
the garden and his wife. As a result, cherubim were appointed
guardians, and men were excluded from the priestly role and the
sabbath enthronement that goes along with it. Only a special peo-
ple, symbolically raised to priesthood, would act the role of
priests, on a provisional basis, until the Second Adam had
become enthroned on behalf of all righteous men.

Thus, we find that the gentiles carried out kingly tasks and not
true priestly tasks, but that Israel carried out both, though its
peculiar calling was to show forth the priestly task to all men, as a
prophecy of the Messiah. The nations labored only in the general
area, while Israel labored in both the general and the special
areas, with emphasis on the latter.

5, Following on the observations in footnote 3 above, it is interesting to note
that the laver of the Tabernacle becomes a “sea” in the Termple. It is that “sea” that
is the source for the waters that flow out of the sanctuary.

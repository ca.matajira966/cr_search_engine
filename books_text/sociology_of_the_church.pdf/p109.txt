Sociology: A Biblico-Historical Approach 95

read that Abraham sent them away to the “land of the East.” In
the beginning, Adam and Eve were cast out of Eden at the east
gate, and they brought sacrifices to that east gate (Gen. 3:24; 4:3,
4; with all the Tabernacle and Temple passages that connect the
door with the east side). By stationing his sons to the east, Abra-
ham was putting them in a position to receive blessing influences
from the seed people, and in a position to bring their tithes and
offerings to that peculiar nation of priests who maintained the
sanctuary land,

Havilah is where Ishmael and his people settled, according to
Genesis 25:18. It is a land directly related to Eden, Now, because
of things said in the New Testament, we have the impression that
Ishmael was not among those elect unto salvation. All Romans
9:6-9 says, however, is that Ishmael was not set aside to be the
seed, the peculiar children of God. These verses speak of election
to service, as it is usually called. The subsequent verses, dealing
with Jacob and Esau, have to do with election unto eternal life,
and preterition.

Similarly, in Galatians 4:21-31, Paul refers to Ishmael’s “perse-
cution” of Isaac, and from this and the rest of what is said, we
draw the conclusion that the downstream, “world” peoples were in
fact enemies of the Edenic, sanctuary people. Indeed, this was
often if not usually the case. It was, however, not always the case.
Indeed, the “persecution” spoken of took a peculiar form in Gene-
sis 21:9, for the Hebrew text docs not say that Sarah saw Ishmael
“mocking,” but that she saw him “laughing.” Is it a sin to laugh?
No, and so we are caused to look further for an understanding of
the problem Sarah faced. We can take Paul’s word for it that
Ishmael’s “laughter” demonstrated hostility, and was part of a
“persecution.” In calling attention only to laughter, however, the
text of Genesis intends something specific.

What was the issuc before Sarah? Isaac means “he laughs.”
This name was given him by God when Abraham laughed to hear
that he would have a son by Sarah (cf. Gen, 17:17, 19; 21:3; and
also 18:12-15; 21:6). The laughter of Sarah and Abraham was in
response to God’s surprise gift of a son to parents too old to have
children. The gospel is God's great surprise in history, a surprise

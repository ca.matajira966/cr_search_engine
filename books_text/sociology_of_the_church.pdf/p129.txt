Soctology: A Biblico-Historical Approach 115

heavenly clocks: Genesis 1:14 says that the lights in the heavens
govern signs and seasons, which should be translated “appointed
times,” (There is a different Hebrew word for seasons of the natu-
ral year.) From reading the Old Testament, we see worship meet-
ings taking place on the new mouns of each month, as well as at
seven special festive times during the year (Lev. 23). The second
system of worship was the weckly sabbath.

After man was cast out of Eden, worship was crippled. What
man was excluded from was not the right to hear God’s Word and
to offer Him praise. Rather, he was excluded from the sacrament
of the Tree of Life. As we have seen, in various ways certain peo-
ple were set apart for the occasional privilege of eating the sacra-
ment, but most continued to be excluded. In Israel, weekly sab-
bath worship was non-sacramental, while Passover among the an-
nual feasts was sacramental. Synaxis and eucharist were segre-
gated in synagogue and Temple. All of this shows the exclusion of
man from the fulness of worship because of sin.

In the New Covenant, these segregations are replaced. Wor-
ship still has non-sacramental (synaxis) and sacramental (cuchar-
ist) elements, but both are conducted together at the same time, in
terms of both a weekly and an annual cycle. Both are conducted
at every place, so that every place becomes potentially a central
sanctuary. There is no longer a distinction between priests and
non-priests; rather, every man and every woman is a priest.

This points to that final coalescence of culture and cult, of
heaven and earth, which will be the characteristic of the New
Covenant in its final (cternal) phase. Today it is still necessary for
us to distinguish between special and ordinary times, and to hold
sacramental worship at the special times, called Lord’s Days (or
Days of the Lord). Today it is still necessary for us to distinguish
between special and ordinary places, the special place being the
environment created around the special sacramental Presence,
Indeed, in a developed Christian culture, the church building
forms a sanctuary for the accused and the oppressed. And today,
even though all are priests, yet there are still some set apart by the
laying on of hands to be elders, special officers in the church.
They and they alone have oversight of the sacraments,

 

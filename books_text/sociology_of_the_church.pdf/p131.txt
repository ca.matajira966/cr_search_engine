Sociology: A Biblico-Historical Approach M17

times for special worship. This point is greatly debated, since the
Puritan parties in the various churches have always maintained
that the sabbath appointment of the Old Covenant is simply
transferred to the New Covenant. For that reason we have to
devote a little more space to the sabbath question.

Paul is quite clear that the sabbaths and annual festivals of the
Old Covenant are cancelled in the New (Col. 2:16; Gal. 4:10f.;
Rom. 14:5). They are part of the “elementary principles,” the
foundational ordinances of the First Creation order. To do justice
to the concerns of the Puritan parties, we have to say that the pat-
tern of New Covenant worship is definitely sabbatical and festive,
and that the Lord’s Day on the first day of the week (1 Cor. 16:2;
Acts 20:7; Rev. 1:10) indicates a continuity of pattern, To do jus-
tice to the Church Fathers and to the Reformers (all of whom
were non-sabbatarian), we have to note that in the New Cove-
nant, “the Son of Man is Lord of the sabbath” (Mark 2:28), The
phrase “son of Man” means Second Adam, and is used to refer to
office-bearers of the Old Covenant (throughout Ezekiel primar-
ily), as well as to officers of the New. Jesus’ illustration demon-
strates this. The decision to eat the consecrated bread, which was
unlawful, was made by the two office-bearers David and Abia-
thar.?3 They had the right to make “adjustments” in the basic pat-
tern set up by God, applying the law to new situations,>* While this
is tremendously exceptional in the Old Covenant, it is a founda-
tional characteristic of the New Covenant: The officers of the
church determine the times and places of meetings. In times of
persecution, they may designate times other than the first day of
the week for meetings, if that is necessary.

There is a sense in which the new creation has come, and a
sense in which it has not. We still live in pre-resurrection, Adamic

33. Jesus’ example of sabbath “breaking” is the cating of the Tabernacle show-
bread. "Not an example that would readily come to our minds! Apparently we are
to regard the Tabernacle as existing in perpetual sabbath time, and its bread as a
perpetual sacrament. Thus, any layperson entering that space was also entering
sabbath time, Here we sce again the connection among land, time, and geneal-
ogy thal existed in the Old Covenant,

34. On the need to make such applications, based on general wisdom derived
from internalizing the Law, see my book The Law of the Covmant.

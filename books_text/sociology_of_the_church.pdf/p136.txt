122 The Sociology of the Church

ter was much more complicated. Whether someone was in the
“world” or not depended on whether he was “downstream” from
you. From Aaron’s standpoint, everyone else was in the world,
and he alone had the privileges of being fully in the church—but
Aaron was painfully aware that he was excluded from the greatest
privilege of continual dwelling in the Presence, and of drinking
wine with the King of kings. From the standpoint of an Israelite,
all gentiles, even converted ones, were in the world.

The principle of outflow is, uf course, still in force. We no
longer have an outflow from an carthly Garden to the four corners
of the world. Nor do we have an outflow from the Temple into
Jerusalem, Judaea, and to the uttermost parts of the earth.
Rather, we have an outflow from heaven to earth. When heaven is
sct up on earth for a temporary period during worship, the people
of God are briefly transfigured, like Moses on the Mount, When
they come out of heaven to earth, they bring the demon-destroy-
ing, world-transforming glory of God with them (Matt. 17:1-20).
Having drunk of Christ, now living water flows out of them into
the world ( John 7:37-38), water that will not stop its work until all
has been transformed (Ezekiel 47).

I should like to close this essay with a few remarks about cove-
nant theology and dispensationalism, What we may call the cove-
nant theology perspective sees the church as being the same in all
ages. From the covenant theology perspective, circumcision for
Israel is the same as baptism for the New Covenant, and is simply
the mark of inclusion in the church. One problem with traditional
covenant theology is that it is unable to explain the phenomenon
of uncircumcised but clearly converted gentiles. The major prob-
lem with traditional covenant theology is that it does not do justice
to the radical nature of the New Covenant, and its discontinuity
with the Old. Traditional covenant theology subsumes both Old
and New Covenants under one overarching “covenant of grace.”
In fact, however, the New Covenant is simply the Old Covenant
dead, resurrected, and transfigured. Christ, born under the Law,
became the embodiment of the Law, and in His death on the
cross, the Law and the Old Covenant died, In His resurrection,
these came to life again, but in transfigured form, Traditional

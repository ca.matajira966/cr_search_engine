Thoughts on Modern Protestantism 131

what the New Testament clearly teaches. We live in a country that
is little better than an insane asylum, because most people go
around pretending that Christ is not King, and that they can
break His laws with impunity. Such people are crazy. ‘hey are
not adjusted to the truth about the real world in which they live.
Our goal is to persuade men of this truth, so that they will stop
suppressing the truth in unrighteousness. They need to recognize
the fact that Christ is King, and bow the knee. When that hap-
pens, the judgments of the Kingdom will turn to blessings. We
don’t make Christ King; we recognize that He is already King,

2. It is a fact that the church of Jesus Christ is unified. Jesus
prayed the Father in John 17 that we might be one, and the Father
does not deny the petitions of the Son. Therefore, we are one. We eat
of one Christ. We hearken to one Word. There is one Lord, one
faith, one baptism, etc. Anyone who denies this is insane, not ad-
justed to reality. Thus, we cannot unite the church, and church unity
is not a problem, any more than we can make America a theocracy.
What we need is for people to stop pretending that the church is not
united, because such a pretense is a denial of the truth, When men
recognize the truth, and stop being fooled by vain appearances, then
the judgment upon the church will be turned to blessing.

To recognize the truth of church unity means to grant prima
facie recognition to the orders and government of all other Trini-
tarian churches, even to the church in Sardis. It means allowing
to communion any baptized person who has not been excommun-
icated and who is a member of a local body. We cannot make the
church united by negotiation. Rather, we must simply confess
that the church is in truth one, and act accordingly."

 

11. What if a man is wrongfully excommunicated? Suppose a man applies for
membership in our church who has been excommunicated by a baptist church.
He claims that he was excommunicated unlawfully because he had come to be-
lieve in predestination and infant baptism. In that case, we should grant prima
facie recognition to the decision of the baptist church, and talk with the pastor to
make sure there was not some other reason for the excommunication. If, m fact,
the man had been wrongfully excommunicated, and things could not be worked
out, then we would simply have to refuse recognition of this one particular ac-
tion. That would not require us, however, to declare the church totally apostate.
Nowadays, of course, the opposite is true, If'a man is declared excommunicate,

The Three Faces of Protestantism 139

tian emperors, not for emperors to judge bishops” (p. 9). In fact,
“in 390, Ambrose went so far as to excommunicate the emperor
Theodosius himself, and Theodosius eventually acknowledged his
faults and performed a public penance in the cathedral of Milan
before being readmitted to communion” (p. 9). Later on, this
same type of battle had to be fought with the rulers of the tribes of
northern Europe.

It became very easy, during the Middle Ages, for the rulers to
northern Europe to pretend that the issue was not church and
state, but rather a cultural battle between southern and northern
Europe. The Pope rules in Italy, they maintained, and he wants
to bring us under his yoke as well. By stirring up hatred for the
Papal court, the kings of Europe concealed their real motive,
which was to dominate the church in their lands.

During the distressing years after the death of Charlemagne
and the rise of the civilization of the high middle ages, the tradi-
tional pagan culture of northern Europe made many inroads into
dominating the church. “All previously established institutions
suffered, not least the church. In every part of Europe ecclesiasti-
cal lands and offices fell under the control of lay lords” (p. 24). It
was easy for the civil powers to point out the crimes of the clergy,
real or invented, as a pretext for taking over the churches — while
the civil rulers themselves lived even more wicked lives!

Gradually, both church and state recovered from the years of
turmoil, and right away there was a tremendous conflict over who
would control the churches of northern Europe. The conflict
reached its first climax in the battle between Henry IV and Pope
Gregory VII. “Henry could not give up the right of appointing
bishops without abandoning all hope of welding Germany into a
united monarchy” (p. 45). In other words, it was a purely statist
goal that led the imperial forces to try to control the church. After
all, why on earth should Germany be united as one big powerful
state? The existing confederacy could join hands to repel invad-
ers, $0 it was just plain statism that motivated the imperial court’s
opposition to “Rome” and “Papacy.” Imperial theologians de-
fended the right of the king to rule the church, while church theol-
ogians argued for the integrity of church government, and the

The Three Faces of Protestantism 141

kind of way, the transcendent power that created the church came
to be identified with the church herself. Instead of being depend-
ent upon the mystical body of Christ for her life, the church began
to see herself (partly) as the mystical body of Christ. This termi-
nological shift by itself would not seem to mean very much, but
the mass of ideas and associations that went along with it were
powerful. The church began to hold what I call a “deposit” view of
grace, the idea that grace has been deposited in the church and
the church manages and dispenses grace. This “deposit” view of
grace works against a “receptionist” view, which says that the
church is nothing in herself, and must get everything from her
Lord. Back when corpus mysticum referred to the sacrament, the
church clearly knew that she got everything she had from Christ;
now, however, it seemed that the church had power in her own
right. Indeed, for some the sacrament got its efficacy from the
church—a reversal of the true order.

The third and final shift came when the phrase “mystical body
of Christ” began to give way to “mystical body of the church,” Of
course, theologians such as Aquinas did not stop talking about the
sacrament and how Christ creates the church; nor did they stop
speaking of the church as the mystical body of Christ. The shift
was this: The political aspect of the church was separated off, to a
great extent, from the sacraments. The political aspect of the
church, the “mystical body of the church,” was centered on the
Pope as its political, earthly head. Because these ideas are strange
to us, putting it simplistically may be of help—so think of it this
way: The “spiritual” church is the body of Christ gathered around
the Eucharist; but the “earthly” political church is a political body
gathered around the Pope. Thus Papal theologians could say, “the
church compares with a political congregation of men, and the
pope is like to a king in his realm on account of his plenitude of
power.”* As Kantorowicz goes on to say, “it was a long way from
the liturgy and the sacramental corpus mysticum to the mystical pol-
ity headed by the Pope.”$

4. fbid., p. 203.
5. fbid., p. 205.

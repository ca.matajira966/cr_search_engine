Conversion 157

Reactions

Let us return now to the matter of conversion experiences.
The neo-Puritan movement reacted strongly against “easy believ-
ism.” From my experience, they tended to substitute “hard believ-
ism” for it. The neo-Puritans complained that the campus conver-
sion experience is too superficial: People aren’t warned about hell,
about the suffering that Christians will face, about predestination,
ete.

My problem with the neo-Puritan critique of campus conver-
sion experiences is the same as my problem with campus conver-
sionism. Both groups act as if some big crisis or decision were nec-
essary to come into the faith. Both groups ignore the reality of the
faith of young children, (In fact, both groups are heavily Baptist,
thus typically American, in orientation; the neo-Puritans being
almost to a man Reformed Baptists.) Both groups put too much
stress on an initial conversion experience. The neo-Puritans don’t
like the soft-sell “easy” conversion; they want a hard-sell gospel
with all the hard facts brought out first. They seem to want to
manipulate “true conversions,” and eliminate “stony ground and
thorny ground” conversions. This, however, I do not think is Bib-
lical. The Sower sowed the stony and the thorny ground, and did
not object to the plants that sprang up from his “easy and free”
sowing. Not all persevered, however, a fact that the Sower also
recognized (Matt. 13:4-9, 18-23).

Perseverance is the real issue here. There is no need to react
against simple evangelistic methods, such as the “Four Spiritual
Laws.” ‘The issue is not initial conversion. Rather, the issue is per-
severance, Once people are brought into the faith, they necd to be
shepherded into maturity.

The Four Spiritual Laws

After all, what is so terribly wrong with the “Four Spiritual
Laws”? The Bible says that God created man good, and offered
him a wonderful plan, That’s law one, and it is exactly where the
Bible begins. The Bible says that man rebelled, and came under
God's wrath, and thus cannot know God's wonderful plan. That's

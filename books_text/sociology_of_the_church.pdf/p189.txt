8

CHRISTIAN ZIONISM
AND MESSIANIC JUDAISM

One of the most grotesque aspects of the sociology of modern
American protestantism is the phenomenon of Christian Zionism.
While related to the theology of dispensationalism, Christian
Zionism is actually something altogether different theologically.
The purpose of this chapter is to explore this movement, and in
particular to point out its grievously heretical theoretical basis. To
facilitate discussion, we shall interact with the expressed beliefs of
a Christian Zionist, Jerry Falwell. We close with a brief note on
Messianic Judaism.

Zionism

Zionism is a political movement built on the belief that the
Jewish people deserve by right to possess the land of Palestine as
their own. During the last part of the 19th and first part of the 20th
centuries, Zionism gained support throughout the Christian
West. This was due to two factors: the influence that Jewish
wealth could purchase among politicians, and the emotional sup-
port that the history of Jewish tribulation could elicit from a
Christianized public conscience.!

With this support, Zionist guerillas succeeded in throwing
Palestine into havoc during the late 1940s, and eventually took
over that land. The result was the disenfranchisement of the peo-
ple who had historically dwelt there. The Moslem Palestinians

1, On the former aspect, see Ronald Sanders, The High Walls of Jerusalem: A
History of the Balfour Declaration and the Birth of the British Mandate for Palestine (New
York: Holt, Rinehart, & Winston, 1984)

175

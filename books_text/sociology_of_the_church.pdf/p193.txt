Christian Zionism and Messianic Judaism 179

We presently live in the “Church Age,” and God’s people today are
Christians, the Church. At the present time, the Jews are apostate
enemies of God and of Christ, and are under God’s judgment un-
til they repent.

Someday soon (It’s always soon!), Christ will return to earth
invisibly and snatch away all the Church-Christians (this is called
the “Rapture” of the saints), At that point, God will go back to
dealing with Israel. There will be a seven-year period called “The
Tribulation,” and during that period, apostate Jewry will form an
anti-God alliance with the Beast, but God will begin to convert
the Jews, and in time the Beast will turn and begin to persecute
these converted Jews. Just when things look hopeless, Christ will
return and inaugurate the Millennium.

One other point to note: There are absolutely no signs that the
Rapture of the Church is near. It will come “as a thief in the
night.”

Now, this entire scheme, though popular in recent years, has
no roots in historic Christian interpretation of the Scriptures, and
at present it is collapsing under the weight of criticism from Bible-
believing scholars of a more historically orthodox persuasion, All
the same, there are several things to note,

First, by teaching that there are no signs that precede the Rap-
ture, dispensationalism clearly implies that the modern State of
Israel has nothing to do with Bible prophecy. If Israel collapsed to-
morrow, it would make no difference. The existence of the State of
Israel, while it may encourage dispensationalists to believe that
the Rapture is near, is of no theologically prophetic importance.

Second, dispensationalism teaches that Jews of today, and
even into the Tribulation period, are apostate, and this certainly
implies that they are under the wrath and judgment of God.
Christians should minister to them, and try to convert them, and
show them all kindness as fellow human beings; but Christians
should understand that during the Church Age, the Jews are not the peo-
ple of God. Rather, the Church is the people of God today.

Third, by teaching that Israel is “set aside” during the Church
Age, dispensationalism clearly implies that the promises made to
Israel are also “set aside” during that period. The land promise,

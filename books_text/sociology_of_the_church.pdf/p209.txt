Should Churches Incorporate? 195

the state to require property taxes of the churches. We could have
applied for an exemption, but to do so meant filling out a long
form, telling the state all about our various ministries. This we
were unwilling to do. We contested the matter for well over a year,
but without success. We could have fought the measure further,
but we almost certainly would have lost, costing us much time
and money that we thought, before God, had better go for more
productive things. We therefore adopted a time honored com-
promise measure, which has been used before in the history of the
church.

In France, the clergy used to vote a “free gift” to the king,
which they were bound by force and custom to do. They refused
to call it a “tax,” since the church is not subject to taxation from
earthly kings. The state thought they were taxing the church, but
the church knew that they were merely giving a free bribe.? With
this in mind, the church of which I am a member decided to enter
the following official statement into her minutes:

Official Statement of Westminster Presbyterian Church

Whereas, certain civil taxing authorities have notified
Westminster Presbyterian Church that the church owes the
payment of certain monies which they term “property taxes,”
“penalty & interest,” and “collection penalty”; and,

Whereas, the church cannot be faithful to the Lord Jesus
Christ and willingly pay tithe money in taxes to Caesar; and,

Whereas, the State of Texas presently claims the coercive
power to lay property taxes on the property owned by the
church of Jesus Christ; and,

Whereas, Westminster Presbyterian Church has not been
given the resources to mount an assault on this practice of taxa-
tion (Luke 14:28-32); and,

Whereas, the sons of the Kingdom are exempt from earthly
taxation, so that Jesus Christ Himself was willing to contribute
[give] money rather than give offense, thereby establishing a

 

3, My source for this point is a brief reference in Henri de Lubac, The Mystery
of the Supernatural (English edition; New York: Herder & Herder, 1967), p. 97

222 The Sociology of the Church

tween hospitality shown to the saints and that shown to outsiders,
for the phrase “washed the saints’ feet” is in part a reference to the
practice of hospitality (cf. Gen, 18:4; 19:2; 24:32; 43:24; 1 Sam.
25:41; 2 Sam. 11:8; Luke 7:44; John 13:5). Here as elsewhere we
are enjoined to do good to all men, but especially to those of the
household of the faith (Gal. 6:10).

The repeated injunctions in the Old Testament to care for the
alien and sojourner in the land are reflections of the concept of hospi-
tality (see, for example, Ex. 22:21; Lev. 19:34; 25:35; Num. 35:1
Deut. 10:19; 27:19; 31:12; Jer. 7:6). The stranger was under the pro-
tection of the Lorn, in His house (land), having crossed the thres-
hold of His house (the Jordan), and thus being entitled to hospitality.

The only persons excluded from Christian hospitality were ex-
communicated persons (1 Cor. 5:9-13) and perhaps false teachers
(2 John 10), As regards the latter passage, John Stott in his fine com-
mentary on the epistles of John points out, first, that it is only teach-
ers, not all adherents to false teaching, who are to be excluded. Stott
also points out that the specific heresy was the denial of the true doc-
trine of incarnation, not some lesser matter. Third, Stott calls atten-
tion to the fact that the epistle is written to a house-church, and thus
it is likely that the prohibition is actually to the church, not to in-
dividual households, The church must not extend an official wel-
come to a false teacher (.e., allow him to teach in their mids
possibly an individual Christian household might show hospitality
to the false teacher in an effort to correct his errors.!

 

 

Holistic Man

The Biblical virtue of hospitality, specifically, ministry to the
whole person in a structured environment, points us to the Bibli-
cal concept of man. Here we arrive at one of the major errors that
has cropped up historically in the church, for the Bible teaches
neither a bipartite nor a tripartite view of man. Rather, the Scrip-
ture teaches that man is a unity, not composed of several parts,
but acting in several dimensions or spheres of life. Man is a spirit

 

1. John R, W. Stott, The Epistles of John. The Tyndale New Testament Com-
meniaries 19 (Grand Rapids: Eerdmans, 1974), comm. ad loc.

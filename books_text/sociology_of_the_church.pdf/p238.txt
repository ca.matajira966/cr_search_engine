224 The Sociology of the Church

called the doctrine of the primacy of the intellect. Because the
brain was regarded as the most important part of man, the most
important work of the church was to communicate intellectual in-
formation to that brain, Thus, instead of the primacy of the
Word, the church fell into the primacy of preaching.

What the Bible teaches, however, is the primacy of the Word
in the work and worship of the church. This means, of course, the
Word read, proclaimed, and taught, but it also means the Word
sung (in Psalms, Bible songs, and Psalm-like hymns), the Word
prayed, the Word obeyed and implemented from house to house,
and the Word made visible and experienced (in the sacraments).
A church that practices the primacy of the Word will have a
healthy balance among all the elements of worship and life, and
will not be a preacher-centered church. The primacy of preach-
ing, however, leads to the primacy of the preacher, the so-called
“three office view,” and all the problems attendant with that.?

The Primacy of the Preacher

There are two large problems that afflict the overly intellec-
tualized church: the primacy of preaching and the problem of re-
vivalism (next section), The primacy of preaching means the pri-
macy of the preacher. It is understandable that the Reformation
resulted in a great emphasis on preaching and teaching the Word.
For centuries, little or no such instruction had been carried on.
Incredible ignorance prevailed all over Europe. Moreover, when
the Reform began, the established church strongly opposed the
teaching of the Bible. Thus, the Reformation was forged in a cru-
cible in which one of the principle elements was preaching. All the
same, the Reformers did not hold to the primacy of preaching in
the sense that their later followers did. John Calvin, for instance,
wanted the Lord’s Supper to be administered in connection with
every preaching service, for the Word should always be made visi-
ble when it is preached, The Reformers emphasized the singing of
the Word, and the congregational praying of the Word in the use

3. See the End Note at end of this essay.

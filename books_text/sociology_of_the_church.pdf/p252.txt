238 The Sociology of the Church

ages thereafter. At Sinai, when God established the covenant with
Isracl, Moses and the elders ate with God (Ex. 24). At the Feast of
Tabernacles, the people were to cat in the presence of God and re-
joice (Deut. 14:22-27). In the wilderness, the people ate manna
and drank water from the rock, both of which were sacraments of
Christ (John 6; 1 Cor. 10:1-5). The milk and honey in the land
(house) of promise were tokens of God’s presence and blessing.
And we can go on and on, not to speak of the other feasts in
Israel, and the Peace Sacrifice that the family shared with the
priest and with the Lord.

Are these all “spiritual” meals? Away with such internalized
Greck nonsense! Of course what matters most is the presence of
Christ, and fellowship with Him, but He has ordained that fel-
lowship to take place at a meal. He invites us over for supper every
week, and we decide to cat with Him four times a year, Do you
think He might possibly be offended? He invites His enemies, in
the Gospel, to join Him for dinner, but we encourage men to con-
template an absent Christ in their souls. Is our evangelistic dis-
play askew?

The Lord’s Supper is not some mystery kept hidden from the
view of the world. Nor is it some mystical rite to be kept “special”
by infrequent observance. It is as simple as dinner with Jesus, and
more profound than any theologian can ever fully understand.

The Lord’s Supper does not have an exclusively backward
orientation. It is a Medieval perversion to focus only on the death
of Christ in the Lord’s Supper, The emphasis in Scripture is
equally on the active presence of Christ at His Supper, and on the
Supper’s prophecy that He will return. Holy Communion is not a
morbid event, but a feast. Let the churches celebrate it as a feast,
before the cyes of the world, so that the unconverted will realize
the full extent of what they are being invited to partake of.

The Time of the Feast

Christ, as God, is present everywhere. Christ, as King and
elder Brother and Guide to His people is present with them all the
time. The question is whether there is any special presence of

God's Hospitality and Holistic Evangelism 241

is not spoken of as a day of rest but as a day of worship, This
raises the possibility that the day of rest, for some people, might
be another day than the day of worship —as indeed is the case for
ministers and for those engaged in works of mercy and necessity.
For the most part, worship and rest should coincide, as they do in
Christ.

The Lord's Day clearly begins with sunrise and continues after
sunset, The sunrise is a sign or token of the New Covenant (Mal.
4:2; 2 Sam. 23:4; Is. 60:1-3), On the first Lord’s Day, Jesus met
with the disciples after sunset and shared bread and wine with
them then (Luke 24:29-43; John 20:19), The preaching of the Day
of Pentecost came in the morning (Acts 2:15), while the Lord’s
Supper was eaten on the evening of the Lord’s Day {1 Cor.
11:20-22, 33-34).

On balance, then, it seems that we should ideally begin our
restored-creation-sabbath rest on Saturday night (unless we must
rest some other day), have a preaching service Sunday morning,
and the Lord’s Supper Sunday night. All things considered, the
Lord’s Supper is an evening meal, as was the Passover, so the
most appropriate time for special Eucharistic worship is Sunday
evening. The fact that people brought their meals to the Agape
Feast (Love Feast) before eating the Lord’s Supper shows that
preparation of food is not forbidden on the Lord’s Day. Thus, we
may wisely and joyfully reinstitute the Biblical Agape Feast (cov-
ered dish supper) for Sunday nights, at least occasionally.

The Lord’s Supper is not optional on the Lord’s Day, The
Bible never contemplates divorcing these things. God commands
our presence at His table. Ordinarily, it is not wise to set up extra
communion services on other days of the week. It is true that the
New Covenant is a kind of perpetual sabbath and Lord’s Day, but
this does not eliminate the special weekly Lord’s Day. In times of
revival, such as are seen in Acts 2:42, 46 and in Calvin’s Geneva,
daily preaching services may occur, and perhaps the Lord’s Sup-
per would be appropriate on a daily basis.

In the writings of theologians, there is a preoccupation with
the question of whether or not the efficacy of the sacrament is the
same as or different from that of the preached Word and general

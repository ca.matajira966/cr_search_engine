242 The Sociology of the Church

daily faith. This question arises only because the Biblical unity of
sabbath, proclamation, sacrament, and gathered priesthood has
been ripped asunder. The Bible cannot answer questions concern-
ing the supposed sacramental status of the sabbath, or what there
is “extra” about the communion service. As Calvin pointed out,
the sacrament is in the nature of a miraculous visible seal to the
preached word.!? Just as Word (authority), Presence, and miracle
(power, control) go together in the Scripture, so Word, Presence,
and sacrament go together in the New Covenant.

If we distinguish the sabbath day from the six cultural days,
and sabbatical activity (special worship, rest, and recreational de-
light in the works of God and man) from cultural activity (creative
work and labor in restraining the curse), we can also distinguish
the special presence of Christ, as heaven is opened on the sab-
bath, from His general presence with His people on the cultural
days. Thus, we can distinguish an informal Bible study or a Wed-
nesday night meeting from a sabbath worship festival. Moreover,
we can distinguish the official gathering of the priesthood under
the leadership of special priests (elders) from general informal
gatherings of the priesthood on the six cultural days for Bible
study. It is the power of the special priest to bind and loose, to ad-
mit to sealing ordinances or to excommunicate, to place God's
blessing on the people (not merely to invoke it, Num. 6:23-27)
and to curse God’s enemies,

Thus, the special efficacy of the sacraments is part and parcel
of the special efficacy of sabbath worship, the blessing of special
priests, the special “official” proclamation of the Word, and pre-
eminently the special presence of Jesus. The difference between
this and daily Christian experience is not normative, as if some-
thing different in the way of principle were involved; nor is it exis-
tential, as if we exercise some other kind of faith; but it is situa-
tional, carricd out on the sabbath day in the special presence of
God, the angels, the spirits of just men made perfect, and the
gathered priesthood (Heb. 12:22-25).

12. Roland S. Wallace, Calvin’ Doctrine of the Word and Sacrament (Tyler, TX:
Geneva Ministries, 1982), pp. 137-141.

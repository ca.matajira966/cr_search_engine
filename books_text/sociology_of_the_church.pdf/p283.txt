Triumphalistic Investiture 269

up to God, and then received it back for the use of the church of
the Old Testament.

Now, clearly there is nothing wrong, then, with lifting up the
collection plates, and the bread and wine to be used later in the
Lord’s Supper, toward heaven during the offertory prayer. The
gifts are given to God, and then He gives them back to the elders to
administer, When we look at it this way, lifting up the offering and
the elements is thoroughly evangelical, Biblical, and Reformed.

But, it is possible to go through the same actions with the
bread and wine, but with a different purpose. They can be lifted
up so that people can worship and adore them, as if the bread and
wine have been transubstantiated into the re-sacrificed body of
Christ. Thus, the Westminster Confession of Faith states in 29:4,
that “worshipping the elements, the lifting them up, or carrying
them about for adoration . . . are all contrary to the nature of this
sacrament, and to the institution of Christ.” Notice that the WCF
only forbids lifting them up for the purpose of adoration; it does not
forbid lifting them up in the Biblical manner of a heave offering.

On this subject, Bucer wrote, “We read that the Lord took
bread in his hands and blessed it, that is to say, he gave thanks:
and this was the custom of the religion of ancient times. For in
order to arouse in the people a greater disposition to offer thanks
to God, the men who presided over the sacred rite used to lift up
the gifts of God for which thanks were due for all the Lord’s bless-
ings (as the gifts themselves suggested): and they set them forward
in the sight of the people.” Because of the practice of adoring the
elements, however, Bucer wanted the practice set aside, at least
temporarily.9

Now, the Reformers found out that the people had been pro-
grammed through decades (even centuries) of bad teaching to
view the ceremonial in this wrong way. When the people saw the
bishop or the presbyter robed in glorious vestments, they did not

 

9. Bucer, Censura, pp. 56ff. Bucer’s explanation of the rite is, unfortunately,
overly psychological. Buccr does not see any value in bodily action as part of
worship, but reduces the relevance of the action to a means of stimulating the
people,

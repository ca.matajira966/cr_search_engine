Triumphalistic Investiture 271

state, and the discussion of vestments was soon absorbed by that
larger conflict.

From what I have been able to uncover about the Vestarian
controversies, it does not seem that the early Puritans rejected
vestments on the grounds of the regulative principle of worship.
Rather, these Puritan Reformers were pastorally concerned lest
their people be led astray by the continuance of customs long
associated with error, and they wanted at least a fast from these
customs for a time. More importantly, the Puritans asserted that
the local churches, or at least bishoprics, had the right to decide
these matters for themselves, without statist interference. Since
the state insisted on certain kinds of vestments, the Puritans in-
sisted that they had the right, maybe even the duty at this point,
not to wear them, They wore the Genevan gown instead. !?

(Later radicals, baptists, and congregationalists found ways to
make everything under the sun a matter of the regulative principle;
but if that is the case, then vestments would be mandatory, since the
Bible teaches that the church of the Old Testament used them!)

To summarize; Because of serious abuses connected with tra-
ditional vestments, the Swiss and many English and Scottish Re-
formers wanted either to replace these with other kinds of vest-
ments (the Genevan gown) or do away with them altogether. I put
this under the category of fasting from an abuse. Ifa man finds he
is watching too much television, he may decide to sell his TV and
fast from it for a while, Later on, he may get another TV and
learn to use it moderately. Similarly with alcohol. Fasting from an
abuse is a proper way to learn to control part of God’s world. The
principle of fasting is the opposite of the doctrine of demons,
aste not” (Col, 2:21; 1 Tim. 5). The goal of fasting
from a thing is to learn proper use of it, not to avoid the thing
altogether. There is nothing inherently wrong with alcohol, televi-
sion, or traditional catholic forms of worship and vestments. In-
deed, the Old Catholic forms are a whole lot more Biblical and
edifying than what generally goes on in conservative evangelical
churches today.

The Reformation was four centuries ago. There is no need to-

   

 

12. Janet Mayo, A History of Exelesiastical Dress (New York: Holmes and Meier
Pub., 1984), p. 72.

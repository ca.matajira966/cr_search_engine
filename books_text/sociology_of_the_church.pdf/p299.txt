A Liturgy of Healing 285

manifested, as tokens of the nature of the New Covenant era.
The problem that most charismatic theologians have with ap-
plying this data is that they are Americans, afflicted with the
overly-individualistic approach to life that characterizes “Ameri-
can Baptist Culture.” In fact, all sickness signifies and manifests
the curse on man for original sin, and all healing signifies salva-
tion in Christ. As Job’s friends had to learn, however, this does
not mean that any particular affliction that comes upon an indi-

 

 

vidual indicates some particular sin on his part. And it follows,
then, that we cannot say to each and every afflicted Christian,
“Jesus wants you well.” There may be many reasons why Jesus
does not want a particular person well at a particular time.

By itself, this fact elimmates virtually all encouragement to
pray for healing. We may well just seek to relax fatalistically in
whatever Providence seems to decree for us. It is at this point that
the charismatic theologians have a salutary corrective to offer, for
in fact we do have a general (not a particular) warrant for believ-
ing that the normal Christian experience is one of health, not of
sickness. ‘This is especially true in terms of the coming of the New
Covenant. God has judicially declared the world cleansed of evil;
that is, God has re-symbolized the world from darkness to light.
This re-symbolization or redefinition is the foundation for the re-
creation of the world. God has re-symbolized man as healed, and
since the symbolic dimension is primary, this means that man is to
be healed physically as a consequence. Thus, the healings per-
formed by Jesus were not “merely” symbols of Spiritual healing,
but were tokens of the fact that physical healing is normally a con-
sequence of Spiritual healing.

Rampant illness in our society as a whole simply indicates that
we are under the Egyptian curse, because of secular humanism
and the refusal of the Christian churches to deal seriously with it.*

3. This phenomenon is discussed at length in James B. Jordan, ed., The Fail-
ure of the American Baptist Culture. Christianity and Civilization No. 1 (Tyler, TX:
Geneva Ministries, 1982).

4. For instance, in spite of all the yelling about abortion, and all the rhetoric
about abortion’s being murder, how many evangelical leaders have come out and
demanded the death penalty for conspiracy to commit abortion? Has anybody? No wonder
God does not take evangelicalism seriously!

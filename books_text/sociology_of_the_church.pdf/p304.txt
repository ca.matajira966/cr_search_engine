290 The Sociology of the Church

healing flows from the general healing that comes from Christ,
made physically manifest in the sacraments. Thus, Christians
should not come to healing services and ask for unction repeat-
edly, “just in case,” or for general health, It is only in specific cir-
cumstances that unction is to be requested.

In summary, God generally wants His people able, strong,
and healthy. He has instituted the rite of unction to take care of ill-
nesses and other maladies. We are not to despise this provision,
but make full use of it. If God does not grant healing the first
time, we should return with renewed petition two more times. If,
after three requests, God still does not grant health, then we are to
understand that He has a special purpose for us, and rejoice in
that exceptional calling, and view the rite of unction as a means of
transforming our problem from a manifestation of the curse into a
special manifestation of the Kingdom, for our good and for the
good of others. It is not my purpose here to discuss all the benefits
that come from suffering, according to Scripture. Suffice it to say
that our Lord Jesus Christ is said through suffering to have learned
to sympathize with our weaknesses and to have learned obedience
(Heb. 4:15; 5:8). Thus, all Christian suffering should be in union
with Christ, a means to learning submission to God, and a means
of learning better to feel for others.

Services of Healing

There is no reason to think that healing services have to be
conducted as part of the weekly liturgy, but there is no reason to
think that such should not be done either. In some churches, per-
sons are invited to come forward during the long prayer (called
the Pastoral Prayer, or the Prayer of the Church), and at that
point they make confession of their sin or of their need, and are
prayed over by the elders, while the rest of the congregation en-
gages in silent prayer. God has blessed this where it has been
done. This could also be done at the beginning of the liturgy, in
connection with the initial confession of sin. Such a thing could
also be done during a Wednesday evening vespers service.

It is also possible, of course, for the elders to visit a sick person

292 The Sociology of the Church

special healing should be made.

The apostle James indicates that the person seeking spe-
cial healing should confess sin, be anointed with oil, and
then be prayed over by the elders of the church. [If done
publicly, here should follow a general explanation of the cir-
cumstances of the present ministration, for instance, “Mr.
N. has approached the session and asked to be anointed with
a view to his coming heart surgery”; or, “Mrs. N. has ap-
proached the session and asked to be anointed and prayed
over concerning a certain affliction from which she has
suffered for a long time”; or, “Mr. and Mrs. N. have brought
their child, N., to the session because. . . .”]

[Then, addressing the sick person, or the parents of the
child]: We therefore ask you the following:

Do you (on behalf of this child)” confess that you have
sinned against God, not only in outward transgressions, but
also in secret thoughts and desires, which you cannot fully
understand, but which are all known to Him? If so, answer:
I confess my sin.

Penitent: | confess my sin,

Officiant: Do you (on behalf of this child) confess that you
are deserving of all misery and wrath in this world, and even
of the eternal fire of hell, for your sins? If so, answer: I con-
fess it.

Peniient: t confess it.

Officiant: Do you (on behalf of this child) flee for refuge to
God’s infinite mercy, secking and imploring His grace and
healing, for the sake of our Lord Jesus Christ? If so, answer:
Ido.

Penitent: 1 do.

10. This formula is used when parents are speaking on behalf of children too
young to speak for themselves. Since children are members of the New Covenant
by baptism, they arc counted as and treated as having faith, and thus as desiring
to serve Christ in wholeness of health. The parent articulates what the child is
assumed to desire, but cannot as yct articulate. Of course, it is not really neces-
sary to ask such children any questions at all. Children who are not baptized, of
course, cannot be the recipients of the ministry of healing, since they are outside

of Christ.

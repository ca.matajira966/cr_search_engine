A Liturgy of Healing 293

Officiant: [The officiant shall dip his finger in the oil, and
place it upon the head or forehead of the penitent, saying:}
(Name,) I anoint you with oil in the Name of the Father, and
of the Son, and of the Holy Spirit. As you are outwardly
anointed with this holy oil, so may our heavenly Father
grant you the inward anointing of the Holy Spirit. Of His
great mercy, may He forgive you your sins, release you from
suffering, and restore you to wholeness and strength, May
he deliver you from all evil, preserve you in all goodness,
and bring you to everlasting life; through Jesus Christ our
Lord, Amen,

(Then shall the elders place their hands upon the peni-
tent, and the officiant shall pray one or more of the following
prayers, as appropriate:] (The prayers are taken from pages
458-460 of the Book of Common Prayer.)

Should the Sign of the Cross be Used?

How should the oil be placed on the forchead? If we are going
to smear it on, will God be angry if we smear it in the sign of the
cross? I don’t see any necessity here one way or another. Since,
however, it is generally assumed in presbyterian circles that sign-
ing with the cross is wrong, I should like to insert here a quotation
from Martin Bucer, the man who taught John Calvin, and one of
the four foremost Reformers. Bucer is actually writing concerning
the sign of the cross in baptism, but what he says is relevant els

 

where: “This sign was not only used in the churches in very an-
cient times: it js still an admirably simple reminder of the cross of
Christ. For these reasons I do not consider that its use is either un-
suitable or valueless, so long as il is accepted and used with a
strict understanding of its meaning, untainted by any admixture
of superstition or servitude of an element or casual adherence to
common custom.”!! The point here is that the protestant Refi
ers did not interpret the “regulative principle of Scripture” in such
a minimalist way as to exclude all simple gestures.

 

   

rm-

 

M1, Bucer, Censura; found in E. C, Whitaker, Martin Bucer and The Book of Com-
mon Prayer. Alcuin Club Collections No. 55 (London: SPCK, 1974), p. 90.

Biblical Terminology for the Church 301

The result of this survey is to reinforce our model of the
church of God. The church as the people of God includes all that
Christians are and do, and is thus synonymous with what the
term “kingdom” is used to denote in much evangelical literature.
The same is true, though in a more specialized sense, of the
church as an organized universal people. The church as locally
organized must primarily be organized sabbatically, but may and
eventually will be organized for other duties as well. The rise and
development of Christian orphanages, hospitals, colleges, and so
forth is evidence of this. In one sense all these things are part of
the church, though they may not be under the government of the
sabbatical ‘dah. The church as a local assembly in the Presence of
God evinces the actual sabbatical function of man. Finally there is
the collective sense in which all the people of God are contem-
plated as in a great assembly before the throne of God.

The failure to keep these distinctions straight has had recur-
ring ill effects on the church. Those who fail to distinguish the
church as a people from the church as an organized sabbatical
community, have tended to ascribe to the institutional sabbatical
‘edah, and to it alone, all the various aspects of the church. We
readily grant that the church as gathered for worship concentrates
in itself in a primary fashion all the multiform aspects of the
church, We also grant that, from the standpoint of historical mat-
uration, the institutional church is the nursery of the kingdom in
its broader manifestations. We deny, however, that all church
functions are only carried out properly when performed by the
sabbatical ¢dah. It is not obvious from Scripture that all Christian
orphanages, hospitals, schools, families, etc., must be under the
command of the officers of the sabbatical ‘edah, The precise deline-
ation of responsibilities has proven difficult historically, and we
must, therefore, attempt it anew.

Errors in interpreting and applying Scripture can rise from a
failure to understand the various senses of the word “church.” Re-
formed theologians have for a long time used the term “kingdom”
for the church as “people of God,” and this usage has now become
common in evangelical circles. This shorthand has its place, per-
haps, but it often Jeads to misinterpretations of Scripture, for

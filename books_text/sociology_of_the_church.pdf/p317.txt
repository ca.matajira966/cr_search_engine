Appendix B

THREE PERSPECTIVES ON THE
CHURCH IN THE OLD COVENANT

In an attempt to do justice to everything the Bible says about
the church in the Old Covenant, I suggest we keep in mind three
perspectives. I am under no illusion that this is the last word to be
said on the subject, but I believe it is a helpful word. At any rate,
Perspective One is as follows: After the sin of Adam, the whole
world is fallen and cursed. After the Fload,! that whole world sys-
tem includes both the provisional, temporary, “Eden-sanctuary”
restorations and also the downstream locations; that is, it includes
both Israel and the Nations. Both are in a fallen situation, and
compared to the glories of the New Covenant, both can jointly be
said to be “in bondage to the elementary principles of the world”
(the Old Covenant arrangement, fallen Adam; Col. 2:8, 20; Gal.
4:3).2 From this perspective, there was no church in the Old Cov-
enant; the church was born on Pentecost.

Perspective Two is this: For symbolic and pedagogical pur-
poses, God set apart a priestly people for Himself. Compared to
the nations, Israel was redeemed and they were not. Israel was
the church, in the “olive tree,” and the nations were not (Rom. 11).

1. Assuming that the Flood washed away the primordial Garden and land of
Eden, which were off-limits to fallen man

2, Interpreters have debated what the “elementary principles of the world”
are, Do they have to do with paganism, or with Old Covenant Israel? The reason
for the debate is that in both the Colossians and Galatians passages, paganism
and Old Covenant religion are conflated. In Colossians Paul speaks of circumci-
sion (2:11) and of sabbaths (2:16) as well as of pagan asceticism (2:22f,). In Gala-
tians, those under the law are slaves/children (3:24ff.}, but so are those who did
not know God and were slaves to idols (4:8ff,). The “elementary principles” are
the principles of the Old Adamic Covenant, in its duality. That entire first or ele-
mentary system has been transfigured in Christ.

303

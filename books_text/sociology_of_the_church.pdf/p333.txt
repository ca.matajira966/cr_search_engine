General Index

Prayer, sitting for, 219
Preaching, 9, 50, 224ff
Presbyterians, use af Bible
in worship, 8f.
Presbyterian Church in America, 148
Presuppositionalism, 1
Priest, Priesthood, 119ff.
of all believers, 31
circles within Israel, 102f., 106
Israel to the nations, 83ff.
nature of, 46f.
priestly and kingly tasks, 90f.
Roman Catholic understanding,
272.
Primacy of the intellect, 224f.
Primacy of the Word, 224ff., 229f.
Procession of the Holy Spirit, 41, 65f.,
132
Prophet as Councilmember, 42f.
Protestantism, 125f., 137#f.
Psalms, Psalmody, 225
composite psalter needed, 24f.
sung or chanted, 220
Puritan, Puritanism, 29f., 59, 117,
2708.
as catholic movement, 145f,
nalism, 209

 

Qahal, 2966.

Rebekah, 94
Recognition, mutual, lif., 147, 149, 166
Re-creation, SIf.
Reformation
catholic movement, 143f.
liturgical movement, 28ff., 199f.,
207f., 224ff., 266f.
Regeneration, 6n.
Regulative Principle, 10, 28, 171, 205,
208, 263ff., 271
Representation in worship, 272ff.
Resurrection and community, 2444.
Revelation, as worship service, 279.
Revivalism, 172f., 227ff.

319

Ritual, 21f.
Rivers, Edenic, 86f.
Roman Catholicism
reaction against, 207f.
stil] largely unreformed, 134
view of priesthood, 272ff.

Sabbath, 238f. (see Annotated Table
of Contents, chapter 3)
Sabbatarianism, 34
Sacrament
false, 47
not for parachurch, 79
as sign and seal, 394.
Sacramental system, 159f.
Salt, 15n.
Samson, 249
Sanctuary
church as, 3f., 49
closed in Old Covenant (see
Annotated Tabie of Contents,
chapter 3)
Savoy Conference, 29f.
Schaff, Philip, 148
Schilder, Klaas, i11n.
Schism, 64ff.
Schmemann, Alexander, 31, 129, 172,
283n.
Scribes, 255
Sea, brazen, 90n.
Second blessing, 41n.
Second commandment, 217
Separation from apostasy, 70f.
Shands, Alfred, 200f.
Shell analogy, 68ff., 194
Simon, Merrill, 181ff.
Sixfold Action, 35f., 87, 189
Slavery, 245, 247
Special Presence, 239f., 242f.
Stars, symbolize Christians, 42, 265
Statismn, 188
in Protestantism, 146
in American Christianity, 147f.
Subordinationism, 41n.

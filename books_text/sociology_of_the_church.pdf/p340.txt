326 The Sociology of the Church

‘Thus not for women or gentiles
In this respect, different from New Covenant baptism
Moses did not practice it while living with Jethro
Israel did not practice it in the wilderness

Priests to the Nations, 101

Syinbolism of Elim

Concentric circles of priests

Jubilee land law excluded gentiles from holy land

Holy meals not extended to all; ¢.g., showbread

Passover only for Israel-priests, sign of coming salvation

Principle of representation

Principle of Old Covenant exclusion

‘Towns as sanctuaries, not etlected by Jubilee

Adoption

 

Laws of boundary and exclusion
Design of Old Covenant sabbath; highlight exclusion from sanctuary
The New Covenant, 107
Romans 2: Both Jew and gentile could be saved in Old Govenant
Romans 4; Abraham father of godly gentiles and well as of Jews
Ephesians 2; Duality of Jew and gentile (Eden and Havilah) overcome in
the New Covenant
Galatians 4: Both Jew and gentile were excluded from fullness of
sanctuary in Old Covenant
Vas Israel the “church of the Old Covenant?
Did the church begin at Pentecost?
The New Creation, 110
‘Yhe duality of heaven and earth emerges to replace Old Covenant
dualities
Relationship of Christ in heaven to church on earth
Demonstration of change = destruction of Jerusalem in a.p. 70
New land: community of the Spirit
New sanctuary: heaven
New flesh: sacramental body of Christ
Sabbath, 114
Seasonal festivals and weekly sabbaths

 

Separation of synaxis from eucharist (sacrament)
Continuing need for appointed times in New Covenant
Difference between Old Covenant priests and New Covenant elders
Cancellation of the sabbath: what it means
Land, HB
Sanctuary in heaven: door to promised land
Worship issucs in mission, restoration of culture
Westward movement replaces eastward
New churches relate to heaven, not to older churches

330 The Sociology of the Church

Us the Church Vulnerable?, 191
‘There is no present need to capitulate on this point
Jurisdiction is the issue
Christ is the church's Protector
The Awesome Power of the Church, 193
Liturgical warfare
Property Taxes, 194
The subterfuge of making a voluntary gift
The Official Statement of Westminster Presbyterian Church
Sample letter to state property taxing authorities
Won't Compromise Hurt our Wiiness?, 197
No grant of jurisdiction is involved in paying a bribe

Part III: Rethinking Worship: Some Observations

Introduction . . oe . . 199
Present liturgical awakening in evangelical and Reformed circles

‘ical movements in the past

1. The Reformation

2, The Romantic Oxford Movement

3. The Belgian liturgical revival

‘Three litur:

 

Emphasis on action in union with Christ
Problems with the impact of the modern liturgical movement upon
evangelicalism
1. Emotional romanticism: Nostalgia
2. Loose language of “incarnationalism” misplaces emphasis away from
the cross, and moves in a monophysite direction
3. Ignoring the regulative principle of worship

10. How Biblical is Protestant Worship? . . 207

‘The Reformation as a purification of worship
‘Lhe principle of fasting from abuses
Reactions against Rome

The Regulative Principle, 208
‘Lhe proper form of it
Simplistic reductions of it
Rationalistic perversions of it

Aspects of Worship, 210
Fullness of worship not necessary for the esse of the church
Yet desirable for her bene esse and plene esse

The Act of Csnssing Oneself, 211
The Reformers did not object to it always
Physical actions not strange to Biblical religion
Anti-catholicism is the wellspring of evangelical objections

Annotated Table of Contents 331

The Cross Design in Scripture, 212
The Edenic square and cross
The labyrinth elaboration
A sign of total expansion and dominion
The Cross in Architecture, 214
Arrangement of the Tabernacle furniture
Arrangement of the Israelite tribes
Cruciformity and humaniformity
The Israelite army: its arrangement and relevance for architecture
Practical implications
Posture and Gesture, 218
Examples from the Bible
Protestant rationalism has blinded evangelicals to it
Pastoral considerations dictate that we consider these things carefully,
and not just rush into trying to do them

11. God’s Hospitality and Holistic Evangelism settee eee 22L
Christian hospitality is a copy of God’s
Survey of New Testament references to hospitality
The stranger in the Old Testament given hospitality
Excommunicated persons not given hospitality
Holistic Man, 222
Biblical view of man is neither tripartite nor bipartite
Soul/body dualism has its origin in pagan reflections on immortality
Human life has several dimensions, of which the religious is primary
The Greek heresy of the primacy of the intellect
The Biblical doctrine of the primacy of the Word
The Primacy of the Preacher, 224
The Reformers wanted weekly communion to go along with preaching
Magistrates in Reformed cities opposed weekly communion
Result: Reformed churches were left with preaching as only center of
worship
Decline of congregational participation
Rise of preaching as a rhetorical event: entertainment
What it means to “sce Jesus”
Preaching to outsiders; teaching to churchmembers
The Tragedy of Revivalism, 227
Unfed Christians seek the miraculous outside of worship
Revivals began in connection with infrequent communion seasons
Irrationalism in American religious life
Creation of new rituals: the altar call
Solutions:
1, Restoration of the Word in all forms to centrality in worship
2, Restoration of full congregational participation: prayerbooks

30 The Sociology of the Church

unto and to declare their consent in the close, by saying Amen.’ "2
In other words, no dialogue, no responsive readings, no congre-
gational praying of the Lord’s Prayer or any other prayer. The
Anglican bishops replied that “alternate reading and repetitions
and responsals are far better than a long tedious prayer.” They
also noted that “if the people may take part in Hopkins’ why not
David's psalms, or in a litany?” In other words, if it is all right to
sing metrical paraphrases of the psalms, why is it wrong to read
responsively the very words of Scripture?

Originally the Puritan movernent had not been opposed to
prayerbook worship, but in time the combination of state persecu-
tion with the continuing strength of the Medieval quietist tradi-
tion led the Puritans into wholchearted opposition to congrega-
tional participation in worship,

Worship and Ceremony

So, “ceremony” came to be a bad word. ‘The Puritan approach
greatly influenced the whole Calvinistic world, and so came into
virtually all of what today is called evangelicalism. Gradually,
however, the Puritan extremes were watered down. Congrega-
tions began to pray the Lord’s Prayer together. Choral recitation
of the Apostles’ Creed was reintroduced. Responsive readings
crept back in. Christmas and Faster became acceptable, as did
the use of the cross as a symbol, At the same time, however, little
has been done to recover the actual perspective and principles of
the early church and of the Reformers. To a great extent, these
catholic practices have crept back into evangelical churches not
because they are clearly seen to be part of Biblical precept, princi-
ple, and example, but because of a de facto abandonment of any
commitment to Biblical regulation at all.

Ceremony is still thought of with suspicion; it is just that cer-
tain compromises have been made. On our agenda today, how-
ever, must be a rethinking of the whole matter of ceremony. In

22. See Francis Procter and Walter H.. Frere, A New History of the Book of Com-
mon Prayer (London: MacMillan, 1908), p. 172.
23. fbid., p. 173.

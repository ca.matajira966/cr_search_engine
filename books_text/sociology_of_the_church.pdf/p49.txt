Reconstructing the Church 35

But then comes the feedback into the area of doctrine, and in
protestantisin the failure to keep the sacrament equal to the read-
ing of the Word in worship has led to the doctrine that faith and
works are separate and opposed one to another. The failure to
“do” has led straight to antinominianism.

Well, then, what is this action? It is what Jesus did, and com-
mands us to do, Originally it was a nine-fold action. Jesus —

1, took bread 6. took wine

2. gave thanks 7. gave thanks
3. broke it

4. gave it 8. gave it

5. they ate it 9. they drank it.

This reduces to a five-fold action of taking, blessing, breaking
down and restructuring, sharing, and consuming. Notice that
there is in this no “setting apart of the elements from common
use,” as if man had such a power. Nor (surprisingly) is there any
invocation of the Holy Spirit. These things are not necessarily
wrong, but they are not the essence of the rite.

A comparison of the steps in this rite with Genesis chapter | is
most revealing. As we read that chapter, we see God repeatedly
take hold of His creation, break it down and restructure it, and
then distribute it to various kingdoms of creatures, We also see
God evaluate His work (“And God saw that it was good”) and en-
joy it (resting on the seventh day). There are five steps here, to
which man as a creature would add a sixth: the giving of thanks to
God.

In this action there is a world-view. Man the priest is called
upon to éake hold of the creation, He is not, however, to do like
Adam, and take hold of it autonomously; he is to give thanks. Hav-
ing done so, he is to work with the creation, dreaking it, restructur-
ing it, and then sharing it with others through giving or trading. At

28. Sce Dix, op. cit., chapter 4. Dix is noted for his discussion of the “four-fold”
action, He docs not sce breaking as a separate act, but simply as necessary for
the act of sharing.

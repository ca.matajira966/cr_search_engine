Reconstructing the Church 39

The Keys of the Kingdom: Word and Sacrament

Man was created the guardian of Eden, but when he fell, he
lost this post (Gen. 2:15; 3:1-5). God gave the keys of Eden’s door-
way to new cherubic guardians (Gen. 3:24). In Christ, however,
the keys have been restored to man, and Christ has given them to
His representatives to use for Him (Matt. 16:18f.). We are only
supposed to bind on earth what we know has already been bound
in heaven, and the way we know that is from Scripture.

What are the keys? Protestants have answered this question
by saying that there is a general and a specific aspect to the ad-
ministration of the keys. The general aspect is the proclamation of
the Word to all men, drawing them in or sealing them out (2 Cor.
2:16). The special or particular aspect is the judicial power of
church discipline, admitting to the church by the sacrament of
baptism, and expelling from the church by excommunication,
This is seen in Scripture imagery again in that the key locks or un-
locks the gate of the city, and the gate of the city was not only the
place of (general) traffic in and out, but is also the (special) place
where the elders sat as a court of law.

What is the relationship between Word and Sacrament? Are
they two different ministries of God? Is the sacrament merely at-
tached to the Word as a seal? Are they equally ultimate?

According to Hebrews 6;13-18, God always gives two or three
witnesses of His truth. Here the two primary witnesses are called
“word and oath.” In the Old Testament, God’s Word was always
accompanied by a sign. The sign was either a momentary mira-
cle, or a lasting memorial (such as a memorial pillar, a memorial
rite like Passover, or a memorial action like circumcision), This
testimony of two witnesses is not God’s condescension to our
weakness, but is a manifestation of the fact that He is Three and
Onc. The Triune God always reveals Himself by two or three wit-
nesses: Word and Sign; or else Word-Sign-Image (men).

The sacrament is the standing memorial of the death of
Christ. When Jesus said, “Do this in memory of Me,” He was not
advocating that they break bread simply as an aid to devotion, a
reminder of Jesus’ death. The context for this statement is the

 

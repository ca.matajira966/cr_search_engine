40 The Sociology of the Church

theology of memorial in the Old Testament. A memorial is a re-
minder, yes, but it is a reminder that exists whether people make
use of it or not. The memorial stones Joshua set up by the Jordan
were standing memorials, even on days when nobody came by to
look at them. Thus, there is an objective “thereness and thatness”
to a memorial that precedes our subjective appreciation of it.
The “thereness and thatness” of the memorial means that the
memorial is always present before the eyes of God. If men pray to
God for blessing on the basis of the memorial (e.g., “in Jesus’
Name”), then the memorial reminds God to bless them. If men ig-
nore the memorial, forgetting God's mighty acts, then the memor-
ial still stands before God, but it calls down His curse. The blood
of Jesus Christ reminds God of those wicked men who crucified
His only Son just as much as it reminds Him of the payment for
the sins of the elect. Thus, the eucharistic memorial is never ncu-
tral, but always works cither blessing or curse (1 Cor. 11:30).
‘The sacrament, as a memorial, is also a continuing miracle
The fascination with the miraculous that has crippled the Ameri-
can churches since the days of the Great Awakening can only be
overcome when the Lord’s Supper is once again part of the center
of our worship, for that is where Christ is specially present in our
midst. As a miraculous memorial, the sacrament has a real influ-
ence or effect. It is never neutral. For the faithful, it magnifies the
grace of the gospel, and for the unfaithful it magnifies the curse.
The sacrament works positively in response to faith, but it also
works negatively in response to faithlessness, In thi
must say that the sacrament does indeed work ex opere operate.
Because the pluriform revelation in Word and Sacrament is a
refiex of the triuniry of God, it will always ultimately evade our at-
tempts to explain it rationally. At the same time, some things can
be said. I should like to propose that the Word is more the work of
the Son, for obvious reasons (John 1:1), while the Sacrament is
more the work of the Spirit. It is the Spirit Who makes Christ
present at the sacrament, and baptism water descending upon
us from above —also is a sign of the Spirit. I say “more” the work
of one or the other, because in the opera ad extra of God, no one

 

ns

 

7 We

 

Person works exclusively.

Sociology: A Systematic Approach 57

The church as a gathered assembly “Waves” through its lit-
urgy, proceeding from confession to consecration to communion.
It “Waves” through the Church Year. It “Waves” into special festi-
vals. ‘There are a hierarchy and variety of such “Waves”, as when
the individual “Waves” in and out of his daily private devotions,
when the family does the same, when the institutional church
does it publicly, and so forth.

The Church as a Government

Finally, che church as a government: What are its characteris-
tics? We may speak of two: the sacraments (which are her power)
and the officers (who administer the sacraments). These are visi-
ble to the eye of man, although the primary visibility is in the sac-
raments.

Considered from the “Field” perspective, the church as a gov-
ernment is set off against other governments. God has set up two
other governments: the state and the family, Man has analogously
set up others, such as the business firm, or the school. Since there
are various levels in the organization of the institutional church,
there are various “Fields” in which she operates. For instance, the
National Council of Churches has some interaction with the poli-
cies of the Federal Government, while local churches interact with
local governments, and so forth. One “Field-view” question is the
question of the interrelationship between the diaconal work of the
church as an institution and the diaconal work of the church as peo-
ple of God, the question of so-called parachurch organizations.

Considered from the “Wave” perspective, the church as an offi-
cial institution is more or less faithful, and thus more or less visi-
ble. This is what the various sectarian groups do not wish to ad-
mit. Each sect believes it has encoded and encapsulated the final
formulation of the truth, and is therefore in a position to demand
that all other Christians get into line, or else be regarded as coun-
terfeits. The fact is, however, that the worship of the church
“Waves” in and out of closeness to God's standard. When the
church is faithful in worship, the Bible is the standard; the Psalms
are chanted (as well as good hymns); the congregation is active in

 

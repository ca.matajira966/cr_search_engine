58 The Sociology of the Church

prayer and praise; a Biblical philosophy undergirds architecture,
decor, color, lighting, vestments, etc.; infants are claimed for
Christ in baptism and admitted immediately to His Table; the
proclamation of the Word is never separated from the sacramental
seal, so that Communion is held every week; and we might go on,
When the church “Waves” out of faithfulness, however, these
things diminish.

 

The “Wave” of Church History
The “Wave” perspective is so valuable and important that I
wish to expand on it a bit more, These “Wave” motions in the
church also have a variety and hierarchy to them. If we look only

 

at particular churches, they scem to be rising and falling at differ-
ent times and in different ways. In the overall history of Christ's
Body, however, the “Waves” are not small, nor are they local. It
took a long time for the church in America to get into the bad fix
she is in today, and she will not be reconstructed overnight. More-
over, while there are superficial differences among the various
kinds of churches, they all have the same fundamental problems,
We are all together in this boat, deep in the present trough (low
point) in the “Wave.” It is not just the Baptist, or the Fundamen-
talist, or the Presbyterian, or the Reformed, or the Episcopal, or
the Lutheran churches that have waved into liturgical slovenli-
ness. They are all about equally guilty, though in different ways.®
And, just as the churches all fell together, they will not rise unless
they rise together.

The problem with sectarianism is precisely that it militates
against a catholic and wholistic reconstruction of the church. We

  

shall consider schism and separation later on, but for now con-
sider these two different options: On the one hand, a conservative
catholic Christian church may separate from a liberalizing de-
nomination without saying that the liberal denomination is a

8. Episcopalians sitting in church and passively listening to the choir sing the
liturgy are in the same position as Baptists sitting in church and passively listen-
ing to the choir and preacher do cverything. The music differs; the problem of
passivity is the same.

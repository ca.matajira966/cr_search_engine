Sociology: A Systematic Approach 67

The Divine and eternal Church becomes visible in Christ, and by
extension in His body.

Now we can ask this question: What aspects of the church cen-
trally and most pointedly make Christ visible? These will be the
irreducable symbols or signs of Christ, without which the church
cannot be said to exist at all, They are three; the proclamation of
the Word of God, the administration of the sacraments, and the
ordination of special officers. These three correspond to the tradi-
tional “marks” of the church: Word, sacrament, discipline. They
also correspond to the three basic perspectives and concerns of
ethics: the normative (law, Word), the situational (sacramental in-
corporation into the body of Christ), and the personal (human ad-
ministration, under the Spirit), Finally, they correspond to the
three aspects of the church, as follows: The Word gives direction
to the people of God in all that they do; the ritual of the sacra-
ments structures special worship in the assembly; and the officers
provide the government for the church as an institution or organi-
zation.

The Bible is the Word of Christ, specifically instituted by Him
through the power of the Holy Spirit. The sacraments of Holy
Baptism and the Lord’s Supper were specially instituted by
Christ. The apostles and their successors were specifically set
apart by Christ to govern the church, Two of these three elements
(sacraments and officers) are directly “institutional” in character.
This means, contrary to the opinions of most of American popu-
lar Christianity, that ¢here is an irreducible institutional character to the
true church of Christ. The church is not first and foremost a group of
people who have come together to worship, and who set up offi-
cers from among themselves, and who do the sacraments. Rather,
the church is first and foremost the visibility of God on the earth,
set up by Him. The people are called by the officers and the sacra-
ments, first and foremost. Thus, if a missionary and his wife go
into a pagan land and make the sacraments visible, and call men
by preaching the Word, the church is fully visible in that place in
its essence, because Christ is visible. This is the wellspring of all
other, wider forms of visibility.

We now have a paradigm for visibility. There is special

 

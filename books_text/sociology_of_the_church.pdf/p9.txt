Preface ix

After I went into the Air Force, in 1971, 1 spent a year attend-
ing Navigator Bible studies, but soon found my way into a Pres-
byterian church, I have remained a Presbyterian since, studying
for a while at Reformed Theological Seminary in Jackson, Missis-
sippi, and finishing at Westminster Theological Seminary in Phil-
adelphia,

I thought it useful to explain my background, since it feeds
into this book. In most ways, my experience was typically Ameri-
can, Christian views of man’s sinfulness and the consequent need
for limited government played a part in my involvement in anti-
leftist political action during my college years (1967-1971), but I
always felt a tension between conservatism on the one hand and
Christianity on the other. That tension was resolved somewhat
with my discovery of Francis Schacffer, but the separation of
“nature and grace” was still too great in Schaeffer to give me long
lasting satisfaction. !

I thus soon found my way to the works of Rousas J. Rush-
doony, whose rigorous Biblicism and presuppositionalism gave a
much more sure foundation for Christian thought and action.
Unfortunately, Rushdoony’s low view of the sacramental body of
the church served to reinforce my natural American bent in the
same direction.? Thus, when I entered seminary it was with a
w of the church and a desire to learn “the faith,”

 

 

rather low vi
while bypassing the institutional church, which, after all, was but
one social sphere among many, I tended to be deaf to the call of
some af my teachers, most notably John Richard de Witt, to rec-

 

  
 

1. L have reference to Schaeffer's “pre-evangelism” set forth in his earlier
books. Schaclfer's lack of clarity on this point becomes more obvious in his Chris-
tian Manifesto, where on the one hand he wants a Christian society, and on the
other he shies away from any real Christian and Biblical law. For a critique, see
the following three essays: Gary North, “The Intellectual Schizophrenia of the
New Christian Right,” and Kevin Craig, “Social Apologetics,” in James B. Jor-
dan, ed., The Failure af the American Baptist Culture. Christianity & Civilization No.
1 (Tyler, TX: Geneva Ministries, 1982); and Gary North & David Chilton,
pologetics and Strategy,” in Tactics of Christian Resistance. Christianity & Civili-
zation No, 3 (Tyler: Geneva, 1983).

2. Fora critique, see my remarks in the “Introduction” to Jordan, ed., The Re-
construction of the Church, Christianity & Civilization No. 4 (Tyler, TX: Geneva
Ministries, 1986).

 

   

  

  

24

11.

12,

13.

14

A Study Guide to Liberating Planet Earth

Power religion is a religion of autonomy, which assumes that:
(p. 42)

OA. Power should come automatically to Genesis 3:1-8
men. Deuteronomy
*] B. Power and wealth can be gained and 8:11-20
retained without reference to God’s Daniel 4:28-37
law. Acts 12:18-23
OC. Power comes and goes unpredictably.
*] D. Power cannot be gained without hard work and intelligence.

Escapist religion reacts to power religion by: (p. 43)

OA. Helping defenseless people escape Ezekiel |
from abusive oppressors. Jeremiah

OB. Opposing it with spiritual power.

Oc Fleeing from the responsibilities of the dominion covenant.

*1 D. Ignoring it.

 

15

What is the basic idea behind the escapist religion? (pp. 43-44)

OA. The dominion covenant is fulfilled in the millennium when
Jesus comes to rule with a rod of iron.

QB. The dominion covenant is not the responsibility of Chris-
tians.

*1 C. Escapist religion denies the dominion covenant.

¢] D. The dominion covenant is fulfilled only in the spiritual realm.

. What do escapists propose as an alternative to the dominion cove-

nant? (p. 44)

QO A. Escapists restrict the focus of discipline to increasingly lim-
ited areas of life.

*1 B. Our resurrection is our ultimate dominion.

OC. We must ask Jesus to come back and do for us what He has
commanded us to do for him.

*]1 D. God is ultimately sovereign.

CHAPTER 4
“The Covenant of Liberation” pp. 50-61
Memory verses: Deuteronomy 8:18-20
1, What is the most prominent characteristic of a covenant? (p. 50)
*] A, Asacrifice by fire.
*] B. A legal document with signatures.

*1 ¢ A selfvaledictory oath.
*] D, The Ark of the Covenant.

2, What is the meaning of the self-valedictory oath? (p. 50)

*] A. The swearer of such an oath calls Genesis 15
down the wrath of God upon himself Genesis 21:22-23

if he disobeys the covenant. Genesis 26:26-31
*] B, The swearer is responsible for pun- __Genesis 47:27-31
ishing himself.

QO) ¢. The swearer does not really deserve the punishment that is
laid upon the animals, but he has to ‘walk between the
pieces.

3. When we speak of God’s “transcendence”, we mean that He is:
(pp. 52-53)

QA. Invisible. Job 38:1-41:6
OB. Is both in and above His creation. Isaiah 40:12-26
*1 c, Not concerned with the creation. TT
*] D. Above the creation, and not a part
of it,

27

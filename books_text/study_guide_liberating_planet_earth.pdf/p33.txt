30

12,

13,

14,

A Study Guide to Liberating Planet Earth

The union between covenant-keepers and their God is ethical; that
is, it requires: (p. 55)

0 A. Knowledge of magic. Deuteronomy

OB. The exercise of autonomous power. 30:15-20

1c, Submission to the law of God. John 14:15,21,
23, 24

*] D, Well-behaved conformity to current
popular values, even in cases in
which we disagree with them.

1 John 5:2, 3

To say that the covenant has a judicial character is to say that:

(pp. 55-56)

OA. It requires certain performances and | Deuteronomy 28
forbids others. Joshua 8:32-35

OB. It promises blessings for covenant-
keepers and curses for covenant-breakers.

OC. It must be implemented by lawyers.

Q D. It lays upon men the task of deciding between good and
evil just as God does.

*1 E. All of the above.
*] F, A, B, and D, above.

 

 

 

 

Satan tries to present himself as a transcendent and immanent
authority, but he cannot because: (p. 57)

*] A. He possesses only limited power. Isaiah 14:12-15
OB. He is a creature, and thus cannot Luke 4:1-13
be everywhere at once. 2 Corinthians
OC. He is transcendent but not 4, 5
immanent.

OD. He is immanent but not transcendent.
QO E. Both a and b.
OF. Both b and c

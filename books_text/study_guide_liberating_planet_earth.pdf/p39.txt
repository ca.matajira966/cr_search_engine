36 A Study Guide to Liberating Planet Earth

8 Why are unbelievers ignorant of God’s truth? (p. 63)

Q A. The Bible has too many errors in it. Psalm 53:1; 92:6

QB. They choose to ignore the reality of John 3:18-21
God’s truth. Remans 1:18-32

OC. Christians do not live consistently.

QD. There are not enough evidences for God’s existence.

9. What will happen in the lives of those who have been given this
new heart? (p. 64)

0 A. They will become friends of all men, Ezekiel 36:26-27
OB. They will submit to God’s will and Matthew 5:13-16
cease resisting evil. Acts 2:37-47
OC. They will bear fruit in their lives by
transforming the world around them by their good works.
Ci D. They will stand in the breach between the poor and the
landowners.

10. How do Marxist liberation theologians say that men must be

saved? (p. 64)

QA. They believe that men and society can be saved only by a
program of top-down state control over every dimension of
life.

OB. They want to provide everyone with a free public education.

*1 ¢, They believe that happiness will come when all workers
have a voice in the production policies of their sector of the
economy.

*] D, They believe that the world will become safe when they
have persuaded everyone to abolish weapons.

11. What is environmental determinism? (p. 65)

*] A, The viewpoint that man must control the environment.

OB. An explanation that excuses individuals for their personal
actions and blames their actions on surrounding external
forces.

*1 c, The teaching that plants and animals of nature must be left
alone to live by their natural powers.

{] p. The belief that God is a part of nature.

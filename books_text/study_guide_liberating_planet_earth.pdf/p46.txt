“The Liberation of the Family” 483

6. INHERITANCE

el A. Obedient children are supposed to Proverbs 13:22;

1B.

*le

“1D.

 

 

 

benefit from the efforts of their 19:14
parents.

Children are expected to take care of themselves so that they
can learn responsibility through experience.

The priorities of the community take precedence over the
financial plans of private families.
Parents do not owe an inheritance to their children at all.

 

In questions 7-11 below, choose the statement under each cate-
gory which best expresses the Satanic imitation of the family in
the Marxist state. (p. 74)

7. TRANSCENDENCE

la
1B.

Oc.

“1D.

The state establishes counseling offices to assist parents and
children to grow in their affection for one another.

The state gets children to inform on their parents, making
the children the representatives of the new god.

The state believes that through a balanced combination of
itself, parents, and school teachers, youth can become inde-
pendent citizens.

Parents are willing representatives of the state.

8. HIERARCHY

Qa.

1B.

Oc.

1D.

The state makes strong requirements of fathers, but does
not violate their basic right to run their families.

The state takes precautions to ensure that there is an author-
ity figure in the family in case something happens to the
father.

The state recognizes the primacy of the family, but interferes
if the father does not treat his wife as an equal.

The state sets itself up over the family; the father must
answer to the state.

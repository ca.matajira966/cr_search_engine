62

A Study Guide to Liberating Planet Earth

15, What does the growing presence of government regulation in the

16,

1.

lives of the people lead to? (pp. 107-108)

Qa.

lB.

Qc.

Op.

 

Increased efficiency in applying the |_| Kings 12:1-19
findings of modern science to social

problems.

Better handling of specific types of problems as government
agencies mature and specialize.

The need for more academic analysis to keep up with and
anticipate the problems of management that arise within
such a large system.

Resentment, and an increasing unwillingness on the part of
the people to submit voluntarily to civil law.

 

 

 

How do socialist programs consistently violate the Ten Command-
ments? (pp. 109-1 10)

Qa.

1B.

elc.
el D.

What

They do not: in fact, they help us love our neighbor in a
much larger way than we could do on an individual basis.

They evoke a spirit of covetousness among men by holding
out to them the prospect of taking other men’s property by
means of the ballot box.

They draw us into political affairs.
They emphasize secular education more than religious edu-
cation.

happens to a society when it becomes pervaded with the

spirit of covetousness? (p. 110)

ola.
OB.

Qc.

el D,

Thieves become common but the people come to their senses
and adopt a saner form of government.

People have the opportunity to learn how important it is to
count the things of this world as unimportant.

Waves of power struggles ensue as special-interest groups
compete for control of the primary agency of wealth distri-
bution.

Management consultants and psychologists develop meth-
ods to train workers to be more cooperative.

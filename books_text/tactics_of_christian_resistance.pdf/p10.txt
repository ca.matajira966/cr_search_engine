x CHRISTIANITY AND CIVILIZATION

not carry his “present”—a dagger —in his hand as he entered
Eglon’s chamber (Judges 3:16). Gideon did act warn the com-
manding officer of the Midianites of his strategy of surprise
(Judges 7:15-25). Jesus spoke in parables in order to keep the
masses of Hebrews from understanding His message and be-
ing converted, and He cited Isaiah 6:9-10 as His justification
(Matt. 13:13-17). Despite the legitimacy of the Christian’s use
of deception against the followers of Satan, the great deceiver,
the building of a Christian resistance movement cannot legitimately be
based on a program of initiation into a secret hierarchy of power. Men
who enter resistance movements are to know who their in-
stitutional superiors are, as in the case of a military chain of
command, and their actions are to be governed by an agreed-
upon handbook, the Bible. ‘There are no secret principles that
must not be revealed to non-initiates, so there can be no ritual
murders or other physical reprisals against those who reveal
biblical principles of resistance. The principles of righteous
action are available to all those who are willing to discipline
their minds and actions by the revealed word of God.

In a period of increasing social, political, and economic
disorder, we can expect to see many resistance groups com-
peting for the allegiance of millions of people, We can expect
to find that thousands of people join organizations that would
have been anathema to these new members a decade earlicr.
The escalating confrontations between these organizations
and the civil government, and between each other, will
mobilize people who had preferred to avoid earlier confronta-
tions, People will be swept away emotionally and perhaps
morally if they are not careful to cxamine the goals, training,
and techniques of all organizations that call members to sacrifice
their lives, wealth, and honor for a cause.

By what standard should these movements be examined?
The Christian should not hesitate to answer, “The Bible.” But
understanding the Bible takes time and effort. It is not an
overnight educational process. Certain biblical truths must be
so firmly planted in the mind that the emotions of the moment
are not allowed to carry the potential victim into the pit. This
is what we expect of Christians who are tempted to become
adulterers. In times of social chaos, as in times of bureaucratic
tyranny, the emotional appeal of resistance and revolution is
often stronger than the appeal of non-marital sex. Wild en-
thusiasm and deviant behavior are common aspects of both of

THE NEW COMMUNITY 93

now as they were a century ago when W. S. Gilbert lampooned
them in Lolanthe:

I often think it’s comical

How nature always does contrive
That every boy and every gal,
That’s born into the world alive,
Is either a little Liberal,

Or else a little Conservative!

This is the dichotomous thinking that invites us to be part of
any movement claiming to be “moral” or any movement that
claims to favor the poor and oppressed. Such follies stem from
blind submission to political symbols instead of seeing the
realities hiding beyond them. To follow the modern
ideologies, however disguised with biblical language, invites
idolatry to set the agenda for the church. The early Chris-
tans, living among eastern Mediterranean populations divided
into Jews and pagans, were called by the latter the “third
race,” and so called themselves. As long as they did not think
of themselves as belonging to one branch or the other of a
twofold division of the world, they could truly be Christian, ©
So it is now. Christians can be liberals if they wish, or conser-
vatives, or radicals, but not until they unmask those false im-
ages can they fulfill their real responsibilities.

It may be, then, that the only healthy relationship the
church can have with the political parties is one of mutual
suspicion, with a willingness to undertake short-term alliances
of limited scope. Since each side is marching to a different
drummer, it is difficult to see how the relationship can be any
firmer, unless one or the other capitulates. Recent history is
not encouraging about which side that would be. If we are
successful, no party could lightly legislate or enforce the law
in ways that are repugnant to Christians. They may finally do
so, but only at political cost.

Representative government is worthy of support in princi-
ple because the biblical view of human nature concludes that
all of us are fawed and unable to handle unlimited power
without falling into pride and irresponsibility, Nevertheless,
the ratification of law by majority vote dees not validate it. To
the democratic ideology, any action is just if it is approved by

10, See R. A, Markus, Christianity in the Roman World (New York:
Scribner's, 1974), pp. 244f.

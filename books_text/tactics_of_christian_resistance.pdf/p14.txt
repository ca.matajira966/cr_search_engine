xiv CHRISTIANITY AND GIVILIZATION

Justice John Marshall declared in the landmark case of 1819,
M'Culloch v. Maryland, In that case, the Court declared un-
constitutional the law permitting a state to levy a tax on
banknotes issued by banks not chartered by the state. The
bank in question had been chartered by the Federal govern-
ment, but not by the state. The constitutional battle was a
battle over sovereignty. Two governments —a state government
and the Federal government—were in conflict. Marshall's
words are significant. The principle of governmental sovereignty
was the issue.

. .. That the power to tax involves the power to destroy; that the
power to destroy may defeat and render useless the power to cre-
ate; that there is a plain repugnance, in conferring on one gov-
ernment a power to control the constitutional measures of
another, which other, with respect to those very measures, is
declared to be supreme over that which exerts the control, are
propositions not to be denied... . The question is, in truth, a
question of supremacy; and if the right of the States to tax the
means employed by the general government be conceded, the
declaration that the constitution, and the laws made in pursuance
thereof, shall be the supreme law of the land, is empty and un-
meaning declamation. .. .5

The question today is this: Is one or another civil govern-
ment the only kind of government allowed to claim definitive
sovereignty? Is the church, as the body of Jesus Christ, not
also entitled in the eyes of God to sovereignty equal to that of
any civil government? While the courts of the civil govern-
ments may not today recognize such sovereignty on the part
of Christ's churches, does this deny its existence? Should the
officers of the various churches act as though the State’s claim
is valid, as if the State has the right before God to impose
taxes on the church, thereby acquiescing to the State’s claim
that it possesses sovereignty superior to that of the church? If
the State declares that it has incorporated the churches and
therefore has legitimate sovereignty over the churches, should
the churches remain incorporated? Have they the moral
obligation before God to admit that previous church officers
were remiss in appealing to the state for a charter? Should
they not de-incorporate and begin anew as sovereign agents

5, MCulloch v, Maryland (1819), in Henry Steele Commager (ed.),

Documenis of American History (6th ed.; New York: Appleton-Century-Crofis,
1958), p. 219.

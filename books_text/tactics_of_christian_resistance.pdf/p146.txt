APOLOGETICS AND STRATEGY

Gary North and David Chilton

Thus, what is of supreme importance in war is to attack the
5
enemy’s strategy. Sun Tzu, The Art of War

E are not sure when Sun Tzu of the Chinese province

of Wu lived, or if he ever really did live. What we do
know is that the collection of brief observations on war at-
tributed to Sun Tzu constitutes the finest summary of military
strategy ever written. He may have been a contemporary of
Plato, or of Alexander the Great and Aristotle in the fourth
century, B.c., in the period of Chinese civilization known as
“classical,” as we also call the civilization of Greece of this
period. B. H. Liddell Hart, the prominent British military
author and strategist, has written that “Sun Tzu’s essays on
‘The Art of War’ form the earliest of known treatises on the
subject, but have never been surpassed in comprehensiveness
and depth of understanding. They might well be termed the
concentrated essence of wisdom on the conduct of war.
Among all the military thinkers of the past, only Clauswitz is
comparable, and even he is more ‘dated’ than Sun Tzu, and in
part antiquated, although he was writing more than two thou-
sand years later. Sun Tzu has clearer vision, more profound
insight, and eternal freshness.”! He admitted that in this one
short book, Sun Tzu had incorporated “almost as much about
the fundamentals of strategy and tactics as | had covered in
more than twenty books.”?

Offensive Strategy
Sun Tzu’s comment on the necessity of overcoming the
enemy’s strategy appears in the third chapter, “Offensive

1. 8. H. Liddell Hart, “Foreword,” Sun Tzu, TheArt of Way; translated by
Gen, Samuel B. Griffith (New York: Oxford University Press, 1963), p. v.
2, Ibid, p. vii.

100

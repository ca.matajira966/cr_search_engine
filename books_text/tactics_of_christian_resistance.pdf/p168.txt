122 CHRISTIANITY AND CIVILIZATION

standards of law are completely opposed to such a form of
civil government. (As for “a sacerdotal order, claiming a
divine commission” — we already have several of those: the In-
ternal Revenue Service, the Department of Education, the
Environmental Protection Agency, the Supreme Court, CBS
News. ...)} For us, the question is not “Theocracy or no
theocracy?” but, increasingly, “Whose theocracy?” (Whenever
we deal with a question which is not a case of “either/or,” but
rather “whose” or “which,” we are dealing with what Rushdoony
calls an “inescapable concept.”)

Just as there is no legitimate possibility of talking about
“theism in general” truc Christian theism being radically and
qualitatively different from the idolatrous “theism” of
non-Christianity—so we must not speak of “theocracy in
general.” The Reformed Confessions, before they were butch-
ered (revised) by the embarrassed twentieth-century descen-
dants of the Reformers, recognized the Scriptural demand for
the Christianization of all of culture. Look, for example, at
the original wording of the Belgic Confession (1561) on the duties
of civil rulers:

Their office is not only to have regard unto and watch for the
welfare of the civil state, but also that they protect the sacred
tainistry, and thus may remove and prevent all idolatry and false
worship, that the kingdom of antichrist may be thus destroyed
and the kingdom of Christ promoted.

And the doctrine of the Westminster Confession (1646) is even
more explicit:

The civil magistrate . . . hath authority, and it is his duty, to take
order, that unity and peace be preserved in the church, that the
truth of God be kept pure and entire, that all blasphemies and
heresies be suppressed, all corruptions and abuses in worship and
discipline prevented or reformed, and all the ordinances of God
duly settled, administered, and observed. For the better effecting
whereof, he hath power to call synods, to be present at them, and
to provide that whatsoever is transacted in them be according to
the mind of God.

We should remember that Samuel Rutherford (whom Dr.
Schaeffer professes to follow) was an uncompromising
theocrat, so defined; and, as one of the members of the
Westminister Assembly, he assisted in drawing up the state-
ment quoted above (which may have been put a bit too mildly
for his tastes). Consider the following statements from his

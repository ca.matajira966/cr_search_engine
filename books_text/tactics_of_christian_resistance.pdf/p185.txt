APOLOGETICS AND STRATEGY 159

of God is not the bedrock claim of each and every book he
writes in the field of philosophy. His fundamentalist readers
do not know that he is a member of a Calvinistic church
(Presbyterian Church in America), and that he is rnore
Calvinistic than most of its pastors. In neither of Mrs,
Schaeffer’s books about her husband does this fact receive
much (if any) discussion. Again, i has been characteristic of Dr.
Schaeffer's apologetic method ta avoid some desperately needed confronta-
tions: either with the humanists (whose commen-ground phi-
losophy he still partially uses) or with the fundamentalists
(who would be alienated by a forthright presentation of his
Calvinistic theology).

What we need is @ theology of confrontation. We need a fully
developed philosophy which says plainly, “Christ or chaos,
God's law or tyranny.” We have not been presented with a full-
scale version of such a philosophy by Francis Schaeffer or the
leaders of the fundamentalist world. But the humanists can
nevertheless sense where the Christians are headed. Abortion
has provided an inescapable issue, and the Christians have
chosen to fight. An army is being raised up. The leaders of
this army are still operating in terms of compromised
apologetic methods of an earlier era. Because of this, Dr.
Schaeffer has offered us only half a manifesto. But there will
be other leaders and other manifestos soon. The humanists
cannot chase this army away. And as it crosses the valley to-
ward the Philistines, many of the troops are quietly picking up
the stones that Dr. Schaeffer has left behind.

To conclude, we are arguing that in order for a successful
full-scale Christian counter-oflensive to be launched and com-
pleted against the humanist civilization of our day, Christians
must adopt and then operate in terms of four fundamental
doctrines: 1) the absolute and unqualified sovereignty of God;
2) the absolute and unqualified self-sufficiency of Scripture,
meaning a presuppositional apologetic; 3) an optimistic
eschatology; and 4) the continuing validity and binding
character of biblical law. Few Christians will affirm even one
of these doctrines today. Until the bulk of those who serve in
the Christian resistance movement do, the humanists will be
able to disregard the Christian resistance movement, except
as an annoyance.

Our conclusion will not be accepted by most of those nec-
essarily dedicated people who are in the Christian resistance

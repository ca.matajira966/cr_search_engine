CONFRONTATION WITH BUREAUCRACY 143

to Rome was constant: “We are the most law-abiding, most
productive members of this society. Let us alone and you will
benefit.”

Tertullian’s Apology, written around the year 200 a.p., is
only one of many such documents, though one of the most
eloquent. He stated his case in no uncertain terms: the
Roman authorities needed the growing Christian minority:

We are but of yesterday, and we have filled every place among
you—cities, islands, fortresses, towns, market-places, the very
camp, tribes, companies, palace, senate, forum —we have left
nothing to you but the temples of your gods... . For if such
multitudes of men were to break away from you, and betake
themselves to some remote corner of the world, why, the very loss
of so many citizens, whatever sort they were, would cover the
empire with shame; nay, in the very forsaking, vengeance would
be inflicted. Why, you would be horror-struck at the solitude in
which you would find yourselves, at such an all-prevailing
silence, and that stupor as of a dead world. You would seek to
have subjects to govern, You would have more enemies than
citizens remaining. For now it is the immense number of Chris-
tians which makes your enemies so few~almost all the in-
habitants of your various cities being followers of Christ. Yet you
choose to call us enemies of the human race, rather than of
human error. Nay, who would deliver you from those secret foes,
ever busy both destroying your souls and ruining your health?+

Intermittent persecutions by the Roman Empire always
were replaced by periods of toleration. Rome’s authorities
could not afford the costs of imposing endiess persecution on
Christian people. During periods of persecution, the church
was strengthened by a kind of scraping away of the marginally
faithful, but the persecutions did not last long enough to wipe
out the institution as a whole. As F. F. Bruce writes: “What
did the Christians do by way of reaction to the persecution
which befell them from time to time? First and foremost, they
maintained their faith and continued to propagate it so suc-
cessfully that their numbers went on increasing."

3. Tertullian, Apology, in The Ante-Nicene Fathers (Grand Rapids, MU:
Eerdmans, 1978), IT.

4. Ibid., Bk. XXXVO.

5. FF Bruce, The Spreading Flame: The Rise and Progress of Christianity from
its First Beginnings to the Conversion of the English (Grand Rapids, MI: Eerd-
mans, 1958), p. 176.

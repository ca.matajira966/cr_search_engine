EDITOR’S INTRODUCTION xxi

Whitehead’s warning takes on new significance since May
of 1983. In that month, two key decisions were made by the
U.S. Supreme Court. The Bob Jones University case of May
24 received a lot of publicity. The Court rermoved the univer-
sity’s tax exemption because it affirmed a policy of racial
segregation (dating, marriage). This policy is in opposition to
public policy, the Court said, so no exemption would be
allowed.

The more significant decision was handed down on May
23, in the case of Regan v Taxation With Representation of
Washington. It received virtually no publicity. A previously
tax-exempt organization had been lobbying in Washington.
The L.R.S. had revoked its tax-exernpt status. The Supreme
Court upheld this decision of the 1.R.S. The opinion of Justice
Rehnquist is significant:

Both tax exemptions and tax-deductibility are a form of subsidy
that is administered through a tax system. A tax exemption has
much the same effect as a cash grant to the organization of the
amount of tax it would have to pay on its income.

From a strictly economic standpoint, Justice Rehnquist is
correct. If the government allows someone to keep money he
would otherwise have been obliged to pay to the tax collector,
the narrowly defined economic effects are the same as if the
State had granted him a direct subsidy. The higher the rate of
taxation, the larger the subsidy. This is a very good reason
why taxes should be limited to under 10% of net income: it
would reduce the size of all tax-exemption “subsidies.” The
State therefore would lose much of its clout in the area of tax~
exempt institutions, because the threat of the removal. of the
“subsidy” would not drastically affect the survival of the
organization. But if the Court intends to adopt a narrowly
economic definition of the meaning of tax-deductibility (let
alone tax exemption), in order to force ali tax-exempt
organizations to adhere to every conceivable public policy (as
defined by the Supreme Court), then an endless war is about
to begin between politicians, bureaucrats, and I.R.S. officials
on one side, and tax-exempt organizations that do not “follow
the public policy guidelines” —and there are seemingly an in-
numerable number of public policy guidelines—on the other.

The race issue in the case of Bob Jones University is now
in principle settled. The Court has set the precedent. Will the

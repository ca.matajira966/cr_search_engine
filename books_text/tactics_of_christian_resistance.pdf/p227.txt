CONFRONTATION WITH BUREAUCRACY 181

Soviet Embassy in Washington for well over a decade, day in
and day out. They protest the poor treatment of Jews in the
Soviet Union. They hand out brochures. This infuriates the
Soviets, but there is nothing they can do about it. These
picketers are dedicated. They have a cause to defend. Why
should Christians be any less committed?

Picketing gets public attention. Some people resent it, but
not all. The point is, you can enlist the support of some
passers-by if you are out there handing out literature and ask-
ing people to sign a petition. It really is not crucial what the
petition says or what effect it will have. The point is, pou yet the
name and address af someone who may be of assistance te you later on.
This goes into your files, preferably computerized.

Ifnecessary, go down each morning to the city hall and get
authorization to demonstrate. This protects you. You may nol
have to do this initially, but if the bureaucrats protest your
picketing, theri get authorization. You are, as always, exercis-
ing your First Amendment liberties as a citizen, This argu-
ment carries weight. You are exercising your right of free
speech and also free assembly. You are expressing your opin-
ions publicly. It is risky to deny you these rights.

If you are denied your written daily or weekly authoriza-
tion, insist that the bureaucrat who denies you your permit
put it m writing on official stationery that he is personally forbidding
Jou to demonsirate. Write up the statement you want him to type
up and sign. Naturally, he will refuse. When he refuses, de-
mand to speak to his superior and get him to sign. Always
carry a tape recorder and have a witness present when you con-
front a bureaucrat who is likely to deny you anything. You do
not need to let him see it. It is wise not to threaten anything, or
reveal a recorder, until after the bureaucrats has said “no.”

Organize teams of mothers or others who are willing to
commit a few hours during the week. Organize a time
schedule. Get sorneone to phone each morning to make sure
no one has forgotten. Protesting effectively takes organiza-
tion, just like anything else. Make up a few signs that say “Let
My People Go,” or “Parents’ Rights to Educate Their Chil-
dren,” or “First Amendment Liberties,” or “Our School Costs
Taxpayers Nothing,” or “Why Are We Being Persecuted?” or
“Keep Our Schoo! Open,” or “Christians Have Rights, Too,”
or “Citizens for Non-Humanist Education.” Get some signs
for the kids to carry in the afternoon, after school: “Of Course

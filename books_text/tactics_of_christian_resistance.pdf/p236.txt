190 CHRISTIANITY AND CIVILIZATION

work towards comprehensive victory.

Only since the late 1970s have even a small number of
Christian leaders begun to perceive the necessity of entering
the arena of conflict. Only recently have they begun to use the
language of victory in motivating their followers. But the
change is here. The humanists are worried about it, as well
they should be. Our advantage at this point is that we seem to
be innocuous. We seem to be no threat to the entrenched hu-
manist system. Organizationally, we are no real threat, yet,
but with time, training, experience, and the proper motiva-
tion, we will be a threat. Their head was crushed by Jesus
Christ at the cross; the fact that Satan bruised our heel is
neither here nor there. As time goes on, we will crush their in-
stitutions just as surely as Christ crushed Satan’s head at
Calvary. We will replace them in those institutions that are
biblically sanctioned and therefore allowed to remain stan-
ding. (The public schools will not be among the institutions
left standing.)

The non-revolutionary humanists always escalate slowly.
This is the way of all non-revolutionary bureaucracies. They
are going to pressure us intermittently, and in so doing, will
give Christian leaders incentives to learn the skills of
resistance. The present pace of tyranny matches our woefully
inexperienced condition. We will “learn by doing.” We will be
taught how to fight by our opponents. They are unlikely to
take the offensive rapidly and comprehensively enough to do
more than make people angry enough to resist. This is just
what Christians need.

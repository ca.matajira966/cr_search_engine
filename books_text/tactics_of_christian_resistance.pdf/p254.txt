208 CHRISTIANITY AND CIVILIZATION

justification for the political charges. As a result, he asked for
an advisor who would be capable of understanding the essen-
tials of the case. Such an advisor was called a “concilium,”!°
The man whom Festus asked to hear and render an opinion
was King Agrippa (Acts 25:22). In Acts 25, Paul had already
used his legal right to “appeal to Gaesar® (which was known as
the right of “provocatio”!!). He did this in order to put aside
the jurisdiction of Festus. As a result, Agrippa would have no
say in either Paul’s immediate condemnation or release.
Festus, however, needed Agrippa’s help in signifying the
nature of the crime in order to write his report intelligently in
sending Paul to Rome (Acts 25:27). Paul, therefore, was more
than willing to argue his case before Agrippa in order to
establish clearly the exact nature of all the things with which
he was accused (Acts 26:2). Paul must have known that
Agrippa’s “expertise” (Acts 26:3) would be cited in any report
which would be subsequently sent to Caesar. Apparently,
Paul argued successfully enough to convince Agrippa that
there truly was no basis for the accusations made by the Jews
(Acts 26:32). Such successful argumentation before King
Agrippa could help Paul immeasurably in light of the fact that
the reputation of King Agrippa carried great weight in
Roman politics.”

There are two other points of Roman law which Paul used
to his advantage. Earlier in Acts 22:25-30, Paul forcefully
asserted his legal rights as a Roman citizen instead of passively
accepting an unjust beating at the hands of the centurion.
This appeal to his citizenship was of such weight that the cen-
turion and chief-captain immediately ordered a procedural
change from the informal harassment of Acts 22:25 to the for-
mal hearing of Acts 22:29-30. This, you will remember, is the
same Paul who insisted that we render obedience to the
powers that be. One thing is certain, Paul was not one to
merely accept any verdict of the state simply by virtue of its
authority as the state.

The last point which should be emphasized was Paul’s un-
derstanding of the importance of case-precedent. A case-
precedent is an important case which has some outstanding

10. Zid, p, 17.

11. Ibid., pp. 63-64.

12. Alfred Edersheim, The History of the Jewish Nation (Grand Rapids:
Baker Book House, 1979), p. 22.

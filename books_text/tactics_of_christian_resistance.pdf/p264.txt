218 CHRISTIANITY AND CIVILIZATION

cut— 30 days, initially—“they can write two or three pages
with their goose quills or their typewriters, and that consti-
tutes the protest. They do nat have time to research it and
dump tens of thousands of bytes into a computer and correlate
the whole thing.” TELAV’s appeal, on the other hand, ran to
600 pages in three volumes.

A Mixed Victory

The BLM canceled the Grouse Creek timber sale just be-
fore it was to go before the Board of Appeals. TELAV had
won-- or had it? By withdrawing from a case that was heavily
documented against it, the BLM avoided an unfavorable rul-
ing that could have been used as a precedent by protesting
groups in other parts of the country.

BLM officials shrug off the Grouse Creek incident as “an
oversight on our part,” caused mainly by a change in state
regulations that tightened up the definition of commercial
timberland. “It was like changing the rules in the third quarter
of a football game,” explains John Dutcher, the Little
Applegate Valley Area manager for the BLM. “Based on that,
we withdrew,” he says, somewhat unnecessarily adding that “it
was kind of awkward.”

But TELAV got its hands on the ball again when the BLM
put the 237-acre Lick Gulch timber sale out for bid; the Bachs
cranked out another 600-page protest, and the proposed sale
is still on appeal. “We're all hanging our hats on that one,”
Dutcher says, but when it will be decided is anybody’s guess.
The BLM is still trying to sell more timber in the area, but
now that the basic appeal format is filed on a floppy disk, Jenny
Windsor, another TELAV activist with an Apple, has cranked
out protests on four more timber sales totaling more than
2,000 acres. She simply calls up the statement of reasons from
the word-processing file, changes the arguments to fit the cur-
rent sale, prints it out, and sends it off.

The arguments have settled into a typical standoff:
TELAV says the valley is too marginal to produce timber, but
the BLM says it’s good enough.

TELAV has since incorporated as a nonprofit organiza-
tion. Using a simple database program called Visifile, one of
Visicorp’s series of packaged programs for microcomputers,
the group has computerized all of the BLM’s reforestation

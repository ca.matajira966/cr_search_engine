COMPUTER GUERRILLAS 219

records in the area, thus putting it on more equal footing with
the federal agency in knowing exactly what’s happening to the
forest lands. This effort has also drawn grudging admiration
from the BLM’s Dutcher. “They do have a neat computer sys-
tem,” he says. “I’ve often kidded them about having better
records than we do.”

Meanwhile, the use of computers to protect Little Apple-
gate Valley is spreading. Another protest group called Apple-
gate Citizens Opposed to Toxie Sprays (ACOTS) recently ac-
quired an Apple II and has prevailed upon Windsor to help
the group get up to speed on its machine.

What the Bachs and their Little Applegate Valley
neighbors did was especially impressive because they came off
the street, so to speak, with no computer background. They
had neither computer-science skills nor an interest in becom-
ing computer programmers, and they didn’t. They were able
to use their computers in the purest sense: as general-purpose
tools. In some respects they were lucky; coming up against an
arm of the federal bureaucracy like the BLM was somewhat
like shooting fish in a barrel. But what about corporate Amer-
ica and the industries that have the legal and financial clout to
stand up to, if not to dominate, the agencies that are supposed
to regulate them? What about an industry that’s an institution
in itself, like the nations utilities?

Taking On the Utilities

The Environmental Defense Fund (EDF) is one of a
number of environmental and consumer groups that have for
years been jousting with utilities over soaring gas and electric
rates and questioning the need for new power plants, particu-
larly nuclear ones. EDF’s goal is to ease the pressure on the
nation’s air, water, and energy resources by arguing for less
exploitation and more conservation. Its focuses are water
quality, toxic chemicals, lead in gasoline, and energy use. Its
weaporis are economic analysis and legal action.

Following the oil crunch of the mid-1970s and the subse-
quent skyrocketing oil prices, EDF began to lose its record of
frequent intervention in utility-rate cases and found itself in-
creasingly unable to stand up to the utilities.

The turning point came in a 1977 rate hearing when Zack
Willey, EDF's chief scientist, tried valiantly to take on one

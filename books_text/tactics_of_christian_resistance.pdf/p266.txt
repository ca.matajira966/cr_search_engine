220 CHRISTIANITY AND CIVILIZATION

utility company. Armed with only a pocket calculator, he sat
in vain punching in 64 variations of a single equation, no
match for the utility's foot-thick computer printout. His
failure was embarrassing. Vowing never to do that again and
having grown tired of getting sand kicked in his face by the
utilities, Willey hired Dan Kirshner, a young graduate of the
University of California at Santa Cruz with degrees in eco-
nomics and physics, to build up EDF’s computer muscle, A
self-taught programmer who looks every bit as young as he is
(he’s 28), Kirshner took six months to computerize Willey’s
single-equation model. The result, written in FORTRAN, is
ELFIN (for electric/finance), a model that allows EDF or
anyone else who uses it to quantify the effects of alternative-
energy proposals. Kirshner has continued to refine ELFIN
into what is now a state-of-the-art model for analysis of alter-
native energy.

ELKIN Proves lis Worth

“Before we had the model, we were basically intimidated
by the utilities in rate hearings,” Kirshner says. “They'd pres-
ent a run from their computer and say, “These are the results.’
There was no way to really challenge it. You could ask if cost
estimates were realistic, but we had no way of examining
substitutes and alternatives.”

ELFIN not only works, it’s cheap. Each run eats up only
$12 of computer time, compared to $1000 or more for cach run
of the utility company’s computer. ELFIN will work on any
“super” minicomputer, such as Digital Equiprnent Corpora-
tion’s VAX or Data General's Eclipse (the machine in Tracy
Kidder’s Soul of a New Machine). In Berkeley, EDF buys time
on the university’s VAX computer, calling in through an
acoustic telephone modem from a dumb terminal in
Kirshner’s office.

The model was first used to pursue a case against Pacific
Gas & Electric Company, the nation’s largest utility, in 1978
rate hearings in California. “PG&E did its best to brush it
aside,” Kirshner says, and although ELFIN didn’t directly
figure in the outcome of that hearing, it turned the California
Public Utility Commission toward making conservation and
alternative energy key considerations in future electrical ex-
pansion. California has, in fact, taken the lead among the 50

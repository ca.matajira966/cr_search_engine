CITIZENS’ PARALEGAL LITIGATION TACTICS

Michael R. Gilstrap

LL government begins with the sovereignty of God.

Men are fully responsible before God, in terms of the re-
vealed Jaw of God. The concept of “government” is broad. It
includes self-government, family government, ecclesiastical
government, and civil government.

English and American common law recognizes the re-
sponsibility of citizens to abide by the law of God and those
civil laws that are in conformity to, and extensions of, God's
revealed law. At the same time, common law also recognizes
the obligations of all citizens to see to it that no area of life to
which civil law legitimately applies is allowed to continue in
lawlessness. The tradition of ctizzen’s arrest is a relic of this prin-
ciple of citizen’s authority.

With the advent of a society of litigation, in which the per
capita number of lawyers is higher than in any other nation
on earth, American citizens have increasingly deferred to “the
legal experts” in every area of life. “Laymen” is a term of con-
descension, if not contempt, in the field of civil law. But the
growing complexity of legislation —the product of a messianic
impulse on the part of the law-makers—not to mention the
complexity of administrative law (bureaucracy) and judicial
review, has begun to place dedicated amateurs on a level with
all but the mast specialized lawyers. An intelligent citizen who
has been taught the rudiments of legal research is in a position
to dedicate many hours to researching one small segment of
the civil law. Most lawyers cannot devote this much time to a
detailed study of an area of law which does not lead to lots of
clients or at least lots of money. Thus, a dedicated amateur
who wants to obtain a reasonable mastery of a narrow, other-
wise ignored field, can become an opponent who is sufficiently
formidable to give the enforcing agents considerable agony.
Furthermore, if the area of law is extremely controversial,
and a lawyer taking a particular position might find himself in

233

CITIZENS’ PARALEGAL LITIGATION TACTICS 241

the statist anabaptists who insist on salvation via legislation. 5
We are told that we must blindly submit to the State for our
own good. Likewise, the anarchical anabaptists are with us.
We are told that because the State comes in and locks the door
to the Church, or takes some Christian school children away
from their parents and places them in the custody of the
welfare authorities, that this gives us the right to disregard all
of the government's laws. The sentiment seems to be, “If they
try to lock the door of one more church, there will be a bloody
war!”!? As Bible-believying Christians, when we wrestle with
the question of resistance to lawfully constituted authorities,
we must be diligent to guard ourselves against each of these
heretical positions. Anarchy is equally as dangerous, and
equally as sinful, as statism.

Canons for Biblical Resistance

From one perspective, a “canon” is a rule or law. Looking
at it from another perspective, a “canon” is a criterion or
standard for judgment. As we consider civil resistance, the
following canons should be kept in mind. They serve to guide
us in our approach and strategy, and they also enable us to
judge the righteousness of our actions.

CANON ONE: The sovereignty and ultimate jurisdiction
of God must be self-consciously confessed.

The fundamental mistake of both statist anabaptists and
anarchical anabaptists is the denial of God’s total, comprehen-
sive sovereignty. When God's ultimate jurisdiction is denied,
then resistance becomes revolution. Instead of lawfully resist-

15. For more a detailed study of this strain of anabaptism sce David
Chilton’s Productive Christians in an Age of Guilt-Manipulators, available from
Geneva Divinity School Press, 708 Hamvasy Lane, Tyler, TX 75701. Mr
Chilton deals in detail with the statist heresy propagated by Ronald Sider,
author of Rich Christians in an Age of Hunger.

16. Although he called himself a presbyterian, Vie Lockman should also
be seen in this category.

17. This description is not intended to reflect the statements of any one
individual. The sentiments, however, have been expressed to the author on
more than one occasion. They are also being expressed as an undercurrent
among many of the independent churches that are presently under direct at-
tack by the State.

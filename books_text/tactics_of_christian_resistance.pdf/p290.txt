244 CHRISTIANITY AND CIVILIZATION

government, it is not a sovereign over law, but is under law. It
is not a creator of law, but a minister of God’s law.”

It was in terms of this Christian consideration that the
Constitutional formulation of “express powers” was devel-
oped.” Our system is a system of enumerated powers. Powers
granted to civi] government in our Constitution are explicitly
granted. If a power is not given, then it is absolutely denied
from a Constitutional perspective.

CANON FIVE; Law is addressed to both individuals and
rulers, and therefore both are responsible to God.

It is important to assert that rulers are not the only ones
responsible to obey and enforce the law. Individuals have
responsibilities here, too, Neither is exclusively authorized to
enforce the law. Both are responsible to God. Once again, the
assertion of Gad’s sovereignty is implied. Responsibility be-
fore God connotes subordination to God’s law; that is, God’s
sovereignty. When this responsibility is removed, anarchy or
statism are the only possible results in the long run.

CANON SIX: Biblical political action must have a legal
foundation,

All obedience and disobedience is grounded upon law. Ulti-
mately, that law is God’s, but, as we have seen, there are prox-
imate laws and authorities ordamed by God. Because we do
not absolutize any one of these lesser authorities, disobedience
to a lesser authority is possible without sin. The most notable
example of this is Peter and John’s declaration in Acts 5, “We
must obey God rather than men,” Another example would be
the case of a pagan husband’s commanding his wife not to go
and worship the Lord. The wife would be obligated to disobey
her husband and assemble with God’s people to worship Him.

Jn the civil realm, we must assert that resistance against an
unlawful act is not rebellion but the mainienance of law. We do not
believe, as do the anarchists, that we are in no way bound to

22, For a fuller discussion of this point, see Michael R, Gilstrap, “John
Calvin's Theology of Resistance," in Gary North, ed., The Theology of Chris-
tian Resistance, Christianity and Givilization No. 2 (Tyler, TX: Geneva
Divinity School Press, 1983).

23. This Independent Republic, p. 37.

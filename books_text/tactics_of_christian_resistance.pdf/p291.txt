CITIZENS’ PARALEGAL LYFIGATION TACTICS 245

obey the laws of the land. Our assertion is that in the face of il-
legal, unjust governmental actions, resistance to those actions
is the establishment of law, rather than its overthrow. This
became a principle of international law with the Nuremburg
trials after the Second World War. The excuse, “I was only fol-
lowing orders,” was repudiated as a legal defense. In any
political action, we must first assume a solid foundation of
law, and then proceed with our resistance.

Summary

These six canons give us the standard to work out a
strategy of resistance to the IRS. We shall first examine in
detail the Constitutionality of IRS attempts to collect income
taxes from unincorporated, “unprivileged” citizens. The legal
basis of this argument will first be established by reviewing
the Constitution and pertinent IRS Codes. Only after a
careful study of the relevant Constitutional issues can an in-
dividual begin to assess a proper response to unconstitutional
invasions of the legal immunities of citizens. At that point, we
shall apply what we. have learned to the issue of ecclesiastical
resistance to the IRS. Churches are by and large unaware of
the great danger that confronts them. Our discussion will
then end with several appendices outlining separate, but
related matters of concern for the Christian as he con-
templates the rationale and strategy of civil resistance.

Individual Resistance to the IRS

The IRS is the most feared of all government agencies,
even more so than the CIA. Since 1913 it has been used to
bring the American people into compliance with an op-
pressive tax burden.® If the IRS can audit, harass, and in-
timidate churches into compliance, as it has been able to do

24. The danger of this precedent as it applies to one nation’s enforcement
of a law retroactively on a defeated nation after a war should be obvious: the
leaders of a nation that is losing will “fight to che last man,” or last nuclear
warhead. They will fear the post-war “victor’s justice”

25. The United States would have to roll back taxes 30% to 50% to even
reach the taxation of Egypt under the Pharoah of Joseph's day (Genesis
41:34; 47:24), and Egypt was the most massive bureaucracy in the ancient
world— indeed, the most massive, probably, until the advent of the Com-
munist tyrannies. .

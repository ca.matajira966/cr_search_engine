CITIZENS’ PARALEGAL LITIGATION TAGTICS 249

government and the people, the Preamble gives a statement of
purpose: “in order to form a more perfect union, establish
justice, insure domestic tranquility, provide for the common
defense, promote the general welfare, and secure the blessings
of liberty to ourselves and our posterity... .” Modern
students of the Constitution almost universally misinterpret
these clauses to be grants of power to the Federal Government,
rather than a statement of purpose. Even a rudimentary un-
derstanding of the English language militates against such a
construction. The clauses are not grants of power, but a state-
ment of purpose for the constituting of a government. In later
sections of the Constitution, the specific powers necessary to
accomplish the stated purpose are outlined and granted. Our
Founding Fathers never intended to give the broad, unilateral
powers claimed for civil government by modern students in
appealing to the Preamble and related sections.

This brings us to a basic distinction which must be under-
stood in order to grasp the fundamental nature of any system
of government, The Constitution does not create any “rights”
for the Federal Government. Civil government does not have
any “rights,” only “powers” to act and “procedures to follow.”
“We, the People,” on the other hand, have “rights” (abilities)
granted to us by our Creator, and under the Lord’s universally
applicable law, we are also given duties and obligations,
Those laws are found in the Bible, and not in some statute
book or regulatory agency’s manual. As a result of our posi-
tion, we also have immunities to certain powers and procedures
of Federal Government.*!

The distinction between “rights” and “powers” also points
out the fact that the Constitution places very strict jurisdic-
tional restrictions on many functions of Federal Government.
A very pertinent case in point is that the power to “lay and col-
lect taxes” is given to Congress. By doing so, there is an ex-
plicit jurisdictional restriction of power. In other words, one of the
other branches of Federal Government has no power to “lay
and collect taxes.” Now, what branch of government lays and
collects taxes today? Although Congress chartered the Inter-
nal Revenue Service, the IRS is part of the Executive Branch,
and as such is oudside of its constitutional authority to collect taxes and

31, For an application of this principle to churches, see section below tided,
“Ecclesiastical Resistance to the IRS.”

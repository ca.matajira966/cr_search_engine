EDITOR’S INTRODUCTION XXXL

Christians need not worry about the outcome of the war.
The outcome has never been in doubt. Christians also need
not fret about the competence of the grand strategy. We do not
know exactly how this strategy will be imposed on history, but
we know that it will be.1® What must concern us is aur performance
of the tactical duties assigned to us. Ours responsibility is to
recognize the nature of the warfare of our age, and to deploy
the forces entrusted to us by God. The grand strategy is God’s
and is known only to God. We know only the general prin-
ciples of action. But these principles are perfectly integrated
with the grand strategy, so that as we seek to conform our-
selves ethically to God's requirements, we thereby take our
positions not only in the grand strategy but also in our tactical
command posts.

We must perceive the principles of the grand strategy —
biblical law—and apply them in concrete historical cir-
cumstances. We must master the details of our own callings,
so that we will be able to perceive the tactical requirements of
the hour. Our strategic plans are, at best, larger tactical plans.
No man lives long enough to develop overall strategies, and
few human institutions survive the test of the battlefield. They
become citadels to conquer and hold. Some of them—~
universities, for example — have been captured by the enemy
within a generation, Satan and his troops seem to be superior
in the development of successful tactics, but it is the long-term
strategy which counts. It is the integrating framework for that
strategy — biblical law—which will enable Christians to
develop more powerful tactics that will fit God’s overall
strategy.

Tactical considerations should be governed by long-run
considerations. What kinds of actions are appropriate? we ask
ourselves. We should seek answers that are conformed. to
strategic requirements:

1, How does biblical law apply in this situation?
2. What are my capacities as a leader?

3. What resources do I have available?

4, What are the capacities of my colleagues?

5. What resources do we have available?

6. Which issues must be dealt with immediately?
7, What is a proper plan of action?

16. R. J. Rushdoony, The Biblical Philosophy of History (Fairfax, VA:
Thoburn Press, [1969] 1978).

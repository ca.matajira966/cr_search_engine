CITIZENS’ PARALEGAL LITIGATION TACTICS 27i

apt to try on the paralegal litigant. The paralegal litigant
should expect such delusion. “1984” is upon us, and the IRS is
a master of “newspeak.” For example, the IRS asserts that the
Federal system of taxation is one of “voluntary compliance.”
Now think about that. What do they realy mean? I think it is
something along the lines of “Do what I order you to do volun-
tarily, or I will force you to comply with threat of fine or jail.”

Ecclesiastical Resistance to the IRS

Now that we have outlined the strategy for individuals in
resisting the IRS, it is absolutely vital that we cover in this
section two crucial “Achilles’ Heels” that will plague the
Church if efforts are not taken to strengthen these weak
points. Both instances revolve around the issue of “jurisdic-
tion” or “sovereignty.” We have mentioned the significance of
this word in our earlier study, but it bears reviewing.

The legal meaning of the word is very large and com-
prehensive. Practically speaking, it refers to “authority.” If a
court or administrative agency has “jurisdiction,” it means
that the court or administrative agency has the authority to take
cognizance of and decide cases. It is the legal right by which
judges exercise their authority, and by which the power and
authority of a court to hear and determine a judicial pro-
ceeding is established. Furthermore, “jurisdiction” is the right
and power of a court to adjudicate concerning the subject
matter in a given case.

The jurisdictional question that confronts the Church is
“Who das authority over the Church?” Does Jesus Christ, as
Head of the Church, have sole jurisdiction; or does the State
have some sort of jurisdiction also? The question here must be
divided. In the sense that the Church is “all Christians in all of
life,” Christ is sole head. The Church as an institution claims
Christ as unique King, but Christ also claims to be King of
kings, King of the nations, King of the State. It has been, and
still is the belief of orthedox Christianity that Christ alone is
Head of the Church, and of the State. That His Word is our
only source of Law. That the Church is a separate, created in-
stitution; equal with or on the same level as the State in one
sense, but having a kind of prirnacy as the locus of the procla-
mation of God’s Word, which governs the State as well. The
Church is not subject to the State; that is, the State does not

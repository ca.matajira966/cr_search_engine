276 CHRISTIANITY AND CIVILIZATION

portant assertions of the Church in the remaining years of this century
Churches must be ready to challenge the jurisdiction of the
IRS or any other Federal agency with the matter of fact of the
Church’s immunity. We must forever strike from our
vocabulary the notion of “tax-exemption” for the Church.“

What does a church do if it is already a 501(c)(3) organiza-
tion and wishes to get out from under that classification?
First, the officers of the church must write a letter to the Inter-
nal Revenue Service instructing, not requesting, that the IRS
dissolve its 50i(c)(3) classification of the church. The letter
should briefly list the principled reasoning supporting the re-
quest, and give a reasonable time limit to respond. Second,
the officers of the church should send a request pursuant to
the Freedom of Information Act® for a copy of all documents,
memoranda, ete, that may be in the possession of the IRS.
Third, if there is no response from the IRS in one month,
then write a second letter a little more strongly worded than
the first enclosing a copy of the first letter. Fourth, if a satisfac-
tory response is not immediately forthcoming, then a final let-
ter must be written. In this letter all the stops should be pulled
out,

Begin with a categorical statement that “XYZ Church’ re-
nounces the pretended jurisdiction of the Federal Govern-
ment through the agency of the Internal Revenue Service.
State in no uncertain terms the reasons behind this assertion.

60. This will become increasingly more important when local state and
county governments begin to attempt to levy property taxes against church
property, This contemptible action is already being done in California, and
it is on the agenda in many other states. In fact, the first scheduled seizure of
church property for payment of back taxes was June 30, 1983 in Poway,
California, The Pomerado Road Baptist Church was scized because it “owed”
$17,000 in back taxes to the state, In Texas, 1983 is the first year the state and
county governments have begun to appraise church property. One way to
fight these state and county taxes is to assert the church's immunity. It
should be enough to simply appeal to the Ist Amendment, but that doesn’t
seem. to be the case anymore.

61. Immunity applies only to the Church and her directly controlled
ministries. Parachurch organizations are not directly under the umbrella of
the Church, Hence, they are under government jurisdiction and subject ta
taxation or tax exemption. Perhaps the issue of taxation is one way in which
God will bring many of the parachurch activities under the discipline of His
Church.

62. See Appendix C.

GITIZENS’ PARALEGAL LITIGATION TACTICS 279

ments must be characterized by brevity, and therefore in the
nature of the case cannot be painstakingly comprehensive.
Nevertheless, I will endeavor to answer the arguments levied
against the movement by two extremely competent Reformed
theologians, R. J. Rushdoony and James B. Jordan.

Rushdoony

In an article appearing in the Journal of Christian Reconstruc-
tion (Volume 2, Winter 1975-76) titled “Jesus and the Tax
Revolt,” Rev. Rushdoony categorically argues that the “tax
revolt” is a “futile thing, a dead end, and a departure from
Biblical requirements.” In other words, he is saying that the
“tax revolt” is not only an exercise in futility, but that it is also
positively sinful.

Rev. Rushdoony advances two substantial arguments in
favor of his conclusion. The jirst is that it is wrong to assume
that one doesn’t have to pay taxes to the government in power
because the government in power is not the lawfully consti-
tuted government. He uses the distinction between a “de
facto” government and “de jure” government. A “de jure” gov-
ernment is one which rules by right of law. In America, a “de
jure” government would be one that rules strictly according to
the Constitution. On the other hand, a “de facto” government
is the one which actually exists and is in power. Rev. Rush-
docny argues that just because the government of the United
States is allegedly unconstitutional, this is no reason not to
pay your taxes. He points out that when Jesus instructed the
hypocritical Pharisees to “render therefore unto Caesar the
things which are Caesar’s; and unto God the things which are
Godl’s” (Matthew 22:19-21), He was speaking in the situation
of a “de facto” tyrannical Roman government. The Pharisees,
and all Jews for that matter, were to pay their taxes regardless
of what they thought of the Roman government, because the
Roman government was the “de facto” government. His con-
clusion then is that the “tax revolt” is unlawful because an in-
dividual in the “tax revolt” argues the same way the Pharisees
did, The “tax rebel” rcfuses to pay his taxes because the gov-
ernment is unconstitutional; that is, our present I'ederal Gov-
ernment is not the lawfully constituted government.

64. I realize that both critics wrote their arguments with the libertarian/
anarchist versions of the tax rebellion in mind. The position I am surveying
is not anarchic, but Constitutional in its presuppositions.

280 CHRISTIANITY AND CIVILIZATION

In answer to this first argument, let me first of all point out
that in the main body of this article the reasoning I have ad-
vanced for participation in the paralegal litigation movement
does not include an argument along the lines attacked by Rev.
Rushdoony. Nevertheless, Rev. Rushdoony’s argument is not
acceptable, even if we accepted his reasoning against “tax
rebels.”

In Christ’s day, what “law” were the Pharisees referring to
when they said, “Is it Jawfud to give tribute to Caesar, or not?”
Were they referring to Roman law, the “de facto” law of the
day? Not likely. It is pretty clear they were referring to the cor
pus of Jewish law, which was what the Pharisees considered the
“de jure” law within the meaning of Rev. Rushdoony’s article.
Jesus, of course, answered correctly. Render to Caesar what
Caesar’s law requires, but only so long as Caesar’s law does not re-
quire you to do something that is sinful. At the same time, give to
God what God requires. As Rev. Rushdoony points out,
Jesus answered in terms of the existing “de facto” government,
to be sure, but the thrust of the passage is tied up in our Lord’s
qualifying phrase, “Render . . . to God the things which are
God’s.” Jesus not only answered in terms of the “de facto” law,
but more importantly, He answered in terms of God and His
law, the ultimate “de jure” law which will one day be the
universal “de facto” law. The paying of the tax in Ghrist’s day
was not unlawful on either account; therefore, the people of
Palestine were to pay it.

When we move up to twentieth-century America, let us
ask the same question that the Pharisees did: “Is it law/ul to
pay income taxes or not?” Jesus said to “Render unto Caesar
the things which are Caesar’s....” In the Roman world,
Caesar was the supreme law of the land. Who is “Caesar” in
twentieth-century America? It would actually be better to ask
“What is Caesar?” because “Caesar” is not an individual in our soci-
ay. The supreme “de facto” and “de jure” law of the land in
America today is the United States Constitution.

65. It is truc that legislators, chief executives, jurists, Constitutional law
experts, and especially bureaucrats have compromised the “de facto” status
of the Constitution’s position as “Caesar,” but the court system still exists,
tial by jury still exists, and we can still appeal to that Constitution.

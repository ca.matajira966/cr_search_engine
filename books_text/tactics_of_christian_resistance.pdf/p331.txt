CITIZENS’ PARALEGAL LITIGATION TACTICS 285

A.D. Since that time, one of the major purposes of a jury has
been not to convict under laws they feel are unjust. At the time
of the Civil War, northern juries refused to convict under the
fugitive slave laws. During Prohibition, government officers
found it difficult to get a conviction under the laws against the
sale or possession of alcohol. The juries in both of these ex-
amples simply refused to convict according to the law because
in their judgment, the law was unjust or unfair.

In the main body of this article, we discussed the impor-
tance of distinguishing “matters of fact” and “arguments of
law.” We saw thai the courts have jurisdiction in both “matters
of fact” and “arguments of law.” At the time of the Dred
Scott/fugitive slave law decision, the authorities recognized
that if juries were allowed to decide on both the facts of a case
and the laws applicable to that case, then the “experts”—the
judges and lawyers—would lose a great measure of control
over the legal system. They wanted to avoid that at all costs.

Immediately following the Dred Scott decision, and for
some 30 years thereafter, the issue of “jury nullification” was
debated back and forth. Some jurists argued that since they
were no longer subjects of the King, but were in fact citizens
just like everyone else, they could be trusted to have the
citizen’s best interests at heart. Others argued that if juries
were charged with judging the justness of particular laws as
well as the facts of the case, it would put too great a mental
strain on the jurors.

In 1895 the issue came to a head in a Supreme Court deci-
sion. That noble body of jurists ruled that although juries
have the right to ignore a judge’s instructions regarding the
law, the jury shouldn't be made aware of 1t.°° What hypocrisy!
Citizens have a might that they shouldn’t be made aware of!
Judges most assuredly may be trusted with “the citizen’s best
interests!” Amazing!

Since that 1895 decision, court rulings mention the exist-
ence of “jury nullification” only grudgingly. It is not taught in
law schools. Judges refuse to tell juries or allow defense
lawyers to tell them. It is as if the right of nullification by
juries simply dropped off the face of the earth, bud there are faint
signs of resurrection!

The “jury nullification” right is not an oddball, long since

66. Sparf x U.S., 156 U.S. 51 (1895).

CITIZENS’ PARALEGAL LITIGATION TACTICS 289

grand jury has tremendously broad investigatory powers. It
can issue summons, and it is not limited to considering only
the case or cases that is set before it.

For instance, let us once again assume that the Federal
Government is pursuing indictments against individuals in
the Christian school movement. The U.S. Attorney brings an
allegation against Pastor Sullivan for operating a Christian
school, Pastor Sullivan has also refused to turn over all of his
records to the Internal Revenue Service, so the U.S. Attorney
is also asking the grand jury for an indictment against Pastor
Sullivan for “willful failure to file.” The U.S. Attorney walks
before the grand jury, pats a large stack of official papers
representing the allegations against Pastor Sullivan, and says,
*This is an open and shut case. Sullivan operates a Christian
school and refuses to cooperate with government officials. He
is just a trouble-maker. I don’t want to take any mare of your
time than necessary, so if the foreman will simply sign the in-
dictment I have already prepared, you can be on your way.”

The foreman of the grand jury, however, is a concerned
citizen. He tells the U.S. Attorney that it seems a little
premature to indict someone of such a serious crime without
even hearing his side of the story. ‘The foreman feels, if none
of his fellow jury members has any objections, that it would be
best to first talk with Pastor Sullivan before signing anything.
The rest of the jury agrees, and Sullivan is brought in.

After about 45 minutes of hearing the horror stories
perpetrated against Pastor Sullivan and his Christian schoo),
the grand jury realizes that it must conduct a full investiga-
tion. They subpeona government official after government
official. They could, if they found enough evidence, indtct every
government official who broke the law when oppressing Pastor
Sullivan. The immediate benefit would be, however, that they
would “no bill’ Pastor Sullivan; that is, they would refuse to
indict him on any charge. If enough grand juries exercised
their very broad powers, once again “We, the People” would
get civil government's attention in a hurry.

In order to take advantage of the jury nullification right,
there are several things you must do. First, to be selected for a
jury, you must be a registered voter. Second, when selected
for jury duty, don’t give any indication that you have the least
bit of intelligence. Lawyers prefer to see “no brains” sworn in. An
idiot can be controlled and influenced more easily. Never let them

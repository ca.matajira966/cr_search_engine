292 CHRISTIANITY AND CIVILIZATION

Act, on the other hand, is designed to give citizens more control
over what information is collected about them by the Federal Govern-
ment, and how that information is being used against them.
Uses of the FOIA and Privacy Act that directly apply to
the issues dealt with in this essay include: Forcing the IRS to
comply with the stipulations of the Privacy Act, using the
FOIA or the Privacy Act as a means of discovering what in-
formation the government has accumulated on an individual,
correcting any misinformation in a government file, challeng-
ing any unauthorized information collected, or using the
FOIA as a means of “discovery” in a court battle, Other com-
mon uses are: exposing possible government wrongdoing, ob-
taining agency records for a2 newspaper article, research in
academic and historical studies, obtaining factual evidence to
challenge a law, for commercial advantage, and to invalidate
an activity of a government agency. As one can see, the possi-
ble uses of the FOIA and Privacy Act are many, complex, and
varied. I have included this list simply to illustrate the breadth
of the law. In order to keep the application of these acts
straight, [ will explain haw to invoke and use the Freedom of
Information Act first, and then deal with the Privacy Act.

Using the FOIA

There are several preliminary considerations that must be
touched upon before we actually write a sample letter. First of
all, the law says that one must be able to “reasonably describe”
any and all documents or records that are sought under the
FOIA. Note, that does nof mean that an exact description is neces-
sary, but enough identifying information must be given to enable
the FOIA clerks of an agency to find the records or documents.”

Second, before using the FOJA, it is important to review the
regulations, not only the official text of the FOIA, but also any
particular regulations an agency may have pertaining to ihe FOLA.

70, An ‘informal’ call to a bureaucrat is sumetimes helpful at this point.
If you are wise and crafty enough, you can often get the bureaucrat to slip
with a name or at least enough information to help you avoid further delays
when you finally make a formal FOIA request.

I might also mention at this point that it is most important to keep records
or notes of any and all conversations, meetings, etc. with a government
official. If possible, tape personal phone conversations and meetings. At the
very least make notes immediately after the meeting. Additionally, keep
copies of all correspondence.

xxiv CHRISTIANITY AND CIVILIZATION

The murder of two Federal agents in early 1983 by a so-
called “Christian tax resister” (as the media dubbed him),
Gordon Kahl, indicates just how serious—and utterly mis-
guided — some of these “Christian patriots” are. Two Federal
officers are dead. So is the murderer. Are our taxes any lower?
Is inflation any less a threat? Is the Federal deficit any
smaller? Let’s face it: if every IRS agent in the country were
murdered, the Federal Reserve System could still create the
money, buy government bonds (or anything else), and let the
government spend the newly created fiat money into circula-
tion. This would “tax” us through price inflation. Americans
would then be forced to pay the inflation tax rather than pay
the Federal income tax. Yet Kahl is being portrayed as some
kind of hero or martyr to a magnificent cause. A flyer on
“Kahliphobia” is circulating in Far Right circles—a veiled
threat to ERS officials that they, too, risk being murdered.

What has murder got to do with tax protesting? Tax
resistance in this case is simply a false ethical shield used by
guilty, violent men. Such bloody protesting is revolutionary
romanticism in action. Did the murderer accomplish any-
thing for other tax resisters or his family by dying in a shoot-
out with authorities after he had vowed not to be taken alive?
What “magnificent cause” did he die for? What glory did he
bring to God or His church by his romantic, suicidal stand?

I spoke to one courageous Christian resister in the sum-
mer of 1983. He predicted (perhaps “vowed” is more
accurate): “There’s going to be a bloody revolution in this
country.” I think he is out of touch with the mainstream of
fundamentalist churches. He has given too many rousing

18.. Counterfeiting Federal Reserve Notes and passing them to unsuspec-
ting private individuals in order to “help pay for the tax resistance move-
ment” partakes of the same immoral romanticism. How is the economy
helped by new injections of fiat money into the economy? Why is private
counterfeiting moral when official central bank counterfeiting is immoral?
Yet I received a letter from one tax protestor who justified counterfeiting as
a valid means of protest. He defended Vie Juockman’s actions in
counterfeiting money for “the cause”~ actions which Lockman himself had
already publicly repented of ~by arguing that if the Federal Reserve System
has the legal right to counterfeit raoney, so should private citizens. Here is a
classic example of the old “two wrongs make a right” ethic. Some segments of the
Far Right have already adopted tactics based on ethizal rebellion, They are also very
close to the violence of romantic revolution.

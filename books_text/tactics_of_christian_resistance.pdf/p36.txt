xxxvi CHRISTIANITY AND CIVILIZATION

lives. They need to understand the theology of resistance, the
tactics of resistance, and the goal of resistance: the Christian
reconstruction of the republic,?@ and then the world. We do
not want or need bloodshed in our struggle against the legally
constituted civil government. (On:the other hand, we may
well need to defend ourselves against lawless anarchists and
criminal bands if the “establishment” order breaks down.)

What we are beginning to hear is apocalyptic rhetoric con-
cerning the moral responsibility of Christians to take up arms
if some church has its tax exempt status revoked, or worse, is
sold at auction for non-payment of property taxes. Pastors
who said nothing when the Supreme Court legalized abortion,
or who have preached an eschatology of non-involvement in
“worldly’ affairs for two decades, are overnight so intensely
involved with “basic principle” that they recommend a suicidal
strike against the Federal government should their church lose
its present status as a “tolerated but ignored organization.”
Why this life-and-death commitment all of a sudden?

Did David murder Saul when Saul persecuted him? David
was forced to flee Israel and pretend to be crazy in the pres-
ence of the Philistines—and not just the Philistines, but the
king of Gath, the'city of Goliath (I Sam. 21:10-15). David was
God’s anointed, yet he did not challenge Saul. He did not sur-
tender, either. He wrote: “I shall not die, but live, and declare
the works of the Lord” (Ps. 118:17).

Is the essence of the gospel tax exemption and bricks and
mortar? Is the heart of our stand for Christ our willingness to
defend to the death our control over a particular piece of
previously tax-exempt ground? This is nonsensical. It is
romanticism. Rushdoony writes concerning the revolu-
tionary: “The destiny of the revolutionary is to hunger for life
and to know that, in spite of all, death is his future. Death is
for him the great enemy, but he longs for death also, because
all of life betrays him in his pretended divinity."2! Why should
self-professed Christians adopt the same sort of suicide men-
tality? Why should they take up arms against the State visibly,
just because some official has stepped on their institutional

20. Harold O. J. Brown, The Reconstruction of the Republic (New Rochelle,
NY: Arlington House, 1977).

24, R. J. Rushdoony, Revolt Against Maturity: A Biblical Psychology of Man
(Fairfax, VA: Thoburn Press, 1977), p, 115.

THE CHURCH AS A SHADOW GOVERNMENT 333

God, however, was missing. Thus, truth became legend and
fiction.

Six, in the city of Cain, initiation was the means of entrance.
Biblical religion is not a process of initiation. The only rite of
passage, perfect obedience to the Law of God, is obtained by
Christ’s obedience. The religions of man are attainments
through some act of heroism or obedience. In a sense, man’s
labyrinthine rites of passage are new-disobedience designed to
earn salvation. Out of this perverted obedience grows all the
previous dlistinctives of initiation. Man is saved, however, by
grace through faith (Eph. 2:8-9). Faith is presuppositional.
Man cannot scize the Kingdom by any form of initiation.
Rather, entrance is like the blowing of the wind (John 3). It
comes from the Spirit of God.

Il. Manipulation

Man’s piracy of creation resulted in a city of manipula-
tion. His power avoided some of the effects of the curse.
Strong walls stood to keep away the judgment of God. Tech-
nology of every kind averted the curse of Genesis 3. Cain
spawned the land of Shinar, the cradle of the world.

Shinar is used to mean the “shaker,” or “he who throws
down.” It usually has reference to some kind of evil manipula-
tion through idols, magic, and sin. The word recurs often.
Ellul writes: “It was around the King of Shinar that the coali-
tion spoken of in Genesis 14 was formed, the coalition which
triumphed over all its enemies, captured Lot, and thereby ran
afoul of Abraham. Abraham vanquished the King of Shinar,
and Melchizedek, the King of Salem, came out to bless
Abraham: the King of righteousness, the prince of peace, the
complete opposite of the King of Shinar. Throughout the ad-
venture of the people of Israel, the presence of Shinar is clearly
the presence of a spiritual power, of a temptation to evil. It is
an object from Shinar that caused Achan’s terrible sin (Josh.
7), the transgression of the Covenant. And it is not by acci-
dent that such is the case, for this country is typically the
country of idols and sin. That is where Nebuchadnezzar took
the vessels of God’s house to incorporate them into the service
of his own god (Dan. 1:2}. Daniel purposely calls the country
Shinar instead of Babylonia, for he wanted to emphasize that
this was a land of thievery and plunder. ... In the fifth
chapter of Zechariah the prophet saw an ephah rising up from

TACTICS OF RESISTANCE
USED BY THE PROTESTANT REFORMERS

Otto J. Scott

HE Reformation came into being in opposition to the

Renaissance. Its success in this effort is the major reason
why modern anti-Christians hate the very memory of the
Reformation and its leaders, though had they not appeared,
and had their efforts not been successful, it is doubtful if the
West would have survived until today.

The Renaissance, therefore, is essential to a proper under-
standing of the Reformation. It was defined by Burckhardt, in
his brilliant but unfinished opus The Civilization of the
Renaissance in Htaly,' as the end of the Middle Ages and the be-
ginning of the modern ages. He described it “from the birth of
Dante to the death of Michelangelo, a period as long and at
least as variegated as, for instance, the time from Watteau to
Picasso, or fromm Milton to James Joyce.”*

This period of four centuries altered the life of Europe and
the attitude of Europeans beyond recall. The rediscovery of
pagan literature, with its vicious splendor, the resurrected
arguments of the Sophists, especially Plato, regarding the
soul, the afterlife, and the possibilities of power excited
scholars and poets, and dimmed the glories of Christianity.

Jialian towns argued over whether or not they were the
birthplaces of famous Romans, and such disputes gradually
displaced the shrines of the Middle Ages. Poetry contests were
revived, laurel wreaths bestowed, and Latin schools ap-
peared. Dante’s Inferno resembled Homer’s underworld of
dead shades, forced to suffer for earthly sins, replete with
topical political references.

Fame replaced immortality as the goal of life. To become
famous was to achieve the only possible immortality. In cities

1. Phaidon Publishers, 1965.
2. Ibid., Foreword, by L. Goldschieder, p. xi.

371

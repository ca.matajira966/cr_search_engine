380 CHRISTIANITY AND CIVILIZATION

Among the precautions for travel were false papers and false
names. These disguises were condoned by the authorities in
Geneva because of the holy purpose for which they were
adopted. Nevertheless, the number of aliases employed con-
fuse the records of Geneva for historians, so that precise iden-
tities are sometimes—even now— unknown. '8

The risks that were taken were as high as any known to-
day, just as the mercilous nature of the authorities then are
matched by the totalitarian powers of madern times. Same of
the travelers seem to have resorted to obscure mountain
routes up into Dauphine. A Dauphine Protestant in the 1950's
told historian Robert M. Kingdon that “old folks can still
point out the network of mountain paths by which the
ministers came into France.” The region is still dotted with
stone farmhouses that contain secret hiding places behind
chimneys or in cellars, a day’s walk apart, that were used by
the Underground against the Nazis during World War II, and
that were created during the period of the French religious
wars and the time of Calvin.

Some pastors used commercial routes, and passed them-
selves off as merchants. The records of the Bernese bailiff in
Lausanne contains a copy of a travel document used by
preacher Jehan Richard, who went to the church of Crest in
Dauphine, bearing a passport written in Latin, made out in
the proper form, identifying him as Richardus, a “merchant.”
Most such clocuments were destroyed after serving their pur-
pose; this one was overlooked.

False papers and identities, special reports, hidden
credentials, and carefully prepared routes dotted with hiding
places, lookouts, and helpers, explain why few preachers were
captured enroute to their churches. “The majority,” according
to Kingdon, “(were seized) after arrival in their new parishes
when they were actually exercising their religious mission.”

As the years passed, the Calvinists established a network
of over 2,000 churches and congregations throughout France.
The concentrations of these converts were not, of course,
uniform. But it should not be forgotten that in those days the
pulpit served as a news agency, into which information of a
political and economic, as well as religious nature flowed, and

18. {bid., pp, 33, 35, 38, 39, 40.

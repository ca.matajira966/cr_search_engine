EDITOR’S INTRODUCTION xiii

tion of resistance. We believe that these materials will serve as
the starting point for all serious, theologically informed
discussions of resistance. We fully expect these two issues to
serve as primary source documents for historians a century or
more in the future, just as Sam Adams’ papers and publica-
tions are documentary sources for the American Revolution.
This is not a matter of arrogance; this is a realistic appraisal of
just how little cogent Christian material on this topic is
available in 1983.

I can recommend one book, above all, to each and every
church. Buy it, read it, and begin to implement it. It is Gene
Sharp’s Politics of Non-Violent Action, published by Porter
Sargent in Boston. Sharp lists dozens of non-violent tactics
that have been used over the years against bureaucratic tyran-
nies. It will provide many ideas for you. (Sharp also wrote a
book on Gandhi. All books on Gandhi need to be offset in part
by the extraordinary book-length essay by Richard Grenier,
The Gandhi Nobody Knows, published by Thomas Nelson Sons
in 1983. It appeared originally as a review of the Academy
Award-winning movie, Gandhi, in the March, 1983, issue of
Commentary.)

Strategies and Tactics

Christianity and Civilization draws on authors from
numerous denominational backgrounds, not to mention
religious traditions. We have found that many conclusions of
people who do not share our opinion of the Puritan and Re-
formed tradition are often in agreement with the conclusions
of that tradition. At the same time, there are some writers
who do not seem to be consistent with their theology. We offer
these essays with a disclaimer: They have been published in
good faith by all parties, but neither the editors nor the
authors agree on all points with each of the essays in this
volume.

Part J deals with general strategies and perspectives which
Christians must bear in mind when devising tactics of resist-
ance to pagan tyranny. Theologian and evangelist Francis
Nigel Lee presents an alternative to The Communist Manifesto
in his own “Christian Manifesto,” originally published in
1974. Dr. Lee outlines the present position of Christians in the
world, and then sets out a blueprint for world conquest, in-

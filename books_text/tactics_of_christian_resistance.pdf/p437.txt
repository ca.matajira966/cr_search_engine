THE USE OF MONEY 391

the scribes and Pharisees, to whom he had been speaking be-
fore, ~“There was a certain rich man, who had a steward,
and he was accused to him of wasting his goods. And calling
him, he said, Give an account of thy stewardship, for thou
canst be no longer steward,” verses 1,2. After reciting the
method which the bad steward used, to provide against the
day of necessity, our Saviour adds, “His Lord commended the
unjust steward”; namely, in this respect, that he used timely
precaution; and subjoins this weighty reflection, “The chil-
dren of this world are wiser in their generation, than the chil-
dren of light,” verse 8: those who seek no other portion than
this world, “are wiser” (not absolutely; for they are, one and
all, the veriest fools, the most egregious madmen under
heaven; but, “in their generation,” in their own way; they are
more consistent with themselves; they are truer to their ac-
knowledged principles; they more steadily pursue their end)
“than the children of light”,;—than they who see “the light of
the glory of God, in the face of Jesus Christ.” Then follow the
words above recited: “And I,”— the only begotten Son of God,
the Creator, Lord, and Possessor, of heaven and earth and all
that is therein; the Judge of all, to whom ye are to “give an ac-
count of your stewardship,” when ye “can be no longer
stewards”; “I say unto you,”—learn in this respect, even of the
unjust steward, —“make yourselves friends,” by wise, timely
precaution, “of the mammon of unrighteousness.” “Mammon”
means riches, or money. It is termed “the mammon of un-
righteousness,” because of the unrighteous manner wherein it
is frequently procured, and wherein even that which was
honestly procured is generally employed. “Make yourselves
friends” of this, by doing all possible good, particularly to the
children of God; “that when ye fail,”—when ye return to dust,
when ye have no more place under the sun, —those of them
who are gone before, “may welcome you, into everlasting
habitations.”

An excellent branch of Christian wisdom is here inculca-
ted by our Lord on all his followers, namely, The right use of
money; —a subject largely spoken of, after their manner, by
men of the world; but not sufficiently considered by those
whom God hath chosen out of the world. These, generally, do
not consider, as the importance of the subject requires, the use
of this excellent talent. Neither do they understand how to
employ it to the greatest advantage; the introduction of which

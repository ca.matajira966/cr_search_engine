398 GHRISTIANITY AND CIVILIZATION

weight in glory.

The directions which God has given us, touching the use
of our worldly substance, may be comprised in the following
particulars. If you desire to be a faithful and wise steward, out
of that portion of your Lord’s goods, which he has for the pres-
ent lodged in your hands, but with the right of resuming
whenever it pleases him, first, provide things’ needful for
yourself; food to eat, raiment to put on, whatever nature
moderately requires for preserving the body in health and
strength. Secondly, provide these for your wile, your children,
your servants, or any others who pertain to your household.
If, when this is dane, there be an overplus still, “as you have
opportunity, do good unto all men.” In so doing, you give all
you can; nay, in a sound sense, all you have: for all that is laid
out in this manner, is really given to God. You “render unto
God the things that are God’s,” not only by what you give to
the poor, but also by that which you expend in providing
things needful for yourself and your household.

Tfthen a doubt should at any time arise in your mind con-
cerning what you are going to expend, either on yourself or
any part of your family, you have an easy way to remove it.
Calmly and seriously inquire, 1. In expending this, am I ac-
ting according to my character? Am I acting herein, not as a
proprietor, but as a steward of my Lord’s goods? 2. Am I do-
ing this in obedience to his word? In what scripture does he
require me so to do? 3. Can J offer up this action, this ex-
pense, as a sacrifice to God through Jesus Christ? 4. Have I
reason to believe, that for this very work T shall have a reward
at the resurrection of the just? You will seldom need anything
more to remove any doubt which arises on this head; but, by
this four fold consideration, you will receive clear light as to
the way wherein you should go.

If any doubt still remain, you may farther examine
yourself by prayer, according to those heads of inquiry. Try
whether you can say to the Searcher of hearts, your con-
science not condemning you, “Lord, thou seest I am going to
expend this sum, on that food, apparel, furniture. And thou
knowest, I act therein with a single eye, as a steward of thy
goods, expending this portion of them thus, in pursuance of
the design thou hadsi in entrusting me with them. Thou
knowest I do this in obedience ta thy word, as thou com-
mandest, and because thou commandest it. Let this, I

LEVERS, FULCRUMS, AND HORNETS 423

developed body of practical answers to the questions that a
newly converted world will raise?

If our answer is “No, we do not have such answers,” then
we are in the unenviable position of a newly elected President
who has no program. Wé will have to stall for time. We will have
to announce, “We can get the answers, but we will need a little
time.” After 2,000 years, and still no answers, how much
more time can we reasonably ask for? How can we ask a
newly converted world to wait patiently, as the humanist cul-
ture is collapsing, while we figure out specific, concrete answers to
specific, concrete problems? If we ask for more time, won't we
make fools of ourselves? Does Christ want us to make fools of
ourselves? If not, what does He cxpect us to do in order to
prepare for a truly biblical revival?

If we cannot come before the world, as the prophets came,
in the name of God and His law, then how can we legitimately
expect to get a revival? What kind of revival will it be? A
biblical revival? Did the prophets ask for an emotional revival
apart from a turning away from sin by the king, rulers, and
common people? Did the prophets not call for comprehensive
repentance? Were not the people of Israel in comprehensive rebel-
lion? Did the prophets offer not offer a message of comprehensive
redemption?'6 Did they not come before Israel with comprehensive
answers? Isn’t this the kind of preaching that produces biblical
revival? Can any other kind of preaching produce biblical
revival?

If we are unprepared to answer the burning issues of the
day, then we are unprepared for revival. 7 call for revival
prematurely is to call for the public humiliation of the church, Chris-
tans will be revealed as incompetents before the world. Chris-
tians talk as though they expect revival to be some sort of
zero-responsibility event—-a kind of cosmic bail-out for the
20th-century church. Was the Hebrews’ return to the land in
Nehemiah’s day a bail-out for Nehemiah? Wasn’t it an era of
major decisions? If Christians refuse to think about revival in
these terms, then they do not understand the comprehensive
implications of revival. Those who preach revival must begin to take
Steps to prepare for the social and institutional effects of revival.

The Israelites in Moses’ day were told that they would not

16. Gary North, “Comprehensive Redemption: A Theology for Social
Action,” The Joumal of Christian Reconstruction, VIII (Surnmer, 1981).

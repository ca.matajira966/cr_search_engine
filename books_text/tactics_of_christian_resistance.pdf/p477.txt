LEVERS, FULCRUMS, AND HORNETS 431

80, Christians 12. We may nol know what quarter we're in,
but we know this much: the game isn’t over yet. CBN has the
technological capacity to change the score for the better.
What will CBN have to give up? Mainly, the advertising
revenues generated from reruns of the thirty-year-old situa-
tion comedies that are broadcast after midnight. On the other
hand, the sales of workbooks, textbooks, and even tuitions
could compensate CBN for the loss of these advertising
revenues. In any case, the 20th century is not going to be
reshaped in terms of the standards of the gospel through
reruns of “My Little Margie” or “The Life of Riley,” however
harmless these shows were in 1952. Now is not the time for
harmlessness; now is the time for hornets, CBN’s satellite is a
perfect lever for stirring up God's hornets, all over the world.

Postscript

I have passed along copies of this proposal to Jerry
Falwell, of the Moral Majority, and Bill Bright, of Campus
Crusade. If Pat Robertson is unwilling to take full advantage
of his own satellite, then perhaps other ministries with a broad
outreach could buy the time on the midnight-to-five a.m. time
slot. They could prepare the necessary training materials and
hire the teachers. If they could reimburse CBN for the
revenues forfeited by the network by removing “The Life of
Riley” and other ancient sitcoms from the air, perhaps CBN’s
management would allow others to use this tool more
effectively.

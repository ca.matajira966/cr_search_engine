TOOLS OF BIBLICAL RESISTANCE 439

conveniently has been obscured~namely, should the state
have any say whatsoever over the terms of the church’s ex-
emption from taxation?

Here is what South Carolina Senator Ernest Hollings re-
cently wrote to a constituent: “But when religious belief is
contrary to the law of the land, then it is the law, not religion,
that must be sustained.”

Right now, Christians are only being asked to pay an un-
fair (higher) price monetarily for being Christians. We must
pay taxes so that unborn children can be aborted through gov-
ernment sponsored “services,” and we must pay taxes for
religious indoctrination in public schools even if we place our
own children in private, Christian schools.

The groundwork has been laid for more than just hitting
up Christians for a surcharge for the practice of their beliefs.
The foundation has been laid for an assault on Christian prac-
tice, and then we will have to choose to serve God or mam-
mon. The choice at that time will cost us more than money.
As Francis Schaeffer put it in A Christian Manifesto: “If there is
no final piace for civil disobedience, then the government has
been made autonomous, and as such, it will have been put in
the place of the Living God.”

The secularization of our education, politics, and law puts
Christians on a collision course with the state. Discussion of
civil disobedience has to be taken seriously if we are to take
our Christianity seriously. If God is the center of our lives,
then He must be the center of our national life as well. Liberty
comes from the Lord. “Where the Spirit of the Lord is, there is
liberty.”

For the moment there are ways short of civil disobedience
and revolution to turn this bad situation around. As God told
Israel in IT Chronicles 7:14, “If my people, which are called by
my name, shall humble themselves, and pray, and seek my
face, and turn from their wicked ways; then will I hear from
heaven and will forgive their sin, and will heal their land.”

When the Lord used Gideon to remove the oppression of
the Midianites from Israel, he had Gideon first seek the face of
the Lord. Israel was in subjection to the Midianites because it
had turned its face from God to Baal. Gideon was ordered to
destroy his father’s temple to Baal. In Schaeffer's words, there
was “no final place for disobedience” under the Midianites—
choosing God and destroying one’s own idol demanded the

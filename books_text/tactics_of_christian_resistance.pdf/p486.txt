440 CHRISTIANITY AND CIVILIZATION

death penalty. When Gideon’s father Joash refused to turn
Gideon over to the mob, the war was started.

To make sure that Israel would not think they had saved
themselves, God reduced the troops available to Gideon from
32,000 to 300. Then, with 300 men, God used Gideon to an-
nihilate the Midianites.

Tool No. 2: A Well-Armed Local Militia

The American Revolution was born. when. the Colonists,
in Schaeffer’s words again, felt that “there was no final place
for civil disobedience,” and that “the government had been
made autonomous, and as such, it had been put in the place
of the Living God.”

The English Parliament had stated in the Declaratory
Acts that the Parliament had the power to regulate every
aspect of every Englishman’s existence wherever he lived.
Right there, they had declared that they were severing
English law from what theretofore had been its basis in
Christ.

When the Colonists declared that their American govern-
ment was independent from a British Parliament which
claimed to be autonomous from the Living God, there was a
price to pay. In an effort to collapse the wherewithal of col-
onial independence, British authorities—in Massachusetts
and in Virginia~endeavored to disarm the Americans by
confiscating their arms and powder. That provoked the war
and triggered the shot heard around the world.

The right.of free persons to keep and bear arms traces
back to Anglo-Saxon jurisprudence long before the discovery
of America. Under the laws of Alfred the Great, who reigned
in the ninth century, all English citizens (nobles and peasants
alike) were obliged to purchase weapons and to be available
for military duty. These laws were in force approximately
1,100 years before enactment of the Kennesaw, Georgia, or-
dinance requiring able-bodied heads of households to own
firearms and ammunition.

According to our best information today, the term “militia”
was first used during the Spanish Armada crisis of 1588 to
refer to the entirety of the armed citizenry.

Virginia, in 1623, forbade people to travel unless they
were well armed, and in 1631, the Colony required colonists to

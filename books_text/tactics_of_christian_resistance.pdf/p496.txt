450 CHRISTIANITY AND CIVILIZATION

Governor Ed Meechem of New Mexico, but he was not ac-
ceptable to Nixon. So, they came up with Walter Hickel,
Governor of Alaska. He was a Westerner and pro-
development, and therefore, came close to satisfying the pro-
development Western Senators on the Interior Committee.
So, Hickel was nominated. Then his hearings came up, and
every left-wing environmental protection group raised a cry of
outrage against him. Now it’s very interesting that they did
that. They understood him better than we did. Senator Allott
and Senator Clif Hansen of Wyoming and Senator Fannin of
Arizona and some other Senators from the West went all out
to defend Hickel. But throughout those days and days of
testimony, which got a lot of headlines, Hickel was sitting
through all this testimony listening, and something in-
teresting was happening. He was confirmed, the vote was
overwhelmingly in his favor. There were only 16 votes against
his confirmation. If you look at that vote, you would say that
was an overwhelming defeat for the people who waged the
battle against Wally Hickel, because they only got 16 votes.
The fact of the matter is, they won the war. Because while
they cranked up all this campaign, the whole situation
affected him to the point that when he got in, he spent the rest
of his term accommodating thase peaple who had criticized
him. He became somewhat anti-development in the policies
he introduced. The very people who had defended him were
disappointed. By the time that he left office, the people who
had defended him in his confirmation hearings were not sorry
to see him leave. ‘Those who attacked him in his confirmation
hearings were praising him by the time he left. Who is to say
that the liberals’ fight, although getting only 16 votes, was the
wrong fight from their point of view? After all, they ultimately
got what they wanted out of the Secretary of Interior.

PY give you another example: When the news came that
Sandra Day O’Connor had been nominated to the Supreme
Court, I had a meeting in my office of 50 different pro-family
and conservative organizations. Since I have done some head-
counting in the Senate on numerous legislative battles, I was
asked in this meeting my assessment of the situation. If we
went all out in the conservative movement, and we did every-
thing we could, what would the results be in the Sandra
O'Connor nomination fight? I said that, barring some secret
revelation that would prejudice her case, in my opinion, if we

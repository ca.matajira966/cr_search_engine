RELIGIOUS LIBERTY VERSUS
RELIGIOUS TOLERATION

Rousas John Rushdoony

NE of the areas of profound ignorance today is religious

liberty and the meaning thereof. The common pattern
throughout history, including in the Roman Empire, has been
religious toleration, a very different thing.

In religious toleration, the state is paramount, and, in
every sphere, its powers are totalitarian. The state is the sov-
ereign or lord, the supreme religious entity and power. The
state decrees what and who can exist, and it establishes the
terms of existence. The state reserves the power to license and
tolerate one or more religions upon its own conditions and
subject to state controls, regulation, and supervision.

The Roman Empire believed in religious toleration. It re-
garded religion as good for public morale and morals, and it
therefore had a system of licensure and regulation. New
relgions were ordered to appear before a magistrate, affirm
the lordship or sovereignty of Caesar, and walk away with a
license to post in their mecting-place.

The early church refused licensure, because it meant the
lordship of Caesar over Christ and His church. The early
church refused toleration, because it denied the right of the
state to say whether or not Christ’s church could exist, or to
set the conditions of its existence. The early church rejected
religious toleration for religious liberty.

Over the centuries, both Catholics and then Protestants
often fought for religious liberty. Over the centuries also, the
churches too often capitulated to religious toleration, with
very evil results. Toleration was productive of fearful evils.
First, one church was tolerated and established by the state, not
by Christ, as the “privileged” or state-tolerated institution.

Reprinted from Chalcedon Position Paper Na. 31. Copyright by Chalcedon,
P.O. Box 158, Vallecito, GA 95251.

32

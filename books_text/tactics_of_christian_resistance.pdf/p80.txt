34 CHRISTIANITY AND CIVILIZATION

church for all the crown realms, including Catholic Ireland.
(reland was never more Catholic than after England imposed
an alien church on the land!) Carl Bridenbaugh, in Mitre and
Sceptre (1962), showed how the fear and threat of a full-scale
establishment with American bishops alarmed Americans and
led to the War of Independence. Meanwhile, in the colonies,
men began to oppose religious toleration in favor of religious
liberty. Here, the Baptists were most important, especially
Isaac Backus.

Backus declared, “We view it to be our incumbent duty to
render unto Caesar the things that are his but also that 2 is of
as much importance not to render unto him anything that belongs only to
God, who is to be obeyed rather than any man. And as it is evident
that God always claimed it as his sole prerogative to deter-
mine by his own laws what his worship shall be, who shall
minister in it, and how they shall be supported, so it is evident
that this prerogative has been, and still is, encroached upon in
our land.” (Wm. J. McLoughlin, editor: Isaac Backus on
Church, State, and Calvinism, Pamphlets, 1754-1789, p. 317. Har-
vard University Press, 1965.) The defenders of establishment
or toleration became, Backus said, “Caesar’s friend” (John
19:12), We cannot make the state the definer of man’s duty to
God, as the establishment-toleration position does. This posi-
tion, Backus held, takes matters of faith from the conscience
of man to the councils of state and thus undermines true faith.
Backus saw that the new country would have no unity if
establishment and toleration became lawful in the Federal
Union. Backus quoted Cotton Mather, who said, “Violences
may bring the erroneous to be hypocrites, but they will never
bring them to be believers.” The heart of Backus’ position was
this: “Religion (meaning Biblical religion) was prior to all
states and kingdoms in the world and therefore could not in its
nature be subject to human laws” (p. 432).

The First Amendment to the U.S. Constitution, replacing
religious toleration and establishment with religious liberty,
was the result of the work of Backus and many other church-
men. It represented a great and key victory in church history.

Now, however, religious liberty is dead in the United
States. It only exists if you confine it to the area between your
two ears. Instead of religious liberty, we have religious tolera-
tion. Now, religious toleration is the reality of the situation in
Red China and Red Russia. In both cases, the toleration is

REBELLION, TYRANNY, AND DOMINION 43

not, for the woman saw that the tree was good for food (cor-
rectly, 2:9) and a delight to the eyes (correctly, 2:9), and that
it was desirable to make her wise (wrongly). Also, how about
being like God? Wasn’t man made in the image and likeness
of God? How, then, is it a temptation to become like God, if
man is already like God? And again, how about knowing
good and evil? Were.Adam and Eve in a state of moral neu-
trality at this point? Obviously not, for they were in covenant
with God. They were morally good, and they had a knowl-
edge of moral goodness. They knew right from wrong, and
especially Adam, as covenant head, was not deceived about
what was going on (1 Tim, 2:14).

The matter becomes even more curious when we notice
the sequel. We read that their eyes were indeed opened (3:7).
We hear God soberly state, “Behold, the man has become like
one of Us, knowing good and evil... .” Was the tempter. right?
Clearly in some sense, the dragon was telling the truth,
though he lied in saying that they would not die.

All of these questions are answered when we realize that
the opening of the eyes, the maturation in God-likeness, and
the knowledge of good and evil, all have to do with investiture
with the robe of judicial office. Concerning the eyes: We have
already seen in Genesis 1 that God’s seeing is part of His pass-
ing judgment. We find in Jeremiah 32:18-19 that God's “eyes
are open upon all the ways of the sons of men, to give every
one according to his ways, and according to the fruit of his do-
ings.” In Psalm 11:4, the eyes of YHWH “behold, His eyelids
try, the children of men.” False gods are witnesses, says Isaiah
44:9, which “see not, nor know,” and which are “put to
shame,” all language reminiscent of Genesis 3. Meredith M.
Kline summarizes, saying that “the picture is of the eyes of
God functioning in the legal sphere to give a conclusive judg-
ment concerning lives of men which have been observed by
God.”8 ‘Thus, God’s eyes cither spare or do not spare men His
judgments (Ezek. 5:11; 7:4; 20:17).

Concerning becoming more'like God, we notice in the text
itself the statement that man is already like God (morally),
and from the text itself we could draw the inference that the
temporary prohibition on the Tree of Judgment was designed

8. Meredith M. Kline, “The Holy Spirit as Covenant Witness” (Th.M.
Thesis, Westminster Theological Seminary, 1972}, p. 72. I am indebted to
Kline’s discussion for the verses cited in this section of my essay.

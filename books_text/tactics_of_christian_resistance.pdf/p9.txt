EDITOR'S INTRODUCTION

Gary North

Ye are the light of the world. A city that is set on an hill cannot be
hid. Neither do men light a candle, and put it under a bushel, but ona
candlestick; and it giveth light unto all that are in the house. Let your
light so shine before men, that they may séc your good works, and
glorify your Father which is in heaven (Matt. 5:14).

A NY discussion of specifically Christian forms of resistance
cannot avoid a careful consideration of the implications
of this passage in Matthew. In the history of man, there have
been a seemingly endless number of conspiratorial resistance
movements. Most of them failed. Few of them left any
records. A few have been successful, the Nazis and the Com-
munists being the prirnary examples in this century. The
ideology of the socialist revolution was a product of two
primary influences in the nineteenth century: occult secret
societies (many of them Masonic) and journalism.? But Chris-
tian resistance is, of necessity, open to the public at large
through public conversion to the gospel. Christian resisters
are not to undergo a secret system of initiation, the hallmark
of occult conspiracies. Christian resisters are not to adopt any
and all resistance measures, irrespective of the morality of
these acts. The Bible, a public document, provides the foun-
dation of ethics, including the ethics of resistance.

This does not mean that everything involved in resistance
must be crystal clear and open to God’s enemies. Ehud did

1, See, for example, Charles William Heckethorn, ‘The Secret Societies uf
All Ages and Countries (2 vols.; New Hyde Park, New York: University
Books, [1897] 1965); Stephen Runciman, The Medieval Manichee: A Study of the
Christian Dualist Heresy (New York: Viking, 1961),

2, The most detailed account of the rise of revolutionary socialism is the
book by James Billington, Fire in the Minds of Men: Origins of the Revolutionary
Faith (New York: Basic Books, 1980).

ix

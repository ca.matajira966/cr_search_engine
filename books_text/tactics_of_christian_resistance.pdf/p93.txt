REBELLION, TYRANNY, AND DOMINION 47

often enough in the rest of Scripture, thinking only for the mo-
ment of the beasts in Daniel and in Revelation. But we may also
see in the clothing with animal skins another meaning as well,
which is that God intends to establish His covenant and to bring
man eventually to a place of true office, but now only on the
basis of a blood sacrifice. The clothing with animal skins was a
token to Adam and Eve that someday a man would be given the
robe of office, on the basis of the death of a substitute.
Noah, the second Adam, was that person.

Seizing the Robe from Human Authority:
The Example of Ham

Before the Flood, God did not give to His people the right
to exercise judicial office. Sinful men, having seized the robe,
did not hesitate to use it in terms of their own perverted stand-
ards. Thus Cain, unwilling to judge himself for his sins and
bring a blood sacrifice as his substitute, chose to execute
capital punishment against his innocent brother, who had
shamed him. The ‘adhamah, drinking Abel’s blood, cried out
for vengeance, but God appointed Himself a city of refuge for
Cain. Cain, however, did not want to hide in God, and built
his own city, ramming it into the ground which kept trying to
throw him off. In time, Gain’s descendents prided themselves
on the violence with which they abused the robe of office, as
seen in the culminatory hymn of Lamech, the seventh from
Adam in the Cainite line (Genesis 4).

How did the righteous fare during this time? Not well, if
Abel is an example. In time, the Godly Sethites succumbed to
the temptation to become part of the enrobed Cainite culture,
and intermarried with it: They were unwilling to persevere, to
wait. Tyranny abounded, and God decided to judge the
world. judicial evil had matured from youth to age, and it was
time to end it (Genesis 6).

After the Flood, on the basis of Noah’s sacrifice (Gen.
8:20), God renewed His covenant with man, and this time
enrobed His people with the office of judge. God had not put
Cain to death, though Abel’s blood cried out for it. Now, how-
ever, shed blood would be avenged, and the image of God,
man himself, would carry it out (Gen. 9:5-6).*2 This was

 

12. Some have argued that Cain was not put to death because it was not
the charge of the family to execute capital punishment, but of the state, and

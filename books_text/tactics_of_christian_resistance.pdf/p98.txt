52 CHRISTIANITY AND GIVILIZATION

become slaves, and if the Canaanites are the example, such
slaves will eventually be exterminated. Those on the other
hand who honor authority, and cover up the indiscretions (real
or supposed) committed by such authorities, will themselves in
time be honored with dominion and rule (Gen, 9:26-27).

How many young men there are in history and today who
will not wait until they are older to become elders in the
church! They go to college, where they acquire virtually no
wisdom, and from there to seminary, where they are isolated
from the wisdom-inducing problerns of church life. Then,
robed with a sheepskin, they get ordained to office at the ripe
age of 25! Is it any wonder that the churches are in sucha hor-
rible condition? One would like to think that there are older
men around who can lead, but sadly in our day and time
those who are older seldom have wisdom, for they have not
matured in terms of the law of God, Virtually all older Chris-
tians in this day and age have grown up believing that law
and grace are opposed one to another, and so have never ac-
quired mature wisdom based on years of study, obedience,
and governance by God's law. Frequently, then, office does
fall to those of younger years. Let them beware the perils,
however, and always be deferential toward those who are
older, if not wiser, in the faith.

Biblical teaching at this point strikes at the heart of perfec-
tionistic and pharisaical religion. If Saul is an evil king, then
Saul should be deposed; yet David, already anointed, being a
man after God’s own heart, refused to depose him. David did
deceive Saul, and avoided him, but he never rebelled against
him. This by itself does not solve all our hypothetical ques-
tions. Do we submit to an invader? Do we submit to a revolu-
tionary regime? Are our rulers anointed of God in the same
way as the kings of the Old Covenant? These questions have
their place, but they are not in view here. What is in view is
motive. The desire to seize power and to make oneself a ruler
(a god), without waiting for it to be bestowed, and without ac-
quiring years of wisdom first, is the essence of original sin.

Apvoiding the Tyrant’s Robe: The Patriarchs

The basic means ‘for dealing with power tyrants in
Genesis, and in the rest of Scripture, is though deception.??

20. On the ethics of lying, see Jim West, “Rahab’s Justiliable Lic,” in

Sanctions 81

giving you” (Deut. 4:1). This statement refers back to the promises of
seed and land given to Abraham (one of the “fathers”) (Gen. 12:1-3).
‘Their obedience was based on the promise. Yet, the promise is not
actualized without obedience. The false dichotomy between grace
and law just will not work. Grace and law are not in conflict. As one
man says, “There is no grace without law or law without grace.”¢

Every covenant with the exception of Christ’s on the cross was
unconditional. Even the first covenant with Adam was uncondi-
tional, Adam had done nothing to be created or to earn the garden,
The terms of not eating the tree of the knowledge of good and evil
were in the context of the unconditionality of creation, We can call
them the éerms of unconditionality. The only person who entered a purely
conditional covenant was Christ. But once He met its terms and
died on the Cross, it became unconditional to everyone who received
it. God the Father will never revoke His declaration that Christ per-
fectly met the terms of the covenant. The unconditionality of the
covenant is assured eternally. As Jong as one lives in Christ, he is
under an unconditional covenant.

This does not mean people in the covenant cannot apostatize.
We enter an unconditional covenant, but there are terms of uncondi-
tionality. James speaks this way when he says, “Show me your faith
without the works, and I will show you my faith by my works”
(James 2:18). If the terms of unconditionality arc not lived out, faith
and its works, then the covenant becomes a works-ortented system. It
then becomes conditional. Thus, the covenant-breaker turns the cove-
nant into a conditional system as it applies to him. He refuses to ac-
cept Christ’s perfect work in place of his own feeble efforts to placate
God’s wrath.

“Not Guilty” or “On Probation”?

Apostasy is real, People can fall away. Question: Fall away from
what? They fall fram the visible covenant, If they never come back and
repent, then they were never truly converted to begin with. From the
fuuman point of view, however, we can only live by the visible cove-

5. Thomas McComiskey, The Covenants of Promise (Grand Rapids: Baker, 1985),
pp. 59-93. Arthur W. Pink, The Divine Covenants (Grand Rapids: Baker, {975}, pp.
139-202.

6. Emest Kevan, The Grace of Law (Grand Rapids: Baker Book House, [1966]
1983), pp. 208-209.

 

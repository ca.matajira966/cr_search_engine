82 THAT YOU MAY PROSPER

nant. We cannot read hearts and say infallibly that a person really and
truly is a Christian. We do not have to view every believer in a skep-
tical manner either, If a person “says” he believes, is baptized, and
shows basic outward evidence of being a Christian, he should be
treated as a believer,

Should he fall away from the faith, his assurance is lost. The
easiest way to understand the apostate’s position is to compare it to a
judge’s granting of a suspended sentence with probation. He does
noi declare them “Not Guilty.” He gives them a suspended sentence
with probation to sec what they can do with their freedom. He
reserves the right to put them back into prison at any time,

Matthew 18 is a major New Testament passage dealing with
Church discipline (sanctions). Jesus gave a parable about a man
who owed a fortune to a lender. When the lender calls in the debt,
the man pleads that he cannot repay the debt yet. The lender gra-
ciously gives the man time to pay the debt. But the man then goes
out and hounds a poor man to pay him what is owed~- money that
cannot possibly go very far to repay the huge debt he owes. The orig-
inal debtor has him thrown into prison. He had not forgiven the
debt; he had only suspended the collection day. Because the debtor
refused to show mercy, the creditor decided that he was no longer
entitled to merey (Matt. 18:23-35).

But what of the terms of the covenant? Disobedience requires
death, from Adam to the present. On what basis does the heavenly
Judge extend time to the sinner? On what legal basis can a suspended
sentence with probation be handed down? Only on the basis of
Christ’s death and resurrection. God does not have to extend anyone
time, but He does so, Even so, some apostatize like the original
debtor in the parable, Such a person dies as an unrepentant sinner,
In this account, the story applies in particular to Isracl. Their lives
built up more wrath for themselves (Rom. 12:20).

Thus, the doctrines of Sovereign Grace and predestination are
“covenantally qualified.” From the moment a person begins his walk
with God, he does so by faith. Then he is baptized and enters life as
a covenant-keeper. All of these things are covenantal qualifications
that he knows the Lord. No one has the ability to look into the decree
of God to see what was written before the foundation of the world.
Man’s assurance can only be in terms of the covenant. Jesus points
to the covenantal response of faith to make this clear. He says, “He
who believes on Me has eternal life” (John 6:47), So anyone who

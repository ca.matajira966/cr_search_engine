Sanctions 83

has believed in Christ should have this assurance. Why? Because
faith is a covenantal response, Faith informs a person that the Holy
Spirit has indeed worked in his heart.

What if this covenantal response of faith stops? What if a person
says he has faith in his heart, is baptized, and then leaves the faith? If
he never comes back to covenantal faithfulness, it indicates that he
was never truly one of God’s elect, and that he had been a tare in the
congregation. The covenant does not consist of perfect but persever-
ing people, Everyone sins, but the test of faith is whether one recog-
nizes, confesses, and moves on in the faith, It is possible, however, to
have members of the Church who are in the process of covenant-
breaking and falling away. The Church is not like the name of a cer-
tain church in my town that is called True Vine Church. Think of the
implications of this name. It means this church believes that only
“truc” believers are members. This sounds so “spiritual,” but it is im-
possible. I know for a fact that there are members who professed
faith, were baptized, and then fell away. And, I also know for a fact
that this is true of every church and every denomination that has
ever existed. The covenant is not inviolate.

To say God deals without dual sanctions, therefore, is to weaken
the covenant. The ratification of the covenant is nothing more than a
“fire insurance policy” against hell. A person lives any way he wants
without the consequences of being cursed. But, the sanctions do not
work this way. They consist of blessing and cursing; they are prom-
issory; they are judicial; and they are both part of one covenant.
Now that we have seen what they consist of, let us address how they
were actually received.

II. By Oath Consigned (Deut, 27:1-30:20)

The sanctions of blessing and cursing were received by an cath, a
self-maledictory (“to speak evil on oneself”) oath, The suzerain
entered the covenant by pledging and calling down evil on himself
from his deities, in the event that he failed to honor his word. The
vassal also entered the covenant by taking a self-maledictory oath.
Perhaps it could be argued that “secular” covenant-cutting involved
two oaths, It seems, however, that the vasyal actually received the
suzerain’s oath. What the suzerain called down on himself would hit
the vassal, should the latter violate his agreement. So, there was
essentially one oath by the suzerain consigned to the vassal.

Even if this were not the situation in secular oath-taking, the Bib-

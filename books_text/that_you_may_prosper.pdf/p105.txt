Sanctions 85

breaking would result in death, But the presence of sin meant that
the maledictory oath would hit the person in covenant with God. To
avoid this judgment he needed a sacrifice, or substitute. In the exam-
ple at the beginning of this chapter, the animals served as objects of
malediction, preventing Abraham’s being burnt with fire. Through
them, Abraham ratified and said “amen” to the covenant.

‘The sacrificial system was al the heart of covenant ratification. It
expressed the need of atonement. It pointed to an oath that was
made in terms of an object that took the place of man. It was directly
linked to the covenant itself. As a matter of fact, there were five sacri-
fices, matching the five points of the covenant.

First, there was the reparation offering.® The introductory formula
to this offering parallels the transcendence point of the covenant,
“When a person acts unfaithfully against the Lord, and deceives his
companion... . He shall make restitution [reparation] in full, and
add to it one-fifth more. He shall give it to the one to whom it
belongs on the day he presents his guilt [sin] offering” (Lev. 6:2).
So the transcendence of the Lord was acknowledged first by paying a
form of restitution as an offering. This also was a show of good faith
that the guilty party believed God was immanent.

Second, the six (purification) offering was a way of confessing ac-
tual sin. This offering provided a process of mediation. Hands were
laid on the head of the purification offering, thereby transferring the
guilt of the sinner to the animal (Lev. 4:33). This is the representa-
tional principle we discussed in the hicrarchieal point of covenantal-
ism. The animal represented the man.

Third, a whole-burnt offering was made. Stress was placed on a
“perfect male animal” (Lev. 1:3, 10). Why? This offering atoned for all
of the breaking of the law so that the individual could completely offer
himself up to Gad." It had specifically to do with the breaking of the com-
mandments.? Lhe ethical section of the covenant comes to mind.

 

9. Gordon Wenham, The Book of Leviticus: New International Commentary on the Old
Testament (Grand Rapids: Berdmans, 1979), pp. 196-112. This offering is also called a
“trespass” sacrifice. Wenham correctly prefers to cal) it a reparation offering.

10. Note by the way that the sacrilices are arranged theolegically in the first chap-
ters of Leviticus. Their itergical order, the actual order in which they would be
offered, is specified in Leviticus six and nine, and Numbers six in reference to the
Nazirite

11. Wenham points out that the other sacrifices “atone,” but that the whole-burnt
offering “atones in a more general sense” p. 57.

12, Bid.

90 THAT YOU MAY PROSPER

You stand today, all of you, before the Lord your God; your chiefs, your
tribes, your clders and your officers, even all the men of Israel, your ittle
ones, your wives, and the alien who is within your camps, from the one who
chops your waod to the one who draws your water, that you may enter into
the covenant with the Lord your Ged, and into His oath which the Lord
your God is making with you today (Deut. 29:10-12).

Notice how Moses first lists the covenantal “heads” of the nation.
Then he includes the children and even the alien, We know from
other passages in the Old Testament that the alien was not circum-
cised, but he was required to live in the general sphere of the cove-
nant (Ex. 29:33).7 Regarding children, however, how can the Bible
include them in the covenant? Parents represent their children. If they
are in the faith, then their children are claimed for the faith as well.

Is this just an Old Testament principle? Even in the New Testa-
ment, salvation by household continued to be a practice. Once when
Jesus’ disciples were keeping parents from bringing their children to
Christ, He rebuked them saying,

And they were bringing even their babies to Him so that He might
touch them, but when the disciples saw it, they began rebuking them. But
Jesus called for them, saying, “Permit the children to come to Me, and do
not hinder them, for the kingdom of God belongs to such as these. Truly I say
to you, whoever does not receive the kingdom of God like a child shall not
enter it at all” (Luke 18:15-17).

Mark makes it clear that Jesus was laying His hands on the chil-
dren to “bless them” (Mark 10:16). Remember, to bless someone is to
impose a sanction. So, Jesus was placing the sanction of the covenant
on these children and including them in the household of faith. The
only basis for doing so was that their parents were in the covenant.

Finally, when one comes to Acts, he discovers that households
are being saved by the same principle of representation. In one
chapter where households are mentioned, Luke speaks of their being
brought into the kingdom and being baptized without stopping to
explain why. He says “Lydia and her household were baptized” (Acts
16:15). Of the Philippian jailer, Luke says, “He [the jailer] took them
that very hour of the night and washed their wounds, and immedi-

17. The “stranger” is forbidden in the Exodus passage t eat sacrificial food
because it is “holy.” Circumcision entitled an Israelite to cat this food (Lev. 1-5), so
the stranger obviously was not circumcised.

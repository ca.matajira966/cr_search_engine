98 THAT YOU MAY PROSPER

converted until after his time in the fields (wilderness).

Joseph ruled as the king’s delegated sovereign agent. He gave
Joseph a wife: the daughter of the priest of On, the holy city. Her
name? Asenath, the daughter of Potiphera (Gen. 41:45), He there-
fore became a son of the priest, He went from the house of Potiphar
to the house of Potiphera. In effect, Joseph was brought under a new
covenant. Covenantally, Joseph no longer was under Jacob's author-
ity. Jacob believed him to be dead. Joseph also thought he was cove-
nantally dead. We should remember that he saw everything in terms
of the covenant, and particularly the covenant made to Abraham.
God had promised the first patriarch seed and land (Gen. 12:1-3).
Joseph had been cut off from both. He had been disinherited by his
brothers, and he had been sold into slavery. He was in Egypt, the
Biblical symbol of hell (the pit), where even Jesus as an infant was
carried to. Covenantally, Joseph appeared to be dead. He could not inherit
from Jacob. His inheritance would come from Egypt. It would be a
large inheritance, but it was nevertheless Egyptian. His only hope
was for some kind of resurrection. More to the point, Ais twe sons
needed resurrection. He could not give them covenantal life. He was
now a covenantal son of Egypt, not a covenantal son of Israel.

So alienated from the past had Joseph become that he named his
two sons with pro-Egyptian names:

 

And Joseph named the first-born Manasseh [making to forget—R.S.],
“For,” be said, “God has made me forget all my trouble and all my father’s
household.” And he named the second Ephraim [fruitfulness— R.S.], “For” he
said, “God has made me fruitful in the land of my affliction” (Gen. 41:51-52).

He named his sons as though he had forgotten two things. First,
he had forgotten the dream that promised that his brothers and
father would bow down to him. Second, he had forgotten God’s
prophecy to Abraham, that before the Israelites could take the land
of Canaan, they would be enslaved and oppressed —afflicted—for
four generations (Gen. 15:13-16). God was not finished with him cov-
enantally yet. He was therefore not finished with Joseph’s sons.
There had to be a transfer of the inheritance.

The Inheritance

Eventually, Jacob and his family came to Egypt. Joseph’s father
and his brothers were forced to bow down to him, even as he had
foretold when interpreting the dream that had gotten him sold into

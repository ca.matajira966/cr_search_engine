Continuity 103

He had fed the Pharoah and his people; he had delivered them,
too. He was therefore the true first-born son of Pharaoh, the cove-
nantally legitimate heir of the Pharach’s double portion. Pharach
understood this, He gave Joseph the double portion in Egypt, the
land of Goshen, the best of Egypt (Gen. 47:6). But the Egyptians did
what he had feared that his brothers and their descendents might do:
deny the legitimacy of his birthright. The Egyptians hated Egypt's
true first-born son, the family that had fed Egypt and saved it: the
Hebrews. The Book of Exodus speaks of another Pharaoh who arose
who did not remember Joseph —did not acknowledge the lawful in-
heritance that Joseph and his family had been given. The later Phar-
aoh stole the inheritance by placing the Hebrews in slavery, just as
Jacob’s sons had done to Joseph in Dothan. Egypt once again proved
itsclf to be a pit, a place of bondage, just as God had told Abraham it
would. There can be no salvation in Egypt.

What was God's response? He killed the first-born sons of Egypt
as His final plague, and the Egyptians let them go free. The Egyp-
tian kidnappers of the Hebrews died, just as the kidnapping brothers
of Joseph would have died of starvation, had not God through
Joseph showed them grace. We need to be fed by the true first-born
son, or else we die.

 

Legitimacy

I call what happens in this story the principle of continuity. What
is it? Continuity is the confirmation of the legitimate heir, thereby
transferring the inheritance to him. First, the inheritance is con-
firmed by the daying on of hands.5 The ratification process of circumci-
sion normally would have entitled Joseph’s sons to their inheritance.
But they were probably uncircumcised because they were the sons of
a covenantally dead man. Probably, they would have been circum-
cised in connection with the process of adoption, though the Bible

3. An inheritance of seed and land is passed from Israel (Jacob) to Ephraim. God
calls it a “blessing” to Abraham (Gen. 12:2). This fact connects it with the original
blessing promised to man in the garden, In the last chapter we called it “rest” (Gen
. But now we should expand our understanding of blessing. It is a person and
an environment. The person is the Seed-Man, the Messianic line and eventually
none other than Jesus Christ (Gal. 3:16). The environment is the Promised Land in
the Old ‘lestament, which becomes the whole world in the New Testament (cf,
Fxed. 20:12; Eph. 6:3; Note the shift from “land” to “earth”). When Christ saves the
warld, Paul describes this as the coming of true “rest” (Heb. 4:1-11). His argument is
that Christ’s First Advent brought this rest, so the Church is supposed to enter into it
by applying the Word of God to the world.

 

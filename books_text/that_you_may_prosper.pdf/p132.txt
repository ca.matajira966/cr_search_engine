112 THAT YOU MAY PROSPER

Notice that the writer makes the connection between the cutting
off of Christ at the crucifixion, and apostasy. I pointed out above that
Christ's death was actually a covenantal separation. So, the point
about covenantal death is extended to the God-to-man level.

‘The third level of covenants—man to man—can also be ter-
minated by covenantal death. At this level we find the three institu-
tions of society: Family, Church, and State. Although they arc cove-
nants among men, they all have a special relationship to God, yet
not indissoluble. Take marriage as an example. Jesus says, “What
God has brought together, let no man separate” (Mark 10:9). This is
an imperative (ethical requirement), not a declaration. He does not
say, “No man can separate.” There is quite a difference between a
“negative” command and the assertion that the bond cannot be
broken, The Apostle Paul argues that, indeed, covenantal as well as
physical death can break the marriage bond (Rom. 7:1-4).

The context of this death is covenantal. The verse immediately
preceding this section is Romans 6:23, “For the wages of sin is death,
but the gift of God is eternal life through Jesus Christ.” Furthermore,
the context directly following is a discussion about death to the law. So,
it is clear that covenantal death is in view regarding the marriage cove-
nant, implying that the marriage bond can be severed, “covenant-
ally” as well as physically, The point is that even at the tertiary level
of covenants, the same principle of termination appears. This is only
one example of how the man-to-man covenant can be destroyed
through covenantal death. But it proves that the “indissolubility doc-
trine” is unbiblical.

The fact that covenants can be dissolved is certain. Let us now
consider how covenantal discontinuity comes about. It is the oppo-
site of the confirmation process.

J. Covenant Denial

The covenant should be continually renewed, Renewal leads to
the laying on of hands, a confirmation of the truc heir. If the cove-
nant is denied, however, the blessing is lost. The heir becomes a bas-
tard son. One clear example of this is Esau. He was the first-born
son, entitled to be the heir of the double portion, the one through
whom the seed would come. But Esau was a man of war. The text
calls attention to the fact that he was a hunter (Gen. 27:3). His name
literally means “hairy” (Gen. 27:11), His name compares him to an
animal, a beast. He is also called “Edom,” meaning “red,” probably

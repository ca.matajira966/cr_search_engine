116 THAT YOU MAY PROSPER

Achan’s family was destroyed. The entire household of Haman was
destroyed for his sin against Mordecai and the people of God (Esther
9:1-19). Many other examples could be given. God imposes cave-
nantal discontinuity on households, and not just on individuals.

In view of the third point, it should be added that the innocent
sometimes die along with the wicked. If they are in the proximity of
a covenantal judgment, they will also be destroyed. When Elijah
brought famine on the land through his prayers, many of the right-
cous were affected by this judgment ( James 5:17). Often God makes
a way of escape, but certainly many Christians have died in history
as a result of covenantal disasters. Of course, the faithful immedi-
ately go to be with the Lord. But they may have to suffer with the
wicked. It is quite possible that many innocent will die from the
AIDS plague in our own day, not because of any immorality but
because they are in the sphere of an overall judgment on the land.

Just as blessing comes according to the covenant, so does judg-
ment. As the wicked benefit from living near the righteous, so the
people of God suffer from sin in the land. Continuity and discontin-
uity are covenantal transmisstons of the inheritance of God. Obe-
dience leads to continuity. Disobedience results in discontinuity.

3. Permisstveness

Discipleship, education, and missions are necessary for the
perpetuation of inheritance. The future is lost without them. Neglect
in these areas is devastating.

Lot's life is a case in point. He started out so well. He was given
the choice of his inheritance, something God rarely gives anyone.
He was confirmed by Abraham. He even experienced conquest in
the land (Gen. 14:1-16). But Lot ended up being disinherited. Why?
He chose Sodom as a place to live, an evil and corrupt environment
(Gen. 13:13). He selected a city where his children could not be dis-
cipled and educated properly in the Bible. He wanted to live in the
luxury of a corrupt society with a wicked educational system, instead
of wandering around in a bunch of tents with Abraham. Lot was a
man who only lived for the present.

The long-term price was great. Lot ended up living in a cave, a
symbol of death (Gen. 19:30). Marc importantly, he lost his children.

15. The Book of Proverbs begins with the concept of moral environment. Obviously
the author of wisdom wants covenantal parents to see thal next to the “fear of the
Lord,” moral environment is the most important issue (Prov. 1:8-19).

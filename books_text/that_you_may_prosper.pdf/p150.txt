130 THAT YOU MAY PROSPER

he warned Israel at the end of the hierarchical section not to
bow down to idots (Deut. 4:15-24). The structure of the Ten Com-
mandments confirms this pattern. The second commandment for-
bids idolatry.*

This Christological hierarchy has an important ramification for
the Church and the world. When Christ says, “All authority has been
given to Me,” He means now; He has it in heaven and on earth; there
is no delay! This authority is not make-believe. It is not some exclu-
sively spiritual power, for He calls on His disciples to go out and
bring the nations under this authority. Nations are real, historical
entities. They are not subdued by make-believe power in some
make-believe, non-historical world.

Christ’s authority is not limited to heaven. He has it on earth. He
said so, on earth. He said so before His Ascension. And His Ascension
accentuates His full heavenly and earthly authority even more so. It
served to activate this power because it was here that He sat down at
the right hand of God the Father to rude, As the familiar verse of the
most popular eschatological Psalm quoted in the New Testament
says, “The Lord said to My Lord, sit Thou at My right hand until I
make Thine enemies a footstool under Thy fect” (Ps, 110:1). Think of
the implications of this statement. We know that Jesus is the “My
Lord” of this statement. He sat down at the “Lord’s” right hand at the
Ascension (Col. 3:1), Thus, He cannot move from the right hand of
God the Father, not even to rapture the Church, untz/ all the enemies
are defeated. Christ has and does exercise full authority because of
His Resurrection. He has ascended on high where His triumphant
heavenly reign now establishes His earthly rule!

3. Ethics (Matt. 28:19a)

The new covenantal mandate continues next with a stipulation,
“Go and make disciples of the nations” (Matt. 28:19). Christ’s com-
mand logically grows out of the previous statements on authority. A
“disciple” is primarily one who is under Christ's authority. He goes
where Christ goes and does what Christ does. He did not say, “Have
an experience and join the rest of us who have had a mystical experi-
ence.” Nor did He say, “learn a catechism and bask with us in the
knowledge of this new catechism.” No, Christ defined discipleship in
terms of authority and obedience.

6. See Appendix 1.

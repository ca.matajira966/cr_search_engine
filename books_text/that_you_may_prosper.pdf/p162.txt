142 THAT YOU MAY PROSPER

reality, the Biblical authority structure is a major ingredient missing
from American homelife. A. C. R. Skynner speaks of the need to
have a set hierarchy, as opposed to an “equalitarian,” everyone-is-equal
approach.

The parts of the group, like the parts of the individual, are not all on
one level but require to be arranged in a certain hierarchical order if they are
to function effectively. No onc disputes this in the organization of the cen-
tral nervous system, where lower centers are under the dominance of higher.
. . . But groups and families also have an optimum type of organization
which must involve a form of dominance hierarchy and while there is again a
range of possibilities of varying effectiveness, it appears that breakdown of
the authority structure — whether through the loss of control of a nation by
its government, abdication of responsibility by a father, or destruction of
the cortex through birth injury — leads to uncoordinated release of tendencies which
can be damaging to the whole system, however valuable these may be within
proper bounds and in their proper place.

The second covenantal principle for the family is a set hierarchy
around the male (under Christ’s authority). What about a day when
there are so many “one-parent” families? The institutional Church is
the Biblical solution until the woman remarries. Women can find the
“true Groom” Jesus Christ to be an immense comfort. The male will
discover that the Bride of Christ, the Church, can offset his deficien-
cies. Nevertheless, the problems do not negate the Biblical reality of
Aterarchy.

Ethics (Gen, 2:23)

After God creates the woman, Adam names her (Gen. 2:23).
“Naming” was part of the cultural mandate and original command
given to man. Not only was Adam told to name everything, but
thereby to have dominion, So, this function is in compliance with an
ethical stipulation. Biblical dominion is ethical and not manipulative,
designed according to the Ten Commandments.

The law of the family is supposed to be the Ten Commandments
of God, with fathers teaching their sons how the commandments are
applied. Moses tells Israel’s fathers,

And these words [Covenant of Ten Commandments] which I am com-
manding you today, shall be on your heart; and you shall teach them dili-

3. A. GR. Skynner, Social Work Today (July 15, 1971), p. 5.

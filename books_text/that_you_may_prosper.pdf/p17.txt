Publisher's Preface xvii

Priestly Code
Transcendence: prohibition of false worship
Hierarchy: no graven images (representation)
Ethics: prohibition against misusing God’s name?
Oath: sabbath as day of judgment (baptism, Lord’s supper)*
Succession: honoring father and mother; long life as the prom-
ised inheritance®

Kingly Code
Transcendence: prohibition against murder (God's image)
Hierarchy: prohibition of adultery (covenant authority)
Ethics: prohibition against theft
Oath: no false witness
Succession; no coveting of another’s property, which constitutes
family inheritance

Next, the covenant’s five-point structure became the integrating
framework for six of the ten volumes in the Biblical Blueprints Series.®
Two of the authors submitted manuscripts that instinctively con-
formed to it, even before they had scen That You May Prosper; Gary
DeMar’s Ruler of the Nations and George Grant's Changing of the Guard.
As the genera) editor, I reversed two chapters in each book (with the
authors’ permission) to produce a pair of books, each with two paral-
Jel sections of five chapters corresponding to the five-point model.

To date, the most spectacular application of the five-point model
is David Chilton’s masterful commentary on the Book of Revelation,
The Days of Vengeance (1987). I had hired Chilton to write this book
three and a half years earlier. He had written Paradise Restored (1985)
in the meantime, but he was bogged down on Days of Vengeance.
Then, in a Wednesday evening prayer meeting in 1986, Sutton pre-
sented his five-point model. Chilton was in attendance, Sutton’s out-
line clicked in Chilton’s mind. He approached Sution immediately
after the mecting and began to ask questions about Sutton’s model

3. The key issue here is property rights, although I did not see this at the time.
God's name is declared “off limits,” just as the tee of the knowledge of good and evil
was. He places a “no trespassing” sign around His name.

4. In the Preface, I used “Judicialfevaluational (sanctions).

5. In the Preface, I used “Legitimacy/inheritance (continuity)."

6. Gary North (cd,), Biblical Blueprints Series (Ft. Worth, Texas: Dominion
Press, 1986-87).

166 THAT YOU MAY PROSPER

There are only two tables in the world, One leads to life and the
other to death. There is no neutrality. Na man can keep a chair at both
tables. Judas was a man who tried to have a chair at both tables. He
was disinherited and cast out. The modern Church needs to learn
from Judas. Its covenant binds it to Christ, Yet, it is possible to lose
the “lampstand.” It can only be lost by trying to eat at two tables at the
same time! When this happens, the legitimate become the illegitimate.

Conclusion

The Biblical Church covenant has all five of the parts of the Deu-
teronomic covenant. Only by applying them can real dominion be
taken inside the Church. It almost sounds preposterous to speak of
taking dominion in the Church. Most churches, however, are strug-
gling with their own internal battles. “Judgment begins at the house
of God” (I Pet. 4:17). Whatever happens in civilization-at-large is
only a reflection of what is going on in the Church. Dominion begins
at home. Dominion begins in the Church. Until it does, it will never
happen in the State!

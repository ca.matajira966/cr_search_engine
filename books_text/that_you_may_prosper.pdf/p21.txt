INTRODUCTION

A young Israelite king was sitting on his throne one day. He may
have been bored; he may have been worried. But he decided to
spend his day counting all the money that had come into the treas-
ury. In those times, the money was kept in the Temple, so he sent a
messenger to perform the task.

When the messenger arrived, he and a priest began to calculate
the king’s wealth. As they moved the great chests of gold back and
forth, the king’s messenger found a strange object. He found some-
thing resembling a “time capsule.”

The messenger called the priest. Slowly they unrolled the ancient
manuscript found inside. In it was a message from their ancestors.
Both knew immediately what had been found. ‘They wept, as their
eyes raced through the document, reading of a time gone by that ob-
ligated them to greatness.

The king had to be told.

The priest was selected to take the document to him. At first he
walked; then he ran. The guards outside the king’s palace were in-
structed to move out of the way, for he had a message from the past.

Running into the king’s court and interrupting the proceedings,
the priest bowed and held the document up to the king. Reaching
out, the teenager clasped the manuscript in his hand. He stood,
frozen like a statue for what seemed like time without an end. Every-
one in this great court covered with gold, silver, and precious jewels
waited specchlessly to see what the king was going to do. Then he
looked up. But his eyes did not mect the numbed faces encircling
him. They stretched up to the heavens.

Then he cried out before all the court, “God, forgive me and my
people,” He tore his clothes and fell on his face, pleading for God’s
mercy.

What had happened?

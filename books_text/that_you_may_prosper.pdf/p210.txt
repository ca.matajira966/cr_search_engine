190 ‘TI[AT YOU MAY PROSPER

doctor who performs surgery for such purposes could be put to death.

Therefore, the New Covenant application of penal sanctions is
both similar and dissimilar to the Old Testament. The historic shift
from wrath to grace in Jesus Christ allows for a much fuller and
powerful application, guaranteeing a truer continuity and discontin-
uity with the covenant.

Continuity

Paul argues that continuity in society should be on the basis of
God’s law (Rom. 13:3a). What about idolatry? Should the magistrate
put idolaters to death? How should this be handled? A variety of ex-
planations have been suggested. In the Massachusetts Bay Colony in
the 1630s, Roger Williams proposed that the magistrate should only
sanction the last six commandments, avoiding the ones having to do
with worship and sabbath keeping. The long-term effect has created
a multi-religious society: polytheism. Has this been good, and if it is
wrong, how could a Christian civilization be legislated?

To the question, “What have been the long-term effects of Roger
Williams’ position?” the answer is not encouraging for those serious
about the Christian faith, The modern Church fives in the age of a
religious cold war. A State that has not formally declared war on the
Church “unofficially” strives to put it to death!

42. Henry Martyn Dexter, As to Roger Williams, and his “Banishment” from the Massa-
chusetts Plantation; with a few further words concerning the Baptists, Quakers, and Religious
Liberty: A Monograph (Boston: Congregational Publishing Society, 1876). Roger
Williams believed the magistrate was only supposed to enforce the last six com-
mandments and not the first four, avoiding the crime of idolatry. For this, he has
been recognized in textbooks as the “Father of Religious Liberty.” In Roger Witliams
and the Massachusetts Magistrates, ed. Theodore Greene (Lexington, Massachusetts:
D. C. Heath and Co., 1964), pp. 1-21, however, fram many of the actual accounts
we see that the Puritans, John Cotton of note, believed that Williams's views would
eventually lead to religious and political chaos, since politics is always an expression
of the religion of the land. Furthermore, Romans 13 does not allow for Williams's
position, which allowed some capital offenses and nat others. On what basis can
such “arbitrary” decisions be made?

For an excellent summary and re-publication of select passages from Dexter, sce
Failure of the American Baptist Culture, ed. James Jordan (Tyler, Texus: Geneva Divinity
Schoo} Press, 1982), pp. 233-243. The reader will discover that Dexter “demytholo-
gizes the many fantasies surrounding the relationship between Williams and the
Puritans of Massachusetts Bay. His 146 pages demonstrate thoroughly, for instance,
that Williams was not a Baptist when he was in Massachusetts, and thus was not
disciplined for being a Baptist by the Puritans; that Williams's views concerning re-
ligious liberty were not at issue when he was dealt with by the Puritans; and that
Williams was never banished into the wintry wilderness, but chose to leave of his
own accord rather than accept comfortable accommodations back to England or
move to another colony” (p. 233).

 

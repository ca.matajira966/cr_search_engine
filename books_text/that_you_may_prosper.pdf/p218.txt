198 THAT YOU MAY PROSPER

high treason and had violated the law of the land and of God. Such
violations were to be punished by death, even the death of the king.
Such an action was a historical precedent.

What was the issue? Up to this time, it was believed that the
King had “Divine right” to the throne. This meant that he was above
the civil law. But with the influence of the Reformation, Western
Civilization adopted an ethical basis of rule. The king’s authority
was given by a covenant. Again we use our “totem pole” analogy to
explain. In the following diagram we see that this time it is not the
“father” or “church leader” placed on a continuum with Ged. It is in-
stead the “king.” If the king is “infused” or “injected” with deity, then
to oppose him is to oppose God.

CHAIN OF BEING VIEW OF STATE

GOD
MAGISTRATE
(POSSESES BEING OF STATE IS VIEWED AS
GOD IN HIS PERSON) CENTER OF SOCIETY
FAMILY OTHER INSTITUTIONS
HAVE LESS BEING. THEY
MUST GO THROUGH
CHURCH THE STATE TO
NON BEING GET TO GOD.
(QUTSIDE OF

MAGISTRATE'S DISTRICT)

The Biblical view, however, is that the King, his other “lesser”
rulers, and the people are all bound in an ethical covenant with God.
When the king breaks that covenant, the people have a basis to sue
the king, without at the same time suing God. According to the other
view, to oppose the king was to oppose God. The king could and did,
many times, do anything he wanted. But the king and God are dis-
tinct, the Puritans insisted. God is God, and the king is part of crea-
tion. When the ethical bond between them, or between the king and
the people is broken, there is a legal way to deal with the king. Ac-
cording to an ethical covenant, a// men are under law.

This was the position of the Mayflower Compact. Although these
people were in submission to the King of England, they made a cov-

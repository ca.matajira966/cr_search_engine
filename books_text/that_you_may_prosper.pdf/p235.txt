The Ten Commandments 215

Structure

How to structure the Ten Commandments has always been a
problem.! Luther’s Catechism, following Augustine, breaks some of
the commandments up and re-groups them. The Shorter Catechism of
the Westminster Confession of Faith (Presbyterian) splits the command-
ments between the fourth and fifth. This structuring has the most
appeal because the first four pertain to God, and the last six refer to
man in a general sort of way. But the first four commandments apply
to man as well as to God. To keep the Sabbath Day is a “manward”
activity. Christ said, “the Sabbath was made for man, and not man
for the Sabbath” (Mark 2:27).

How about the next six commandments?? Even though they
seem to be oriented to man, they are still primary obligations to
God. The prophets refer to adultery as a “spiritual” problem. Israel’s
adultery with other gods resulted in “domestic” unfaithfulness (Mal.
2). The 4:6 structure does not hold up well either.

Besides, the fifth commandment has always been the real prob-
lem. All the others are expressed in the negative, “Thou shall not.”
The fifth is stated in the positive “Honor your father and your
mother.” The question is: “Why?” It could be because it heads up the
beginning of the “manward” commandments. The commandment
heading up the “Godward” commandments is in the negative. Prab-
lem: the “manward” commandments also begin with a negative.

Why try to break the decalogue into two groups at all? God com-
municates with a double witness. It takes two witnesses to convict
someone (Nu. 35:30; Deut, 17:6). This “two-fold witness” concept is
everywhere. For example, there are two copies of the Ten Com-
mandments placed in the ark (Deut. 10:5), two cities {city of God
and city of man). Even the language of the Bible is written in paralle/-
ism (i.e. The Psalms and Proverbs). This is so much a part of God's
manner of communication that the interpreter should instinctively
look for it.

1. Notice that I have not referred to the divisions as “tables” of the law. Kline has
conclusively demonstrated that the “tables” were actually two copies of the same law.
See Structure of Biblical Authority, pp. 17-120

2. U. Gassuto, A Commentary On The Book Of Exodus (Jerusalem: ‘The Magnes
Press, 1951), p. 249. Cassuto basically gocs with a God/man division of the com-
mandments. He notes that the first and the tenth commandments have to do with
loving “God” and loving “man” (your neighbor) respectively.

224° THAT YOU MAY PROSPER

Conclusion

The Ten Commandments express God’s law in a covenantal
way. There are five parts to the covenant, and so God’s ten laws fol-
low the covenant twice, each re-enforcing the other.

Can we find this five-fold pattern anywhere else? The next ap-
pendix jumps to the Psalms. It, too, has a Deuteronomic structure as
we shall see.

226 THAT YOU MAY PROSPER

a good possiblity that Ezra was the one who organized the Psalms
according to this arrangement.

Most modern translations new include this division. Why five? I
believe this great collection of psalms follows the covenantal pattern of
Deuteronomy. Let us now consider the structure of the Psalms ac-
cording to the following outline.

The Govenantal Structure of the Psalms

Book I (1-41): True Transcendence.
Book I (42-72): Hierarchy.

Book HI (73-89); Ethics.

Book IV (90-106): Sanctions.

Book V (107-150): Continuity.

4. True Transcendence (Ps. 1-41)

The Psalms begin with a “sanction” word, “blessed,” placing the
entire book in the context of covenant renewal on the Sabbath Day
of worship. Remember, the fourth commandment pertains to the
Sabbath and corresponds to the “sanctions” section of the covenant,

Be this as it may, the Psalms open on one of the three transcen-
dent themes: creation. The setting of Psalm 1 is a “garden” and “trees
planted by water” (1:3). Sound familiar? The Psalmist speaks of a
new genesis, or beginning, “how to become a new creation in God's
new garden.” We are called back to the garden where there was the
first tree of life. The psalm wants us to see David’s beginning as
being that of a new Adam. All those who receive (meditate on) the
Law of God become “miniature” new Adams, Ultimately (eschato-
logically) this psalm is about Christ, who was the “Tree of Life,” and
one who “did not sit in the seat of scoffers.”

Another pronounced “transcendent theme” stands out. The first
psalm contrasts those who meditate on God’s truly transcendent
Word, and those who “sit in the counsel” of man’s revelation. The
Deuteronomic covenant began with the same contrast. God’s Word
was distinguished from Moses’ words. In this regard, Psalm 1 is vir-
tually identical to Deuteronomy 1:1-4.

3. R. K. Harrison, Jntroduction to the Old Testament (Grand Rapids: Eerdmans,
1969), pp. 986-87, Harrison notes that although the actual length of the Psalms has
been debated, the book division was probably made by Ezra and could be consid-
ered part of the text.

230 THAT YOU MAY PROSPER

theme sets the tone for the judicial aspect of the covenant. Psalm 89 is
the perfect transition.

4, Sanctions (Ps. 90-106)

Book IV of Psalms highlights the judicial theme. More than in
any other section of the Psalms, the sanctions are mentioned. Both
Psalms 103 and 104 begin, “Bless the Lord oh my soul.” Man blesses
God because God first blessed him. The remainder of those psalms
speak of God’s rich benediction on man.

Most of the other psalms of this section are imprecatory. An im-
precatory psalm invokes God’s curse on His enemies. Reasons are
given and then a request is made to vindicate and carry out immedi-
ate vengeance on those who are attacking God's people. Psalm 94
begins,

O Lord, God of vengeance; God of vengeance, shine forth! Rise up, O
Judge of the earth; Render recompense to the proud. How long shall the
wicked, O Lord, How long shall the wicked exult? They pour forth words,
they speak arrogantly; All who do wickedness vaunt themselves. They
crush Thy people, O Lord, and afflict Thy heritage. They slay the widow
and the stranger, and murder the orphans. And they have said, “The Lord
does not sce, nor docs the God of Jacob pay heed” (Ps. 94:1-7).

Book IV ends with Psalm 106. It is somewhat of a complete his-
tory of Israel. In a way, all of the coyenantal themes come together,
but the psalm ends on the note of Israel’s being disinherited from the
land. Another receives the final blessing of the Lord (Ps. 106:40-48).
This carries out the judicial sanctions and pushes into the final part
of the covenant.

5. Continuity (Ps. 107-150)

The final book of the Psalms ends on the inheritance theme, In
this section, we find the most quoted psalm in the New Testament,
“The Lorp said to my Lord, ‘Sit at My right hand until Thy enemies
are made a footstool under Thy feet’” (Ps. 110:1). What does this have
to do with inheritance? The “Lorn” is God the Father, and “my Lord”
is Jesus Christ, God the Son. Jesus Christ goes to the right hand of
God the Father at the Ascension (Acts 1:6-11). He is not to come back
“until” all the enemies of God are defeated. His estate passes into the
trusteeship of the saints. This matches what we have previously ob-
served, At the end of all the other covenants, they speak of a new in-

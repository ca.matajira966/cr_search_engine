Romans 251

tions. Then he speaks of his personal program for conquest in the
spread of the Gospel by outlining his plans to go to Spain, Jeru-
salem, and finally to Rome (15:22-29), He closes with another
“amen” (15:33).

4. Sanctions (Rom. 16:1-2)

The judicial section is short and expressed in the form of a com-
mendation, or special “blessing” (16:1-2), Paul “commends” Phoebe, a
diaconess, who is entrusted with the responsibility of bringing the
Word to them, Paul's letter to the Romans, She does not hold the
office of deacon: rather, she holds a special appointment. This was one
of the unique functions a woman could have in the early church.
Paul's comments fall in the “sanction” category because Phoebe was
sent with special blessing to bring the special blessing, the Epistle to
the Romans.

5. Continuity (Rom. 16.3-27)

True to the form of so many of the epistles, Paul concludes his
letter with a long list of names. Why? Continuity is maintained.
Remember that the continuity section establishes the true heirs, In
this case, Paul “greets” several people. He expresses his approval:
“Greet Prisca and Aquila, my fellow workers in Christ Jesus, wha tor
my life risked their necks, to whom not only do I give thanks, but
also the churches of the Gentiles” (16:3); “Greet Epaenetus, my beloved,
who is the first convert to Christ from Asia” (v. 5); “Greet Mary, who
has worked hard for you” (v. 6); “Greet Andronicus and Junius, my
kinsmen, and my fellow prisoners, who are outstanding among the
apostles, who also were in Christ before me” (v. 7); “Greet Apelles,
the approved in Christ” (v. 10); “Greet Rufus, a choice man in the
Lord? (v. 13).

There are many more listed here, but these are special greeting’
citations. These are the people in the local church at Rome who
would most definitely carry it forward. But Paul mentions some who
are not so noteworthy. He says, “Now I urge you, brethren, keep
your eye on those who cause dissensions and hindrances contrary to
the teaching which you learned. . . . And the God of peace will soon
crush Satan under your feet” (16;17-20). Why doesn’t Paul list their
names? He does not want to give them permanent place among the
honor roll listed above. He is singling them out, however, to point
out the “bastards” of the community who not only break down contin-

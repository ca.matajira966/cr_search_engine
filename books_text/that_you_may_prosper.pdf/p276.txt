256 THAT YOU MAY PROSPER

one that had just been renewed and signified by the very name of
Deuteronomy, “second” law,

Many other parallels could be drawn, but Revelation definitely
begins according to the structure and influence of Deuteronomy,
Moses is God's “servant” delivering the transcendent words of the
covenant, and John is the new messenger in the transcendent pres-
ence of the Lord, revealing a new, better, “renewed” message about
the end of the ancient religion.

2. Hterarchy (Rev, 2:1-22)

The second section of the Deutcronomic covenant develops the
hierarchy of lordship which is confirmed in history. Revelation con-
tains all these elements in its second section, the seven letters to the
churches.

First, they have to do with a hierarchy. The letters are addressed
to an “angel,” properly translated “messenger” (angelos), and symboli-
cally portrayed as a “star” (Rev. 1:20; 2:2). Who are these “stars,” or
“messengers” of the churches? The fact that they “receive” the letters
indicates that they are “elders,” not angels. William Hendriksen
says,

The “angels” cannot indicate the messengers of the churches sent to visit
John, as the Scofield Bible holds. Then the expression: “To the angel of the
church at... write” (Rev. 2:1 for example) would have no meaning.
Again, real angels, heavenly beings, cannot be meant, It would have been
rather difficult to deliver the book or its epistles to them! . . . For an excel-
lent defense of the view that these angels refer to the bishops or pastors or
ministers of the churches, see R. C, Trench, Commentary on the Epistle to the
Seven Churches in Asia, pp. 53-58.°

Since the recipients were “elders,” a hierarchy is implied, ‘The
“elders” were to convey the message to their churches and implement
discipline. Furthermore, the “messengers” (elders) of this section in
Revelation are the same “messengers” of Revelation 8-11 who pour
out judgment on the earth. In other words, the representatives in
God's hierarchy become a means to issuc some kind of imprecation
against the enemies of the New Covenant people. Perhaps Im-

6. William Hendriksen, More Than Conguerors (Grand Rapids: Baker, [1939]
1977), p. 73, note 16, See also, Moses Stuart, Commentary on the Apocalypse (Andover:
Allen, Morrill, Wardwell, 1845), Vol. II, pp. 55-56.

 

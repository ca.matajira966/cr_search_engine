Revelation 267

still supposed to sacrifice animals? Should we go to church an Satur-
day instead of Sunday?

In the next appendix, we will examine these questions and many
more, seeking to understand the relationship between Old and New
Covenants.

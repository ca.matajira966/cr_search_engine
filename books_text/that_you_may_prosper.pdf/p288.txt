Appendix 6

OLD COVENANT/
NEW COVENANT COMPARISON

We have overviewed the covenant. The whole Bible contains its
structure and content. The fact that the New Covenant is called a
“covenant” indicates continuity. But, does this mcan there are no
differences between the Old and New? No. Hebrews 8 is a chapter
that helps us to summarize the similarities and dissimilarities. But
before we engage in this comparison, I should explain the designa-
tion: Old and New Covenants.

The Two Covenants

There are two and only two covenants in the Bible, and they are
called Old and New. Immediately, two issues are raised. How do we
determine the number of covenants? And, what distinguishes these
covenants from each other?

First, as to the number of covenants, the Bible only speaks of
two, Some students of the covenant have tried to specify more
because covenants are made with specific individuals such as Noah,
Abraham, and David. But these are merely the re-establishment of the
first covenant made with Adam, the Old Covenant. For example,
God says to Noah according to the New American Standard Ver-
sion, “I will establish My covenant with you” (Gen. 6:18). But the
Hebrew (*heqim” not “karath") should be properly translated, con-
firm.’ So God “confirmed” an already existing covenant. This mcans
thal covenants exist in the Bible where the literal word “covenant” is
not used.

More importantly, the original covenant made with Adam is re-

1. Warren Gage, The Gospel of Genesis (Winona Lake, Indiana: Carpenter Books,
1984), p. 30.

268

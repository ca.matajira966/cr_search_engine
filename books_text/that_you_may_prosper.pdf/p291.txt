Old Cavenant/New Covenant Comparison 274

1. True Transcendence (1-5)

Hebrews 8 begins with the transcendence theme. The High
Priest of the New Covenant sits in the heavens in a heavenly temple
“which the Lord pitched, nat man” (Heb, 8:1), The contrast between
the Old and New Covenant is the contrast between an earthly tem-
ple, priesthood, and sacrifice, and a heavenly temple, priesthood, and
sacrifice. The first is a copy, and the second is the original (Heb.
8:5). Heaven and earth arc distinguished from one another, just as
we have seen that God and the creation were distinguished in the
Deuteromic covenant. The “heavenly” character of the original gives
it transcendence,

In the first covenant, the garden was a copy of the heaven around
the throne of God, called a “glory cloud” (Ezek. 1:4-28). Earth was a
copy of heaven. How do we know? The tabernacle and temple were
simultaneously pictures of earth and heaven (Exod. 25-27).2 Inside
the tabernacle, a blue ceiling represented the blue sky, Trees that
symbolized people lined the walls (Ps. 1:3). In the center of the Holy
of Holies stood the ark, the throne of God. So the tabernacle was a
picture of what the world was supposed to be, ordered space around the
throne of God. The garden was supposed to have been the same. Orig-
inally, it was. Everything was arranged around the Tree of Life,
God’s throne.

But even though the garden was the throne of God, it was still
only a copy. Its transcendence was reflective, not original. The gar-
den was heavenly, not heaven. The difference is quite significant.
Heaven/y means influenced and characterized by heaven. Heaven is
the source. The serpent tempted Adam and Eve with original tran-
scendence, deification (Gen. 3:5). He wanted to make earth into
heaven, a utopia. Adam and Eve bought his proposition with all that
God had given them. They set out to make earth into heaven instead
of carrying out the cultural mandate, which would have made earth
like heaven, a reflection, not the original. They wanted earth to be
the original. Consequently, they fell into sin and death and with
their every effort marred what imaged God. The heavenly garden
became earthiy.

God restored them and pravided models of a heavenfy world. As
I have already pointed out, the tabernacle and the temple were

2. Meredith Kline, Images of the Spirit (Grand Rapids: Baker, 1980), pp. 13-26.

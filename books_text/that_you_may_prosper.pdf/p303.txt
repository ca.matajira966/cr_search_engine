Meredith G. Kline: Yes and No 283

tried to show in my book, but in Kline's book of 149 pages, there
ought to have been more, if the author intended to do anything with
the insight.

Hierarchy

He does not relate the hierarchical structure of the Mosaic court
system to the rebellion of Israel. Consequently, he misses the frame
of reference for this second point. He does not call it hierarchy. He
includes it as part of the historical prologue. He does not see why
God begins the whole section with the hierarchical passage (Deut.
1:12-18), What is crucial is the court system of Israel, He does not ask
or answer the question that I regard as crucial: Why does this historical
prologue begin with a presentation of Israel's court system?

Again, my criticism is that he only briefly mentions part two of
the covenant structure, and then he does nothing with it. He does
not ask the obvious question concerning the law courts. Thus, his
book makes no application of his insight. All he says is, “This reason
for righteous administration of justice is at the same time a reminder
of the theocratic nature of the Israelite kingdom, a reminder that God
was the lord who was making covenant anew with them this day.”®

Why does he fail to develop this theme? One obvious reason is
that he believes that this Israelite theocratic kingdom and its laws
represented an intrusion into the plan of God for the ages—some-
thing not carried over into the New Testament. He does not want
New Testament judicial reminders “that Ged is the lord who is mak-
ing covenant anew with us this day.” I do want such reminders.

Ethics

Kline does not discuss the whole principle invalved in Biblical
ethics, namely, that there is an ethical cause-effect relationship. Kline does
not believe that such a relationship is visible in history. Whatever
cause-and-effect relationship there is, is known only to God. To
mankind, such relationships supposedly are inscrutable. I think it is
appropriate here to cite Gary North’s observations concerning
Kline’s view of ethical cause and effect:

If you preach chat biblical law produces “positive feedback,” both per-
sonally and culturally—that God rewards covenant-keepers and punishes

6. [bid., p. 53.

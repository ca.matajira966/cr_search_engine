Meredith G. Kline: Yes and No 285

of the church, royal theocratic authority with its prerogative of imposing
physical-cuitural sanctions resides solely in Ghrist, the heavenly King. ‘The
judicial authority af the permanent spccial officers whom Christ has ap-

pointed to serve his church on earth is purely spiritual-cultic.®

 

Here is where Kline’s implicit antinomianism becomes explicit.
What about Romans 13? What about the ministers of justice ap-
pointed by Christ to protect His Church, as well as to protect all men
in their “non-cultic” activities? Paul writes of God’s appointed au-
thorities, which includes (though is not limited to) civil magistrates:

 

   

Let every person be in subjection to the governing authorities. For there
is no authority except from God, and those which exist are established by
God. Therefore he who resists authority has opposed the ordinance of God;
and they who have opposed will receive condemnation unto themselves

{Rom. 13:1-2).

When Paul reminds us that rebels will receive condemnation, he
is reminding us, using Kline’s words, that the kingdom’s “earthly
and cultural form was symbolic of the ultimate consummation. ‘The
judicial infliction of cultural sanctions by its officers typified the final
messianic judgment of men in the totality of their being as cultural
creatures.” The State legitimately inflicts sanctions, acting as God’s appointed
agent. These sanctions point directly to God as the final Judge. This
did not end with the Old Testament. This is why Paul calls the civil
authority a minister of God. “For he [the authority] is a minister of
God to you for good. But if you do what is evil, be afraid; for he docs
not bear the sword for nothing; for he is a minister of God, an avenger
who brings wrath upon the one who practices evil” (Rom. 13:4).

The problem is this: What is the nature of the sanctions in the
New Testament age? Kline cannot show why or how something in
the New ‘lestament restricts to the Mosaic economy the dual sanc-
tions of Deuteronomy 27-28. My question is simple: What principles
govern God’s historical judgments in this New Testament cra?
Silence in this case is not golden. Kline does not see any New Te

 

 

ment civil applications. Specifically, he daes not see the civil sanc-
tions as being tied to a system of covenantal adoption. (He does
make one important covenantal application of his insights regarding
the dual sanctions of the covenant: the covenantal basis of New Tes-

9. By Oath Gonsigned, pp. 100-4.

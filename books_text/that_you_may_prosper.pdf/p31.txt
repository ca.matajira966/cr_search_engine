Introduction u

removing the abortion question from the Federal courts and the
Supreme Court, the issue would be returned to the states, where it
began, and where all other capital penalties are enforced. (Abortion
is murder; murder is a capital crime; therefore. . . .) So where were
our elected representatives? Evidently, we have an authority crisis in
our land!

Third, America was originally built on a clear sense of right and
wrong. There was fixity of law, or ethics. Not so any longer. White-
head explains this Joss of “fixity of law and absolutes above men” in
the following comment.

Justice Hughes’s statement [quoted above] was representative of a clear
break with the American legal past. His view of law deviates from the
American concept of constitutionalism— limited government under the rule
of law,

This concept was laid down in the colonial documents, including both
the Declaration of Independence and the United States Constitution. But
in its adoption of the English common law [which applied accepted biblical
principles in judicial decisions], the Constitution was acknowledging that a
system of absolutes exists upon which government and law can be founded. More-
over, under constitutionalism the people are governed by a written docu-
ment embodying these principles and, therefore, not by the arbitrary opinions
to be found in the expressions of men.2

Fourth, the concept of sath has been almost completely destroyed
by attacks on the use of the Bible to administer it. If men do not
swear by God, there is nothing transcendent to enforce the oath.
There are no real sanctions. So we are left asking, “Where is social
justice in our land?” Crime is up. On the one hand, the State is slow
to respond to the fears of the average American. Crime is high on
everyone's list of American crises. It seems that one has to commit a
multiple killing before the ancient law, “an eye for an eye,” is hon-
ored, On the other hand, some Americans are prepared to take mat-
ters into their own hands. More and more “vigilante” movies appear
each year, Handgun sales are higher than ever. Self-defense courses
are full. All sorts of protection devices are being installed. As one

 

cision of the Congress had removed its jurisdiction. ‘The Court affirmed that Con-
gress interprets the appellate jurisdiction of the Supreme Court. The case was Ex
Parte McCardle vs. Mississippi (1869), See The Gonstitution of the Uniied States: Analysis
and Interpretation, Congressional Research Service, Library of Congress (Washing-
ton, D.C.; Government Printing Office, 1972), pp. 750-752

28. Whitehead, Second American Revolution, pp. 20-21. Emphasis and brackets mine.

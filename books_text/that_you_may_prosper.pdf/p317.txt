Covenant in the Flesh 297

The false passover meal at the foot of Mt. Sinai during Moses’
absence had destroyed the families of the participants. As the death
penalty was about to be carried out on the apostates, Moses said,
“Dedicate yourselves today to the Lord—for every man has been
against his son and against his brother—in order that He may bestow a
blessing upon you today” (Ex. 32:29). The “blessing” was the transfer
of inheritance. So, as Israel had lost its family continuity through
an idolatrous meal, it would receive it back through faithful com-
munion.

The covenant was legitimated in terms of a meal. As we have seen in
previous covenants, at the end of the covenant, the death of the cove-
nantal head seals the disinheritance of the old heirs and the inheritance of the
new heirs. When Israel left Egypt, the “bones of Joseph” were taken.
Remember, Israel had lived in the Promised Land once before.
When Joseph died outside of the land, he needed to be returned to
establish continuity. Joseph, in other words, had to be connected
with Joshua. The same thing happencd in the New Testament. John
the Baptist died, representing the Old Covenant, Jesus Christ cre-
ated continuity with the final Old Covenant Head. He died and was
buried outside of Jerusalem, outside the land. After the Resurrec-
tion, He returned to the land, just as Joseph returned with Joshua.
He established continuity with His new army.

When Christ was born, Israel was eating meals with the “golden
calf” again. Israel had become like a “new” Egypt. Everywhere Jesus
went, He encountered people cursed with plagues like sickness,
demon-possession, and lameness. Jesus said their father was the
devil (John 8:44). When they ate the Passover, or any sacramental
meal, they were eating and drinking judgment to themselves. They
would die just as their forefathers hac. Christ told them who He was
Lime and again. Like Moscs, He went through the camp offering a
time of repentance. They refused, and by His own birth, death, and
resurrection, He removed the “flesh” (Col. 2:11-13). For those on the
Lord’s side, the oncs whe turned and trusted in Christ, His death
and resurrection definitively accomplished salvation. Seventy a.p.
was the progressive outworking of the destruction of the Old Cove-
nant, which had become flesh, on those who wanted to remain in the
wilderness of rebellion. But Christ did all of this not by the sword,
but by His own covenantal death, Dominion over the apostates was
through the application of the covenant.

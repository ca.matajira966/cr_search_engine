300 THAT YOU MAY PROSPER

The Spirit had come in special measure, “given life” (II Cor. 3:6),
and had life-giving signs. Ullimately, this is the great difference be-
tween Old and New. Baptism and communion are signs of Life
through the Holy Spirit. The great transition point was the resurrec-
tion of Christ. After He was raised from the dead, He began to fill His
disciples with the Holy Spirit (John 20:22), After He ascended to the
right hand of God, the Holy Spirit was given to a broader circle at
Pentecost. Jesus had said the Spirit would be left with man to be
Christ’s Presence on earth. The Spirit would overcome the “flesh.”
Certainly “individual” men still lived by the “flesh” and “died,” but
the contrasts of the New Testament between Old Covenant and New
Covenant lay in the fact that the Spirit had been poured out on all of
humanity. The Spirit would put down alll rule by the flesh.

The signs of the New Covenant are symbols of the Spirit, not of
the “flesh.” There are two: baptism and communion. Why only two?
Aren't there other symbols in the New Covenant? Yes, marriage is a
sign of the covenant. Why isn’t it a sacrament? Marriage is a symbol
but it does not in any way sea/ into the covenant. Paul argues that
one is free to marry or not to marry. Neither situation makes one
more or less in covenant with Christ (I Cor. 7:8-24). Some churches
include marriage as a sacrament, but only because they have a much
broader definition of sacraments. This word should be reserved, how-
ever, for (hose covenantal acts which seaf a person to Christ. These
are baptism and communion.

 

Baptism (Boundary Sacrament)

We have already seen in the Great Commission Covenant that
baptism is the new sanction. It represents the work of the Holy Spirit.
Jesus says, “John baptized with water but you shall be baptized with
the Spirit” (Acts 1:5). Paul says, “He saves us, not on the basis of
deeds which we have done in righteousness, but according to His
mercy, by the washing of regeneration and renewing by the Holy
Spirit? (Titus 3:5). We should keep in mind the principle of the Trin-
ity as we read such a passage. The “water” itself does not regenerate,
but signifies the incorporation of the believer under terms of the covenant. It
seals the believer covenantally to the Holy Spirit. Baptism warns the
baptized: break the terms of the covenant by renouncing Christ, and
you will perish by the terms of the covenant. Because baptism is a
sanction, there are dual sanctions: blessing and cursing. Baptism
seals to Christ, and is in this sense automatic, but it does not auto-

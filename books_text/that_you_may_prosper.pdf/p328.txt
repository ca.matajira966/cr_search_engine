308 THAT YOU MAY PROSPER

ciples of legitimacy mentioned earlier, the wicked are judged. The
best illustration is toward the last part of the Book of Revelation.
The last series of judgments are the “seven chalices” (Rev. 16:Lff.).
These are clearly references to the “chalice” of communion.

The implication is that as the Church exercises discipline
through its own communion, God begins to clear away the “clutter”
on the earth. God pours out His vengeance on all thosc who oppose
the Church, In a way, this is now happening in the Western world.
For the first time in centuries, the Church is beginning to commune
in disciplined fashion. It ts not surprising to see God curse the world
with a disease like AIDS. The same thing happened in the 14th cen-
tury with the Plague, It happened again in 1493, when Columbus’
crew brought Syphilis to Europe, and it spread across the continent
within a decade. (In 1517, the Reformation began.) Judgment begins
at the House of God. God weeded out Gideon’s army before He
allowed him to fight the Midianites. Discontinuity with the world,
through communion, had to occur before any kind of continuity
with the new world could be allowed. In other words, faithful com-
munion with Christ clears away the “fleshly” world so that Christians
can live in it.

Third, communion establishes continuity and discontinuity
within the family, God claims whole familics, We saw this cartier
when I talked about “household baptisms.” But baptism only applics
the sanctions. Every member of the family is entitled to Christ’s inher-
itance if he (or she) has been baptized. Paul says of the Red Sea bap-
tism and communion, “Ai were baptized into Moses in the cloud and
in the sea; And aff ate the same spiritual food; and ai! drank that
same spiritual drink, for they were drinking from a spiritual rock
which followed them; and the rock was Christ” (I Cor, 10:2-4). The
“all? means that each individual was baptized and ate the spiritual
communion of Christ. Significantly, this context applies these princi-
ples ta communion.

The Lord’s Supper is for the whole family, providing they have
already received the sanctions in baptism, To take communion be-
fore the sanctions is the same as attempting to have life apart from
the sanction of death, This is a denial of the fundamental principle of
life through death. For those who have been baptized, however, com-
munion is part of the inheritance. Full family communion establishes
continuity among family members. If the children are not allowed to
commune, then they have no continuity with the rest of the family.

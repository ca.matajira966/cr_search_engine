The Covenant Structure of the Thirty-Nine Articles 317

phasis of each section in light of the covenant structure. I think we
will see fairly easily without forcing them that the sections of the
Thirty-Nine Articles parallel the covenant model, making them a cove-
nant document.

The Covenant Structure of the Thirty-Nine Articles

1-5 (Transcendence): Section one begins, “There is but one living
and true God.” The articles open with a discussion of the Trinity and
the Incarnation. Could there be any clearer reference to the first point
of covenantalism; transcendence? These articles describe God and
who He is. They note His transcendence as well as His immanence.

5-8 (Hierarchy): The next section focuses on Holy Scripture and
the creeds. These are the authorities of the Church. It should be men-
tioned that article seven briefly describes the relationship between
Old and New Testament and in this context talks about the law. But
the emphasis is not so much on law as it is on the authority of the Old
Testament over New Testament Christians. This article is a direct
attack on those deny any abiding validity (authority) of the Old Tes-
tament for New Testament believers.

9-18 (Ethics): The third part of the Thirty-Nine Articles addresses
judicial theology. Section nine starts by presenting our fegal identifica-
tion with Adam. Then it talks about justification. And finally, it
spends a considerable amount of time on “good works.” Article four-
teen says, “Voluntary works besides, over and above, God's Com-
mandments, which they call Works of Supererogation,? cannot be
taught without arrogancy and impiety.”

19-35 (Sanctions): The fourth group of articles concentrates on the
Church and particularly the sacraments. These are the sanctions of
the covenant. I must break slightly with Howe’s division at the
thirty-sixth article. I think the section probably ends on the
thirty-fifth because article thirty-six has to do with suecesston, the con-
secration of priests and bishops. This is a change of subject matter.
And, as we have seen, the final section of the covenant is continuity,
the confirmation of a successor.

2. Works added. The Catholic Church believes there can be works beyond the
commandments themselves, a kind of “super” righteousness. But Reformation theol-
ogy argues that the commandments are a reflection of the very character of God.
There cannot be supererogation because it is impossible to be holier than God.

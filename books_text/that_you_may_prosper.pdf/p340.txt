320 THAT YOU MAY PROSPER

Greidanus, Sidney. Sola Scriptura: Problems and Principles in Preaching
Historical Texts. Toronto, Ontario: Wedge, [1970] 1979. Greidanus
shows how a covenant view of Scripture affects preaching. No
preacher should step into the pulpit without reading this book. As
the title indicates, it talks about “how to preach historical texts.” In
a day when most preaching is done from the Pauline Epistles,
Bible students ought to ask, “Why?” Answer: the historical texts
cannot be adequately handled apart from the covenant, particu-
Jarly the “redemptive historical” principle. This enables the reader
to see the major Old Testament “deliverers” as “redemptive” fig-
ures, typifying Christ. Greidanus shows how that when preaching
in Holland turned away from the covenantal principle, it degen-
erated into psychologizing and moralizing the text. This book is a
must for our day and time.

Jordan, James. Judges: God’s War Against Humanism. Tyler, Texas:
Geneva Ministries, 1985. This is an excellent example of how a
“popular” commentary ought to be done. Mr. Jordan thinks
through the text with “covenantal presuppositions,” appling a “re-
demptive historical” approach to the text. This is a way of recog-
nizing the progress of redemption in the Old Testament without
having to “moralize” the text.

. The Law of the Covenant. Tyler, Texas: Institute for
Christian Economics, 1984. This is a commentary on Exodus
21-23. It is another fine example of “covenantal exegesis.”

, ed. Christianity and Civilization, Vol. I, Tyler, Texas:
Geneva Divinity School Press, 1982. This volume critiques a
“non-covenantal” world and life view: the Baptist culture and the-
ology. This will help the reader to appreciate the practical out-
workings of covenanta] theology. Furthermore, there are excellent
theological essays in this volume that touch on the issue of the cov-
enant, Of particular note is “Baptism, Redemptive History, and
Eschatology: The Parameters of Debate,” by Richard Flynn.

Kline, Meredith G. By Oath Consigned. Grand Rapids: Eerdmans,
1968. As I mentioned in the introduction, I am grateful for the
work of Kline. My book has largely built on his. By Oath Consigned
is a covenantal analysis of the sacraments. Building on the “dual
sanctions” idea, he sees the sacrament of baptism as basically a
“governmental symbol” only, symbolizing not redemption but the

sanctions, 143-44
(historical), 155-57
transcendence, 139-140
(historical), 148-150
Feasts, 46
Federal Theology, xiv
First-born
communion, 307
Joseph’s sons and, 100-102
Pharaoh's, 103
Flesh, 273, 28981, 290, 299
Ford, J. Massyngberde, 254n.2
Forgiveness, 242
Foundations, 157
Frame, John, 62n.2
Franklin, Benjamin, xvii
Food, 126
French Revolution, 196
Fuller, Daniel P., 238

Gaffney, Edward McGlynn, 2n
Gaffin, Richard B., 273n.3
Gage, Warren, 268n.1
Garden, 226, 227
Gaustad, Edwin Scott, 5n.12
Genealogy, 93-94
Genesis, 100
George, Charles & Katherine, 5.11
Gerizim, 78
Gerrish, B. A., 39n.15
Gethsemane, 77
God
image of, 26
Trinitarian name, 88
Goetze, A., 43n.1
Goliath, 72n.12
Goshen, 103
Gospel-Covenant, 4
Goulder, M. D., 236n.4, 263n.10
Grace, 81
Great Commission, 127-136
baptism and, 133-34
continuity, 135
ethics, 130-31
hierarchy, 129-130
sanctions, 133
structure, 128

Index 341

transcendence, 128ff.
Great society, 9
Greek Philosophy, 184
Greek Religion, 37
Gutiney, Edward, 1470.2

Haldane, Robert, 314
Hale, Matthew, 187
Halfway covenant, 7, 92
Haman, [51
Hamlin, John, 108n.10
Harris, R. C., 1440.4
Harrison, Jane, 37
Harrison, R. K., 226n.3
Hatch, Nathan G., 6n.13
Hell, 96, 98
Henry, Martyn Dexter, 190n.12
Hermeneutics, 275
Hendricksen, Williarn, 256, 310, 311
Hierarchy
accountability of, 48
Biblical covenant, 43
bottom-up, 50
bureaucracy and, 50
court system of, 43
Cultural Mandate, 1268.
centralization, 51
Old Testament, 46
representative, 47
representative principle, 46
satan’s, 52
satan’s challenge of, 44
Saul’s violation, 42
second point of covenantalism, 41
summary, 120
suzerain treaties, 43
wartime, 52
Hillers, Delbert, 119
History
cavenantal, 53
hierarchy and, 53
progress of, 54
redemptive, 53
transformation, 56
Hobbes, Thomas, 69, 70
Hodge, Charles, xvi, 314
Holifield, Brooks, 92n.18

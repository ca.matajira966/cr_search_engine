342 THAT YOU MAY PROSPER

Holwerda, B., 66n.5
Homosexuality, 188
Hosea
book of (structure), 207ff.
meaning of name, 207
prophet, 2058.
Household
baptisms in, 90
infant baptisms and, 89-91
sacraments and, 89
salvation of, 89ff.
Hughes, Charles Evans, 10
Humanist Manifesto I & II, 8
Hurrian marriage contracts, 150

Idolatry
rebellion and, 42
Ten Commandments and, 217,

220-21

Illegitimacy, 109

Image of God, 220

Immigration, 12

Immanence, 25
definition, 35
false, 34

Imputation, 27ff.
covenant death and, 30
covenant life and, 31
institutions and, 31
marriage and, 31

IRS, 12

Incarnation
false, 217

Incest, 117

Incorporation, 87

Individualism, 114

Infusion theology, 29

Inheritance, 90ff,
blessing and, 103
double, 100
double portion, 103
false, 102-103
legitimacy and, 103
New Covenant, 103
first-born, 100
second-born, 100

Intellectual schizophrenia, 184

Interposition, 180
Intrusion theory, 281
Isaac, 113

Jacob, 98-99
‘Jehovah's Witnesses, 106
Jesuits, 131
Jews, OF
Job, 67-68
Johnson, James Turner, 1471.1
Jones, Archie, 184n.5
Jordan, James B., 46n.4, 49n.6, 54,
55, 188n.11, 190n.12, 22in.6, 222n.9
Joseph, 968.
Joshua, 60, 104
Josiah, 1-2, 14, 19, 21
Jubilee, 135
Judges, 60
Judicial Theology, 30
Judgment
blessing follows, 79
circumcision, 292
day of, 79

Kalland, E., 144n.4

Kavenagh, W. Keith, 148n.3

Kenizzite, 102

Kempis, Thomas &, xvii

Kevan, Emest, 81n.6

Key of David, 257-58

Kingdom, 309-315

Kings and constitutions, 200

Kline, xii

Kline, Meredith G., 16, 161.31, 17,
24n.3, 43n.1, 60n.1, 105n.5, 215n.1,
235n.1, 236n.10-12, 255n.4,
2271n.2, 281, 282n.5, 284, 285

Korah, 96, 115

Krauth, Charles, 309n.3

Kuiper, R. B., 312n.6

Kuteh, E., 1190.1

Laban, 97

Law vs. grace, 81

Lawsuit, xv

Laying on of hands, 106
ee, Nigel, 125, 185

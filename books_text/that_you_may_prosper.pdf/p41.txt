A. First Point of Covenantalism

TRUE TRANSCENDENCE
(Deuteronomy 1:1-5)

Just as King Josiah sat on his throne, another king also sat on a
throne, a chair like a miniature mountain in the midst of a sea of or-
nate wealth. He had conquered all that could be conquered. He had
built the greatest city in the world. Through his palace window, he
could see the beautiful landscaped “hanging gardens” on the walls of
the palace—gardens so legendary that they have been included
among the seven wonders of the ancient world. Out of the corner of
his eye, he caught a brief glimpse of a chariot darting by on the top
of the wall surrounding the city, a wall so thick that it could handle
six chariots riding side by side with room to spare. Who could break
through such a fortress? Who could topple such a king?

He was to learn the answer soon: the God Who sits above men’s
fortresses.

His eyes then withdrew from the distance, distracted by the sun
reflecting off the solid gold walls in the room where he was sitting.
Old Babylon was truly one of the great wonders of the world, and
this powerful king had been part of the creation of the empire. Then
the old king began to think, “What’s left for me? How can I rise
above myself and history? How can 1 become immortally great?” In-
stantly, words popped into his head, seemingly from nowhere, but
definitely from somewhere other than his own imagination.

I will ascend to heaven;
[ will raise my throne above the stars of God,
And I will sit on the mount of the assembly
In the recesses of the North.
J will ascend above the heights of the clouds;
I will make myself like the Most High.’

1. These are statements made by a king of Babylon (Isa. 14:13-14). Some com-
mentators attribute them to others, even Satan himself.

21

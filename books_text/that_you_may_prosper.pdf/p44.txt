24 THAT YOU MAY PROSPER

Old Testament written by a non-Jew. Nebuchadnezzar’s own words
outlasted every kingdom of antiquity.

What was his theological lesson? First, he owed his position to
God. Second, it is the essence of man’s rebellion to think, speak, and
act as though man is God. Third, God casts down those who seek to
elevate themselves to the throne of God. In other words, man falls
when he seeks to rise in his own power. Fourth, God offers restoration to
man through man’s grace-imposed humility and faithfulness to God’s
Word. A man who faithfully rules under God will rute over the creation.

God was above Nebuchadnezzar, yet He was also with Nebuchad-
nezzar. He could raise him up or pull him down. He is transcendent
over mankind, yet close enough to know mankind’s heart and to
judge mankind perfectly. God, and God alone, is truly transcendent.
The Biblical covenant starts with this point.

True Transcendence (Deut. 1:1-5)

The Deuteronomic covenant begins with a declaration of tran-
scendence, when it says, “Moses spoke to the children of Israel ac-
cording to all that the Lord commanded him to give to them” (Deut.
1:3). How does this statement indicate éranscendence?

The Creator/Creature Distinction

Biblical transcendence means there is a fundamental distinction
between the Creator’s Being and the creature’s being. The Deuter-
onomic covenant specifies such a distinction. Moses’ words are
differentiated from God’s.? He speaks what God has given him. His
words are not original. Thus, the covenant first declares God to be
the Lord and Creator: the Creator of the covenant and everything
else. Why Creator?

3. Kline, Structure of Biblical Authority, pp. 135-136. Klinc makes note that the
Hebrew tiles of books are not the same as the tides in our English versions. They
derived their titles from the Septuagint, the Greek translation of the Hehrew text
(circa 200 u.c.), In the case of Deuteronomy, instead of receiving its name from
Deuteronomy 17:18 (A copy of this law,” or “This second law” from the Greek, to deu-
teronomion toute), the Hebrew name of the text would actually be the first words,
“These are the Words of,” Since this phrase is clearly identical with the first lines of
suzcrain treaty covenants, Kline concludes that this way of naming the books of the
Bible was a covenantal method of distinguishing the documents.

He also likens it to the phrase, “Thus saith the Lord.” I find this observation to be
extremely helpful because it means that the “thus saith the Lord formula” is the be-
ginning of miniature covenants, and explains why it is constantly used.

         

True Transcendence 25

The same fundamental distinction is basic to the creation of the
world. God did not create the universe out of His own substance; He
created it out of nothing. God says of His creation covenant of the
world,* “In the beginning God created the heavens and the earth,
and the earth was formless and void, and darkness was over the sur-
face of the deep, and the Spirit of God was hovering over the surface
of the waters” (Gen. 1:1-2). Space, time, matter, and history had a be-
ginning. They are not eternal. Often God uses His transcendent crea-
tion of the world to make the point that He is Lord. Isaiah says,

Iam the Lord, and there is no other; besides Me there is no God . . .
The One forming light and creating darkness, causing well-being and
calamity; I am the Lord who does all these (Isa, 45:5-7).

Paganism has always rejected this view of the origins of the uni-
verse. Pagans say that matter has always existed, whether they are
primitive pagans (“the cosmic egg”) or Greek pagans (“the co-eter-
nity of matter and form”) or modern scientific pagans (“the Big
Bang”). They refuse to accept that God could and did create matter
out of nothing. This would point to a God Who presently sustains His
creation personally, which in turn points to the existence of a God Who
Judges His creation continually. Pagans seek above all else to escape
God’s judgment.

The Biblical idea of creation involves God’s providential sus-
taining of creation throughout time and eternity. This is the mean-
ing of providence. Because God made the world and personally sus-
tains the world, He is present with the world, but is not part of the
world. Because He is the Creator and Sustainer, He is also present
(immanent) with us. This presence of God is equally an aspect of
true transcendence. No other being is fully transcendent, so no other
being is universally present. God alone is omnipresent (present
everywhere).5

4, It is significant that God created the world with “ten words” in the repeated
phrase, “And God said” (Gen. 1:3, 6, 9, Il, 14, 20, 24, 26, 27, 29), just as He revealed
His covenant-law with “ten” words (Deut, 4:13), Also, Jeremiah refers to creation as
a covenant (Jer. 33:19-26). Both in literary form and covenantal principle we see
that the creation covenant begins with fue transcendence, making creation itself
another manner in which this principle is declared.

5. This becomes extremely important when we discuss the concept of hierarchy.
Because Satan is not omnipresent, omnipotent, or omniscient, he must rely heavily
on his covenantal hierarchy for information and power. God does not rely on His
covenantal hierarchy; His people rely on it, but God does not. He voluntarily binds
Himself to His creation by the terms of His covenant (Gen. 15), but He is not bound
by any created hierarchy.

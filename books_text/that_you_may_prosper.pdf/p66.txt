46 THAT YOU MAY PROSPER

This is why Paul encourages the Ephesian Church to take rule!
Christ has conquered the powers and now He wants them out of
office. It is time for Christians to take their place of authority! How
is this done? First, we have to understand the central principle of hi-
erarchy. Second, we can then analyze how God puts His people into
positions of authority through accountability.

The Representative Principle

The second section of the covenant presents Israel as the Lord’s
representative on the earth. They were a priesthood, a hierarchy. They
not only had an internal system of representation, they represented
the rest of the nations. In the Old Testament, the priests of Israel
during the feast of booths (trumpets) offered sacrifices for a week in
the name of the gentile nations. ‘This feast symbolized the ingather-
ing of the nations. On day one, they offered 13 bulls, day two they
offered 12, and so on, for seven days, On the seventh day, they
offered seven bulls (Nu. 29:13-34). This totalled 70 bulls, corre-
sponding to the 70 nations that represented all the nations of the
earth (Gen. 10).*

The Church in New Testament times serves a similar function.
The New Testament compares the Church’s liturgy to the Old Testa-
ment's: “. . . golden bowls full of incense, which are the prayers of
the saints” (Rev. 5:8; cf. 8:3). The same function of prayer was
found in the Old Testament (Ps. 141:2). Instead of burning bulls on
an altar, the Church offers prayers as sacrifices. Churches are sup-
posed to pray “on behalf of all men” (I Tim, 2:1), and also for civil
rulers, that we might experience peace (I Tim. 2:2). The key phrase,
of course, is “on behalf of—the representative function.

Representation is inescapable. Van Til observes, “The covenant
idea is nothing but the expression of the representative principle consist-
ently applied to all reality.”5 All of creation images God, especially
man. And because of sin, man needs a representative to atone for
him. Paul explains the representative principle when he says, “For if
by the transgression of the one [Adam] the many died, much more
did the grace of God and the gift by the grace of one Man, Jesus
Christ, abound to many” (Rom. 5:15). Since mankind has no essen-

4, James B, Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
Texas: Institute for Christian Economics, 1984), p. 190.

5. Cornelius Van Til, Survey of Christian Epistemology (Grand Rapids: Den Dulk
Christian Foundation, 1969), Vol. II, p. 96. Emphasis added.

Ethics 61
do what God has told Joshua. God commands, Joshua obeys, and Israel

receives what was promised. There is a cause/effect relationship between
faithfulness to the commandment and the fulfillment of blessing.

The Book of Judges illustrates the other side of the ethical
cause/effect principle: unfaithfulness resulting in death. The last
verse of the book, “They did what was right in their own eyes,” sums
it up (Judg. 21:25). During the period after Joshua’s great con-
quests, the people reverted to unfaithfulness. Judge after judge was
raised up. Over and over again the people turned away from Moses’
commandments. They learned the hard way that the ethical cause/
effect relationship cannot be avoided. Break the commandments,
and the curse af death will be fulfilled.

The ethics principle of the covenant is just this simple. All
through the Bible, the command/fulfillment pattern repeats itself.
Why? Man must relate to God by covenant. He cither keeps or
breaks the covenant; he is either faithful or unfaithful. Everything
comes about according to this basic ethical cause/eflect relationship.

Furthermore, the stipulations section of Deuteronomy specifies
that only a certain kind of person can [ulfill the commandments of
God, a true son of the covenant. The ethics segment instructs the
fathers to be true sons of God themselves by teaching their own sons
the commandments (Deut. 6:1-26). The fathers imaged God by
training their sons to image them, And when the sons followed their
fathers in obedience to God, the second generation also imaged its
Heavenly Father, In other words, a faithful son manifests his sonship
by being a true image-bearer of his True Father, God (Gen. 5:1ff.)
‘Vhe faithful son in Genesis was to have demonstrated his sonship by
carrying out three offices: prophet, priest, and king. So the motif of
faithful sonship appears among the stipulations of Deuteronomy in
the fulfillment of the same three offices. The kings apply God’s law to
the land (Deut. 16 & 17). The priests guard the sanctuary and law of
God (Deut, 18:1-8). The prophets deliver messages from God’s
heavenly council (Deut. 18:9-22). Hence, the theme that only the
true son can keep the commandments of God consistently surfaces in
the ethics part of the covenant, and it comes to its ultimate fulfill-
ment in Jesus Christ, the ultimate Son of the covenant.

 

Three Aspects of Biblical Ethics

When I speak of an ethical cause/effect relationship, am I just
talking about an external obedience to the Ten Commandments? No.
Following God’s commandments involves proper motivations, stand

  

64 THAT YOU MAY PROSPER

2nd Word (12:1-13:18): Hierarchy of the Sanctuary

3rd Word (14:1-29): Consecration of God’s Presence

4th Word (15:1-16:17): Sabbath rest

Sth Word (16:18-18:22): Fathers of covenant community

6th Word (19:1-22:12): Laws of killing God's transcendent
image bearer

7th Word (22:18-23:14): Laws of submission

8th Word (23:15-24:22): Laws against various forms of
manipulation

9th Word (25:1-19): Justice

10th Word (26:1-19): Firstfruits

As one reads through the list of the Ten Commandments, ex-
pounded in detail, perhaps he notices that they follow the five points
of covenantalism, repeating them twice (5 and 5). I will go into this
structure at length in a later chapter where the Ten Commandments
are used as support of the five-fold covenantal concept. But even
with this short summary of the third section of the covenant, the ob-
jective character of God’s ethics stands out. He has categorized His
requirements.

Have they changed? Although aspects of the commandments
themselves have been altered, the ten summary categories have not.
Listen to what Jesus says when He was asked about the “greatest
commandment.”

He said to them, “‘You shall love the Lord your God with all your heart,
and with all your soul, and with afl your mind.’ This is the great and fore-
most commandment. The second is like it, ‘You shall love your neighbor as
yourself’ On these two commandments depend the whole Law and the
Prophets” (Matt. 22:37-40).

Do these words sound familiar? They should. They are direct
quotations from Deuteronomy, particularly the section to which we
just referred above (6:5). When Jesus was asked about the greatest
commandments, He quoted the Law. Thus, God’s standard of right-
eousness does not change.

His standard, for that matter, reflects His character. [f His char-
acter does not change, neither can He. Several years ago, I remem-
ber that I took the New Testament and started to list all of the com-
mands. To my surprise, they could all be grouped in the categories of
the Ten Commandments. All of them were there. So, God's stand-

68 THAT YOU MAY PROSPER

Job: A Fly in the Ointment?

There is one apparent exception, one that caused the Puritans a
great deal of consternation: the life of Job, T have spent considerable
space developing the relationship between how man responds to
God and what happens in life: the ethical cause/effect principle.
Does this concept break down in the story of Job? No.

Job’s life only breaks down cause and effect in that from all out-
ward appearances he is temporarily stripped of all blessing. He lost his
children, possessions, health, and friends. His friends, moreover,
continue throughout the main body of the book to use the cause/effect
principle to persuade Job that he is guilty. Yet, Job does not yield.
He argues that he is justified before God. In many ways, the entire
book is an exposition of the great doctrine of justification. Finally, he
is exonerated by God. In the end, Job does receive the blessing of the
covenant. Not in another life but in és life.®

So, there are times when it seems like there is no connection be-
tween ethics and effects. Indeed, there are moments in history where
it appears that the wicked are “blessed,” and the righteous are “cursed.”
Why? Sometimes, the people of God as a covenantal whole are being
disciplined. In His discipline, God restricts and even takes away
blessing from individuals, This in itself confirms the ethical cause/
effect principle. At other times, God’s pcople— martyrs and others
who have to undergo unusual suffering— receive delayed portions of
the blessing. Their blessing comes in the form of internal and even
greater amounts of blessing when they reach heaven. Finally, God
sometimes tests His people to see if they will live by faith (James
1:1-2). The test is the apparent reversal of the causc/cffect principle to
strengthen the righteous’ faith. Job is the classic example. In the
short term, he was “cursed” for being righteous. In the long term, he
was blessed doubly, receiving multiples of his original blessings ( Job
42:10-17). Job is not a “fly in the ointment.” His life reinforces the
ethics principle of the covenant.

The very center of the covenant is ethics. Man is called upon to
view the world through the eye of faith. He is to believe that every-
thing going on around him is tied to the covenant. If he does not
understand life’s events in these terms, he will resort to other cause/
effect explanations.

6. Jesus confirms the promise of “thie life” bicssing when He says, “Phere is not
one who has left house or wife or brothers or parents or children, for the sake of the
kingdom of God, who shal] not receive many times as much at this time and in the
age to come, inherit cternal life" (Luke 18:29-30). In the New Covenant, everyone
gets to be like Job!

86 The Changing of the Guard

appearance as a man, He humbled Himself and became obedient
to the point of death, even the death of the cross. Therefore God
also has highly exalted Him and given Him the name which is
above every name, that at the name of Jesus every knee should
bow, of those in heaven, and of those on earth, and of those under
the earth, and that every tongue should confess that Jesus Christ is
Lord, to the glory of God the Father (Philippians 2:5-t1).

They were also to imitate the Apostle Paul (Philippians 3:17)
who asserted:

But indeed [ also count all things loss for the excellence of the
knowledge of Christ Jesus my Lord, for whom I have suffered the
loss of all things, and count them as rubbish, that [ may gain
Christ and be found in Him, not having my own righteousness,
which is from the law, but that which is through faith in Christ, the
righteousness which is from God by faith; that I may know Him
and the power of His resurrection, and the fellowship of His suffer-
ings, being conformed to His death, if, by any means, I may attain
to the resurrection from the dead (Philippians 3:8-11).

Believers can stand firm on the truth despite opposition and
persecution. We can emerge victorious, transforming prison into
promise. We can know the power of Christ’s resurrection.

But only if we are humble.

We cannot hope to win the battle for the hearts of men and the
souls of nations if we constantly compromise Biblical essentials
(Matthew 28:20), But then, neither can we hope to win that battle
if we carry airs of superiority (Matthew 5:3-5).

The Religious Right

You might automatically assume that the emergence of the re-
ligious right as a major force in American politics is cause for
great joy. And in many respects it is.

Certainly, steadfastness has been manifested. The religious
right has been thrown into a media lion’s den of criticism and
persecution, yet it continues to stand for Scriptural values. It con-
tinues to prick the American conscience and sway the American
consensus.

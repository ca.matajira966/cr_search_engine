Prayer and Precept 93

Ask, and it will be given to you; seek, and you will find; knock,
and it will be opened to you (Matthew 7:7).

‘Watch and pray, lest you enter into temptation. The spirit in-
deed is willing, but the flesh is weak (Matthew 26:41).

Watch therefore, and pray always that you may be counted
worthy to escape all these things that will come to pass, and to
stand before the Son of Man (Luke 21:36).

..» praying always with all prayer and. supplication in the
Spirit, being watchful to this end with all perseverance and sup-
plication for all the saints (Ephesians 6:18).

We are to pray.

We are to pray with wholeheartedness (Jeremiah. 29:13), We
are to pray with contrition (2 Chronicles 7:14). We are to pray
with all faith (Mark 11:24). We are to pray with righteous fervor
(James 5:16). We are to pray out of obedience (t John 3:22) and
with full confidence (John 15:7). We are to pray in the morning
(Mark 1:35), and in the evening (Mark 6:46), during the night
watch (Luke 6:12), and at all other times (t Thessalonians 5:17).

God has given us access to His throne (Hebrews 4:16) and fel-
lowship with Christ (1 Corinthians 1:9). And He expects us to
make use of that glorious privilege at every opportunity (1 Timothy
2:8).

Nehemiah did.

At every turn, Nehemiah made supplication to the Lord God
on high. When he appeared before Artaxerxes to make petition to
rebuild the wails of Jerusalem, he prayed (Nehemiah 2:4), When he
entered the ruined city to begin the task, he prayed (Nehemiah
2:12). When threats of violence and conspiracy jeopardized the
fledgling reconstruction project, he prayed (Nehemiah 4:2). When
there were conflicts and crises among the people that required his
judicious hand, first he prayed (Nehemiah 5:19). When an attempt
on his life threatened the entire project, he didn’t panic — he prayed
(Nehemiah 6:9). When his own brethren turned against him, he
prayed (Nehemiah 6:14), And when the work on the walls was com-
plete—you might have guessed it—he prayed (Nehemiah 13:31).

Nehemiah’s whole political platform was built on prayer.

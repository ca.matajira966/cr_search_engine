98 The Changing of the Guard

Adherence to the sure Word of God’s precepts is so important
in revoking the curse of subjectivity from our prayer lives that
God expressly forbids ua from exceeding what is written (1 Corinthi-
ans 4:6). God’s will is found in God’s Word. Period.

You shall not add to the word which I command you, nor take
anything from it, that you may keep the commandments of the
Lord your God which F command you (Deuteronomy 4:2),

Whatever I command you, be careful to observe it; you shall
not add to it nor take away from it (Deuteronomy 12:32),

Every word of Gad is pure; He is a shield to those who put

their trust in Him. Do not add to His words, lest He reprove you,
and you be found a liar (Proverbs 30:5-6).

It is essential that we steer clear of the insidious trap of “doing
what is right in our own eyes.” We must tie our prayer lives to the
objective standard God has given us in the Bible.

‘There is a way which seems right to a man, but its end is the
way of death (Proverbs 14:12).

Therefore, laying aside all malice, all guile, hypocrisy, envy,
and all evil speaking, as newborn babes, desire the pure milk of the
word, that you may grow thereby (1 Peter 2:1-2).

Prayer is a mighty force to be reckoned with. And Biblical
prayer is a mighty force for truth, justice, mercy, and peace
(i Timothy 2:1-8).

‘That makes for rea/ political action.

Conclusion

The ninth basic principle in the Biblical blueprint for political
action is that we must pray. Prayer changes things. It moves
mountains, changes hearts, and alters history. Thus, before we
rally support, or call for conventions, or draft manifestos, we
must pray. We must make certain that we don’t put the cart before
the horse.

It is essential that Christians take up the ministry of political
action. Now. En masse. But~we must make certain that we do

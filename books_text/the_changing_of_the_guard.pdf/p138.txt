106 . The Changing of the Guard

port, no notoriety, and no cooperation, like Elijah (1 Kings
19:1-18) or Jeremiah (Jeremiah 1:4-10).

‘We may have to start “in weakness, in fear, and in much trem-
bling” (1 Corinthians 2:3), without “persuasive words of wisdom”
(1 Corinthians 2:4), like the Apostle Paul (1 Corinthians 2:1) or
Moses (Exodus 4:1-17).

Instead of allowing their limitations and liabilities to discour-
age and debilitate them, the heroes of the faith went to work,
God’s power was made manifest in their weakness (1 Corinthians
1:26-29), In spite of the odds, they won. By faith.

And God has set them before us as examples.

Against all odds, Ehud faced the power of Moab and won
(Judges 3:12-30). Against all odds, Shamgar faced the power of the
Philistines and won ( Judges 3:31). Against all odds, Gideon faced
the power of Midian and won ( Judges 6:12-8:35). Against all odds,
David faced the power of Goliath and won (1 Samuel 17:42-52).
Against all odds, Jonathan faced the power of the Canaanites and
won (1 Samuel 14:1-15).

Against all odds, God gave His faithful people victory, Against
all odds, He made them examples for us.

Isn’t it high time for us to imitate them, to follow in their foot-
steps, to pay heed to their example? Isn’t it high time for us to
demonstrate to an unbelieving world that God can still beat the
odds? Isn’t it high time for us to prove to a fallen and depraved
generation that God can raise up a weak and unesteemed people
against all odds and win? Isn’t it high time for a changing of the
guard? Isn't it?

Covenant Blessings

Tn the heart of the Pentateuch, God lists the covenant bless-
ings that He will shower upon any society that is faithful to Him
and His Word:

Now it shall come to pass, if you diligently obey the voice of the
Lord your God, to observe carefully all His commandments which
T command you today, that the Lord your God will set you high
above all nations of the earth. And all these blessings shall come

108 The Changing of the Guard

To be sure, the odds are stacked against us. Even so, God has
assured us the victory is ours for the taking.

The road to victory may be fraught with danger and difficulty.
But the Lord will clear the way. He has promised us He would.

‘We need only to follow the example of the righteous men and
women who have gone before us. We need only to imitate those
who have taken God at His Word, facing the odds and claiming
dominion,

If we are salt and light, reclaiming the land with excellence,
valor, and honor, no force on the face of this earth will be able to
withstand us. Nothing will be withheld from us.

For though we walk in the flesh, we do not war according to the
flesh. For the weapons of our warfare are not carnal but mighty in
God for pulling down strongholds, casting down arguments and
every high thing that exalts itself against the knowledge of God,
bringing every thought into captivity to the obedience of Christ,
and being ready to punish all disobedience when your obedience is
fulfilled (2 Corinthians 10:3-6).

Against all odds, we can win. Against all odds, we shall win.

Summary

God has set before us examples: good examples to imitate,
bad examples to avoid,

This is, in fact, the essence of discipleship: following after the
righteous from glory unto glory and from victory unto victory.

For this reason God gives double witness to His very great and
precious promises: Word and deed. He gives us models to pattern
ourselves after.

The Israelites had this two-fold witness when they went-up to
the Promised Land, but they failed to hear and heed. And thus
they were defeated.

The lesson for us is clear: We must not imitate those faithless
Jews at Kadesh. Instead, we must pattern ourselves after Abra-
ham, Moses, Gideon, David, Daniel, and the disciples.

If we follow them, then their victory will become our victory.

We can thus face the odds . . . and win,

We can thus effect a changing of the guard.

13
WHAT YOU SHOULD DO

One church can make a real difference.

One family can make a real difference.

Even one person can make a real difference.

Every vote matters.

In 1645, Oliver Cromwell gained control of England by one
vote. In 1776, one vote determined that English, not German, would
be the official American language. In 1845, one vote brought Texas
into the Union. In 1860, one vote determined that the radical
Unitarians would gain control of the Republican party, thus
sparking the War Between the States. In 1923, one vote gave Adolf
Hitler control of the Nazi party. In 1960, John F. Kennedy
defeated Richard Nixon for the presidency by less than one vote per
precinct. George McGovern and Paul Laxalt first won their seats
in the U.S. Senate by less than one vote per precinct, while Jere-
miah Denton and Paula Hawkins lost theirs by less than one vote
per precinct. The drubbing Republican candidates received in the
mid-term elections of 1986 averaged out to less than one vote lost
per precinct nationwide.

Every vote matters.

The fact is, never once in the history of our Republic has a
majority of the citizens elected a president. Just 26.7% of the elec-
torate put Ronald Reagan in the White House in the “landslide”
of 1980. In 1836, less than 12% made Martin Van Buren the chief
executive.

Since only about 60% of the people are registered to vote and
only about 35% of those actually bother to go to the polls, a can-

M45

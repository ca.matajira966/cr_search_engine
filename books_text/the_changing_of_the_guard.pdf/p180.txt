148 The Changing of the Guard

satisfactory, But strive to get 100%, even if you need to do a little
follow-up work after all is said and done. After all, every vote matters.

Getting the Word Out

Some Christians are hesitant to get involved in the political
process simply because they are uncertain about issues or candi-
dates. Politics is so complex. The campaigns are filled with accu-
sations and counter-accusations, contradictions and counter-
contradictions. How can anyone know for certain what the truth is?

We certainly can’t depend on the newspapers, radio, or televi-
sion to give us the straight story. Information from the media is all
too often gathered by questionable people from questionable
sources who work for questionable organizations. The media spe-
cializes in name recognition, and they are able to create a desired
image by controlling (or sometimes withholding) information.
The result of voting under this counsel is that responsible Chris-
tians may be influenced by name recognition and an “honest”
image in the press only to see later that these men stand for princi-
ples totally opposed to God’s Word.

Thus, you will need to be sure to have plenty of well-
researched, non-partisan literature on hand. If there are no
“Christian scorecards” available in your area, do your own home-
work. Call or go by the various campaign offices and ask where
each candidate stands on such issues as abortion, pornography,
taxes, and commercial regulation. ff a candidate is an incumbent,
obtain a copy of his voting record. Tally the results of your find-
ings and then make copies for distribution at church or in your
community,

Of course, if you want to get a bit more ambitious than simply
holding a coffee clatch after church handing out your literature,
you may want to tap into the direct mail boom.

Utilizing the mails, a small name list, and a personal com-
puter, you can make a real difference. You can really get the word
out and ultimately you get the vote out.

The master of political mailing lists is Richard Viguerie, a
member of the “new right” who began with a list of donors to

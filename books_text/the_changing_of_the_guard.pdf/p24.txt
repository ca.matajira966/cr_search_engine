xxiv The Changing of the Guard

care one way or the other, if they are left alone— if they are allowed
to be left alone. But in the revolutionary political changes of the
last two centuries, almost nobody has been ieft alone. Revolu-
tionary humanists, unlike most Christians today, have long recog-
nized that the war between humanism and Christianity is total,
and it therefore involves everyone and everything. Nobody is
allowed to be left alone. It is this fact that Christians in the United
States have only begun to recognize since 1980: the humanist state is
not going to leave Christians alone. Furthermore, it never intended to
leave them alone; and this disturbing realization on the part of
Christians is part of the faint signs of a coming political transfor-
mation. As Christians begin to rethink their situation, something
unique is taking place in their thinking. For one thing, you are
reading this book, Would you have read it five or ten years ago,
had it been available?

Wolin has argued that political revolutions create revolutions
in political theory. Since 1965, the West has been experiencing the
preliminary shocks of a looming political earthquake. At the Uni-
versity of Houston in the early 1970s, George Grant became
fascinated with the teachings of Sheldon Wolin. He studied under
one of Wolin’s former students. Oddly enough, a dozen years ear-
lier I too had studied political thought under another of Wolin’s
former graduate students. Grant understands the potentially rev-
olutionary nature of the next dozen or so years. His book reflects
this awareness.

George Grant brought some unique qualifications to this writing
project. Besides having pastored a Church for nearly a decade in
Houston, Texas, he is the author of several ground-breaking books
on current social issues and theology. He is the president of HELP
Services, a charitable relief organization that has been profiled in
the national media including the Wall Street Journal, ABC’s Nightline,
and CBN’s 700 Club. He also serves as the host of the nationally
syndicated radio program, “Christian Worldview,” as the president
of a Crisis Pregnancy Center, and as the director of LifeNet, a
coalition of pro-life organizations in the Texas-Gulf Coast region.

Grant believes that the coming disruptions of the world econ-

10

The Changing of the Guard

the end from the beginning and from ancient times things which
have not been done, saying, My purpose will be established, and I
will accomplish all My good pleasure (Isaiah 46:9-10).

For not from the east, nor from the west, nor from the desert
comes exaltation; but God is the Judge; He puts down one, and
exalts another (Psalm 75:6-7).

Let all the earth fear the Lord; let all the inhabitants of the
world stand in awe of Him. For He spoke, and it was done; He
commanded, and it stood fast. The counsel of the Lord stands for-

ever, the plans of His heart from generation to generation (Psalm
33:8-9, 11).

God rules the hearts and minds and ways of men:

A man’s heart devises his way, but the Lord directs his steps
(Proverbs 16:9),

For in Him we live, and move, and have our being (Acts 17:28).

It is God who is at work in you, both to will and to work for His
good. pleasure (Philippians 2:13),

For this God is our God forever and ever; He will be our guide
even unto death (Psalm 48:14).

God rules the nations of the earth:

For the Kingdom is the Lord’s, and He rules over the nations
{Psalm 22:28).

Why are the nations in an uproar, and the peoples devising a
vain thing? The kings of the earth take their stand, and the rulers
take counsel together against the Lord and against His Anointed:
“Let us tear their fetters apart, and cast away their cords from us!”
He who sits in the heavens laughs, the Lord scoffs at them. Then
He will speak to them in His anger and terrify them in His fury
(Psalm 2:1-5).

The Lord nullifies the counsel of the nations; He frustrates the
plans of the peoples (Psalm 33:10).

And they sang the song of Moses the bond-servant of God and
the song of the Lamb, saying, “Great and marvelous are Thy
works, O Lord God, the Almighty: Righteous and true are Thy

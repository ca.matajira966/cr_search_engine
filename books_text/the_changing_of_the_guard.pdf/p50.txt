18 The Changing of the Guard

meant by that. To “render unto God the things that are God's”
means to render unto God everything. It means to render unto Him
all that we have and all that we are. It means to recognize His
theocracy. For, “the earth is the Lord’s” (Exodus 9:29). “The earth
is the Lord’s and all it contains” (1 Corinthians 10:26).

To “render unto God the things that are God’s” means that we
recognize the rule of God as authoritative over all things, and éhat
means that every other power, though valid, is Himéted, being sub-
ordinate.

Thus, Jesus affirmed the degitimacy of Caesar’s rule while at the
same time announcing the /imitation of Caesar’s rule.

Later, the Apostle Paul would reiterate this position:

Let every person be in subjection to the governing authorities.
For there is no authority except from God, and those which exist
are established by God. ‘Therefore he who resists authority has op~
posed the ordinance of God; and they who have opposed will re-
ceive condemnation upon themselves. For rulers are not a cause of
fear for good behavior, but for evil. Do you want to have no fear of
authority? Do what is good, and you will have praise from the
same; for it is a minister of God to you for good. But if you do what
is evil, be afraid; for it does not bear the sword for nothing; for it is
a minister of God, an avenger who brings wrath upon the one who
practices evil. Wherefore it is necessary to be in subjection, not
only because of wrath, but also for conscience’ sake. For because of
this you also pay taxes, for rulers are servants of God, devoting
themselves to this very thing. Render to all what is due them: tax
to whom tax is due; custom to whom custom; fear to whom fear;
honor to whom honor (Romans 13:1-7).

Because God ordains civil government, the magistrates have
real authority. But at the same time, because God ordains civil
government, the magistrates are under authority. Caesar represents
God's rule and so be must be obeyed. But precisely because he
represents God, he must abide by the constraints that God has
placed on him. He is a “servant” (Romans 13:6) or a “minister”
(Romans 13:4) of God. He is thus bound to obedience just as the
people are bound to obedience.

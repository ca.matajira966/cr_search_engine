44 The Changing of the Guard

“bless those” who blessed him and “curse those” who cursed him
(Genesis 12:3) and to “bless” him all the days of his life (Genesis
12:2). But central to ail these promises was the one promise: the
promise of /and.

God chose Abram to “go forth” out of Ur of the Chaldees and
from his “relatives” and from his “father’s house” in order to take
possession of land (Genesis 12:1),

And the Lord appeared to Abram and said, “To your descen-
dants I will give this land” (Genesis 12:7).

For all the land which you see, I will give it to you and to your
descendants forever. Arise, walk about the land through its length
and breadth; for I will give it 10 you (Genesis 13:15, 17).

And I will give to you and to your descendants after you, the
Jand of your sojournings, all the land of Canaan, for an everlasting
possession, and I will be their God (Genesis 17:8).

By faith Abraham, when he was called, obeyed by going out to
aland which he was to receive for an inheritance; and he went out,
not knowing where he was going. By faith he lived as an alien in
the land of promise, as in a foreign land, dwelling in tents with
Isaac and Jacob, fellow heirs of the same promise; for he was look-

ing for the city which has foundations, whose architect and builder
is God (Hebrews 11:8-10).

Land was central to the promise God gave Abram, and it was
central to the mission God gave him, as well. The promised land
was promised land. (See Gary North’s book in the Biblical Blue-
prints Series, Inherit the Earth: Biblical Principles for Economics.)

The Land

Land and faith in the Bible are interrelated in three essential
ways.

First, land gives us a place to worship. It affords us with a holy
place where we can draw into God’s presence and render him due
honor and praise. It provides us with a sanctuary.

In the beginning “God planted a garden” (Genesis 2:8). It was
lush with “every tree that is pleasing to the sight and good for

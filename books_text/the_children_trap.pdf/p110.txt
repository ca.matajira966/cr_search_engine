90 The Children Trap

of the power religion. People are compelled to send their children
into non-neutral schools. In our day, this has meant that Chris-
tian parents have been forced to finance the indoctrination of
most children (including their own) with a rival religion, the hu-
manist religion of evolutionism.

A war is going on that Christians have begun to recognize
only in the last two decades. This has threatened the public
schools as nothing has threatened them in American history. Yet
the majority of Christian voters still do not recognize the nature of
the religious battle, nor the moral issue of compulsory attendance.

In summary:

1. Egyptian religion was power religion.

2. They believed the state is the agency of salvation.

3. Paul called the Greeks of Athens religious.

4. Modern education is “Greek”: supposedly based on neutral
reason.

5. John Dewey wanted humanist religion in the public
schools, and only humanist religion.

6. Modern education is based on the “lowest common denom-
inator” principle. .

7. Evolution has replaced creation as the explanation of ori-
gins.

8. Evolution also has replaced the doctrine of God’s provi-
dence with the gospel of science-controlled evolution,

9. Students are taught to respect the state, the new sovereign
over our world.

10. The myth of neutrality is the legal basis of public education.

11, Neutrality is a myth.

12. Therefore, the public schools are caught in a dilemma: they
need neutrality, yet they cannot get it if everything is relative (as
they teach).

13. The public schools are at war witk Christianity.

14, The public schools have serious mora} problems as a direct
result of their humanist beliefs.

15, The humanists are losing faith in the future.

16. The Christian school movement can now offer a better edu-
cation,

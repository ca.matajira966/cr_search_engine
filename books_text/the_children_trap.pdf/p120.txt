10¢ The Children Trap

phy, teacher qualifications, and other aspects of the school.

The requirements for accreditation may appear to be very rea~
sonable and consistent with the school’s standards. The difficulty
may arise when the requirements are changed. The new stand-
ards may be totally unacceptable. Then the school must accept
them or “lose its accreditation.” In the public mind, it is a terrible
thing for a school to “lose its accreditation.” It is better never to
have had it in the first place.

Accreditation can work hardships upon a school. In Holland,
Michigan, many years ago, the Christian schools were told that
they must have shop and home economics to be accredited. The
schools wanted neither because they felt this was being handled
adequately at home. Requiring such courses can be expensive,
not to mention that time is taken away from teaching the basics.

Understand also that these schools were run by Dutch-back-
ground churches. (Holland, Michigan!) Those people were very
often farmers, Their churches were filled with skilled craftsmen.
Dutch-owned Michigan mariufacturing companies had and still
have excellent national reputations for high quality production,
These people did not resist the idea of shop courses because they
had no respect for craftsmanship. They just knew that students
could learn such skills better in the local community in
profit-sceking businesses. What students needed to learn in school
were the intellectual skills that are normally only available from
trained teachers.

Accrediting agencies might require a lower teacher-pupil ratio
than is necessary for effective teaching. This would raise the cost
of education without enhancing quality. The result would be that
some students could not afford to attend the Christian school.

A law school in Northern Virginia was established by Christians.
It had Biblical ethics as its basis. The school was refused accredi-
tation by the American Bar Association. As a result, the students
couldn’t take the bar exam. If they couldn’t take the bar exam,
they couldn’t practice law. Thus a chain of control was set up.

The reason given for not accrediting the law school was that
the American Bar Association was opposed to “free-standing” law

14 The Children Trap

ernment to repeal these laws. Civil government should be protect-
ing our property instead of confiscating it by zoning laws. Prop-
erty owners who want to control the land around them should
enter into voluntary arrangements with their neighbors. Cove-
nants made in this way will protect property owners from undesir-
able encroachments.

Fairfax County proposed an ordinance in the 1960’s requiring
churches to obtain Use Permits in order to build. Only six Chris-
tians came to the public hearing to object. This was in a county
with a population of nearly one half million.

But times are changing. Recently, Fairfax County proposed to
regulate what went on inside the church buildings. The Christian
community came out in vast numbers. Over 100 speakers signed
up. In the middle of the first speech, the county supervisors ran
up the white flag. They were ready to surrender, and did. If the
Christians had come to the initial meeting when Use Permits for
churches were legislated, the situation today would be different.

The Other Costs: Hidden and Not So Hidden

“Free” public education isn’t free. It isn’t even cheap. Not by a
long shot. For starters, there are the visible costs of operating the
schools. These costs are usually publicized and may be the only
costs the public hears about. Buildings are another cost. If the
money is borrowed, the cost of interest must be figured. Tax-
exempt interest-bearing bonds are a hidden cost, as I pointed out
previously. No taxes are paid by public schools on their vast real
estate holdings.

The money to run the schools is collected by another part of
the bureaucracy. That cost must be figured in. All money that
comes from the state must be considered. The aid from the Fed-
eral government is also a cost. There are many subsidies, such as
those for milk and school lunches, that represent a real cost to the
taxpayers. There are license fees for buses that are “free” to the
public schools.

I will not go into the social costs of public education. The
tremendous cost to the taxpayers resulting from drug and alcohol

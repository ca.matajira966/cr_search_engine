9
WHAT THE FAMILY CAN DO

After the death of Moses the servant of the Lorp, it came to
pass that the Loxp spoke to Joshua the son of Nun, Moses’ assis-
tant, saying: “Moses My servant is dead. Now therefore, arise, go
over this Jordan, you and all this pcople, to the land which I am
giving to them the children of Israel” (Joshua 1:1-2).

No man shall be able to stand before you all the days of your
life: as I was with Moses, so I will be with you. I will not leave you
nor forsake you” ( Joshua 1:5).

God wanted the Israelites to be future-oriented. They were to
go in and possess the land as He had promised. They were to be
optimistic. God promised them victory, They were to take over
completely the land of Canaan.

God has promised that the meek shall inherit the earth—the
meek before God. Christians are to occupy until. Christ returns
(Luke 19:13). To occupy means fo fake aver. Paul says that the saints
shall judge the angels (1 Corinthians 6:3). Jesus said to go into all
the world to preach the gospel and disciple the nations. He re-
quires that we teach them all the things He has commanded us
(Matthew 28:18-20). Jesus promises to be with us. We are com-
missioned to go by Him to whom all authority in heaven and on
earth has been given. We are promised victory. We are to be opti-
mistic. We are to be future-oriented. Our duty is to carry out
Christian Reconstruction, The earth is the Lord’s. We are to
claim it for Him.

In the following chapters, I want to set forth some specific

127

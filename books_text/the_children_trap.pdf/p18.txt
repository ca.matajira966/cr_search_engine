xviii The Children Trap

transfers the greatest sovereignty to parents, yet it also makes pos-
sible the division of labor.

This conclusion will upset the more vocal defenders of home
schooling, whose schools rarely can make full use of the division of
labor, especially at the high school level. It will also upset the de-
fenders of non-profit, board-operated (“parent-controlled”) schools.
Such schools are far more bureaucratic than profit-seeking
schools, since the operators do not get to keep the profits person-
ally. Such board-operated, “parent-run” schools eventually are
run by parents of adult children who have long since graduated,
not the parents of the presently enrolled students. So “parent-run
boards” are a misnomer, The only absolutely parent-run school is
a home school. There can be parent-influenced schools, but to call
a non-profit bureaucracy a parent-run school prejudices the case.

This book finally challenges the idea of church-run schools, for
these schools always seek indirect subsidies — financial or space—
from church members whose children are not enrolled, or who
have no children, or who have a rival approach to education.
These schools generally place too much responsibility on pastors,
who are not being paid to administer a school. Church-run
schools bring dissension into the church. They invariably reduce
the influence of the parents of enrolled students, to the extent of
the indirect subsidies being paid by other people.

It would be far better to have a family-owned, profit-seeking
school next door to a sponsoring church, with the church renting
the school’s facilities in the evenings and on weekends. But then
what happens if another family in the church wants to start a
school (competition for the first school), but cannot get the same
subsidy? Once again, you get avoidable dissension in the church.

The solution is freedom. The answer is free market competi-
tion. The answer is the principle of the market: something for some-
thing. The answer is: parents should pay the full costs of educating
their own children, the same way they buy food, shelter, haircuts,
Christmas presents, and just about everything else. The answer
is: no more compulsory subsidies —etther in God's name or the state’s.

Perhaps some church members really cannot afford to send

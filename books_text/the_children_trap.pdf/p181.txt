What the Civil Government Can Do 161

Your goal must be to sink the ship with the step-by-step coop-
eration of its captain, crew, and passengers. You must bore many
small holes to do this. These holes must be perceived as beneficial
to the ship. If you get thrown overboard early in the cruise, you
will not succeed in your assignment. Never lose sight of this goal.

Specific Recommendations

First, start pushing for larger classes and fewer teachers.
Nudge the board in the direction of refusing to rehire as many un-
tenured teachers as possible, As I have pointed out earlier, a
higher pupil-to-teacher ratio cannot be shown to affect student
performance. There is little or no statistical evidence showing that
lowering the student-teacher ratio will increase student perform-
ance. The idea that such a relationship exists is called into ques-
tion by two generations of shrinking classes and falling student
test scores.

The eight-grade little red schoolhouses of rural America and
the crowded classes of the New York and Boston public schools
turned farm children and non-English speaking immigrant chil-
dren into the most productive work force in history. They mas-
tered at least the Sixth McGuffey Reader. Today's high school
graduates haven’t been given equal skills.

Let me give you an example about student-teacher ratios.
Wayne Roy was for over three decades a very popular, highly suc-
cessful social studies teacher in a southern California high school
district. One year, he taught the editor of the Biblical Blueprints
Series. (His wife taught the children of California governor Duk-
mejian.) He was a fundamentalist Christian who taught creation-
ism in the classroom, as well as Biblical monogamy, and got away
with it, decade after decade. He drove liberals and humanists to
despair. They finally gave up trying to get him fired. He took early
retirement in the summer of 1986 in order to devote full time as a
European tour guide for graduating high schoolers, and to con-
tinue his work as a California real estate developer.

He once offered the following deal to the district. Let him
teach every senior the required senior problems course. Fire the

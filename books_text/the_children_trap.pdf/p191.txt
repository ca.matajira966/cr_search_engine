What the Civil Government Can Do 171

Creationism in State Schools (but not Colleges)

The other will-o-the-wisp these days is getting six-day crea-
tionism taught in the science classes. What a preposterous goal!
What a dream that absorbs our money and time! The humanists
will never allow it. They run the schools, and they will never
allow it. Equal time for creationism is against their religion. Can-
adian Professor Michael Ruse has spoken for all evolutionists,
and they control the curriculum in state school biology courses:

I believe Creationism is wrong: totally, utterly, and absolutely
wrong. I would go further. There are degrees of being wrong. The
Creationists are at the bottom of the scale. They pull every trick in
the book to justify their position, Indeed, at times, they verge right
over into the downright dishonest. Scientific Creationism is not
just wrong: it is ludicrously implausible. It is a grotesque parody of
human thought, and a downright misuse of human intelligence. In
short, to the Believer, it is an insult te God.?

Under no circumstances would I tet Creationist ideas into [tax-
financed school] biology classes, or anywhere else where they
might be taken by students as possible frameworks of belief. I
would not give Creationism equal time. I would not give it any
time}

The courts constantly uphold their monopoly over curricu-
lum. When we have enough political clout to restructure the
courts, we will also have enough clout to abolish tax-supported
education.

We are wasting our time on such suicidal school reform proj-
ects. Better to put our efforts into building Christian schools and
electing school boards that will steadily reduce the money going to
public education.

Summary
There must be no compromise with the public schools by

2. Michael Ruse, Darwinism Defended: A Guide to the Evolution Controversies
(Reading, Massachusetts: Addison-Wesley, 1982), p. 303.
3. fbid., p. 321.

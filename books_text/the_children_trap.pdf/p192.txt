172 The Children Trap

Christian parents. They must not send their children to such
schools. The first step is to pull your children out of the public
schools. All reform programs begin here.

Next, we need to cut off the funding for public schools, elec-
tion by election. Fight all increases in property taxes. Vote no on
every school bond issue. Organize others to do the same.

Next, do whatever possible to reduce political pressure against
private education. Fight every zoning commission decision
against Christian schools. Fight every decision of state or local
school authorities to register, test, or in any other way infringe on
the authority of parents or private schools.

Run for the school board. Impose a program of cutting ex-
penses. Impose a program of full accountability of school officials
and teachers to the public. Get a merit pay system running, Get a
program of teacher evaluation by students. Represent taxpayers
and students. Nobody else does.

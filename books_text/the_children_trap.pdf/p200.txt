180

dominion religion, 89

“the droolers,” 168

drug education, 87

drugs, xvii, 4, 53, 65, 75, 87, 159, 163
“Dynamic Sociology,” 85

East Berlin, 73
“eat, meet, and retreat,” 153, 157
Ecole Polytechnique, 80-81, 83
economies of scale, 44
Eden, Garden of, 76, 88, 133
education
Biblical view of, xv
church-run schools, xviii
compulsory education, xvi, 54, 66,
72, 77, 84
conditioning process, 31
control of education, 5, 22, 47
financial responsibility of the
parents, x, xii
financial responsibility of the state,
x, xiii
free education, viii, x, 47, 60
free public education, 72, 114
God-centered education, 25-26, 30,
32-33
government's legitimate role, 122
hierarchy in education, 38
home schools, xviii, 39-41, 43, 150
how children learn, 38
irresponsible parents, 64
Japanese approach to education, 56
licensure, 93-95
Memorization, 39
moral relativism, 84
non-profit, board operated schools,
xviii
parents responsible for education,
xix, 8, 26, 35-36, 95, 153
religious presuppositions, 152, 157
socialized education, 37
state-supported compulsory
education, xvii

The Children Trap

taxpayer-financed education, xi, 152
voluntary education, 62
world-and-life view, 64

Educational Research Associates, 162

Egypt, xiv-xv, 6, 92, 155

Egyptian power religion, see power
religion, Egyptian

Egyptian(s), 79

Einstein, Albert, 102

eminent domain, 16, 113

Engels, Frederick, 17

environment, 31, 84

environment of the public schools, xiv

Epicureans, 81

Essenes, 14

established church in America, new,
33, 66, 159

established churches, xv-xvii

established religion in America, new,
33, 66, 82

evangelism, 129

Eve, ix, xi, 36, 84, 133

evolution, ix, xiii, 29, 82-83, 171

excommunication, 87

Exodus, Book of, 89

“facts,” 33

facts, interpretation of, ix, 19

Fairfax Christian School, 40

Fairfax County, 110-12, 14

Fairfax County Public Schools, 3, 57,
7

Fall of Man, 12, 84

the family, 35

Federal Register, 109

Federat Reserve Bank, 17

“Ferris Buellers Day Off,” 33

financial audit, total, 164

financial responsibility of the parents,
x, xiii

Flesch, Rudolph, 72

forty-hour week, 137

founding fathers, 50

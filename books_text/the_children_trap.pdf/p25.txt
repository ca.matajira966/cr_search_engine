Introduction 5

The fault lies with the system, no matter how much they try to
put the blame elsewhere. Jacques Barzun, who spent 48 years at
Columbia, states in his Teacher in America, “The once proud and
efficient public-school system of the United States—especially its
unique free high school for all—has turned into a wasteland
where violence and vice share the time with ignorance and idle-
ness, besides serving as battleground for vested interests, social,
political, and economic.”

To Light a Candle

God tells us in the Bible that we will reap what we sow. I do
not need to belabor the point that the public schools in America
are bad, and getting worse. They are getting plenty of criticism
from within the system. I am not surprised that public school
teachers frequently send their own children to Christian schools.

Tam not going to write another book bashing the public school
system, Nor am I interested in reforming the public schools. That
is a waste of time. I decided a long time ago that it is better to light
one little candle than to curse the darkness.

My interest is in the establishment of Christian schools
throughout the world and the eventual abolition of all public
schools, This is based on the conviction that the enty solution to
the moral and academic crises in education is a system of educa-
tion based on the Word of God. “Unless the Lorp builds the
house, They labor in vain who build it.” (Psalm 127:1)

The most important issue in education today involves the
question of control. The question of control centers on the matter
of responsibility, Just who is responsible for education?

The other issue has to do with the content of education. What
is the purpose of education? How does a Christian education
differ from a public school education?

Humanists Have Always Been In Control
“The teachers are in charge of the classrooms, the students

control the restrooms, and the halls are a no-man’s land.” That is
the way a public school board member in a wealthy suburban

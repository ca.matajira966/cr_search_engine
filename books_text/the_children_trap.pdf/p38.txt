18 The Children Trap

anarchists who call for the destruction of the state and who wor-
ship the power of the free market or the power of voluntary com-
munes. Theirs is a power religion, just like the religions of the
ancient pagan world. The state is the most powerful institution in
the humanists’ worldview, so it is their god. Public school teach-
ers, not parents, are its lawfully appointed trustees.

Even though their answer was wrong, they got their way.
They set the agenda for education. Their wrong answer was more
powerful than no answer at all. Their answer won by default.
This should remind us of the old political truth: “You can’t fight
something with nothing.”

The Nature of the Fight

“For I am not ashamed of the Gospel of Christ, for it is the
power of God to salvation for everyone who believes, for the Jew
first and also for the Greek” (Romans 1:16).

Christian schools make no pretense of “neutrality.” They are
not ashamed of Jesus Christ, nor afraid to speak up for His cause.
Jesus Christ is the Author and Finisher of our faith. He is the be-
ginning and the end. He is the fountain of wisdom. He is the mas-
ter teacher of all time. In Him are hid all the treasures of wisdom
and of knowledge. He is the truth. He is the Saviour of the world.
He is God. He is Lord over all.

The goal of the Christian school is clear. These schools exist to
glorify God.

The Bible is the textbook for the Christian school. It is the
only infallible Word of God. It is the basis for everything. All
Scripture is inspired of God. “Inspired” means “God-breathed”
(2 Timothy 3:16). Every word of the Bible is from God. We call
this the verbal inspiration of the Bible. Ministers may err, churches
may err, but God’s Word is inerrant. It has no errors in it of any
kind—no theological errors, no scientific errors, and no historical
errors.

The Bible is the starting point and foundation for every area of
knowledge. It is also the final court of intellectual appeal. The
world about us is the creation of God. We can not understand that

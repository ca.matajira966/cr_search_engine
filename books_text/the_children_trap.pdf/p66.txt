416 The Children Trap

less wealthy people can take advantage of the division of labor and

the tutorial system.

‘We must be very wary of cult-like attitudes in Christian edu-
cation, the view that “our way is the only way.” There are many
sorts of children, many sorts of skills, and many sorts of ways to
impart skills to children. But eventually, they all involve discipline

and structure.
In summary:

1, God assigns to parents the responsibility of instructing chil-
dren.
. The Bible is God’s word.
. Education is God-centered.
. Education is the parents’ responsibility.
. Christians say they oppose the welfare state.
. Yet Christians support the welfare state’s education system.
. When we send our children to a taxpayer-financed school,
we are accepting tax-financed welfare.

8. Karl Marx recommended public education as a means to
weaken family responsibility.

9. Children learn by example.

10. They learn a Jot by watching parents.

11. Home schools take advantage of this aspect of children’s
learning processes.

12, Children should be receiving disciplined, structured educa-
tion by age 5.

13. Parents should make use of the division of labor principle
when educating their children.

14, In most cases, this involves sending children to a Christian
school; the debate is over just when.

15. The answer is: When the parents no longer have the time
and skills to compete with professional teachers in a local Christian
school.

16. When a gifted home school teacher accepts her first pupil from
outside her family, she is no longer involved in home schooling.

17. We should avoid cult-like attitudes regarding education.

NAURON

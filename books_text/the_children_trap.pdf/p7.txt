EDITOR’S INTRODUCTION
by Gary North

Meeting with a group of Christians in Austin [Texas] on May
19, 1986, [Attorney General James] Mattox revealed his true colors
when asked, “Es it (rue that the State of Téxas owns our children?” Mattox
retorted, “Yes, it is true... and not only your children, but you, too!”)

Hard to believe, isn’t it? Yet this is an ancient view in the
United States. John Swett served as California’s Superintendent
of Public Instruction from 1863 to 1868. In 1864, he published the
First Biennial Report of his office. He cited favorably several judicial
decisions in eastern states. Among them was this:

Parents have no remedy as against the teacher — As a general thing,
the only persons who have.a legal right to give orders to the teacher,
are his employers— namely, the committee in some States, and in
others the Director of Trustees. If his conduct is approved of by his
employees, the parents have no remedy as against him or them. . . .?

“On March 28, 1874, the California Legislature made it a
penal offense for parents to send their children to private schools
without the consent of the local state school trustees.”3

How did Christian parents allow themselves to be swept into
supporting the public school movement? Why do they still con-
tinue to support it financially and verbally? Why haven’t they
risen up in political revenge against anyone who proposes tax fin-
ancing of education, a public school monopoly, or state-imposed

1. Texans Against Compramise in Education (July 18, 1986).

2. Cited by R. J. Rushdoony, The Messianic Character of American Education
(Nutley, New Jersey: Craig Press, 1963), p. 81.

3. Idem.

vii

Editor's Introduction ix

The Myth of Neutrality

What has been the most successful lie in the history of Christi-
anity? There can be no doubt: the myth of neutrality, This myth has
kept Christians for two thousand years from developing explicitly
and exclusively Biblical solutions to their problems, They have re-
turned, generation after generation, to Greek, Roman, and mod-
ern philosophy and institutions, on the assumption that God and
Satan, good and evil, Christians and non-Christians share certain
fundamental beliefs, or at least share certain views of the world
around them, But they don’t. Every fact is an interpreted fact. Satan
interprets the world in his way, while God interprets it in another.
Satan sees this world as rightfully his, not God’s; God sees it as
rightfully his, not Satan’s. There can be no reconciliation between
these two views, All attempts at reconciling them necessarily lead
to Satan’s view: that God has no right to tell us how to interpret
His world,

There is no neutrality. There may be indifference. A person
may not care which athletic team wins a game, but he cannot be
neutral about whose creation has made possible the game. Neu-
trality is the devil’s most successful myth. He used it on Eve. He
persuaded her to become a neutral experimenter. She could test
God’s word, to see whether or not she would die on the day she ate
the fruit. “Just a neutral scientific experiment,” he implied. “What
have you got to lose?” The answer: everything.

The modern institution that is most self-consciously built in
terms of the myth of neutrality is the-“public” school, meaning the
government school, meaning the taxpayer-financed school. Its legal
foundation is the myth of neutrality. No religious or sectarian
views are supposed to be taught in a public school, because people
of many different religious beliefs are required by law to support it
financially. Expenditures of tax money are supposed to be neu-
tral, non-religious expenditures.

All this talk of neutral education is sheer nonsense. You can-
not teach without ultimate concepts of true and false. Label one
idea false—the evolution of the universe out of random matter

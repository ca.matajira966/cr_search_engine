Taxes 97

home. They have used money left after paying in-
come taxes. Sales taxes have been paid on the mate-
rials used in building their houses. Payroll taxes
were paid on the labor. Now the house is paid off.

Or is it? The tax collector ups the assessment.
The property taxes are raised. If they are not paid,
the sheriff sells the house, Many people find that
their real estate taxes exceed what their house
payments once were. They do not really own their
houses,

As a result of this tax, many elderly must sell and
move away.

Most real estate taxes are paid by the banks out
of escrow funds. Taxes and insurance are added to
principal and interest payments each month. The
homeowner does not get hit with one big tax bill
once a year.

This feature of taxes is built ‘in by government.
The withholding of Federal income taxes was started
as a temporary measure during World War II. (By
the way, there is no such thing as a “temporary” tax.)
If taxpayers had to shel] it all out at one time, we
would have a tax revolt on our hands. The Social
Security tax is collected by the withholding method
also.. The end result is that workers talk about “take
home” pay. They don’t miss the money they never
jay hands on. The politicians are smart when it
comes to methods of extracting the most with the
least amount of pain.

Property taxes are the mainstay of the. counties
and cities. Being a local tax, there is more likelihood
of controlling it. Local. decisions are always pre-

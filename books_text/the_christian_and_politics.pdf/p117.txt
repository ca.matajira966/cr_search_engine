LAND USE Laws 105,

laughed out of the county.

I pointed out to the bureaucrats that their own
building which housed the County Courthouse and
was located in the middle of the city, didn’t have
sidewalks. One had to jump down an embankment
to walk on one side of it. Needless to say, I got my
way.

Tt took months for us to obtain site plan approval
so we could get our building permit. We constructed
the building in 60 working days. 1 spent so much
time at the Courthouse obtaining permits that one of
our friends thought I had a part-time job there.

When the building was completed we were told
we could not use it until we got an occupancy per-
mit. We could not get the occupancy permit until
some endwalls were constructed at the culvert under
the driveway. The endwalls had nothing to do with
anyone’s health, safety, or welfare. (My neighbor
downstream managed very well with a 12 inch
culvert. My engineer calculated that we. ought to
have a 24 inch culvert. He made it 36 inches just for
good measure. The county bureaucrats upped. it to
42 inches. Guess what? Beginning right at 42 inches
you have to put in these fancy endwalls.)

I did them one better. We put in an old
smokestack that is 44 inches in diameter and I guar-
antee it will be there long after the bureaucrats and ]
are gone,

It took a month or two to get the endwalls in.
Meanwhile it was September and we needed the
building to house all the students who were clamor-
ing to get out of the wonderful Fairfax County

108 THE CHRISTIAN AND POLITICS

contractual ‘arrangements which we voluntarily
make.

The fact that we object to statist planning does
not mean that we oppose planning. .Adam was
placed in the Garden of Eden to dress it and keep it.
We ought to plan carefully when we are constructing
a school, a church, a place of business, a house, or
whatever else we believe God wants us to build.

We have to consider safety, health, and other fac-
tors. Our septic fields are not to infrmge on our
neighbor's property rights. The water we offer the
public should be safe to drink. Civil goverment
through the court system has a responsibility to ad-
judicate disputes that arise..If we injure others or
damage their property, the Bible requires that we
make restitution,

This is‘a far cry from the unrealistic demands
made by many planners today. In operating a school
I have always felt that we should do our job so well
that civil government would have no excuse to in-
trude,

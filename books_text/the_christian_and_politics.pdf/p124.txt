112 THE CHRISTIAN AND POLITICS

God warned the Israelites not to compromise
with evil nations. On September 1, 1983, the Soviet
Union shot down an unarmed Korean passenger
plane carrying 269 men, women, and children, in-
cluding Congressman Lawrence P. McDonald, an
arch foe of Communism. President Reagan talked
tough, but did nothing of any significance. Congress
passed a resolution condemning the Soviets. The
Soviets said they would do it again under the same
circumstances. We are back to business as usual with
the Soviets.

We need a strategy of victory over Communism.
Liberals in America are soft on Communism. They
constantly harp on the sins of anti-Communists and
overlook the greater sins of the Communists.

Weapons are needed to defend ourselves against
the Communists, Weapons without a strategy of vic-
tory are of no value, We can not defeat Communism
by continuing to support it.

Communism can not be stopped by promoting
socialism. Much aid given to governments of other
countries has been. used to build government owned
industries. The economies of those countries have
been weakened as a result. Our country has been
weakened as well.

FOREIGN AID There is nothing in the Constitution
that authorizes Congress to give away money to
other countries. Our country is being drained of its
resources, The aid given has not brought about
beneficial results.

Huge loans have been made by large banks in

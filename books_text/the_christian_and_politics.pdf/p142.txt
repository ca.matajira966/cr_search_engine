130 THE CHRISTIAN AND POLITICS

have ta get a lot of bills passed. Killing bad bills
could be more important than passing new ones. It
was good advice.

I introduced very few bills. I introduced legisla-
tion and worked in areas that I felt had substance to
them. Virginia was one of the states that had not
passed the Equal Rights Amendment, Only a hand-
ful of states were needed to add it to the Constitution.

I knew the passage of ERA would have. far-
reaching implications for America so I made its
defeat a primary goal. The proponents of ERA
claimed that polls showed 57% of the people of
Virginia wanted the amendment. The. women’s lib
groups contended that a small group (the “Dirty
Dozen” they were dubbed) of men in the House
Privileges and Elections Committee were thwarting
the will of the people of Virginia. They were con-
stantly reminding the public through letters to the
editor and in other ways that this was the case.

My staff and I decided to call their bluff. Bob
Beers, who was one of my legislative aides, sug-
gested that I introduce a bill to provide an Advisory
Referendum on the ERA, It had to be of an advisory
nature because the U.S. Constitution stipulates that
only the state legislaturcs can pass on a Constitu-
tional amendment.

We wanted to give the people of Virginia an op-
portunity to vole on whether we should pass the
ERA, We knew that the ERA had been voted down
in every state where it had appeared on the ballot,
including liberal New York.

My bill went to the Privileges and Elections

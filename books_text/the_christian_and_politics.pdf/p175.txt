RUNNING A CAMPAIGN 163

working together in the political area, we could ac-
complish what we could not do in smaller groups.

THE MEDIA The media presents a real problem for
conservative candidates because it is usually so biased.
The campaign schools offer some good information
on how to deal with the media.

Don’t have a blind trust in reporters. P've had
plenty of them act really friendly and then write ter-
rible articles. They are experts at twisting words to
make you look bad.

The problem isn’t always the reporters. One
reporter interviewed me and wrote an article. Before
the paper came out, he called to warn me that he
would be quoting me as saying things that I never
said. “My editor changed the story,” he explained.
Later he was to quit the paper because he found he
could not get promoted unless he went along with
the paper’s policy.

Many will remember Janet Cooke of The
Washington Post, who won a top prize about a child on
drugs in Washington, D.C. It turned out to be a
completely fabricated story.

After the Janet Cooke affair, The Washington Post
carried an article quoting a freshman at George
Mason University by: the name of Sally McKenna.
George McGovern came to speak at the university
and Sally, a member of the Moral Majority and a
fundamentalist, said some very unkind things about
him. She hoped his false teeth would fall out, and
assured the reporter that Jesus would take care of
McGovern when He returned. Sally also called

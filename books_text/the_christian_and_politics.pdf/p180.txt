168 THE CHRISTIAN AND POLITICS

candidate for political office. It is not to lose sight of
its mission in the world.

This does not mean the church as such should
not influence civil government, including the legisla-
tive process. There are many things the church can
and properly should do,

I know a church that was not even willing to an-
nounce that an election was forthcoming. I see no
reason why a minister should refrain from urging
church members to get out to vote.

A minister should preach the whole counsel of
God. That will inchide all that the Bible teaches
about civil government, education, penalties for
crime, taxation, economics, abortion, the role of the
family, etc.

The minister who faithfully preaches the Word of
God and shows its relevance to all of life will be lay-
ing a moral foundation for godly government in
every area, including the area of civil-government.
This is the most important. contribution a minister
can make to politics. Without strong preaching in
the churches, we aren’t going to have a proper politi-
cal system no matter how skilled we are at organiza-
tion or how hard we try to get people elected.

I would never want to see churches sidetracked
from their duty under God—to proclaim His Word
faithfully to all men.

ENORMOUS POTENTIAL It has been estimated
that there. are at least 50,000,000 born again Chris-
tians in the United States. These are people who
claim to believe the Bible from cover to cover. If

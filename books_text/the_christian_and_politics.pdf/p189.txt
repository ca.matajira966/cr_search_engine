RECOMMENDED READING List 177

Organizations to Help on Political Campaigns

The Conservative Caucus, 450 Maple Avenue East,
Vienna, Virginia 22180. The Conservative Caucus is
an educational and lobbying organization headed by
Howard Phillips. Very good on showing how Con-
gressmen vote,

The Committee for the Survival of a Free Congress, 721
Second Street NE, Washington, D.C. 20002. The Com-
mittee for the Survival of a Free Congress is headed
by Paul Weyrich. Holds excellent campaign schools.
These schools deal with every aspect of campaign-

ing.

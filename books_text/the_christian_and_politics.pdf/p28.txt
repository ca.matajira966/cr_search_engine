16 THE CHRISTIAN AND POLITICS

inside could add a lot more that isn’t generally
known.

Yes, I have met politicians whom I would
hesitate to buy a used car from. But then [ve met
some used car salesmen whom I would not buy a
used car from. And I’ve met some who could not sell
me a new car for that matter. I would even hesitate
to sell them a new car for fear I might get skinned.
And, yes, I know some farmers who couldn’t sell me
a used horse, and some dentists who are never going
to get an opportunity to pull my teeth. I even know
some “Christians” whom I wouldn’t trust with my
dry cleaning, let alone my soul.

The point is that yes there is a sense in which
politics is dirty. There is power there and the corrupt
will seek power. There is money there and some will
seek money. Campaigns can get rather nasty. A lot
of mud gets thrown around. Politics can get dirty.
Elections can be stolen. Dead people have been
known to vote. One candidate said he was going to
distribute his literature in the cemetery next time
because he lost the election there before.

There is dirt everywhere, not just in politics.
There is dirt because there is sin. There are dishon-
est businessmen, dishonest clergymen, and dishon-
est laymen. I don’t think politicians are any better or
worse than the general public. Sometimes we get
better men than we deserve in public office. I know
used car salesmen who are honest and I know elected
officials who are conscientious and trustworthy.

The public has a low view of politicians and car
salesmen. A poll was, taken which showed that

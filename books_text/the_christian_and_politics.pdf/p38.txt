26 THE CHRISTIAN-AND POLITICS

THE ATTACK ON THE CHRISTIAN SCHOOLS It is
within the Christian school movement that the ma-
jor attack is being mounted against Christians. I
think there is a reason-for this. The Christian school
movement represents a major defeat for the human-
ists. The public schools (or government schools, as I
prefer to call them). are thé chief purveyors of hu-
manism in America. Secular humanism is the new.
established religion in America, supported by
confiscating the wealth of the citizens by taxation.
The government schools are the new church ‘where
the priests and priestesses of this new religion work
to indoctrinate the children.

Until the Christian school movement, the hu-
manists in civil government knew they could tolerate
the churches, at least for the time being. With the
children forced into the humanistic government
schools and the people forced to pay for them, the
humanists felt secure about the outcome. When you
can teach a child six or eight hours a day, five days a
week for nearly the whole year, why worry if they get
an hour or two on Sunday in some church, especially
if the church isn’t relating the faith to the child’s
world anyway. The humanists knew they would win
if they were just patient, Everything was going their.
way.

Then came the Christian school movement.
Government school enrollments began to decline.
There were many reasons. The humanists had been
promoting abortion and the myth that the world was
being overpopulated. So there just weren’t as many
children around to attend the schools. With the

“17'S A MATTER OF SURVIVAL" 27

Christians pulling their children out of the govern-
ment schools right and left, the humanists really got
frightened, The teacher unions could count noses
(even if some of their students couldn’t add and sub-
tract anymore) and all they could see were declining
enroliments. Declining enrollments mean closing
schools and losing jobs, The teachers began to worry
about job security. Having alternative Christian
schools around was a problem for the humanists in
other ways. Parents now had a choice as to whether
they would patronize the government schools or
send their children to a Christian school. The Chris-
tian schools meant an opportunity for parents to
compare the quality and the cost. With Christian
schools operating at costs far below that of the gov-
ernment schools and producing superior results
academically and otherwise, thé humanists began to
panic. The Christian school movement had to be
stopped.

I started Fairfax Christian School 22 years ago
and I could write a book just about the problems we
encountered with the county. I don’t think they were
trying to give us a hard time. It was all part of the
bureaucratic maze that has grown up at every level of
government. At one point I even had to write a letter
to myself in order to get a site plan approved prior to
obtaining a building permit. I owned a piece of land
on which [ built in 1964. The following year I pur-
chased an adjoining parcel on which to build again. I
needed to drain water froni the second site onto the
first piece of land I owned. The county informed me
that I had to have permission from the owner of the

 

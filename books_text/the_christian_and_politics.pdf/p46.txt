34 SHE CHRISTIAN AND POLITICS

the family.

Marx advocated the graduated income tax. This
was adopted by the United States at the same time as
the Federal Reserve System was set up. The gradu-
ated income tax was sold to the American people as a
soak the rich proposal. Only a handful were initially
affected by the tax, It was proposed at the time that a
five percent limit be placed on the income tax. This
was not done because there was fear that this would
be a reason for raising it that high! The rates start at
14% now.

The top rate of taxation is now only 50%. It was
once over 90%. What was started as a scheme to
soak the rich has come back to haunt the middle
class and the poor. Practically everyone pays income
taxes. The middle class bears the burden. Along
with property taxes, employment taxes (including
Social Security), sales. taxes, and a host of other
taxes, the income tax is taking nearly half the wealth
of the average family each year. This makes it more
difficult for the father to pravide for his family. The
family becomes ever more dependent on the state.

The inheritance tax was also advocated by Karl
Marx and duly adopted by humanists and. unthink-
ing Christians in the United States. The inheritance
tax is based on the idea that the state should inherit
the wealth of the family. Many family businesses had
to be liquidated because inheritance taxes had to be
paid.

The Israelites in the time of Samuel wanted a
king so they could be like the other nations. They
wanted a strong centralized government instead of

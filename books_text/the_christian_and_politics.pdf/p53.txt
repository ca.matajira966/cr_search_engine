IT'S ALSO A MATTER OF vicToRY 41

God's kingdom is like that. Christians are the
leaven in the world. Leaven works quietly. It works
by contagion, affecting that which is next to it, That
becomes leaven and in turn affects the meal further.
Eventually the whole is leavened. As Christians
work in the world they influence the world. God pro-
mises growth in His kingdom. There is no sugges-
tion that there will be defeat or merely maintaining
things as they are.

Jesus said to His disciples, “Ye are the light of the
world. A city that is set on an hill cannot be hid.” In
the same context Jesus said that Christians are to be
the salt of the earth. We are to be seen by the world
and we are to influence the world.

THE EARTH BELONGS TO GOD The earth does
not belong to the devil. The devil is a usurper. “The
earth is the Lord’s and the fullness thereof, the world
and they that dwell therein.”

The German philosopher Kant wanted to leave
God out of the universe. He taught that God, free-
dom, and immortality were part of the noumenal
world. Kant believed that man lived in the phenom-
enal world, God had nothing to do with the phenom-
enal world. Kant wanted to make God unimportant
and out of touch with the world.

The non-believing world, following Kant, wants
to keep Christians and Christianity confined to a
small and what they.consider to be, an unimportant
role on the earth. They think Christians should be
put in their place. The Christian is looked upon as a
second-class citizen. The Christian is to be allowed

THE ROLE OF CIVIL GOVERNMENT 53

ernment. [t seems with the current political parties it
is just a matter of degree. Limitations on govern-
ment need to be carried out according to Biblical
law. Otherwise government never gets limited. The
other problem is that in some areas the government
is not doing enough. I[t is too limited. For example,
courts have placed restrictions on prosecutors to the
point where it is difficult to punish criminals. Along
this line it is fashionable to quote Lord Acton’s fa-
mous dictum, “Power corrupts, but absolute power
corrupts absolutely.” The difficulty with this state-
ment is that it is a half-truth and half-truths can be
dangerous. God has absolute power, but He is not
absolutely corrupted. He.is not corrupted at all. God
gives man power. A father has power to rule over his
wife and children. That power does not corrupt in
and of itself. It is corrupt only if wrongfully used.
God gives the church officers power. It can be used
in a proper way or an evil way.

God gives civil rulers power. That power can be
used to punish evildoers or it can be used to oppress
the godly. We need the godly exercise of power in the
family, the church, and the state. It is not power as
such that we fear, but ungodly power. George
Washington observed that government is like fire.
When properly contained it can be a faithful ser-
vant. When not contained it becomes a fearful
master. Civil government has become a fearful
master not because government is in and of itself
bad, but because we have moved away from the
Biblical world-and-life view in regards to it.

One of our national political leaders of an earlier

‘THE ROLE OF CIVIL GOVERNMENT 55

is ultimate diversity. God is One. God is Three. The
One is not more important than the Many and the
Many is not more important than the One.

Because the Christian believes in the Triune
God, he can devclop a consistently Biblical approach
to civil government and to society. Unitarians do not
believe in the Trinity. They think oneness or unity is
ullimaie. Oneness is important. This is why
Unitarian theology results in totalitarian politics.
The Unitarians sacrifice the individual to the group.
‘That is why Unitarians are so liberal in politics. ‘The
Christian believes in the Trinity. He believes in the
Deity of Jesus Christ. Jesus Christ is God with us.
He, not the state, is God walking on earth. He is the
incarnation of God. The Christian worships and
serves God. He does not worship the state. With this
in mind, he develops from the Bible the correct idea
of civil government.

@0 THE CHRISTIAN AND POLITICS

length of time.

Why has the system failed? Because the religious
ideas behind the prison system are false. “Peniten-
tiary’” is a religious term, It is amusing that human-
ists want to keep religion out of the schools, but they
have it in the penal system. Even the word “cell”
comes from religion. The penitentiary system was
introduced by the Quakers in Pennsylvania: They
believed in salvation by the inner light. Their idea
was that lawbreakers should be put in a cell where
they could meditate on their evil deeds and become
penitent:

The penitentiary system confuses the role of the
church and, the-state. The church is to be a ministry
of. grace. Through the proclamation of the gospel
man is led to repentance. Character is corrected or
reformed through regeneration by the Holy Spirit
and adherence to the law of God. The state is to be a
ministry of justice, to punish evildoers, not to reform
them. As we. shall see, a Biblical approach to
penology establishes the basis for reform.

Liberal theology has influenced the current sys-
term of penology in another way. Liberals hold that
the state is god. They believe in the universal
fatherhood of God. So the relationship between man
and the state changes. Man is no longer a citizen of
the state, but a child of the state. Paternalism
develops. Since the offender is considered a child of
the state, punishment becomes remedial instead of
retributive. An excellent discussion of this concept is
found in Robert Webb’s Reformed Doctrine of Adoption,
a book written about a century ago.

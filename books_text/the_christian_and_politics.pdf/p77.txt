CRIME AND PUNISHMENT 65

The debt is not paid until the victim has been com-
pensated.

This is why the penitentiary system docs not
result in the reform or correction of the criminal. He
pays a debt to society, but his debt to his victim re-
mains unpaid.

As Christians we need to work to change the sys-
tem of punishment so that it conforms to God’s law.
The specifics are given in the Bible. Case law is
found in the Bible. This provides the basis for mak-
ing decisions that are Biblical. For ¢xamples of laws
on restitution, let us look at Exodus 22. If a man
stole a sheep he had to réstore fourfold to his victim.
If he stole an ox he had to restore fivefold.

Tf our laws required restitution, several benefits
would be forthcoming. We could eliminate peniten-
tiaries at great savings to the taxpayers. The victim
would be compensated for his losses. The offender
would learn that crime docs not pay. (Currently it all
too often does as far as this world is concerned.) The
offender would be forgiven and restored because he
would have made restitution.

BIBLICAL SAFEGUARDS Some worry about capi-
ial punishment because a mistake might be made
and an innocent person be put to death. Man is falli-
ble and a mistake could be made. But this can be
used against any kind of punishment. Innocent per-
sons have been put into prison.

While human justice will not be perfect, the
Bible provides safeguards. Cities of refuge were es-
tablished in Biblical times. Persons who acciden-

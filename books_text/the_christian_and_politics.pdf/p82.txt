70 THE CHRISTIAN AND POLITICS

granted in this country. They are another example of
government interference with the right to work. The
person who is not worth the minimum wage gets no
work at all.

Wage and price controls are another way of in-
terfering with work. Governments have no business
setting floors on wages and prices nor putting ceil-
ings on them. The inevitable result of such controls
is to create artificial shortages and to divert capital
into fess productive areas of the economy,

THE SABBATH AND GRACE Man is to work six
days. He is to rest-one day each week. This reminds
him that he does not. save himself by his labor. He is
saved only by Ged’s grace.

Laws that protect man’s Sabbath rest are good.
The Sabbath has other implications for economics as
well. Every seventh year all debts were cancelled.
Slaves were set free. After seven sevens (49 years)
the Jubilee was held.

The words on the Liberty Bell, “Proclaim liberty
througheut all the land. . . .” are from Leviticus.25
and deal with the Jubilee. The Jubilee was proclaimed
on the Day of Atonement. The Day .of Atonement
was the appointed day for the lamb to be sacrificed
and the blood sprinkled in the Holy of Holies. This
typified the death of Jesus Christ to free man from
his sin.

God did not want His people to live in slavery:
Debts were cancelled in the seventh year because
debt is a form of slavery. A person went into debt
only in case of an emergency. He was not. to be

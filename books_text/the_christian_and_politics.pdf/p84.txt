72 THE CHRISTIAN AND POLITICS

were all farmers we could not have dominion over
the earth to the extent that we do.

There is also geographical division of labor. It is
better to grow oranges in Florida and potatoes in
Maine. Some products can be cheaper in one part of
the country than another, And some products can be
produced more efficiently in some parts of the world
than in other parts.

When tariffs or other. restrictions on trade are
sanctioned by government, we have a repudiation of
the division of labor principle. Trading does not
benefit one party at the expense of another. People
trade with each other because the other person has
something which is considered. more valuable to the ©
person wanting it. Both parties benefit by trading.
Otherwise they wouldn’t do it.

The Constitution provided that there would be
no trade barriers among the states. This has helped
bring material prosperity to. America. By the same
token, elimination of trade barriers among nations
would improve the standard of living for everyone.
‘This.does not mean we should sell materials to coun-
tries that intend to destroy us. I would not sell a man
a gun if I thought he was going to shoot me.

TANSTAAFL Perhaps you thought this heading was
a misprint. It isn’t. TANSTAAFL means “There
ain’t no such thing as a free lunch.” That is a lesson
in economics in nine words.

The Bible teaches that we should not expect to
get. something for nothing. Humanists would like us
to believe that we can get free education, free

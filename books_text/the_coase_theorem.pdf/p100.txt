82 THE COASE THEOREM

damage his neighbor’s property. He does not discuss anywhere in
the essay the economic costs to society of compromising the injured
party's right to demand and receive by law economic restitution from the
offending party.

Coase does not even seem to understand the implications of
his own argument. Most astounding of all, his arguments have
been taken seriously by economists who see themselves as de-
fenders of the free market order. Economic reductionism is a
very real threat. The more rigorous the logic, the more the
threat to real-world policy-making, if this rigor is purchased by
the surrender of private property rights, let alone justice.

Transaction Costs at the O.K. Corral

Coase’s academic colleague at the Universily of Chicago,
Nobel Prize-winning economist George Stigler, has extended
the Coase theorem. Coase argues that in the absence of transac-
tion costs, different initial assignments of property rights will
lead to the same economic output. In his authoritative text-
book, The Theory of Price, Stigler takes this thesis one step far-
ther. He concludes that if there is perfect competition, meaning
perfect foreknowledge, market transactions between the pollut-
er and his victim will lead to the production of exactly the same
economic output as would have been produced if one firm had
owned both the source of pollution and its sink.® In other
words, the rights of private ownership — the legal right to ex-
clude ~ and the sense of outrage at an invasion of one’s prop-
erty are economically irrelevant. In a world of perfect competi-
tion, amazing things happen. The economic significance of the
thefi volved in polluting a neighbor’s environment is zero.*"

50. George Stigler, The Theory of Price (3rd ed., New York: Macmillan, 1966), p.
113.

51, Incomplete agreement is Warren G. Nutter, “The Coase Theorem on Social
Cost: A Footnote,” fournal of Law & Economics, XI (Oct. 1968).

The Crisis: Living With Dialectical Schizophrenia 89

of hypothetical scientific neutrality, complex formulas, mathe-
matics, and (usually) taxpayer-financed tenure.

The assumption of ethical neutrality is the essence of what
we might call “economic formalism.” Pro-free market econo-
mists continually appeal to efficiency apart from equity. In this
respect, Coase is representative of the entire profession. How
can we maximize value, they ask, questions of equity apart? This is
the perhaps the major problem that pro-free market defenders
have: overcoming the objections of socialists and other critics of
the free market, who point to questions of equity and fairness
as the crucial ones, rather than questions of efficiency.

The collapse of the Communist economies has, at least for
the present, silenced the socialists. Questions regarding cco-
nomic efficiency could not be avoided forever; the social costs
of socialist economic planning kept rising, even though social
costs cannot be measured scientifically according to the epistem-
ology of methodological individualism. But this increase in
economic awareness regarding the irrationality of socialism is
not the same as a scientific refutation of socialist economic
theory. The retreat of the socialists afler 1988 was a paradigm
shift based overwhelmingly on the public admission by Soviet
leaders that the Communist system was not working according
to plan, especially five-year plans. It was not the logic of Mises
in his 1920 essay on the irrationality of socialist economic calcu-
lation that persuaded the socialists;® it was Gorbachev’s new
Party line in 1988. He needed Western credits, and he was
willing to admit the economic failure of Communism in order
to get them.

Until 1989, the free market’s academic defenders generally
failed to convince the socialists and ethicists that the benefits of
economic efficiency are greater than the social and personal

6. Mises, “Economic Calculation in the Socialist Commonwealth” (1920), in FA.
Hayek (ed.), Collectivist Economic Planning (London: Routledge & Kegan Paul, [1935]
1963}, ch. 3.

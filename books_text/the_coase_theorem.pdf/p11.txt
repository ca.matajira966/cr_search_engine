Preface xi

individualism — or his logic. It was based on his rejection of the
inescapable implication of Robbins’ postulate: the removal of all
scientific content from policy-making. Harrod insisted that Rob-
bins’ conclusion was professionally unacceptable, not illogical.
Harrod was reviving the old dilemma raised by Jeremy
Bentham: the aggregating of pleasure and pain in a world of
hypothetically autonomous men. Bentham rejected any “anar-
chical” assumption that there are “as many standards of right
and wrong as there are men,” but on what basis was Bentham’s
rejection valid? As Halévy asked so many years ago: “But why
is it necessary that a science of social man, based on a quantita-
tive comparison of pleasures and pains, should be possible?” He
pointed to the underlying flaw in Bentham’s position: “But why
does not the principle of utility enter, in the last analysis, into
the class of ‘anarchical’ principles? Wherein does the notion of
happiness, or of pleasure, necessarily imply, to use Bentham’s
expression, ‘dimensions’? Can present pleasure be compared
with past pleasure, which, by definition, no longer exists, or
with future pleasure, which, by definition, does not yet exist?
Can the pleasure experienced by one individual be compared
with the pleasure of another individual?” He cited Bentham’s
unpublished fragment, “Dimensions of Happiness”:

’Tis in vain to talk of adding quantities which after the addition
will continue distinct as they were before, one man’s happiness
will never be another man’s happiness: a gain to one man is no
gain to another: you might as well pretend to add twenty apples
to twenty pears, which after you had done that could not be
forty of any one thing but twenty of each just as there was be-
fore

7. Elie Halévy, The Growth of Philosophical Radicalism, translated by Mary Morris
(1928) (Boston: Beacon, [1901-4] 1966), p. 495.

8. Bentham, “Dimension of Happiness,” University College manuscripts; cited by
Halévy, idem.

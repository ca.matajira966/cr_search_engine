The Crisis: Living With Dialectical Schizophrenia 93

definition in the late 1980's that describes the situation in Eu-
rope: “Socialist, noun: a capitalist who, for political reasons,
cannot admit it publicly.” Nevertheless, economic pragmatism
is not sufficient to serve as the foundation for an entire civiliza-
tion. Envy still has a large political constituency.'’? There is a
desperate need today for a moral and ultimately religious de-
fense of capitalism." It will not suffice to defend the formal
efficiency of the free market by means of an appeal to the for-
mal political techniques of democracy. An appeal to formal
rationalism from the market to the election booth and back
again is little more than the proverbial pair of drunks who lean
on each other in order to stay on their feet. Eventually, they
tumble together.

Weber's dualism between substantive rationalism and formal
rationalism is as applicable to democratic theory as to market
theory, The spirit of democratic capitalism needs moral content
derived from outside market theory and democratic theory.
The naked public square needs more than the fig leaf of politi-
cal and religious pluralism to protect it from the socially de-
structive elements of revolutionary violence and moral ero-
sion."* The same can be said for economic theory.

An Alternative to Dualism

The Christian cconomist who acknowledges the validity of
Van Til’s epistemology (and who also understands its applica-
tion) sees no hope in the quest either for a rational ethics — an

12. Gonzalo Fernandez. de la Mora, Egalitarian Envy: The Political Foundations of
Social Justice, translated by Antonio T. de Nicholas (New York: Paragon House,
1987), Part B.

13. Paul Johnson, “The moral dilemma confronting capitalism,” Washington Times
(Feb. 21, 1989),

14. Michael Novak, The Spirit of Democratic Capitalism (New York: Touchstone,
1982).

15. Richard John Neuhaus, The Naked Public Square: Religion and Democracy in
America (Grand Rapids, Michigan: Eerdmans, 1984). Cf. Gary North, Political Polythe-
ism: The Myth of Pheralism (Tyler, Texas: Institute for Christian Economics, 1989),

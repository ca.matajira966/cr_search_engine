There’s No (Autonomous) Accounting for Taste 111

misinterpret their account books and conclude that capital
consumption is in fact profit. Only when it comes time to re-
place worn-out equipment do they discover that they have
made a mistake,

Pierre Goodrich, the multi-millionaire whose money estab-
lished the Liberty Fund in Indianapolis, made a fortune for his
Independent Telephone Company of Indiana by being on the
right side of many long-term coal contracts prior to the infla-
tion of the late 1960’s and the 1970's. He had been advised by
University of Michigan accounting theorist and free market
economist William Paton to set up two sets of books, one of
which was tied to the government’s GNP price deflator."
Paton told him to make his decisions on the basis of this “shad-
ow” set of books, not the conventional accounts recommended.
by his CPA firm (and required by the Federal government).
Goodrich for years actually published both sets of books in his
company’s annual report, the first U.S. corporation to do so. In
the early 1970’s, companies from across the U.S. ordered a
copy of the report to see how it was done.”

But there is a major theoretical problem with accounting, a
problem more readily understood when we discuss inflation
accounting: Who is to say what the “right” commodities are for
inclusion into the government's price index? There is more
than one available index: wholesale prices, consumer prices,
GNP price deflator. The statisticians must assign weighted
numerical values to each commodity and service in an attempt
to reflect its overall importance to the economy. Question:
Whose economy? Yours or mine? How can every participant in
the economy agree on the proper weights assigned to the se-
lected commodities and services? (Personal evaluations by econ-
omists of the economic importance for “the economy at large”

11. Paton died in 1990 at age 101.

12. An early Liberty Press publication was Economic Caleulation Under Inflation
(1976), with essays by Solomon Fabricant, William Paton, Paul Grady, George Ter-
borgh, and others.

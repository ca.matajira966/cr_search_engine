112 THE COASE THEOREM

of a limited number of goods and services are called “weights,”
which gives you some idea of the problem: a physical term for
a psychological phenomenon, meaning a phenomenal term for
a noumenal “noumenon.”) How can the statisticians be sure
which commodities and services should be in the selected list?
How important are they, relatively speaking, in the minds of
most participants? How do we find out how important an eco-
nomic good is in the minds of most economic actors?

In constructing their statistical index numbers, the statisti-
cians have to “feel” their way along. They must intuit the appro-
priate weights. But when we use the words “feel” and “intuit,”
we have returned to Kant’s practical reason (the noumenal
realm) where numbers determine nothing, and pure reason
(the phenomenal realm) is necessarily silent.

How does a decision-maker know for sure that what the
account books seem to be telling him is exactly what he wants
to know? He cannot be sure, according to subjectivist economic
theory. If he is a consistent believer in subjective value theory,
he must conclude that all objective price indexes are inherently
corrupt and theoretically unjustifiable. He must conclude that
any change in the purchasing power of the monetary “unit”
cannot in fact be measured in a meaningful subjective manner,
for to discuss “purchasing power” you must discuss index num-
bers of compiled prices, and all index numbers, being aggre-
gates of individual preferences (“weighted averages”) are theo-
retically invalid. Why? Because ever since the classic study by
Lionel Robbins, The Nature and Significance of Economic Science
(1932), economists have known that it is illegitimate to make
interpersonal comparisons of subjective utilities. This rules out
all index numbers. It also rules out all applied economics, and
all economic advice to decision-makers. If taken seriously, this
crucial application of subjectivism destroys economics.

Yet these same defenders of subjective economics want to be
able to discuss certain relationships. For example, a defender of
the “Austrian” monetary theory argues that rising prices are not

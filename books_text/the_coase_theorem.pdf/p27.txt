Introduction 9

Some Odd Conclusions

An exclusively subjectivist view of cost and choice can lead to
some very odd conclusions. (So, for that matter, can any other
exclusive line of human reasoning.) G. F. Thirlby follows the
logic of an individual's one-time decision. He concludes: “Cost
is ephemeral. The cost involved in a particular decision loses its
significance with the making of a decision because the decision
displaces the alternative course of action.” He says emphati-
cally chat “the cost figure will never become objective, i.e. it will
never be possible to check whether the forecast of the alterna-
tive revenue was correct, because the alternative undertaking
will never come into existence to produce the actual alternative
revenuc.”"! This is Buchanan’s conclusion, too. But if no cost
ever becomes objective, what is (he purpose of accounting?

Should You Fire Your Accountant?

What does all this mean for the accounting profession? What
does it do to the very concept of personal or corporate budget-
ing? He does not say, but he does not stop, either. Following
the persuasive logic of subjectivism, Thirlby concludes that
“The cost is not the things - e.g., money - which will flow along
certain channels as a result of the decision; it is a loss, prospective
or otherwise, to the person making the decision, of the oppor-
tunity of using those things in the alternative course of action.
A fortiori, this cost cannot be discovered by another person who even-
tually watches and records the flow of those things along those chan-
nels.”"= Then of what objective use are accountants? Why was

10. G. F Thirlby, “The Subjective Theory of Value and Accounting Cost,” Econo-
mica, XII (Feb. 1946); reprinted in James Buchanan and G. F. Thirlby (eds.), L.S.E.
Essays on Cast (New York: New York University Press, 1981), p. 140. L.S.E. stands for
London School of Economics.

11. Thirlby, “The Ruler,” South African Journal of Economics, XIV (Dec. 1946), p.
264; ibid., p. 182.

12, Thirlby, “Subjective Theory,” ibid., p. 189.

 

 

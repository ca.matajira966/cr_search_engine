14 THE COASE THEOREM

tionships between the mind and matter. These assumptions are
very seldom spelled out by economists.’ Epistemology, the
fundamental question of all philosophy - “What can man know,
and how can he know it?” — is not a popular topic within the
economics profession,

The Problem of Measurement

The advent of modern economics is generally dated from
the early 1870’s, when three scholars independently came to
the same conclusion, namely, that economic value is imputed:
the concept of subjective value.* Value, they concluded, is sub-
jectively determined. It is not an objective quantity. The key
unit of value is the value (subjective) of the marginal unit. The
decision-maker asks himself: How much (objectively) of this
must I give up in order to obtain that? By 1900, virtually all
non-Marxist economists had broken with the older objective
value theories of the classical economists, such as the labor
theory of value or the cost-of-production theory of value. By
grounding economics on the subjective valuations of individual
decision-makers, economists today believe that they have es-
caped from the intellectual dilemmas that had arisen as a result
of classical economics’ objective value theory. (The most famous
one was Adam Smith’s “water-diamond paradox.”)°

3. Gary North, “Economics: From Reason to Intuition,” im North (ed.), Founda-
Hons of Christian Scholarship (Vallecito, California: Ross House, 1976).

4. The three scholars were William Stanley Jevons (England), Carl Menger
(Austria), and Leon Walras (Switzerland). See R. 8. Hovey, The Rise of the Marginal
Utility School, 1870-1889 (Lawrence: University of Kansas Press, 1960); Emil Kauder,
A History of Marginal Utility (Princeton, New Jersey: Princeton University Press, 1968).

5. “The things which have the greatest value in use have frequently little or no
value in exchange. . . . Nothing is more useful than water: .. . A diamond, on the
contrary, has scarce any value in use; .. .” Adam Smith, Wealth of Nations (1776), end
of Chapter IV. The paradox: Why is it that something as valuable to human life as
water is worth so little in comparison to diamonds, which are not really crucial to
mankind? The marginalist-subjectivist’s solution: “We never choose between water in
general and diamonds in general. We choose between a specific amount of water and
a specific amount of diamonds at a specific point in time. In the middle of a desert,

 

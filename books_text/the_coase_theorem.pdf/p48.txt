30 THE COASE THEOREM

nomic life is still organized on barter principles. The ‘marriage
market,’ child rearing, and a friendly game of bridge are exam-
ples. These services have value which could be monetized by
reference to substitute services sold in explicit markets or in
other ways.”

Question: Who should make the initial distribution of an
ownership right to whomever “values it the most”? How does
this sovereign agent know scientifically which potential owners
“are likely to value them [ownership rights] the most”? In
short: By what standard of value does he make the initial distribution?
Dead silence from Chicago School economists. To say anything
at this point would be a public admission that economic science
is no longer regarded by them as being value-free. The Coase
theorem would have to be acknowledged for what it is: an
important component in a giant academic shell game. The
ethical pea is always concealed beneath the seemingly neutral
scientific shell of cost-benefit analysis. To paraphrase the late
John Mitchell, U.S. Attorney General under President Nixon:
“Watch what the economist does, not what he says he is doing.”
He is invariably making interpersonal comparisons of subjective
utility every time he recommends a policy decision.

The debate over social costs raises once again the ancient
debate between objective and subjective knowledge. It is one of
the persistent antinomies in all humanist thought. The epistem-
ological problem of social cost is an ethical problem, and as
such, humanists cannot solve it “scientifically.”

Reciprocal Harm

Coase reformulated the terms of the debate over externali-
ties. “The question is commonly thought of as one in which A
inflicts harm on B and what has been decided is: how should
we restrain A? But this is wrong. We are dealing with a prob-
lem of a reciprocal nature. To avoid the harm to B would.

22. Tbid., p. Bt.

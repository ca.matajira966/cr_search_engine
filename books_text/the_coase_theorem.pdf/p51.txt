The Coase Theorem 33

Subjective Value vs. Social Policy

Coase never comes to grips with this problem. “What answer
should be given is, of course, not clear until we know the value
of what is obtained as well as the value of what is sacrificed to
attain it.”*° Value? As economists, we need to ask ourselves
several questions: Value to whom? Society as a whole? The
value to the cattle owner? The value to the farmer? Also, how
can judges make such estimates of economic value, since all
economic value is supposedly exclusively subjective? Questions
of economic value are the main problems raised by his paper,
yet he cannot answer them by means of the “scientific econom-
ics” he proclaims. No economist can. Economist Peter Lewin
has gone to the heart of the matter when he writes in a wither-
ing critique of Coase that

costs are individual and private and cannot be “social.” The
social-cost concept requires the summation of individual costs,
which is impossible if costs are seen in utility terms. The notion
of social cost as reflected by market prices (or even more prob-
lematically by hypothetical prices in the absence of a market for
the item) has validity only in conditions so far removed from
reality as to make its use as a general tool of policy analysis
highly suspect... .

The foregoing suggests that any perception of efficiency at
the social level is illusory. And the essential thread in all the
objections to the efficiency concept, be it wealth effects, distor-
tions, or technological changes, is the refusal by economists to
make interpersonal comparisons of utility. Social cost falls to the
ground precisely because individual evaluations of the sacrifice
involved in choosing among options cannot be compared.”

26. Coase, “Social Cost,” p. 2.

27. Peter Lewin, “Pollution Externalities: Social Cost and Strict Liability,” Cato
fourmat, il (Spring 1982), pp. 220, 222.

44 THE COASE THEOREM

lem, you understand. It is as if a sprinter were trying to reduce
his time in the hundred meter race to one second flat by shav-
ing a tenth of a second off his time in each preliminary heat. It
is an empirical problem, you understand. If he could just get
better shoes or a track with better traction!

Calabresi knows all this. He acknowledges that the decision
which would be reached if the transactions were costless is an
“unreachable goal.”"* He also acknowledges that “the gains
which reaching nearer the goal would bring are not usually
subject to precise definition or quantification. They are, in fact,
largely defined by guesses. As a result, the question of whether
a given law is worth its costs (in terms of better resource alloca-
tion) is rarely susceptible to empirical proof. . . . It is precisely
the province of good government to make guesses as to what
laws are likely to be worth their costs. Hopefully it will use what
empirical information is available and seek to develop empirical
information which is not currently available (how much infor-
mation is worth és costs is also a question, however). But there
is no reason to assume that in the absence of conclusive infor-
mation no government action is better than some action.”””

Please get his argument clear in your mind: welfare econom-
ics is essentially an empirical science, except that empiricism
cannot really solve the issues of welfare economics, so the State
will have to decide what is the appropriate allocation of re-
sources, but economists nevertheless hope that the bureaucrats
will use empiricism as the means of finding solutions to the
specific allocation problems, though only an economically effi-
cient quantity and quality of empiricism should be purchased.
In any case, the State's decision will necessarily be based pri-
marily on guesswork. If this explanation resembles a walk
through a hall of mirrors, it is because it és a hall of mirrors.
Yet virtually all essays in welfare economics are little more than

16, Idem.
U7. fid., pp. 69-70.

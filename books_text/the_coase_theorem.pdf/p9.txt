Preface ix

spend much time pondering epistemology’s challenge: “What
can I know and how can I know it?” At a symposium held at
the annual meeting of the American Economic Association in
1951, Fritz Machlup identified the reality of the economics
profession in the United States: “Usually only a small minority
of American economists have professed interest in methodolo-
gy. The large majority used to disclaim any interest in such
issues.”* In this regard, things have not changed much since
1951. Kuhn identifies two types of scientists: the “normal” prac-
titioner and the revolutionary innovator. The typical normal
practitioner of the economics profession has never even consid-
ered the issues that I discuss in this book.

Several years ago, a safely tenured economist at a large
American state university assigned his graduate students an
essay that I wrote in 1976, It dealt with the epistemological
crisis of modern economics.’ He reported to me that they re-
sented having to read the essay. They did not want to be both-
ered by questions regarding the fundamental presuppositions
of their life’s work. They just wanted to get on with it.

This self-imposed blindness to questions of epistemology is,
in the language of the profession, a product of rational self-
interest. It is unwise to spend time pondering solutions to a
problem that cannot be solved, given one’s presuppositions,
especially since any public discussion of this problem could
reduce the demand for one’s professional services. If I am
correct about the epistemological weakness of all modern eco-
nomic theory, and if economics is correct about the rational
self-interest of acting individuals, then we should expect to

4. Fritz Machlup, “Introductory Remarks,” American Economic Review, Papers and
Proceedings, XLT (May 1952), p. 34. Note: the annual Papers and Proceedings issue of
the American Economic Review is regarded by the profession as less relevant than
publication in the other four issues of the A.E.R.: Siegfried, ap. cit., p. 34.

5. Gary North, “Economics: From Reason to Intuition,” in North (ed.), ounda-
tions of Christian Scholarship: Essays in the Van Til Perspective (Vallecito, California: Ross
House, 1976).

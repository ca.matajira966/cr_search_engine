72 The Covenant Enforced

of them. When we start coming up with our own ideas out of our
brains, He refuses every bit of it. Therefore, let us hold to the
grace of our Lord Jesus Christ, knowing that we are washed and
cleansed by His blood, and that this is the only remedy that God
sets forth for us.

Salvation in Christ Alone

Thus we have two things to mark. The one is that if we should
be judged by the law of God, there would not be needed anything
more than this one sentence to damn us all, yea even the holiest
man that ever lived. For no man has ever satisfied God’s law, and
therefore are we all condemned. For if the holy fathers, who had
an angelic holiness in this world, were all the same at fault before
God, what will become of us? Let us now make a comparison
between us and them. How far removed are we from the holiness
of Abraham, the purity of David, the rectitude of Job, and the
perfection of Daniel? When these stand condemned, where do we
stand? Let us, then, learn to pull in our horns, and let every one
of us keep his mouth shut, as St. Paul says (Rom. 3:19} when he
brings us to the righteousness of faith and to the mercy of God.

What we need to keep in mind, then, is that God has removed
all self-righteousness from us, to rid us of all presumption and
pride, so that we should no longer pretend to come to account
with Him to bind Him to us, but that we should willingly con-
demn ourselves. That is the first thing.

Now from this we have to consider the remedy that God has
Jeft, which is that we can be righteous by means of our Lord Jesus
Christ. For He has delivered us from the curse that was due
against us, and for that reason He was hanged upon a tree, as St.
Paul says (Gal. 3:13), We have seen already in this series, in the
twenty-first chapter, that as many as were hanged upon a tree
were all accursed (Dt. 21:23). Now when God made that law, did
He not know what He had already ordained concerning His only
Son, who was to be hanged upon a tree? Surely it was an un-
changeable decree, made even before the foundation of the world.
Seeing this is so, we are redeemed from the curse by our Lord

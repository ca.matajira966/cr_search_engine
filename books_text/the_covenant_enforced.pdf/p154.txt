116 The Covenant Enforced

preference? Nothing, obviously. Therefore, it is God who must
make choice of us through His own mere goodness, and when
He has declared Himself to be our God, then we may also for our
part be bold in all assurance and without doubt call upon His
name. Now we sce what is the effect of this sentence.

Moses intends to declare the preeminence that God gives to
those whom He has adopted as His children, which is that al-
though they are mingled among men, and encompassed with
many miseries, yet they are preserved, being under His hand and
protection. This is because He holds them and avows that they
are of His household, and not for any reason, except His own
good pleasure. Let us therefore courageously defy Satan, when
we see that he practises all that he possibly can against us, and
let us count ourselves assured against all the dangers of this world,
seeing that God has done such favor as not to leave us to fortune,
as the unbelievers imagine. And why? Because we are unto Him
a holy people.

And what do we get this preeminence from? We have it
because He has testified to us that He is our God. If we had not
gotten this word from Him, we should always be in perplexity.
We should still doubt. We should be questioning this or that, and
our life should hang as it were by a thread, as we shall see in this
chapter. But seeing that God has uttered His fatherly love to us,
and it has pleased Him to open His mouth to make us understand
that He has given us familiar access to Him; seeing, I say, that
we have such assurance, let us call upon Him. Let us not doubt,
but glory in this, that He is our Savior, and that since we are
His, we cannot perish.

All the same, let us beware that we do not call upon the
name of God falsely, as all those who abuse it, making a mockery
of Him, and despising His majesty. If we daim the name of God,
let it be because we are grounded upon His promises and have
received them through faith, and then let us call upon Him, let
Him be our refuge, and let us not give ourselves to fond bragging
as they do who think it enough for them to bear the bare name
of Christian. No, let us follow the call of our God, as Moses shows

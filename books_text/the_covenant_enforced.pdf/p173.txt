Sermon 156: God’s Threats 135

This is a beginning, and indeed it is only a beginning, inso-
much that if we should remain there, we would not yet be plucked
out of the mire. But when we have thus begun, then does the
Lord match His threats with a taste of His goodness in such a
way that we are drawn to Him. And in so doing, He shows
Himself to be our Father. Now when we understand that God
indeed is ready to reward us when we have served Him, even
though we are not able to merit anything but rather only provoke
His wrath, then we must have our refuge in His mere mercy to
obtain remission of our sins as it is offered to us in our Lord Jesus
Christ and as He has purchased it-for us by His death and
passion. When we are thus reformed, and are rid of all trust in
our own works, then we ought to offer up ourselves willingly in
sacrifice to God, as St. Paul also exhorts us in Romans 12:1. He
sets forth no reward, but says, “I beseech you, brethren, by the
mercy and compassion that God has shown towards you, that
every one of you renounce this world and also himself, that you
become living sacrifices to God, for that is your reasonable serv-
ice.”

Promises Come Before Threats

But yet, somebody is sure to raise a second objection, and
say, “If threats make us ready for the promises of God, it seems
that they should be placed in the first rank, and that the promises
should follow them.” But the answer to this doubt is this: that
God will hold us more convicted before Him when He begins with
us by means of His promises. For we shall have to admit that
there is a shameful thanklessness in us, seeing that while our Lord
seeks only to win us by love, we still deal rebelliously with Him
and draw backward when He comes so lovingly toward us. You
see then why promises are set before us first of all: that we might
be the more reproved for our rebelliousness,

Then God moves on to show us that His goodness will not
avail us unless He threatens us. In doing so, He uses rigor, and
the same rigor is profitable to us, since when we are touched with
it our hearts are daunted. Not everyone, of course, for there are

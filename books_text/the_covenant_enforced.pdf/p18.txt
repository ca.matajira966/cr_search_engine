xviii The Covenant Enforced

God’s will that we should have enemies and be kept occupied with
wars, yet notwithstanding He holds us still in His keeping, and
we are maintained and defended by His power and goodness.
[Below, pp. 152-53.]

There can be little doubt that Calvin believed in a covenantal
view of history in which the ethical character of men’s lives affect
their outward conditions. The judicial content of Calvin’s ethical
system was explicitly biblical. Without this belief in covenantal
cause and effect in history, there could be no possibility of creating
an explicitly biblical social theory. That such a view of history is
rejected by most Protestant theologians today, and has been re~
jected as far back as 1700, explains why no Protestant group other
than the Christian Reconstructionists have attempted to devise a
uniquely biblical social theory. It also helps to explain the enor-
mous hostility of modern Calvinist theologians and fundamental-
ist church leaders to Christian Reconstructionism: they hate Old
Testament law with a passion. Even more than this, they hate the
idea of God’s sanctions in history in terms of this law, for such a
view of sanctions would make Christians morally responsible for
applying His law to the details of life, preaching the conclusions
publicly, and enforcing them wherever legally possible. In short,
it would make Christians responsible for what goes on in society.
Responsibility on this scale is what modem Christianity for over
a century has desperately sought to avoid.

Ethically Random History:
A Non-Calvinist Theology
We now come to the third aspect of this inquiry: the concept
of history taught at Calvinist seminaries. Before beginning this
inquiry, let us once again consider Calvin’s view of the covenantal
nature of God’s sanctions. He insisted that this covenantal rela-
tionship did not end with the New Covenant era:

Now Moses says that this people shall be an astonishment, a
proverb, a byword, and a ridicule among the nations in which
they will be dispersed. Here our Lord shows that as His goodness
should be displayed among the people of Israel, so that every man

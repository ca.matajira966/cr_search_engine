Sermon 156: God’s Threats 147

to pray to Him to vouchsafe to cure our faults in such a way that
they may not proceed to the extremity here expressed, but that
as soon as we feel a little stroke of the rod of His hand, the same
may be sufficient to bring us again to Him; indeed, to Him with
the kind of repentance that will show that we are honest, acting
in simple truth, and that we continue therein to the end.

Prayer

Now let us fall down in the sight of our good God, accusing
our whole life of the malice and rebellion that is therein, beseech-
ing Him to vouchsafe to chastise us with gentleness and to spare
us in such a fashion that we are not cast down into despair but
rather are brought home again to Him, And therewithal, since
we cease not to offend Him and it is also necessary that He should
daily call upon us and awaken us that we may fare the better by
His corrections, let us continue to lament and mourn until that
time when He has rid us from all the corruptions of our flesh and
has brought us to the perfection of righteousness wherein lies our
true rest, which is the heavenly blessedness we hope and long for.
And since we cannot obtain any such matter except by means of
our Lord Jesus Christ, may it please Him to reconcile us to God
His Father by His death and passion and in the meantime so to
mortify our old Adam that His own Image may be restored in us,
and His glory shine forth in us. That it may please Him to grant
this grace not only to us but also to all people and nations upon
earth; etc.

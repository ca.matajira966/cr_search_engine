Sermon 157: God’s Plagues 151

though only a little while before no man thought to have stirred
himself up. Let us therefore understand that when wars occur in
the world, God shows tokens of His wrath.

It is certain, as we have already noted, that even though we
serve God faithfully and stand in His protection, yet we shall not
cease to have enemies. For it is His will to exercise us by that
means. Such is the present state of the Church, and such it has
always been. But this good comes from it, that God will maintain
us against our enemies. When they have devised evil against us,
yet they will not bring their purpose to pass. They will be disap-
pointed. And though they are as fierce as lions, and full of desper-
ate rage, yet will God tame them at the last; and though they
continue in their purpose to devour us, yet they will not have the
power to do it. As has been said before (Dt. 28:7), if they come
against us one way, they will flee seven ways.

But now it is said contrariwise that even though we are more
powerful than our enemies, and attack them in good order so that
the victory may seem to be already gained on our side (as we see
how the wicked are inflamed with pride and presumption), yet
when we think ourselves to have attained the goal, God will touch
us with such a fear that we shall not know which way to run fast
enough, but every one of us will be at his wit’s end. Let us
understand from this that when God stirs up wars to chastise us,
though we are fully equipped and have all the means possible for
fighting, yet we must perish if God is against us. For victory is
not obtained by the force and valor of men, but by the Lord of
hosts. And there is no other help for us but to be at peace with
God, that He may choose to guard us under His wings — according
to His use of the same similitude, that He will play the part of a
hen towards her chickens (Matt. 23:37). For then we shall be safe,
though all the world conspire for our destruction.

And even though our enemies be exceedingly mighty and
strong, and fully determined in their malice to destroy us utterly,
yet is it sufficient that God protects us, and that even though we
have no succor from men, yet we are sufficiently fortified by His
power alone. But if we proceed to offend Him, and He sees that

206 The Covenant Enforced

That is to say, He will punish them after a strange fashion, in a
way not commonly seen among men, so that they will be com-
pelled in spite of themselves to say, “Surely this is the hand of
God.”

Indeed, can there be a more beastly contempt of God found
than what was shown by Pharaoh? He was a man not only drunk
with pride, but wholly senseless. He was a man who despised the
majesty of God, a man so rebellious that he could not in any
manner be dealt with. When he heard Moses and Aaron speak,
he laughed them to scorn. When he felt the first strokes, he refused
to yield. But in the end he needed no prophet to admonish him,
for he himself could say, “Surely this is the finger of God.” We
see therefore how God often expresses His power in such a way
that even the most fierce are constrained to perceive and to think
that there is some majesty in heaven, which before that time they
had not acknowledged, and as a result they enter into considera-
tion of their sins and confess them, and are the more lively touched
therewith. This is what Moses here means concerning signs and
wonders.

This matter is worthy to be marked. For as 1 have said, if
God begins to punish men, it is usually ascribed to fortune. This
is agreeable to our state, for we know that man’s life is subject to
much wretchedness — so they think; yet all the while the hand of
God is not regarded. And if He doubles the punishment, yet men
continue to be dull and seem as if they could continue to hide
themselves and escape away. They do not enter into their con-
sciences to search out the sins therein. They don’t want to know
them. It is as ifa man should go and hide himself in a dark corner
in order to shun the brightness of the sun at noonday. Even so
do we behave in all the chastisements God sends us to warn us
of our sins and to draw us to repentance.

But in the end God augments His punishments in such a way
that they become miracles. That is to say, they exceed the com-
mon measure, the ordinary order and course of nature, so that
we might be ravished with astonishment, and thereby perceive
that God is showing Himself as though His hand appeared from

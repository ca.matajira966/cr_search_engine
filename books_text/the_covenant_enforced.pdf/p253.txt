Sermon 160: Signs and Wonders of Wrath 215

what do we find? He sees us not disposed to receive Him. We
despise Him, and thrust away His grace. Must there not be a
horrible perversity in men? Let every one of us excuse himself as
much as he wishes, yet this saying is true; and when we have
kicked and spurned as much as we can, yet we shail at the last
be convicted of this evil, that we could not find it in our hearts to
permit God to deal gently with us, “nor have served Him with joy
and a merry heart.”

Therefore, let us not wonder when He handles us as we
deserve, since we are so rebellious against Him. For when He
sees that we kick against Him, He must needs break us, and deal
with us in such a way that we may understand that He is our
Master. It is not as if those who are punished wind up serving
God, but that they understand that He has the majesty over them
when the punishments come so fiercely as to be signs and won-
ders. When God appears to them as it were in a visible manner
from heaven, then do they perceive the reality, “Alas, I cannot
flee or escape the hand of God.” Then do they understand that
He has the lordship over them, not that they willingly yield
themselves thereto, but that they lie languishing and astonished,
as men locked up in prison.

What we have to remember, then, is that since we could not
be content that God should handle us gently, by bowing under
His hand that He might guide us, tum us, and return us which-
ever way He wished; therefore, we must be forced by tribulation
and sorrow to understand that He has full sovereignty over us,
and that His utter breaking and overthrowing of us is because
we could net abide to be governed by His hand when He was
ready to guide us. This is what we have to note here.

The Loss of Spiritual Blessings
We see the same thing in all humanity, and not only concern-
ing the afflictions of this present life —we have this reproach laid
upon. us, that we are still as it were famished for want of spiritual
blessings. For our father Adam was created in such a state that
the world was an earthly paradise. The whole earth yielded him

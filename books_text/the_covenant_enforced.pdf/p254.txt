216 The Covenant Enforced

all good things to his wish. He endured neither heat nor cold, nor
any grievous want. Thus was our father Adam appointed lord and
master over all the world. All the elements and all the beasts of
the earth served him quietly, and all fruits served his taste and
savor. What was he in his person? He bore the image of God, and
was of such a great nobility and worth that he was like the angels
of heaven.

And he would have dwelt in this world with all his lineage,
in a place in which he would have had no trouble, but he could
not abide to be so gently entreated. When God had thus enriched
him with His benefits, he had to ruin things for himself, for he
could not serve God with a good heart and with joy. What could
have grieved him? After all, God had showed him a fair and
gracious countenance, and poured out the treasures of His fa-
therly love towards him. But Adam could not abide that, and
through his ingratitude alienated himself from God.

And how is it with us today? We must serve God in hunger
and thirst, in nakedness and reproach, for the earth is cursed to
us. When we have tilled it, it must bring us forth thistles and
thorns. We find the seasons contrary, so that when we wait for a
good year, we see hail or frost, drought or rain, which serve to
pluck the bread out of our mouths and to disappoint us of our
food. We see the air troubled, and infections that often engender
diseases. Great is the toil of men. For when they have gotten food
with great distress, yet they lack something to clothe themselves
with. See there, I say, what a state we are in. And why? Because
we would not serve our God cheerfully and with a good heart,
when He gave us abundance of all manner of blessings.

But this is not the chief matter, as I have said already, for
we are destitute of the righteousness of God. Our very truest
ornament was that we could have fashioned ourselves to all man-
ner of rightness, and how we have ruined this! Then we had
reason and understanding, but now we have become beastly, for
the brightness that should shine in us is but darkness. Again, we
are covered with reproach, and where the image of God should
shine in us, now we have the marks of sin, so that even our very

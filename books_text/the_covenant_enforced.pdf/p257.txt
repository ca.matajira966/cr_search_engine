Sermon 160: Signs and Wonders of Wrath 219

such mildness that we must lack both sense and reason if we are
not benefited by the goodness He employs.

But if we will not hearken to our God when He speaks to us
in so gentle and gracious a manner, what then? Then He will
speak to us with the heavy strokes of halberds, pikes, and hack-
buts [or, battle-axes, bayonets, and pistols].° We shall find this
hard to comprehend; their language will be strange to our intel-
lects. And why does this come about? Because we had no ears to
hear when God spoke graciously to us, indeed when He stooped
so low as to teach us like little children that are taught their
ABCs,

Let us then understand that when we are so deaf to God’s
word, He must speak with us in another language, and He must
stir up some barbarous and brazen-faced people that have no fear,
reason, or justice. When you petition such people for pity and
compassion, it will be in vain. They will give no ear to you. You
will find yourselves in such straits, whether you think so now or
not.

And what is the remedy for all these evils? Let us enter, let
us enter I say, into our consciences, Let us not grind our teeth at
men, as we are prone to do. Let us not strive with them, for that
is not where our combat lies. But let us understand that God
intends to chastise us by means of men, because we have been
stubborn against Him and refused to be edified by His Word
according to His first intention. And therefore, let us benefit our-
selves by these warnings and corrections God sends us. And let
us not wait until we feel the strokes, but whenever God does us
the favor of teaching us at the expense of other men, let us receive

5, Halberd—a military weapon, especially in use during the fifteenth and six-
teenth centuries; a kind of combination of spear and battle-ax, consisting of a
sharp-edged blade ending in a point, and a spear-head, mounted on a handle five
to seven feet Jong.

Pike—a weapon consisting of a long wooden shaft with a pointed head of iron or
steel; formerly the chief weapon of a large part of the infantry; in the eighteenth
century superseded by the bayonet.

Hackbut — an. early kind of portable firearm.

(Definitions from The Oxford English Dictionary.)

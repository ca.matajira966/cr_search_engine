Sermon 161: God Our Fortress 225

Sudden and Swift Enemies

Now Moses repeats something touched upon earlier, which
is that God shall bring us enemies from a far country, and that
they will be like eagles (v. 49). We should not think that God
needs to make any preparations long beforehand. If a man should
threaten us, we would look to see whether he had a sword in his
hand, and whether he is ready to execute what he has threatened.
But if God but whistles (as He says in the prophets), immediately
He has His men of war in readiness. He does not need to muster
them, or enroll them, or pound the drum. He needs none of this.
At His mere whistle (for He uses this similitude) all the whole
earth must needs be moved. Therefore, let us not look to see
whether or not things are in readiness, or whether or not any
hurt or harm may be done to us. Let us not look whether any
worldly means have been prepared. But let us understand that
before we can even conceive what evil might happen to us, we
shall be overthrown.

And why? Because it is God who speaks. He makes men to fly
like birds, even from one end of the earth to the other. Neither sea
nor mountain is able to save us from the experience of what we
never believed could happen. God has so ordered things, and even
among the heathen is often seen an incredible swiftness when God
is disposed to chastise the greatest part of the world. So much so
that some one man has risen up with a small number of people
to chastise an infinite multitude that did set themselves against
Him. And that not for one time only, but twice or thrice, one after
another; sacking towns and fortresses that seemed to be so well
appointed and strong by reason of natural situation that they
could not be approached unto, and yet they have been sacked as
if God had meant to renew a great part of the world. It has been
likewise seen that a people without renown or estimation have
risen up and made the most mighty to tremble.

And thus God gives examples of what is written here, which
is that He can surely bring our enemies upon us suddenly, and
they shall make such dispatch that they will come swifter than the
post. And when we suppose that we have some respite by truce,

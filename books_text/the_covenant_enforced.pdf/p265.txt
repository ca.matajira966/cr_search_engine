Sermon 161: God Our Fortress 227

Surely the mischief is doubled when we shield ourselves with
the creatures against God, and thereby become stubborn when
He threatens us, holding on still when he utters forth tokens of
His displeasure. Let us consider that the offense is then much
more grievous. Yet, this is exceedingly common. For while we do
not perceive any danger toward us from the world, do we not
continue in our sins? And do we not abide in stubbornness? God
speaks, but we regard Him not. If men favor us, and if we have
the means to fortify ourselves, we think that the hand of God
cannot come at us. And therefore it is with good reason that our
Lord reproves the wickedness of all such as are disobedient to-
wards Him; that is to say, they that put their trust in their
fortresses, in their high walls, in their munitions, and in such like
things.

Now, since we are subject to these things, it were better for
us if we have neither hedge nor anything else, than to be well
fenced with walls and bulwarks. For they serve but to blind our
eyes, when we have no more respect for our God, but are be-
witched by Satan to put our trust in things that will be our
confusion. However the case stands, let us advisedly hold this rule
in general; that is, that God be our fortress always, as we see He
promises in His prophet Isaiah in the twelfth chapter and also
chapters 55 to the end, for that whole section turns on this point.
Indeed, it is so common a teaching in the Holy Scripture that we
can scarcely turn a leaf but we shall see some text in which God
tells us that He will be our ramparts, our walls, our moats, indeed
double moats, our towers, and whatever else is necessary for our
defense. And why? Because (as I have said before), our minds
rapidly slide into vanity so that God is not able to hold us back
to Himself, and to count on our putting our whole trust in Him.

What must we then do? First of all, if we are destitute of
human aid, let us bear in mind that God by Himself is sufficient,
if we return to Him: “Ah, Lord, it is certain that we are the prey
of men, and we do not have the wherewithal to save ourselves, for
all the world fails us; but yet it is enough that You are our
succor.” After that manner should we learn to resort to Him. And

248 The Covenant Enforced

Papists have overflowed the whole world with superstition and
idolatry and all manner of evil. Yea, and moreover they harden
their hearts in pride and scoff at God with open mouth. “How
now,” say they, “Are not we the Church? Is it not said that Jesus
Christ will be with us unto the end of the world? Can He forsake
His Church?” Yet they continually crucify Him as much as in
them lies;6 they spit in His face, and do Him all the reproach in
the world; they make Him as if He were a prisoner or a bondslave
among them;’ and they do not consider that such a horrible
destruction happens throughout the whole world, through God’s
just vengeance, for the shameless hypocrisy that has been in them
that have rebelled against the gospel. Yet notwithstanding, God
has continued to save His people, as it were under the earth. And
in our days He has raised His Church, as if its bones of rotten
bodics should recover flesh and strength again (of. Ezk. 37). For
what were we through our unbelief? You see how it is a miracu-
lous resurrection that God has wrought in this way.

Mark well therefore that when our Lord promises to maintain
His Church and to preserve it, this promise does not apply to
them that abuse His name falsely or to them that come to despise
Him because He is gentle and merciful. For they will evermore
be disappointed of that favor that God has reserved unto His
people, because they remove themselves from it through their own
malice and unthankfulness. Yet notwithstanding, however the case
stands, God is ever faithful and will find incomprehensible means
to give place to His truth, by fighting against the malice of men.
That is what we have to remember concerning this text, where
He says that He shall waste the Jews though they were as the
stars in the sky.

And in fact the same appeared even in Jesus Christ the Head

6. A reference to the perpetual sacrifice of the Mass, in which the transubstanti-
ated bread and wine are seen as resacrificed. Calvin seems to have Hebrews 6:6 in
mind here and in the next phrase.

7. A reference to the claim that the priest has some sort of dominion over God
when he performs the “consecration” of the sacramental elements. The Roman
Catholic Church has modified this type of claim in recent years.

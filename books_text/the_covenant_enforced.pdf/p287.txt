Sermon 162: A Plethora of Plagues 249

of the Church. And therefore we ought not to think it strange
that the members of the body should be fashioned like unto Him.
Whence came Jesus Christ when God His Father sent him to be
the Redeemer? The prophet Isaiah says that He would spring
from a stock, as if a tree were hewn down and there remained but
the stub within the ground, the sort that men might tread upon
(Is. 11:1). It is not said that He would come out of the house of
David, but out of the house of Jesse, who was a cowherd and a
man despised. Such was the father of David: a man of no estima-
tion. Although David was so excellent a king that he was chosen
of God to be magnified to the skies, yet the prophet shows that
when Jesus Christ would come into the world, it would not be
with any show of that royal majesty that was in David, but He
would come out of the house of Jesse, as if He came out of a herd.
And that in addition he would not come from a tree, but from a
stock that had been cut off. And in what manner? And after what
fashion? As a little branch, says he.

Now seeing that God did thus to the Head of the Church,
we must understand that all the body must be secure, but this
does not mean that hypocrites will be partakers of what God
reserves to His elect, whom He has chosen, who show themselves
to be His children and are obedient to Him, as to their Father,
and who also come not only with all confidence, but also are
drawn with a right affection to honor Him. And when we perceive
that at this day God is sending horrible destructions into the
world, let us understand that He practices what is here declared.
Yet, let us not doubt, but realize that He always preserves His
Church. Let us not doubt, but realize that He upholds us as often
as we have our recourse to Him, according to this saying, that
whoever calls upon the name of the Lord shall be saved, yea even
amidst the greatest trouble that may ever be (Joel 2:32). Though
heaven and earth should run together, yet we are sure that by
calling upon the name of God, we shall be preserved. But let us
take heed that we do not abuse His name, to make thereof a false
cloak, for He can waste us well enough. And if we boast that we
are of His Church when He has cut us off, He can quickly raise

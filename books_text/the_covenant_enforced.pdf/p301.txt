Sermon 163: Exodus Undone 263

rooted out of it,

Godly Fear

Now since we hear such horrible threats, let us learn to walk
in the fear of our God. For there is no other way to live in rest,
and to be comforted in the midst of our sorrows and adversities,
except to come with a free will to yield ourselves obedient to our
God, and to do Him such reverence as is due to Him. For when
we acknowledge Him to be our judge, we shall not act as rebels,
but shall be willing to be reclaimed, so that we do not wind up
included in the number of them that have refused Him. You see
then that we must willingly hearken to God when He menaces
us, be moved by it, and be touched to the quick. Based on this
we must endeavor to serve Him and to obey His Word. For it
may well be that we shall sometimes be vexed with fear and
unquietness, but God will deliver us from them. It is certain that
we shall be weakened by that means, and it is good for us so to
be. But yet however we fare, God will so keep us that we shall
not fall into utter despair.

Moreover, when we sometimes feel any disquict in ourselves,
and we lack the power to call upon God but are tormented with
distrust, let us understand that it is the fruit of our sins. And let
us desire God to make us perceive that we have not kept touch
with Him as we ought, and that this is why He casts us into
vexation and distress. But let us not so tempt our God, lest He
should proceed to the rigor here mentioned, namely, that we
should have no power to commit our life into His hands,

So then, whereas mention is made here of a trembling heart
{v. 65), let us consider wherein our true rest remains: that God
is our father and that we are His children. Likewise mention is
made of failing or sunken eyes. Let us understand that we must
Jook upwards as often as we are in any fear, and that there is no
other remedy to assuage our griefs but to lift up our eyes to
heaven. For as long as men look downward, what will they find
there but an infinite mass of miseries able to scare them out of
their wits, so that they will not know where to turn? Let us

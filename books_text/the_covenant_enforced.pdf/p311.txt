Sermon 163: Exodus Undone 273

redeemed us in vain but with the condition that we should hence-
forth live in liberty, despite death and the devil, just as the exam-
ple of St. Paul shows,

Prayer

Now let us fall down before the majesty of our good God with
acknowledgement of our sins, beseeching Him to touch us more
and more to the quick, that being stricken with such a fear as
may make us see that of ourselves we are dead and forlorn, we
may yet for all this not fail to flee for refuge to His mercy. Let us
therefore seek the way thither, which is that we acknowledge
ourselves to be reconciled by Jesus Christ, and that He is our
peace unto the end, according as He continually declares and
testifies to us by the teaching of His gospel. And that in the
meantime it may please our good God to give us the spirit of
mildness and meekness, to the end that we no longer rebel against
Him or have our affections straining and rebelling against His
law, but rather that we may commit ourselves to His guiding and
be confirmed in the assurance He has given us that He holds us
for His people, so that He may show by effect that He watches
over us and that He wili continue with us to the end to preserve
us both in life and in death. That it may please Him to grant this
grace not only to us but also to all people and nations of the earth;
etc,

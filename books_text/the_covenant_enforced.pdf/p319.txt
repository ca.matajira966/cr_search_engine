GENERAL INDEX

(See also the Annotated Table of Contents, which follows,
for a listing of Calvin’s major themes.)

Abraham, Abrahamic Covenant, 246ff.
accountability, 136f.

Adam, 215f.

altar, 6ff.

amazement, 179ff.

apostasy, 185ff.

assault, 56fF.

Augustine, xxxiv

Bahnsen, Greg L., viii-ix, xx
baptism, 10, 12
blessings, 24ff.
all from God, 106ff
material, 99f., 119f.
motivational, 13148
purpose of, 101ff
(see also promises, sanctions)
blindness, 45ff, 161
boldness, 115
bribes, 61ff
burial, 153i

Calvin, John
Sermons on Deuteronomy, xxvii soci
Canaan (land), 15
cannibalism, 2298.
captivity, 196f
Catholicism (see Papal religion)

cause and effect, 1434,
chastisements, 92, 163, 171ff
and blessing, 98, 102A

severe, 180F,

(see also sanctions)
Christ, head of Church, 248£
clergy, authority of, 5f
common grace, xxi
compassion, 47
counterfeiting, 44
curses, 137f.

come from God, 139f.

clinging, 1458

for disobedience, 668.

greatest, 262

(see also sanctions)

Daniel, 2116
David, 104£, 241
Deism, xxii
deliverance, 170
disease, 158M.
dispersion, 252f
dominion, 126M
drought, 120ff.
duty, 78f

Edwards, Jonathan, xxiv

281

General Index 283

obedience, xi, 16f, 321, 65¢,
85, 88, 1281.
and blessing, 28
officers, church, 86
old covenant: more childlike, 100
oppression, 48-51, 168, 173

Papal religion, 9, 13, 35f, 66ff,

82, 89, 1898, 247f,
parents, honor due, 41-43
Parker, T. H. L., xxvii, xxviii
patience of God, 152-53
peace offering, 15
peace with Ged, 267M.
pessimism, Galvin’s, xxv
Pharaoh, 206
plagues, 2308

Egyptian, 241

extreme, 244 ff,

(see also curse, sanctions)
possessions, 175ff.
postmillennialism, xx, xxxvi
preeminence of saints, 116, 126ff.
promises, 784

precede threats, 135f,

(see also blessings, sanctions)
prosperity, 187f,
providence, xxiv

Raguenier, Denis, xxviii
rain, 1206

Red Sea, 271

reprobation, 162

restlessness, 262

rewards, 26f.

righteousness, loss of, 2)6f.

Roman Catholicism (see Papal religion)
Rushdoony, Rousas J., xxiv

Samaritans, 8
Samuel, A. L., ix
sanctions, xii-xviil, xxxiliscxv, 22ff.

 

abiding, 191
(see also blessing, chastisement, curse,
Judgment — plagues, promise, threat, wrath)
Sawyer, Jr., Jack W., xxiii
secret sins, 38, 60f.
seminaries, Calvinistic, xvitixxiv
Servetus, Michael, 15
shelters from God’s wrath, 226ff.
signs, judgments as, 201ff
sluggardry, 125
social theory, Calvin's, xii
stoicism, in Calvin, 213, 250
suffering, xxxiv, 970, 2574
benefits of, 204f,
superstitions, 69
(see also Papat religion)
Sutton, Ray R., viii, xii
syphilis, 159

‘Ten Commandments, x
thanksgiving, 4, 13ff
theft, 43M, 175ff.
theonomy
Calvin and, viii-xii, x0di
definod, viiiix
threats, 29f, 131
as motives, 1348,
(See also curses, sanctions)

unity, 11ff

Wallace, Ronald, xuvii, 61, 71, 114,
213

war, 150, 177, 217

weapons, 219

weather, xxiii, 143

wrath of Cod, 226ff, 250%

yoke of iron, 2356
Young, E. J., xxiv

TABLE OF CONTENTS

   

 

Publisher's Preface, by Gary North. . +e vii
Editor’s Introduction, by James B. Jordan... 0 ee ee xxvii

1. Sermon 149: Altars and Ensigns. .........,5 I
Deuteronomy 27:1-10

2. Sermon 150: Blessings and Curses.......... 21
Deuteronomy 27:11-15

3, Sermon 151: Secret Sins. . 2... eee eee 40
Deuteronomy 27:16-23

4. Sermon 152: The Curse and Justification... ... 59
Deuteronomy 27:24-26

5. Sermon 153: Promise, Obedience, Blessing. .... 7
Deuteronomy 28:1-2

6. Sermon 154: Blessing and Affliction. ........ 95
Deuteronomy 28:2-8

7, Sermon 155: Separation Unto Blessing, ...... 113
Deuteronomy 28;9-14

8. Sermon 156: God’s Threats... ... saeecees 130
Deuteronomy 28:15-24

9. Sermon 157: God’s Plagues... ....... anes 148
Deuteronomy 28:25-29

10. Sermon 158: God’s Anger... 0... seer eee 166
Deuteronomy 28:29b-35

11. Sermon 159: The Peril of Apostasy... ....... 183
Deuteronomy 28:36-45

12, Sermon 160: Signs and Wonders of Wrath... .. 201

Deuteronomy 28:46-50

v

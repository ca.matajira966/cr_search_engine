26 The Covenant Enforced

together: one, that He will not deceive us in any thing; and two,
that He binds us to serve Him and declares that He will bear
with us in our infirmities and not deal severely with us to pay us
as we deserve, but will use a fatherly goodness.

Now on this basis we may be of good comfort to serve Him,
when we may say, “Surely it is true, Lord, that I do not discharge
myself of the hundredth part of my duty towards You, but no
matter what, You will not fail to accept me because You do not
respect what I do, but take a pleasure in me as in Your own
child.” You see, then, how God pardons us, and does not regard
our faults and imperfections, which are in the service we yield to
Him. When we serve Him of a sincere good will, and not hypocri-
tically, He likes all we do and rewards us for it. Since we hear
this, let us take pains, and receive the bridle into our mouths (as
they say) and press on; and even though we are hindered by the
vices of our flesh, yet let us force ourselves to go further. And
why? Because we shall not lose our labor.

Thus you see what God means. We perceive His inestimable
goodness in that of His own good will He offers His promises to
us, although He is in no wise bound unto us, as we have seen
heretofore; His will is to win us to Himself by all the means that
He may. Now He repeats this point again, and that is done
because of our sloth and negligence. For that reason He adds this
aid; and all for our profit, for what is He advantaged thereby?
Will He gain anything by our service? Let us defy Him to the
utmost, what will that hurt Him? But, He will possess us for our
own welfare.

The Rewards of Faithful Works

And with this He shows us also what mind is requisite for
observing His law properly, which is that we come willingly and
yield ourselves to Him, and place our whole felicity and joy in
serving Him, and put this sentence into our hearts, that where
our treasure is, there will our hearts be also (Matt. 6:21}. You see
what we have to notice touching the first point on the blessings.
In effect, then, what is it that we have to do? Although nowadays

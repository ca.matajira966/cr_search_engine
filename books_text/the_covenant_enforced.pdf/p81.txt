Sermon 151: Secret Sins 43

not give ourselves over to them or let the bridle slack. For this
reason let us learn to restrain ourselves, and to be sorry, seeing
we are not as perfect as is required. But with all that, as I have
declared. before, let us strain ourselves to please our Lord God
and to obey Him, and let us have such a record in our own
consciences that we may freely and with open mouth say, “Cursed
be he who has not followed the teaching of salvation in such a
way as it is showed to us.”

To make any long discourse on the honor that every man
owes to his father and mother is not needful at this point, because
the law has been expounded already before this. It suffices now
to know that in this text God declares that all disobedience, as
well against fathers and mothers as against all superiors whom
He has set in authority in this world, is intolerable in His sight.
For He will not have us to live here in a disorderly manner like
beasts, but He will have order and government observed among
us. And that cannot be done unless we stand in awe of such as
bear any office for the common government of men. Whoever,
then, breaks God’s order, let him look to be accursed, just as St.
Paul also tells us (Rom. 13:4) that those who rebel are not resist-
ing creatures or men, but make war against God Himself when
they go about to overthrow the superiority He has ordained and
commended to us, Mark that for one point.

Secret Theft

Now next is added, “Cursed be he who plucks up his neigh-
bor’s boundary marks” (v. 17), We must always bear in mind
what I have told you already, namely that here under one heading
God includes all similar kinds. I told you at that place! that if a
man’s lands are not kept secure, no man will be master of his own
possessions, but ail will go to spoil and chaos. And surely the
maintenance of just weights and measures, of lawful money, and
the keeping of boundaries unchanged, are things that are univer-
sally acknowledged. How can men buy and sell, or engage in any

1, Deuteronomy Sermon 114, on Dt. 19:14.

Sermon 151: Secret Sins 57

is condemned to hellfire (Matt. 5:22), what will become of him
who lifts up his fist? Surely, even to stir up a man’s tongue against
his neighbor is a sin worthy of grievous punishment. If we so
much as go “hmpf” in a scornful or disdainful way, it is forbidden.
How much more, then, shall we be blamed if we go about trying
to outrage them in some way. Let us therefore mark that here,
under one particular, God generally comprehends all the outrage,
violence, and misdeeds that we can do toward our neighbors.

And He says expressly, “in secret,” or “privately,” so that
we should keep in mind something I have mentioned to you
before, namely that in this case the matter is not determined by
a yielding of account before men. Suppose we have broken all the
commandments, and yet no man challenges us for it, and the
public authority approves of our doings, and when we are accused
of it we are acquitted so that no man dares to complain about the
disorders we have committed; suppose that were the case, still
we know that God speaks otherwise, saying that if we have done
our neighbor any harm privately, his blood will cry out against
us for vengeance, when we think we have escaped.

And we see what is said of Abel. Even though no man gave
information against Cain, and no process of law went against
him, yet the blood of the murdered man spoke. God does not say,
“I have heard a report about this,” but rather, “The blood of thy
brother cries to Me against thee.” Here, then, we see that God is
not threatening us with some punishment to be suffered at the
hands of men, but rather is telling us that we must walk before
Him and in His presence. And even though we do not fear any
earthly justice, yet we ought not to forbear to restrain ourselves.
For when God exercises His office, then the vengeance He has
spoken of here must be executed upon all such as have escaped
by favor, or concealed their crimes, or offended so cunningly that
no man could ever find it out.

Seeing that this is so, let us look to it that we enter into our
own consciences, and have God’s law written there. Let us not
be so much concerned with what men think, saying, “I have not
been blamed or reproved.” Rather, let us consider that our God

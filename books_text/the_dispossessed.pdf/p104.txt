88 Tue DispossesseD

sticketh closer than a brother, persuading them that within its
doors only is refuge, relief.”!

Various studies of poverty and homelessness in our own day
confirm the conclusions of Riis: When there seems to be no other
solace, strong drink becomes the refuge of the dispossessed. As
many as 60% of the repeat clients in a Houston shelter showed
signs of alcohol dependency.? In a Salvation Army shelter in
Cleveland that figure has been estimated at 75%.3 A soup
kitchen in Denver says that its clientele of chronic homeless are
“almost universally afflicted with alcoholism.”* In New York
City’s shelter system, estimates range from lows of 25% to highs
of 55%.° Many thousands of the homeless find themselves in
dire straits only temporarily. Surveys found that these short term
temporary homeless—four months on the street or less—are
70% less likely to evidence drinking problems than the long term
permanently homeless.* But even then, a full 30% of the short
term homeless are troubled by alcoholism.’ That is no small
number! Alcoholism, then, is undeniably part and parcel of the
problem of homelessness. The typical stereotype is at least par-
tially correct.

But, once the correlation between alcoholism and homeless-
ness — and especially alcoholism and chronic hardcore homeless-
ness — is established, social scientists invariably turn to the ques-
tion of cause and effect: “Did the alcoholism cause the poverty or
did poverty cause the alcoholism?” In many ways, this is a fruit-
less endeavor, like asking, “Which came first, the chicken or the
egg?” But in other ways, it is the most relevant of questions, one
rife with ethical implications.

If the homeless are on the streets due to their own sloth, irre-
sponsibility, and driving addictions, that is one thing. But if life
on the streets is so wretched, so hopeless, so demeaning, and so
dcbilitating that it drives the homeless to drink, then that is quite
another,

So, which is it? The chicken or the egg?

Driven to Drink

All thosc who drink too much have a good excuse. Or so they
say. They have reasons. They may be drowning sorrows, drink-
ing to forget —like Henry. They may be doing battle against the
ravages of guilt or envy or bitterness. They may be feeding the

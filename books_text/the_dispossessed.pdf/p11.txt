FOREWORD

 

by James B. Jordan

“And as they were going along the road, someone said to
Him, ‘I will follow You wherever You go.’ And Jesus said to him,
“The foxes have holes, and the birds of the sky have nests, but the
Son of Man has nowhere to lay His head’” (Luke 9:57-58). With
this statement, our Lord called attention to His condition of
homelessness during His ministry on earth. Jesus’ remarks are a
continuing challenge to His people, for they indicate that “home-
lessness” is, in one particular sense, a mark of true discipleship.
Indeed, just a couple of verses later, we read: “And another also
said, ‘I will follow You, Lord; but first permit me to say gaod-bye
to those at home.’ But Jesus said to him, ‘No one, after putting
his hand to the plow and looking back, is fit for the Kingdom of
God’” (Luke 9:61-62).

From time to time in the history of the Church, strange sects
have arisen that misinterpret our Lord’s words. With a literal-
mindedness that totally rejects the Biblical context of Jesus’
statements, they insist that the true Christian will drop out of so-
ciety and become a wandering beggar or nomad. Such move-
ments were numerous and influential toward the end of the
Middie Ages, and in our own time we have seen something
similar in parts of the “Jesus Movement” of the 1970s.

There is an implicit cruelty in such an interpretation, for if
physical homelessness is a mark of spirituality, then clearly we
should do nothing to house and shelter the homeless. We don’t
want to diminish their spirituality, after all! Let them remain
homeless ~ it is better for them,

Obviously this was not our Lord’s intent. Rather, Jesus was
calling men to make the New Home of the Kingdom of God their
first priority, and not to cling to their fallen, ruined Adamic homes.

xt

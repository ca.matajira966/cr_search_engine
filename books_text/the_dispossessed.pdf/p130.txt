it Tue DispossesseD

more, dot the sides of the tracks. Mostly deserted, their windows
shattered or checker-boarded, they look like a scene from war-
torn Europe. There is a dreariness to these cities. It’s not a lazy
kind of dreariness, but more a tired kind. There’s no life, no an-
ticipation. The squeaks and groans of the train wheels seem to
pass judgment on it all.”

America’s economy has changed. Down to its very founda-
tions. And these basic structural changes have obviously not
come without pain. The “gradual transformation” of our nation
“from an industrial society to an information society,” as John
Naisbitt has described it in his mega-bestselling book, Mega-
frends, seemed anything but “gradual” to the men and women
who made their homes, raised their families, and staked their
lives on the northern industrial corridor? It seemed anything
but “gradual” to those men and women who were unwilling par-
ticipants in the great economic shakedown of the late seventies
and early eighties: plant closings, corporate disinvestment, and
the dismantling of basic heavy industry.'3 It seemed anything
but “gradual” to those people who watched their once vital man-
ufacturing centers turn — almost overnight — into giant industrial
ghost towns.!#

It seemed anything but “gradual” to Mick Gallin.

During the 1960s, the nation’s overall economic growth
averaged 4.1% per year.'5 As a result, the gross national product
(GNP) expanded by a hefty 50% over the decade, consumers
were able to post a 33% gain in buying power, and industry
claimed a whopping 25% share of the world’s manufactured ex-
ports.6 America had the highest standard of Jiving in the world!”
and everyone expected more of the same. The sky was the limit.

But the 1970s were a different story altogether. The GNP
grew a mere 2.9% each year, giving consumers only about a 7%
increase in purchasing power. Industry’s share of the world
market dropped below 17% and the American standard of living
dropped behind Switzerland, Sweden, Denmark, West Ger-
many, Luxembourg, Iceland, Abu Dhabi, France, the
Netherlands, Kuwait, and Belgium. Dreams were dashed. Il-
lusions were shattered,

The reasons for the drastic turnaround in the economic
strength and vitality of the nation were multitudinous.” But
chief among them was a rabid fear of change. Americans clung

6 ‘THE DispossEsseD

ing, despite a big boost from the video and computer revolutions,
saw a 21% cut.® As a result, nearly 38 million jobs were elimi-
nated from the labor force—once the pride of the nation, the
envy of the world.3#

Most Americans were totally unprepared for unemployment
on that scale. We were shocked to see our neighbors and peers
standing in lines to obtain USDA milk, cheese, and butter. We
were flabbergasted by the sight of tent cities and overcrowded
rescue missions, Fear gripped the nation.

Of course, the transformation process continued unabated
despite the cataclysmic deindustrialization. Without many of the
declining industries to sap their strength, investors were able to
capitalize a number of new endeavors more in keeping with
market needs and directions. Thus between 1969 and 1976,
twenty-five million new jobs were created, about 3.6 million per
year. And between 1977 and 1984, another 27 million jobs were
added.3° With a bit of retraining and readjustment, most of
those who had lost jobs in old, dying industries were able to find
new jobs in emerging service and information fields.?”

Most of them. But not all of them. Some like Mick Gallin
never made the transition,

Between 1981 and 1984, there were almost 2.6 million people
that joined Mick Gallin on the long list of former workers who
had exhausted their unemployment benefits without finding re-
placement work,# 2.6 million people joined him in the long,
frustrating search for jobs: scanning the want ads every day,
making the rounds of employment services, union halls, trade
schools, and government offices, pounding the pavement, send-
ing out letters and resumes, and hoping against hope that some-
thing would break, Soon, Before they broke. Like Mick Gallin.

Housing and Unemployment

Sustained unemployment and major industrial transition are
fuel for the consuming fires of homelessness. The link between
joblessness and homelessness is obvious enough. With no money
coming in, people can’t afford to pay the rent. It’s that simple.
They are displaced.

Not all of these displaced workers finally wind up in the street
or in the shelters or in tent cities. There are alternatives to home-
lessness for the displaced. But they are few and far between.

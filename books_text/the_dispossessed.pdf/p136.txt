4120 ‘THE DispossesseD

expanding the economy in general. Entitlement was never an
issue. Opportunity was.

The U.N.’s Answer

Like the congressional leaders who have pushed through job
creation bills in the legislature, the officials in charge of the
U.N.’s International Year of the Homeless comprehend the close
correspondence of joblessness to homelessness. And like those
legislative leaders, U.N. spokesmen see the state as “the neces-
sary facilitator of job creation and equitable job distribution.”®

So, though most of the International Year concern has been
focused on “the development of sheltering options, and the
removal of socio, ethno, and legal obstacles to advancement,”
the idea of “governmentally subsidized development program-
ming” takes a high profile as well.®

Unfortunately, the same two fatal flaws that have crippled
the job creation initiative in the U.S. have also infected the
U.N.’s agenda. Several brochures and monographs produced by
the UNCHS allude to the “just redistribution” of earth’s “fixed
resources.”6 They speak of man’s “right to work”® and his “right
to employment.” They even castigate free market economies
for “raising living standards before affording full and equitable
employ,”7!

The solution? More “governmental control,”7? more “central-
ized regularization,” and more “state supervision”’t of “hiring
practices, and income distribution.””5

Gleaning

Clearly, the legislative leaders and the U.N. officials are cor-
rect in their assertion that the homeless must find employment.
But how should that employment be supplied?

According to the Bible, work is the means by which the poor
advance their lot. God awards power, wealth, blessing, and do-
minion to those who labor diligently (Deuteronomy 8:18; Prov-
erbs 10:4). Far from being a part of the fall’s curse, work is a vital
aspect of God’s eternal purpose for man (Genesis 1:28). In fact,
“a man can do nothing better than find satisfaction in his work”
(Ecclesiastes 2:24; 3:22). That is why God built work opportuni-
ties for the poor into the fabric of the Old Testament society.”

But instead of making a continual redistribution of wealth

Leave By the Dogtown Gate: Transiensy 141

the streets. Remembering was taboo. Remembering was painful.

“At first, they didn’ know what to do. They could a’ moved in
with the grandparents, But they was already puttin’ up a couple
a’ other a’ the kids. I think that was it. I don’t know. Yeah, I think
that was it... Whatever. They figured with Anya and the two
babies — three kids an’ all—it’d be just too much. Anya’s daddy,
he was proud. Didn’ like charity, y'know. Never even collected
on unemployment.”

Anya was up now, walking toward us again. She couldn't
stand to be a part of our conversation. She couldn’t stand not to
be. Her eyes were hard again. Like before. Haunting.

Elz looked her way and began again. This time more care-
fully. “So, they just up and moved. Weren’t no jobs there in Bal-
timore. Was Baltimore, wasn’t it, hon?”

“Yeah, Baltimore.” Anya’s terseness matched her mood. Elz
didn’t seem to notice.

“There was s’posed t’ be work to be found in Florida, tho’.
They figured, why not give it a shot. Packed up everything they
could. Piled into the ol’ car. An’ left. Just left cold. Drove off into
the sunset.”

“That was the end of the good times.” Anya said it with all the
finality and solemnity of a eulogy. “That was the end of it all.
Oh, I mean we was still together an’ all for a while. But once
we'd left home — Baltimore—that was the end.”

The story came out like that. In fits and starts. Anya would
tell a bit of it, then fall into a silent gloom. Elz would pick it up
and carry it for a while. Then it would go back to Anya. Back
and forth. Back and forth. For nearly four hours we talked. Or
really, they talked. I mostly listened.

There were no jobs in Florida. At least not for someone with
few skills and no contacts, someone like Anya’s father. So they
moved on. West to New Orleans. North to Little Rock. Back-
tracking to Atlanta. More desperate with each passing moment.
Tensions rising. Tempers flaring. Life crumbling. Hope dis-
sipating.

“The movement changed them,” wrote John Steinbeck of
another time, another place, yet oh so familiar. “The highways,
the camps along the road, the fear of hunger and the hunger it-
self changed them. The children without dinner changed them,
the endless moving changed them.”!

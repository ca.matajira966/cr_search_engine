144 Tue DrspossesseD

A Nation of Migrants

Though it may have been new to Anya and Elz, migration is
nothing new to most Americans. After all, America is a nation of
immigrants and settlers. Our history is punctuated with the bold
pioneering bursts that sent opportunity seekers through the
Cumberland Gap, down the Mississippi, across the Great
Plains, and up the Oregon Trail. In 1849 thousands left their
homes in a gold rush to California. In 1854, 1871, 1906, and 1932,
depressions sent thousands more scrambling across the nation in
search of opportunity—in search of a place they could call
“home.”

Between 1820 and 1978, almost fifty million people emigrated
to the United States!?—displaced, uprooted, looking for the
green pastures and still waters of home. In the first decade of this
century alone, 8.8 million people arrived at Ellis Island” to gaze
upon Liberty who beckoned: “Give me your tired, your poor,
your huddled masses yearning to breathe free, the wretched
refuse of your teeming shore.”” During that single decade, the
nation’s population swelled by over 11% simply as a consequence
of this deluge.”

And though they entered at the gates of Liberty in New
York, they certainly didn’t stay put. They migrated to every cor-
ner of this vast new land. Thousands of Scandinavians took to
the rich hills of Minnesota and Wisconsin. Germans moved
about, finally settling in Missouri, Texas, and Nebraska. Bas-
ques spread into Nevada, Utah, and Idaho. Poles settled in
Michigan, Illinois, and Colorado, Italians staked out Pennsyl-
vania, Ohio, and Indiana, Chinese migrated throughout Cali-
fornia, Texas, and Massachusetts. Caribbean Islanders moved
into Louisiana, Florida, and Georgia. The entire nation was
abuz with movement.

This nation was built by newcomers, outsiders, aliens, and
sojourners. It was built by people who left all that was near and
dear to follow a dream to a better place.

Transience has always been an aspect of our national profile.

Settlement and Resettlement Laws

The free acceptance, and even encouragement of transience
in carly America is an aberration in Western history. Aliens and
sojourners, far from being treated as a national resource, as an

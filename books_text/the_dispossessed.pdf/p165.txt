Leave By the Dogtown Gate: Transiency 149

anarchy (1 Corinthians 6:20; Titus 2:14). No, we have been set
free to follow the true course of liberty: obedience to God’s Law.
But it does mean that we no longer need to resort to the coercive
and repressive tactics of containment and resettlement in order
to solve the dilemma of homelessness.

The freedoms established and enforced by Gad’s Law en-
couraged expansion, provided real options, guaranteed justice,
and eased transitions. Instead of sequestering the poor in ghet-
tos, or by uprooting the poor and scattering them to the four
winds, Biblical Law urged mobility through opportunity. Aliens
and sojourners were to be treated with respect and compassion
(Exodus 23:9), Strangers were allowed to participate in the life
of the community (Exodus 20:10; Leviticus 16:29; Exodus 12:19).
Relief was afforded to all equally (Deuteronomy 24:17-22). In-
heritance laws mitigated against stasis and concentration and
encouraged moving out to the uttermost parts of the earth to
take dominion for Christ (Leviticus 25:8-55).

The difference then between Biblical faith and humanism is,
as Gary North has asserted, the difference between “Dominion
Religion” and “Power Religion.”

And that makes all the difference.

Conclusion

In times of economic distress, people very often pull up their
stakes and move on. That’s a distinctly American legacy.

Sometimes migration is caused by homelessness, as in the
case of Elz Weltzberg. Sometimes it causes homelessness, as in
the case of Anya Schiller. Either way, it causes upheaval, disrup-
tion, and disorientation, On the other hand, it allows people to
adapt to changing conditions and to pursue opportunity where-
ver it may lead.

For this reason, Biblical Law buffers the disadvantages and
highlights the advantages of migration rather than clamping
down on the situation with heavy handed controls. Humanism
doesn’t have that option, so it resorts to either containment or
resettlement, coercively keeping the poor in their place in the for-
mer, coercively putting the poor in their place in the latter. So in-
stead of alleviating the anguish of homelessness, the humanist
proposals only aggravate it.

In order to overcome homelessness, the migrant poor need

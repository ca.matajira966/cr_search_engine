TWELVE

THE GOLDEN SHIP:
THE SECULAR DRIFT

 

Jimmy stood out in the hallway fidgeting. Tears streaked his
cheeks. His head reeled. Not believing the task before him, he
reached for the door handle again—the tenth time in as many
minutes. He took a deep breath and entered the busy office.

The room had classic corporation lines, Clean. Efficient.
Modern.

“Well hello there, Jimmy. What can we do for you today?”
The sleek secretary was genuinely friendly. Her smile comforted
him a bit, and at the same time saddened him all the more.

“Uh... is Mr, Greenspan in his office? Do you think I can
see him for a minute?”

“He sure is. Let me buzz him. See if he’s got some time.”

Jimmy swallowed hard. What would he say? How would he
react?

Greenspan appeared at his office door and beckoned to the
tall, lanky youth. “Come on in, Jimmy, how are things?”

Jimmy wanted to turn and run. He hated this, Hated it.
Tears welled up in his eyes again. His throat tightened. He
fought for control, taking a seat in front of the man’s comfortable
and commodious desk. Looking around, a flood of memories
cluttered his consciousness. Just three months ago sitting here,
in this very room, in this very chair, he’d been given the chance
of his life. His dream come true. Now he had to throw it all

away.
“Mr. Greenspan, I have to... I uh, I have to.. .”
“What is it, Jimmy? Trouble?”
“I'm gonna have to . . . quit.” There. He'd said it.

“Do what?” The man was obviously taken aback. Flabber-
gasted.

165

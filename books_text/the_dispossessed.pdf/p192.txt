176 Tue DisrossxsseD

point of view, Luther and his followers needed the help of ‘godly
princes’ in order to protect them from Papal threats.

“Conflicts in Germany over the Reformation eventually led
to the formulation cuius regio, eius religio: whoever reigns, his
religion, The faith of a given region would be determined by the
religion of the ruling prince. At this point, Lutheranism in Ger-
many had become pretty much wholly statist in character, in
terms of any real independent power for the Church. Lutheran
acquiescence in the power of the state has continued to be a
problem for Christianity in Germany down to the present day,
and accounts for the passivity of the Lutheran Churches in the
face of Nazism.”

And of the other great magisterial reformation that took
place in England, Jordan writes, “Everybody knows that Henry
VII had less than pure motives in ‘reforming’ the English
Church. It is noteworthy that the first ‘reforming’ act of the new
Church was the elimination of two feast days from the medieval
calendar: the feast of the martyrdom of Thomas Becket, and the
day observed (o memorialize the public penance performed by
Henry II, who had been responsible for Beckct’s death. Becket
had stood against the power of the statc, and for the integrity of
Church government. The magisterial reformation in England
clearly set its face against any true Church government.”

Thus while the anabaptists were dropping out of sight alto-
gether, scuttling themselves into a cultural backwater and an
evangelical ghetto, and while the Swiss Reformers were striving
to cleanse and reform the “one, holy, catholic Church,” Luther
and his followers in Germany and England were falling into the
trap of statism.

Because the Roman system was so terribly and malignantly
corrupt, this centralization of power was actually an improve-
ment over the short run. The poor who were once wards of the
Church and who were now wards of the state certainly saw some
short term benefits. But in the long run, because the Scriptural
pattern had been abandoned, they suffered more than ever
under the collapsing rot of the late medieval Romanist system,
Two wrongs never make a right.

Unfortunately, the pattern was set. The dye was cast.
Luther’s three magisterial presuppositions became the fount
from which Western social policy would flow ever after.

7 ‘Tue DispossrsseD

Tt was agreed that relief was a right of citizenship. But before
Jong, the upper and middle classes began to grow resentful.
They wearied of meeting this entitled obligation. When relief
was a function of the Church and its ministry, it was dispensed
with charity, but now animosity and vindictiveness crept in.
Relicf programs became punitive. And again the poor suffered.

Before long, the situation was utterly abominable.”

The horrid conditions imposed upon the poor by the malad-
ministration of the various social welfare programs brought a
storm of protest and spawned a vast number of private alter-
native programs. Inspired by the writings of Adam Smith in the
1770s, of Joseph Townsend in the 1780s, of T. R. Malthus in
the 1790s,” and perhaps most especially of Thomas Chalmers in
the 1830s,7! a number of dedicated and compassionate social ac-
tivists entered onto the relief scene. In 1860, John Richard Green
founded the London Society for the Relief of Distress.” In 1862,
Frederick Denison Maurice established the Society of Christian
Socialism, attracting the avid support of John Ruskin, Thomas
Huxley, and Charles Dickens.’3 In 1869, Octavia Hill launched
the work of the remarkably influential Charity Organization
Society,# And contemporaneously the charitable enterprises of
Charles Haddon Spurgeon,” Florence Nightingale,”* and Lang-
don Lowe”? shaped the nature of compassionate ministry well
into the twentieth century.

Even so, after all was said and done, these private initiative al-
ternatives were widely regarded as supplements to, but never as
replacements for, the government’s system. As historian Charles
Kingsley reported, “The societies for relief and the christian char-
ities had then, as now, an honored place in the social welfare
structure but only in a subsidiary manner. Magistratal oversight
is an unquestioned reality. What with their parochial and pastoral
intents and purposes, the private works cannot hope to claim wide
public appeal beyond their present narrow contribution.” In
short, the three basic presuppositions first proposed by Luther re-
mained unchallenged despite a vigorous reform movement.

Into the Medern Era

Shortly after the onslaught of the Great Depression, both the
British and American governments initiated a number of new
reforms to combat hunger and homelessness. In England the

The Golden Ship: The Secular Shift 179

Beveridge Report, and in the U.S. the New Deal, ushered in
hundreds of new programs, projects, and administrations to care
for the swelling ranks of the destitute. Vast changes were in the
offing.

By the spring of 1933, when Franklin Delano Roosevelt
assumed the office of President, almost one-third of the Ameri-
can labor force was unemployed.”? Wages had plummeted to an
average of $17 a week.®? Immediately the President signed the
Emergency Relief Act, thus centralizing the apparatus of welfare
and vastly expanding its spectrum. By the following spring,
nearly twenty million people had gone on the dole for the first
time. They received food, lodging, and cash. They received
medical care and vendor goods. They received rehabilitative
training and public works employment. They received the most
comprehensive package of social benefits since the golden age of
the Roman and Incan Empires.®! Under the Determination of
Needs Act of 1941 a similar transformation shook the social ser-
vice apparatus in England. But notice, though the programs had
grown, developed, multiplied, and expanded, the basic presup-
positions upon which these programs had been built remained
unaltered. As Charles Murray has commented, “Conservative
mythology notwithstanding,” none of the 3(’s reforms “had much
to do with the purposes of welfare.” They merely “changed the
locus of the institutions that provided the welfare.”® The presup-
positions remained intact.

In 1964, President Lyndon B. Johnson ushered in another
social welfare revolution by declaring an “unconditional war on
poverty.”83 His reforms picked up where his hero Roosevelt’s left
off, Billions of dollars were poured into thousands of projects ad-
ministered by hundreds of agencies.

The “war on poverty” was driven by a passionate and idealis-
tic philosophy that offered a dramatic departure from previous
social welfare thought in a number of areas.

Roosevelt had argued that “continued dependence upon re-
lief induces a spiritual and mora! disintegration fundamentally
destructive to the national fiber. To dole out relief in this way is
to administer a narcotic, a subtle destroyer of the human spirit.
It is inimical to the dictates of sound policy. It is in violation of
the traditions of America.”®*

His successor Harry Truman often quipped, “No more soup

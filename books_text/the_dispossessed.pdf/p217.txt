Recoverable Enchantments: A Biblical Perspective 201

For the Lord God is a sun and shield; the Lord gives
grace and glory; no good thing does He withhold from
those who walk uprightly. O Lord of Hosts, how blessed
is the man who trusts in Thee (Psalm 84:1-12).

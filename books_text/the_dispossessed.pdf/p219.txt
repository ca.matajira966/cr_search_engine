FOURTEEN

THE
INSTRUMENTALITY
OF MANKIND:

A BIBLICAL PATTERN

 

God is just.

He works righteousness and justice for all (Psalm 33:5).
Morning by morning, He dispenses His justice without fail
(Zephaniah 3:5) and without partiality (Job 32:21). All his ways
are just (Deuteronomy 32:4) so that injustice is actually an
abomination to Him (Proverbs 11:1).

It is for this reason that Scripture continually emphasizes the
fact that God Himself protects the weak, the oppressed, the or-
phan, the widow, and the homeless. God’s justice demands that
those who are most vulnerable, most susceptible, and most in-
secure be defended. So, He cares for them. He doesn’t care for
them any more than He cares for others, for He is no respecter
of persons (Acts 10:34). But He does care for them, He most
assuredly will not tolerate any injustice. Thus, He is especially
adamant about ensuring the cause of the meek and the weak
(Psalm 103:6).

Time after time, Scripture stresses this important attribute of
God.

But the Lord abides forever; He has established His
Throne for judgment, and He will judge the world in
righteousness; He will execute judgment for the peoples
with equity. The Lord also will be a Stronghold for the op-
pressed, a Stronghold in times of trouble (Psalm 9:7-9).

203

210 Tue DispossesseD

after God’s work in us. Because the covenant és that pattern.

God's relationship with us is covenantal, He judges us cove-
nantally. He comforts us covenantally. He fellowships with us
covenantally. He disciplines us, rewards us, and cares for us cov-
enantally.

Theologian James B. Jordan describes the covenant as “the
personal, binding, structural relationship among the Persons of
God and His people. The covenant, thus, is a social structure.”?
It is the divine-to-human/human-to-divine social structure. It is
the legal means by which we approach, deal with, and know
God, It is the pattern of our relationship.

According to Scripture, the covenant has five basic parts.3 It
begins with the establishment of God’s nature and character: He
is both transcendant and immanent, redeeming for Himself a
people, Second, it proclaims God’s sovereign authority over that
people: They must obey Him, Third, the covenant outlines
God's stipulations, His law: The people have clear cut responsi-
bilities, Fourth, it establishes God’s judicial see: He will evaluate
His people’s work. And finally, the covenant details God’s very
great and precious promises: The people have a future, an
inheritance.

This outline of the covenant can be readily seen not only in
God’s dealings with Adam (Genesis 1:26-31; 2:16-25), Noah
(Genesis 9:1-17), Abraham (Genesis 12:1-3; 15:1-21), Moses (Ex-
odus 3:1-22), and the disciples of Christ (1 Corinthians 11:23-34),
but also in the two tables of the Law, the Ten Commandments
{two tables of five statutes)*, the structure of the Pentateuch (five
books), the Book of Psalms (five sections), the Book of Deuteron-
omy (five parts),® the Book of Revelation (five stages),6 and
many other passages of Scripture, both Old and New Testa-
ments.” Again, this is because God deals with us covenantally.
That is how He works in our midst.

The covenant is the paéfere of our relationship with God. It is
the context for His service.

Since we are to do unto others as He has done unto us, we
obviously need to deal with one another in terms of the cove-
nant. We need to build our human relationships around the five
basic parts of the covenant. We need to do as He has done. We
need to serve covenantally.

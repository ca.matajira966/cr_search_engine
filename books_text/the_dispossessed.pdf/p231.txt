The Instrumentality of Mankind: A Biblical Pattern 215

Diligence is required because diligence is blessed with pros-
perity.

Family participation is required because families are the
basic building blocks of society (1 Timothy 5:8).

Even more than these though, obedience is required. Submis-
sion to the standards of the Kingdom is required. In order to take
advantage of the covenant privileges, a man must be in the cove-
nant or dependent on the covenant. Even when the Church
reaches out into the streets, and lanes, and hedgerows, drawing
in the dispossessed, responsibility must be enforced.

Jesus said,

When you give a luncheon or a dinner, do not invite
your friends or your brothers or your relatives or rich
neighbors, lest they also invite you in return, and repay-
ment come to you. But when you give a reception, invite
the poor, the crippled, the lame, the blind, and you will
be blessed, since they do not have the means to repay
you; for you will be repaid at the resurrection of the
righteous. And then one of those who were reclining at
the table with Him heard this, he said to Him, “Blessed
is everyone who shall eat bread in the Kingdom of God!”
But He said to him, “A certain man was giving a big din-
ner, and he invited many; and at the dinner hour he sent
his slave to say to those who had been invited, ‘Come,
for everything is ready now.’ But they all alike began to
make excuses. The first one said to him, ‘I have bought a
piece of land and I need to go out and look at it; please
consider me excused’ And another one said, ‘I have
bought five yoke of oxen, and I am going to try them
out; please consider me excused.’ And another one said,
‘I have married a wife, and for that reason I cannot
come.’ And the slave came back and reported this to his
master. Then the head of the household became angry
and said to his slave, ‘Go out at once into the streets and
lanes of the city and bring in here the poor and crippled
and blind and lame.’ And the slave said, ‘Master, what
you commanded has been done, and still there is room.’
And the master said to the slave, ‘Go out into the high-
ways and along the hedges, and compel them to come in,

218 THE DispossEssED

By grace we are “created in Christ Jesus for good works,
which God prepared before hand, that we should walk in them
(Ephesians 2:10).

In other words, God sheds His grace upon us, so we are to be
gracious to others. We are to do unto others as God has done un-
to us. This truth is inherent even in the word “grace.” The Greek
is “charis,” from which we get our word “charity.” Grace and
charity. They are inseparable concepts.

This principle is illustrated beautifully in Christ’s parable of
the Good Samaritan.

“A certain man was going down from Jerusalem to
Jericho; and he fell among robbers, and they stripped
him and beat him, and went off leaving him half dead.
And by chance a certain pricst was going down on that
road, and when he saw him, he passed by on the other
side. And likewise a Levite also, when he came to the
place and saw him, passed by on the other side. But a
certain Samaritan, who was on a journey, came upon
him; and when he saw him, he felt compassion, and
came to him, and bandaged up his wounds, pouring oil
and wine on them; and he put him on his own beast, and
brought him to an inn, and took care of him, And on the
next day he tock out two denarii and gave them to the
innkeeper and said, “Take care of him; and whatever
more you spend, when I return, I will repay you.’ Which
of these three do you think proved to be a neighbor to the
man who fell into the robbers’ hands?” And he said, “The
one who showed mercy toward him.” And Jesus said to
him, “Go and do the same” (Luke 10:30-37).

In this story, Jesus uses an outcast, a Samaritan, as an exam-
ple of perfect virtue, He strictly observed the Law, shaming the
priest and Levite who “passed by on the other side” (Luke
10:31-32). He paid attention to the needs of others (Deuteron-
omy 22:4) and showed concern for the poor (Psalm 41:1). He
showed pity toward the weak (Psalm 72:13) and rescued them
from violence (Psalm 72:14), Knowing the case of the helpless
{Proverbs 29:7), he gave of his wealth (Deuteronomy 26:12-13),
and shared his food (Proverbs 22:9).

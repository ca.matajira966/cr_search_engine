220 Tue DispossesseD

are to do unto others as He has done unto us. We are to dispense
justice through service, within the covenant, by grace.

When we fail to serve, the state steps in and dominates, hurt-
ing the poor, setting them back. Farms are foreclosed on.
Women are used and abused. The feebleminded are driven fur-
ther and further away from reality. And the homeless are dispos-
sessed all the more. They are left to fend for themselves, out in
the howling wilderness.

When we fail to serve within the context of the covenant,
again, the state steps in and tosses the poor between two ex-
tremes, Either they are smothered with promiscuous entitle-
ments, stripping them of their dignity and their incentive, or
they are penalized with discriminatory and arbitrary welfarism.
Either way they lose. They are still left with no Home.

Finally, when we fail to serve within the context of the cove-
nant graciously, the state steps in and imposes welfare as a citizen-
ship benefit. Charity is lost. Compassion is lost. The personal
care of the Scriptural Good Samaritan model is swallowed up by
the bureaucratic care of the lords of the Gentiles model (Luke
29:25-30).

That is why God didn’t tell the government to feed the hungry, clothe
the naked, and shelter the homeless. He told us to. He told the
Church.

Service. Covenant. Grace. Doing unto others as God has
done unto us.

PART ONE

 

LIFE ON THE
STREET

Faust: First, I will question thee about heli. Tell me,
where is the place that men call hell?

Mephistopheles: Under the heavens.
Faust: Ah, but whereabouts?

Mephistopheles: Within the bowels of these elements,
where we are torturd and remain for ever: Hell hath no
limits, nor is circumscrib’d in one self place; for where we are
is hell, and where hell is, there must we ever be: and to con-
clude, when all the world dissolves, and every creature shall
be purified, all places shall be hell that are not heaven.

Faust: Come, I think hell’s a fable.

Mephistopheles: Ay, think so still, till experience change
thy mind.

Christopher Marlowe
Doctor Faustus, Act X

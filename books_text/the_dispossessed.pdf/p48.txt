32 Tur Dispossrssen

listless, aimless world, void of hope, ambition, direction, or
bonds. It is a world populated by the remnant casualties of path-
ology, psychosis, perversion, and privation.

As early as 1890, Jacob Riis identified the chronically
vagrant as the culprit in 80% or more of the crimes against prop-
erty and person.’ In 1960, they accounted for more than 50% of
all arrests, and in 1968 for 38% .°8 For the chronically vagrant,
life is an endless cycle of arrest, detention, arraignment, convic-
tion, incarceration, release, and re-arrest punctuated only by
periodic psychiatric confinements and sprees of drunkenness.

“I’ve worked with hardcore street people for over 27 years,”
confided Dr. Ambrose Polk, “and quite frankly, there is very lit-
tle that we can do for them. Keep them out of trouble, maybe.
Keep their medical problems down to a bare minimum. That
sort of thing. But as far as long term recovery. . . well, the
prognosis is not too terribly good.”

According to Dr. Elbert Hillerman, another sociologist who
has devoted his carer to the chronically homeless, “It is next to
impossible to help anyone who really does not desire help. The
most that we can do is to try to protect society from their irre-
sponsible antics, and perhaps more frequently, to protect them
from themselves.”©

He goes on to conclude, “It is indeed fortunate that their
numbers are few. Ants always outnumber grasshoppers, the
careful outnumber the slothful. Thank God for that.”®

Temporary Homelessness

Despite the popular conception that most of the homeless are
these hardcore vagrants and derelicts, the evidence says other-
wise. A vast majority taste only briefly the grapes of wrath. Most
were, until the economic and political cataclysms of the seven-
ties, solidly entrenched in the work force, actively pursuing the
“American Dream.” Many were skilled industrial workers.

“In Tulsa,” says Roland Chambless, the Salvation Army
Commander there, “most of the people we fed a year ago were
derelicts and alcoholics, but today it’s unemployed oil field work-
ers, mothers, and small children . . . families.”®

Sergeant E. D. Aldridge of the Houston Police Department’s
Special Operations Division has said, “It used to be that most of
the homeless on the streets were alcoholics and things like that.

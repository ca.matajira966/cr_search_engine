36 ‘Tue DispossEssep

Clearly it is a problem that has grown and grown and grown
to monstrous proportions. Just as clearly it is a problem that
eludes nice, neat, easy definition.

The worst of it is that this gargantuan, elusive crisis at hand
is not just impersonal facts and figures. It is not just numbers. It
is not just messy charts and graphs marring the immaculate and
impeccable record of our once proud chambers of commerce.
This crisis is people. It is people— drawn into a common stew of
indignity and sheer animal horror. It is people—mocked and
jeered by a kind of devil’s comedy. It is people — tossed to and fro
by the waves of passion and travail far beyond our imagining,
just the flotsam of the general ruin of ugliness and want. It is
people—needful not of our specchless pity nor our heart-racked
sympathy, but our diligent charity.

There is a crisis at hand.

It demands our best efforts. It requires our greatest care, our
deepest commitment and our surest love.

Because “it” is “them.”

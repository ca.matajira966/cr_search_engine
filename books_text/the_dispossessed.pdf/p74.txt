58 Tue DispossesseD

Prolixin. “Staples in the trade,” he confided to me over his shoul-
der, “Can’t imagine where we'd be if we didn’t have the drugs to
hold them at bay. They're all crazy, you know.”

Raul dropped his prescription in the bag as we passed. Glar-
ingly, he called out to me:

Half a league, half a league, Half a league onward,
All in the valley of Death rode the six hundred! Forward
the Light Brigade! Charge for the guns! he said, and into
the valley of Death rode the six hundred.§

The guard just smiled. “That’s Raul for you. Yes, sir,” he
chuckled again, “That’s Raul for you. Only talks in poems.”

According to all the best estimates, a darge percentage of all
the dispossessed on our cities’ streets are feebleminded, insane,
delirious, or mad. Like Raul.

In 1980 the homeless men in Manhattan’s sheltering system
were carefully examined in an extensive study conducted by the
N.Y. State Office of Mental Health in conjunction with the N.Y.
City Human Resources Administration. The screening, which
extended over several days, focused primarily on the psychiatric
condition of the residents. The findings were astounding. Fully
70% of the men were diagnosed as mentally disordered to some
degree; 60% of them, moderately or severely so and 9% were
found to be in need of immediate hospitalization,’

Similar studies in St. Louis,* San Francisco,® Philadelphia,”
Chicago,"' Boston,!? Denver,'? and even in England! and Can-
ada! confirm those findings. A clear coincidence of chronic
homelessness and mental illness is characteristic of the dispos-
sessed everywhere.

The National Institute of Mental Health asserts that at the
very least, a third of the nation’s homeless suffer from social and
emotional dysfunction.!® The Heritage Foundation,” the Com-
munity Service Society of New York,'!* and the National Coali-
tion for the Homeless'9 all maintain that the percentage is even
higher. “There can be no question,” says Arlene Murdoch of
New York’s Office of Mental Health, “a large majority —a very
large majority—of the homeless strect dwellers in this nation
have lost more than just a place to stay; they have lost their san-
ity as well.””

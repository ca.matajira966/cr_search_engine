62 Tue Disrosskssen

between the increasing number of helpers and the increasing
number of those who need help. The more psychologists we
have, the more mental illness we get; the more social workers
and probation officers, the more crime; the more teachers, the
more ignorance. One has to wonder at it all. In plain language,
it is suspicious.”

Here is an establishment psychologist, a tenured professor of
educational psychology at a prominent Ivy League University
who is actually suggesting that “psychology and other social
sciences might be doing actual harm to our society,” that in fact
they may be terribly “destructive.

The evidence seems to support Dr. Kilpatrick’s claims.

As early as 1952, Hans Eyseneck reported that “neurotic peo-
ple who do not receive therapy are as likely to recover as those
who do.”3§ Modern methods of psychotherapy, he found, were
“not any more effective than the simple passage of time.”5” A
flurry of new studies immediately after confirmed his results.

Later, Eugene Levitt of the Indiana University School of
Medicine released a study that showed that “disturbed children
who were not treated recovered at the same rate as disturbed
children who were,”

Still later, in the extensive Cambridge-Somerville Youth
Study, researchers found that “uncounseled juvenile delinquents
had a lower rate of further trouble than counseled ones.”

A series of other reports, including the recent Rosenham
studies, have indicated that “mental hospital staff could not even
tell normal people from genuinely disturbed ones,” and that “un-
trained lay people do as well as psychiatrists or clinical psycholo-
gists in treating patients.”#

As for the “wonder” drugs that psychotherapists have so freely
dispensed over the last twenty years, the evidence suggests that
they too do more harm than good.

Thorazine, the most commonly used anti-psychotic, anti-
emetic agent today, has a whole host of second and third degree
effects that patients must endure. It provokes “drowsiness, dizzi-
ness, lethargy, a dulling of pain and other conditioned reflexes,
increased appetite, blurred vision, diplopia, headache, nasal
stuffiness, dry mouth and skin, constipation, urinary retention,
hypothermia, peripheral edema, endocrine disturbances, reacti-
vation of psychotic symptoms, bizarre dreams, hyperglycemia or

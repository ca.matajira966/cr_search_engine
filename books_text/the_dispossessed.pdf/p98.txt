62 ‘THe DrspossesseD

The Legacy of the West

Despite much feminist rhetoric to the contrary, women in
Western Civilization have always been held in the highest
regard. Rarely have they been reduced to the kind of pauperism
and deprivation that they are facing today. And when they were,
it was due primarily to great political, social, economic, or philo-
sophical upheavals, soon to be corrected. The Scriptural founda-
tions of the culture necessitated reform whenever inequity
became apparent.

Tn the fourteenth century the simultaneous transformations
wrought by defeudalization and mercantilism were calamitously
aggravated by a scourge of panic, famine, and death when the
bubonic plague swept over Europe.%? Between one-quarter and
one-third of the entire population perished and another third
was displaced.°? Women especially suffered, widowed and with-
out the protection of either the feudal manor or the mercantile
guild.% But within two decades, due to the combined efforts of
the Church and the “wheels of commerce,” women were once
again brought under the protective cover of society, honored as
“the weaker vessel.”

The great worldview shifts in the fifteenth century with the
onset of both the Reformation and the Renaissance, and in the
eighteenth century with the onset of the Industrial Revolution
brought great difficulties to women and their children. But
again, the surging Christianization of Western culture quickly
remedied the basest abuses and set into motion reforms that
quelled the rising tide of the feminization of poverty.

During the Reformation for instance, the great charitable
works established by Calvin in Geneva and Knox in Scotland
quickly spread throughout the continent of Western Europe and
reached deep into Eastern Europe and portions of Asia and
Africa as well.%7 And the great revivals sparked by Whitefield
and Wesley in the eighteenth century catalyzed massive cultural
and legislative reforms—in England through the sponsorship of
Wilberforce® and in America through the sponsorship of Web-
ster and Clay.™ Due to the activism of the Church and the high
profile of Scripture, orphans, widows, and destitute women were
cared for and protected. Afftuence was certainly never the norm,
but then neither was the feminization of poverty.

If Western history can be taken as any sort of guide then, the

 

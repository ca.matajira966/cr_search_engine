Economic Value: Objective and Subjective 63

ments that compare the quantity of capital in one country with that in
another is a convenient and relatively harmless fiction.” He politely
dismisses Mises’ argument by means of Mises’ other arguments. Yet
he is too polite; such a comparison is morc than a harmless fiction,
In terms of the logic of subjective economics, it is nothing short of a
subterfuge, a sleight-of-hand deception to be used by capitalism’s
defenders to dismiss the arguments of their socialist opponents.
Most capitalists point to capitalism’s productivity as a major defense
of capitalism, yct the logic of modern economics denies that such a
conclusion can be reached using the logic of subjective value theory.
Consistent subjectivism denies the validity of all such comparisons.

Hayck has stated that “cvery important advance in economic
theory during the last hundred years was a further step in the consis-
tent application of subjectivism.”** He wrote those words in 1952. It
would seem that we have reached the end of the road, or at least a
major fork in the road, for subjectivism. It, too, has run directly into
the implications of its own presuppositions. Pure subjectivism makes
lonely solipsists of us all, with no way for us to test our generaliza-
tions or compare the products of our hands, let alone the products of
billions of other human beings. When Mises wrote that “the macro-
economic concept of national income is a mere political slogan
devoid of any cognitive value," he simultancously denied the
validity of all statistical comparisons of the productivity of nations,

 

including his own comparisons.

All of this may seem like academic hair-splitting, as indeed it is.
All scholarship, all intelligent pursuit of truth, eventually gets in-
volved in hair-splitting. But the point I am trying to make is not
merely technical; it is absolutely fundamental, Purely “objective”
theories of value produce incongruous conclusions, so the promoters
of such theories have always returned to the market forces of supply
and demand to explain prices. But the markct's evaluation of price
and valuc is not’stable, since conditions change. It is therefore not an
objective source of value. On the other hand, subjective value
theory’s success in explaining the way in which the market operates
has not overcome ihe inherent contradictions of radical subjec-
tivism. Economic theory in a purely subjectivist mold cannot

43, Kirzner, “Theory of Capital,” p, 142.

44. Hayek, Counter-Revolution of Science, p. 31.

45. Mises, The Ultimate Foundation of Economic Science (Princeton, New Jerscy: Van
Nostrand, 1962), p. 87. Reprinted by New York University Press.

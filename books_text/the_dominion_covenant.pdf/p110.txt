5
GOD’S WEEK AND MAN’S WEEK

Thus the heavens and the earth were finished, and all the host of them.
And on the seventh day God ended his work which he had made; and he
rested on the seventh day from all his work which he had made. And God
blessed the seventh day, and sanctified it: because that in it he had rested
from all his work which God had created and made (Gen. 2:1-3).

We come now to a topic which is exceedingly controversial
theologically, and has important implications for modern business
practices. A full consideration of its business implications must be
deferred until the exegesis of Exodus 20:8-11. At this point, it is morc
important to consider the sabbath in relation to Adam and his do-
minion responsibilities.

We are told that at the end of the sixth day, God saw everything
that He had made, and that it was very good (Gen. 1:31). The whole
ercation was without a flaw. By “whole creation,” I mean the earth,
the inhabitants of the earth, and the physical celestial bodies. We are
not explicitly informed about the condition of the angelic host. We
are not told that Satan had fallen, along with his followers, although
some Christian expositors have assumed that this event had already
taken place prior to the sixth day, perhaps even before the creation of
the earth in Genesis 1:1,1 But we know that the physical creation was

1. Since we are not told specifically about the creation of the angelic host, we can
only speculate about the time, or pre-time, of their creation. But since the angels are
often associated with the stars (Jud. 5:20; Dan. 8:10; Matt. 29:29; Jude 13; Rev,
1:20; 3:1; 6:13; 8:12, etc.), and the stars were created on the fourth day, it is reason-
able to assume that this was the day of the creation of the angels, whose purposes
include the worship of God and service to man. If this is the case, then the rebellion
of the satanic host probably occurred on the morning of the first sabbath, just prior
to man’s rebellion. We are certainly not told in the Bible of any function of the angels
that necessarily pre-dated the creation of the physical universe. Their close associa-
tion with man’s purposcs points to their inclusion in the week of the creation. Satan
could inherit Adam’s lawful inheritance if he could successfully place Adam beneath

66

 

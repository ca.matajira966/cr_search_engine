82 THE DOMINION COVENANT; GENESIS

This is why inflation of the money supply has been a feature of
human history from the beginning of our records. Governments
cheat.

Honest civil governments are not the creators of money, they
are, at most, the certifiers of money. This is why the Bible again and
again warns about the sin of fraudulent weights and measures.?
When Jeremiah bought the field from his kinsman, he “subscribed
the evidence, and sealed it, and took witnesses, and weighed him the
money in the balances” (Jer. 32:10). The money in this case was
seventeen shekels of silver (Jer, 32:9). The debasement of the cur-
rency is nothing less than tampering with the weights and measures,
whether done by private coin clippers, counterfeiters, or State offi-
cials. The universal abolition of the gold standard in the twentieth
century after World War I led directly to universal inflation, revolu-
tion, and boom-bust trade cycles in the same historic period. There
is no escape from the moral laws of God, whether or not the hired
professional economists recognize such a moral order’s existence.
The gold coin or silver coin standard, or multiple coin standard of
freely exchangeable currencies, is the direct result of biblical Jaw.
The abolition of honest weights and measures, through the creation
of fractional reserve banking, printing press money, coin debase-
ment, or coin clipping, must inevitably result in unpleasant social
and economic repercussions. When someone issues a receipt for
metal of a certain fineness and weight, he must have just exactly that
on reserve. To issue more warehouse receipts (bank notes) than
there is metal on reserve is nothing less than tampering with the
scales, for the results are identical to coin debasement. It is the same
sin; it must result in the same judgment. We live in a universe which
is personal and governed by moral law. Economic crises are the
built-in self-regulating devices—built into man and the ercation—
that restrain men in the pursuit of cvil. Dishonest weights, dishonest
money, dishonest authorities, and dishonest cultures go together.
And with them go disasters.

Conclusion

Though the Roman Empire is dust today, its gold and silver
coins still can be exchanged for scarce economic resources. The

3, R. J. Rushdoony, Institutes of Biblical Law (Nutley, New Jersey: Craig Press,
1973), pp. 468-72. See Lev. 19:35/f.; Deut. 25:13, 15.

98 THE DOMINION COVENANT: GENESIS

tion? Why should we expect to see the peacemakers succeed in at-
taining supremacy in a political order in which the quest for total
power is the obvious inducement to enter the political process?

Conclusion

The harmony of interests is unquestionably a biblical standard.
It is that social standard which existed in Eden, exists for the institu-
tional church (I Cor. 12:12-17), exists now in heaven, and shall exist
in the New Heavens and New Earth (Isa. 65:17-25), The entrance of
sin into the world disrupted this world, but God has provided in-
stitutions that restrain such disharmony. The free market is one of
these institutional arrangements that promote cooperation, even
among those who do not agree on first principles. Class warfare,
which is the ideological foundation of Marxism and the modern
trade union movement, is foreign to biblical standards of morality.
All things are reconciled in Christ (Col. 1:20; Eph. 2:11-16; Jas.
2:1-9), including the supposed eternal struggle between classes. The
opening words of Marx’s Communist Manifesto (1848) are familiar to
most students of the history of socialism: “The history of all hitherto
existing society is the history of the class struggles.”!! Marx never did
succeed in defining just what a class is. He never completed the third
volume of Das Kapital, the last three paragraphs of which are devoted
to the consideration of this crucial topic, “What constitutes a class?”!2
But even if he had succeeded in defining “class” accurately, within
the framework of his own work, he would have been incorrect. The
history of all societies is not class warfare, but ethical warfare against a
sovereign God, and the working out of men’s salvation and damnation
over time. The history of mankind is the history of the extension of
the covenant of dominion. History is theocentric, not humanistic,
Bloody warfare of man against man began with Cain and Abel; the
origin of such warfare is man’s ethical rebellion against God. As
James put it: “From whence come wars and fightings among you?
Come they not hence, even of the lusts that war in your members?”
(Jas. 4:1).

Redemption eventually will triumph over rebellion, and the

1, Karl Marx and Frederick Engels, Manifesto of the Coramunist Party (1848), in
Marsx-Engels Selected Works (Moscow: Foreign Languages Publishing House, 1962), I,
p. 34,

12. Marx, Capital: A Critique of Political Economy (Ghicago: Charles H. Kerr & Co
Co-operative, 1909), IIL, The Process of Capitalist Production as a Whole, ps. 1031,

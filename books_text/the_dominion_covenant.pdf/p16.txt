xvi THE DOMINION GOVENANT!: GENESIS

Nothing less than this is acceptable to me as a lifetime goal. A com-
prehensive restructuring of every academic discipline is mandatory.
The only model adequate for such a restructuring is the biblical cav-
enant. Christian scholars must self-consciously adopt methadologi-
cal covenantalism as their epistemological foundation. Neither phi-
losophical nominalism. (individualism and subjectivism) nor realism
(collectivism and objectivism!”) can serve as consistent, reliable
foundations of human thought, including economics. This economic
commentary can serve as a model for how other academic disciplines
can and should be restructured. We need similar commentaries in
many other fields.

Obviously, if a comprehensive Christian revival does not take
place in the future, this publishing project will be regarded by future
historians as an expensive oddity produced by an eccentric, assum-
ing that historians ever come across a relatively complete set of the
documents produced by this eccentric. Without a worldwide revival,
this commentary will become, at best, a set of primary source docu-
ments potentially useful to some doctoral candidate’s dissertation at
one of the world’s less prestigious universities. I have better things to
do with my life than to provide primary source materials, free of
charge, to some myapic doctoral student in search of the quickest
and easiest way to get out of graduate school and into the sheltered
and unremunerative world of college teaching, assuming he can get
a teaching job at all. I am laying the biblical-theolagical foundations
for a restructuring of world civilization, not the foundations of a doc-
loral dissertation in history. I have confidence that I will succeed in
my goal. I am confident because I am a Calvinistic postmillennialist
who knows that God has foreordained the worldwide triumph of His
gospel.

 

Eschatology and Personal Motivation

Postmillennialism is an important motivation to those scholars
who are self-consciously dedicated to long-term Christian Recon-
struction, People frequently ask me, “Does it really make much
difference what eschatology a Christian holds?” And I answer: “It

17. By “objectivism,” I am not referring to the peculiar atheist cull movernent
originated by the novelist Ayn Rand. For critique of Randianism, see John Rob-
bins, Answer to Ayn Rand (Washington, D.C.: Mount Vernon Publishing, 1974). For
an amusing autobiography of an ex-Randian, see Jerome Tuccille, é Usually Begins
With Ayn Rand: A Libertarian Odyssey (New York: Stein & Day, 1972).

The Burden of Time ig

ethical standing before God. The essence of life is right standing ethically
before God, and Adam had forfeited life.

The burden of time was placed on his physical body. His ycars
would now be limited. He would be given a fixed amount of time to
work out either his salvation or damnation with fear and trembling.
Infinite time, apart from regeneration, was forbidden to him, for
God knew that Adam would prefer the burden of eternal time rather
than eternal judgment, where none of God's unmerited temporal
gifts is available. Without judgment at the cnd of his days, man is
not motivated Lo face the full implications of his ethical rebellion
against God.

Time is therefore central to any philosophy of life and death.
Men desperately wish to escape the burdens of time, yet they fear
death’s cessation of temporal existence. The meaning of time is an in-
escapable concomitant to any consideration of the meaning of life.

The ancient world, apart from the Hebrews, believed in some
version of historical cycles. Nature’s seasonal changes were regarded
as normative. The world continues through endless cycles. Hesiod’s
poem, Works and Days, which was written al about the same time that
Isaiah’s ministry began, was one Greek’s speculation about the rise
and fall of civilizations and even the creation itself. It began with the
age of gold, degenerated to the age of silver, and continued through
the age of brass.! Ours is the dead age of iron, he said. His language
was similar to the visionary dream of King Nebuchadnezzar: the
great image which was made of gold, silver, brass, and iron mixed
with clay. But the end of that image was total destruction by the
stone cut without hands, which smashed the image, and then grew
into a great mountain which filled the carth (Dan. 2:31-35). The
kingdoms of man will be replaced by the eternal kingdom of God
(Dan. 2:36-45), History in the biblical outline is linear, not circular.

The ultimate uniformity in all pagan systems of thought, whether
cyclical in nature, or evolutionary development, or the static and
fundamentally unchanging structure of “pure being,” cannot be
challenged successfully by any pagan deity. The central uniformity
is the sovercign; gods and men must conform themselves to this fun-
damental sovereign. Both the gods and mankind are in bondage to
it. Man must submit to its power. Time ts the god of paganism, and
chance is his throne.

1. Hesiod, Werks and Days, lines 109-201.

The World Trade Incentive 155

true that free trade will make more obvious the real economic costs
of cutting off such exchange through military conquest, or attempts
at conquest. It is built into post-Babel society that men, though scat-
tered abroad, though divided by language and culture, though heirs
of very different historic traditions, will always be faced with an cco-
nomic lure to increase their productivity by trucking and bartering.
Those who refuse to trade thereby reduce the size of their market,
and as Adam Smith said’so long ago, the division of labor is limited
by the extent of the market. By reducing its own national division of
labor, a society reduces its per capita income, for it has necessarily
reduced its per capita output. No society can choose to trade less
without bearing the costs of forfeited per capita income. ‘Trade
brings added wealth,

Conclusion

The dominion covenant impels men to extend their contral over
the earth. The curse of the ground limits the productivity of solitary,
autonomous men. The scattering of Babel has reduced the ability of
central planning bureaucracies to substitute socialist allocation for
voluntary exchange. The unity of mankind can be expressed
through trade, but the diversity of cultures and environments pre-
vents this economic unity from becoming the foundation of a bu-
reaucratic one-world State. Unity and diversity are held in balance,
or at least not tipped so far as to allow either to become totally
destructive of society. The subduing of the earth can continue, there-
fore, by the operation of all these factors: the dominion covenant,
the curse of the ground, the scattering of mankind, and frce trade.
When Viner chose as the title for his lectures, “The Role of Provi-
dence in the Social Order,” he had the right idea.

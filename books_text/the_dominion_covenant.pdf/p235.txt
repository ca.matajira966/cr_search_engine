 

The Uses of Deception 191

confidence Isaac had that his blessing, as a patriarch in the covenant
line, would have historical impact in time and on earth. He had confi-
dence in his own word. There is no doubt that he did have confidence
in his own word. It was an important word. It was so important that
Jacob and Rebekah had to use deception in order to be assured that
this word by Isaac would be applied in the way that God had proph-
esied. His word was so important that he could tell Esau that fe had
transferred long-term authority to Jacob (Gen. 27:37). The power of
his word was nevertheless dependent on his faith in God, and it took
a conspiracy against him to make certain that his verbal blessing ac-
tually conformed to the announced intention of God to bless Jacob.
It took deception, in other words, to bring Isaac’s words into line
with the God to whom Isaac was officially committed, but whose
own words were being defied in practice by Isaac.

One conjecture made by critics of Jacob’s deception is that he
was repaid, like for like, when his uncle Laban deceived him into a
marriage with Leah, and then was able to compel Jacob to work an
additional seven years to pay for Rachel (Gen. 29:23-28), He, too,
used a disguise to trap the victim. The Bible says nothing about any
“like for like” retribution being involved in this incident. We are told
that Jacob faithfully served Laban six additional years. So faithfully
did he serve, in fact, that Laban begged him to remain as an admin-
istrator of his flocks after the seven years were over (30:27-28), Fur-
thermore, despite Laban’s continued deceptions, it was Jacob who
prospered (31:1-13). It is true that Jacob had been deceived, but he
wound up so wealthy that his own wives, Laban’s daughters, were
viewed by Laban’s household as strangers (31:5). Laban had used up
the capital of the family, leaving the daughters with no inheritance
(31:5), God had transferred Laban’s wealth to Jacob's household
(31:16).

It should be clear enough for anyone who examines the record of
Jacob's sojourn in the household of Laban that Laban’s deceptions
resulted in the opposite outcome from what he had intended. He
knew that God was with Jacob, which is why he hired him after the
second seven years were up (30:27). Yet he persisted in numerous
deceptions, trying to gain economic advantage with respect to Jacob
(31:7). He lost his daughters, his wealth, and even his household
idols (31:19). Yet when he confronted Jacob, he had the audacity to
assert that everything Jacob owned was his (31:43). (The idols were
his, but he never found them: 31:33-35.) Jacob told his uncle just

General Introduction to The Dominion Covenant xxvii

This confidence is resented by pessimillennialists who are
equally confident that the gospel will not triumph in history, that the
church will fail in its God-assigned mission of worldwide evangel-
ism, and that Christians are historical losers. They deeply resent the
fact that Reconstructionists implicitly are calling them to full
accountability for their present cultural impotence every time we
assert that the gospel of Jesus Christ can and will be triumphant in
history, when Christ’s followers at last become covenantally faithful
to Him by obeying His law through the empowering of the Holy
Spirit. Pessimillennial pietists believe that Reconstructionists are
pointing an accusing finger at them for the failure of the church in
our day. They are correct; we are. But at least we are polite cnough
to use only our index fingers, unlike the response we have received
from several of our critics.

Reconstructionists also believe that God’s law establishes the
basis of truth in every area of life. This means that Christians who
are passively or even vocally content to let the humanists run the
world are acting immorally, for they are implicitly accepting the
satanic lie that something other than God’s law is the standard in his-
tory. We reply that God’s law is the only standard, now and in eter-
nity? Again, they clearly recognize that we Reconstructionists are
calling them moral weaklings for failing to assert the crown rights of
King Jesus in every area of life. We are not criticizing them because
they have not figured out clear answers to all of life’s problems. Our
complaint is not that they presently lack sufficient knowledge to ex-
ercise dominion. Intellectual problems can be overcome through
hard study and hard work in applying what has been learned. We
are criticizing them because they implicitly and explicitly deny that
Christians alone have al their disposal the only possible sources of
truth in history, the only possible guidelines for God-pleasing moral
behavior in any area of life, and the only possible foundations of vic-
tory in history: the ethical mind of Christ (I Cor. 2:16), His inspired
written Word (II Tim. 3:16), and the Holy Spirit.

Our critics understand that by saying that the church of Jesus
Christ is not doomed by Bible prophecy to be progressively impotent
in history, Christian Reconstructionists are also saying that the
sceming impotence of the church today must be the responsibility of

33. Greg I.. Bahnsen, By This Standard, Bahnsen, Theonomy in Christian Ethics (2nd
ed.; Phillipsburg, New Jersey: Presbyterian & Reformed, 1984).

236 THE DOMINION COVENANT: GENESIS

thoughts after Him—cven as God imputes value to His creation
(Gen. 1:33). In their activities as acting agents responsible before
God for their thoughts and actions, men assign value to objects,
including money. Money does not measure value; on the contrary, acting men
impute value to money, As external conditions change, or men’s evalua-
tions of external conditions change, they may well shift their value
preferences away from money to something else, or from one form of
money to a newly popular form. In Egypt, men learned that they
could not eat gold. They could not induce other men to exchange
food for gold. The fact that they could not eat gold does not, of
course, mean that they could have eaten paper money, credit cards,
or certified checks from prestigious banks.

What we must recognize is that money does not have intrinsic value.
Only the Word of God has intrinsic value— permanent, unchanging
value imputed to those words by God Himself, who is the source of
value. God is self-attesting, self-sustaining, and absolutely autono-
mous. No human device, including gold and silver, possesses intrin-
sic value. All human devices are transitory; all shall pass away ex-
cept God’s Word (Matt. 24:35). Gold and silver have demonstrated
for millennia that men evaluate their value in relatively predictable,
stable ways. This stability of purchasing power over time and geo-
graphical boundaries testifies to the historic value of these monetary
metals, but historic value is no? the same thing as the hypothetical in-
trinsic value ascribed to the metals. Some people may use the term
“intrinsic” when they really have in mind only the concept of Aistorte
value, but other people are actually quite confused about the concept
of intrinsic value. They assume, for example, that some sort of long-
term relationship of “16 to one” exists between the exchange value of
silver and gold; 16 units of silver being equal to one unit of gold. No
such relationship exists, except on a random basis, in a free market.
Ne permanent exchange value can exist between twa or more economic abjects.
Human. action is subject to change, and exchange values are no exception to this
law of human life.

By ‘intrinsic value,” we must limit our discussion to market goods
and services. We are speaking of market value. Of course, God im-
putes intrinsic value to this or that aspect of the creation. He imputes
intrinsic value to the souls of His people. He evaluates the intrinsic
evil of His enemies. But when we come to values imputed by acting men
to market phenomena, there is nothing which possesses intrinsic value —
nothing, in short, which remains a created constant within the frame-

Conclusion 241

No need to spend money on warmed-over, baptized humanism: go
straight to the source. Ga next door and take your economics courses
from the Keynesians at the University. Let the University hire the
faculty, buy the library books, and worry about budget deficits. Let
the college concentrate on the courses that are exclusively Christian,
such as missions, evangelism, and how to run a church. If the
students think they need “secular” courses, let them get their secular-
ism from the certified humanists, at tax-subsicized tuition levels.
Why drain the funds of the kingdom — the “Christian courses”—by
importing third-rate humanists to teach the students? The president
of the college never said any of this, of course, but there is no doubt
in my mind that he was operating in terms of a world-and-life view
similar to what I have described. So do Christian college presidents
everywhere. The difference is only that he was close enough to a
state university, and smart enough, to make use of its humanistic op-
portunities. The students who took the college’s advice and enrolled
part-time at the University probably did not corrupt themselves
intellectually any more than the thousands of students in Christian
colleges do, five days a week, when they take classes in baptized
humanism. If anything, the Christian student at a state university
might be more alert to humanist propaganda than the student in a
“Christian’s” classroom. He knows he is getting his humanism
straight. The student at the Christian college doesn’t.

Genesis gives us the foundations of Christian thought. It tells us
of the Creator-creature distinction. It tells us of the dominion cove-
nant. It tells us of the Fall of man and the curse of the ground. We
learn that we are stewards of Gad’s property, fully responsible to
Him for the proper administration of His goods. At the samc time,
we learn that we are also legitimate owners, in time and on earth,
during the period of our stewardship. God entrusts His property to
individuals and organizations, and they are called to increase the
value of this property. Wealth is therefore not an innate evil, but a
means of opportunity for godly service, as we learn in the case of
Abraham. Wealth is preserved and expanded by means of character
and lawful stewardship (Abraham, Jacob, Joseph), and it is lost by
those with poor character and no respect for Gad’s law. (Lot, Laban,
Esau).

Genesis teaches us that there is a curse on the ground. This means
that we must cooperate with other men to increase our per capita
wealth. There is a division of labor principle, as well as the law of

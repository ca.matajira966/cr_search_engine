278 THE DOMINION GOVENANT: GENESIS

of creating new forms of life. The General Electric Company has
even filed patents on one new life form, and an appeals court in 1979
upheld the firm’s property right to this new species. The “gene
splicers” are in our midst. Several books have been written, gener-
ally by non-scientists, all based on published scientific data, warn-
ing about the hazards of such research. These warnings are unlikely
to stop the experimental mania of modern biological scientists. The
technological imperative is too strong: “If it can be done, it must be
done.”°8 The hope of profits also lures research firms into the field.
Pharmaceutical firms are financing numerous projects in the field of
DNA research. Financial success, which is likely over the short run
at least, will bring in the competition. Recombinant DNA, the tool
of the “gene splicers,” discovered in 1973, has opened a true
pandora’s box of moral, intellectual, medical, and legal problems. °
As one popular book on the subject warns: “‘Man the engineer’ may
soon become ‘man the engineered.’”!°! They go on to cite recent
statements by biological scientists that are in line with everything
that has been said since the days of Thomas Huxley: “Over these
past three billion years, one hundred million species have existed on
this planet. Of those, ninety-eight million are now extinct. Among
the two milion that remain today, only one, Homo sapiens (‘wise
man’), has evolved to the point of being able to harness and control
its own evolutionary future, Many biologists welcome this possibil-
ity, seeing it as a great challenge that will ennoble and preserve our
species. ‘Modern progress in microbiology and genetics suggests that
man can outwit extinction through genetic engineering, argues Cal
Tech biologist James Bonner, ‘Genetic change is not basically im-
moral. It takes place all the time, naturally. What man can do, how-
ever, is to make sure that these changes are no longer random in the
gigantic lottery in nature. . . . Instead, he can conirol the changes
to produce better individuals.’ Bonner’s viewpoint is seconded by Dr.
Joseph Fletcher, professor of Medical Ethics at the University of
Virginia School of Medicine, who sees in genetic engineering the
fulfillment of our cosmic role on earth, ‘To be men,’ he believes, ‘we

98, The idea of the technological imperative is the foundation of the critical
book by Jacques Ellul, The Technological Society (New York: Vintage [1964] 1967).

99. Wall Street Journal (May 10, 1979). Article by Marilyn Chase,

100, John Lear, Recombinant DNA: The Untold Story (New York: Crown, 1978).

101. Jeremy Rifkin and Ted Howard, Who Should Play God? (New York: Dell,
1977), p. 14.

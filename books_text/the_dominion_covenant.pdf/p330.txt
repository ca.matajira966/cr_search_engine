 

286 THE DOMINION GOVENANT? GENESIS

reading of Genesis; or even that he had furnished impressive scien-
tific authority for the nineteenth-century habit of thinking in terms
of wholes and continuities rather than in discrete parts and rigidities;
or that the evolutionary orientation stressed context and complexity
— though all of these influences could be bothersome when used by
‘materialists.’ The worst threat of all was that Darwin’s universe
operated not by Design but by natural selection, a self-regulating
mechanism... . Natural selection pictured the world in a constant
process of change, but without any prior intention of going any-
where in particular or of becoming anything in particular. This was
a devastating proposition to the conventional theologian — more so,
perhaps, than the Copernican theory had been, because it struck so
close to home. Natural selection therefore seemed, to many, hope-
lessly negative, fraught with blasphemy and conducive of despair.”145
This despair was initially covered by optimism concerning the power
of man to take over the direction of the evolutionary process, an op-
timism which still survives, though not without fear and foreboding
on the part of some tists and philosophers, in the late twentieth
century.

Appleman’s paint, however, is well taken. “So it made a difference to
philosophers and theologians that man not only evolved, but evolved
by natural selection rather than by a vital force or cosmic urge of
some sort, Darwinism seemed uncompromisingly non-teleological,
non-vitalist, and non-finalist, and that basic fact could not help but
affect the work of philosophers. ‘Once man was swept into the evolu-
tionary orbit,’ Bert James Lowenberg has written, ‘a revolution in
Western thought was initiated, Man was seen to be a part of nature,
and nature was seen to be a part of man. The Darwinian revolution
was not a revolution in science alone; it was a revolution in man’s
conception of himself and in man’s conception of all his works.’ ”"6
Appleman chronicles the decline in the opposition to Darwinism on
the part of Roman Catholics and other theologians. “The activities of
science, relentlessly pushing back the margins of the unknown, have
in effect been forcing the concept of ‘God? into a perpetual retreat
into the still-uanknown, and it is in this condition that ‘God’ has fre-
quently come to have meaning for modern man.”!!?

 

115. Philip Appleman, “Darwin: On Changing the Mind,” Epilogue in Appleman
(ed.), Darwin, pp. 636-37.

116. Ibid., p. 637.

117, Ibid., pp. 638-39.

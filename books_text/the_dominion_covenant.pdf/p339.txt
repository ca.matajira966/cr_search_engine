From Cosmic Purposelessness to Humanistic Sovereignty 295

Darwinists is not all that clear. Generally, they have countered the
Social Darwinists in the name of a higher reason, a collective human
reason. Man is the capstone of an unplanned evolutionary process.
He has transcended this undirected process, or at least may be about
to transcend it. Through conscious planning, elite members of the
race will be able to integrate the plans of all the members into an
overarching whole, and this overarching whole will guarantee the
survival of all, including the “least fit,” who might otherwise be pre-
pared to kill off the “mutants.”

What other approach would be better? If you believed that you
are a “mutant” —an expert, a rich man, the member of the planning
elite—wouldn’t you come before the “about-to-be-superseded”
masses and tell them that you are “just one of the boys,” and “we're
all in this together,” and that we all need to buckle down “for the sake
of humanity”? In other words, wouldn’t you devise a social philoso-
phy which would promise to the masses sufficient benefits to guaran-
tee their survival in the competition? Would you continue to shout
them down as members of an about-to-be-superseded species, and
would you tell them that it is their responsibility to play the game by
your ferocious rules or get off the playing field, when getting off the
field means death? If you were really a mutant, then the one thing
you would not have is numerical superiority. The one thing you
could not risk would be a head-on collision with the massive num-
bers of “about-to-be-superseded” voters, troops, or whatever. You
would make your pitch in terms of the greatest good for the greatest
number. And you would tell the masses that the greatest good for the
greatest number involves playing the game by your rules, which on
the surface seem to be democratic, but which in fact are radically
elitist. You would deny that blood lines count, or that the feudal
principle is valid. You would offer them democracy, bureaucracy,
universal free education, welfare redistribution, and so forth. Then
you would select only those members of the masses who showed
themselves willing and able to compete in terms of the elitist system.
You would give a few of them scholarships to the best universities,
and you would recruit them into what they believe (and you may
even believe) is “the inner circle.” You would expand the power of the
government, and then you would open high-level positions in that
government only to those specially chosen by the ruling power.
What you would do, in short, is to construct precisely the statist
system which exists today in every major industrial nation—a sys-

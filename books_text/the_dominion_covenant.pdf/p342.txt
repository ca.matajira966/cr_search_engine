298 TIE DOMINION GOVENANT: GENESIS

ment and all supernatural religion. It is diflicult to say which he hated
more, although religion received the more vitriolic attacks. Dynamic
Sociology stands as the first and perhaps the most comprehensive
defense of government planning in American intellectual history. It
was published about 15 years too early, but when his ideas caught on,
they spread like wildfire. In fact, they became the “coin of the realm” in
planning circles so rapidly that the source of these ideas was forgotten.
Because the book is almost unknown today, and because Ward’s con-
cepts and language are so graphic, I am providing an extended sum-
mary and analysis of his thought. In Dynamic Sociology we have the
heart and soul of modern, post-Darwin social evolutionist philosophy.
Ward did not pull any punches. He did not try to evade the full impli-
cations of his position. Modern thinkers may not be so blatant and
forthright, but if they hold to the modern version of evolution — man-
directed evolution — then they are unlikely to reject thc basic ideas that
Ward. sels forth. If you want to follow through the logic of man-
directed evolution, you must start with Ward’s Dynamic Sociology.
Ward was forthright. He made it clear that the cnemy is revealed
religion, which in the United States in the early 1880's, meant Chris-
tianity. In the 82-page introduction to the book, in which he outlined
his thesis, Ward announced that those people claiming to have
received divine inspiration, and those who have founded religious
systems, have been found by modern medicine to be not only
“pathological” but to be burdened by “an actually deranged condition
of their minds.”? Because of the power these religious leaders have
wielded historically, “we can only deplore the vast waste of energy
which their failure to accomplish their end shows them to have
made.”'*3 (Waste, above all, was what Ward said his system of social
planning would avoid.) There is no evidence, he wrote in volume
two, that religion provides any moral sanctions whatever. As a matter
of fact, we find in the advanced countries thal individuals whe avow
no religion are the true moral leaders. “The greater part of them are
found among the devotees of the exact sciences. Yet there is nov more
exemplary class of citizens in society than scientific men. . . .”#
Furthermore, the “criminals and the dangerous classes of society are

142. Lester Frank Ward, Dynamic Sociology; or Applied Social Science, as Based Upon
Statistical Sociology and the Less Complex Sciences (New York: Appleton, [1883] 1907), 1,
p. 12, (Reprinted by Johnson Reprints and Greenwood Press.)

143. Ibid, 1, p. 17.

144, {6id,, TL, pp. 281-82.

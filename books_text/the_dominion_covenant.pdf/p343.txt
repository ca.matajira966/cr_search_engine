From Cosmic Purposelessness to Humanistic Sovereignty 299

generally believers in the prevailing faith of the country which they
infest... .”445 In any case, morals precede religion. “It is morality
which has saved religion, and not religion which has saved morality,”146
Prayer is a social evil, because it is “inconsistent with that independ-
ence and originality of mind which accompany all progressive move-
ments.”!47 It deters effective action, He then devoted several pages to
a demonstration of the anti-progressive influences of all religion, but
he provided examples primarily from paganism and animism. * He
said religion leads to a retreat from this world and a divorce between
man and nature, !*° There are two methods for modifying the exter-
nal world to make it conform to man’s needs; science and religion.
There is a perpetual conflict between these two methods, and
religion will lose this war. 15°

Ward's second intellectual enemy was Social Darwinism. The
Secial Darwinists have misunderstood evolution, he argued.
Nature’s ways are not man’s way. The progress of nature is too slow,
and it is so inefficient that earth’s resources will not be able to sup-
port such slow progress forever. What is needed is “something swifter
and more certain than natural selection,” and this means man.45! We
need a new éeleology, he argued—the crucial argument of all post-
Darwin social and even biological evolutionists. The evolutionary
process nceds a sure hand to guide it. We must adopt, he said at the
end of the second volume, “the teleological method.”'5? We must re-
ject Social Darwinism (although he never used this phrase to desig-
nate his opponents). Here is the familiar and absolutely central argument of
modern evolution, predictably formulated first by a social scientist
rather than a natural scientist: “Again, it becomes necessary to com-
bat the views of those scientists who, having probed deep enough to
perceive how nature works, think they have found the key to the way
man should work, thus ignoring the great distinguishing characteris-
tic of intellectual labor. Having found the claims of those who believe
that nature is a product of design and outside contrivance to be un-
sound, they conclude that there is no design or contrivance, and

145. féid., II, p. 282
446, Jhid., TL, p. 283
147. Ibid,, IL, p. 286
148, Ibid, IT, pp. 2878.
149. Ibid., IL, p. 298.
150. Zid, II, p. 305.
151, fbid., I, p. 16.
152, Tbid., I, p. 627.

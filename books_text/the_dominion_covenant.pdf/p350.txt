306 THE DOMINION COVENANT: GENESIS

or at least heavily influence private education, Therefore, with
respect to private education, “The less society has of it the better, and
therefore its very inefficiency must be set down as a blessing.”!* The
radical elitism here should be obvious, but Ward was kind enough to
spell out the implications (something later elitist evolutionists have
not always been willing to do). “Lastly, public education is im-
measurably better for society. It is so because it accomplishes the ob-
ject of education, which private education does not, What society
most needs is the distribution of the knowledge in its possession.
This is a work which can not be trusted to individuals. It can neither
be left to the discretion of children, of parents, nor of teachers. It is
not for any of these to say what knowledge is most useful to society.
No tribunal short of that which society in its own sovereign capacity
shall appoint is competent to decide this question.”!85 Are there to be
teachers? Yes, but very special kinds of teachers, namely, teachers
totally independent from “parents, guardians, and pupils. Of the lat-
ter he is happily independent. This independence renders him prac-
tically free. His own ideas of method naturally harmonize more or
less completely with those of the state.”!84 True freedom, true in-
dependence, is defined as being in harmony with the State. This, of
course, is the definition of freedom which Christianity uses with
respect to a man’s relation to God.

Was Ward a true egalitarian, a true democrat? Did he really
believe that the masses would at last reach the pinnacle of knowl-
edge, to become equal with the scientific elite? Of course not. Here
is the perennial ambivalence of the modern evolutionists’ social
theory. Society needs planning and direction, and “society” is mostly
made up of individuals, or “the masses,” So they need direction, They
need guidance. They cannot effectively make their own plans and
execute them on a free market. Téleology is too important to be left to the
incompetent masses, acting as individuals on a free market. They simply are
not intelligent enough. “Mediocrity is the normal state of thc human
intellect; brilliancy of genius and weight of talent are exceptional.
. . . This mass can not be expected to reach the excessive standards
of excellence which socicty sets up. The real need is to devise the
means necessary to render mediocrity, such as it is, more comfort-

184, Idem.

189. fbid., I, p. 591.

186. Ibid., I, p. 590, Cf, Ward, “Education,” (1871-73), in Karier (ed.), Shaping
the American Educational State, pp. 145-59.

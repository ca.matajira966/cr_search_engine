320 ‘HE DOMINION COVENANT: GENESIS

mechanism, its freely fluctuating price system, and its system of eco-
nomic calculation for private individuals. The price that post-
Darwin evolutionists are asked to pay, religiously speaking, is simply
too high. In short, the defenders of the free market have priced themselves out of
secular humanism’s marketplace of ideas.

This is not to say that every modern economist is self-consciously
a defender of the kind of planning outlined by Lester Frank Ward.
Not very many economists are éhat confident about centralized eco-
nomic planning. This is also not to say that the majority of men, or
even a majority of trained social scientists, understand fully the
sleight-of-hand operation of modern cvelutionism, with its shift from
purposcless origins to man-directed evolutionary process. Never-
theless, the climate of opinion in the twentieth century is strongly in-
fluenced by this sleight-of-hand operation, and ils conclusions re-
garding the sovereignty of planning over collective mankind have
permeated the thinking of those who probably do not fully under-
stand the epistemological and metaphysical presuppositions of these
conclusions. The fact is, autonomous men want their godhead
unified, and the hydra-headed, impersonal, spontaneous institution
we call the free market is not sufficiently conscious and purposeful to
satisfy the longings of modern men for cosmic personalism, meaning
humanism’s version of cosmic personalism, meaning deified Man.

In conclusion, we cannot hope to succeed in making a successful
case for the free market by using the logic of Kant, the logic of Dar-
win, or the logic of Miscs, Hayek, Friedman, and other Kantian
Darwinists. We cannat hope to convert modern evolutionists to the
free market, if we ground that defense in terms of a less consistent
version of evolutionism. The alder Darwinist heritage simply does noi gain
large numbers of adherents, precisely because modern evolutionists are involved
in a religious quest for man-directed cosmic evolution, and this quest is at odds
with the logic of decentralized markets. Yf the case for the free market is to
be successful in the long run, it must be made in terms of a fully con-
sistent philosophy of creationism and theocentric cosmic per-
sonalism. The case for the free market must be made in terms of the
doctrines of divine providence, biblical revelation, the image of God
in man, and the covenant of dominion. While this intellectual
defense may not impress today’s humanistic evolutionists, including
Christian scholars whose methodology is still grounded in humanis-
tic evolutionism, it will enable Christians to have a foundation that
will survive the predictable disruptions of the economic, political,

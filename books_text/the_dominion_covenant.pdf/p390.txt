346 YHR DOMINION GOVENANT: GENESIS

dismisses trial and error as a standard of economic success. This
is too purposeful a process. It involves “conscious adaptive behav-
ior.”*7 This allows far too much importance for the decisions of act-
ing men. Trial and error, he asserts, cannot serve as a success in-
dicator in a changing environment. “As a consequence, the measure
of goodness of actions in anything except a tolerable-intolerable
sense is lost, and the possibility of an individual’s converging to the
optimum activity via a trial-and-error process disappears. Trial and
error becomes survival or death. It cannot serve as a basis of the indi-
vidual’s method of convergence to a ‘maximum’ or optimum position.
Success is discovered by the economic system through a blanket-
ing shotgun process, not by the individual through a converging
search.”34 Success is discovered by the economic system. Survival is
the sole criterion. The aggregate process screens the survivors. There
ts nothing rational or purposeful about this process. It is altogether im-
personal.

What is left? Jmitatton.39 Men seek profits. The economic system
screens out the successful imitators and innovators from the unsuc-
cessful. “The economic counterparts of genetic heredity, mutations,
and natural selection are imitation, innovation, and positive
profits.”*° Alchian does not deny purposeful actions on the part of in-
dividuals, he says, but he asserts that “the precise role and nature of
purposive behavior in the presence of uncertainty and incomplete in-
formation have not been clearly understood or analyzed. It is
straightforward, if not heuristic, to start with complete uncertainty
and nonmotivation and then to add elements of foresight and
motivation in the process of building an analytical model.”#"

Gary Becker, who acknowledges his intellectual debt to Alchian’s
article, has attempted to do just this. In a pathbreaking essay —
although nobody has followed him down his path— Becker argues
that it is not necessary to assume that men act purposefully or ra-
tionally in order to conclude that aggregate markct demand curves
are negatively inclined (that is, that at lower prices, people in the ag-
gregate will purchase more of the scarce resource in question). We
do not need to assume that either individuals or households are eco-

37. Bid., p. 30
38. Zbid., p. 31.
39. Ibid., p. 29.
40, Ihid., p: 32.
41. Tbid., p. 34.

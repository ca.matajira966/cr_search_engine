xl THE DOMINION GOVENANT: GENESIS

and the doctrine of creation is basic to the covenant. Any downplay-
ing of the doctrine of God's creation of the universe out of nothing in
six literal 24-hour days is an implicit attack on the covenant. It is not
a coincidence that Darwinism denied the creation and also denied
any covenantal system of personal responsibility of man under God.
This is why Darwinism swept the world: nineteenth-century men
wanted to escape their sense of covenantal responsibility before God.
(See below, pages 250-53.)

2. Hierarchy/Authority

I agree entirely with Rushdoony on this point: “The second char-
acteristic of Biblical law is that it is a éeaty or covenant.” Man has
been placed by God over the creation. In Chapter Two, we see that
the sun, moon, and stars were created after the earth and the plants
were, They were created to serve the needs of man, primarily as
chronological devices. This hierarchy is basic to the covenant struc-
ture: under God and over the creation: “And God said, Let us make
man in our image, after our likeness: let them have dominion over
the fish of the sea, and over the faw! of the air, and over the cattle,
and over all the carth, and over every creeping thing that crawleth
upon the earth” (Gen. 1:26). This was repeated to Noah after the
flood (Gen. 9:1-3).

What representatives of modern pictistic fundamentalists have
denied, in their desperate attempt to retreat from personal responsi-
bility, and in their implicit alliance with power-seeking humanists
who seek dominion,* is that this covenant involves hicrarchy. They
argue that while men get to exercise dominion over nature, there is
no element of human hierarchy in these verses. This, of course, is
theological nonsense: God placed Adam over his wife, and the par-
ents over the children. He places church officers over congregation
members, and civil magistrates over citizens and local residents.
There can be no dominion over nature without human hierarchies. The ques-
tion then is: Whose covenant law will rule these hicrarchies, man’s
or God's?

Throughout European history, we have seen similar doctrines of
a world without hierarchy. In the early radical religious revolution-

 

 

5. R. J. Rushdoony, Institutes of Biblical Law (Nutley, New Jersey: Graig Press,
1973), p. 7.

6. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler,
Texas: Institute for Christian Economies, 1985), pp. 2-6.

Cosmologtes in Conflict: Creation vs. Evolution 369

Fall of man and the Incarnation of the Son of God in Jesus Christ,
their view of the universe was understandably geocentric. But they
took the Ptolomaic construction of the universe as physically geocen-
tric as a valid representation of the ethical geocentricity of carth in the
creation, The earth was understood as round. (The incredible por-
tolano maps of the middle ages rival the accuracy of modern maps;
they were probably pre-Phoenician in origin. }*! But it was supposedly
placed at the center of a huge system of translucent spheres, to which
the sun, planets, and stars were attached, all rotating in perfect
spherical harmony around the earth. While the existence of comets
should have warncd them against the translucent spheres, it did not.
Galileo’s telescopes, not comets, smashed these spheres.

Some commentators, such as J. B. Bury, have argued that this
geocentricity gave men a sense of importance and power in the
universe. This was supposedly destroyed by the advent of modern
astronomical theories.52 Others, such as Arthur O. Lovejoy, have
argued just the opposite: the earth was seen as the garbage dump of
the universe, with hell at its center. “It is sufficiently evident from
such passages that the geocentric cosmography served rather for
man’s humiliation than for his exaltation, and that Copernicanism
was opposed partly on the ground that it assigned too dignified and
lofty a position to his dwelling-place.”° The fact secms to be that
man’s escape from the geocentric universe could be viewed either as
a contraction of man’s physical (and therefore historical) place in
creation, or as an elevation, ethically, because of one’s cscape from
the wrath of the God of the formerly confined creation. On the other
hand, men might view the universe as majestically huge, and there-

51. Charles H. Hapgood, Maps of the Ancient Sea Kings (Philadelphia: Chilton,
1966). This is one of the most starding books ever published. Ignored by professional
historians and geographers, it produces evidence that accurate maps vf the world,
including Antarctica, were available to explorers in the sixteenth century, probably
in the twelfth century, and very likely long before the Phoenecians. Antarctica was
not rediscovered —discovered, given the standard textbook account— until the eigh-
teenth century. The book is an eloquent rebutal of cultural and historical evolution-
ists: if anything, it indicates cultural devolution, No wonder it is ignored by modern
scholars!

52. J.B. Bury, The Idea of Progress (New York: Dover, (1932] 1955), p. 115. The
book first appeared in 1920.

53. Lovejoy, Great Chain, p. 102; cf. Alexander Koyré, From the Closed World to the
Infinite Universe (Baltimore: Johns Hopkins Press, [1957] 1970), pp. 19, 43. This
garbage-dump cosmology was an Aristotelian conception of the world: Great Chain,
p. 104,

 

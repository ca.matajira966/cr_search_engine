Cosmologies in Conflict: Creation vs. Evolution 371

Kepler, the mathematical genius who discovered that planetoid
motion is elliptical, was a sun-worshipper and an astrologer.®? The
leaders of the institutional church understandably were disturbed by
these thevlogically and cosmologically heretical individuals.

The debate over whether or not the universe is infinite is still
with us today. Einstein’s curved (in relation to what?) and finite uni-
verse is obviously not in harmony with the absolute space of
Newton’s cosmology. Prior to the sixteenth century, however, Euro-
pean scholars had not raised the question. Aristotle’s rejection of the
idea was considered final. The problem is exceedingly intricate, as
anyone understands who has attempted to struggle through Alex-
ander Koyré’s book, From the Closed World to the Infinite Universe (1957).
Copernicus and Kepler rejected the idea, although their speculations
vastly expanded men’s vision of the creation. Galileo, whose tele-
scopes shattered the transluscent spheres as comets never had, was
content to affirm an indeterminate universe. Descartes, who above
all other men of his era, believed in a totally mathematical universe,
and whose vision in this regard was crucial for the development of
modern science, said that space is indefinite. He was always cautious
on theological or semi-thcological topics. The limit, he thought, may
well be in our minds; we should therefore avoid such disputes. In
fact, Descartes’ refusal to postulate limits (due to men’s inability to
conceive such limits) really served as an assertion of an infinite
space.*! Descartes’ god was simply pure mind, having nothing in
common with the material world.®

Henry More (not Sir Thomas More), in the latter part of the
seventeenth century, was converted to a belief in an infinite void space,
identifying this with God’s omnipresence. The limited material
universe is therefore contained in this infinite void. Space is eternal,
uncreated, and the necessary presupposition of our thinking. He
identified the spatiality of God and the divinity of space.® Space is
an attribute of God in this perspective—a dangerous linking of
Creator and creature. (This position, by the way, was also held by
Jonathan Edwards in his youth.®*) More is not that crucial a figure
in the history of Europe, but his opinion on the infinity of space was

 

 

60. Zbid., pp. 56-58, 69. Kepler’s Platonism was tempered by his Christian faith.

61. Koyré, Closed World, p. 124.

62. fhid., p. 122.

63. Ibid., pp. 150-53,

64. R. J. Rushdoony, This Independent Republic (Fairfax, Virginia; Thoburn
Press, [1964] 1978), p. 6. Rushdoony cites Edwards’ youthful notebooks: "Notes on
Natural Science, Of Being.”

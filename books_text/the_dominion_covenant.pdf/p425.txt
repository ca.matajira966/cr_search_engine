Cosmologies in Conflict: Creation vs. Evatution 381

forces are evenly balanced. “If we ask what of significance has hap-
pened in this expanse of time, the answer is, ‘Nothing’ There have
been no unique events. There have been no stages of growth. We
have a system of indifference, of more or less meaningless fluctua-
tions around an eternal mean.”%! As Walter Cannon points out, this
is not developing time— the time of the modern historian. It is simply
unlimited, meaningless time. We might say that his impersonal time
is like an infinitely Jong geometrical line, composed of an indefinite
number of identical points. Uniformitarian time does not, in or of
itself, give us a theory of evolution, for evolution implies growth,
and the eighteenth-century world machine could not grow, It was a
gyroscope, not a seed. But it was an exceedingly old gyroscope, and
that was to prove crucial.

There is a distinctly religious impulse undergirding uniformitar-
ianism. Hiseley is correct when he says that Hutton was proposing
an anti-Christian concept of time. Charles C. Gillispie concludes
that “The essence of Huttonianism lay not in specific details of
weathering, denudation, or uplift, but in its attitude towards natural
history.”8? Consider what Hutton was saying. On the basis of his
own limited wanderings and observations around Edinburgh, Hut-
ton announced a new theory of change to the world. In doing so,
modern commentators have concluded, he created the first truly his-
torical natural science, geology. Hutton challenged the biblical ac-
count of Noah’s flood, the researches and conclusions of the Neptun-
ists and the more cataclysmic Vulcanists, and concluded that what
he had seen—slow, even imperceptible geological change—is all
men now know. Furthermore, we can assume that such impercepti-
ble change is all any man can know —~ past, present, and future. Since
he had never seen the universal flood, obviously no one has ever seen
one. His operational presupposition was about as sophisticated as
the opinion of the Soviet Union cosmonaut who announced, after re-
turning from a few revolutions above the earth’s atmosphere, that he
had not seen God up there! What Hutton imposed, all in the name
of rational historical insight, was the most arrogant and blatant form
of what historians call “the tyranny of the present.” What was true in

91. Walter F. Gannon, “The Basis of Darwin's Achievement: A Revaluation,”
Victorian Studies, V (1961); reprinted in Philip Appleman (cd.}, Darwin: A Norton Criti-
cal Edition (New York: Norton, 1970), p. 42.

92. Charles Coulston Gillispie, Genesis and Geology (New York: Harper Torch-
book, [1951] 1959}, p. 83

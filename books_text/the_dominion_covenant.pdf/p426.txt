382 THE DOMINION COVENANT: GENESIS

Edinburgh in 1780 was true for the whole world throughout endless
eons of time. If any other historical data refute such a claim—the
Bible, the almost universal pagan myths concerning a universal
flood, the astoundingly precise calendars of the Babylonians and
other ancient cultures, the equally astounding Babylonian astro-
nomical records—then they must be disregarded as insufficiently
historical. History is what we can observe here and now, not what
primitive people used to think they were observing. Or, as Van Til
has put il, “what my net won’t catch isn’t fish.” Yet what Hutton and
his endless troops of defenders have claimed is that he alone was truly
empirical, truly concerned with the “facts.” But no fact is allowed
which seems to come into direct conflict with Hutton’s deeply reli-
gious presupposition that rates of change today have always existed,
or at the very least, that we have no evidence that indicates that the
rates of change have ever been different.

The prolix, unreadable writing of James Hutton did not con-
vince men to. believe in the uniformitarian religion. It was not the
testimony of the rocks near Edinburgh that converted the world to a
theory of an ancient earth. It was rather the built-in desire of men to
escape the revelation of a God who judges men and societies, in time
and on earth, as well as on the final day of judgment. They prefer to
believe in the tyranny of the present because the past indicates the
existence of a God who brings immense, unstoppable judgments
upon sinners. Afen prefer the tyranny of the present lo the sovereignty of God.
Nothing less than a deeply religious impulse could lead men to ac-
cept a presupposition as narrow, parochial, and preposterous as the
theory of uniformitarian change. Hutton announced, “today Edin-
burgh; tomorrow the world—past, present, and future,” and men
rushed to join the new anti-millennial religion. Like the Soviet cosmo-
naut, Hutton just could not see any sign of God in the Edinburgh
rocks, and those were the rocks men soon wanted.

James Hutton is long forgotten, except by specialists in the his-
tory of geology. But his most famous follower, Sir Charles Lyell, can-
not be ignored, for it is Lyell’s book, Principles of Geology (1830-33),
which gave Charles Darwin his operating presuppositions, The son
of a botanist, Lyell was by profession a lawyer. He studied geology
on the weekends. He was in his early thirties when his multi-volume
work was published, and it became an instant classic — indeed, the

Cosmologies in Conflict: Creation os. Evolution 387

of Enlightenment thinkers, especially Frenchmen. By the 1750’s, this
perspective was becoming a part of the European climate of
opinion.!©2 The idea of stages of historical development fascinated
the writers of the day. The cosmological evolutionary schemes of
Kant and Laplace were discussed as serious contributions, and
Maupertuis and Diderot, the French secularists, offered theories of
biological development—“transformism.”!** Three important fea-
tures were present in these new theories; without these theoretical
axioms, there would-have becn no reason to assume the evolutionary
perspective. First, change (not stability) is “natural”—one of the key
words of the Enlightenment. '* Second, the natural order is regular;
nature makes no leaps. This is the doctrine of continuity (aniformitar-
janism). Finally, in the late eighteenth and early nineteenth centur-
ies, the method of investigation selected by the progressivists was the
comparative method. Classification preceded the demonstration of evo-
lationary change. !05

Classification: this was all-important. Because of the influence of
the Greek concept of the chain of being, men had long regarded all life
as a harmonious interdependence of every species, from God at the
top of the chain (or ladder) to the lowest creature. (This presented
problems in theory: Are Satan and his angels therefore metaphysi-
cally necessary for the operation of the cosmos? Is Satan at the bot-
tom of the scale because of his ethical depravity, or just under God
Himself because of his metaphysical power? In fact, if he is totally
evil, can he be said to have true existence at all? Questions like these
destroyed the jerry-built “medieval synthesis” of Greek philosophy
— itself sel{-contradictory ~ and biblical revelation: Yet even in the
eighteenth century, much of the original potency of the concept of
the “great chain of being” remained.) But this chain of being was

 

102. J. B. Bury, The Idea of Progress (1920), is a standard account of sccular
optimism.

103. Bentley Glass, “Maupertuis, Pioneer of Genetics and Evolution,” and Lester
G. Grocker, “Diderot and Eighteenth Century Transformism,” in Glass (ed.), Fore-
runners of Darwin,

104. On the importance of the word “nature” to the eighteenth century, sve Carl
Becker, The Heavenly City of the Bighteenth-Century Philosophers (Ithaca: Cornell Univer-
sity Press, 1932). On the way in which “natural history” was used, see Robert A.
Nisbet, Social Change and History, ch, 4, It meant, essentially, conjectural history, that is,
how cvents would automatically develop “naturally” if there were no “artificial”
restraints on them. Developmentalism was w become biological evolutionism in the
nineteenth century.

105. Frederick J. Teggart, Theory of History (Yale University Press, 1925), pp. 129-32.

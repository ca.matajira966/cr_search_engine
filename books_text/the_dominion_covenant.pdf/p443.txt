Cosmologies in Conflict: Creation vs. Evolution 399

mineteenth-century society (and perhaps today).

The matter might have ended there, an obscure footnote in some
obscure Ph.D. dissertation (which is the fate of most scholarly ar-
ticles published in obscure academic journals), had it not been for
Darwin’s willingness Lo bring his Origin of Species to a conclusion. It
was published on November 24, 1859, and it sold out the entire edi-
tion of 1,250 copies in one day.!?° ‘his must have surprised the
publisher, John Murray, who had begged Darwin to write a book on
pigeons instead.}2? The reading public, which had purchased 24,000
copies of Vestiges of Creation, in marked contrast to the subscribers to
the Journal of the Linnean Society, obviously was in tune to the times.
(Or, in Darwinian terminology, was better adapted to the intellec-
tual environment.)

There can be no question about the book’s impact. It launched
an intellectual revolution. Many historians and scientists have tried
to grasp this instant success, and few can. It was an unpredictable
fluke, by human standards. Thomas Huxley remarked years later
that the principle of natural selection was so clear, so obvious, that
he could not understand why he had not thought of it before. This
was the reaction of most of the academic community. For about a
year, the reviews in professional magazines were hostile, One excep-
tion—“by chance”— was the review in the Times, which had been
assigned to a staff reviewer, and had in turn been referred to Huxley
when he had decided that it was too technical for him to review.
Thus, the December 26, 1859 review was very favorable.128 Yet at
first it had not appeared that Darwin’s victory would prove so easy.
Huxley wrote much later: “On the wholc, then, the supporters of
Mr. Darwin's views in 1860 were numerically extremely insignifi-
cant. There is not the slightest doubt that, if a general council of the
Church scientific had been held-at that time, we should have been
condemned by an overwhelming majority.”1?9 By 1869, the Church
scientific (except in France) was in Darwin’s camp.'2°

Darwin knew in 1859 just what is needed to pull off an academic
revolution: younger scicntisis and the support of laymen, He went
after both, and he won. As he wrote to one correspondent within two

126. Life & Leiters, UL, p. 1.

127. Himmelfarb, Darwin, p. 252.
128. {bid., p. 264.

129. Life & Leiters, 1, p. 540.

130. Himmelfarb, pp. 304-9.

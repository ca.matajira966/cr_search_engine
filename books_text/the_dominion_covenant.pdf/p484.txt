440 THR DOMINION COVENANT: GENESIS

of pride (Isa. 14:12-15). Such a path leads to destruction (Isa.
14:16-23). Man is supposed to think God’s thoughts after Him, not
attempt to be an autonomous creature. When man becomes humble
in all his ways before God, victory is within his grasp, in time and on
earth: “And it shall come to pass, if thou shalt hearken diligently
unto the voice of the Lorp thy God, to observe and to do all his com-
mandments which I command thee this day, that the Lorp thy God
will set thee on high above all nations of the earth: And all these
blessings shall come on thee, and overtake thee, if thou shalt hearken
unto the voice of the Lorp thy God” (Deut. 28:1-2). Or, in other
words, “But seck ye first the kingdom of God, and his righteousness;
and all these things shall be added unto you” (Matt. 6:33). Christ is
given all power (Matt. 28:18). He gives power to us.

Time and Development

“And as it is appointed unto men once to die, but after this the
Judgment: So Christ was once offered to bear the sins of many; and
unto them that look for him shall he appear the second time without
sin unto salvation” (Heb, 9:27-28). History has meaning; it deter-
mines the place of each man in eternity: “Every man’s work shall be
made manifest: for the day shall declare it, because it shall be re-
vealed by fire; and the fire shall try every man’s work of what sort it
is. If any man’s work abide which he hath built thercupon, he shall
receive a reward. If any man’s work shall be burned, he shall suffer
loss: but he himself shall be saved; yet so as by fire” (I Cor. 3:13-15),
History had a beginning (Gen. 1:1), and the fallen carth shall have
an end (I Cor, 15), Therefore, in absolute opposition to ancient
pagan philosophies, the Bible teaches that time is Hnear It is also
limited, Only after the final judgment shall the burden of time be
removed from this world (Rev. 10:6). God is the ruler of time.

Sanctification in a personal sense is a progressive process, after God
has imparted the perfect sanctification of Christ to us at the moment
of regeneration, Paul speaks of running the good race (I Cor. 9:24)
and fighting the good fight (II Tim. 4:7). As with the individual who
strives against sin in his own life (Eph. 6:10-18), so it is with Christian
institutions and nations. The earth is to be subdued to the glory of
God, not just in eternity, but in time—not just after the final judg-
ment, but before it, when sinners are still alive on earth (Isa. 65:20).
History has purpose, direction, and meaning, preciscly because God's
decree controls all events. Ours is a personal universc, not an imper-

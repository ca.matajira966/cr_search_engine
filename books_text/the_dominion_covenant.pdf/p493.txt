Baste Implications of the Six-Day Creation 449

travaileth in pain together until now, And not only they, but ourselves also,
which have the firstfruits of the Spirit, even we ourselves groan within our-
selves, waiting for the adoption, to wit, the redemption of our body (Rom.
8218-23).

Ethical response outwardly to the law of God brings God’s
covenantal blessings. The very blessings will tempt those who are
only outwardly obedient to forget God and violate His statutes. But
the regenerate community will use His blessings to further His glory
and expand His kingdom into ail areas of life. Thus, special grace is
necessary to maintain common grace’s blessings. (By common grace,
theologians mean— or should mean—the unearned gifts of God to all
men, including the unregenerate, All men deserve death as a result
af Adam’s sin [Rom. 5}; life itself is a sign of common grace, that is,
an unearned gift.) What we learn in Deuteronomy 8 and 28 is that
the external world of nature responds in terms of a community’s out-
ward conformity to or rejection of God’s law. Thus, as always,
ethical questions are primary, not metaphysical questions of being.
The creation itself is closely liked to man’s ethical response to God; it
was cursed when man sinned, and it shall be restored progressively
as men are conformed once again to God's legal requirements.

God makes it plain that His requirements are ethical rather than
metaphysical. Magic is therefore rejected as a means of pleasing
God. Men do not manipulate God by manipulating some aspect of
the creation. The magical formula, “as above, so below,” which un-
dergirds astrology, divination, and other forms of ritualistic manipu-
lation, is a false formula. Man is only analogical to God, not a par-
ticipant with God in some universal “being.” God requires ritual, but
not ritual devoid of spiritual content. “Will the Lorn be pleased with
thousands of rams, or with ten thousands of rivers of oil? shall I give
my firstborn for my transgression, the fruit of my body for the sin of
my soul? He hath showed thee, O man, what is good; and what doth
the Lorp require of thee, but to do justly, and to love mercy, and to
walk humbly with thy God?” (Mie, 6:7-8), This is why.God can
promise external restoration; it will have been preceded by personal
regeneration in the elect, and by outward conformity to the law of
God by both the regenerate and the unregenerate.

Fatherhood and Adoption
As far as man is concerned, no more crucial distinction in the
Bible exists: created sonship and adopted sonship. Men’s eternal

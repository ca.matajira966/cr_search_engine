458 THE DOMINION COVENANT: GENESIS

trial. Eve knew of God’s instructions, but only because Adam had told
her. She had not been present when God had spoken these words to
Adam. Her testimony would have been based on “hearsay evidence.”

Judicially speaking, God’s word could not be legally challenged
in this court, for there were not two witnesses, But Satan proceeded
as if he were in a position to bring charges. He appealed to Eve, who
then acted autonomously, and who subsequently brought her hus-
band into the court as an implicit witness against God, for she gave
him fruit to eat, and he ate. Adam never verbally confirmed Satan's
charge. He did not verbally lie. He simply acted out his rebellion.
But his act of rebellion constituted his testimony, for implicit in his eating of
the fruit was a denial of the binding authority of God’s word.

The Christian view of God is trinitarian. God is three Persons,
yet also one Person. Each Person always has the corroborating testi-
mony of the others. Therefore, God’s word cannot be successfully.
challenged in a court. Two Witnesses testify eternally to the validity
of what the other Person declares. Each has exhaustive knowledge of
the others; each has exhaustive knowledge of the creation. The truth
of God's word is established by Witnesses. As the supreme Witness,
God casts the first stone on the day of final judgment, and then His
people follow Him in executing judgment.

‘The doctrine of the two witnesses also throws light on the New
Testament doctrine of the rebellious third. In Revelation 8, we are told
that a third of the trees are burned up (v. 7), a third of the sea be-
comes blood (v. 8), and a third part of the creatures and ships in the
sea are destroyed (v. 9), A third part of the rivers are hit by the star
from heaven (v. 10), and a third part of the sun, moon, and stars are
smitten (v. 12). In Revelation 9, we read that angels in judgment
work for a time to slay a third part of rebellious mankind (v. 15), to
testify to the other two-thirds of the coming judgment, yet they do
not repent (y. 21), A third of the stars (angels) of heaven are pulled
down by Satan’s tail (Rev. 12:4).

Why these divisions into thirds? Because for every transgressor, there
are two righteous witnesses to condemn him. God's final judgment is
assured, for in God’s court, there will always be a sufficient number
of witnesses to condemn the ethical rebels.

Instant Judgment
What was the primary lure of this particular fruit? It would make
men wise. But what kind of wisdom was this? Tl was the wisdom
given to Solomon by God: the ability to make wise judgments. Satan's

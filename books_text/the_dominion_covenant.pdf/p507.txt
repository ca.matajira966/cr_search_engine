Witnesses and Judges 463

them, and set a flaming sword in front of it, specifically to keep them
from eating from the tree of life and gaining immortality (Gen.
3:22). The tree of life still retained its life-giving power. Why did
they refuse to eat during God’s physical absence?

We come to the heart of man’s sin when we answer this question,
Man would have had ta subordinate himself to God's word in arder to receive
eternal life. They saw that they were raked, Their eyes had been
opened. The serpent’s word was partially fulfilled, God had not told
them about this aspect of the trce of knowledge, and now the serpent
was apparently vindicated, However, the second half of the serpent’s
word had yet to be fulfilled, namely, that on the day that they ate,
they would not surely die. But God had said that they would die. So
the partial fulfilment of Satan’s word—having their eyes opened —
was insufficient to prove the case. This was merely Satan’s additional
information ys. the silence of God. The crucial test was still unde-
cided. What would be the outcome of the two antithetical words?
Would the rebels dic before the day was over?

Satan had said that they would not die. Why would they believe
such a thing? Because God is immortal. By implication, becoming as
God would mean that they, too, were immortal. Would they not par-
ticipate in the very being of God? Would not His attributes become
theirs, including immortality? This temptation, James Jordan says,
is the origin of the chain of being philosophy.

They were still clinging to their false witness. They would not
admit that Satan was lying, that God’s word was sure, They did not
go directly to the tree of life while there was still time remaining
prior to the judgment of God. They refused to admit ritually that the
day was not yet over, that God would surely come in judgment and
slay them as He had promised. Instead, they spent their time mak-
ing coverings for themselves. It was a question of saving their skins or
covering their hides. They chose to cover their hides. Their pride con-
demned them.

Jordan argues that they sensed their need for coverings because
they understood a judge’s need for a robe. The robe in the Bible is a
robe of judicial office. Joseph’s long, sleeved robe (sometimes translated
“coat of many colors”) from Jacob was just this kind of robe (Gen.
37:3), It signified his authority over his brothers. When he told them
of his dream that they would bow down to him (37:5-11), they
stripped him of his robe (37:23) and tore it up (37:32). They refused
to tolerate his authority over them. They cast him into a pit, and the

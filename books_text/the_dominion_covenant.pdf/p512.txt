468 THE DOMINION GOVENANT: GENESIS

So man’s dream has been turned against him: hoping to render
judgment instantaneously, he has had to render judgment progressively.
His dream of autonomy has also been thwarted: he can declare judg-
ment against God under Satan, or he can declare against Satan
under God. But he is a provisional judge, not a final judge. He is
always under the overall sovereignty of God, but cthically he places
himself under the judicial sovereignty of cither God or Satan.

Standards of Judgment

What God has declared definitively must serve as man’s standards
provisionally, for man will be judged in terms of these standards finally.
This points inescapably to the continuing validity of biblical law. Re-
bellious man will attempt to adhere to the dominion covenant by
rendering judgment, but as he grows more consistent with his condi-
tion as a covenant-breaker, he will seek to declare his own standards,
and to render final judgment,

There are two humanistic standards that covenant-breakers sub-
stitute in place of biblical law: natural law and positive law. Natural
law theorists declare that man, as judge, has access to universal
standards of righteousness that are binding on all men in all periods
of time. These standards are therefore available to all men through
the use of a universal faculty of judgment, either reason or intuition. In
fact, to declare judgment in terms of such a law-order, the judge
must exercise both reason and intuition, in order to “fit” the morally
binding universal standard to the particular circumstances of the
case. What is therefore logically binding becomes morally binding in
natural law theories. What is logical is therefore right.

Positive law docs not appeal to universal standards of logic in
order to discover righteousness. It appeals to the particular case.
Circumstances determine what is correct. The legislature declares
definitively what the law is, and this becomes the morally binding code
of justice. But the legislature has a rival: the judiciary. The judge in-
terprets the law, and this judgment finally becomes the true law, if
“the peuple” (or the executive) are willing and able to enforce what
the judge declares. In short, what the State can enforce is therefore right.

Neither system can escape the need to declare some sort of
coherent (logical) standard, and neither system can avoid the use of
some non-logical human facility (intuition) to apply the law. “Cir-
cumstances” do not speak with a universally clear voice, nor does
“reason.” In fact, each system relies on aspects of the other to impose

 

Index

harmony of interests, 97, 99, 242
logic of, 332
Social Security, 168f
Social utility, 43
Society, 312
Sodom, 157f, 178
Solomon, 126
Soul, 447
Sour grapes, 182
Sovereignty, 435f
authority, 10%
Bible’s, 229
chance, 87
family, 91
God's, 22, 27, 31
market, 25
process, 22
purpose and, 22f
State’s, 139f
subordinate, 28
time, 119
Space, 247f
Special creation, 384f
Specialization, 133, 153
Species, 405
Speculation, 226f (se entrepreneurship)
Speed of light, 254
Spencer, Herbert, 290ff
survival of fittest, 21, 250
Spock, 285
Spontaneity, 319
Spying, 184, 193, 194
Stagnation, 222
Stars, 12, 66n, 163
Star Trek, 284
State, 274, 303, 312
centralized, 97, 230
divine, 228
humanism and, 265
keepers, 141
monetary unit, 81f
time and, 130
(see also socialism)
Starism, 96f, 195
Statistics, 58, 59
Status quo, 426
Steady state, 380f, 418

503

Stephen, 452
Stew, 195, 203
Stewardship, 35, 69, 202, 214,
Qt, 444
Stewart, James, 204
Stoics, 361
Store of value, 233, 235
Strangers, 157, 163
Subjectivism, 57
dead-ends, 55
imputation and, 41
{see also values: subjective)
Subordination, 68, 73, 76, B4ff, 92f,
104, 145, 216, 337, 365
Successive creatures, 384f, 394, 424
Suicide, 29, 36
Sumner, William G., 290M
“forgotten man,” 313f
Sun god, 228
Supermen, 294
Supply and demand, 39f, 112
Survival, 26, 257f, 261, 289ff, 293,
337, 343, 346
Sutton, Ray, xxxvi-xxxvili
Suzuki, D. T., 3644
Switzerland, 341

‘Take-off, 131
‘Yalismans, 216, 388
Taxes, 141
Technology, 207, 210
Teleology
Darwin and, 408f
elites, 3044
ipersonalism and, 255f
individualistic, 319
Lyel’s, 383
man’s, 268f, 299
Marx and Darwin, 18
natural selection vs., 286
Paley’s, 390
science vs., 262, 268, 377, 391
19th-century theology, 286
Temple, 172f
Temptation, 8, 447
revelation, 106
Temptations, 449
Ten commandments, 452

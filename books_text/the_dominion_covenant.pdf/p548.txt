504

Testing, 86f, 92, 1008, 109, 439, 446
Testing man, 110
Testing (period of), 104f
Theater, 218
Thett, 45
“Then Came Bronson,” 135¢
Theocentric, 19
Theodoret, 154
‘Vheophany, 200f
Thrift, 170, 217
Throne, 172£
Time, 118ff
burden of, 440, 441
changeless, 129f
creation’s framework, 430
curse of, 130
cyclical (see cycles)
dominion and, 127
endless, 253, 370, 376, 409
escape from, 128, 130
Tear of, 441
God and, 380
Greek views, 3598
limited, 207, 440
linear, 119, 1638, 367, 440
means of production, 441
opportunity, 120, 127, 130
pagans’ god, 119, 196
processes of, 129f
Rebekah, 194
“safe? 392
scarce resource, 126
vast, 376
waste of, 74
Time-preference, 126f, 127, 182, 207
Tithe, 445
Totalitarianism, 97, 137, 151, 153
Toulman, Stephen, 14f, 419
Tower of Babel, 150 (see Babel)
Townsend, William, 389
Toynbee, Arnold, 34
Trade, 1538
Tradition, 334, 335, 339
Transcendence, 34f, 414, 432f
Treason, 184, 185
Treaty, 185
Tree of knowledge, 69, 86, 87,

‘THE DOMINION COVENANT: GENESIS

1008, 102
Tree of life, 102ff, 105
Turgot, 204, 208
Two tables, 452

Uncertainty, 79n, 215, 220f, 221f,
235, 345, 346, 348
Uniformitarianigm
Buffon, 376
Christianity vs., 422f
Darwin, 407
defined, 253f
Hutton, 379f
man overcomes, 272
removing God, 393
social change, 129
Unity, 150, 365f
ethical, 151
man’s, 150f
political, 151
Universe
autonomous, 249
depersonalized, 265, 279
size, 247
Universities, 174f
University of Oregon, 240
Ur of the Chaldees, 156
U.S. Labor Party, 161n
USSR, 116n
Usury laws, 128ff
Utilitarianism, 44ff, 49
Utility
aggregation, 48, 548
comparisons, 331, 332, 336
diminishing, 44

Values
aggregation, 43ff, 608
evolutionary, 333f
hicrarchy, 101
historic, 79, 80, 236
imputed, 37M, 42¢f, 59f, 100f, 107,
233
intrinsic, 236£
labor theory, 38f, 58
measure of, 235
objective, 37, 424, 59f, 100f, 107

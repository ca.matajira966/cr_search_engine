4
ECONOMIC VALUE: OBJECTIVE AND SUBJECTIVE

And God saw everything that he had made, and behold, it was very good.
And the evening and the morning were the sixth day (Gen. 1:31).

The first chapier of Genesis repeats this phrase, “and God saw
that it was good,” five times (vv. 10, 12, 18, 21, 25), in addition to the
final summation in verse 31. God’s creative acts were evaluated by
God and found to be good. They reflected His own goodness and the
absolute correspondence between His plan, His standards of judg-
ment, His fiat word, and the results of His word, the creation. The
creation was good precisely because it was solely the product of
God's sovereign word. God therefore imputed positive valuc to His
creation, for He created it perfect. It was completely in conformity to
His decree. The doctrine of imputation lies at the heart of the doctrine
of creation, The creation was good because God created it good and
because God said it was good. It was good objectively because of its in-
herent conformity to God’s decree. It was good subjectively because
God announced ils perfection, indicating ils conformity to His
standards. The Creator is also the Imputer. God’s original subjective
—meaning personal, not rclative—standards served as the sole stan-
dard of the creation itself; once created, the creation then was
evaluated in terms of the original standards, and God the infallible,
subjective evaluator announced that in no way did the creation
deviate from His standards.

Prior to his ethical rebellion, man was consistently able to think
God's thoughts after Him in a creaturely, human fashion. Man had
language from the beginning; he had the power to rclate mental con-
structs to the external realm of creation, He was assigned the task of
naming (classifying) the animals (Gen, 2:19) and dressing the gar-
den (Gen. 2:15), indicating his ability to fulfill God’s requirement
that he establish dominion over the creation. In both tasks, human

37

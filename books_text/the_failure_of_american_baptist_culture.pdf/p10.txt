x CHRISTIANITY AND CIVILIZATION

kind of physical sacramental worship. Of course, Mr. Chantry
would not want to push the matter so far, but his disciples doubtless
will!

Since the Baptistic individualist sees salvation only in terms of
the individual’s journey to sorne other world, some evangelical Bap-
tists have begun to teach that the Bible is inerrant only when it deals
with matters of individual salvation. Broader cultural concerns,
since they are not within the purview of Baptistic salvation, are not
dealt with by Scripture in an inerrant fashion, they say. Clearly,
many Baptists reject such reasoning, but the position is consistent
with the individualism of Baptist theology. Since the catholic and
Reformed faith sees salvation as the restoration of the whole fabric
of life, we must see inerrancy as pertaining to every detail of science
and history in Scripture, for these are given us as part of our salva-
tion.

Ideas have consequences.

You see, if we really push the matter, then Baptists disagree with
catholics over such fundamental issues as the Trinity, the nature of
salvation, and even the nature of Biblical authority! But we don’t
want to push the matter; we simply want to expose éendencies of
thought, to provoke you, courteous reader, to rethink these matters.

More things could be said. For instance, there is a difference be-
tween the catholic Reformed and the Baptists over the nature of the
sacraments and the Church. Paul K. Jewett has written: “Baptism,
in an evangelical theology, is an act of confession on man’s part in
response to an act of rencwing grace on God’s part” (Infant Baptism
and the Covenant of Grace [Grand Rapids: Eerdmans, 1978], p. 162).
If Jewett is correct, then the Reformed faith is not evangelical.
Neither Augustine nor Calvin was an evangelical, for they did not
sec the sacrament first and foremost as man’s response, but as visible
words from God. The sacraments are not man’s work of response, in
catholic and Reformed thought, but they are God’s gracious call,
they are physical signs of God’s Word. They are God’s claim and
promise, to which man is to respond in abedience and faith. Jewett’s
statement presupposes what he needs to prove: that baptism is man’s
profession, not God’s claim. This foundational flaw renders Jewett’s
book of only marginal value for the réal discussion at hand. (Paren-
thetically, the third issue of Christiantty and Civilization is scheduled to
be a symposium on the reconstruction of the Ghristian Church, and
this issue will be more fully dealt with in that volume.)

In Baptistic thinking (and many catholics and Calvinists are
guilty of this as well), faith and the sacraments are not presupposi-

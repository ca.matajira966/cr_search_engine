MEDIA THEO-POP
A Review by Michael] R. Gilstrap

By What Authority: The Rise of Personality Culis in American Christi-
anity, Richard Quebedeaux. Harper & Row, 1982. 204 pp.,
indexed, cloth-bound. $11.95.

HIS particular book was sent to me by accident. I did not order
T it, nor did I particularly want to read it. It was placed on my
shelf for several weeks and forgotten. Then one day, for some rea-
_son, I picked it up and casually began to scan through it. The sub-
title interested me, and the table of contents looked promising, but I
knew that I did not have time to waste in finding the needle that is
always promised in the haystack of the typical, modern, evangelical
book. Evangelicalism is always long on promises, but short on
delivery, Not this time. Richard Quebedeaux has written a very im-
portant, first-rate and penetrating analysis of the roots and fruits of
the religion of mass culture. Many of the leaders of popular religion
in America have become “stars” with celebrity status by virtue of
their visibility in the mass media (TV, radio, films, newspapers,
magazines, etc.). Pat Robertson, Jerry Falwell, Oral Roberts,
Robert H. Schuller, and their kin in the leadership of the “electronic
church” are part of an industry that brings in over $1 billion
annually in donations alone, and that doesn’t include their huge
advertising and sponsorship. revenues. Each of these men has
become the center of a “personality cult,” and has become just as
popular as “secular” T'V personalities. Because of the position that
they hold in American Christianity, they exercise an influence and
wield an authority that must be reckoned with. Quebedeaux’s By
What Authority examines both the nature and the impact of popular
religion, and the influence of its celebrity leaders.

The book is divided into three sections. The first discusses
popular religion in general with a particular emphasis on the social
impact of the mass media and technological advance on modern
American religion. The rise of the religious personality cult occupies
the second major section. The history of celebrity leaders from 1865
to the present establishes a tradition of personality-centered religion
which began during the nineteenth century with the rise of the

99

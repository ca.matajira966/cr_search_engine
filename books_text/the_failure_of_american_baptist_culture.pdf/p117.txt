MEDIA THEO-POP i101

As Quebedeaux remarks, popular culture developed somewhat
differently from other cultures in the past. “Tf the rise of traditional
culture — of civilization itself—was a gradual, progressive, orderly
process, then popular culture is its opposite” (p. 4). Effortless and
immediate results are promised by mass culture, and that is pre-
cisely what makes it so tantalizing to the modern American. In fact,
writes Quebedeaux, “Success, the highest god of the American pan-
theon, can —just like the rest of the ‘good things in life’ be achieved
merely by passive absorption” (p. 4).

Popular culture is supplied by newspapers, magazines, records
and tapes, radio, and primarily by television. Its prominent themes
range widely over love and crime, the activities of cowboys, detec-
tives, oil barons, housewives, science, and religion. It must be
distinguished from the “high culture” of special groups with a
heritage of taste and Jearning, and from the “folk culture” that has
emerged at various times during our nation’s history more or less
spontaneously. It is primarily because of the absence of strong,
native-grown high and folk traditions in preindustrial America,
along with the mass influx and absorption of immigrants with
heterogeneous traditions into American life, that popular culture

- has become so strong in America. Quebedeaux’s comments are par-
ticularly telling at this point,

If high culture elites— with more than average prestige, power,
and income — once dominated the pre-industrial world im politics,
religion, and society in general, and determined what was to be
produced, culturally and otherwise, they do so no longer. With
the development of industry, it is the great mass of consumers
who now determine what is to be produced. Elite status, leader-
ship in any form, is achieved and maintained today by catering to
the masses, by giving them what they want. Thus industrialists
become multimillionaires by selling to farmers, for instance, and
their business is helped by giving their customers, via television,
the entertainment they desire. As society becomes fully indus-
trialized, popular culture becomes the nerm and colors almost all
aspects of private and social life. (p. 5)

Quebedeaux goes on to say that because the media that carries
popular religion to the consumers are a mere part of the vast flow of
mass culture, then popular religion itself must be located within that
flow. Religion produced by the mass media for consumption is
“popular” because “it is fashioned for everyday people with the aim
of helping them meet everyday problems. It uses plain language that
is understandable and meaningful to the masses” (p. 5). Popular

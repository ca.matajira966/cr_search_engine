MEDIA THEOQ-POP 107

organizations use to communicate the message. The method musz
be both personal and visible. For example, Jerry Falwell comes
right into the living room of millions of homes each week on his
“Old Time Gospel Hour.” He speaks directly to his audience in a
visible way. In addition, the evangelist sends out millions of “per-
sonal” letters each year. On the other hand, the head of the mainline
Pratestant ecumenical agency, the National Council of Churches,
communicates primarily through the very formal and bureaucra-
tized structure with memos and dictums. In this case, authority is
not only invisible and anonymous, but it is also very impersonal.

Thirdly, “the influence of religious leaders is a function of their
particular constituency or audience, the people they communicate
with” (p. 111). To use our previous example, the general secretary
of the National Council of Churches influences her staff, who in
turn influence their staff, and so on. The further the influence goes
down the line, the more it wanes, until at the lowest level, the local
church member, the influence is nil. On the other hand, Jerry
Falwell communicates directly with the masses. As a result, his
influence is very powerful on the popular level.

Fourthly, the modern religious leader is influential by virtue of his
status in the media. The more visible a leader is, the more influence
he has. In other words, it is not the message that is important with
the public, but the “star status” of the average “TV preacher.”

Criticisms

Now that we have looked at Quebedeaux’s excellent analysis of
the religion of mass culture, let us turn to the last section of the book
where he criticizes popular religion. Quebedeaux’s criticism
revolves around two main lines of thought: the superficiality of
popular religion, and the inadequate view of the authority of the
Bible that characterizes much of evangelicalism. In Quebedeaux’s
mind, superficiality is the root problem (the cause), with the view of
Scripture being a distant second (the effect).

Quebedeaux rightly sees that the present formulations of the
various aspects of the doctrine of Scripture (inerrancy, infallibility,
inspiration, etc.) by the evangelicals are not that much different
from the naturalistic and philosophical systerns that they are oppos-
ing. Because the doctrine of biblical inerrancy, as we have it
presented today in popular religion, was formulated and systemna-
tized by late 19th and early 20th century Presbyterian and Baptist
theologians, Quebedeaux accurately assesses the fact that those

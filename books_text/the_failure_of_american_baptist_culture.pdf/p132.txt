116 CHRISTIANITY AND CIVILIZATION

splendor of his light—-especially if he has not removed their
ignorance before taking them from the prison of the flesh?!

The Reformed Baptist theologians have generally been aware of
this ambivalence and have chided the paedobaptists accordingly.
They have argued that the traditional paedobaptist position is
neither fish nor fowl, Both parties agree that the ‘sacrament pri-
marily signifies regeneration evidenced by faith. Both agree that the
elernents stir up and strengthen faith. Then when the paedobaptist
wants to baptize children of believers, the baptist charges him with
inconsistency and special pleading and of hanging on to a vestige of
sacramentalism. And the Reformed, for their part, have often been
somewhat confused in trying to explain the power and efficacy of
baptism and the precise meaning of the sacrarnent when adminis-
tered to infants, since they have already conceded it to be efficacious
only through faith.)

In summary, we suggest that classical Reformed theology, by
emphasizing the soteric and forensic aspects, overlooked others, In
particular the redemptive-historical-eschatological aspects were
underdeveloped. !? This in turn resulted in the meaning of baptism
being seen exclusively in soteric categories, and then in the category
of the sotericlogy of the individual to whom it was applied. It in-
dicated forgiveness and ablution. It.indicated the regeneration of an
individual soul. Yet the forgiveness of the sinner was effected only
through faith; there was no instrumental power in the sacrament,
inherent in either the elements or the institution. There was no odjec-
dive significance of efficacy in the sacrament itself. The significance
and power of the sacrament was effected only through faith.

In the case of infants this has seemed to eviscerate the sacrament
of objective significance and power.!3 The Reformed have always
denied this of course, arguing correctly that the ground of the sacra-
ment lies not it its perceived power but in the God who commands it

 

10. John Calvin, Institutes of the Christian Religion, 4:16:19.

11. Consider, for example, the various options proposed by Reformed
theologians to describe the signilicance of the faith signified in the sacrament:
vicarious faith, infunl faith, proleptic faith, objective faith, etc.

12, Note we say “underdeveloped.” Galvin appears to have come closest to the
redemptive-historical perspective by insisting that baptism indicated ingrafting into
Christ Unstitues 4:15:1-6). But even here the more “objective” eschatological
elements of the dawning of the new age, the Messianic reign of the Lord, and judg-
ment upon wickedness are not developed.

13. Over and over one finds that the weight of the Baptist case is found in the
assertion that the Baptist practice seems to protect the meuning, significance, and power
of the sacrament.

 

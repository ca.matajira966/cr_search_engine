124 CHRISTIANITY AND CIVILIZATION

covenant, Murray again interprets this through the paradigm of
sovereign grace. The greater the grace bestowed, the higher the
response demanded. So it was with Abraham. He writes, “the
necessity of keeping (the covenant) is but the expression of the
magnitude of the grace bestowed and the spirituality of the relation
constituted.”33

What, then does Murray make of these “conditional” elements
in the Abrahamic covenant? He argues that, “the continued
enjoyment of this grace and of the relationship established is con-
tingent upon the fulfillment of certain conditions. For apart from the
fulfillment of these conditions the grace bestowed and the relation
established are meaningless.’** Thus, Murray wishes to defend the
priority of grace sovereignly bestowed. Obedience to the conditions
of the covenant is not the ground on which the covenant is estab-
lished. Grace alone establishes the covenant relationship; obedience is
the ground of continued enjoyment of the covenant. He concludes,
therefore, that “by breaking the covenant what is broken is not the
condition of bestowal, but the condition of consummated
fruition.”35

Tt follows from the above considerations that circumcision had as
its “primary and essential significance” that which was the sign and
seal of the “highest and richest blessing which God bestows upon
men”—namely, union and communion with himself.3¢ It signifies
and seals the gracious relationship sovereignly esiablished between
God and his people.

Before moving on to consider Kline’s position we must make a
few observations about Murray’s treatment. Murray correctly
argues that the primary and essential significance of circumcision
was religious and spiritual and it pointed to union and communion
with God—the “highest level” of religious relationship.3? But the
intended significance of the formal seal does not necessarily corre-
spond with the actual outworking or response. Murray of course is
aware of this distinction, He acknowledges that there are both exter-
nal and internal blessings in the covenant. “The covenant embraces
external blessings, but it does so only insofar as the internal blessing

33. fbid., p. 18.

34, Ibid., p. 19.

35, Idan,

36. J. Murray, Christian Baptism (Nutley, N. J.: Presbyterian and Reformed,
1977), p. 49.

37. Covenant, p. 17.

BAPTISM, REDEMPTIVE HISTORY, AND ESCHATOGLOGY 141

We believe that baptist theologians, in their conception of the
Old Covenant religion, have read into the Scriptures precisely what
they must prove. They have claimed that physical generation apart
from spiritual birth constituted membership in the Old Covenant,
so religion had to be grounded in externals.

 

But let us examine this briefly. David Kingdon puts the question
this way: “The crux of the matter is whether or not participation in
the temporal earthly blessings of the covenant was sufficient in the
Old Testament period to give a right to circumcision.”* He points
out that one could apparently enjoy life under the godly, equitable
judicial system of the theocracy, one could enjoy the privileges of
gleaning and other social welfare provisions and so on, without ever
being regenerate. Participation in these blessings was enough to
grant the right to circumcision.

 

But we must counter by observing that Kingdon’s rhetorical
question by its very form prejudices the case. It begs the question of
whether one has to participate in blessing before having the
sacrament administered. Yet this is the very point at issue. We may
reply with another question: Which came first, the promise unto
Abraham and his children followed by the command to cireumcise
the children, or did the children enjoy the temporal blessings first?
Obviously the former. So the Reformed have always argued that the
right to baptism/circumsision comes from the promise and com-
mand of Gad, not from the priar possession of certain blessings.’°
To be sure, the circumcised enjoyed blessings, but these were
enjoyed decause he was a covenant member; he was not admitted to
the covenant because he enjoyed them. Thus the argument of Kingdon
and others fails because it does not prove enough. It does not prove
that one has to participate in blessings before one is admitted to the
covenant,

On the other hand, Kingdon's argument proves too much. Many
uncircumcised participated in the temporal earthly blessings without
being granted circumcision. This being so, we may conclude that
participation in temporal blessings had nothing to do with whether
one was circumcised. Consider, for proof, the case of the resident
alien. The resident alien (ger) was “essentially a foreigner who lives
more or less permanently in the midst of another community.””!

 

69. Kingdon, Children, p. 42; ef. Jewert, p. 97.

70. Thus Berkouwer, p. 184, The achninistration of baptism, he asserts, rests
upon God,

71. Roland de Vaux, Ancient Israel, 2 vols. (New York: MeGraw Hill, 1965), 1:74.

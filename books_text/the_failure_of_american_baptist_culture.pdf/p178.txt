166 CHRISTIANITY AND CIVILIZATION

in deep sin. Putting it another way, break the mirror and the shame
disappears. The knowledge of sin comes via the law. All of the
perfectionistic movements of history have rejected both a knowledge
of the law and the knowledge of sin.4! Interestingly. these groups
have produced attendent political theories which are usually com-
munistic. An example is Oberlin College, which was unques-
tionably perfectionistic from its inception, The Oberlin Covenant
reads, “we will hold and manage our estates personally, but pledge
as perfect a community of interest as though we held a community of
property.” Thus perfectionism and communism are inseparably
linked by Pelagius’s faulty interpretation of original sin.33 Only an
objective standard found in God's special revelation, the Bible,
exposes it.

ILL. Theotogp of Anabaptism and Ethics

Because Anabaptisis have not looked to an external standard,
their ethics has been subjectively based. At times they have even
depended on natural law more than the Scripture. Some historians
try to preserve some objective standard by pointing out that the
Anabaptist tradition has lived by the Sermon on the Mount and the
New Testament. Even so, they have fallen into subjective ethics.
First, once the Revelation of God is divided and it becomes a matter
ot pick and choose, man becomes the determinor of Scripture.
Modern evangelicalism illustrates this: They struggle over
inerrancy while pitting Old Testament ethics against new. Is it any
wonder that the liberals da not take them seriously? Second, their
view of the Sermon on the Mount is subjective. The Scrmon on the
Mount says that it is net intended to do away with the Old Testa-
ment Law (Matthew 5:17ff.). Baptists, however, have historically

 

persisted in their emphasis on a “New Testament” hermeneutic.3¢
For these reasons hermeneutical extremes have not been avoided.

The Dutch Humanist and politician Goornheert exemplifies
these tendencies. “At the time when he lived and worked, ecclesias-
tical conclitions in the Netherlands were still unsettled. This was the
period when the Baptist communities, which have been mentioned,
were being reorganized. Coornheert urged on his contemporaries

$1. Benjamin B. Warfickt, Perféctioninm (Phillipsburg, NJ: Presbyterian and
Reformed, 1958), pp. 400-454.

2. Ibid, p.

33. Ibid, p. 63.

34. Davis, p. 24

 

 

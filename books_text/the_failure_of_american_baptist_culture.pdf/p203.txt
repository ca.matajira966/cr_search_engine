CALVIN’S COVENANTAL RESPONSE 189

one can see why Calvin did not view paedobaptism in a narrow class
by itself, but instead as an important safeguard of Scripture and
doctrine. To affirm infant baptism meant that one saw the unity of
the Bible and consequently its constant theme of redemptive history.
In other words, infant baptism was covenantal for Calvin, and since
so many other doctrines of Scripture were related to the covenant, to
deny the sacrament meant that other central truths were in jeopardy
as well. It is clear, then, that Calvin would not agree with those who
claim that they do little harm to his system by simply excising
paedobaptism. ‘To deny infant baptism is to deny the covenant, and
so to put the other doctrines of Scripture in danger. As one explores
Calvin’s thought with respect to the covenant, he is immediately
struck with the numerous points of doctrine that he intimately
couples with it. In this way, Calvin demonstrates the danger to all
doctrine by the Anabaptist approach.

 

Calvin’s point in this extended discussion is to show that chis same destructive view
weighs on all Anabaptists as a logical result of their insistence on distinguishing so
absolutely the Old and New Testaments. Even if it is denied, it must be done by an
inherent logical inconsistency.

8. It is often thought that Calvin paid litle attention to the covenant idea. and
that covenant theology developed after the Reformation. Gf. Perry Miller, 7he New
England Mind (New York: The MacMillan Co., 1939), pp. 381, 389. Fred Lincoln,
“The Development of the Covenant Theory” Biblioeheca Sacra 100 (1943); 136 states,
“It was [covenant theology], of course, unknown to the apostolic and early church
fathers, never taught by the church leaders of the middle ages, and nat mentioned even
by any of the great teachers of the reformacion period isclf” (italics mine), Similarly,
Charles C. Ryrie, Dispensationalism Today (Chicago: Moody Press, 1965), p. 180
writes, “Covenant theology does not appear in the writings of Luther, Zwingli,
Calvin or Melanchthon, . . . They had every opportunity to incorporate the covenant
idea, but they did not. It is true that Calvin, for instance, spoke of the continuity of
redemptive revelation and of the idea of a covenant between God and His people,
but this was not covenant theology,” While Ryrie indicates an advance on Lincaln’s
“not mentioned," he still insists that there is no covenant theology in Calvin.

Perhaps the best response to this assertion is to point out the frequency of the cove-
nant idea in Calvin’s writings and the general contexts where Calvin makes impor-
tant use of the idea. Calvin uses in the 1559 edition of the /nstitutes the Latin terms
Pactum 35x, Foedas 154x, and Testamentum 94x, for a toval of 273x. As we shall see,
Calvin makes great use of the idea of the covenant in the context of the relationship
of the Old and New Govenants and sacraments (53x in IV. 16 in his discussion of in-
fant baptism alone!). In addition, he uses the idea in several texts: on the law—II. 8
(7x), on fsith—ILL.2 (2x), on prayer—TIL. 20 (11x), on repentance—once each in
TIL, 4. 32, 1. 1. 27, [V. 15. 17. His discussion of justification uses the concept 13x in
TIL. 1?. His analysis of election uses the idea 12x in IIL, 21, This is remarkable since
many see the covenant as an idea that is antithetical ta the doctrine of election! He
also uses the idea of the covenant as a justification of the Protestant Reformation
against Rome in IV. 2. i1, IV. 7. 30, IV. & 2, 1V. 9. 2, IV. 18. 15. I personally

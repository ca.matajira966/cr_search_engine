190 CHRISTIANITY AND CIVILIZATION

I. Calvin's Argument for the Continuity of Doctrine
in the Old and New Covenants

Calvin’s fundamental proposition in his argument for the con-
tinuity of the covenants is that God always covenanted His people to
Himself by the same law and doctrine. Thus he writes,

. . all men adopted by God into the company of his people since
the beginning of the world were covenanted to him by the same

law and by the bond of the same doctrine as obtains among us.
(HL, 10. 1)

Similarly he states, “The covenant made with all the patriarchs is so
much like ours in substance and reality that the two are actually one
and the same. Yet they differ in the mode of dispensation” (II. 10.
2). Not even the Mosaic legal system can be scen to be without its
necessary conjunction with the one divine covenant,

I understand by the word “law” not only the Ten Gommand-
ments, which set forth a godly and righteous rule of living, but the
form of religion handed down by God through Moses. And Moses
was not made a lawgiver to wipe out the blessing promised to the
race of Abraham. Rather, we see him repeatedly reminding the
Jews of that freely given covenant made with their fathers of -
which they were the heirs. It was as if he were sent to renew it.
This fact was very clearly revealed in the ceremonies. (IL. 7. 1)

Calvin beautifully portrays his understanding of the single covenant
of God in its different administrations in terms of progressive
redemptive history,

 

believe that Calvin taught an inchoative doctrine of works as well. Gf the Spring
1981 edition of the Westminster Theological journal where I seek to defend this in an
article entitled, “Ursinus' Development of the Covenant of Creation: A Debt to
Melanchthon or Calvin?” Considering the fact that the Bible only uses the covenant
idea 314x, and even Dispensationalisis give a great deal of attention to this idea, it
does not seem quite accurate to say that Calvin's teaching at this point “was not cove-
nant theology!” He did in fact teach a covenant of grace and a covenant of works, as
well as make extensive use of idea in a wide range of contexts

9. That Calvin understands this “law” and “covenant” that have existed from the
“beginning of the world” to include Adam in his unfallen state is seen in the fact that
the moral law is equivalent to man’s conscience and natural law: “. . . the very
things contained in the two tablets are in a way dictated to us by that internal
law . , . written and stamped on every heart (11. 8. 1)," and“. . . the law of God,
which we call moral, is nothing other than the testimony of the natural law and of
that conscience which Ged has engraved on the minds of men. . . (TV, 20. 6).” This
is important evidence for the question of whether Calvin taught a pre-fall covenant
with Adam. Il appears from this that he did.

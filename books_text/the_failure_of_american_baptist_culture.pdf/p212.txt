198 GHRISTIANITY AND CIVILIZATION

tual observance of ceremonies and delivered to the Jews; it was
temporary because it remained, as it were, in suspense until it
might rest upon a firm and substantial confirmation. It became
new and eternal only after it was consecrated and established by
the blood of Christ. Hence Christ in the supper calls the cup that
he gives to his disciples, “the cup of the New Testament in my
blood.” By this he means that the Testament of God attained its
trath when sealed by his blood, and thereby becomes new and
eternal. (II. ii. 4)

The third difference between the Old and New Covenants, and
the second between the law and gospel, is the letter-spirit distinc-
tion. This idea is in many respects an extention of the point Calvin
has just explained—that the Old Govenant became the New Cove-
nant. In the prior point, the change from the Old to the New was by
the coming of Christ. In this difference, the basis for the variation is
due to the special work of the Holy Spirit in the New Covenant.
Here Calvin explains Jeremiah 31-31-34 and II Corinthians 3:6-11.
‘The passages arc undeniably critical for Calvin’s perspective since
they clearly contrast the Old and New Covenants. The “Old” is
termed that which was broken by Israel or a covenant only of the let-
ter, while the “New” is called a covenant that is written by God upon
the heart and hence a spiritual covenant. These passages seem to
argue that there is not one divine covenant throughout Scripture,
but rather that there are two of quite a different character. Should
thal interpretation be correct, then Calvin would be forced to con-
cede the argument to the Anabaptists after all. How can he explain
this difference and still maintain the continuity of the Covenants? !é

 

16, This point is argued by Lincoln, of, cit., p. 135, “Therefore, in spite of the
multitude of texts which place the ‘uld covenant’ of the law of Moses in direct con-
trast with the ‘new covenant’ of grace in Christ, showing that the one was a failure
and the other superseded it (comp. Jer. 31:31-34; Heb. 8:7-12, ete.), in order to
maintain the unbroken continuity of the Covenant of Grace, they are forced to the
unscriptural and untenable position of saying that the law of Moses was a part of the
grace covenant.”

An interesting departure from histuric Reformed covenant theology is that view
point articulated by Meredith G. Kline, By Oath Consigned (Grand Rapids: W. B.
Eerdmans, 1968), pp. 16-25. Kine in cepts the viewpoint of dispensa-
tiunalism in asserting that there is a fundamental opposition of the covenant made al
Sinai with that made with Abraham and renewed by Christ. Hence, Kline
asticulates the law-gospel distinction as portrayed by Lincoln as the difference
hetween the “law-covenant” and the “promise-covenant.” In the “law-covenane,”
God is not the one who swears to the stipulations of the covenant, but only man. On
the other hand, the “promise-covenant” occurs without human stipulation but only
divine stipulation. or promise. While it is not our purpose to critique Kline's perspec-
ive here, it is important to evaluate bis use of Calvin to justify his viewpoint, Kline

  

ence

 

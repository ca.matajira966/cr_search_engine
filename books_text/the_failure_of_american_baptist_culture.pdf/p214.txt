200 CHRISTIANITY AND CIVILIZATION

not counted part of the law, when only the nature of the law is
under discussion. They ascribe to it only this function: to enjoin
what is right, to forbid what is wicked; to promise a reward to the
keepers of rightcousness, and threaten transgressors with punish-
ment; but at the same time not to change or correct the depravity
of heart that by nature inheres in all men. (II. 11. 7)

In other words, the law can only be letter because in itself it can only
tell sinful men what to do and hence point out their sin, but never
enable them to overcome their evil. The gospel, on the other hand,
has the Holy Spirit that enables men actually to begin to be holy and
do what the law dernands, since all of their sin is forgiven by Christ's
redemptive work.

This letter-spirit distinction is very carefully addressed in
Calvin's commentaries on the passages under discussion. Thus
Calvin explains how one ought to compare law and gospel in his
comments on Jeremiah 31:32ff. First, Calyin notes, one must
recognize what the law is in itself—a rule of righteousness that only
speaks to the ear as letter since it does not have the Spirit. But
secondly, Calvin adds, this contrast ceases once the Spirit is joined
with the Jaw. It is then no longer letter, but actually spirit or the
gospel itself. In fact, Calvin insists that it is not a new law that the
Spirit writes on the heart, but the very same law that was once only
letter.!7 Therefore Calvin insists that the benefits of the New Cove-
nant were even present in the law of the Old Covenant. To illustrate
this, Calvin mentions John 1:17, If grace and truth have come
through Christ and the law was of Moses, does this mean that these
benefits were absent from the law? His answer is that even though
grace and truth are only found in Christ, and the law does not have
them as benefits it can actually bestow, they were nonetheless pres-
ent adventitiously. Simply, there were borrowed from the gospel. In
light of this, Moses can be considered in two different senses. If he is

17. The Lutheran conception is quite clistinct from Calvin's view of the jaw at this
point, Commencing on Psalm 19, Luther writes, “This psalm teaches similarly thata
new Word will be preached, namely, one (hat will go through the whole world and
save those who believe in it; for the Law of Moses was given only to the Jews”
(Luther’s Works, 19:139). Again, he says, “For the Law reveals Gad’s wrath and not
God's grace 1 us, Therefore now God is rightly known” (LW 12:140). Here one can
see that Luther interprets Psalm 19 as a prophecy of the Gospel to the utter rejection
of the law of Moses. Not only does this overlook entirely the historical context of the
Psalm, but it also shows the great difference between Calvin and Luther at this
point, For Calvin, the law is unquestionably the law of Moses in terms of the moral
law.

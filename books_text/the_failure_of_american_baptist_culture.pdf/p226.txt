212 CHRISTIANITY AND CIVILIZATION

elect of Gad can never fail to persevere, they must ever be on guard
against the flesh and are in fact aided in their struggle by the warn-
ings.

Here, then, one sees that Calvin’s understanding of the leiter-
spirit distinction has a bearing of his view of the church. The church
is not composed entirely of those who have the Spirit-written law
upon their hearts, but also of those who have the promise that such
will be done (baptized children) and those who claim that it has been
done, but in reality are hypocrites. The Anabaptist view of the
gathered church grows out of an absolute view of the differences be-
tween the Old and New Covenants. Calvin’s perspective on the
church recognizes that there is a broader sphere of election than
those who are the true recipients of the Spirit. This is in keeping
with the church of Israel where there was a mixed multitude. The
difference for Calvin, then, is found in the fact that there are many
more truly elect in the New Covenant church than in the Old Cove-
nant church, but since the covenant is broader than its actual ap-
plication, there can be still covenant-breaking in the New Cove-
nant. In this way Calvin is able to explain how the reality of
covenant-breaking relates to the infallibly applied New Covenant.

It is of interest to observe how Calvin applies this approach to
the covenant to his own experience. Calvin had been baptized into
the covenant by a Roman priest. Yet, he failed to keep the promise
of baptism and become a covenant-breaker. Nevertheless, God in
His mercy restored him back into the covenant relationship. Each of
these points may be observed in his comments on Hosea 2:19, 20,

What fellowship have we with God, when we are born and come
out of the womb, except he graciously adopts us? for we bring
nothing, we know, with us but a curse: this is the heritage of all
mankind. Since it is so, all our salvation must necessarily have its
foundation in the goodness and mercies of God. But there is also
another reason in our case, when God receives us into favour; for
we were covenant-breakers under the Papacy; there was not one
of us who had not departed from the pledge of his baptism; and so
we could not have returned into favour with God, except he had
freely united us to himself: and God not only forgave us, but con-
tracted also a new marriage with us, so that we can now, as on the
day of our youth, as it has been previously said, openly give
thanks to him.

Did Calvin take the warning of falling away from the covenant seri-
ously? Did he believe that he might stumble away from the covenant
even though he was one of God’s elect? Calvin answers this question

AS TO ROGER WILLIAMS AND HIS “BANISHMENT” = 235

to the rhetoric of superficial biographers, or prejudiced historians or
the misapprehensions of a later public sentiment by them misled.
This it is proposed now to attempt.

AR RR ee

Studying carefully now all this evidence, I find it conducting the
mind with irresistible force straight toward one conclusion. It is true
that Mr. Williams did hold, in an inchoate form, and had already to
some extent advocated, that doctrine of liberty of conscience, with
which his name afterward became prominently identified. It is true
that the language of the official sentence is susceptible of a constrac-
tion which might include this among his “newe and dangerous opi-
nions.” It is true that Mr. Williams did himself claim that it was so
included. But it appears to be also true that he himself never claimed
more than this; and that, in his own view, his banishment was only
incidentally—in. no sense especially—for that cause. While the
careful and repeated statements of Mr. Cotton, with their reiterated
endorsement by Gov. Winthrop, go to show that Mr. Williams was
mistaken in supposing that the subject of the rights of conscience
had anything whatever to do with the action of the Court upon his
case; action, in reality, solely taken in view of his seditious, defiant,
and pernicious posture toward the State. This, it appears from the
testimony of Mr. Gorton, and of Gov. Winslow, supported by:that
of Secretary Morton, of Mr. Hubbard, of Judge Scottow, of Cotton
Mather and of Gov. Hutchinson, was the general understanding
had of the matter by the New England public of that day; while
Edwards and Baillie speak to the same point from over sea. And, as
I am aware of nothing purposting to be proof to the contrary, other
than the (necessarily biased, and presumable ill-informed and par-
tial) opinion of Mr. Williams himself, before cited; 1 cannot help
thinking that the weight of evidence is conclusive to the point that
this exclusion from the Colony took place for reasons purely
political, and having no relation to his notions upon toleration, or
upon any subject other than those, which, in their bearing upon the
common rights of property, upon the sanctions of the Oath, and
upon due subordination to the powers that be in the State, made
him a subverter of the very foundations of their government,
and —with all his worthiness of character, and general soundness of
doctrine—a nuisance which it seemed to them they had no alter-
native but to abate, in some way safe to them, and kindest to him!

Let it here be distincily remembered that Roger Williams was,

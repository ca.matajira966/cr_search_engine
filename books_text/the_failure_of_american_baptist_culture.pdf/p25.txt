INTELLECTUAL SGHIZOPHRENIA 9

times. 13 Most Christians agree; however, the dispensationalists have
been more outspoken on this topic. Few Protestants have been as
vociferous in their condemnation of biblical law as the dispensa-
tionalist leader Donald Gray Barmhouse, who was quoted favorably
by S. Lewis Johnson, Jr., in Dallas Theological Seminary’s schol-
arly journal, Bibliotheca Sacra, in 1963: “It was a tragic hour when the
Reformation churches wrote the Ten Commandments into their
creeds and catechisms and sought to bring Gentile believers into
bondage ta Jewish law, which was never intended either for the
Gentile nations or for the church,”!* This was standard fare within
dispensational circles, from 1870 until the late 1970's. As Marsden
states, however, not many of them were absolutely consistent in
their commitment against biblical law, and their abandonment of
politics. There was always an ambivalence.

The growing emphasis on the role of the Holy Spirit, however,
almost demanded some sort of dispensationalism that would draw
a clear line between the Old Testament dispensation of law and
the New Testament dispensation of the Holy Spirit. In 1839,
Charles Finney was already declaring that the day of Pentecost
marked “the commencement of'a new dispensation,” in which the
new covenant replaced the old. The distinction between the two
covenants was not new, but the central place given to Pentecost
and the Holy Spirit soon pushed interpretation in a new direction.
In the new dispensation those who had received the anointing
with the power of the Holy Spirit were radically different from
professing Christians who were still in bondage to the law. More-
over, the freeing and empowering work of the Spirit was known
experientially, not by laboriously conforming to codes of law and
order, Accordingly, in the thirty years after Finney and Mahan
first adopted their holiness views, the place of the law was
drastically reduced in the writings of Reformed advocates of
holiness. After 1870, when they spoke of the dispensation begun
at Pentecost, they stressed the personal experience of being filled
by the Spirit and the resulting positive personal power for service.
By this time it was rare to find holiness teachers of any sort stress-

13. Writes Roy L. Aldrich: “In conclusion, the abrogation of the Mosaic law does
not mean abrogation of the eternal moral Jaw of God. Laws are not identical because
they are based upon identical moral principles. Only a divinely instituted theocracy
could enforce the Mosaic ten laws with their death penalties, and no such govern-
ment exists today. The moral law of God belongs to all ages and its authority extends
to all intelligent creatures whether men or angels.” Aldrich, “Has the Mosaic Law
Been Abolished?” Bibliotheca Sacra (Qet., 1959), p. 335. (Problem: who is to enforce
this moral law, and using what sanctions?)

14. 8. Lewis Johnson, Jr, “The Paralysis of Legalism,” Biédiotheca Sacra (April-
June, 1963), p. 109.

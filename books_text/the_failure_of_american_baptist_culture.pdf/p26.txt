10 GHRISTIANITY AND CIVILIZATION

ing the Old Testament law as the secret to a happy Christian life.
The mood of the revivalist evangelicalism of the day was
suggested by Philip Bliss’s verse, “Free from the law, oh happy
condition, . . .”

The Spirit-oriented holiness teaching, spreading quickly in this
period, encouraged a clear distinction between law and Spirit,
Old Testament and New Testament, and seems to have been a
major factor paving the way for the acceptance of a more definite
dispensationalism in the later nineteenth century. By the 1870s
when the dispensationalist movement began to take hold in Amer-
ica, holiness teachers already commonly spoke of “the Dispensa-
tion of the spirrr.” This and similar phrases became commonplace
within the premillennial movement, with the age of the Spirit
sharply separated from the age of law. C. I. Scofield in his classic
formulation called these two dispensations “Law” and “Grace.”
He did not make Pentecost itself the turning point but he did
argue that the special characteristic of the age of grace was the
presence of the Holy Spirit in every believer and the necessity for
repeated “fillings” with the Spirit.

The contrast between the present New Testament age of the
Spirit and the previous Old Testament age of law did involve a
shifl toward a more “private” view of Christianity. The Holy
Spirit worked in the hearts of individuals and was known pri-
marily through personal experience, Social action, still an impor-
tant concern, was more in the province of private agencies. The
kingdom was no longer viewed as a kingdom of laws; hence civil
law would not help its advance. The transition from postmillen-
nial to premillennial views was the most explicit expression of this
change. Politics became much less important.

Few premillennial-holiness evangelists, however, carried the
implications of their position to the conclusion ~ more often found
in the Anabaptist tradition ~ that since Satan ruled this age and it
governments, Christians should avoid all political action, even
voting. Far more characteristic was a position—typical of the
pietist tradition—that saw governments as ordained by God to
restrain evil, so that politics in this respect was a means to do
good, What they gave up—at least in theory — was the Calvinist-
Puritan Old Testament covenantal view of the identity of the peo-
ple of God with the advance of a religious-political kingdom. Even,
this idea was not abandoned totally or consistently. Sabbath
legislation—despite its Old Testament origins and intention to
promote both Christianity and human welfare—continued to be
an interest of many. Likewise, prohibition, which was both an at~
tack on a demonic vice and a progressive reform for improving
civic lite, received support from almost all evangelical quarters.

15. Marsden, Fundamentalism, pp. 87-88. Note: he contrasts Anabaptism with
Calvinism-Puritanism.

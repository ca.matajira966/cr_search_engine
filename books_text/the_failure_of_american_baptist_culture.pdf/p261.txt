CHRISTIANITY AND RELIGIOUS LIBERTY 247

from the Calvinists, though as I understand them, he mis-
understood what they were saying. He helieved that man was so
fallen into sin as to be incapable of making right decisions in
religious matters. There was no grace given to solve this problem
sufficiently. The Reformed, or Calvinist theologians never believed
that.

Qddly enough, Williams thought that one of the chief flaws in
man’s nature was his faulty conscience. God had given him enough
grace to save his soul for heaven, but nothing beyond this bare
minimum. He could be saved, but he still had a conscience prone to
exror. If anything was certain, it was that man had a tendency to er-
ror. I know I make my share of errors, K. D., and I know you
would say the same of yourself.

Well, in light of the fact that we have faulty consciences, and that
we're bound to err by our very nature, none of us is really capable of
making judgments regarding the absolute truth or falsehood of
another man’s religious beliefs. Since we are all finite and subject to
error, one man must be prevented from imposing his possibly errant
religious opinions on another. The conscience of man is just not de-
pendable. Here’s what Williams said:

The conscience is found in all mankind, more or less; in Jews,
Turks, Papists, Protestants and pagans . . . I have fought against
many several sorts of consciences; is it beyond all possibility that I
have not persecuted Jesus in some of them?

Granted. We all make mistakes, But it seems to me that he is not in-
terested in any “more or less” error, or in any distinction between
possible and probably error. Man is subject to error, so we should all
just keep quiet and do our own things. You can’t judge me because
you don’t know whether you're judging error or judging Christ. Isn't
it possible that, in the end, youre persecuting Christ?

He extended his position of personal, individual fallibility to the
Christian Church as well. Now I know that we both agree that there
is no infallible Pope, or for that matter, any infallible church. And
neither of us believes that as individuals we are infallible. So it
would seem to follow that just as the individual conscience could be
mistaken in some important matters, so could the corporate
conscience~the Christian Church, And just as the individual
should be prohibited from inflicting his possibly erring conscience
on anyone else, so also the church should be prohibited. You as an
individual might err in your judgments and condemn others by
your own misconstrued religious opinions. The church also might

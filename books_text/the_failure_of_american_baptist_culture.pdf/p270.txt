256 CHRISTIANITY AND GIVILIZATION

things. There was a common ground of religious ideas to which all
seemed to subscribe. Especially, they thought, there were certain
standards of practical religion to which all men and religions
subscribed. And, if all men required the same standards for practical
religion, then what further need did they have for defending all the
possible bones of contention among them? There was no need for
them to squabble over religious doctrine or dogma. The generally
accepted moral code was enough. Everybody knows, K. D., that
you just don’t go around killing people. Not only is it unchristian, it
just isn’t reasonable. What religion could condone it?

And that brings up the subject of “reason.” It seems like a com-
mon possession of everyone. It clearly dictates “rights” from
“wrongs,” And if there is to be any neutral ground found on which
we can build a peaceful and orderly society, it has to be on that com-
mon ground of reason.

From my reading of the seventeenth century, “Reason” (with a
capital “R”) was the heart of a new religious philosophy called
“Deism.” [ think this affected Williams. I don’t know whether you're
familiar with it, but it seers to me that it plays a crucial part here in
this subject. Williams and his followers, and for that matter all those
who support the “religious liberty” idea, all embrace the religion of
Deism in principle and practice, if not also in name. I hope I don’t
sound too dogmatic to you, but this Deism is so slippery a notion
that it could come into our thinking without our ever really knowing
it. And frankly, K. D., that worries me.

As I understand it, one of the “fathers” of this religion was a
fellow named Edward, Lord Herbert of Cherbury. Quite a name to
carry around. Now Mr. Cherbury believed that despite the fact that
man is in nature, a state removed from grace, he is a rational being.
Here’s where J think Williams was contradictory in his thinking;
man’s conscience is so marred that he can’t be assured of the legiti-
macy of any of his actions in a world that’s a dungheap; yet he’s not
so unreasonable and not so covered with the soot that he can’t make
a proper society —a “city” as he called it. I think he was influenced
by Mr. Cherbury or someone like him.

Mr. Cherbury believed that all men, according to their reason,
held to some “Common Notions.” His book, De Véritate, Prout
Distinguitur a Revelatione, a Veristmili, a Posibili, et a Falso presents
some of his position on this. Mr. Cherbury, whom I think we could
call a “religionist” more than a “theologian,” says that among all
religions there are certain undisputed, basic principles; and these
principles are evident from religion, philosophy, law, and of course,

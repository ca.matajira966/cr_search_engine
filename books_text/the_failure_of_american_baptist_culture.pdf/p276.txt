262 CHRISTIANITY AND CIVILIZATION

trouble, From looking at some of the problems Rhode Island had, I
think, K. D., that you will see where this idea eventually leads,
Then Pll pick back up the “neutrality” subject.

I propose, K. D., that when the “liberty of conscience”
“religious liberty” idea is allowed to run its full course, it leads in-
evitably to dissent ahd social disintegration. Williams himself came
to recognize this in a limited way through his conirontation with
consciences more liberated than his own, And this is the problem:
the “liberated conscience,” Once you grant that it is the ultimate
standard along with “reason,” there’s no defense against it. Man,
Williams found out, was not always as “reasonable” as he had sup-
posed him to be. And he found out that man could even appeal to
“liberty of conscience” and “religious liberty” to support his
unreasonable rights.

The Quakers, a religious group in colonial America, claimed
something in particular which bothered Williams and pressed his
principle of “liberty” to its breaking point. It was their claim to
union with Ged. This struck right at the Achiles heel of Williams's
Nature/Grace dichotomy.

Rather than appealing to Nature and to Reason, as Williams
and so many other had done, the Quakers claimed direct commu-
nion with God-=--or, we could call it, with grace. As I understand it,
their emphasis lay in direct communion or communication with
God as His Spirit would emerge at a typical meeting and speak in
and through the people. With this Williams had no sympathy. Com-
plaining against the Quakers he said:

The Spirit of God was most purely rational, and a spirit of pure
order, and did not prompt or move men to break hedges and leap
over one ordinace to another.

Williams says that the Spirit is “purely rational,” and therefore, He
cannot be operating the way that the Quakers say He is. But the
Quakers would say that it is perfectly reasonable for the Spirit to
work the way He does—alter all, if the Spirit comes to us and works
this way, then we have to conclude that this is “reasonable” accord-
ing to “spiritual reason.” We have to be open to the way the Spirit
works. We can’t confine Him to our reason; we have to let Him work
according to what is natural for Him,

Here the Quakers were destroying Williams’s Nature/Grace
dichotomy —or rather, emphasizing one aspect of it. That is, ac-
cording to the Quakers, God (or grace) could break into nature and
operate any way in which He chose. Nature is not such a self-

 

A REFORMED VIEW OF FREEMASONRY

Everett C. De Velde, Jr.

Y first meaningful encounter with Freemasonry occurred dur-
M ing my. ministry to a Presbyterian Church in Savannah,
Georgia, One of the elders had been a 32nd degree Mason and one
of the deacons was a leader of the “Blue Lodge.” With several other
leaders of the church being involved with Freemasonry, I undertook
a study of the subject and after due course all who were so involved,
demitted Masonry. These men supplied me with ample quantities of
study materials and the local city library provided much more.
While Freemasons certainly entertain “secrets,” the doctrines of
Freemasonry have been openly published and are readily available.
Tt seems incredible that many men, who have given so rauch of their
lives to Freernasonry, should know so little about it. In this essay 1
will attempt a brief averview of what I have come to know as
Freemasonry.

kee ERK

The term “Freemason” is supposedly derived from King
Solomon’s use of Huram’s craftsmen. Huram (or Hiram), then
King of Tyre, did in fact supply craftsmen to work on the Temple in
Jerusalem, but he received from Solomon “wheat, barley, oil and
wine”! in return for his services. The term “Freemason,” at the
outset, is to some extent a misnomer.

The actual historical origins of Freemasonry relate to the fact
that, unlike other craft guilds, masons had to travel from place ta
place to find work. In order to preserve distinctions of rank, and
craft secrets, masons devised a set of secret signs so that one mason
might recognize another as a true mason, and not as a pretender to
the craft. In time, numbers of Renaissance freethinkers came to be

1. TI Chronicles 2:15

277

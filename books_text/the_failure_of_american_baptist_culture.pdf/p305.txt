REVIEWS 291

the Augustinian has an answer for this: God chooses to draw only some men
to Himself, and they alone have ability to come; yet God holds all men
responsible, For the Pelagian, however, responsibility is limited by ability,
Another element of Pelagianism is seen in the notion, expressed in Davis's
statement above, that men are “totally without capacity... to achieve
anything, redemptively, unaided by special grace.” Implicit in this
phraseology is synergism, the heresy that man cooperates with God in sav-
ing himself, God does so much, but man must mect God half-way and com-
plete God’s work. Thus, Davis's atternpt to dissociate Anabaptism from the
heresy of Pelagianism simply serves to implicate him in the same error.
A second aspect of Pelagianism, generally considered, is perfectionism.
Zwingli was one of the first to charge the Anabaptists with perfectionism,
but he was not the last, for the charge has been repeated throughout the
history of Anabaptism. Once again, Davis's anempt to clear “pure”
Anabaptism from this charge only convinces the reader that in fact the
Anabaptists really did believe in perfection, potential and actual. “Evangeli-
cal Anabaptists conceived of the goal of holiness, or godliness, as a limited
kind of ‘divinization’ (participation in the divine nature) of man by a resto-
ration through a regenerative and healing proces

 

in conjunction with one's
conscious, voluntary emulation of Christ” (p. 137). Davis secs this as incor-
poration into Christ's humanity, not implying actual divinization, so that
“the awareness of one’s creatureliness, of an eternal ‘distinctiveness of being’
from ultimate Deity, is never lost” (p. 137}. We may ask, however, whether
Davis is perhaps reacting orthodox Calvinism back into Anabaptist thought
al this point. Whatever the confusion among the Anabaptists at this point, it
ar that the line between creature and Creator wa

 

not fully maintained

 

is
even by the more evangelical wing of Anabaprism, and wherever that line is
obscured, Pelagianism results, and with it perfectionism. A defective view
of depravity always leads to personal or social utopianism (cf. B. B.
Warfield, Perfectioniom [Presbyterian and Reformed], pp. 3, 63.). Augusti-
nianism, however, follows Scripture in holding that indwelling sin remains

 

until man receives the glorified budy (Rom, 7:7-25; 8:30).
A third line of
between Franciscanism and Pelagianism. Davis's greatest strength, and the

 

ticism here is that Davis fails to speak to the connection

  

real contribution of his book, is his drawing of theological connections
among the Anabaptists and their predecessors. This reviewer, however,
believes that Davis fails to make several important connections, important
from a Reformed point of view at least. For example, in common with all
Anabaptist historians this reviewer is familiar with, Davis wants to speak of
pure Anabaptism as essentially ditlerent from certain extreme or aberrant
forms of it. German mystics and Muntzer fanatics are two of the groups
which Davis tries to sever from Anahaptism as a whole. Theologically, how-

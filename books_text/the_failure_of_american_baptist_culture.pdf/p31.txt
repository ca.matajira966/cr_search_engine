INTELLECTUAL SCHIZOPHRENIA 15

minds of other “reasonable” men; 3) that the mind of man can dis-
cover the regularities of external nature; 4) that mathematics— the
precise logic of the mind—is uniquely capable of describing the
operations of the external world.22 Most important, men assume that
no prior reference to God the Creator needs to be made by any scientifically objec
live investigator. Christians have accepted these assumptions of the
humanists, despite the growing evidence that philosophy since
Hume, Kant, and Hegel—let alone the followers of Heisenberg—
cannot defend these presuppositions. Thus, Christians have granted
to self-professed autonomous man the right to interpret the facts of
existence apart from God and His revelation.

Cornelius Van Til has devoted his long career to a relentless
refutation of these assumptions. In this sense, he broke with
Machen and the older rationalistic apologetics, from Justin Martyr
to Aquinas, to Bishop Butler, to Paley, and to the Calvinists at
Princeton Theological Seminary. He also broke with virtually all
fundamentalist philosophers. In 1932, in his syllabus, The
Metaphysics of Apologetics, he wrote:

From these considerations it ought to be evident that one can-
not take the possibility of neutrality for granted. To be philosophi-
cally fair, the antitheist is bound first of all to establish this possi-
bility critically before he proceeds te build upon it. If there is an
absolute God, neutrality is out of the question, because in that
case every creature is derived from God and is therefore directly
responsible to him. And such a God would not feel very kindly
disposed to those who ignore him. Even in human relationships it
is true that to be ignored is a deeper source of grief to him who is
ignored than to be opposed. It follows then that the attempt to be
neutral is part of the attempt to be antitheistic. For this reason we
have constantly used the term antitheistic instead of nontheistic.
To be nontheistic is to be antitheistic. The narrative of the fall of
man may illustrate this point. Adam and Eve were true theists at
the first. They took God’s interpretation of themselves and of the
animals for granted as the true interpretation. Then came the
tempter. He presented to Eve another, that is, an antitheistic
theory of reality, and asked her to be the judge as to which was the
more reasonable for her to accept. And the acceptance of this posi-
tion of judge constituted the fall of man. That acceptance put the
mind of man on an equality with the mind of God. That accep-
tance also put the mind of the devil on an equality with God. Be-
fore Eve could listen to the tempter she had to take for granted

29. See especially the brilliant interpretation of the historian of science, Stanley
Jaki, in The Road of Science and the Ways to Gud, ch. 16.

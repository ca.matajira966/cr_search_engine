20 CHRISTIANIVY AND CIVILIZATION

to state-supported universities; nobody would give them five
minutes in a courtroom, But they try to get some judge to listen to
them regarding the high schools. Result, after almost fifteen years of
lobbying: the schools teach evolution, and nothing but evolution.

What is the proper argument? Simple: there is no neutrality,
and since there is no neutrality, the present legal foundation of
government-financed edication is a fraud. Conclusion: close every
government-financed school, tomorrow. Refund the taxes to the tax-
payers. Let the taxpayers seek out their own schools for their chil-
dren, at their expense (or from privately financed scholarships or
other donations). No more fraud. No more institutions built on the
myth of neutrality.

But the fundamentalists instictively shy away from such a view.
Why? Because they see where it necessarily leads: to a theocracy in
which no public funds can be appropriated for anti-Christian
activities, or to anarchy, where there are no public funds to ap-
propriate. It must lead to Ged’ civil government or ze civil govern-
ment. In short, it leads either to Rushdoony or Rothbard.?” Most
fundamentalists have never heard of either man, but they instinc-
tively recognize where the abandonment of the myth of neutrality
could lead them.

How can one have compulsory education and the separation of
church and state, if education is by nature religious? This was the
issue which Rushdoony dealt with in The Messtanic Character of
American Education, and so did Sidney E. Mead, a prominent church
historian in the early 1960's. In a book published in the same year as
Messianic Character, Mead wrote:

Here are the roots of the dilemma posed by the acceptance of
the practice of separation of church and state on the one hand,
and the general acceptance of compulsory public education spon-
sored by the state on the other. Here is the nub of the matter that
is all too often completely overlooked. It was very clearly stated by
J. L. Diman in the North American Review for January, 1876, If it is
true, he said,

that the temporal and spiritual authorities occupy two wholly
distinct provinces, and that to one of these civil government
should be exclusively shut up... . it would be difficult to make
our a logical defense of our present system of public education. If,

27. Murray N, Rothbard, Power and Market: Government and the Economy (Menlo
Park, California: Institute for Humane Studies, 1970). Rothbard is the chief econo-
mist in the anarcho-capitalist movement.

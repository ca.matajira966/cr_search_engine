INTELLECTUAL SCHIZOPHRENIA 21

on the contrary, it be the right and duty of the state to enforce sup-
port of public education . . . [upon all citizens], then our current
theory respecting the nature and functions of the state stands in
need of considerable revision.

Diman’s point is based upon the recognition that of neces-
sity the state in its public-education system is and always has been.
teaching religion. It does so because the well-being of the nation
and the state demands this foundation of shared beliefs. In other
words, the public schools in the United States took over one of the
basic reponsibilities that traditionally was always assumed by an
established church, In this sense the public-school system of the
United States is its established church. But the situation in
American is such that none of the many religious sects can admit
without jeopardizing its existence that the religion taught in the
schools (or taught by any other sect for that matter) is “true” in
the sense that it can legitimately claim supreme allegiance. This
serves lo accentuate the dichotomy between the religion of the
nation inculcated by the state through the public schools, and the
religion of the denominations taught in the free churches.

In this context one can understand why it is that the religion of
many Americans is democracy —why their real faith is the “demo-
cratic faith”—the religion of the public schools, Such under-
standing enables one to see religious freedom and separation of
church and state in a new light.?&

How can creationists and fundamentalists support “the religion
of democracy” — the legitimacy of public education — and simultane-
ously deny the validity of the religion of secular humanism? The
religion of democracy is secular humanism in America.

The Christian School Movement

Some fundamentalists. have begun to understand the impli-
cations of the myth of neutrality, at least in the field of primary and
secondary education. They have pulled their children out of the
humanist schools. Sometimes this has been because they are tired of
the drugs, or the permissiveness, or the sex education classes, or
some other “side-effect” of humanist education. But steadily, the
parents and headmasters are coming to a better understanding of
the positive aspect of the Christian school movement: the new
religious and philosophical foundation which the Christian school
offers to their children, and even to society in general. As Christians

28, Sidney E. Mead, The Lively Experiment: The Shaping of Christianity in America
(New York: Harper & Row, 1963), pp. 67-68,

23 CHRISTIANITY AND CIVILIZATION

for years with Rev. Billy Graham, who is also a T. V. evangelist. No
one associates Rev. Graham’s ministry with the “electronic church.”
The commentators never attacked Kathryn Kuhlman. They ignore
Bill Bright's Campus Crusade for Christ. They ignore Rex Hum-
bard, too. Bur Jerry Falwell is fair game. Why? Because Jerry
Falwell is getting mixed up in politics. This scares traditional fun-
damentalists, too. They think he has “quit preaching the gospel,”
meaning the religion of retreatist pietism. The same hatred by the
press has been aired at evangelist James Robison, and the same
criticism has been made by traditional fundamentalists.

Yet both Falwell and Robison claim to believe in religious lib-
erty. They both have forthrightly proclaimed the right of free
speech. Why, then, are they attacked? The Moral Majority is ex-
plicitly non-theological; it is strictly a political movement, its
founder and governors maintain. When Rev. Falwell appears on a
talk show, he is painfully careful to stress that he is not speaking as a
potential theocrat, but only as a concerned citizen. In his interview
in Penthouse (which is analyzed in detail by Kevin Craig in an essay
which appears elsewhere in this journal), he explicitly denied that he
was trying to impose his religious views on other people. But the
humanists know better: his political views are the product of his religious
views. One cannot successfully separate religion from politics, for
one cannot separate morality from legislation. There is no moral
neutrality, just as there is no political or legislative neutrality. Rev.
Falwell’s political opinions are shaped by his religious conclusions,
just as his opponents’ political opinions are shaped by their human-
istic religious conclusions. In short, the humanists recognize far bet-
ter than even Mr. Falwell—ifhis words are to be believed —that he
is indeed trying to legislate his religion. Rev. Falwell certainly
recognizes that his humanistic opponents are themselves defenders
of a rival religion. Why fight a religious war ~ Christian morality vs.
humanistic morality—in the name of religious neutralism (unless it
is a tactical move)? It is a religious war. Why try to conceal it (unless
it is a tactical move)?

Here is the intellectual problem that is disrupting the New
Christian Right. If there is no neutrality~and the argument against
the religion of secular humanism is based on this conclusion—then
no moral system can legitimately be constructed in terms of neutral-
ity. But if neutrality is removed as a foundation for morality, then
what must be put in its place? A political majority? But that is the
argument of the humanists, who cling to the religion of democracy.
What must be the foundation of morality? By what standard must we

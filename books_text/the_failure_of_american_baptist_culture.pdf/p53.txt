INTELLECTUAL SCHIZOPHRENIA 37

humanist culture-~a warning issued in 1969, a decade before the
New Christian Right came:into existence:

But, to continue, a man may claim to believe in God when he is
actually an atheist to all practical intent if he tries to separate
religion and state, if he denies God His sovereignty over the state.
It is impossible to separate religion and state. All law is enacted
morality, and all morality rests on religious foundations, and is the expres-
sion of religion. Thus, every legal system, i. €., every state, repre-
sents a religious order and is a religious institution, The state can-
not be neutral to religion. It is either Christian or anti-Christian.
A state may he neutral with respect to churches, i. e., the particu-
lar institutional forms of Christianity, but it cannot be neutral
with respect to Christianity. Today, Christianity is in the process
of being disestablished as the religion of Western states, and
Humanism is rapidly being established as the official religion of
church, state, and school. The decisions of the courts increasingly
have little reference to Christianity and older legislation: they are
religious decisions which promulgate the faith of Humanism. . . .

In every area, all authority is in essence religious authority.
The religious vary from country to country, but authority is in
essence religious. When men deny the ultimate and absolute
authority of God, they do so in the name of another ultimate
authority, the aulonomous consciousness of man. Where author-
ity is broken, either chaos and anarchy will reign after a time, or
brutal coercion will prevail... .

As Christians, we may rightly hold that a Christian-theistic
doctrine of authority should prevail, but we may not destroy
institutions by revolutionary activity: we must create new institu-
tions by means of new (converted) men.36

Conclusion

The traditional goal of ecclesiastical independents, narnely, the
rigid separation of church from civil government, must not be
misinterpreted to mean the separation of religion from civil govern-
ment. We must not be blinded by the myth of neutrality in any
form. We can make use of it, however, whenever we face those who
still cling to it, and who can, in a court of law, be challenged success-
fully to leave us alone while we go about our business of Christian
reconstruction.

We have to face up to the choice that must be made between
God’s law or man’s law. We have to acknowledge the inescapable

36. Rushdoony, Newsletter 4 (January 1, 1969), p. 2.

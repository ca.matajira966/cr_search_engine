46 CHRISTIANITY AND CIVILIZATION

sinners shall be converted unto thee” (Ps. 51:13). The evangelism of
the fundamentalists is not Biblical as long as the demands for
Biblical social action are not proclaimed. And the opposite error
renders their social action impotent as well.

They Divorce Evangelism from their Social Action.

The separation of social action from evangelism is somewhat un-
conscious, being dictated by the dispensational theology of evangeli-
cals and fundamentalists. But the separation of evangelism from
their social action is more deliberate, and this is what is so ironic, It
is no longer the fundamentalists, or the evangelicals, but the
“Reconstructionists,’ who are stressing the importance of
evangelism. Evangelism strangely plays no part in the social action
strategy of the “Religious Right.” Dr. Jerry Falwell, in an article
enigmatically entitled, “Moral Majority Opposes Christian
Republic,” declares, “Moral Majority is a political organization and
is not based on theological considerations.” The Moral Majority
apparently asks no one to believe the Bible and to obey it. The legiti-
mate “right” of all men to be as gods, deciding for themselves what
is right and what is wrong, is defended. The Moral Majority merely
wishes that men would exercise their sovereignty as traditional conser-
vative gods, rather than as progressive liberal ones. Once this
awesome program of conversion takes place, Congress should
recognize the new consensus and pass laws accordingly. That this
committment to religious neutrality is wrong should be immediately
apparent, But it deserves scrutiny.

Proposition III: Biblical Social Action Must, in the Name of
Christ, Self-Consciously Challenge Man’s Autonomy.

The “Religious Right” will not convert men—fully turn them around
to a changed direction of life in obedience to God’s Word. Thus,
they cannot achieve lasting, meaningful social change. They refuse
to challenge man’s claims to political autonomy with the claims of
“the self-attesting Christ of Scripture,” of Him Who demands faith
and obedience to His Word because He is the Lord. Four popularly
accepted political myths fly in the face of the claims of the King of
Kings. None of thern is challenged by the “Religious Right.” Let us
examine each.

16, Jerry Falwell, “Moral Majority Opposes Christian Republic,” Moral Majority
Report, Vol. 1, No. 13 (Oct. 15, 1980), p. 4.

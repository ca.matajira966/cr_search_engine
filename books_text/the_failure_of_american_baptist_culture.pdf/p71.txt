SOCIAL APOLOGETICS 35

Bible sets down a camplete social pattern for parents and for the
State. It is only the slightest exaggeration to state that judges need
walk into their courts with their Bible and nothing else. Men are not
given the prerogative to legislate new laws or even to confirm God’s
law on the basis of the “dignity of Man” (or any other theological
concept). God has spoken. Man is naw to obey. God’s law, not
“Human Rights,” is the source of justice; God’s law guarantees our
freedoms.

Myth #3: The Myth of Neutrality: The “Religious Right” Will
not Challenge Man’s Claims to Religious Neutrality.

Biblical social action is the reconstruction of our society according
to the standards of God’s law as revealed in the Old and New
Testaments of the Bible. We who seek to please the Lord Jesus
Christ wish to persuade all men to submit to the Biblical world-and-
life view. Even if we separate our political efforts from our attempt
to evangelize, we still view evangelisrn as somehow important, and
our involvement in the political arena affords us new evangelistic
opportunities, We throw these opportunities away if we accept the

“myth of neutrality.

There is no such thing as an agnostic. The agnostic claims that he
does not know whether or not God exists, but that he is willing to
search and perhaps someday find out. Much less is there such a
thing as an atheist. The atheist says he sincerely, honestly, and ge-
nuinely does not believe that there is a god, and certainly not the
God of the Bible. The Bible has some very strong words for such
men: They are liars. When the atheist tells you “he’s just looking at
the facts” when he concludes that there is no God, he is lying. The
“facts” are not neutral. There is only one legitimate conclusion one can
draw from the “facts” and that is that the Lorp He is God.

Let us look at two passages from the Bible that declare the non-
neutrality of all “facts.” First, Psalm 19:1-4:

The heavens are telling the glory of God, and the firmament is
showing His handiwork. Day after day pours forth speech, and
night after night declares knowledge, There is no speech nor lan-
guage where their voice is not heard. Their line goes out through
all the earth, and their words to the end of the world.

Every day, men look out onto the world and see inescapable proof that
the God of Scripture is their Creator and Lord. Even the unique
ability to communicate verbally with one another evidences our cre-
aied nature. The “facts” are not neutral: they declare the glory of God!

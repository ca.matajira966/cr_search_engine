SOCIAL APOLOGETICS 57

true, but the Bible tells us that the latter is always true.3*

The myth of neutrality is obviously the theological first-cousin of the
myth of free-will, so ably destroyed by Martin Luther in his debates
with Erasmus over the Bondage of the Will. Because so many in the
"Religious Right” are governed by an Arminian, rather than
Augustinian, theology, they easily fall prey to the myth of neutral-
ity. The presumptuous claim of the unbeliever is that he could
become a Christian if he wanted to; that he could agree with the
teachings of Scripture if he chose to, but just hasn’t been persuaded
for lack of evidence. The Arminian agrees. The unbeliever tells
himself and the world that his thinking processes are just fine. Not
just in the area of “religion” or “spiritual” matters,39 but in every
subject, and every discipline. And there is no dissent from the Armi-
nian. In fact, the Arminian would be the first to tell the natural,
unregenerate man that, if he wanted to, he could see the Kingdom
(John 3:3), understand the Christian system (I Cor. 2:14), object-
ively hear the Word of God (John 8:43), and receive its truth (John
14:17), Following this fair-minded and non-partisan examination of
the truths of Christianity, the unregenerate man could then submit
to the Lordship of Christ (but only if Ag made the decision —I Cor,
12:3), and then subject himself to the whole law of God, surely
becoming a great Christian statesman (Rom, 8:7). Clearly, the
Arminian, in affirming the sovereign will of unsaved man over the
sovereign grace of the Triune God, runs pell mell against the Word
of God. According to the Arminian,

There is not necessarily any sin invalved in what the unbeliever,
or natural man, does by. way of exercising his capacities for
knowledge and action. On this view the natural man does not
need the light of Christianity to enable him to understand the
world and himself aright. He does not need the revelation of
Scripture or the illumination of the Holy Spirit in order that by
means of them he may learn what his own true nature is... .

38. Many Christian social activists fail vo understand just how non-neutral the
natural man is. Many have the idea that the non-Christian is not neutral wich
respect to “religion” in general. The humanists assert that Christianity is not
“scientific,” to which some will reply, Oh yeah? Well it rakes just as much faith to be
an atheist, buddy! In other words, no man is neutral because all men hold lo some
beliefs that are not susceptible to the “scientific method.” But the Bible says more.
Religion is not defined by the scientific method; it is defined by the sense of deity that
every man haa: all men know of the Triune Cod, but actively hate Him and rebel
against Him, In éiis respect no man is neutral.

39, Of course, we must recognize that alf subjects are ultimately religious (cf.
note 18 above).

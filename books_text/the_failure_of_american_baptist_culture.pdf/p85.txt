SOCIAL APOLOGETICS 69

Particularly is this true when they are confronted with the specific
contents of Scripture. The natural man will invariably employ the
tool of his reason to reduce these contents to a naturalistic level.
He musi do so even in the interest of the principle of (logic). For
his own ultimacy is the most basic presupposition of his entire phi-
losophy. It is upon this presupposition as its fulcrum that he uses
the law of contradiction.-If he is asked to use his reason as the
judge of the credibility of the Christian revelation without at the
same time being asked to renounce his view of himself as ultimate,
then he is virtually asked to believe and to disbelieve in his own
ultimacy at the same time and in the same sense. Moreover this
same man, in addition to rejecting Christianity in the name of the
law of contradiction, will also reject it in the name of what he calis
his intuition of freedom. By this he means virtually the same thing
as his ultimacy. We seek our point of contact not in any abstrac-
tion whatsoever, whether it be reason or intuition. No such
abstraction exists in the universe of men, We always deal with
concrete individual men. These men are sinners. They have “an
axe to grind.” They want to suppress the truth in unrighteousness.
They will employ their reason for that purpose. And they are not
formally illogical if, granted the assumption of man’s ultimacy,
they reject the teachings of Christianity. On the contrary, to be
logically consistent they are bound to do so.7?

The basic tenet of humanism is that every man is his own god. It
would he iHogical for humanists, as long as they are humanists, to pass
a law against abortion; every man must decide for himself what is
right and what is wrong. This is the problem in America today. It isn’t
a lack of logic; it is increasing consistency to a position of humanism. Men
do not need to take a course in logic. They need to submit to the
Lordship of Jesus Christ. In our day, there is no King in America.
Every man. does that which is logical in his own eyes (Judges 21:25).

The Facts Versus the Faith

Leaders of the “Religious Right” seem embarrassed to be Bible-
believers, so they encourage the troops to appeal to neutral things
like history, public opinion, modern science, and logic. As Kelly ex-
horts, “Force people to fight the facts before they fight the faith.”75
But as we have seen, men fight the facts in order to fight the faith,
because the “facts” reveal God.

The issue between believers and non-believers . . . cannot be
settled by a direct appeal to “facts” or “laws” whose nature and

72, Van Til, Defense, pp. 83-84
73. Kelly, p, 66.

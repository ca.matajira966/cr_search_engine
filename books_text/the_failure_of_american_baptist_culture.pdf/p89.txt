SOCIAL APOLOGETICS 73

consistent political philosophy but only a “practical” defense of the
existing status quo, enshrined as embodiments of the American
“tradition.” Yet, as statism grows and accretes, it becomes, by
definition, increasingly entrenched and therefore “traditional”;
conservatism can then.find no intellectual weapons to accomplish
its overthrow. °°

The only reasons “liberals” have been able “to relegate us to some
isolated corner of the theological world” is that they outnumber
those who espouse a Biblical politics, But now fundamentalists are
beginning to see that a consistent position, when it builds force, can
withstand “pseudo-intellectual hatchet jobs.” Even the Moral
Majority, with its less-than-Biblical presentation, has had some
good effects, and has incurred the wrath of the liberal politico-
theologians. But Jerry Falwell has at least a glimmer of the answer:

The problem is that they have never had credible opposition.
They had always been able to portray the Conservatives as
religious fanatics and got by with it. They can’t do that anymore.

80. Rothbard, Liberty, pp. 301-302. F. A. Hayek, Nobel Prize-winning free-
market economist, is no “extremist,” but echoes Rothbard’s thinking:

‘We must make the building of a free society once more an intellectual adventure, a
deed of courage. What we lack is a liberal Utopia, a programme which seers

- neither a mere defence of things as they are nor a diluted kind of socialism, but a
truly liberal radicalism which does not spare the susceptibility of the mighty (in-
cluding the trade unions), which is not tno severely practical and which does not
confine itself to what appears today as politically possible. We need intellectual
leaders who are willing to work for an ideal, however small may be the prospects
of its early realization. They must be men who are willing to stick to principles
and to fight for their full realization, however remote. . . . Free trade and free-
dom of opportunity are ideals which still may rouse the imaginations of large
numbers, but a mere “reasonable freedom of trade” or a mere “relaxation of con-
trols” is neither intellectually respectable nor likely to inspire any enthusiasm.
The main lesson which the true liberal must learn from the success of the socialists
is that it was their courage to be Utopian which gained them the support of the in-
tellectuals and thereby an influence on public opinion which is daily making possi-
ble what only recently seemed utterly remote, Those who have cancerned them-
selves exclusively with what seemed practicable in the existing state of opinion
have constantly found that even this has rapidly become politically impossible as
the result of changes in a public opinion which they have dane nothing to guide.
Unless we can make the philosophic foundations of a free [read: “Biblical”] soci-
ely once more a living intellectual issue, and its implementation a task which
challenges the ingenuity and imagination of our liveliest minds, the prospects of
freedom are indeed dark. But if we can regain that belief in the power of ideas
which was the mark of liberalism at its best, the Lattic is not lost. “The Intellec-
tuals and Socialism,” in Studies in Philosophy, Politics, and Economies (Chicago: Univ.
of Chicago Press, 1967), p. 194.

 

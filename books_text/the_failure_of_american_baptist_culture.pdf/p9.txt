EDITOR’S INTRODUCTION ix

platonism, at least among predestinarian Baptists. (Peter Lillback’s
essay in this symposium demonstrates with finality that there can be
no such thing as a “Calvinistic” or “Reformed” Baptist.) P. Richard
Flinn has done a good job of exposing and demolishing the platonic
thinking found in “Reformed” Baptist writings, so I shall be brief
here. Platonism (usually realistic) makes a nice accomodation with
nominalism when salvation is seen as the escape of individual
“souls” from this world into heaven, instead of the rebuilding and
resurrection of this world under the influence of heaven. Cornelius
Van Til has written:

Would that all Christians saw the logic of their Christianity!
They would not then seek by haphazard, nervous methods of
revivalism, of individualistic preaching and teaching think of the
salvation for eternity alone and thus fail in large part to ac-
complish what they set out to do. In covenant education [Van Til
is writing on Christian education]’we seek not to extract the
human being from his natural milieu as a creature of God, but
rather seek to restore the creature with his milieu to God. Incom-
parably the wiser is this method since it transplants the plant with,
instead of without its soil. (Essays on Christian Education [Phillips-
burg, NJ: Presbyterian and Reformed, 1974], p. 143.)

In Genesis 1:1, God set up two realms, heaven and earth. The
goal of history is not for earth to be forsaken, but for heaven to im~
press its pattern on earth. “Thy kingdom come, Thy will be done on
earth as tt is in heaven,” we pray. The duality of heaven and earth is to
be resolved, eschatolagically, when carth has become so like heaven
that they are one community. Thus, in Revelation 2f and 22, the
New Jerusalem proceeds out from heaven to earth. Indeed, this is
what the whole doctrine of the economic procession of the Holy
Spirit is about.

In contrast to this Biblical position, platonist Walter Chantry, in
a book full of appalling innuendos against some unnamed adver-
saries (probably British Israelites, from his description), writes:

Who needs stimulation to choose this present world order with its
physical objects? This world holds out its arms to us with powerful
appeals to the senses, A temporal system looks so inviting. We are
drawn by this forceful magnetism of the material. (Emphasis mine;
God’s Righteous Kingdom (Carlisle, PA: Banner of Truth Trusz,
1980], p. 23.)

There is obviously no room in Mr. Chantry’s docetic theology for a
physical incarnation of God in human flesh, hor for a physical resur-
rection such as the Apostles’ Greed teaches all catholics, nor for any

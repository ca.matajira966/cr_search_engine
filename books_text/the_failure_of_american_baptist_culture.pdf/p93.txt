THE MORAL MAJORITY: AN ANABAPTIST CRITIQUE
A Review by James B. Jordan

The Moral Majority: Right or Wrong?, Robert E. Webber. Crossway
Books, Westchester, Illinois, 1981. 190 pp., hardcover. $9.95.

I N his latest book, Mr. Webber uses the so-called Moral Majority
asa springboard to advance the general party line of what he calls
“centrism,” a position which, as the term indicates, steers a middle
course between left and right wing political concerns, and generally
a middle course among evangelical, Catholic, and modernist
theological positions. The book opens with a summary of the think-
ing of the Moral Majority, as interpreted through the writings of the
movement's leader Jerry Falwell. Then, Mr. Webber balances his
discussion and critique of the New Christian Right with a discussion
and critique of the Old Liberal “Christian” Left. The third and
fourth sections of the book are devoted to further shots at the left and
right, and a farther development of the “centrist” position. The
book closes with four appendices, which reprint four “centrist”
creedal statements, and with a bibliography of recommended
readings.

While Mr, Webber does make a number of valid points, some of
which I shall try to note in this review, his book is open to major
criticisrns on four counts: He has too facile a view both of the New
Christian Right and of the Old Liberal “Christian” Left; he makes a
number of historical errors; he has a naive understanding of civil
religion; and his theological perspective is (unwittingly, we trust) far
more similar to the gnostic heresy at certain crucial points than it is
to orthodox, catholic Christianity.

The New Christian Right and the Old Liberal Left

Mr, Webber makes no claim to be interacting with the entire
New Christian Right; rather, the title of his book indicates that his
concern is only with one organization, the Moral Majority, headed
by Rev. Jerry Falwell. At the same time, the Moral Majority is only
one part of a larger group of men and organizations loosely termed

a5

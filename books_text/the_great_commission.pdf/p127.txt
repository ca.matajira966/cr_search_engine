THE CHURCH AND THE GREAT COMMISSION

And He put all things under His feet, and gave Him to be head over all
things to the church which is His body, the fullness of Him who fills all in
all (Ephesians 1:22-23),

As I pointed out earlier, all theological and biblical truth
necessarily has practical implications. The Bible is God’s Word
given to direct us in the paths of righteousness.‘ In the Chris-
tian life, theory is foundational to practice. Or to put it in bibli-
cal terms, truth is foundational to sanctification: “Sanctify them
in the truth; Thy word is truth” (John 17:17).

God has ordained three basic institutions in society: the the
Church, the family, and the State.* A biblical understanding of
their respective roles and inter-relationships is fundamental to
developing a Christian worldview. The fulfilling of the Great
Commission in history will require not only a proper under-
standing of each of these institutions, but also concerned involve-
ment in each.

I now turn briefly to consider a few practical directives for
promoting the truths contained in the Commission. What, then,
are some initial, practical applications of the Great Commission
for each of the three fundamental institutions? In this chapter,
I will focus on the Church.

In 1981 the Association of Reformation Churches published

1. Psa. 110:105; Isa. 2:8; Matt. 7:24; Js. 1:22,
2, See: Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
‘TX: Institute for Christian Economics, 1986), ch. 3: “Oaths, Covenants, and Contracts.”

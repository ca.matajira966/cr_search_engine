The Church and the Great Commission 113

the demands of the Great Commission regarding discipleship.
What, then, should be the Christian’s approach to church life,
as he submits himself to Christ under the Great Commission?

Principles of the Covenantal Church

1. Commitment to the local church. A major and indispensable
aspect of our commitment to Christ involves our membership
in, attendance at, worship in, and service through the local
church. Church attendance and membership is expected and
obligated on several grounds: (a) Christ established the Church
as a part of His ongoing plan for His people.® (b) Christ died
for His Church, evidencing a great love and concern for it.” (c)
The Church is the central place God has ordained for Christian
fellowship and service.* (d) Church attendance puts us under
the ministry of doctrine for our spiritual growth.* (e) Christ has
ordained church officers to govern His people.” (f) Christ has
given spiritual disciplinary power to the officers of the Church
for the good of His people." (g) God has given the sacraments
only to the Church.'* The Lord’s Supper specifically is desig-
nated for the corporate communing among God's people." (h)
God clearly commands us not to forsake attending church.'*

2. Engagement to worship. Christ expects His people to worship
Him in spirit and in truth (John 4:24), corporately in the fel-
lowship of God’s people."

Worship is man’s highest calling. It is to be both generic and

(Spting, 1976), pp. 408.
i. :18; Acts 20:29; Eph, 2:19-22; 1 Pet, 2:5-9.

   

1 "Thess. 5:18-14; Heb.
‘Cor 1L:236

11:20-84.

25. Sce the symbolism of systemic unity in John

   
  
 

32; Acts 17:25; See also: Paul E. Engle, Discovering the
Fullness of Worship (Philadelphia: Great Commission Publications, 1978).

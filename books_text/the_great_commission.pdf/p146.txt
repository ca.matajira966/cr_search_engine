130 THE GREATNESS OF THE GREAT COMMISSION

does not possess unimpeachable authority."° Only Christ has
“all authority” to command us (Matt. 28:18).

3. Exposing evil governmental policies. In that man is sinful,
government easily lapses into sin and must be exposed for its
wickedness,

Unrighteousness anywhere is hated by God. In the sphere of
civil government wickedness especially has horrible and danger-
ous consequences. Bowing to the ultimate authority of Christ
(Matt. 28:18) and seeking actively to “disciple the nations”
(Matt. 28:19), the Christian will “expose the works of darkness”
(Eph. 5:11)." He will be recognized as one at odds with cer-
tain governmental policies because of his commitment to Christ
(Acts 5:21, 29; 17:7-10).

4. Involvement in civil government. In that faith must be exhib-
ited in works (Jms. 2:14-26), and prayer must be undergirded
by labor,” we should engage ourselves actively in our govern-
mental process, and not just “be concerned.”

The call to “disciple the nations” involves actively and dili-
gently setting forth the claims of Christ even before govern-
ments. The Christian should promote governmental polices
rooted in God’s Law. One of the express purposes for God's
establishment of Israel under His Law was that it give an exam-
ple to the governments of the world regarding righteous law.'*
Civil governments are to glorify God in governing their popula-
tions by founding their governments on God’s Law.

In doing this, we should recognize the importance of local
governmental offices, because: (a) Most higher, federal offices
have been gained by those experienced in lower, more local
governments. Thus, in the long run (Matt. 28:20), this will reap
rewards. (b) We have more influence on local government than

10. Exo. 1:15-20; Josh. 8; Dan. 8:8-80; Acts 5:29.

11, For help in this area, see: William Billings, The Christion’s Political Action Manual
(Washington, D.C.: National Christian Action Council, 1980).

12, Op. Matt. 6:11 with 2 Thess. 3:10.

13. Psa. 119:46; 148:11,13; 1 Tim. 1:8-10; Rom. 13:4-9. See Greg L. Bahnsen, By
This Standard (Tyler, TX: Institute for Christian Economics, 1985) and Rushdoony,
Institutes of Biblical Law, 2 vols.

Isa, 24:5; 1:4; Psa. 2:9-10; 119:118,
18, 2 Sam, 28:8; 2 Chron. 19;6-75 Psa. 2:10-12; 148:1, 11.

 
 

   

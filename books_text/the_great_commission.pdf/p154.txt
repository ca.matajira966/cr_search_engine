138 THE GREATNESS OF THE GREAT COMMISSION

mystery, which was altogether unknown to and unexpected by
the Old Testament prophets.

2. God has a separate and distinct program and plan for
racial Israel, as distinguished from the Church. The Church of
Jesus Christ is a parenthetical aside in the original plan of God.

3. The Kingdom offered by Christ in the first century was
postponed until the future.

4, The Church experiences some small scale successes in
history, but ultimately loses influence, fails in her mission, is
corrupted as worldwide evil increases and intensifies toward the
end of the Church Age.

5. Christ returns secretly in the sky to rapture living saints
and to resurrect the bodies of deceased saints (the first resurrec-
tion). These are removed out of the world before the Great
Tribulation. The judgment of the saints is accomplished in
heaven during the seven year period before Christ's return to
the earth.

6. At the conclusion of the seven year Great Tribulation,
Christ returns to the earth with His glorified saints in order to
establish and personally administer a Jewish political kingdom
headquartered at Jerusalem for 1000 years. During this time
Satan is bound and the temple and sacrificial system is re-estab-
lished in Jerusalem as memorials. Hence: the system is “premil-
lennial,” in that Christ returns prior to the millennium, which is
a literal 1000 years.

7. Toward the end of the Millennial Kingdom, Satan is
loosed and Christ is surrounded and attacked at Jerusalem.

8. Christ calls down fire from heaven to destroy His enemies.
The resurrection (the second resurrection) and judgment of the
wicked occurs. The eternal order begins.

Representative Adherents: In the ancient church: None (created
ca, 1830). In the modern church: Donald G. Barnhouse, W. E.
Blackstone, James M. Brookes, L. §. Chafer, John Nelson
Darby, Charles Lee Feinberg, A. C. Gaebelein, Norman Geisler,
Harry Ironside, Hal Lindsey, C. H. MacIntosh, G. Campbell
Morgan, J. Dwight Pentecost, Charles C. Ryrie, C. I Scofield,
John F. Walvoord, and Warren Wiersbe.

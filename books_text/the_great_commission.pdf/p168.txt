152 THE GREATNESS OF THE GREAT COMMISSION

age until the coming of the Son of Man.”*

Such a view obviously is held by amillennialists, as indicated
by Louis Berkhof: “The fundamental idea . . . that the whole
world will gradually be won for Christ, .. . is not in harmony
with the picture of the end of the ages found in Scripture. The
Bible . . . does not lead us to expect the conversion of the
world.”* But dispensational writings are the most widely read
and evidence the most vigorous opposition to the cultural influ-
ence of the gospel, hence my special attention to their views.

Given the widespread popularity of the dispensational system
among evangelicals and dispensationalism’s attempted disavowal
of historical pessimism,” 1 will cite several of their writings in
order to press the point home most convincingly. Dispensa-
tionalist Charles Stevens puts it about as clearly as can be, when.
he states: “The New Testament concept of the church in this
age is typified by the wilderness tabernacle, serving a pilgrim
people, built with traveling facilities, ‘going’ after the lost, visit-
ing, seeking, praying.”** John Walvoord writes: “It is not God’s
plan and purpose to bring righteousness and peace to the earth
in this present age. We will never attain the postmillennial
dream of peace on earth through the influence of the
church.” Wayne House and Thomas Ice agree: “Nowhere in
the New Testament does it teach the agenda of Christianizing
the institutions of the world.”

Dave Hunt follows suit in downplaying postmillennial expec-
tations: “this impossible goal of Christianizing the world is now
being presented as the long overlooked true intent of the Great

25, Ladd, Theology, p. 202.

26. Louis Berkhof, Systematic Theology (Grand Rapids: Wm. B. Eerdmans, 1941), p.
718,

27. See: House and Ice, Dominion Thealogy, “Does Premillennialism Believe in Domin~
ion in History?” (pp. 142-150).

28, Charles H. Stevens, in Charles Lee Feinberg, ed., Prophery and the Seventies
{Chicago: Moody, 1970), p. 110.

29. John F. Walvoord, in Charles Lee Feinberg, ed., Prophecy ond the Seventies (Chica-
go: Moody, 1971), p. 211.

30. H. Wayne Honse and Thomas D. Ice, Dominion Theology: Blessing or Curse?
(Portland, OR: Multnomah, 1988), p. 155.

CONCLUSION

“Hallowed by Thy name. Thy kingdom come. Thy will be done, On
earth as it is in heaven... . For Thine is the kingdom, and the power,
and the glory, forever. Amen” (Matt. 6:9-10, 13b).

I now have completed a fairly thorough analysis of the Great
Commission. Hopefully I have provided solid, Bible-based an-
swers to our three opening questions:

What is the Great Commission?
What is the goal of the Great Commission?
What is the nature of the Great Commission?

I trust that the answers provided will be hope inducing, vision
expanding, and labor encouraging.

It seems to me that the change that is most needful today in
Christian circles in order to recover the greatness of the Great
Commission is a major shift in practical, applied Christianity.
The contemporary Church is afflicted by three corrosive agents:

(2) Rampant smiley-faced superficiality, so characteristic of mega-
ministries and much of Christian publishing and broadcasting, which
largely is resultant from inattention to the Great Commission,

(2) Decades old cultural retreatism, which largely has been engen-
dered by a misconception of the Great Commission.

{3) The time perspective problem, which largely involves a denial of
the Great Commission time perspective,

158 THE GREATNESS OF THE GREAT COMMISSION

The Church and Superficiality

Regarding the matter of superficiality, John A. Sproule la-
ments: “The tragedy today .. . is the apparent disinterest in the
preaching of doctrine in the church. . .. Caught up in the craze
for ‘Christian’ entertainment and psychology, the church is
worse off for it.”' Regarding the accelerating changes in this
direction inside American evangelical churches, David Wells
warns that “the impetus to change is coming from without rather
than from within, and this impetus is primarily sociological, not
theological.”®

Too much in the popular church growth mentality reduces
the role of sound biblical preaching and teaching in deference
to crowd-pleasing antics to draw the play-oriented masses into
churches.” These masses must then continually be entertained
by throwing Christian theology to the lions. Of last century's

 

1. John A. Sproule in John S. Feinberg, ed., Continuity and Discontinuity: Perspectives on
the Relationship Between the Old and New Testament (Westchester, IL: Crossway Books, 1988),
p- $18. James Davison Hunter has written a powerful critique of the theological drift in
evangelicalism entitled: Evangelicalism: The Coming Generation (University of Chicago Press,
1987). In an article on Hunter's book, entided “Theological Drift: Christian Higher Ed
the Gulprit?,” Randy Frame notes: “Hunter argued in the book that contemporary
evangelicalism is moving away from tenets of belief and practice long considered ortho-
dox. There are some who say that the current conflict at Grace Theological Seminary
exemplifies Hunter's observations. . . .” (Christianity Today, April, 9, 1990, p. 48). See
article in the same issue: “Trouble at Grace: Making Waves or Guarding the ‘Truth?,” p.
46.

2. David Wells, “Assaulted by Modernity,” Christianity Teday 84:3 (February 19, 1990)
16. A remarkable illustration of this may be found in the conservative Presbyterian
Church in America's The PCA Messenger. A reader, Carl Gauger, complained in a letter to
the editor: “I find in the article . . . further confirmation of a disturbing trend in evangeli-
calism. Although we continue to voice confidence in the inerrancy of the Bible, we are
tending to use it less and less. .. . In this article... we are given no pretense to believe [the
writer's assumptions] becouse of any biblical authority, It is disappointing enough to see psychol-
ogy parading in a cloak of misquoted biblical references, but when even the pretense of
Biblical authority is removed, I think God's people should rise up and cry foul.” Editor
Bob Sweet responded (in part): “But do you examine everything you read so critically? Do
you require ‘biblical authority’ for everything?” (pp. 8, 4). Wells’ “impetus to change” is
often from secular psychological theories.

3. See Richard Quebadeaux, By What Authority: The Rise of Personality Cults in American
Christianity (Gan Francisco: Harper and Row, 1982). For important correctives to this work,
see: Michael R. Gilstrap’s review, “Media Theo-Fop” in James B. Jordan, ed., The Failure
of the American Baptist Culture, vol 1 of Christianity and Civilization (Tyler, TX: Geneva
Divinity School, 1982), pp. 99-110.

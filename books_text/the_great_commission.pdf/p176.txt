160 THE GREATNESS OF THE GREAT COMMISSION

Having isolated life into neat compartments and having exalt-
ed the “spiritual” over the “material,” the Church has given up
on the world and retreated into its own four walls. The very fact
that churches often sponsor “retreats” inadvertently demon-
strates the acceptability of this mentality. When there is a tend:
to retreatism, there is no proper practice of a holistic Christian faith.

There is dangerous atrophy in the Body of Christ (the
Church) due to chronic retreatism and the more recent onset of
acute superficiality. And the recovery of the true strength of the
family, the Church, and the State will take both effort and time.
Fortunately, the Great Commission, when properly understood,
provides us with the strength needed for the effort (“I am with
you,” promises the One with “all authority”) and the time neces-
sary to the task (“even to the end of the age”).

The Church and Time

Few things have been more destructive to the implementation
of a well-rounded, biblically-grounded Christian worldview than
one’s time perspective. A classic, though inadvertent illustration
of this, is available in an interview with evangelist Billy Graham
a few years back:

Q. If you had to live your life over again, what would you do differ-
ently?

A. One of my great regrets is that I have not studied enough. I wish
1 had studied more and preached less... . Donald Barnhouse said
that if he knew the Lord was coming in three years he would spend
two of them studying and one preaching. I’m trying to make it up.”

A similar problem is admitted by Tim LaHaye. Many Chris-
tians are committed to the approaching end of the age, with all
of its horror (according to their dispensational view):

pp. 17, 18, 19, See also: Franky Schaeffer, Addicted to Mediocrity: 20th Century Christians and
the Arts (Westchester, IL: Crossway Books, 1981); Gary North, ed., Biblical Blueprint Series
(Ft. Worth, TX: Dominion Press, 1986-1987), ten volumes.

7. “Taking the World's Temperature” (no author) in Christianity Today (Sept. 23,
1977), p. 19,

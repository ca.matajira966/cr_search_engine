2 THE GREATNESS OF THE GREAT COMMISSION

The Issue

Surely the most important debate between liberal and ortho-
dox theologians today has to do with the issue of the inerrancy
of Scripture. Is the Bible God's Word and without error? Does
it possess impeccable authority as the certain revelation of God?
This is a fundamental issue with great implications for Christian
faith and practice.

Among evangelical Christians today a related discussion has
developed. This discussion has to do with the problem of the
apparent irrelevancy of Scripture. Is the whole of Scripture
confidently to be applied to ail of life today? Is God’s Word
practical for Christian living and social conduct in every aspect
of modern society? And the significance of the Great Commis-
sion lies at the very heart of this important discussion.

Basically, the issue of the greatness of the Great Commission
may be resolved by properly answering the following three
major questions.

1. What Is the Great Commission? A. Is the Great Commission.
a wholly new divine program to respond to sin, which is set in
stark contrast to and is discontinuous from the Old Testament
program? B. Or is it the capstone of the longstanding coven-
antal program of God to respond to sin, and the fruition of the
development of the progress of redemption that is continuous
with the Old Testament?

2. What Is the goal of the Great Commission? A. Is its goal pessi-
mistic, directing the Church bravely to be a witness in a hope-
lessly lost and dying world despite overwhelming resistance,
while “snatching brands from the fire”? B. Or is its goal opti-
mistic, empowering the Church successfully to promote the
salvation of the world against all resistance, while leading the

8. See the following for helpful studies in this area: Rousas John Rushdoony, Jnfalli-
dility: An Inescapable Concept (Vallecito, CA: Ross House, 1978). James Montgomery Boice,
Does Inerrancy Matter? (Oakland, CA: International Council on Biblical Inerrancy, 1979).
Ronald Youngblood, ed., Evengelicals and Inerrancy (Nashville: Thomas Nelson, 1984).

4, See: Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
‘TX: Institute for Christian Economics, 1985). Rousas John Rushdoony, The Fnstitutes of
Biblical Law, 2 vols. (Vallecito, CA: Ross House, [1973], 1982). Gary North, Tools of
Dominio: The Case Laws of Exodus (Tyler, TX: Institute for Christian Economics, 1990).

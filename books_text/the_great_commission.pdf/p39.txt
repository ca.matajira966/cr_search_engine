The Covenant and the Great Commission 23

covenantal allegiance to Him. “[Baptize] them in the name of the
Father and the Son and the Holy Spirit” (Matt. 28:19b).

4. Ethical stipulations. Those who are bound to Him in baptism are to
learn and obey the stipulations of their sovereign, Christ Jesus. “Make
disciples of all the nations. . . . Teach them to observe all that I com-
manded you” (Matt. 28:19a, 20a).

5. Succession arrangements. Christ establishes His commission for the
extension of His authority through space (“all nations,” Matt, 28:19b)
and through time (“And, lo, I am with you always, even to the end of
the age,” Matt. 28:20b).

Conclusion

The repeated emphasis of Scripture on covenant cannot be denied.
Our God is a covenant-making God, who speaks and acts in history
among men. The redemption He provides in Christ cannot properly
and fully be understood apart from the covenantal progress exhibited
in Scripture. Neither may our tasks as Christians be properly grasped
apart from the covenant. As we shall see, the covenant framework of
the Great Commission holds within it the essence of the Christian
enterprise, of the Christian's calling in the world.

May we dedicate ourselves to that task, as we come to a better com-
prehension of it.

The Declaration of Sovereignty 35

19:16). In Chapter Five we will see how the Great Commission
is also a prophetic Commission. As the Great Prophet, Christ
declares the will of God for all the world, by teaching men “to
observe all that I commanded” (Matt. 28:20a). In Chapter Six,
the priestly aspect of the Commission will become evident. As
the Great High Priest, He secures the worshipful oaths of those
over whom it holds sway, in His command to “baptize” the
nations (Matt. 28:19b).

The Literary Context

The beautiful structure of Matthew's Gospel merits our at-
tention as we consider the Great Commission. Blair comments
regarding Matthew 28:18ff: “Here many of the emphases of the
book are caught up.””” Cook concurs: “With this sublime utter-
ance St. Matthew winds up his Gospel, throughout which he has
kept the principles, which are thus enunciated, distinctly before
our minds,”**

I would go a step further and note that what we read in the
closing words of Matthew's Gospel in the closing days of Christ's
ministry has already been anticipated in the opening words of
the Gospel and of Christ’s earthly life and the beginning of His
ministry. Thus, the very opening chapters of Matthew seem to
expect the conclusion we get in the Great Commission. Let me
just briefly draw out the parallels; they do not seem to be mere-
ly coincidental. They speak of a King who comes (Matt. 1-4) and
receives sovereignty (Matt. 28) over a kingdom.

1. Jesus as “Immanuel.” A. In the birth announcement to Jo-
seph, we have the angelic declaration of the fulfillment of Isaiah
7:14 in Jesus's birth: “Behold, the virgin shall be with child, and
shall bear a Son, and they shall call His name Immanuel,”
which translated means, ‘God with us“ (Matt. 1:23). “God with

Messiah the Prince o The Mediatorial Dominion of Jesus Christ (Edmonton, Alberta: Stil
‘Waters Revival, rep. 1990 [1884)]). See also: Greg L. Bahnsen and Kenneth L. Gentry, Jr,
House Divided: The Breakup of Dispensational Theology (Tyler, TX: Institute for Christian
Economics, 1989), ch. 12. John Jefferson Davis, Christ's Victorious Kingdom: Postmillennialism
Reconsidered (Grand Rapids: Baker, 1984), ch. 4.
87, Edward P. Blais, Jesus in the Gospel of Maithew (New York: Abingdon, 1960), p. 45.
98. F.C, Cook, St. Matthew, p. 45.

 

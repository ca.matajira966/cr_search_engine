38 THE GREATNESS OF THE GREAT COMMISSION

sion, we again have clearly reflected the Trinity: “baptizing
them in the name of the Father and the Son and the Holy
Spirit” (Matt. 28:19).

9. The Mountain. A. Before Christ formally begins His minis-
try, He endures the temptation by Satan in Matthew 4. There
we read of the role of a “mountain” in the temptation to king-
ship: “The devil took Him to a very high mountain, and
showed Him all the kingdoms of the world, and their glory”
(Matt. 4:8). B. In the Great Commission, Christ speaks from a
mountain with newly won royal authority: “the eleven disciples
proceeded to Galilee, to the mountain which Jesus had desig-
nated” (Matt. 28:16).

10. Kingdom Given. A. In the temptation at the opening of the
ministry of the Prophet, Priest, and King,“ Jesus Christ, Satan
offers to give Him the kingdoms of the world: “The devil took
Him to a very high mountain, and showed Him all the king-
doms of the world, and their glory; and he said to Him, ‘All
these things will I give You, if. . .’.” (Matt. 4:8-9). B. In the con-
cluding Great Commission, Christ sovereignly declares that He
had been “given”* “all authority,” not only aver the kingdoms
Satan had authority over, but also in heaven: “All authority has
been given to Me in heaven and on earth” (Matt. 28:18).

11. Worship. A. In the temptation, Satan seeks Christ's wor-
ship of him: “All these things will I give You, if You will fall
down and worship me” (Matt. 4:8). B. In the Great Commis-
sion, we read that Christ receives worship: “And when they
saw Him, they worshiped Him” (Matt. 28:17).

 

44, Following forty days of fasting, the first temptation to turn stones to bread (Matt,
4:2-8) reminds us of the prophet Moses (cf. Deut. 18:18), who fasted forty days and nights
(Exo, 24:28). The second temptation (in Matthew's order) was on the temple, wherein the
priests ministered (Matt. 4:5). The third temptation on the mountain was for Him to
become hing over “the kingdoms of the world” (Matt. 4:8-9).

45. The same word “give” (from the Greek: didom:) is used in two temptation
accounts (Matt. 4:9; Luke 4:6) and in the Great Commission (Matt. 28:18). Luke's Gospel
yecords more detail of this temptation, where Satan says: “To thee I will give ail this
authority, and their glory, because to me it hath been delivered” (Young's Literal Translation,
New Testament, p. 42). Compare this to Christ's “All authority has been gium to Me.”

46, The same Greek word for “worship” is employed in both places (Greek: pros-
bunao).

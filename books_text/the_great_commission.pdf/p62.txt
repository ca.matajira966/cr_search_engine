46 THE GREATNESS OF THE GREAT COMMISSION

have been overwhelming were it not undergirded with the
universal authority claimed by Christ. Hence, the significance of
the “therefore” connecting verse 18 with verse 19.

An exact literal translation of the Greek of verse 19a reads:
“Going, therefore, disciple ye all the nations.”*' The “going” is
a translation of a participle in the Greek. Although they express
action, participles are not true verbs, but rather verbal
adjectives. On a purely grammatical basis, then, participles are
dependent upon main verbs for their full significance. Thus,
they cannot stand alone (hence a writer’s dreaded fear of the
“dangling participle”!).

Some have argued from the grammar here that since the
word translated “go” (literally, “going”) is a participle, it may
not properly be viewed as a command to the disciples, in that
participles do not have mood.” They point out that if it were
intended to express a command to go, it should have been
expressed by a verb in the imperative mood. The position
drawn from this grammatical argument is that Christ's
command actually should be understood as: “Wherever you
happen to be, make disciples.”

Of course it is true that “wherever” we “happen to be” it is
incumbent upon us to make disciples. Nevertheless,
grammatically a participle can carry the force of the main verb’s
action. This is because the participle is so closely tied to the
main verb that it partakes in sense the verb’s force. And the
participle here contextually does have the imperative force of
the main verb, despite its not having the imperative form.*

Furthermore, that this is actually a command to “go” may be
seen in the history of the early church contained in Acts. There

only the eleven were present (Matt. 28:17). For example, A. T. Robertson, Word Picture
in the New Testament, 6 vols., (Nashville: Broadman, 1980), 1:244. Nevertheless, the point
remains: the number was small in comparison to the task.

1. Alfred Marshall, The Interlinear Greek-English New Testament (Qud ed: Grand
Rapids: Zondervan, 1959), p. 136.

22. Robert O. Culver, “What Is the Church's Commission?”, Bibliotheca Sacra 125
[1968] 239-253.

23, See: William Hendriksen, The Gospel of Matthew (New Tastament Commentary)
(Grand Rapids: Baker, 1973), p. 999; Cleon Rogers, “The Great Commission,” Bibliotheca
Sacra 130 [1973] 258-267.

 

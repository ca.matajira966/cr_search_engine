78 THE GREATNESS OF THE GREAT COMMISSION

good works” in all of life. The winning of the mind and will
of the lost will involve teaching all things Christ teaches us in
His Word, in both Old and New Testaments.

69. Tit. 2:14; Eph. 2:10,

VERUSALEM UNDER SIEGE 107

rise up with this generation at the judgment
and shall condemn it, because she came from
the ends of the earth to hear the wisdom of Sol-
omon; and behold, something greater than Sol-
omon is here.

Now when the unclean spirit goes out of a
man, it passes through waterless places, seek-
ing rest, and does not find it. Then it says, “I
will return to my house from which I came”;
and when it comes, it finds it unoccupied,
swept, and put in order, Then it goes, and
takes along with it seven other spirits more
wicked than itself, and they go in and live
there; and the last state of that man becomes
worse than the first. That is the way it will also be
with this evil generation (Matthew 12:41-45).

Because of Israel’s rejection of the King of kings,
the blessings they had received would turn into
curses. Jerusalem had been “swept clean” by Christ’s
ministry; now it would become “a dwelling place of
demons and a prison of every unclean spirit, and a
prison of every unclean and hateful bird” (Revela-
tion 18:2). The entire generation became increasingly
demon-possessed; their progressive national insanity is
apparent as one reads through the New Testament,
and its horrifying final stages are depicted in the
pages of Josephus’ The Jewish War: the loss of all abil-
ity to reason, the frenzied mobs attacking one
another, the deluded multitudes following after the
most transparently false prophets, the crazed and
desperate chase after food, the mass murders, execu-

JERUSALEM UNDER SIEGE 113

As it actually worked out in history, the Jewish
rebellion in reaction to the “locust plague” of Gessius
Florus during the summer of a.p. 66 provoked Ces-
tus’ invasion of Palestine in the fall, with large num-
bers of mounted troops from the regions near the Euphrates
(although the main point of St. John’s reference is
the symbolic significance of the river in Biblical his-
tory and prophecy). After ravaging the countryside,
his forces arrived at the gates of Jerusalem in the month of
Tishri—the month that begins with the Day of Trumpets.

What happened next is one of the strangest stor-
ies in the annals of military history. The Romans
surrounded the city and attacked it continuously for
five days; on the sixth day, Cestius successfully led
an elite force in an all-out assault against the north
wall, Capturing their prize, they began preparations
to set fire to the Temple. Seeing that they were com-
pletely overwhelmed, the rebels began to flee in
panic, and the “modcrates,” who had opposed the re-
bellion, attempted to open the gates to surrender
Jerusalem to Cestius.

Just then, at the very moment when complete
victory was within his grasp, Cestius suddenly and
imexplicably withdrew his forces. Surprised and
encouraged, the rebels turned from their flight and
pursued the retreating soldiers, inflicting heavy
casualties in their attack. This unexpected success by
the rebel forces had the effect of creating an enor-
mous but completely unrealistic self-confidence
among the Jews, and even the moderates joined in
the general enthusiasm for war. Instead of heeding
the true message of this warning blast of the

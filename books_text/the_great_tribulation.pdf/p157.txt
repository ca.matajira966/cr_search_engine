TTISFINISHED! 144

fire (Matthew 22:7). St. John underscores this point
again by referring to the Lord as God the Almighty, the
Greek translation of the Hebrew expression Ged of
Hosts, the Lord of the armies of heaven and earth (cf.
1:8). The armies coming to bring about Israel’s de-
struction— regardless of their motivation—are God’s
armies, sent by Him (even through “lying spirits,” if
necessary) to bring about His purposes, for His
glory. The evil frog-demons perform their false won-
ders and works of error because God’s ange! poured
out his Chalice of wrath.

The narrative is suddenly interrupted by Christ’s
statement in verse 15: Behold, I am coming like a thief!
This is the central theme of the Book of Revelation,
summarizing Christ’s warnings to the churches in
the Seven Letters (cf. Revelation 2:5, 16, 25; 3:3, 11).
The coming of the Roman armies will be, in reality,
Christ’s Coming in terrible wrath against His ene-
mies, those who have betrayed Him and slain His
witnesses. The specific wording and imagery seem to
be based on the Letter to the church in Sardis: “7 wall
come like a thief, and you will not know at what hour I
will come upon you” (Revelation 3:3; cf. Matthew
24:42-44; Luke 12:35-40; I Thessalonians 5:1-11).

The same Letter to Sardis also says: “Wake up,
and strengthen the things that remain, which were
about to die; for I have not found your deeds com-
pleted in the sight of My God. . . . But you have a
few people in Sardis who have not soiled their garments;
and they will walk with Me in white; for they are
worthy. He who overcomes shall thus be clothed in
white garments . . .” (Revelation 3:2, 4-5). Similarly,

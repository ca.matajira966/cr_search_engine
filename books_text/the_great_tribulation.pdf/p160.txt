144 THE GREAT TRIBULATION

This is then followed by God's declaration that
He will remove from Israel the idols, the false proph-
ets, and the evil spirits (Zechariah 13), and that He
will bring hostile armies to beseige Jerusalem (Zech-
ariah 14).

“Megiddo” thus was for St. John a symbol of de-
feat and desolation, a “Waterloo” signifying the de-
feat of those who set themselves against God, who
obey false prophets instead of the true.

The Seventh Chalice

Finally, the seventh angel pours out his Chalice
upon the air, in order to produce the lighting and
thunder (v. 18) and hail (v. 21). Again a Voice comes
“from the Temple of heaven, from the Throne,” sig-
nifying God’s control and approval. St. John has
already announced that these seven Chalice-plagues
were to be “the last, because in them the wrath of
God is finished” (Revelation 15:1); with the Seventh
Chalice, therefore, the Voice proclaims: Jt és done!
(cf. John 19:30; Revelation 21:6).

Again St. John records the phenomena associ-
ated with the Day of the Lord and the covenant-
making activity of the Glory-Cloud: flashes of light-
ning, peals of thunder, voices, and “a great earth-
quake” (Revelation 16:18). Seven times in Revelation
St. John mentions an earthquake (6:12; 8:5; 11:13
[twice]; 11:19; 16:18 [twice]), emphasizing its cove-
nantal dimensions. Christ came to bring the definitive
earthquake, the great cosmic earthquake of the New Cove-
nant: “one such as there had not been since the men
came to be upon the Land, so mighty an earth-

PUBLISHER'S EPILOGUE 177

place of God’s blessing, and both hell and heaven are
limited by history. God makes His declaration of
“Jost” to those in hell, just as He declares “saved” to
those in heaven. Hell is no less real than heaven; it is
simply impotent compared to heaven. Death is
equally ultimate to life covenantally. In fact, life and
death are primarily covenantal concepts, not physi-
cal concepts, as we shall see. They exist in relation to
God’s covenant. Life and death must always be de-
fined in terms of God’s five-point covenant structure,
a structure described best in Ray Sutton’s book, That
You May Prosper: Dominion By Covenant (Box 8000,
Tyler, Texas: Institute for Christian Economics,
1987; $14.95):

. The transcendence (yet also presence) of God
. The hierarchy of God’s creation

. The law of God

. The judgment (sanctions) of God

. The inheritance (or disinheritance) of God

om we

Heaven and hell are limited by time and by their
relation to events on earth. The two post-resurrec-
tion worlds will not be limited by time. God's grace
will shine forth perfectly in one place, and His wrath
will shine forth perfectly in the other. There is no
escape from God in history: “Whither shail I go from
thy Spirit? Or whither shall I flee from thy presence?
If I ascend up into heaven, thou art there: if I make
my bed in hell, behold, thou art there” (Ps. 139:7-8).
How much more is God present in eternal judg-
ment, in both the place of unrestrained blessing and

178 THE GREAT TRIBULATION

the place of unrestrained cursing! The presence of
God is eternal; hence, once created, human beings
are creatures of endless future duration. Many peo-
ple will wish they weren’t everlasting. For those in
the lake of fire, endless duration is the opposite of
eternal life: it is the eternal second death.

The Bible speaks here of God’s presence in the
sense of knowing and observing all things, determin-
ing all things. It is not speaking of His presence in
the sense of ethical presence: showing grace (common
grace or saving grace) to people. That kind of pres-
ence will not exist in the lake of fire, The residents of
the lake of fire are separated from God eternally, not
in the sense that men can escape God’s presence, but
in the sense that they cannot pray to God, seek God’s
face, or expect God’s mercy. He is present with them
in some sense as He was present in the burning
bush: as a consuming fire. He is present in some sense
as the worm that never dies. (It is not Satan or a
fallen angel that serves as the worm, for they are
equally impotent, equally under the curse.) He is
present because God is omnipresent: present every-
where. This very presence as the Judge is the ulti-
mate curse of God: no ethical presence with people
as the Savior and source of grace. They spend eter-
nity in the presence of God’s wrath, not God's grace.

The key issue here, as always, is ethics. Life is a
function of covenantal ethics, not duration as such.
So is death. Life is a gift of God’s grace, an un-
mitigated blessing: “He that believeth on the Son
hath everlasting life: and he that believeth not the
Son shall not see life; but the wrath of God abideth

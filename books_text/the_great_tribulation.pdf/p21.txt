THE TERMINAL GENERATION &

point out the Temple buildings to Him. And
He said to them, “Do you not see all these
things? Truly I say to you, not one stone here
shall be left upon another, which will not be
torn down.” And as He was sitting on the
Mount of Olives, the disciples came to Him
privately, saying, “Tell us, when will these
things be? And what will be the sign of Your
coming, and of the end of the age?” (Matthew
24:1-3),

Again, we must take careful note that jesus was
not speaking of something that would happen thousands of
years later, to some future temple. He was prophesying
about “all these things,” saying that “not one stone here
shali be left upon another.” This becomes even
clearer if we consult the parallel passages:

And as He was going out of the Temple,
one of His disciples said to Him, “Teacher,
behold what wonderful stones and what won-
derful buildings!” And Jesus said to him, “Do
you see these great buildings? Not one stone shall
be left upon another which will not be torn
down” (Mark 13:1-2).

And while some were talking about the
Temple, that it was adorned with beautiful
stones and votive gifts, He said, “As for these
things which you are looking at, the days will come
in which there will not be left one stone upon
another which will not be torn down” (Luke
21:5-6).

‘THE TERMINAL GENERATION 17

7. The Abomination of Desolation. “Therefore
when you see the Abomination of Desolation
which was spoken of through Daniel the
prophet, standing in the holy place (let the
reader understand), then let those who are in
Judea flee to the mountains; let him who is on
the housetop not go down to get the things out
that are in his house; and let him who is in the
field not turn back to get his cloak” (vv. 15-18).

The Old Testament text Christ referred to is in
Daniel 9:26-27, which prophesies the coming of ar-
mies to destroy Jerusalem and the Temple: “The
people of the prince who is to come will destroy the
city and the sanctuary. And its end will come with a
flood; even to the end there will be war; desolations
are determined. . . . And on the wing of abom:nations
will come one who makes desolate, even until a com-
plete destruction, one that is decreed, is pourcd out
upon the desolate.” The Hebrew word for abomination
is used throughout the Old Testament to indicate
idols and filthy, idolatrous practices, especially of the
enemies of Israel (e.g., Deuteronomy 29:17; I Kings
1:5, 7; HL Kings 23:13; [1 Chronicles 15:8; Isaiah
66:3; Jeremiah 4:1; 7:30; 13:27; 32:34; Ezekiel 5:11;
7:20; 11:18, 21; 20:7-8, 30). The meaning of both
Daniel and Matthew is made clear by the parallel
reference in Luke. Instead of “abomination of
desolation,” Luke reads:

 

But when you see Jerusalem surrounded by
armys, then recognize that her desolation is at

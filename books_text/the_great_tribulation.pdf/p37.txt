COMING ON THE CLOUDS = 21

As you can see, two important differences come
to light in the correct translation: first, the location
spoken of is Aeaven, not just the sky; second, it is not
the sign which is in heaven, but the Son of Man who is
in heaven. The point is simply that this great judg-
ment upon Israel, the destruction of Jerusalem and
the Temple, will be the sign that Jesus Christ is en-
throned in heaven at the Father's right hand, ruling over the
nations and bringing vengeance upon His enemies. The
divinely ordained cataclysm of a.p. 70 revealed that
Christ had taken the Kingdom from Israel and given
it to the Church; the desolation of the old Temple
was the final sign that God had deserted it and was
now dwelling in a new Temple, the Church. These
were all aspects of the First Advent of Christ, crucial
parts of the work He came to accomplish by His
death, resurrection, and ascension to the throne.
This is why the Bible speaks of the outpouring of the
Holy Spirit upon the Church and the destruction of
Israel as being the same event, for they were intimately
connected theologically. The prophet Joel foretold
both the Day of Pentecost and the destruction of
Jerusalem in one breath:

And it will come about after this

That I will pour out My Spirit on all flesh;

And your sons and daughters will prophesy,

Your old men will dream dreams,

Your young men will see visions.

And even on the male and female servants

I will pour out My Spirit in those days.

And I will display wonders in the heaven and
on the earth:

COMING ON THE CLOUDS 23

they will see the Son of Man coming on the clouds of
heaven with power and great glory.” The word tribes
here has primary reference to the tribes of the land of
Israel; and the “mourning” is probably meant in two
senses. First, they would mourn in sorrow over their
suffering and the loss of their land; second, they
would ultimately mourn in repentance for their sins,
when they are converted from their apostasy (see
Romans il).

But how is it that they would see Christ coming
on the clouds? This is an important symbol of God’s
power and glory, used throughout the Bible. For ex-
ample, think of the “pillar of fire and cloud” through
which God saved the Israelites and destroyed their
enemies in the deliverance from Egypt (see Exodus
13:21-22; 14:19-31; 19:16-19). In fact, all through the
Old Testament God was coming “on clouds,” in sal-
vation of His people and destruction of His enemies:
“He makes the clouds His chariot; He walks upon
the wings of the wind” (Psalm 104:3). When Isaiah
prophesied of God’s judgment on Egypt, he wrote:
“Behold, the Lorn is riding on a swift cloud, and is
about to come to Egypt; the idols of Egypt will trem-
ble at His presence” (Isaiah 19:1). The prophet
Nahum spoke similarly of God’s destruction of
Nineveh: “In whirlwind and storm is His way, and
clouds are the dust beneath His feet” (Nahum 1:3).
God's “coming on the clouds of heaven” is an almost
commonplace Scriptural symbol for His presence,
judgment, and salvation.

More than this, however, is the fact that Jesus is
referring to a specific event connected with the de-

3

THE COMING OF THE ANTICHRIST

According to Jesus’ words in Matthew 24, one of
the increasing characteristics of the age preceding
the overthrow of Israel was to be apostasy within the
Christian Church. This was mentioned earlier, but a
more concentrated study at this point will shed much
light on a number of related issues in the New Testa-
ment— issues which have often been misunderstood.

We generally think of the apostolic period as a
time of tremendously explosive evangelism and
Church growth, a “golden age” when astounding
miracles took place every. day. This common image
is substantially correct, but it is flawed by one glar-
ing omission. We tend to neglect the fact that the
early Church was the scene of the most dramatic out-
break of heresy in world history.

The Great Apostasy
The Church began to be infiltrated by heresy
fairly early in its development. Acts 15 records the
meeting of the first Church Council, which was con-

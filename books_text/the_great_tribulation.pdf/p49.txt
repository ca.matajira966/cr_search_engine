‘THE COMING OF THE ANTICHRIST 33

Whoever denies the Son does not have the
Father; the one who confesses the Son has the
Father also. . . .

These things I have written to you concern-
ing those who are trying to deceive you (I John
2:18-19, 22-23, 26).

Beloved, do not believe every spirit, but test
the spirits to see whether they are from God;
because many false prophets have gone out into
the world.

By this you know the Spirit of God: every
spirit that confesses that Jesus Christ has come
in the flesh is from God;

and every spirit that docs not confess that
Jesus Christ has come in the flesh is not from
God; and this is the spirit of the Antichrist, of
which you have heard that it is coming, and
now it is already in the world.

You are from God, little children, and have
overcome them; because greater is He who is in
you than he who is in the world.

They are from the world; therefore they
speak as from the world, and the world listens
to them.

We are from God; he who knows God lis-
tens to us; he who is not from God does not lis-
ten to us. By this we know the Spirit of truth
and the spirit of error (I John 4:1-6).

For many deceivers bave gone out into the
world, those who do not acknowledge Jesus
Christ as coming in the flesh, This is the de-
ceiver and the Antichrist.

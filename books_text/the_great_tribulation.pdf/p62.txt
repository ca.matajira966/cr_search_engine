46 THE GREAT TRIBULATION

and that Israel had rejected Him to its own damna-
tion (Matthew 21:42-44; I Peter 2:6-8). It was the sign
of judgment and reprobation, the signal that the apos-
tates of Jerusalem were about to “stumble backward,
be broken, snared, and taken captive.” The Last
Days of Israel had come: the old age was at an end,
and Jerusalem would be swept away in a new flood,
to make way for God’s New Creation. As St. Paul
said, the gift of tongues was “for a sign, not to those
who believe, but to unbelievers” (I Corinthians 14:22)
—a sign to the unbelieving Jews of their approaching doom.

The early Church looked forward to the coming
of the new age. They knew that, with the visible end
of the Old Covenant system, the Church would be
revealed as the new, true Temple; and the work
Christ came to perform would be accomplished.
‘This was an important aspect of redemption, and
the first-generation Christians looked forward to this
event in their own lifetime. During this period of wait-
ing and severe trial, the Apostle Peter assured them
that they were “protected by the power of God
through faith for a salvation ready to be revealed in
the last time” (I Peter 1:5). They were on the very
threshold of the new world.

Expecting the End
The Apostles and first-generation Christians
knew they were living in the last days of the Old
Covenant age. They looked forward anxiously to its
consummation and the full ushering in of the new
era. As the age progressed and the “signs of the end”
increased and intensified, the Church could see that

THE FOUR HORSEMEN 67

21:5-6, 20-24, 32), few seem able to make the obvi-
ous connection: the Big Apocalypse (the Book of Rev-
elation) is a prophecy against Israel as well!

The Biblical Background of the Horsemen

The central Old Testament passage behind the
imagery of the “Four Horsemen of the Apocalypse” is
Zechariah 6:1-7, which pictures the Four Winds as
God’s chariots driven by His agents, who go back
and forth patrolling the earth. Following and imitat-
ing the action of the Spirit (cf. Revelation 5:6), they
are God’s means of controlling history (cf. Revela-
tion 7:1, where the Four Winds are identified with,
and controlled by, angels; cf. also Psalm 18:10, where
the “wings of the wind” are connected with “cherubs.”)
Biblical symbolism views the earth (and especially
the Land of Israel) as God’s four-cornered altar, and
thus often represents wide-sweeping, national judg-
ments in a fourfold manner. The Horsemen, there-
fore, show us God’s means of controlling and bring-
ing judgment upon the disobedient nation of Israel.
In particular, they symbolically represent the great
devastations that Jesus predicted would come upon
Israel in the last days of the Old Covenant era, lead-
ing up to the destruction of Jerusalem and the Tem-
ple (Matthew 24).

Just as important as Zechariah in the back-
ground of this passage is the Prayer of Habakkuk
(Habakkuk 3), the traditional synagogue reading for
the second day of Pentecost, in which the prophet
relates a vision of God coming in judgment, shining
like the sun, flashing with lightning (Habakkuk

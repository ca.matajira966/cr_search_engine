The Origin of Higher Criticism 19

similar loss of confidence also appeared in the mid-
1980’s behind the Iron and Bamboo Curtains. The
implicit and inescapable dualism of all post-Kantian
thought — fact vs. meaning, science vs. ethics, phe-
nomenal vs. noumenal'’ —became a growing intellec-
tual problem after the 1880’s, and it could not, like
Humpty Dumpty, be put back together again.'® The
social and political effects of this accelerating intellec-
tual disorientation became clear to most social ob-
servers after 1963. Mcanwhile, the appearance of Van
Til’s presuppositional apologetics in the mid-1940’s,!9
the revival of biblical creationism after 1960,2° and
the preliminary recovery of the Puritan vision of the
earthly victory of God’s Kingdom have combined to
produce a new intellectual perspective: Christian recon-
struction.

Basic to this reversal has been the recovery of
confidence by Christians in the reliability of the whole
Bible. They have been presented with a growing body

(Ft. Worth, Tex

17, Richard Kroner, Kent's Weltanschawung (Chicago: University
of Chicago Press, [1914] 1956).

18. H. Stuart Hughes, Consciousness and Society: The Reorientation of
European Social Thought, 1890-1930 (New Vork: Knopf, 1958).

19. Gornelius Van Til, The New Modernism: An Appraisal of the The-
ology of Barth and Brunner (Philadelphia: Presbyterian & Reformed,
1946).

. Henry M. Morris and John C. Whitcomb, Jr., The Genesis
Flood: The Biblical Record and its Scientific Implications (Philadelphia:
Presbyterian & Reformed, 1963).

 

: Dominion Press, 1986), Introduction.

 

 

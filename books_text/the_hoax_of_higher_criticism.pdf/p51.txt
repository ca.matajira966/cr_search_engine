The Ethics of Higher Criticism 43

consciously spend their myopia-inducing lives search-
ing for internal evidence that denies the unity of that
historical document. I agree with Walter Kaiser’s ob-
servation of the crucial link between higher criticism
and men’s loss of faith in the unity of the biblical
message (including its ethical requirements): “For
many it is too much to assume that there is consis-
tency within one book or even a series of books al-
leged to have been written by the same author, for
many contend that various forms of literary criticism
have suggested composite documents often tradition-
ally posing under one single author. This argument,
more than any other argument in the last two hun-
dred years, has been responsible for cutting the main
nerve of the case for the unity and authority of the
biblical message.”*

 

Higher Criticism and Evolution

Higher criticism is based on an evolutionary model
of human morality and human history. It assumes,
and then seeks to prove, that the texts of the Bible,
and especially the Old Testament, were self-con-
sciously altered by later scribes and “redactors” in
order to make the Bible’s message conform to the lat-
est ethical and economic principles of the day. It
helped to create the early nineteenth century’s intel-
lectual climate of opinion that was so favorable to

4. Kaiser, Tiward Old Testament Ethics, p. 26.

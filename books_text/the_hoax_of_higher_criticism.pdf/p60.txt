52 ‘THE HOAX OF HIGHER CRITICISM

skilled scholars productively busy for scveral gencra-
tions.

Meanwhile, let the higher c
own footnotes, the way that Arias died by falling head-
first into a privy.’ Let the dead bury the dead, prefer-
ably face down in a scholarly journal.

 

tics drown in their

 

3. R. J. Rushdoony, Moundations of Social Order: Studies in the Creeds
and Councils of the Early Church (Fairfax, Virginia: Thoburn Press,
[1969] 1978), p. 17

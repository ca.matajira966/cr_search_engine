TABLE OF CONTENTS

 

Introduction. ©... ee ee 1
1. The Origin of Higher Criticism... .. . 9
2. The Techniques of Higher Criticism. . . 21
3. The Ethics of Higher Criticism, . . . . . 37
Conclusion... ee 49
Bibliography... 2.2.0. ee 53
Scripture Index. . 2.0... 57
Index. 0. ee 59

About the Author, 2 2.2.2... ee 63

vii

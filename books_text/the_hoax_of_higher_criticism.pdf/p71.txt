ABOUT THE AUTHOR

Gary North received his Ph.D. in history from
the University of California, Riverside, in 1972. He is
the author of approximately 30 books, and served as
the editor of the Journal of Christian Reconstruction for its
first eight years. He is the author of a multi-volume
economic commentary on the Bible, The Dominion Cove-
nant, His reviews and essays have appeared in about
three dozen newspapers and periodicals, including the
Wall Street Journal, The Freeman, Modern Age, National
Review, Westminster Theological Journal, Banner of Truth,
and joumal of Political Economy. He is the writer of two
bi-monthly Christian newsletters, published by the
Institute for Christian Economics: Biblical Economics
Today and Christian Reconstruction. He is the publisher
of several financial newsletters, including Remnant
Review, Low Profile, Clip Notes, and Investment Coin
Review, as well as the monthly bulletin, Washington
Report. Three of his essays appear as appendixes in
R. J. Rushdoony’s Lnstitutes of Biblical Law (1973). He
lives with his wife and four children in east Texas.

63

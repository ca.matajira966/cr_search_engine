“You Have Heard It Said” 87

Or; “You have heard it said that Adam had intercourse with
every beast of the field before cohabiting with Eve,* but I tell you
that bestiality is a great sin before God.”

Or; “You have heard it said that a homosexual who seduces a
boy under the age of nine need have no guilt, while others have
argucd that age three is the minimum,* but E say unto you that
anyone who does this should be executed, as required by biblical
law.”

Read the footnotes! This is only the beginning, but it should
be sufficient. You now recognize that the ‘Talmud is not a conven-
tional commentary on the Old Testament, although with certain
key New Testament concepts missing. On the contrary, the Tal-
mud’s contents are only peripherally related to the Old ‘Testament.
The Talmud is a giant cxercise in finding ways to escape the Old
Testament texts. The Pharisees were in rebellion against God’s
law, all in the name of God’s law. This was Jesus’ assertion from
the beginning:

 

Woe unto you, scribes and Pharisces, hypocrites! for ye com-

 

 

asked Jesus: ‘Who is esteemed in that world?” Jesus said: ‘Israel.’ ‘Shall one join
them?’ Jesus said to him: ‘Further their well-being; do nothing ta their detriment;
whoever touches them touches even the apple of His eye.’? jauish Encylopedia, 12
vols, (New York: Funk & Wagnalls, 1904), VII, p. 172.

3. *R. [Rabbij Elcazar further stated: What ia meant by the Scriptural text, This
is now bone of my bones, and flesh of my flesh? Vhis teaches that Adam had intercourse
wilh every beast and animal but found no satisfaction until he cohabited with Eve.”
Babylonian ‘Valmud, Yebamoth 63a, Eleazar was an important scholar of the oral law
in the years immediately following the fall of Jerusalem in AD. 70.

 

 

4, “Rab said: Pederasty with a child below nine years of age is not deemed as
pederasty with a child above that, Samuel said: Pederasty with a child below three
years is not treated as with a child above that.” Babylonian Talmud, Sanhedrin 54.
The modern commentator's note explains: “Rab makes nine years the minimum; but
if one committed sodomy with a child of lesser age, no guilt is incurred. Samuel makes
three the minimum,” Rab is the nickname of Rabbi Abba Arika (1752-247 A.D.), the
founder of the Jewish academy in the Persian city of Sura [Sora], one of the three
great Jewish academies in Persia. Samuel was Mar-Samuel (180-257 A.D,), Rab's
contemporary and fellow teacher at Sura, a master of Jewish civil law, See Heinrich
Gractz, History of the Jews, 6 vols. (Philadelphia: Jewish Publication Society of Amer-
ica, [1893] 1945), IT, pp. 512-22.

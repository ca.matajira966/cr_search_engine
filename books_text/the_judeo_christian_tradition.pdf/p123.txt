Rabbi Moshe Ben Maimon, “Maimonides” 109

The words mishneh Torah mean “repetition of the Torah” or
law. It is the phrase by which Jews have traditionally identified
the Book of Deuteronomy. Deuteronomy restated the Mosaic law
for the sake of the childrcn of the generation that had dicd in the
wilderness. Their days of wandering were about to end; they would
now face the problems of running God’s earthly commonwealth.
‘The rabbis believed that these laws would serve as the starting
point of all discussion of the legal rules for ordering the lives of all
Jews throughout history. Lerner writes: “Maimonides’ Code has a
similar character; in it he restates the laws of the Torah and of the
Talmud without limiting himself to those laws that are applicable
to life in the Diaspora. Maimonides’ Mishneh Torah, like Moses’, is
concerned with the practical needs of an actual state, that is, the
Jewish state prior to the Diaspora and after the coming of the
Messiah”? The influence of this work on medieval and subsequent
Judaism was very great, beginning almost from the day he wrote
at,

Jewish legal scholar George Horowitz writes: “The restate-
ment of Maimonides, the Mishneh Torah is still the most orderly
and logical classification of the Halakah [Jewish law ~G.N_] in
existence.”!° He is not alone in his assessment of Maimonides’
Code. Maimonides specialist Isadore Twersky says that “The Mish-
neh Torah, which was to change the entire landscape of Rabbinic
literature, also pushed back the frontiers of Maimonides’ sphere
of influence and made his fame global as well as imperishable. It
transformed him, in the course of a few decades, from the ‘light of
the Fast’ ta ‘the light of the [entire] exile.” He almost literally
became a major Jewish luminary. . . . Tn one broad generaliza-
tion, we may say that the Miskneh Torah became a prism through
which reflection and analysis of virtually all subsequent Talmud
study had to pass. There is hardly a book in the broad ficld of

9. Ralph Lerner, “Moses Maimonides,” in Ico Strauss and Joseph Cropsey
(eds.), History of Political Philosophy (Chicago: Rand McNally, 1963), p. 193.

10. George Horowitz, The Spirit of Jewish Law (New York: Central Book Co.,
[1953] 1963), p. 16.

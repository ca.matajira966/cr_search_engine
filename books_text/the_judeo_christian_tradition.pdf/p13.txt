Preface xiii
tally separate people are very important, not just for the sake
of cach individual soul, but for the enormous external blessings
which God has promised when the Jews, as a people, accept
Christianity as their New Covenant faith. ‘The Jews, as a people,
are uniquely tied into God’s plans for blessing His church in the
future,

2. Judaism and Christianity are implacably opposed religions.
There can be no successful meshing of the two systems. Neither
side wants such a meshing, and all “halfway house” attempts
to achieve this will inevitably fail. The Jews must come to God
on Jesus’ terms, and on no other — just as all other people must.

3. There are good reasons on both sides why Orthodox Jews and
orthodox Christians can and should co-operate socially and
politically, for the sake of the peace, in battling the hydra-headed
monster called secular humanism. Secular humanism is a com-
mon enemy of both religions,

4, There are good geopolitical reasons for a continuation of close
ties between the United States and the State of Israel. These
reasons, however, are not necessarily eschatological in nature.
‘The eschatological significance of the State of Israel depends
on what God intends for that political unit, as distinguished
from ethnic Jews as a people. No man knows what God intends
in this regard, although many Christians say that they know. I
personally pray that God will use that nation positively in the
expansion of God’s carthly kingdom in my lifetime.

There are no doubt many readers who will say to themselves:
“Well, I certainly don’t need to read éhis! I know when something
cannot possibly be true.” Quite frankly, I am not after the support
or commitment of such readers. I am after those who are mature
in their faith, and who are psychologically ready to examine the
facts instead of either leaping to conclusions or holding on grimly
to past conclusions that they have never really cxamincd carefully.

This book has the facts. The question is: Do readers have the
spiritual maturity?

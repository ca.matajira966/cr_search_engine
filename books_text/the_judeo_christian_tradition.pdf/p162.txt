148 The Judeo-Christian Tradition

lished institutional and philosophical foundations that have stead-
ily undermined ‘Yalmudic religion and culture. The more ethically
evolutionary any particular worldview has been, the more rapidly
it has succumbed to this powerful pair of social forces. Judaism
was especially vulnerable.

The factor that most threatened Orthodox Judaism was indus-
trial society’s growing toleration. In the mid-nineteenth century,
when Jews in Western Europe and the United States began to
enter the new industrial capitalist world, they found that the older
discriminatory legal barriers had been progressively weakened by
the new forces of economic competition. An individual’s economic
productivity in an open (“impersonal”)® competitive market is
judged apart from considerations of his religious affiliation. To the
extent that non-market forms of racial or religious discrimination
persist, those who discriminate against economically efficient em-

 

ployees or suppliers (or — much more rare — buyers) must pay a
price for their actions: reduced income because of reduced effi-
ciency.? The free market penalizes cconomically all those who
discriminate on any basis except price and quality of output. Price
competition has always been fundamental to the spread of free
market capitalism,!° and Jews became masters of competitive
pricing." Jews began to move out of the ghetto. The ghetto’s walls,
both literal and figurative, came tumbling down.

Jewish legal scholar Menachem Elon has argued that it was

 

8. On the proper and improper use of the term “impersonal” to describe market
economies, see Gary North, The Dominion Covenant: Genesis (nd ed.; Tyler, Texas:
Institute for Christian Economics, 1987), pp. 9-11.

9. “The least prejudiced sellers will come to dominate the market in much the
sare way as people who are least afraid of heights come to dominate occupations
that require working at heights: They demand a smaller premium.” Richard A.
Posner, Economie Analysis of Law (Boston: Little, Brown, 1986), p. 616.

10. Max Weber, General Economic History, trans. Frank H. Knight (New York:
Collier, [1920] 1966), p. 230.

11. ‘The common phrase, “he Jewed me down,” points to this phenomenon of the
Jew as a price-cutter. If one were to say, “he Jewed me up,” it would make no sense.
‘The Jew as the price-cutting haggler is universally recognizable, but not the Jew as
the price-gouger. He is resented by people in their capacity as producers and retail

 

 

The Isolation or Erosion of Orthodox Judaism 149

the Jews’ system of separate civil courts that was crucial to the
maintenance of the autonomy of Jews as a people. When judicial
emancipation began in eighteenth-century Western Europe, this
autonomous character of Judaism began to erode. Jews were
increasingly entitled to civil justice in secular civil courts, and they
took advantage of this revolutionary development. Jewish com-
mercial law and other areas of “secular world” law began to
atrophy. This secularism began to undermine the foundations of
Orthodox Judaism!? — a term which itself was the product of the
process of changc.'* Rabbi Samson Raphacl Hirsch asked the key
question which most Jews have refused to face: “What would you
have achieved if you became free Jews, and you ceased to be
Jews?” + Nevertheless, his own efforts to integrate the techniques
and findings of modern science and philosophy with Judaism
eventually led to a reduced resistance of Orthodox Judaism to
secularism, as surely as Aquinas’ analogous efforts had done for
Christianity seven centuries earlier.

The Faustian Bargain
From the New Testament period to the present, the lure of
pagan philosophy has proven irresistible to Jews, as it has also for
Christians. Out of Greek philosophy came Hellenism, and Hellen-
ism’s influence on early rabbinic Judaism was very great.!5 Never-

 

sellers, not as consumers, Gentiles are always looking for the clusive “Jewish brother-
indaw deal.”

It is not random that the four ethnic groups that are thought of as price-cutters
have had decidedly biblical backgrounds: the Dutch (“Dutch treat” dates are those
in which the girt pays), the Scots, the Armenians, and the Jews.

12, Menachem Elon, “Introduction,” in Elon (ed.), The Principles of Jewish Law
(Jerusalem: Keter, 1975), col. 35.

13. Tt was Rabbi Samson R. Hirsch who accepted the term “Orthodox” which
had been used as an epithet by secular Jews in the mid-nineteenth century. I.
Grunfeld, Samson Raphael Hirsch — The Man and His Mission,” in fudaism Etemat:
Selected Essays from the Writings of Samson Raphael Hirsch (London: Soncino Press, 1956),
p. xlvii,

14. Ibid., p. xxxix.

15. Martin Hengel, Judaism and Hellenism: Studies in their Encounter in Palestine during
the Early Hellenic Period, 2 vols. (Philadelphia: Fortress Press, 1974).

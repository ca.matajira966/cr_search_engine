The Isolation or Erosion of Orthodox Judaism 151

tics had not achieved in the 1930's, Prussia’s earlier export of the
academic state certification system did achieve: the suppression of
traditional religion through the enthusiastic co-operation of the
suppressed. Secular education is the humanist world’s hoped-for
“fal solution” for both orthodox Christianity and Orthodox Ju-
daism.

In the twentieth century, the lide has rapidly flowed against
Talmudic Judaism; first the Nazis and then secularism uprooted
Orthodox Judaism. Higher criticism of the Bible has produced the
same bitter fruit of skepticism and libcralism in Jewish circles that
it has produced in Christian circles.!® There was not only bitter
fruit but also forbidden fruit to be eaten. By the millions, they
have feasted on this forbidden fruit. Schechter was correct: biblical
higher criticism was in fact the “higher anti-Scmitism,” for it
obliterated the official foundation of the Jewish experience.” But
this was a case of the hermeneutical chickens coming home to
roost, for Judaism had long undermined this original foundation
through its cver-evolving traditionalism.

Traditional Judaism’s ethical rules began to change, and there-

 

19. ‘The Jewish scholar most responsible for the introduction of higher criticism
into Jewish curricula was the extraordinary linguist, Julian Morgenstern, who also
served us president of Hebrew Union College in Cincinnati, Ohiv, after 1921. Born
in 1881, he was still writing scholarly essays in the mid-1960’s in the Hebrew Union
College Ansual, (“The Hasidim — Who Were They?” HUCA, XXXVIIT, 1967.) Indica-
Give of his extent of his life's work was his four-part study, “The Book of the
Covenant.” Part T appeared in the 1928 issue; Part IT appeared in 1930; Part II] in
1931-32; and Part IV in 1962, He was elected president of the American Oriental
Sociely in 1928-29 and president of the Society of Biblical Literature in 1941.
“Morgenstern assumed a position of pre-eminence as a philosopher and theoretician
of Reform Judaism. . . . Modem developments, he showed convincingly, are only
the latest manifestations of the adjustments that have taken place over and over
whenever Judaism has come into contact with a superior culture,” Morris Lieber-
man, “Julian Morgenstera — Scholae, Teacher and Leader,” Hebrew Union College
Annual, XXXIE (1962), p. 6. Morgenstern was a dedicated humanist and interna-
tionalist. C£ Morgenstern, “Nationalism, Universalism, and World Religion,” in
Charles Frederick Walker (ed.), World Fellmoship, Addresses and Messages by Leading
Statesmen of AU Faiths, Races and Countries (New York: Liveright, 1935). This was his
address to the second Parliament of Religions, held in Chicago in 1933

20. Cohen, Myth of Judeo-Christian Tradition, p. xviii.

 

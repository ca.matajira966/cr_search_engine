The Isolation or Erosion of Orthodox Judaism 155

judicial authority to the extent that they adhere to the terms of
their covenant. They can have no legitimate hope in political
salvation, which is theologically healthy, but they also can have
no legitimate hope in cultural dominion. At best, they can pray for
and work for a holding action: social peace until the Messiah
comes, This circumcises their temporal horizon. They can hope
only in a future discontinuity. There is no relationship between
political and social action now and the triumph of their covenant
in history. For this reason, Orthodox Jews in the United States
were initially divided about the importance and meaning of the
state of Israel in 1948, and even today they look at that nation as
a political phenomenon rathcr than a kingdom phenomenon. They
have thereby adopted the mythology of humanism: the myth of
kingdom-less politics.

The essence of the coming of the Messiah is kingdom disconti-
nuity. This reduces the Jew’s incentive to transform this world in
preparation for the Messiah. What the faithful Jew does in time
and on earth will not hasten the appearance of the Messiah. Thus,
the earthly manifestation of the kingdom of God is far in the future,
or, if there are signs of it in the present, it is temporally truncated
by the expectation of the great discontinuity. Expectations are high
regarding the Messiah; they are inescapably low regarding the
comparative accomplishments of Judaism before thc Messiah comes.

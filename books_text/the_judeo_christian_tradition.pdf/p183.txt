Conclusion 169

a broad market.

If the words of Paul are to be honored, Christians as self-
conscious Christians must begin to offer an alternative to the
disintegrating culture of our day. If we achieve nothing worth
becoming jealous about, we will fail in the task of evangelizing the
Jews. And the Church has been given this special task by Paul, for
the Jews are still special in God’s sight.

The State of Israel

Orthodox Jews have long becn ambivalent about this political
experiment. This was especially true prior to 1948. Norden writes:
“Some of the anti-Zionist rabbis split hairs — they intimated that
although the new state could never do the messiah’s job, it might,
if it became truly Jewish, prepare the way for him.” But then a
non-Kosher fly appeared in the vintment:

Satan appeared in the person of David Ben-Gurion, Zionist
hero and Israel’s first Prime Minister. Here was a Jew who put all
his considerable energy into the forging of a New Jew, one without
skullcap or carlocks, a Jew who would use Hebrew in the street,
who would eat whatever he liked, who would know much of the
Bible by heart but none of the Talmud, who would read Plato in
the original and do yoga, who would be a farmer, a cop, a scientist,
a soldier in his own nation-state. BG was a hard-nosed kind of
visionary. Unlike Martin Buber up at the Hebrew University, he
was blessed with a biography and an idcology preempting senti-
mentality about the black coats. He knew they were not pictur-
esque and heartwarming."

These are the words of a decidedly non-Orthodox Jew, but
nonetheless an accurate summary of the choice faced by anti-
Zionist Jews in 1948. The war launched against Israel by the
Arabs is what silenced the Orthodox critics of the state of Israel,
as is depicted memorably in the movie, The Chosen. Up until that
time, Rebbe Saunders’ outrage against “Ben-Gurion and his hench-

13. Edward Norden, “Behind ‘Who Is a Jew’: A Lecter from Jerusalem,” Commen-
tary (April 1989), p. 22.

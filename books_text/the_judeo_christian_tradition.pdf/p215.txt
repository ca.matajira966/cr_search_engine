Subject Index 201

eradication of, 8

evolutionary ethics, 133, 140-43,
145-46, 151-52

exclusivism, 153
French Revolution, 152-53
ghetto, 148
Hellenism, 149-50
humanism, 152
ignorance about, 62-67
law, 33
little theology, 146
non-Jewish, 173
occultism, 63
Orthodox (see Orthodox Judaism)
Pale of Settlement, 21
rabbinic, 14]
Reform, 152
relativism, 147
secularism, 149-52
separate courts, 149
Sephardim, 62
star of David, 65-67
textbooks ignore, 64
toleration, 162
Torah defined, 80, 81
two laws, 91-92
university, 150

Judeo-Christian tradition
evidence?, 33
fabrication, 55-56
future of, 162-67
humanism &, 28
law &, 52
myth of 21-24
nationalism, 24
Old Testament law, 164-67
two questions, 51

Kabbalah, 26, 29-30, 53, 63
Karaites, 80, 99, 125
karma, 17

Kethubah, 100

kidnapping, 138-39
kingdom, xi, 9, 33, 155
kings, 41, 42

Kirzner, Israel, 168
Kline, Meredith, 2
Koran, 52, 80

land, 140-42

law
abandoning, 16+
abandonment, 41
anti-dualism, 90
breaking, xiv
changing, 115-16
condemnation by, 174
equality before, 91
heathen student, 59
ignored, 54
inequality before, 91
Judeo-Christian tradition, 52
keeping, xi
letter & spirit, 91, 133
Maimonides, 116-20
oral, 76, 80, 89, 99, 103
religious, 27
sanctions, 91, 141
society's god &, 48
source of, 48
two kinds (Judaism)

legal order, 57

legalism, 115

Lehrman, 8. M., 88-89

Lerner, Ralph, 109

Levine, Aaron, 168

tex tationis, 91

liberalism, 153, 165

liberals, 50

Liberty, 470

Lindsey, Hal, 177

lost property, 117-19, 126
Luther, Martin, 21, 26, 129

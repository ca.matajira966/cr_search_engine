14 The Judeo-Christian Tradition
pervert the words of the rightcous (Deuteronomy 16:19).

Wherefore now let the fear of the Lorn be upon you; take heed
and do it; for there is no iniquity with the |orp our God, nor
respect of persons, nor taking of gifts (II Chronicles 19:7).

These things also belong to the wise. It is not good to have
respect of persons in judgment (Proverbs 24:23).

For there is no respect of persons with God (Romans 2:11),

But he that doeth wrong shall receive for the wrong which he
hath done: and there is no respect of persons (Colossians 3:25).

 

Divorce by Execution

Biblical divorce is achieved only through execution. ‘his some-
times is to be accomplished through actual physical execution, and
sometimes it is limited to legal-covenantal death. But the death of
the sinful spouse is the basis of the divorce. The marriage vow,
“Until death do us part,” is legally binding.'*

The victimized husband in Israel could lawfully insist on the
death penalty against his adulterous wife. ‘Whe civil magistrate was
required by God to carry out the sentence. But if the adulterous
wife had to die, so did her adulterous consort. God’s law does not
respect persons. Conversely, if the victimized husband decided to
show mercy to his wife, he also had to show the same degree of
mercy to her consort or consorts. God’s law does not respect
persons.

From the beginning of Israel’s covenantal existence as a sepa-
rate people in Abraham, God had promised to bring the gospel of
salvation to the gentiles. Speaking of His seed, Jesus Christ, God
had promised to Abraham “That the blessing of Abraham might
come on the Gentiles through Jesus Christ; that we might receive
the promise of the Spirit through faith. Brethren, I speak after the
manner of men; Though it be but a man’s covenant, yet if it be
confirmed, no man disannulleth, or addeth thereto. Now to Abra-
ham and his sced were the promises made. He saith not, And to

13, Sutton, Second Chance, chaps. 2, 4.

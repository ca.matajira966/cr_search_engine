42 The Judeo-Christian Tradition

Because committed Christians and committed Jews have be-
lieved this lie, it has become easy for them to accept the claims of
the humanists that “the people” alone are sovereign. We are the
kings of our own political order. Why not come together in a civil
covenant and exercise legitimate rule as equals? We thereby sub-
mit only to ourselves. But when we fail to acknowledge the sover-
eignty of the God of the Bible as the basis of all political authority,
our cry is in principle still the same: “We have no king but
Caesar.”

‘The sovereignty of man is the premise of the American civil
religion, as well as the religion of democracy in Europe. This is the
legal foundation Americans refer to as “the Judeo-Christian tradi-
tion.” But there is a problem with this view of civil religion: the
covenantal nature of the civil order is hidden from the eyes of the
faithful. The political order rests officially on the premise of the
political sovereignty of man —not a subordinate political sover-
eignty delegated to man by an absolutely sovereign God, but a

primary sovereignty delegated by man to his political representa-
10

 

tives. We pretend that we anoint rulers by our own authority,
But we nevertheless discover that we have great difficulty in
displacing them. We “sovereigns” have once again become subor-
dinate. We serve our “public servants,” who are never satisfied
with our performance,

We have believed the humanists’ lie. There is im fact no escape
from questions of kingship and covenant. There is no escape from
civil covenants, no matter what we call them. Eighteenth-century
social contract theory succeeded for a while in deflecting our
perception of the covenantal foundation of the political process,
and Marxism’s nineteenth-century economic reductionism did not
improve men’s covenantal awareness. But as we approach the
third millennium after Christ, the sands in Enlightenment human-
ism’s hour glass are running out. It matters little whether it is the
right-wing Enlightenment (Adam Smith, Thomas Jefferson, and

   

10. Edmund §, Morgan, Inventing the People: The Rise of Popular Sovereignty in England
and America (New York: Norton, 1988}.

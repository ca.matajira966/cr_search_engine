50 The Judeo-Christian Tradition

God in the spirit, and rejoice in Christ Jesus, and have no confi-
dence in the flesh” (Phil. 3:3), Only theological liberals on both
sides of the debate can sensibly play down these differences, since
liberals do not accept the truth of either religion’s set of hermeneu-
tical principles.

Orthodoxy and Liberalism

This book deals with Orthodox Judaism and its relation with
orthodox Christianity, Orthodox Christianity is no longer the
dominant stream of Christianity in the West, just as Orthodox
Judaism is no longer the dominant stream of Judaism outside of
the state of Israel, and which is in sharp political conflict with
secular Judaism inside that nation. Always in the background of
the life of the orthodox Christian and the Orthodox Jew are the
liberals “within the camp.” The Orthodox Christian does not
believe that liberal, mainstream Christianity is really Christianity,
just as the Orthodox Jew does not believe that mainstream Juda-
ism is really Judaism.’ Van Til is correct in his assessment of the
theological unity of the liberal Jew and the liberal Christian:

When Jesus says that all power is given to him by the Father
in view of his death and resurrection, and says that he will van-
quish the last cnemy which is death, the modern Jew and the
modern Protestant consider this mythology. The modern Jow will
gladly join the modern Protestant in speaking of Christ as a Mes-
siah if only the messianic idea be demythologized by means of the
self-sufficient ethical consciousness, The modern Protestant theolo-
gian is ready and eager to ablige the modern Jew.

   

The implicit theological unity that modernism creates between

4. There is a problem here for Bible-allieming Christians. ‘They normally do
accepl as valid the baptisms of converts out of mainstream churches. ‘They would not
accept Mormon baptism as valid. So, to some degree, they do accept mainstream
churches as still Christian. For the Orthodox Jew, the determination of who is a Jew
is established by examining the training of the rabbi who circumcised him or
circumcised her father or husband.

5, Cornelius Van ‘Vil, Christ and the Jews (Philadelphia: Presbyterian & Reformed,
1968}, p. 97.

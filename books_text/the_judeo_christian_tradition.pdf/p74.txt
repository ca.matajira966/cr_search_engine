60 The Judeo-Christian Tradition

Any person or group possessing this God-delegated right can
claim to speak for God in history. Who, then, is authorized by God
to speak in His name, Christians or Jews? Possessing legal access
to God’s revealed law is a mark of authority in the biblical civil
hierarchy. He who does not possess this lawful authority cannot
claim access to the sword in the final manifestation of God's
kingdom in history. It is clear what the Jews believe about the
hicrarchy of this world. Only they possess lawful access to the
‘Yorah. How do we know this? The Talmud tells us so. But gentiles
have no legal right to read the Talmud, according to the Talmud
(Sanhedrin 59a). Vhey must take the Jews’ word for it. More to the
point, they must take the word of Orthodox Jews, for it is they
alone who stil! maintain that the whole Talmud is an inspired
book.

T am not making fun of the Jews, On the contrary, T am
presenting their implicit and even explicit view of the question of
spokesmanship and representation. Any group which claims to
represent God or the forces of history must adopt some variant of
this argument. The Jews have just made it more obvious. They
tell us that we are all deserving of death whenever we look into
God’s law, authorized access to which is a mark of covenantal
supremacy.

Social Peace

The typical non-Jew would imagine that Jews throughout
history would have rejoiced whenever gentiles read the Old Testa-
ment in search of God’s permanent moral and civil standards of
rightcousness. After all, this would tend to bridge the cultural and
judicial gap between Jews and non-Jews. This, however, was
precisely the problem in the minds of the rabbis for at least 1,700
years, The rabbis did not want this gap bridged; at most, they
wanted external peace and quiet for Jews, meaning they wanted
social order in the midst of gentile culture {sec below, pp. 125-28).
Sufficient social order within the gentile world is supposedly achieved
through their adherence to the seven commandments specifically

A Separate People in Our Midst 63

can culture, But as to how these Jewish groups overlap,!! or which
group dominates Judaism either in the U.S. or in the state of Israel
today,!* the average Christian has no idea. Few Christians have
heard that there is a third branch, Oriental or Yemenite Judaism
{North African}, members of which have long complained that
they are discriminated against politically in the state of Israel.

Christians are unaware that the medieval Jewish body of
literature known as the Kabbalah (“tradition”) is not only mysti-
cal but closely tied to numerology and occultism.'* They do not
know that the mystical-magical tradition of the Kabbalah had its
roots in the Talmud.’* They have never read anything about the
history of Zionism, either pro! or con,!®

 

of America, 1972), For the Ashkenazi in America, see Eric E. Hirshler (ed.), fas from
Germany in the United States (New York: Farrar, Strauss & Cudahy, 1955); Irving
Howe, World of Our Fathers (New York: Simon & Schuster, [1976] 1983); Irving
Howe and Kenneth Libo, How We Lived: A Documentary History of Immigrant Jews in
America, 1880-1930 (New York: Richard Marek, 1979); Stephen Birmingham, *Our
Crowd”; The Great Jewish Families of New York (New York: Harper & Row, 1967).

11. Thomas Sowell, Ethnic America: A History (New York: Basic Books, 1981), ch.
4: “The Jews.”

12. L refer to che “state of Israel” rather than “Israel” out of respect for the
terminology of Orthodox Jews, who sharply distinguish the two.

13, “Kabbalah,” in Lewis Spence (ed.), An Engwlopedia of Occultism (New Hyde
Park, New York: University Books, [1920] 1960), An example of popular (though
underground) magical literature bused on the Kabbalah, which has been reprinted
generation after generation, is The Sixth and Seventh Books of Moses. Sec also Arthur
Edward Waite, The Holy Kabbalah: A Study of the Secret Tradition of Israel (New Hyde
Park, New York: University Books, 1960 reprint); Denis Saurat, Literature and Occult
Tradition, trans. Dorothy Bolton (Port Washington, New York: Kennikat, [1980]
1966), Pu. TIT, ch. 2. The pioneering modern Jewish studies of the Kabbalah are by
Gershom G. Scholem: Major Trends in Jewish Mysticism (3rd ed; New York: Schocken,
1961) and On the Kabbalak and Its Symbolism (New York: Schocken, [1960] 1965). The
primary source of Kabbalah is The Zohar, 5 vols, (London: Soncino Press, 1934).

14. Gershom G. Scholem, Javish Gnosticism, Merkabah Mosticism, and Talmudic Tra-
dition (2nd ed.; New York: Bloch, 1965)

15. Walter Laqueur, A History of Zionism (New York: Holt, Rinehart & Winston,
1972); Ronald Sanders, The High Walls of Jerusalem: A History of the Balfour Declaration
and the Hirth of British Mandate for Palestine (New York: Holt, Rinehart & Winston,
1983).

16. Gary V. Smith (ed,), Zionism: The Dream and the Reality, A Jewish Critique (New

 

 

 

  

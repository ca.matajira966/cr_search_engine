A Separate People in Our Midst 67
mentions it,

The hexagram and pentagram, it should be pointed out, both
carried the designation “Seal of Solomon” and were employed in
both Christianity and Islam as symbols with magical or amuletic
power. On the parchment of many medieval mezuzot (capsules
placed on the doorposts of every Jewish home) the hexagram and
pentagram (Sea! of Solomon) were written out and also served as
a talisman or had magical powers to ward off evil spirits.

The point is, few Jews or gentiles are aware of any of this,
That the flag of the state of Israel bears an ancient pagan symbol
is not a well-known fact either to those who respect it or who resent
it. In short, the vast majority of Christians and many Jews know
very little about the history of Judaism. Jews and Christians are
aware that their respective religious practices are quite different,
yet not many of them know why, and to what extent, their religions
differ. People speak of “the Judeo-Christian tradition,” yet they are
not quite sure what this tradition is, or if it even exists.?!

Rival Religions

I agree with the astoundingly prolific Orthodox Jew, Jacob
Neusner, whose studies on Jewish law are as close to definitive as
the writings of any one person can be.”* He writes: “Judaism and
Christianity are completely diffcrent religions, not different ver-
sions of one religion (that of the ‘Old Testament,’ or ‘the written
Torah,’ as Jews call it). The two faiths stand for different people
talking about different things to different people.”*? He: argues
that the key differences center on the two rival programs: salvation

20. Joseph Gutmann, The Jewish Sanctuary (Leiden: E. J. Brill, 1983), p. 21. This
study is Section XXIM; Judaism, of the Iconography of Religions, produced by the
Institute of Religious Iconography of the State University Gronigen, Netherlands.

21. Arthur A. Cohen, The Myth of the Judeo-Christian Tradition (New York: Schocken,
1971), J. H. Hexter, The Judeo-Christian Tradition (New York: Harper & Row, 1966)

22. Jacob Neusner, History of the Miskna Laws, 5 parts, 43 volumes (Leiden,
Netherlands: E. J. Brill). He has written many other books.

23. Jacob Neusner, “lwo Faiths ‘Talking about Different Things,” The World & I
(Nov. 1987), p. 679.

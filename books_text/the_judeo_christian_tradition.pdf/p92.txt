78 The Judeo-Christian Tradition

ences between the two, and his summary makes it clear why Jesus
rejected both groups:

What I would now explain is this, that the Pharisecs have delivered
to the people a great many observances by succession from their
fathers, which are not written in the law of Moses; and for that
reason it is that the Sadducces reject them, and say that we are to
esteem those observances to be obligatory which are in the written
word, but are not to observe what are derived from the tradition
of our forefathers... 2!

. the Pharisees are those who are esteemed most skillful in the
exact explication of their laws, and introduce the first sect, They
ascribe all to fate [or providence,] and to God, and yet allow, that
to act what is right, or the contrary, is principally in the power of
men, although fate does co-operate in every action. They say that
all souls are incorruptible; but that the souls of good men are only
removed into other bodies,— but that the souls of bad men are
subject to cternal punishment. But the Sadducees are those who
compose the second order, and take away fate entirel

 

y, and sup-
pose that God is not concerned in our doing or not doing what is
evil; and they say, that to act what is good, or what is evil, is at
men’s own choice, and that the one or the other belongs so to every
one, that they may act as they please. They also take away the
belief of the immortal duration of the soul, and the punishments
and rewards in Hades.??

‘The Sadducees’ influence faded rapidly after the destruction
of the Temple in A.D. 70. Herbert Danby, whose English transla-
tion of the Mishnah is still considered authoritative by the schol-

 

J. H. Hertz, “Foreword,” The Babylonian Talmud, Seder Nezikin (London: Soncino Press,
1935), p. xiv, Unitarian scholar R. Travers Herford has writen several sympathetic
accounts of the tradition of the Pharisees, most notably The Pharisees (London: George
Alien & Unwin, 1924); The ihics of the Talmud: Sayings of the Fathers (New York:
Schocken, [1945] 1962).

21. Josephus, Antiquities of the Jews, Bk. XITL, Ch. X, Sect. 6. William Whiston
translation, 1737.

22. Josephus, Wars of the Jews, Bk. 11, Ch. VII, Sect. 14.

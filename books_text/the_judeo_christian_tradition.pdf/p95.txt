The Talmud: A Closed Book, Even When Open 81

The rabbinic Torah is very different from the Old Testament.
Danby comments: “It includes the Written Law, the laws explic-
itly recorded in the Five Books of Moses; it includes also ‘the
traditions of the elders’ or the Oral Law, namely, such beliefs and
religious practices as piety and custom had in the course of centu-
ries, consciously or unconsciously, grafted on to or developed out
of the Written Law; and it includes yet a third, less tangible
element, a spirit of development, whereby Written Law and Oral
Law, in spite of seeming diffcrences, are brought into a unity and
interpreted and reinterpreted to meet the needs of changed condi-
tions.”29 In short, there are three elements that comprise the
Torah: the Old Testament, the oral law, and casuistry.

The two primary questions that I am raising in this book are
these: 1} Is traditional Judaism’s casuistry even remotely biblical?
2) Is it the product of an anti-Old Testament perspective?

Dialecticism vs. Casuistry

The Talmud is just about useless as a source for writing a
Bible commentary, not simply because it is such a difficult set of
books to use by Jews or gentiles, but also because the large number
of comments by the rabbis are so often very brief, and so often
contradictory to each other. A self-conscious dialecticism underlies
the Talmud: endicss debate without authoritative or logical reconcili-
ation. Dialecticism is one aspect of Judaism's tradition of deliber-
ate secrecy, a tradition adopted by Maimonides in the style of his
Guide of the Perplexed.

 

to us by word of mouth from generation to generation, right from the time of Moses.
‘They have been ¢ransmitted to us precise, correct, and unadulterated, and he who
does not give his adherence to the unwritten law and the rabbinic wadition has no
right to share the heritage of Israel; he belongs to the Sadducees or the Karaites who
severed connection to us long ago.” Chajes, Student’s Guide Through the Talmud, p. 4,

29. Danby, “Introduction,” Mistuak, pp. xiii-xiv.

30. For a detailed discussion of these additions to the written law of the Old
Testament, see R. Travers Herford, Talmud and Apocrypha (London: Soncino, 1933),
pp. 66-69. Herford was a Unitarian scholar; Soncino Press is the Jewish publishing
house that published the official and unabridged English-language Taimud.

31. “. . . Maimonides deliberately contradicts himself, and if a man declares both

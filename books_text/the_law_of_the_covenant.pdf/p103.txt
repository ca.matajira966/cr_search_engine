84 The Law of the Covenant

Friends, and may sit at the Lord’s Table as full members of the
household, and heirs. We let them know that as long as they are
alive they live in the Lord’s house, and partake of certain benefits
of the death of the Son; but that if they refuse the offer to become
adopted slave-sons, they will eventually be cast out and lose all
benefits.

Female Slaves

A separate set of laws governs the female slave:

7. When a man sells his daughter as a slave, she shall not go out
as male slaves do.

8. If she is not pleasing in the sight of her master, who has
designated her to himself [or, who has not designated her], then he
shall cause her to be redeemed; to a foreign people he shall have no
right to sell her, since he has dealt faithlessly with her.

9. If he should designate her for his son, he should deal with
her as with a daughter.

40. If he should take to himself another wife, her food [flesh],
her clothing [covering], and her marital rights [response] he
should not diminish.

11. And if he does not do these three things for her, then she
shall go out for nothing, without payment of money.

There seems to be a contradiction between this passage and
Deuteronomy 15:12, which says “If your kinsman, a Hebrew man
or woman, is sold to you, then he shall serve you six years, but in
the seventh year you shall set him free.” Here the woman is let go
in the sabbath year. The contradiction, however, is easily resolved.
Ifthe woman is purchased in order to become a wife, she is not set
free, because marriage is perrnanent. If, however, the woman is
purchased for labor alone, to be a lady’s servant for example, she
is to be returned to her family’s house in the sabbath year.

As regards the daughter sold to be a wife, the following may be
observed. Ordinarily, a husband provided his wife with “bride money”
which was a form of insurance for her. Robert North explains:
“The purchase of the bride was generally made by the father of the
youth. The lady retained the payment, at least in some cases, in

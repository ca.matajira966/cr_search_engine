Slavery (Exodus 21:2-I1) 85

the form of coins strung upon her body as ornaments, so that if at
any time she was divorced, she would not be wholly unprovided;
moreover, the cupidity of her spouse would deter him from rashly
giving up control of such tangible assets,”!? In the case of the
daughier sold into slavery, this bride money went to the father of
the girl instead of to her. Thus, she was a wife without property or
insurance, and therefore not a free woman. Exodus 21:7-11 speaks
of such a girl as the wife of the master or of one of his sons, but vv.
2-6 indicate that the girl might be purchased to become the wife or
one of the master’s slaves. This would be part of the contract at
the time of sale. Mendelsohn points out that documents from
Nuzi reveal that the status of the man whom the girl married was
written into the contract when she was sold.8 Because the unin-
sured slave-wife was exposed to greater liabilities than her free
counterpart, special provisions are included in the law to protect
her.

Verse 8 can be read either of two ways, depending on how one
interprets a Hebrew particle. In one reading, the master had
designated her for himself, but once the girl became nubile, he
changed his mind. The alternate reading gives that she was not
pleasing to the master, and he designated her for nobody. Either
way, the girl is not to be married after all. The master is not per-
mitted to sell her outside the covenant family, though possibly he
might sell her to another household. Her own family, however,
has the first right of redemption in this case, In the second case,
the master contracted not for a wife but for a daughter-in-law.
The girl must be treated as a daughter (v, 9), which makes this an
adoption contract. A third case, understood from Exodus 21:4f., is
for the girl to be purchased in order to be given as wife for a slave.

There are three things that the husband-master of the slave-
wife must do for her. First, he is not to diminish her “flesh.” This

V7, Robert North, Sociology of the Biblical Jubilee. Analecta Biblica Investiga-
tiones Scientificiae in Res Biblicas 4 (Rome: Pontifical Biblical Institute, 1954),
p. 151. See Chapter 8 for further discussion of this matter, I don’t agree that the
bride was “purchased” in this transaction.

18. Isaac Mendelsohn, Slavery in the Ancient Near East (New York: Oxford,
1949), pp. 10ff.

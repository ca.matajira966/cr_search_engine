86 The Law of the Couenant

word is used for meat in Psalm 78:20, 27 and Micah 3:2, 3, and
seems to refer to good food. When the people tired of manna, they
wanted tastier food, called “flesh” in Psalm 78:20, 27, Thus, the
master is not only to provide her with food, but with food as good
as what the other members of his household eat.

Second, he is not to diminish her “covering.” ‘This means not
only clothing in general, but the protective covering of a heavy
cloak. (The same word appears in Exodus 22:27, with this mean-
ing.) Metaphorically, the husband must grant her protection, as
well as warmth and clothing.

Third, he is not to diminish her “response.” Traditionally, this
term has been seen as referring to sexual relations. The only other
place where the term “answer” seems to have a sexual meaning is
Malachi 2:12, “ds for the man who does this [fornicates with a
foreign woman], may the Lorp cui off from the tents of Jacob
everyone who arouses and responds... .” In the context of
Malachi, what is in mind is the action of Phinehas in skewering a
fornicating couple in Numbers 25:8. The man was involved with
a foreign woman, and they were killed in the tents of Jacob. The
covenant of peace with Phinehas (Num. 25:11-13) is referred to in
Malachi 2:15.19

‘The occasion for the master’s mistreatment of his slave-wife is
his marriage to a second woman. Clearly, however, mistreatment
of the woman for any reason would be grounds for divorce. And
clearly this is a divorce, the grounds being maltreatment. Those
who insist that the wife should remain with her husband, even if
he beats her and otherwise abuses her, are completely out of line
with Scripture at this point.

Ifa slave wife can get a divorce for maltreatment, how about a
free wife? This is a hard question to answer. A free wife in Israel
had certain privileges, particularly access to considerable mone-
tary property (the bride money). Having this property gave her a

19, The verb for “arouse” can bi scen to have some degree of sexual overtone
in Canticles 2:7; 3:5; 4:16; 5:2; 8:4. The phrase “arousing and responding” can
easily be scen as a discreet reference to the pleasures of sexuality. For alternative
views of Exodus 2:10, see Jordan, Slavery and Liberation (Corthcoming).

 

Violence (Exodus 21-12-36) 105

The prohibition of kidnapping speaks to a number of issues,
such as the proper penalty for ransom-kidnapping, the impress-
ment of seamen, and perhaps even the military draft. Naturally, it
also speaks to the slave trade. Note, however, that slave traders
are not sentenced ta death here, but only kidnappers. This may
seem strange, but as we pointed out in Chapter 5, heathen cul-
tures regularly practise slavery, and the sale of such slaves to
Israelites was a means of evangelizing them, The Hebrews were
free to purchase slaves from traders (Lev. 25:44).

The Bible is not revolutionary. As the world becomes Chris-
tian, chattel slavery will disappear for two reasons: First, slaves
will become converted, and earn their freedom. Biblical law pro-
tects their right to do this. Second, no new people will enter
slavery ag a result of kidnapping and slave raiding. Thus, the laws
of Scripture are designed to eliminate slavery without destroying
the existing fabric of society.

V. Repudiating Parents

God requires death for the son who “curses” his father or his
mother. There are two words for “curse” in Hebrew, One has as its
basic meaning “to separate from or banish,” and is used for the
curse in Genesis 3:14. The second, which is used in Exodus 21:17,
basically means “to make light of, or repudiate.” As Umberto
Cassuto has pointed out, this verb “to make light of” is the op-
posite of the verb which means “to make heavy, honor, or
glorify."!7 For the Hebrew, to glorify or honor someone was to
treat them as weighty, just as American slang in the 1970s and
1980s uses the word “heavy” to refer to important or impressive
matters. .

The fifth commandment orders sons and daughters to honor

 

1% Umberto Cassuto, Exodus (Jerusalem: Magnes Press, 1967), p. 271.
Cassuto calls attention to 1 Samuel 2:30 and 2 Samuel 6:22 as examples where “ta
make light of or curse* is set in opposition to “to glorify or honor.” An extended dis-
cussion of the conception of cursing in Scripture, relevant to Exodus 28:17, is
Herbert G. Brichto, The Problem of “Curse” in the Hebrew Bible, Journal of Biblical
Literature Monograph, Series XIII (Philadelphia: Society of Biblical Literature,
1963). The verb “to make light of” (gulal) is discussed on pp. 134ff.

130 The Law of the Covenant

towuship, and you are no longer liable.*#!

Il. Ox Gaves Ox

As in verse 28, the owner is not liable if his ox has not been
notorious in the past, If the ox gores another ox, the value of each
is divided between the two owners, the dead ox for its hide, and
the live ox for full value.

Tf, however, the ox was notorious, then the owner is liable. He
has to pay the full value of the dead animal to its owner, but gets
to keep the hide for himself.

Now, suppose the law in your town reads that dogs are to be
penned or leashed. Your dog escapes and kills the neighbor’s prize
winning cat. What is to be done? Well, in this case, your dog is
assumed to be notorious because the law said to keep it restrained.
You, therefore, owe the neighbor for the full value of the cat,
whatever your neighbor might have gotten on the open market if
he had sold the cat alive.

Suppose, however, that there is no ordinance requiring dogs
to be penned or leashed. In that case, your dog is not assumed to
be notorious unless it has a personal history of viciousness. In that
case, however, you do not get off scot free, because you lose your
dog. The dog must be sold, if it can be, and the money divided
with the neighbor whose cat was killed. Thus, you still have some
incentive to act responsibly, even if your dog or ox is not
notorious.

41. To say that liability for hazardous property must be limited to some degree
is not to grant approval to the modern limited liability corporation. The merits of
limiting liability must be considered on a case by case basis. See Rushdoony, Jn-
stitutes of Biblical Law, pp. 664ff., for a critique of the limited liability corporation.
For another viewpoint, see Gary North, .C.#. Position Paper #1 “Why Churches
Should not Incorporate.” (Institute for Christian Economics, P.O. Box 8000,
Tyler, TX 75711.)

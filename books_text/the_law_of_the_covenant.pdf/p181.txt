162 The Law of the Covenant

There is no penalty attached to cursing God in this sense, If,
however, a man actively and openly repudiates the covenant, the
punishment is death (Dt. 13:17). This is what Naboth was accused
of in 1 Kings 21:10, 13 (and see Job 2:9). Also, public cursing of
God with the tongue merits death, according to Leviticus
24:10-23. The man who “curses” (treats God lightly) will bear his
own sin (Lev. 24:15), but the man who “blasphemes” (publicly at-
tacking the Name of God) must be put to death (v. 16). From this]
conclude that the man who simply decided to quit paying the
tithe, or failed to give his firstborn, was not punished by the
courts. It was left to God to deal with him.

Ti. Cursing Rulers

A different verb is used here, one which generally means to
speak evil of someone. It is the opposite of “to bless,” rather than
of “to honor.” The term for rulers (Hebrew: nasi’) here primarily
designates a civil authority, though ecclesiastical rulers are not ex-
cluded. Even if rulers are evil, we are to pray for them, not curse
them (Rom. 12:14). All authority is from God, and must be
respected. “Even. Michael, the archangel, when he disputed with
the devil and argued about the body of Moses did not dare pro-
nounce against him a railing judgment, but said, ‘The Lord
rebuke you’ ” (Jude 9).

Nothing in the Bible indicates that there is a civil penalty for
the violation of this law, The Jews surely would have invoked it
against Paul, had there been such: “Then Paul said to him, ‘God
is going to strike you, you white-washed wall! And do you sit to
try me according to the Law, and in violation of the Law order me
to be struck?’ But the bystanders said, ‘Do you revile God's high
priest?” And Paul said, ‘I was not aware, brethren, that he was
high priest; for it is written, “You shall not speak evil of a ruler of
your people” ’ ” (Acts 23:3-5).

The officers God has set in authority are not to be undermined
by our words. Mothers are not to undermine fathers in the eyes of
their children. Elders in the Church and officers of the state are

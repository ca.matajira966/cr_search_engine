180 The Law of the Covenant

prison” (Matt. 5:24; cf. Luke 12:58).

Finally, the Law of Equivalence only applies to the false ac-
cuser, not to any mere witness who may give false testimony,
Other kinds of punishments would, however, be in order for the
latter (ef., e.g., Dt. 25:3).

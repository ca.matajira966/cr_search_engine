182 The Law of the Covenant

the Old Creation, in bodies of the first Adam (pre-resurrection),
the pattern of rest and festivity set out in the Old Testament is ap-
plicable to us.

I suggest the following formula: In the Old Covenant, men
were to worship on the sabbath day (Lev. 23:3); in the New Cove-
nant, men are to sabbath on the day of worship. The Lord’s Day
is not a “sabbath day,” and properly should not be called such, ex-
cept by extension. (We do not wish, however, to quarrel about
words.) The day now is defined by worship and judgment, not by
rest.

The typological aspects of the sabbath do not concern us here,
though they bear on how we are to keep the Lord’s Day in the
New Covenant. The passage before us concerns the practical
aspects of rest and the giving of rest. We shall, however, be con-
cerned with the typological aspects of the festival calendar. We
may divide the section into two parts:

1. Three laws concerning the observance of sabbaths (wy.
10-13).

2, Ten laws concerning the observance of festivals (vv. 14-19),

Sabbaths

10-1. Six years you shall sow your land and gather its produce,
(i1.) but the seventh year you shall let it rest and lie fallow, and the
poor of your people will eat, and what they leave the wild beasts
will eat. Thus you shall do to your vineyard and your clive or-
chard.

12. Six days you shall do your work, and on the seventh day
you shall rest, so that your ox and your ass may rest, that the son of
your maidservant and the stranger [sojourner] may be refreshed.

13. And in all that I have said to you (pl.}, you (pl.) shall take
care, and the name of other gods you (pl.) shall not mention nor let
be heard out of your (s.) mouth.

When Israel was in Egypt, they were in the cruel bondage of
pagan chattel slavery. They never got to rest. The wording of the
sabbath laws here reflects the deliverance from slavery, in that the
erophasis is on rest, and the giving of rest to one’s subordinates,

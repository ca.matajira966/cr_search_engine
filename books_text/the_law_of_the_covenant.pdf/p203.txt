184 The Law of the Covenant

In my pamphlet, Sabbath Breaking and the Death Penalty: An Alter-
nate View, I have addressed the question of the death penalty for
sabbath breaking, and some wider ramifications of sabbath keep-
ing in general.

Festivals

14. Three times in the year you shall keep a feast to Me.

45a. The Feast of the Unleavened Bread you shall keep: For
seven days you shall eat unleavened bread, even as 1 commanded
you, at the appointed season in the munth of Abib, for in it you
came out of Egypt;

15b. And none shall present himself before Me empty-handed,

16a. And the Feast of the Harvest: of the first-fruits of your
work, of what you sow in the ficld.

léb. And the Feast of Ingathering at the end of the year, when
you gather in the fruit of your labor from the field.

17. Three times in the year all your males will present ihem-
selves before the Master, the Lorn.

18a. You shall not offer the blood of My Sacrifice with leavened
bread.

i8b. And you shall not let the fat of My Feast remain until
morning.

19a, The first of the first-fruits of your ground you shall bring
into the house of the Lor.

19b. You shall not boil a kid in its mother’s milk.

God’s demand to Pharaoh was that His people be let go so that
they might celebrate a worship festival to Him (see Chapter 2). It
is most appropriate, then, that the Book of the Covenant closes
with laws which specify this great privilege to the sons of Israel.

The festival calendar of the Old Testament is no more and no
less binding on the Church today than is the sabbath day. Just as
the Lord’s Day has come in the place of the sabbath day, so the
Church has devised voluntary festivals in the place of those of the
Old Covenant. Just as the Old Covenant feasts followed the
rhythmic pace of the natural year, giving typological meaning to

3. Forthcoming from Geneva Ministries.

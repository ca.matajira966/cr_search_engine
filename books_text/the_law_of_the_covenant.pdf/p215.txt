Appendix A
OLD AND NEW COVENANT

Theologians use the terms ‘Old Covenant’ and ‘New Cove-
nant’ in a variety of ways, and so it is necessary for me to spell out
how I am using the terms.

Tt is sometimes assumed that man was created to be a
covenant-receiver, but which of the covenants man receives has
nothing to do with his fandamental nature. Man is always “in”
some covenant, but by nature he is not in any particular cove-
nant. I reject this idea, [t seems to me that a Biblical philosophy of
man and covenant should be worded this way: that Adamic flesh
is definitively linked ta the Old Creation and to the First (or Old)
Covenant. The placement of the sabbath in the First Covenant
(with Adam) indicated that the goal of history was a transfigured
condition, with new flesh, a new creation, and a new covenant.!
Because of sin, this consummation comes through death and
resurrection, so that we speak not of a transfigured body, but of a
resurrection body.

The New Covenant cannot, then, arrive in history apart from
the resurrection body, for the New Covenant is tied to the candi-
tion of man in resurrection and in the new creation. Jesus Christ
alone is in the resurrection body, and in the perfect environment
of heaven, so that He alone is in the New Covenant in this full
sense.

Christians still live in the Old Creation, in Old Adamic flesh,

1. [have dealt with this more extensively in my essays on the Sabbath in “The
Geneva Papers.” These are available from Geneva Ministries, P.O. Box 8376,
Tyler, TX 75741.

196

198 The Law of the Covenant

No one can be saved under the Old Covenant, for the sin of
man brings only the curse of that covenant. Abraham and David
were saved by faith in the New Covenant, in the Christ to come,
even though they lived under the Old Covenant. Christians live
under the New Covenant, but in the Old Creation.

In this chart, I have graphed this view, showing how people
were to live under each covenant (the mode of life), what the
essential nature of the period of history is, and what the condition
of the creation is.

 

Specific Covenant Condition of the General Covenant | Mode of Life
Physical Creation (Covenantal Nature | (Which General
of the Period) Covenant the Power
for Life Proceeds
From)

 

Adami (Pre-Fall) Old (First) Creation] Old Covenant Flesh (Power of
the First Creation
proceeding from
the First or Old
Covenant)

Adamic (Post-Fall) | Old Creation Old Covenant Spirit (Provisional
Power of the New
Creation, proceeding
from the New
Covenant)

 

 

Noachie Old Creation Old Covenant Spizit (provisional)
Abrahamic Old Creation Old Covenant Spirit (provisional)
Mosaic Old Creation id Covenant Spirit (provisional)
Davidie Old Creation Old Covenant Spirit (provisional)
New (pre-consum- | Old Creation New Covenant

mational)

   

New (consumma- | New Creation New Covenant
tional) (New Heavens tional)
and Earth)

 

 

 

 

 

 

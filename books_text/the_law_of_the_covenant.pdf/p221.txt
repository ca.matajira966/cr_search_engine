202 The Law of the Covenant

the blood of the creature, but solely from God. Life is to be
returned to God, Who gave it.

18:1-30 Uncovering nakedness through abominable practices,
When Adam and Eve sinned, their nakedness was exposed, and
they were cast out of God’s presence, where His Name had been
placed (the garden).

19:1-37 Various laws, all predicated on the need to fear the
Name of God. Rationale for obedience: “I am the Lorn.” Thatis,
fear my Name.

20:1-27 Punishments for uncovering nakedness.

241-24 The priests are especially associated with God’s
Name, and thus must especially separate from ceremonial death.

22:1-33 The sacrifices offered must not “profane My holy
Name” (vv. 2, 32).

23:1-44 Worship in the presence, before the Name, of God.

24:1-9 The light of God and the Name of God.

24:10-23 The punishment for cursing the Name of God.

Similarly in Deuteronomy:3

14:1-2 Excessive mourning not compatible with wearing God’s
Name.

14:3-20 Unclean animals.

14:21a Do not eat dirt, flesh rotting and returning to dirt.

14:21b Do not confuse life and death by cooking a kid in its
own mother’s milk, which had been its source of life.

14:22-29 Rejoice at the place where God has established His
Name (v. 23).

The Fourth Commandment

The fourth commandment has to do with rest, festivity, and
release from bondage. The Ordinances in Exodus 21 ~ 23 begin
and end with laws related to the fourth commandment:

21:1-11 Release of slaves from bondage into dominion-rest.
23:10-13 Sabbaths.

3, Kaiser puts Deuteronomy 13 and 14 under the third commandment. [
believe that he and Kaufman have not located the precise ethical perspective of
the third commandment properly.

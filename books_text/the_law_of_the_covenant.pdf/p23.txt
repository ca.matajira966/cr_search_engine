4 The Law of the Couenant

Man depends on God for Life, but man does not participate in God’s
existence.> At the same time, however, man was created to partici-
pate in social fellowship with God, to enjoy the life of God in this
way. Man was to have a community of life with God, though not a
community of essence.

According to Genesis 1:27, “God created man in His own im-
age, in the image of God created He him (singular); Male and
female He created them (plural).” What this says is that man is a
copy of Ged, Man's capacities, his personality, and his morality are
copied from God. Also, the fact that man is a social being is
copied from God. Each individual human being is an image of
Gad, but also each human society is an image of God.*

Now, to get back to our original question: What is the cove-
nant? We may say, in part, that the covenant ts the personal structural
bond among the three Persons of God. The only time that bond was
ever broken was when the Lord Jesus Christ went into hell
(separation from God) on the cross, crying “My God, My Ged,
why hast Thou forsaken Me?” (Matt. 27:46). This is a great
mystery.

The inter-personal relationships among the Persons of the
Trinity constitute a covenantal bond which involves Persons and a
structure. This bond is simultaneously personal and corporate. God's
personal relationships with men are therefore also covenantal.
When God created man in His image, man was incorporated
into this covenant among the three Persons of God, This relation;
ship was not only personal, however; it was also structural. There

3. Jt might be worth calling attention to the fact that the Bible continually
symbolizes man’s dependence on God by means of food and water. Ged is Giver
of both, Man does not have life in himself, and must have them to live, From the
‘Tree of Life in the Garden to the Holy Eucharist of the Church, God’s gift of life
to man is signified through food, This is particularly prominent in the wilderness
experience of Israel (Exodus - Numbers), but it can also be seen in the very
description of the promised land as a land of milk and honey. See James B, Jor-
dan, Food and Faith (forthcoming).

4. A more extensive discussion of covenant and of man’s imaging of God is
poe in James B. Jordan, Trees and Thorns: An Exposition of Cenesis 1-4 (forthicom-
ing).

212 The Law of the Covenant

11:33f., Jude 12). Some churches have occasional Love-Feasts
(Agapes, or covered dish meals). Others have them monthly (new
moons) or weekly. It is appropriate to use the first part of one’s
tithe to pay for the dinner you bring to these suppers. The poor, of
course are to be sponsored by those better off.

21. Ordinarily, the tithe went to the Levites. The New Cove-
nant affirms that all the Lord’s people are Levites (Dt. 33:9 + Matt.
10:35-37, etc.). This does not mean that the Old Covenant people,
under the provisional administration of law and grace, were not
also priests. Indeed, the Levites came into being as substitutes for
the firstborn of all Israel (see #5 above), so that foundationally
every household in Israel was a priestly community. What this
means is that the Levites were ecclesiastical specialists, called to
special office.

22. The Biblical view of special office is neither democratic nor
aristocratic. Every Christian has the general office. The rationale
for special office is in terms of gifts and in terms of the need for
good order (i Cor. 12; 14:40), not in terms of priesthood in any
pagan (aristocratic) sense. In times of distress, any general officer
may teach, baptize, and administer communion (cf. Ex. 4:25),

23. The tithe went to the Levites because they were ecclesiasti-
cal specialists. The elders of the gate governed the use of the syna-
gogue’s money. Churches which distinguish between preacher-
teachers and ruling elders have an analogous system today.

24. The Levites tithed to the high priest and his family
(Numbers 18). Analogous to this, since the high priest was the
high court of the Church (Dt. 17:8-13), there is a place for a tithe
of the tithe to be passed from the local church to larger courts for
their purposes.

25. The local tithe was administered by the elders for two pur-
poses: the salary of the synagogue Levite and care for the poor
(including the widow, fatherless, and alien). The national tithe
was used by the Levites for a number of purposes, principally
educational or cultic in character. An examination of these will
show us what the tithe should and should not be used for today.

26, A study of the temple taxes of the Bible shows that the sac-

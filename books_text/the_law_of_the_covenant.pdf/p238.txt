Appendix C—Tithing: Financing Christian Reconstruction 219

who makes Christ specially present at His sacraments. Baptists
have no theory of social order, for they have taken Western
nominalism to its extreme of almost total individualism; for them
the sacraments are mere symbols.!°

50. The reconstruction of society means that foundational at-
tention must be paid to the reconstruction of worship, Hard
thinking must be devoted to architecture, building churches that
can accommodate true love feasts, orchestras and choirs,
sacramental worship in the round, and even places for sacred
dancing. Work needs to be done in music, training in psalm sing-
ing and chanting, the development of competent choirs and or-
chestras, writing music truly worthy of the worship of God (as op-
posed to the cheap junk of the last century or so), The develop-
ment of a professional class of theologians and Biblical lawyers,
who can speak ta the legal questions of our day and retrain our
civilization in the Word of God, is also a task of the tithe. And of
course, the general care and retraining of the poor and helpless is
a task of the tithe as well.

Should the Tithe Always Go to the Church?

51. Because of the incredible failure of the Church in our day,
it is very easy to make a case for giving the tithe to parachurch
organizations (non-sacramental teaching orders). I believe that
the question here must be approached with care. My thesis is that
the elders of the gate (the local church) should in normal healthy
times administer the tithe, and they may use it in part to support
various agencies; but that in times of apostasy the tithe must go to
the Lord, and this may mean giving it to non-sacramental
teaching organizations.

52. It will not do to say that the general office of all believers
means that the tithe may be given wherever the individual wants.
Nor will it do to say that the special office in the Church is to be
given the tithe under any and all circumstances. Rule in the

10. See James B. Jordan, ed., The Fuilure af the American Baptist Culture, Christi-
anity and Civilization No. 1 (Tyler, TX: Geneva Divinity School Press, 1982),

The Bible and the Laws of God 5

were rudes which defined what perfect personal and social morality
was to be like. When Adam and Eve broke these rules by rebelling
against the structure God had given them, they broke the cove-
nant, and were subsequently cast out of the presence of God, into
an earthly wilderness which eventually will become hell.

The covenant ts a personal-structural bond ushich joins the three Persons
of God in a community of life, and in which man was created to participate.
On the cross, Jesus Christ descended into hell as a substitute for
His people, and as a result, His people are reunited into the cove~
nant. We call this work of Christ redemption. Redemption is the
doorway back to the garden of Eden, back to the covenant
fellowship of God.

Were we to make a transcription of the moral character of any
one Person of God, or of the One Person of God, we should have a
description of a morally perfect man as well, because man is the
image of God.* This would show us what individual morality con-
sists of.

Were we to make a transcription of the moral character of the
covenant relationship among the three Persons of God, we should
have a description of perfect social life for man. This would show
us what social morality consists of.

God is no more One than He is Three, and no more Three
than One. In the same way, the social life of man is no more im-
portant than his individual life, and his individual life is no more

5. “I do not ask in behalf of these alone, but for those who believe in Me
through their word; that they may all be one; even as Thou, Father, art in Me,
and J in Thee, that they also may be in Us; that the world may believe that Thou ~
didst send Me” (John 17:20, 21).

6. “With regard to the doctrine of the Trinity, Van Til denies that the paradox
of the three and one can be resolved by the formula ‘one in essence and three in
person’ Rather, ‘We do assert that God, that is, the whale Godhead, is one per-
son.” John Frame, “I'he Problem of Theological Paradox,” in Gary North, ed.,
Foundations of Christian Scholarship (Vallecito, GA: Ross House Books, 1976), p.
306f. The point is that we cannot address “God” in prayer unless the Oneness of
God is personal; otherwise, we should only be able to address one of the Three
Persons in prayer, never simply “God.” Thus, we can speak of man as an in-
dividual as an image in general of the One Person of God, even though human
nature most closely scems to image in particular the Second Person, the Son,

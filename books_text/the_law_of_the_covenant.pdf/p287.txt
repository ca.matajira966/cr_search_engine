268 The Law of the Covenant

signify the office-bearer.

Oppressing the Poor

The two instances in Scripture where the law of four-fold resti-
tution is seen in operation both have to do with the oppréssion of
the poor by a powerful man, who misuses his office to bring about
harm to the dominion of a poor man. The poor man does not have
much dominion, but he exercises comprehensive dominion over
what he does have, and this is to be regarded as inviolable.

The first case is in 2 Samuel 12. Nathan tells David a story
about a poor man who had nothing except one little ewe lamb,
who was like a child to him. A rich man stole the lamb and killed
it, David immediately states that this man is worthy of death, but
that the actual penalty to be inflicted is four-fold restitution.
Nathan then informs David that his stealing of Bathsheba from
Uriah, and his murder of Uriah, correspond to the rich man’s
stealing and killing the ewe lamb. Four-fold restitution will be ex-
acted from David (the death of Bathsheba’s child, the death of
Amnon, the death of Absalom, and the death of Adonijah).

The second case is that of Zaccheus in Luke 19:8. Upon his
conversion, Zaccheus states that wherever he has defrauded
anyone, he will make four-fold restitution. It is customary to see
Zaccheus as going beyond the bare requirements of the law, which
specify merely adding the fifth part in the case of voluntary resti-
tution (Lev. 6:1-7). This understanding of the event raises a prob-
lem, however,

If Zaccheus is going beyond the law, we should expect to see
his generosity expressed in terms other than the particular re-
quirements of the law. The Bible is usually quite particular in
distinguishing among gifts, heave offerings, tithes, and so forth.
We could reasonably expect to read something like this: “Half of
my possessions I will give to the poor, and wherever I have
defrauded anyone of anything, I will make it good by adding the
fifth part, and also bestow upon that person a gift of such and such
an amount.”

This is a problem, and it directs us back to the text to see if we

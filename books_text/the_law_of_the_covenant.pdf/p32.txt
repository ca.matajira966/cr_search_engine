The Bible and the Laws of God 13

his faith in God that God would save him from sin and death
through some future act to which these rites pointed. The New
Testament believer puts his faith in God in the same way, but he
can lay hold directly on the death of Christ as his substitute and
way of salvation. The same principle is involved in both cases, the
same law is obeyed, except that the historical circumstances have
changed.

Second, the outpouring of the Holy Spirit gave io the Church more
power than she had known before. Because the Old Testament Church
was relatively weaker than the New Testament Church, God
made special provisions to protect the Church during her Old
Testament infancy. This varied from period to period, and at
some times we see very few such special provisions (as during
Abraham’s day). What we think of as the definitive expression of
the Old Covenant, however, the Mosaic administration through
the monarchy, does show such special provisions. During this
time, God’s protection took the form of tying the Church closely to
a particular nation, with geographical boundaries, with a military
force, with supernatural acts of protection and special super-
natural guarantees. Moreover, since the people were to be holy,
but since the Holy Spirit had not been poured out in power, God
gave them many peculiar regulations as reminders of obedience.
They were, for instance, to dress in a peculiar manner.!7

In the New Covenant, all these reminders coalesce in the
Memorial Supper, the Holy Eucharist.

Third, the cleansing of the world means thal men are no longer defiled
by coming in coniact with death. Under the Old Covenant if a man so
much as picked up a dead roach with his hand, he was unclean

17, “The Lord also spoke to Moses, saying, ‘Speak to the sons of Ierael, and tell
them that they shall make for themselves tassels on the corners of their garments
throughout their generations, and that they shall put on the tassels of each corner
acord of blue. And it shail be a tassel for you to look at and remember all the
commandments of the Lorn, so as to do them and not follow after your own
heart and your eyes, after which you played the harlot, in order thar you may
remember to do all My commandments, and be holy to your God’ * (Numbers
15:37-40).

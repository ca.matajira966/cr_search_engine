18 The Law of the Covenant

The Laws of God have Multiple Equity

In saying that the laws of God have multiple equity, we mean
that each specific law implies God’s law-system as a whole, and as
a result, a given law will have applications in more than one area
of life. Multiple equity means multiple applications, and even
multiple principles. The Westminster Larger Catechism (Ques-
tion 99, Section 3) puis it this way: “That one and the same thing,
in diverse respects, is required or forbidden in several command-
ments,” citing as proof texts Colossians 3:5, Amos 8:5, Proverbs
1:19, and 1 Timothy 6:10, to which we might add James 2:10.74

This means that more than one basic principle may underlie
any particular command of the law. For this reason, some laws
are repeated two or three times in the Torah, but in different con-
texts, showing that they pertain to different basic principles. For
example, the prohibition on witcheraft occurs in Exodus 22:18, in
connection with laws of adultery, since witchcraft is apostasy from
the Divine Husband of Israel.28 In Deuteronomy 18:10, the same
prohibition occurs in a section on submission to authority, since
witchcraft is identified with rebellion (1 Sam. 15:23, as demon-
strated by 1 Sam. 28:7). Again, witchcraft is’ forbidden in
Leviticus 19:31 and 20:27, where the context is the separation of
life and death— part of the so-called “ceremonial” law. Thus, the
sin of witchcraft is related to the 8th Commandment, to the 5th
Commandment, and to the 3rd Commandment (to wear God’s
Name to good effect is illustrated by the laws commanding that
life be preferred over death),?*

This is what is meant, then, by asserting that the laws of God
have multiple equity. The anti-witchcraft legislation has equity in
the areas of adultery, rebellion, and blasphemy (as well as others).
This fact explains why it is that the law codes in the Bible may

22. "For whoever keeps the whole law yet stumbles in one point, he has become
guilty of all.”

93. The clearest passages of Scripture showing God’s marriage to Isracl are
Ezekiel chapter 16.and Hosea chapter 2. See Appendix F.

24. On the nature of the 3rd Commandment, see Appendix B.

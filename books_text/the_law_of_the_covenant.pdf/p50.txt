pe

The Law and the Redemption. of Israel 314

that socio-religious order.?

The History of the Seed People

When God created man, He put him into a model environ-
ment called the garden of Eden. The essential characteristic of
this garden was the presence of God, and His presence was what
brought the model environment to pass. When man rebelled
against God, he was driven out of this model environment into
the wilderness. God, however, promised to raise up from the seed
of humanity a Savior Who would crush the adversary and restore
God’s people to fellowship with Him. That restoration to
fellowship would naturally entail as a consequence some kind of
restoration of the Eden-type of environment.

God placed enmity (hatred) between His chosen people and
His enemies. Humanity was divided between those whom God
would save and those who would be left to go their own way.
Right away, this hatred. broke out into oppression and murder.
Under pressure, Cain revealed his true nature and murdered his
brother. God made it plain at the outset that there was to be a war
to the death, a point too often forgotten by modern Christians.

The evil in the world deepened and matured until God
destroyed humanity in the Flood, beginning again with a second
Adam, Noah. After a couple of centuries, the truth of God that
Noah had passed to his descendents began to be eclipsed again by
the rising tide of evil. God then called Abram and his household

3. We must distinguish between the time when God reveals something and the
time at which that revelation is recorded in writing, Not only is this the case with
the Law, it is also the case wilh Wisdom literature. The Wisdom
literature —Proverbs, the Song of Solomon (Canticles), and Ecclesiastes —was
written and recorded during the reign of Solomon for the most part. This makes
sense, for Solomon had asked for wisdom and had been granted it. ‘Thus, he
became an “incarnation” of wisdom, and a precursor-type of Christ, the incar-
nate Word-Wisdom of God, It was only fitting, then, that he should be the
preeminent recorder of Wisdom literature. This does not mean that Solomon
wrote all the Proverbs attributed to hi; rather, he collected them under Divine
guidance, They doubiless had been developing and circulating in Isracl for a
long time, Proverbs 17:23 is found in Exodus 23:8 and Deuteronomy 16:19, for
mstance.

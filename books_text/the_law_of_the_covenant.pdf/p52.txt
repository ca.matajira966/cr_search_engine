The Law and the Redemption of Israel 33

to flee (Gen, 31).5 The blessings to the Israelites in Egypt also
brought down on them the envious wrath of the wicked, as we
shall see.

It is significant that Laban’s treatmient of Jacob parallels in
certain respects Pharaoh’s treatment of the Hebrews. Although
Laban inztially welcamed Jacob, there came to be a change in Laban’s
attitude which resulted in Jacob’s being reduced to an inferior posi-
tion, bordering on slavery, This change may be recorded in
Genesis 29:15, “Then Laban said to Jacob, ‘Are you my brother,
and should you therefore serve me for nothing? Tell me, what
shall your wages be?’” A family member would not have worked
for wages.® After earning his wives, Jacob labored six additional
years (31:41), the period of slave service (Ex. 21:1). Jacob was op-
pressed (Gen, 31:39f.). God saw his affliction (31:12, 42), just as He
later saw affliction of the Hebrews in Egypt (Ex, 3:7), In violation
of custom (Dt. 15:12-15), Laban would have sent Jacob away
empty-handed (Gen. 31:42). Even though Jacob had earned Leah
and Rachel, Laban acted as though they were slave-wives given

5, We need to distinguish among envy, covetousness, and jealousy.
Covetousness takes the attitude: “He has it. I want it, I will take steps to get it away
from him." Envy, on the other hand, is not grasping, but destructive. It says: “He
has it. I want it, l know I can never have it. Therefore, I will take steps to sce to it
that nobody enjoys it.” On envy, see Helmut Sehoeck, Enuy: A Theory of Social Be-
havior (New York: Harcourt, Brace, [966] 1970). While Schoeck’s distinction be-
tween covetousness and envy is not made in Scripture, it definitely brings aut
some Biblical ideas.

Jealousy is a virtue, an attribute of God (Ex. 34:14). It is not other-directed,
but self-directed. Jealousy says: “What’s mine is mine, Don’t feol with it without
my permission,” A man is properly jealous of his wife, and angry if others solicit
her attentions, God is properly jealous of His rights and worship, and His
jealousy protcets His wife (the church) from the attacks of Satan, Human
jealousy is never to be absulute, because “what's mine is mine” is only true under
God, When men play God, the virtue of jealousy is corrupted by selfishness and
egoism.

Tn modern Fnglish, jealousy’ is sometimes -used as a synonym for
‘covelousness’ and/or ‘envy! This unfortunate confusion of meanings means that
the reader must always try to ascertain how a given writer is using these terms.
This fontnote identifies how [ am using these terms,

6. Defense of this interpretation is well set out ky David Daube and Reuven
Yaron, “Jacob’s Reception by Laban,” Joumal of Semitic Studies 1 (1956): 60-61.

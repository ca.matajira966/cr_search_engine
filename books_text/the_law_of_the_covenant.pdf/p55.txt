36 The Law of the Covenant

separated from paganism by three factors: geography, occupation,
and enmity. Persecuted by Egypt, they had reason to hate Egypt’s
gods. Even so, we are told that they worshipped them (Josh.
24:14), The threat of their mixing with the heathen was real, and
the time for deliverance had come.

Redemption and Vengeance

The Hebrew term translated “to redeem” actually means “to
act as kinsman, to redeem, to avenge.” The participle form of this
verb, which is brought over into English as goel (“GO-el”),
becomes the noun, and thus means “kinsman redeemer-avenger.”
The duties of the goel were four. First, he was to redeem (buy
back) any inalienable property sold by his kin (Lev. 25:25). This
law applied to the property assigned to the family at the original
division of the land of Ganaan among the Israelites. Second, the
goel was to redeem the gerson of his kin if he sold himself into
slavery to a foreigner (Lev. 25:47#.). Third, the goel was to act as
a proxy for his deceased kin in receiving any restitution owed him
(Num. 5:8). Fourth, the goel was to avenge the blood of his kin if
he was murdered,

Redeemer and avenger are one and the same person. \t follows that if
the Lorn is Israel's Redeemer, He is also their Avenger. In one
important particular, the Divine Goel differs from His human
copy (or analogue). The human goel redeems the person of his kin
by paying a sum and buying him out of slavery. God, however,
pays no one, since He is sovereign. God redeerns by means of
vengeance poured put upon the slave-holder. When God acts to
redeem His people, we always see alongside of this an act of Div-
ime vengeance directed against their oppressors. In redeeming
Noah, God destroyed the wicked. In redeeming Israel, God
destroyed Egypt. In redeeming the world through Jesus Christ,
God poured out vengeance on Jerusalem, which had become His
enemy and the enemy of His people. As Christ moved toward the
cross, this act of vengeance was on His mind even as was the act
of redemption He was to effect. He spoke of the destruction of
Jerusalem just a couple of days before His death (Matt. 24, Mark

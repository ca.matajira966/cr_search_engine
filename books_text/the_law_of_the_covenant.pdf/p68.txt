The Book of the Covenant: Tis Context 49

thing to keep in mind is that the Old Covenant laws describe and
prescribe the tasks of man in the first creation, in the world of sin.
We still live in the first creation, in the world of sin, and with the
same tasks. For this reason, these laws, insofar as they bear on the
Adamic task, are addressed to us. They will continue to be neces-
sary for us until this creation is transfigured into the New Earth,
and we acquire our resurrection bodies.

Israel got its power for living from the provisional grace of
God, while we get our power from the definitive arrival of the
Spirit on Pentecost. Israel got its rationale for faithfulness from
the events of the exodus, while our foundation is in the work of
Jesus Christ. Israel was to obey out of gratitude for their
deliverance by God from Egypt, while we obey out of gratitude
for our deliverance in Christ Jesus from the slave market of sin.
The Adamic task, however, is common to both Israel and us.

To summarize, we can see that these laws continue to have
relevance in the New Covenant era from at least two factors.
First, the law has undergone a death and resurrection in Christ,
so that it still has relevance in the New Creation of the Church.
Second, since we are not yet fully in the New Creation, the very
particulars of the law, insofar as they address the Old Creation
situation, are directly relevant to us. Adam was to extend the
Garden over the whole earth, following the four rivers to the four
corners of the earth. So was Israel. So are we.

Adam was to create a God-honoring civilization, with worship
of the true God at its heart. So was Israel. So are we.

Adam was to love righteousness, and guard Eden from the inva-
sion of evil (Gen. 2:15, “keep” is “guard”). So was Israel. So are we.

Adam. was to punish evil. So was Israel. So are we.

The law of God, as found in the Old Covenant books,
describes the Adamic task, and prescribes how it is to be carried
out. In no way has this aspect of the covenant changed. What has
changed is the administration of the covenant, and its source of
power. The basic standards have not changed.*

3. Of course, the applications do change with circumstances. See the discus-
sion in Chapter 1.

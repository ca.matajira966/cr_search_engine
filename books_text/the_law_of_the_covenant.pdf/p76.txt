The Book of the Covenant: Its Context 57

unintentional spillage of seed (Lev. 15:18).!2

And so, God drew near on the third day (alter God’s an-
nouncement to Moses on the fourth day), which was the sixth day
of the week, to renew covenant with men. It was not the New
Covenant that God was renewing at Sinai, but the Old Adamic
Covenant. It was the Old Covenant temporarily and provisionally
reestablished in the sphere of temporary, provisional, ceremonial
(New Covenant) resurrection. It was temporary; but just as the
original Adamic Covenant had pointed forward to sabbath rest,
so the renewed Adamic Covenant at Sinai pointed forward to the
work of Christ and the New Future Sabbath Covenant to come.

It was the third day, and the third month (19:1, 16). For the
significance of this we need to look at Numbers 19:11-12, The man
who is unclean from contact with a corpse is to be cleansed on the
third day and again on the seventh day. This double resurrection pat-
tern is found all through the Scriptures. For instance, in John
5:21-29, Jesus distinguishes a first resurrection, when those dead
in sin will hear the voice of Christ and live (v. 25); and a second
resurrection, when those dead in the grave will come forth ta a
physical resurrection (v. 29). The jirst resurrection comes in the mid-
dle of history to enable men to fulfill the duties of the old creation.
The second resurrection comes at the end of history to usher men into
the new creation.

Jesus was raised on the third day, thereby inaugurating the
New Covenant in the midst of the week of history, Christians live
between the third and seventh days of history, Spiritually resur-
rected and in the New Covenant, but physically mortal and
assigned to complete the tasks of the Old Adamic Covenant. The
fact that the law was given at Sinai on the third day, and in the
third month, was a provisional anticipation of the third-day resur-

12. © "fa man lies with a woman so that there is a seminal emission, they shall
both bathe in water and be unclean until evening?” That is, unl the evening
sacrifice, so that their cerernonial death is removed by the death of the sacrifice.
This command may also have been intended to highlight the consummation of
the Lorp’s marriage to Israel (cf. Ezk. 16:8).

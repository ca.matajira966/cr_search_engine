The Ordinances: Structure and Characteristics 63

correlation with the fifth commandment for verses 12-17.) The
word for “kill” in this commandment is not the word for murder,
but has more to do with offering violence. Thus, Jesus expounds
the law in terms of verbal violence and unrighteous anger (Matt.
5:24f.). Moreover, as we shall see in Chapter 6, these laws are ar-
ranged in a descending order of severity in terms of punishments. —

Ill. Laws Concerning Property and Stewardship (22-1-15)

A, Theft (22:1-4)

B. Pollution (22:5-6)

C. Safekeeping (22:7-13)

D. Borrowing and Rent (22:14-15)

It is not clear why we skip to the eighth commandment here,
before going to the seventh. Most likely it is because the laws
against violence are organized in such a way as to lead into laws
against theft. This demonstrates the overlap between these two
root norms. (See Appendix B on the overlap between the seventh
and eighth commandments in Deuteronomy.)

IV. Laws Concerning Marriage and Faithfulness (22:16-31)

A. Seduction (22:16-17)

B. Spiritual adultery (22:18-20)

C. Mistreatment of God’s bride (22:21-27)

D. Respect for God as Divine Husband (22:28-31)

This might seem just a bit arbitrary, but the language of mar-
riage is found throughout the passage, and it seems to fit better
than any other category (see Chapter 8). The widow and
fatherless have God as their Husband and Father (v. 22). If they
cry out to Him, He will destroy the family of their oppressor {v.

7. Verse 16 does not begin with “and,” indicating a new section. The con-
catenation of “and” is not, however, maintained in this section, or in the re-
mainder of the covenant code. Kaiser, idem., calls this section “cases involving so-
ciety I can agree with that, but I think that it is precisely society as the bride of
YHWH that is addressed, so that the seventh commandment js the overarching
principle.

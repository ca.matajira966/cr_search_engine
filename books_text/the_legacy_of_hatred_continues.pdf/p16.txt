4 The Legacy of Hatred Continues

Christian Reconstruction where the charge of “anti-Semitism”? is an-
swered in full. Neither will the reader find reference to James B.
Jordan’s article “Christian Zionism and Messianic Judaism” in
The Sociology of the Church. Lindsey’s chapter on the law (“Israel in
the Present: Rejected,” Chapter 7 in The Road ta Holocaust) is loaded
with misrepresentations, the worst I have ever read. His portrayal
of Christian Reconstructionists as “The Modern-Day ‘Pharisees’ ”
who “mix Law and Gospel” is reprehensible. An honest study of
Rushdoony’s Institutes of Biblical Law (Vol. 1) and Greg Babnsen’s
Theonomy in Christian Ethics and his By This Standard will thor-
oughly refute Lindsey's caricature of what he maintains Christian
Reconstructionists believe. Reconstructionists do not believe that
man is saved by keeping the law.

Can Lindsey Be Trusted?

While such inaccuracies do not necessarily nullify Lindsey's
critique of Christian Reconstruction, they ought to make one
question how accurate Lindsey will be in interpreting what Chris-
tian Reconstructionists are saying. If he can’t get easily docu-
mented facts correct, readers have a right to question Lindsey’s
interpretation of data he gathered for The Road to Holocaust as it
relates to the “anti-Semitism” charge and other charges not dealt
with in this short book.

The Limited Design of This Book

The Legacy of Hatred Continues is an attempt to deal with Lindsey’s
suggestion that anyone who is not a dispensational premillennialist
is “anti-Semitic.” Lindsey has chosen Christian Reconstruction as
his whipping boy. How well did he do his research? Bantam Books
tells us that Lindsey’s opinions were “derived from his exhaustive

 

7. “Anti-Semitism” is in quotation marks because the word has different mean-
ings for diferent groups. Jews and Arabs are Semites. They have common
ancestors (Shem and Abraham). We are using “Anti-Semitism” as it is commonly
used today: Anti-Jewish attitudes or actions, We believe this is the way Hal Lindsey
wants his readers to understand the term.

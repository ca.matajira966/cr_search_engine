8 The Legacy of Hatred Continues

Christians should not shrink from debating other Christians.
Efforts to remove error from the Church of Jesus Christ is a life-
long process. We should always be pursuing “sound doctrine”
(i Tim. 4:6; Titus 1:9). Contemporary Christians who disdain de-
bate are not familiar with New Testament Christianity or the early
Church apologists and councils which gave us the great creedal
formulations of Nicea (a.p. 325) and Chalcedon (a.p. 451), or the
later confessions of Westminster (1643-47) and Savoy (1658).

But there is a proper way to debate: (1) Represent your oppo-
sition’s position accurately; (2) tell the truth about your own posi-
tion; (3) present the facts of both Scripture and history reliably;
(4) do this all to the best of your ability.

Hal Lindsey and Truth

Hal Lindsey has been a popular author since the publication
of his first book, The Late Great Planet Earth, selling over 25 million
copies in 100 printings.!! While Lindsey’s other books have sold
well, they have not had the impact of his first blockbuster. It was
The Late Great Planet Earth that made Hal Lindsey the Jeane Dixon
of dispensationalism: He predicted the year of the rapture of the
church! Like Jeane Dixon, Lindsey was wrong.

Lindsey’s latest book is The Road to Holocaust. With highly in-
flammatory cover copy, Hal Lindsey defames and maligns Bible-
believing Christians who happen to disagree with him on a number
of biblical doctrines, in particular eschatology and Christian attitudes
toward Jews. And he uses a New Age and Occult publisher to do
it.!2 Why do we think The Road to Holocaust needs to be answered?

« Lindsey is a popular author many people trust, When
Lindsey speaks, unfortunately, millions of Christians listen.

11, Gary Friesen, “A Return Visit,” Moody Monthly (May 1988), p. 30.

12. Here are some Bantam Book titles: Dance of the Spirit: The Seven Steps of
Women’s Spirituality, Healing Visualizations: Creating Health Through Imagery, Quantam
Healing: Exploring the Frontiers of Mind/Body Medicine, Case for Reincarnation, Channel-
ing, Comparative Crystal Guidebook, Natural ESP. If you would like a catalog from
Bantam, you can write to them at 666 Fifth Ave., New York, New York 10103.

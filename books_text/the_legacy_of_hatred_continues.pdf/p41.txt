Dispensationalism’s “Future Holocaust” 29

The Parenthesis

According to dispensationalism, Israel has no prophetic signifi-
cance in God's program until the church is raptured prior to the seven-year
tribulation period (Daniel’s 70th week). This is the dispensational
view as espoused by E. Schuyler English:

An intercalary period of history, after Christ’s death and res-
urrection and the destruction of Jerusalem in a.p. 70, has inter-
vened. This is the present age, the Church age. . . , During this
time God has not been dealing with Israel nationally, for they have been
blinded concerning God's mercy in Christ... . However, God will
again deal with Israel as a nation. This will be in Daniel's seven-
tieth week, a seven-year period yet to come.”

According to dispensationalism, God is now dealing with His
church, His “heavenly people.” God is no, according to dispensa-
tionalism, dealing with Israel, His “earthly people.” The promises
made to Israel are “postponed.” Technically speaking, with this
perverse dispensational view in mind, there can be no such thing
as “anti-Semitism” as Lindsey describes it! The Jews are like
everybody else: They are lost in their sins until they embrace
Christ as their Lord and Savior. “Anti-Semitism,” according to the
dispensational view, is no different from anti-Japanese, anti-
Italian, anti-Arab, anti-Irish, or anti-German attitudes. Jews are
not God’s chosen people this side of the rapture. This ts the dispen-
sational view!

Consider this as well. If the promises to Israel as a people and
nation are postponed, as dispensationalism teaches, then the land
promise, and the promise “those who bless you, I will bless,” also
have been set aside, until the prophetic clock begins to tick with
the rapture of the church. Treating Jews with special care or
persecuting them will impress God in no special way. He is not
obligated to keep a promise that has been postponed. Hai Lindsey
wants to have it both ways: Israel’s not significant until after the

7. E. Schuyler English, A Companion to the New Scofield Reference Bible (New
York: Oxford University Press, 1972), p. 135. Emphasis added.

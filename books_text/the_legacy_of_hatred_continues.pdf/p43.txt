Dispensationalism’s “Future Holocaust” 31

The most important sign in Matthew has to be the restora-
tion of the Jews to the land in the rebirth of Israel. Even the fig-
ure of speech “fig tree” has been a historic symbol of national
Israel. When the Jewish people, after nearly 2,000 years of exile,
under relentless persecution, became a nation again on 14 May
1948 the “fig tree” put forth its first leaves.

“Jesus said that this would indicate that He was “at the door,”
ready to return. Then He said, “Truly I say to you, éhis generation
will not pass away until all these things take place” (Matthew
24:34, NASB).

What generation? Obviously, in context, the generation that
would see the signs—chief among them the rebirth of Israel. A
generation in the Bible is something like forty years. If this is a
correct deduction, then within forty years or so of 1948, all these
things could take place. Many scholars who have studied Bible
prophecy all their lives believe that this is so."!

With this new version of dispensationalism, Hal Lindsey
broke with standard dispensationalism on the regathering of
Israel to the land as an indicator of prophetic time.!? He had to
make Israel’s regathering to the land a fulfillment of Old Testament
prophecy in order to sell his belief in an imminent rapture. But as
fellow-dispensationalist Thomas Ice tells us: “Dispensationalism
has always affirmed that the signs of the times, the ‘prophecy
clock,’ would not resume ticking ‘until after the rapture of the
church, Therefore, no one could possibly predict the rapture on
the basis of events taking place in the current church age because
there are no signs relating to the rapture, The fruit of date-setting
and many contemporary errors has not been gathered from the
root called dispensationalism.”!? But date-setting sells books, as
Hal Lindsey and Edgar Whisenant know.

In The Road to Holocaust, Lindsey has used the same perverse

Ii. Hal Lindsey, The Late Great Planet Earth (Grand Rapids, MI: Zondervan,
1970), pp. 53-54.

12. Charles L. Feinberg, “The State of Israel,” Bibliotheca Sacra (October,
1955), p. 319

13. Thomas D. Ice, “Dispensationalism, Date-Setting and Distortion,” Biblical
Perspectives (September/October, 1988), p. 1. Emphasis added.

PUBLISHER'S FOREWORD
by Gary North

Even a fool, when he holdeth his peace, is counted wise: and he that
Shuueth his lips is esteemed a man of understanding,

Proverbs 17:28

“Bantam doesn't know one thing from another when it comes to Chris-
fianity.”
Hal Lindsey!

Hal Lindsey made a terrible mistake. He wrote The Road to
Holocaust (1989). Bantam Books also made a terrible mistake.
They published it, Gary DeMar and Peter Leithart show in this
little book just how big a mistake these people made. I believe the
correct term is humongous.

No one made Mr. Lindsey write it, but now that he has done
so, his free ride ends. He will finally learn what criticism from
competent theologians is all about. It will not be a pleasant experi-
ence for him; people at his age seldom are given public thrash-
ings. You are about to read one.

For twenty years, Mr. Lindsey had received a nearly free ride,
intellectually speaking. Scholars within the Christian community
have paid almost zero attention to his books. You cannot easily
find references to his writings in the footnotes of dispensational
theologians, let alone non-dispensationalists. Mr, Lindsey was

1. Hal Lindsey ‘Lape Ministries, audiotape #217, “The Dominion ‘Theology
Heresy” (4987).

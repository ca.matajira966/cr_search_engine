THE CHURCH IN AN AGE OF DEMOCRACY 97

predestinated to be created before he was created (Eph.
1ff.).?

Later in history, men started to look more to what they
were “good at.” This too, proves predestination because God
put that “natural” ability in to begin with.® But man in all of
his fallenness attempts to find definition in his essence apart
from a God and calling which transcend himself. He would
like to recreate society in his own image. An expression of this
is the non-differentiated society of totalitarianism where men
and women dress and look alike.® In the movie THX-138,
one sees a 21st century world under the ground. The people
only differ functionally, and are kept on drugs to prevent se/f
expression and assertion. The leading character of the movie,
Robert Duvall, eventually escapes to the overworld of free-
dom and the ability freely to be himself by transcending (in a
Biblical sense) beyond function. Only the world above can
provide new meaning and definition, Nevertheless, the world
of totalitarianism removes created and recreated differentia-
tions. Christianity teaches that regardless of “natural” ability,
God calls men to do certain tasks. The genius of Reformation
theology was that it pulled the Western world back to an un-
derstanding that calling comes from God.

7, This is the order of Decrees, but the point is that God planned before
He created, His plan provides the definition to the universe by which it
must operate. Thus, He is the one who must change that definition,
Redemption, however, is God's calfing man back to his original definition in
the garden. See L. Berkhof, Systematic Theology (Grand Rapids: Eerdmans,
1939), pp. 100-108.

8. This is why consistent Marxists object to the concept of natural ability,
An example of this is found in a pamphlet un the POWs of the Korean War.
Communists would allow our soldiers to play baseball, but their positions
could not be selected on “natural ability” because that would prove predesti-
nation. Marxists understand better than most evangelical Christians, who
are mostly Arminian, the implications of “natural ability.” See Major
William E. Mayer, Communist Indoctrination— Tis Significance To Americans,
(The National Education Program), p. 20,

9. Diversity is created by God. Distinctions in creation are an expression
of this diversity. In a very important work, Psychological Seduction (Nashville:
‘Thomas Nelson Publishers, 1983), p. 144ff., William Kilpatrick discusses
the need for sacred/profane distinctions. He does not refer to a distinction ina
nature-grace-dichotomy sense. Rather, he speaks of the need to maintain
ethical distinctions. The concept of calling would fit into this category and
reinforce vocational distinctions.

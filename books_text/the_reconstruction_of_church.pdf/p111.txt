THE CHURCH IN AN AGE OF DEMOCRACY 99

spiritual gifts, in chapters 12-14 for example, is that the people
with leadership gifts should lead, and everyone else ought to
do what they are gifted or called to do.

I, The Definitive Church

While we are on the subject of calling, another govern-
mental concept is tied to the use of this idea. Significantly, the
Greek phrase for “called saints” (1:2) is often found in the Sep-
tuagint, where it is used to translate the Hebrew phrase ‘holy
convocation” (Lev, 23:4). Allowing the parameters of Hebrew
Scripture to inform our understanding of the Greck New Tes-
tament, as did the original writers and readers, “called saints”
takes on added meaning.

The “holy convocation” of the Old Testament was a num-
bered body. It was not just anyone who stumbled into Israel. In
fact, the Old Testament saints were very precise in their cen-
sus about who was and was not in their camp. Their member-
ship was definitive.

In the modern Church, there is much discussion about
whether a Church should have a membership roll, The Bibli-
cal concept, “called saints,” should forever put the issue to
rest, The Church should have a roll. The rationale is obvious,
but we have more to argue from than just the logical deduc-
tion that there can effectively be no discipline if a membership
roll does not exist. Therefore, as the “holy convocation’ of the
Old Testament was definitive, so is the New Covenant
Church.

Building on the “called saints” concept, as a matter of fact,
one finds reference to several lists of membership in Scripture.
The Book of Revelation mentions a roll (Rev, 13:8; 17:8), and
the Epistles have lists of names at the end of them. Granted,
these lists were not designed to be membership rolls, but that
is what they effectively became. For, when the Apostle/authors
mentioned someone as being in a local Church, that person
could not claim membership in some nebulous universal
Church, as is done today, to avoid discipline.

HE, The Obligatory Church

Continuing to build on the concept of a called group of peo-
ple, we can also conclude that the Church is not a voluntary

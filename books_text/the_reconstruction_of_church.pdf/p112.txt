100 CHRISTIANITY AND CIVILIZATION

organization in the strictest sense of the word. The notion of
being called implies the question, “Who did the calling?” An.
swer: God did. This call, like the call of creation into exist.
ence, cannot be resisted. The Church, therefore, is not a val-
untary organization. Membership is mandatory, and the life
which God requires of His saints is obligatory.

In the present multi-denominational society, many pro-
fessing Christians believe that Church membership is not re-
quired. This is a denial of their own profession whether they
realize it or not. By the way, this is not to say that one is not a
Christian if he is outside the institutional Church. But it does
mean that no effective Church discipline can be implemented
on him. Normaily, a Christian will enter under the roll of a
local Church where he is more likely to be accountable. Not
all Churches with membership rolls exercise discipline. But
one thing is fairly certain. One cannot be excornmunicated
from that of which he is not a definitive member. So, without
the obligation of Church membership, the local Church
quickly degenerates into a social club.

IV, Theology of Place

Paul refers to the “Church in Corinth” in the introduction.
This designation speaks volumes to a modern protestant
Church. But one cannot catch the relevance of Paul’s com-
ment without seeing the theology of place in Scripture.

Man was made out of dust, and has continuity with the
land. Whatever he does, it has some relationship to the
ground. The mandate given to him was specifically to culti-
vate the ground and have dominion over all the animals that
lived off the land. The designated sphere of man’s dwelling
was the garden. God created the garden as the first place where
man met God and received his mate, and it was to be expanded
to cover the whole earth. Everything about man was tied to a
specific place.

After the fall, one of the aspects of the blessing of the cove-
nant was place, Abraham was blessed with the promise that he
would inherit land (Gen. 12:1-2), and this land is identified as
covenantal ground. Throughout the Old Testament the peo-
ple of God look forward to inheriting and maintaining the
promised land because this is one indicator that the covenant
is not broken.

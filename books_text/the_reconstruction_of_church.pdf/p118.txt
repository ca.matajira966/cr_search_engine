i106 CHRISTIANITY AND CIVILIZATION

unusual abilities, normally in the Olyrnpic Games, he was
considered above nature and invested with more of the being
of God. His reward was elevation above others,

In Christian ministry, Church members often adopt this
pagan model, Leaders are understood to be personally closer
to God (to possess more of the being of God), and end up be-
ing virtually worshipped.1? The principle of plurality of
leadership is a check and balance against this one-man-show
deification of leadership. Nevertheless, man loves to worship
himself via one heroic leader. The Corinthians were rallying
around select individuals, and undermining the work of a team
ministry. How ironic—a democratic society, filled with mythi-
cal visions of equality, was quick to create partisan side-shows
along the lines of a Greek model of ministry.

Unfortunately, modern discipleship programs use this
same Eastern and/or Greek model of discipling. The Chris-
tian method is Trinitarian expressing a one and many influ-
ence on the disciple. In practical terms, this means that one is
discipled by the group as much as the individual. When either
the individual or the group takes on a more predomimmant role,
imbalance results. The disciple either fails to realize the influ-
ence he can have as an individual, or he becomes pampered.

Discipleship programs have done an excellent job at show-
ing the importance of individual influence and involvement.
But, today, our Churches are filled with “pampered” individu-
als who are the product of too much direct attention. They
float from Church to Church trying to find someone who will
give his/her life for him. Only one has totally given His life for
the Church, and that was Jesus Christ. Perhaps this was not
the precise problem in Corinth, but there is point of contact
between one-man discipleship programs, ego~-centered
ministry, and the Greek model of leadership.

The corrective is (1) the correct doctrine of office. It is not
because leaders are closer to God, or possess more personal
holiness that they should be followed. It is because they have
been officially set apart by God and then the Church.

And, the other corrective is (2) plurality of leadership.
Christian leaders are supposed to be servant team-players.
They should be willing to take leadership, but see’ their re-

17. This phenomenon grew to be called the heresy of Donatism. We will
discuss this subject later.

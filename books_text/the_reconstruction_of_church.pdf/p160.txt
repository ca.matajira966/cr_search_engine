148 CHRISTIANITY AND CIVILIZATION

a quiet withdrawal can only be construed. as ‘sweeping sin under
the carpet.

c. Discipline, according to biblical revelation . . . is neces-
sary for the benefit of the offender because being followed by the
loving admonitions and prayers of the whole congregation, it
may lead him to repentance. Christ and the apostles clearly at-
tribute an efficacy or power to the church acts of discipline (Mat-
thew 18:18; { Corinthians 5:4-5). The failure to administer disci-
pline is equivalent to a tacit admission that there is no spiritual
power or authority in the act, but simply a breaking of outward
tes.

d. Excommunication forewarns of the future and final judg-
ment of God upon the unrepentant person, a judgment which
none can escape by quiet withdrawal. (This further serves to
deter others from sin.)

e. To allow a quiet withdrawal would be to seek peace
through compromise rather than obedience. This is a worthless
type of peace.

f. A church has a duty to other Christian churches not to
allow a person to leave its membership in apparently good stand-
ing when it is known that that person is living in sin. This might
not have been a problem in first-century Corinth, but it is a very
real one today. No Christian church has the right to forsake its
responsibilities to other Christian churches. If another church,
knowing that a certain person is under discipline, procceds to
receive that person into fellowship, their sin will be upon their
own heads. On the other hand, if one church allows an unrepen-
tant sinner to withdraw quietly, and then that person joins
another church, the first church (which failed to discipline) is
responsible for allowing the corruption of another church, when
it might have been prevented by the proper action of the first
church,'!

The words of the Cambridge Platform seem appropriate here:
“The church cannot make a member no member but by ex-
communication” (Ghapter XIII, Article 7).12

 

1, Daniel E. Wray, Biblical Church Discipline (Edinburgh: The Banner of
Truth Trust, 1978), pp. 14-15.

12. This, of course, does not pertain to letters of transfer or commenda-
tion which display the Scriptural ecumenicity of the Christian Church. We
mention this because of our awareness of some churches bearing Reformed
Creeds which actually excommunicate those who would transfer to other
churches!

It may also not apply to the selfexcommunicate who has formally
denied the Faith (let us say blatantly and self-consciously) and who, has
departed from the congregation. Such a person may come under this cen-
sure only insofar that the judicatory.excommunicates by excommunicative

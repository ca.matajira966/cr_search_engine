6 CHRISTIANITY AND CIVILIZATION

Church is important. They can hear good preaching over the
radio or television. They can read good Christian books, and
get guidance for life. Home Bible studies frequently provide
better fellowship, Often the Church seems kind of dead com-
pared to other Christian works. Thus, the institutional
Church seems relatively unimportant. We have to say that
this is the Ghurch’s own fault, for failing to make its purpose
clear to the people.

Tt is important for us to see briefly how this came about.
During the Middle Ages, because of a superstitious view of
the sacrament, people stopped partaking of it. They removed
it from their children, and stopped drinking the wine.' To-
ward the end of the Middle Ages, the Italo-papal nationaliza-
tion of the Church was pretty much complete, and the Church
was almost wholly corrupt in its ministry. Lay preachers,
monks, and other types of reformers conducted ministry out-
side the boundaries of the institutional Church, though they
always directed people to engage in formal command perfor-
mance worship on Sunday. At this time it was still understood
that public worship was as important as private worship.

With the Reformation, the preachers triumphed, and took
over the Churches. The political hold of the Italo-papal court
over the rest of Europe was broken. Since the Reformation
grew out of a preaching movement, it was natural for protes-
tants'to emphasize preaching in their worship services. At the
same time, people were not used to taking communion more
than once or. twice a year, if that often, Though the major Re-
formers, such as Luther, Calvin, and Bucer, greatly desired
weekly (even daily) communion, they were completely unable
to persuade the people to go along. Quarterly communion
was the most they could get.

As a result of all this, protestant people came to think of
preaching as the most important aspect of the institutional
Church. This was a mistake, because God has not given
many gifted orators to the Church. (St. Paul was ridiculed for
his lack of oratorical skill, and Moses had the same problem;

5, Itwas the laity, not the clergy, who rejected the cup, out of fear of spill-
ing it. Tt was also the laity whe stopped bringing their children for commu-
nion, for the same reason, Until the later Middle Ages, children were
welcomed at Jesus’ house for a weekly dinner with Him. All baptized chil-
dren, from infancy on, were present at the Table.

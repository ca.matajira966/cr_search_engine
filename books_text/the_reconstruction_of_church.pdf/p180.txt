168 GHRISTIANITY AND CIVILIZATION

Nathan: Sure. I don’t know what they mean, but there are
two special words they keep using in all their prayers.

Papa: What words?

Nathan: Well, the first one is “just.” They keep saying it.
“Lord we just thank you for just being so just special.” Stuff
like that. They must have it written down, because they ali do
it,

Papa: What's the other word?

Nathan: It’s not really a word. It’s a special sound, like a little
clucking noise: “Tsk.”

Papa: What?
Nathan: Tsk. Tsk.
Papa: What are you talking about?

Nathan: Listen. It goes like this: “Lord, tsk, we just, tsk, we
just, tsk, we want to, tsk, thank you, tsk, Lord, for, tsk, for
just, tsk, being just so, tsk, special, tsk.” Right?

Papa: OK, quiet down and listen to the special music.
Nathan: Wait. What’s that guy doing? He looks weird.
Papa: Shhh. He’s just singing.

Nathan: Yeah, but he’s shaking all over the place. He looks
like he’s going to fall dawn,

Papa: Well, that’s the way the “special music” singers do it in
this church, He’s just trying to rack to the beat.

Nathan: Why? It looks dumb.

Papa: Let's figure it out. Why do we have a choir in our
church? What do you think they’re doing there?

Nathan: I?’s part of our worship. They help us worship God.

Papa: OK. Now, why do you think this church has people
sing?

Nathan: Weill, I guess they're trying to worship too. But it
seems more like they're trying to look like they're on television.

Papa: Sort of like MTV?

 

 

 

 

 

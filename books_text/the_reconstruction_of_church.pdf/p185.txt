CONVERSATIONS WITH NATHAN 173

dumb. Do they have to wait till they're older to decide if they
want to obey their parents, too?

Papa: Not usually. But they want their children to wait until
they're ald enough to love God.

Nathan: But I love God. I always have. And the Bible says
that people can know Gad even when they're in their mama’s
tummy, doesn’t it?

Papa: Well, these people think you have to wait until you are
older and smarter, so that you understand what it’s all about.

Nathan: You mean you can’t have dinner with Jesus until you
understand what it means?

Papa; That's the idea.

Nathan: Papa, do grownups understand everything about
what Communion means?

Papa: Some people probably think they do.

Nathan: I dor’t think these people understand much about it.
If they did, they'd bring their children into the Covenant and
let them have dinner in heaven with them. And anyway, how
are the kids supposed to learn what it means without doing it?
That’s like trying to get nutrition from reading a recipe, in-
stead of eating the food!

Papa: Not bad. I'll have to remember that one.

Nathan: OK, so how can a kid get Communion in this
church?

Papa: Well, when he gets older—say, around twelve or so—he
asks Jesus into his heart.

Nathan; Papa, don’t be silly. This is serious.

Papa: I’m not being silly. They tell you to ask Jesus to come
into your heart.

Nathan: I’ve never heard that. Is that in the Bible?

Papa: No. But they think it is. It’s just an expression someone
made up that means becoming a Christian. They also call it
“receiving Christ,” which is a little more Biblical.

Nathan: But Jesus is in heaven. And we receive Him every

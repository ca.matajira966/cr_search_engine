 

THE LITURGICAL NATURE OF MAN 191

is the culmination of the worship meeting (Acts 20:11).

The Book of Revelation is one continual worship scene
because it takes place around the throne of heaven, In
general, the book begins with a confrontation of the sins of the
Church (Rev. 1-3). Then, through a process of antiphonal in-
teraction between heaven and earth, and the passing of judg-
ment through the proclamation of the Word of God, the Apos-
tle John sees the world offered up to God as a living sacrifice
(Rev. 4-19), Finally, the world is brought to the Eucharistic
table in the city of God (Rev. 20-22).1!

My observations are not new. The Historic Church,
although there is much variation within the broad structure I
have outlined, has seen the sacrificial structure of New Testa-
ment worship. The Mass of the Roman Catholic Church is set
up in this fashion. And, while rejecting the transubstantia-
tional view of the Mass, Lutheranism has carried over the
basic structure,

The Festival System

Turning to the sabbath system of the Old Testament, we
can see how it has been carried over into the New Testament
concept of worship. To make our points, let us back track fora
moment.

The feasts of the Old Testament were called sabbaths.
Everything grew out of and returned to them. In the spring,
the Old Covenant man began his year with Passover. In
autumn, he ended with the feast of Booths. Passover signaled
that re-creation begins with redemption. The feast of Taber-
nacles points to the end of history’s culminating with the Mes-
sianic reign. Both times of the year these festivals were ex-
tended sabbaths. One led to the other, and when one was
over, the faithful man eagerly awaited the other.

The yearly pattern was a magnification of the main tem-
poral unit, the week. A man worked six days to sabbath. He
sabbathed to work six days. Special sacrifices were doubled on
the sabbath because it was special. Holy Convocation, special

tl. Massey H. Shepherd, The Paschal Liturgy and the Apocalypse (Rich-
mond, Virginia: John Knox Press, 1960), pp. 77-84. Shepherd develops the
liturgical structure of the book in a much more thorough fashion. Although
his development differs stightly from the one I have laid out, it generally
confirms my point.

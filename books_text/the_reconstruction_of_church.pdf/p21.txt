THE CHURCH: AN OVERVIEW 9

the gospel was fully and faithfully preached. This, T humbly
apprehend,’ he adds, ‘is every gospel minister's indisputable
privilege.’ It mattered not whether the pastors who thus fully
and faithfully preached the gospel, were willing to consent to
the intrusion of the itinerant evangelist or not. ‘If pulpits
should be shut,’ he says, ‘blessed be God, the fields are open,
and I can go without the camp, bearing the Redeemer’s re-
proach. This I glory in; believing if I suffer for it, I suffer for
righteousness’ sake,’ If Whitefield had the right here claimed,
then of course Davenport had it, and so every fanatic and er-
rorist has it. This doctrine is entirely inconsistent with what
the Bible teaches of the nature of the pastoral relation, and
with every form of ecclesiastical government, episcopal, pres-
byterian, or congregational, Whatever plausible pretences
may be urged in its favor, it has never been acted upon with-
out producing the greatest practical evils.”10

Thus, the Great Awakening went far toward breaking
down the historic connection between the wholistic ministry
of the jocal Church and the preaching of the gospel.!! Subse~
quent revivals have only worked to further the disaster. Piety
came to be seen exclusively in individualistic terms— individ-
ual souls responding to the ministry of the preacher —and cor-
porate piety as the public performance of worship visibly on
the earth before the throne of God for His glory, was increas-
ingly lost from view.

The Growth of Parachurch Organizations

The second factor in the disarray of the Church today is
the growth of independent Christian organizations, called
“parachurch” organizations. Here I am talking about radio
and television ministries, evangelistic associations, and inde-
pendent theological seminaries. All of these parachurch
groups do good work, but they do it outside the context of the
Church and sacramental worship. The result is twofold. First,
an impression is created that the Church is unnecessary, or at

10, Jbid., 11:98.

11. For extended discussions of the Great Awakening, by scholars of a
Calvinistic persuasion who have some sympathy for its context, see C.
Gregg Singer, A Theological Interpretation of American History (Phillipsburg, NJ:
The Craig Press, 1964); and Rousas J. Rushdoony, This Independent Republic
(Phillipsburg, NJ: The Craig Press, 1964).

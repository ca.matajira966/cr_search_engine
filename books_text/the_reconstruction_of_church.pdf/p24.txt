i2 CHRISTIANITY AND CIVILIZATION

the servant priesthood.

An example of this special weight is seen in James 5:14-15:
“Is anyone among you sick? Let him call for the elders of the
Church, and let them pray over him, anointing him with oil
in the name of the Lord. And the prayer of faith will save the
sick, and the Lord wili raise him up. And if he has committed
sins, he will be forgiven.” Any Christian may visit the sick and
pray for him, but only elders are empowered to anoint with
oil. This is not medicine, for the Bible clearly distinguishes
between the ministry of physicians (such as Luke) and the
ministry of special Church officers. ‘The Christian who tries to
go it alone, outside the Church, is cutting himself off from the
possibility of special healing at the hands of the elders.

‘The particular power of the Church, however, is the power
of jurisdiction. This is the priestly power, the power to judge and
to make binding decisions on how the Church is to be run.
Ordinarily this power is exercised solely by ordained elders
(called in some churches presbyters, vestrymen, priests,
pastors, etc.). The elders exercise this power joinily, acting
together as a court. This is the governmental power.

God has distributed special powers to three institutions in
life, and has given a special symbol to each. To the parents is
given the power to inflict physical pain upon the children, for
their correction. The symbol of special (priestly, judgmental)
parental power is the rod (Proverbs 13:24, etc.). To the state is
given the power to put men to death for capital crimes, and to
punish men in lesser ways for lesser crimes. The symbol of
special state power is the sword (Romans 13:4). To the
Church is given the power to admit or restrain men from the
Table of the Lord. The special symbol of that power is the
sacraments.

Jesus said to Simon Peter, “And | also say to you that you
are Peter [a man of rock], and on this rock [the great rock of
the whole body of converted men] I will build My Church,
and the gates of Hades shail not prevail against it. And I will
give you the keys of the kingdom of heaven, and whatever you
bind on earth shall have been bound in heaven, and whatever
you loose on earth shall have been loosed in heaven.” The
Church is only to bind on earth what she knows has been
bound in heaven, How can she know? From studying the
Bible, and governing strictly in accords with its laws.

‘The power of the keys is the power of the sacraments. The sacra-

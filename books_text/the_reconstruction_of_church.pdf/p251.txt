CLOTHING AND CALLING 239

preciated my comments about clothing and calling. He
showed me his hands bearing the unmistakable marks of a
mechanic’s calling, and said, “After hearing what you had to
say, for the first time T am not ashamed of the grease under
my nails.” Puzzled ax that response, I asked for further ex-
planation.

He said that he understood from what I had said that
every man has a calling, and each call will have distinctive
clothing. The grease under his nails is part of his uniform. He
realized that he should not be ashamed of his calling or
uniform. I thought later that my collar, properly understood,
could do the same for other men. Jt has. Yet part of the
mythology about clerical clothing is that it plays down other
callings; that simply is not the case unless the Church teaches
the wrong reason for ministerial clothing.

Third, distinctive clothing which corresponds to calling
enhances opportunity for one’s profession, People talk about
cars to mechanics. They talk about ailments of their physical
body to doctors. They also will talk about their “soul” to
ministers. The problem is that most of the time, people do not
know who the ministers are unless they wear distinctive dress.

A while back, I was in England to establish a mission
Church for the Association of Reformation Churches. My
wife and I went sightseeing one day and ended up in the great
Canterbury Cathedral. Standing in the basement of this great
Cathedral I could see several priests standing beside rooms for
people who wanted counsel. A.young girl came up to one of
the priests, weeping. He began to speak to her.

T thought, “Wait a minute. I am an Elder and a Bishop in
Christ’s Church. This girl ought to be speaking to me rather
than this priest who probably does not even believe in the or-
thodox faith.” But then it dawned on me that this girl had no
way of knowing I was a.pastor. There I stood in my cowboy
boots and coat (pastors in Texas often wear boots).

Since that experience, I have started wearing distinctively
ministerial clothing, and I have had more opportunities than I
had ever imagined. Once when I was on my way to speak in
another state, a couple standing in line behind me initiated
conversation. They were extremely talkative, and wanted to
know all about the Church I pastored. Then the man said,
“We're on our way to a funeral. My brother-in-law just
dropped dead of a heart attack. And he was the same age as

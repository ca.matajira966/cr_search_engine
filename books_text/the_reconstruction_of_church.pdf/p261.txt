CHURCH MUSIC 1N GHAOS 249

texts in the Thnity Hymnal is at work here (orthodox Calvin-
ism), but there is quite a bit more liturgical sense in evidence.
There are, however, only about 175 hymns (not counting
psalms). Moreover, since the psalter is not really complete,
the church interested in serious psalm singing will have to
supplement the Psalter Hymnal with something else. Also, the
Psalter Hymnai does not have any responsive readings.

We discuss psalters below. Given the situation today, it is
not possible to have a complete psalter and a good hymnal in
one volume. Thus, the serious church will have to have two
books, one for psalms and one for hymns. Assuming that we
have a psalter, there is no point in buying a hymnal which has
ahigh proportion of psalms, because then the two overlap one
another.

Presbyterians should consider using one of the threc really
good hymnals which are available. The Episcopal Hymnal
(1940) is one. It contains 600 or so hymns, all of high quality
from all traditions except gospel songs. Like the Hymnbook,
there are a few (very few) liberal hymns, which are easy to
detect and avoid. It also contains quite a few canticles for
chanting the text straight from Scripture. It contains few
psalms (since Episcopalians chant these from other books),
and thus little overlap with whatever psalter the serious
church selects. The rule for amening hymus here is the same
as in the Service Book and Hymnal, mentioned above.

The Lutheran Service Book and Hymnal is the second hym-
nal which should be given serious consideration. It contains
about 600 hymns, no gospel songs, and reflects conservative,
orthodox Christianity throughout (except again for one or two
liberal hymns, easily avoided). It contains few psalms, since
these come from other books in the Lutheran tradition. Mus-
ically it is the best hymnal ever published, to my knowledge.
The LCA and ALC Lutherans have, in recent years, switched
to a new hymnal (inferior, and more liberal in tone), and fre-
quently you can buy used SB@Hs from such Lutheran
churches. Also, it can still be purchased from Fortress Press
(dial 1-800-FOR TRESS).

The Service Book and Hymnal also contains a compendium of
prayers, and a responsive reading psalter, set out according to
the traditional (Hebraic) pattern. The text is the AV. The
psalter is incomplete, but contains 113 psalms, almost all com-
plete (e.g., Ps. 137 leaves off the second half). (Compare this

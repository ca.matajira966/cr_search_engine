252 CHRISTIANITY AND CIVILIZATION

usable by a congregation, is simply not available. Given what
we have said in this essay, one would think that such a thing
would be a high priority in churches committed to the absolute
inerrancy and plenary authority of the Bible, but such is not the
case. Millions of dollars are spent by conservatives to produce
a simplified paraphrase of the holy Scriptures (the New Inter-
national Version}, which will be here today and gone tomor-
row, but no money can be raised to produce a good chanting
psalter, sa that our congregations can be transformed from.
gospel song wimps into militant psalm-chanting warriors,

So, since something is better than nothing, let us take a
look at what is available in the line of metrical psalmody. Here
we enter into another problem. Reformed and presbyterian
theologians rightly adhere to what is called the ‘regulative
principle of worship,” which means (in slogan form) that
whatever is not expressly commanded for worship in the New
Testament is forbidden. The Westminster Confession of Faith
is far more wise in saying that God may not be worshipped in
“any other way not prescribed in the Holy Scripture.” This
leaves open the possibility of consulting the Old Testament,
and it leaves open a discussion of what it means for the Scrip-
ture to “prescribe” something. For instance, on the basis of the
whole Scripture, one can make a case that a church calendar,
a Church Year, is “prescribed.”

Now, here is the rub: Certain Dutch Reformed theologi-
ans insist that it is not commanded that men should harmon-
ize the singing of the psalms. Thus, harmonizing is wrong.
All should join in unison before God. On the other hand, they
see that the Bible does advise the use of musical instruments,
and particularly the organ, Thus, in conservative Dutch
churches, the regulative principle means that the organ plays a
prelude before cach psalm, an interlude between cach stanza,
and a postlude at the end. The organist, if he or she is good,
can use a different harmony for each stanza. The people are to
sing in unison, and not harmonize. Thus, Dutch psalters only
give the melodic line.

Now, the Scottish and Puritan advocates of the regulative
principle found something else commanded in the sacred text.
They found that musical instruments are forbidden, since
nowhere in the New Testament are they found in use by the

 

‘
i
i
|
i
i
i
:
i
|
!

CHURCH MUSIC IN GHAOS 261

were reworked into hymns, but the following points need to
be kept in mind. First, Europe at that time had a genuine folk
culture and genuine folk music. To take such music into the
Church is quite different from taking over commercial dance-
hall and radio music from a culture like ours. Second, Euro-
pean folk music had been influenced by Church music for
1000 years by that time. Folk music returned the favor in giv-
ing good tunes back to the Church. Today’s popular music has
been cut off from Christian influence for a long time, and in-
deed is often radically demonized. The situation today is far
different from what it was then.3+

Fifth, and this is more important than the space I can take
for it here might indicate, evangelicalism and presbyterianism
have lost any real sense of the boundary between Church cul-
ture and general culture. The notion of a distinctive “Chur-
chy” worship building, of clergy wearing collars and robes,
and of distinctive worship music, has been lost in the per-
vasive stew of American democratic sentiment. This is unfor-
tunate, and has had incalculably destructive effects on the
Church at large. Church architecture should be different.
Church officers should wear uniforms. And Church music
should be different. This is not because general culture is bad,
or inferior; it is because general culture is general, and Church
culture is special. Psychological associations are important, and
are not neutral. This bears directly on the question of the pro-
priety of using either guitars or full orchestras in worship, and
it bears directly on the question of bringing over either “Chris-
tian radio” style music or concert music into formal worship.*?

In my opinion, this is the problem with modern “Scripture
song.” The music lacks profundity, strength, and greatness.
Thus, the music does not parallel the text, but tends to
weaken it. Moreover, stylistically the music is still the com-
mercial radio style, which is unworthy of the formal praise
sacrifices of public worship before the throne of the King of

31, Where did early Church chant come from? From the Temple and
synagogue. See Eric Werner, The Sacred Bridge (Columbia University Press,
1959). The paperback edition (Schocken, 1970) only contains the first half of
the book, on the wansfer of liturgical form from Israel to the Church. You
need to get the original hardcover edition in order to get the study of the
transfer of music and melodies.

32, On the subject of special and general, see my essay, “The Church: An
Overview,” in this symposium.

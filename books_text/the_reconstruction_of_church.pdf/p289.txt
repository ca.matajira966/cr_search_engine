CHURCH ARGHITECTURE 277

cause it is center stage for the incarnation of God, as The
Christ. In Genesis 1:14 we are told that the sun and moon
were created to be “signs” separating the day from the night,
marking the days and years, the “seasons,” moéd, or times of
holy convocation, the visitaiion of God, Moéd refers to a set
time when men are to gather themselves together as a solemn
assembly to worship before the special presence of God, as
Cain and Abel brought sacrifice at the appointed time. And as
one would expect, from ancient times to the present, men
have marked time or calibrated their clocks by the motions of
the heavens. Even modern men continue to order their lives
by the turning of the spheres, but have for the most part lost
any deep sense of its symbolic meaning. Men continue to re-
gard one day above another as holidays, but do not think in
terms of “holy days” because most of the biblical significance
has been removed or covered over by humanistic rebellion. In
a drought farmers in America would seed the clouds with
silver iodide, and then pray to God for rain; but they would
hardly think of the planting season beginning with a special
church service.

Among the ancients most lemple structures had a specific
astronomical orientation for telling the time of religious obser-
vance, which was always the starting point for any cultural
dominion. Marking, or we might say dividing time has
always carried with it a complex astronomical symbology uni-
versally expressed in antiquity; and although their origin and
meaning was often attributed to a pantheon of competing
gods, the over-all patterns remained the same, and were in-
corporated into almost every architectural structure of signifi-
cance. The twenty four hour day is divided into evening and
morning. Night is ruled by the moon and day by the sun, two
primary astronomical signs that find their historical meaning
in the redemptive plan of God, old covenant and new, one be-
ing the reflected glory of the other. I say these things now in
brief because I want the reader to begin thinking in symbolic
terms as the ancients did; and more particularly, as early
Christians did. The early church was extremely sensitive to
the symbolic significance of the sun as an image of Christ, and
the New Covenant.

Continuing, then, there are also twelve months in the
year, and twelve divisions of the sky, called “great houses” for
the constellations. Twelve is divisible by two, the number of

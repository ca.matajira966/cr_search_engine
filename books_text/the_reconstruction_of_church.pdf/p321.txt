THE RECLAMATION OF BIBLICAL CHARITY 309

from an emphasis on missions has impoverished the church
and has diminished the motivations of our congregations to
fulfll the Great Commission. But special meetings, confer-
ences, and seminars can help change all that.

Sixth, the deacons of the church must be mobilized for the
work of missions, Since their Scriptural task is almost ex-
clusively defined by the work of Biblical charity,3* they are a
natural starting place. Capture the hearts of the deacons, and
you've captured many a church. Encouraging deacons to read
books on Biblical charity, or having time set aside in each
deacons’ meeting to study the Scriptural injunctions concern-
ing the care of the poor, would go a long way to achieving that
end. Or, what about having a deacons’ retreat where a pastor,
an evangelist, or a Biblical charity pioneer can lead in an inten-
sive training session? Or, perhaps a series of Saturday morning
prayer-and-study breakfasts, where the issue of welfare and
poverty and the church's response can be discussed?

If the church is to be motivated to undertake the monu-
mental task of building alternative structures of Scriptural
compassion, the deacons’ support is critical. Don’t push,
Don’t shove. But, by all means, don’t bypass the deacons.

Seventh, the youth of the church must be enlisted in the
work of missions. Many of the great revivals the church has
experienced throughout history, and many of the great mis-
sions movements, began with the young. But, asicle from that
very obvious lesson, church history also teaches us that any
effort that ignores the youth is a short-lived effort, lasting only
one generation. That simply won't do in the case of Biblical
charity. Its complexity and magnitude requires us to think in
multi-generational terms.

The punch-and-cookies approach to youth ministry is a
tragic waste of time, money, and lives.5 Why not involve the
youth of the church in Biblical charity projects instead? Why
not orient the youth ministry to the service of others? Why not
channel the standard youth ministry fare of fund-raising, mis-
sions trips, fellowships, etc., into the fulfillment of the Good
Samaritan mandate? Why not unleash the creative and pro-
ductive labors of Christian kids on problems that really matter?

34, See Chapters 3 and 5 in Bringing In the Sheaves.
35. Sce the youth book for the Bringing In the Sheaves package entitled Faith
at Wark, also available from American Vision.

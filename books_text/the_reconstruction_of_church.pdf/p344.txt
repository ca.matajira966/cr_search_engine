332 CHRISTIANITY AND CIVILIZATION

done on earth as it is in heaven” (Matt. 6:10).

In looking at theology and in particular eschatology it is
evident that the Christian stands in the last time, somewhere
between the “at-handness” of the kingdom and the “not yet”
which will occur at Christ’s return in judgment, glory, and
consummation. In this last age the Christians are related to
the King of kings and Lord of lords as His disciples and citi-
zens of His kingdom. Therefore, Christians are both above
cultural loyalties and in submission to them: above them in
the sense that the Christian’s citizenship is in the kingdom of
heaven because the bonds of allegiance to the pagan culture
have been broken; and in submission to legitimate earthly
powers and systems, according to the commands of the Lord
(Romans 13:1-7), For the Christian the Scriptures interpret
culture and not the other way around.

There is a dynamic process to contextualization. Most of
those who are writing on this subject refer to three parts of this
dynamic process, For the sake of simplicity they are 1) text,
2) interpreter, and 3) context. Each writer may not use these
terms in the same way. For our “kingdom life-style” model I
have defined them as follows: Text is the inscripturated Word
of God. The interpreter is the Christian who is being used of
God to relate the biblical message to someone in another cul-
ture. The context is the pagan culture in which the new disciple
is seeking to make his new faith the normative factor in his
own life and also the total life of his own culture. The practice
or implementation of Christ’s kingship over the totality of life
is that which is of primary importance in contextualization.
“At the heart of evangelical contextualization will be praxis.”15
Praxis is the work of the Christian in his culture, not just
words. It is the work of changing society by bringing the mercy
of God to permeate all that the Christian does.

Text

Kingdorn life-style contextualization starts with making
the gospel “transcendental” from its cultural context. This
means that the gospel is to be the pure gospel that God has re-
vealed. It is to be raised above the cultural context of Galilee,
Samaria, and Judah. It is also to be raised above the cultural

15. Ibid., p. 2.

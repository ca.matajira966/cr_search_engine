36 CHRISTIANITY AND CIVILIZATION

abandoned the means to recovery. Only a return to explicit
obedience opens the mouth and creates the voice that the God
of covenant mercy hears. Let us not be surprised, then, that
the Lord has rejected many unfaithful local churches in our
day. He is under no obligation to renew the unfaithful.

The fundamental question remains: Shall we work toward
the renewal of this church? And the second question is like
unto it: Ifnot, what shall we do with it, and ifso, how shall we
renew it? No formula answer can be given to the first ques-
tion, but some guidelines for evaluation are possible. If the
troubled church has had a chronic performance problem, if its
leadership is uniformly incompetent, if its local fields are
black unto destruction (Rome has not been white in
centuries), then it probably needs to be razed to the ground.
Or better yet, sold to a newer congregation which has an ex-
cuse for not having filled the building.

On the other hand, if the problems are immediate, if the
leadership is committed and willing to sacrifice, willing to
change or be replaced, if the greater community is composed
of young families (without which the building of a stable local
church is impossible), then renewal is a realistic option.

Renewal by Re-Creaiion

The greatest hindrance to the creation of a thriving work,
all other things being equal, is demographics. Churches in older,
inaccessible areas have an uphill road even if all other areas of
weakness are dramatically improved. The church must move.
A pastor of an inner city church in its terminal stage asked
members of the congregation if, knowing the importance of its
distinctive teachings, they would be willing to travel an extra
ten or fifteen miles to attend services. They were overwhelm-
ingly unified in their unwillingness (unity at last). Why, then,
the pastor asked, should prospective members, not yet em-
bracing the doctrinal position of the church, be willing to
drive the extra miles in the other direction? If you, who know
the truth, are unwilling to go the extra miles, why should the
ignorant make the sacrifice?

A local church cannot be built on narrow strata of society.
The evangelical Arminian ideal of the teenage church is
untenable. The hopes of building a local congregation on
young singles, apartment dwellers, the childless, divorcees,

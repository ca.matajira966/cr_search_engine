CHURCH RENEWAL: THE REAL STORY 37

the elderly, and other minor strands of our social fabric, are
largely illusory. To be sure, all these are a part of the church,
but they cannot and do not provide a center for long-term sta-
bility and growth. If the troubled church insists on working
primarily with these people, it must understand that it is tak~
ing on a halfway-house mission project. It cannot expect to
grow stronger,

The church looking for new life must move. It must go where
the young families have gone—the outer suburbs of the city—not
to substandard housing projects scheduled for perpetual
transiency and poverty, but to new middle-class areas. The
church must recruit young families with children. Such fami-
lies provide a growing financial base and a growing group of
covenantal children so necessary to the future development of
the church and the lawful dominion of the greater community.

But relocation is not enough. The leadership must be changed.
Men who have consistently failed to lead the church must rec-
ognize that stepping down is the only hope. It will not do to
use the leadership positions of the church as training slots.
Paul lays down for Timothy and Titus (1 Tim. 3 and Titus 1)
the actual qualifications for actual elders, not the potential
qualifications for actual elders.

The idea of resignation is not popular in troubled
churches, especially among the leaders. But it is the price that
must be paid for healthy change. Providentially, the market-
place tells the businessman when he has failed by driving him
out of business. Church leaderships fail to read the ecclesiasti-
cal handwriting. They are like the businessman who refuses
to believe he has failed, and continues to pour good money
after bad. Often the real pain of bankruptcy is necessary to
get the point across. Perhaps church leaders ought to have
their entire financial futures tied up in the success or failure of
their churches. They would quickly make room for the com-
petent.

Can the entire board resign? Where does this leave the
church? If a congregation is serious about renewal, it ought to
be able to trust its pastor to make minor decisions {and even
some major ones) during the period that new leaders are be-
ing raised up. Pastors do have their personal and financial
futures bound up in the success of the church, and usually do
everything possible to promote its success. Godly pastors (and
you must have one of these) do not intend to “take over”

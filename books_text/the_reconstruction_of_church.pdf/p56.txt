44 CHRISTIANITY AND CIVILIZATION

The church needs more than a new face; it needs a new
heart, a new soul, a new mind, and a new strength. And it
will be hard for veteran supporters of desolate churches to ac-
cept the fact that the glory has departed. The younger mem-
bers of the church learn the ways of their elders or depart for
other congregations where the leadership is positive. The ded-
icated Christian is dynamic, not static. Without positive lead-
exship, the committed will not stay; without the committed,
the church expires, With the proper’ external and internal
changes the church can keep her young and prosper. Without
the necessary changes, the young see only hypocrisy in stag-
nation.

Diagnosis in the Dock

The indictment of the troubled church is merely an indict-
ment of the church in general. The problems that have be-
come acute in the decimated church are often present in germ
form in churches less suspect. Eventually the erosion of doc-
trinal standards and practices reflects itself in the worship and
work of the church. Demography and theology work together
for the ill of the church without solid biblical moorings.

History is filled with stories of successful men, written by
those men, purporting to reveal the secrets of their success.
Each. thinks he has discovered his own prosperity formula.
But the physician hesitates to diagnose his own illness, and
the lawyer says that the man who represents himself has a fool
for a client. Discerning the ills of the church is, likewise, rarely
the forte of its leadership. Self-reforrmmation by the entrenched
is a scarce commodity, and the leadership willing to take criti-
cism to heart and transmit it into action is, indeed, a diamond
in the rough. Ironically, leaderships of high quality are not
normally found in distressed works.

The church leader, not ithe church member, must be the
reformer, Whatever sort of board administers the affairs of the
church can change pastors. Recalcitrant members of the con-
gregation may be disciplined out. The leaders can change the
location of the church, they can even create a new one to re-
place it. But unless they themselves change or replace them-
selves with men who will commit their time, lives, and fortunes
to the work, little else of substance will be accomplished.

Pastors must wrestle with the central issue presented here:

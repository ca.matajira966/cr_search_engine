REVIVALISM AND AMERICAN PROTESTANTISM 53

eenth century. William Warren Sweet has identified three
phases of this revival: Presbyterian, Baptist, and Methodist.15
In its early stages the awakening generally retained the coven-
antal structure of Puritan Calvinism. In its later stages, the
inner logic of revivalism led to the individualistic theology and
piety of the Baptists and Methodists. The pattern of develop-
ment for later revivals, thus, can be recognized even in the
rather conservative context of the Great Awakening.
Preaching was the backbone of revivalistic Christianity.
The preachers of the Great Awakening, having been educated
in a system still informed by Puritanism, graphically depicted
the justice and wrath of God in their sermons. Theodore Frel-
inghuysen (1691-1748), a Dutch Reformed revivalist in New
Jersey, berated his listeners, calling them “impure swine,
adulterers and whoremongers,” and warned of “a fire hotter
than that of Sodom and Gomorrah to all that burn in their
lusts.”*6 Samuel Davies (1723-1761), a Presbyterian, asked his
congregation if they dared “go home this day with this addi-
tional guilt upon you, of disobeying a known command of the
supreme Lord of heaven and earth? . . . this day repent. If
you refuse to repent, let this conviction follow you home, and
perpetually haunt you, that you have this day . . . under pre-
tence of worshipping God, knowingly disobeyed the great
gospel-command.”!7 Jonathan Edwards's (1703-1758) famous
“Sinners in the Hands of an Angry God,” with its terrifying
image of man as a spider suspended by a thin web over a hell
of Hames, was not uncharacteristic.'® This is not to say that
the doctrines of salvation were ignored. For Edwards the pur-
pose of the revival was to restore the Reformation doctrine of

 

Great Awakening: Documents on the Revival of Religion, 1740-1745 (New York:
Atheneum, 1970) and J. M. Bumsted, The Great Awakening: The Beginnings of
Evangelical Pietism in America (Waltham, Mass.;: Blaisdell, 1970) are good col-
lections of original documents. Darrett B. Rutman’s compilation, The Great
Awakening: Event and Exegesis (New York: John Wiley and Sons, 1970) has the
added attraction of selections from major historians’ interpretations of the
era. Also available are numerous biographies of the leading revivalists and
several regional monographs.

15, Sweet, pp. 36-39.

16, Quoted in Jbid., p. 49.

47. Quoted in Jbid., p. 69.

18. Edwards, “Sinners in the Hands of an Angry Gad,” Anthology of
American Literature, ed. George M. McMichael (New York: Macmillan,
1974) 1:242-256.

REVIVALISM AND AMERICAN PROTESTANTISM 67

ratization. “History,” Beecher wrote, “teaches us that in all
past time the earth has been owned, and knowledge and
power have been monopolized, by the few.” Beecher was no
utopian. Freedom of opporunity alone would achieve a de-
mocratization of land.85 Nevertheless, the egalitarian nature
of his goal is noteworthy. Thus, in social, political, and eco-
nomic affairs, many revivalists, even rationalistic revivalists
like Beecher, spoke as democrats. Revivalism in the early
nineteenth century was well suited indeed to the era of Jack-
sonian Democracy.®*

Less radical clergymen contended themselves with speak-
ing on vaguely defined personal and domestic matters and
preaching the “simple” gospel. By the mid-nineteenth century,
Ann Douglas comments, the Protestant minister was the only
professional who did not have to master a body of knowl-
edge.” The anti-intellectualism and simplification of thealogy
that was implicit in the First Great Awakening developed
more fully in Finney. He had little use for seminary educa-
tion, claiming that young ministers graduated from seminary
“with hearts as hard as the college walls.” But his greatest com-
plaint was that seminary graduates did not know how to use
the knowledge they acquired. Their ministries were un-
productive. Despite his criticism of education, Finney was
sometimes accused of being an “intellectualist” and later
became a university president.8? On this issue, as on most
others, Finney stood midway between the rationalists and the
enthusiasts. The same could not be said of Peter Cartwright
(1785-1872), a Methodist itinerant who revelled in the fact that
“illiterate Methodist preachers set the world on fire” while
other denominations “were lighting their matches.”#? He
feared Methodism’s growing interest in education. Other de-
nominations had experimented with an educated ministry

85. Cole, pp. 174-175,

86. Cross point out that “ultraism and Jacksonian Democracy rose and
fell together” Cross, p. 271. And Weisberger notes that revivalism was a
part of “freedom’s ferment.” Contrary to the conservative intentions of its
earliest advocates, revivalism allied itself with nineteenth-century pro-
gressivism. Weisberger, p. 78. Mead argues that in fact democracy itself
became the abject of veneration. Mead, The Lively Experiment, pp. 67-68.

87. Douglas, pp. 165-168.

88. Hotstadter, pp. 93-94.

89. Quoted in Douglas, p. 37.

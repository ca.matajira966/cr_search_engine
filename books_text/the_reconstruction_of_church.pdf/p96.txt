84 CHRISTIANITY AND CIVILIZATION

self, The believer benefits and grows little. The simplicity of
the Gospel is emphasized. Irrelevance and unconcern for the
state of the world has become a matter of boasting. On the
other hand, there are churches which preach an entirely social
(read political) salvation, and a millennium during which all
men’s rights everywhere will be held sacred. Sin is basically in
the environment, not in the heart. Such churches bewail the
complexity of social issues, and appeal to experts for advice
and to politicians for relief of social ills.

The nature of the church, its message and mission, its
worship and government must all be covenantal. Revivalistic
individualism, antinomianism, and pessimism must be thor-
oughly purged from American churches. The reconstruction
of the local church, like all reconstruction, must begin with
change in the hearts and minds of men. For two and a half
centuries a subtle internal humanism has been gnawing at the
foundations which support American Protestantism: We have
nurtured our greatest enemy. It is counterproductive to point
to outside forces as the cause of the church’s decline. Chris-
tians themselves are responsible, We no longer act or think
like a church; indeed, we have forgotten how a church should
think, or what its characteristics are. The mere recognition
that the churches of America themselves are responsible for
their present impotence and confusion indicates where recon-
struction of America must begin. We must first tidy our own
house, and then we will be qualified to speak with authority
on the issues which confront the modern world (Matt. 7:1-5).

What Is New Age Humanism? id

become a god, or that man is “an exact duplicate of God,” Recon-
structionists have taught over and over again that there is a fun-
damental Creator/creature distinction.

The Meaning of Deification”™

We should, however, at least examine how these men use these
terms. Some of the most orthodox church fathers used similar
phrases but meant something different from the way present New
Agers use them. They too spoke of the “deification” of. man in
Christ. Athanasius,* in a famous statement from his classic work,
On the Incarnation of the Word of God, wrote: “The Word was made
man in order that we might be made gods.” David Chilton makes
this point:

The Christian doctrine of deification (cf. Ps. 82:6; John
10:34-36; Rom. 8:29-30; Eph. 4:13, 24; Heb. 2:10-13; 12:9-10; 2
Pet, 1:4; 1 John 3:2) is generally known in the Western churches
by the terms sanctification and glorification, referring to man’s full
inheritance of the image of God. This doctrine (which has abso-
lutely nothing in common with pagan realistic theories of the continuity of
being, humanistic notions about man’s “spark of divinity,” or Mormon poly-
theistic fables regarding human evolution into godhood [emphasis ours])
is universal throughout the writings of the Church Fathers; see,
e.g., Georgios I. Mantzaridis, The Deification of Man: St. Gregory

23, For a detailed discussion of deification, the reader is encouraged to study
Robert M. Bowman, Jr., “Ye Are.Gods?: Orthodox and Heretical Views on the
Deification of Man,” Christian Research Journal (Winter/Spring 1987), pp. 18-22.

24. Athanasius (c. 296-373) led the. theological battle against Arianism, a
heresy that denied the eternality of Jesus Christ the Son of God as the Logos.
Arianism taught that Jesus was only a subordinate being, that He was not the
Second Person of the Trinity. Athanasius challenged Arius and the Arians during
most of the fourth century by teaching the eternal Sonship of the Logos (Jesus,
John 1:1), the direct creation of the world by God-(Gen. 1:1; Col. 1:17-23), and the
redemption of the world and men by God in Christ. A good dose of the
Athanasian Creed would go a long way in helping present day cultists. See Ap-
pendia C.

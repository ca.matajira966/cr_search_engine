What Is New Age Humanism? A

premilicnnial; its answer to the problems of the world was to
postpone solutions to the “any moment return” of Christ. Anti-
nomianism thus led to an intense interest in and expectation of
Christ's return as the only solution to the world’s problems,
Christ’s law being denied the status of an answer.®

Dave Hunt and other critics of dominion theology and Chris-
tian reconstruction have become pietists, retreating from the so-
cial problems of this world. Some positive confession adherents
have been seduced by elements of mysticism. What do Dave
Hunt and those he criticizes have in common? A denial of the law of
God as a standard for righteous living.

But many of the positive confession preachers are escaping
from this antinomian trap. (Dave Hunt’ attacks on them are im-
portant motivations in this defection from dispensational anti-
nomianism to Christian reconstruction.) The law of God is being
accepted for what it is: the law of God. The whole Bible is accepted
as the standard for righteous living for individuals, families,
churches, and civil governments. This is what Christian recon-
stractionists have been saying for a number of years, long before
New Age humanism became popular and Dave Hunt began to
write on the subject.

4, Reincarnation, karma: Salvation is a multi-lifetime process
of progression or digression.”

New Age humanism makes its “leap of being” from mere man
to god through raising the state of consciousness, evolutionary
development, reincarnation, or some combination of the three.

65. Rushdoony, Jastitutes of Biblical Law, p. 654.

66. Rushdoony, Institutes of Biblical Law, 1973; Law and Society (Vallecito, CA:
Ross House Books, 1982); Law and Liberty (Tyler, TX: Thoburn Press, 1971);
James B. Jordan, The Law and the Covenant: An Exposition of Exodus 21-23 (Tyler,
TX: Institute for Christian Economics, 1984); Greg L. Bahnsen, Theonamy in
Christian Ethics and By This Standard.

67. “If one accumulates good karma, positive benefits accrue in later lives.
Bad karma produces future punishments. Eventually one may leave the cycle of
birth and rebirth entirely through the experience of enlightenment.” Groothuis,

Unmasking the New Age, p. 150.

New Age Humanism: A Kingdom Counterfeit 109

‘We do not often consider “theological counterfeiting” as a way
the devil might hide the truth from Bible-believing Christians. Yet
the Bible shows us that there are counterfeit Christs (Matt. 24:5;
Acts 5:36-37), counterfeit prophets (Matt. 7:15; 24:11), counterfeit
miracles (Ex. 7:8-13), counterfeit angels (2 Cor, 11:14), counterfeit
gods (Gal. 4:8; Acts 12:20-23), counterfeit good works (Matt.
7:15-23), counterfeit converts and disciples (1 John 2:19), counter-
feit spirits (1 John 4:1-3), counterfeit doctrines (1 Tim. 4:3), coun-
terfeit kings (John. 19:15), counterfeit names |(Rev. 13:11-18; cf.
14:1), and counterfeit gospels (Gal. 1:6-10). Why should we be sur-
prised if there are counterfeit kingdoms (Dan. 2; Matt. 4:8-11;
Acts 17:1-9) and a counterfeit new age (Rev. 13:11-18)? The New
Age Movement is a counterfeit. It wants the fruit of Christianity
without the root, :

What shiould this tell us? When Jesus came to earth to do the
work of His Father, there was heightened demonic activity.
Satan’s purpose was to counterfeit the work of Christ, to confuse
the people. The devil knew his time was shorti(Rev. 12:12; Rom.
16:20), He was making a last-ditch effort to subvert the work of
the kingdom: Satan gathered his “children” around himself to call
Jesus’ mission into question (John 8:44). At one point, Jesus was
even accused of being in league with the devil (Luke 11:14-28), As
Jesus moved closer to establishing peace with God for us through
His death and resurrection (cf. Rom. 5:1), the| power of the devil
was grounded, made impotent (Luke 10:18). But through Jesus’ dis-
ciples the worid.was turned upside down (Acts 17:6). Satan’s king-
dom was-spoiled and left desolate (Luke 11:20; Acts 19:11-20). The
Apostle Paul then tells the Roman Christians that God would “soon
crush Satan,” the great counterfeiter, under thei | feet (Rom. 16:20).

Religious corruption was Satan’s new strategy for subverting
God's kingdom work. Jesus’ battles. were with the religious leaders
of the day. The scribes and Pharisees were scrupulously theologi-
cal in their.evaluation of Jesus. The law was quoted, but certainly
misapplied. Jesus was always accused of not keeping the law, of
not following Moses. The devil had the Pharisees convinced that

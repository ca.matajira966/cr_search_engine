New Age Humanism: A Kingdom Counterfeit 121

prentice devil Wormwood about his Christian “patient,” senior
devil Screwtape writes:

By the very act of arguing, you awake the patient's reason;
and once it is awake, who can foresee the result? Even if a partic-
ular train of thought can be twisted so as to end in our favour,
you will find that you have been strengthening in your patient the.
fatal habit of attending to universal. issues and withdrawing his
attention from the stream of immediate sense experiences. Your
business is to fix his attention on the stream. Teach him to call it “real life”
and don’t let him ask what he means by “real?

The devil wants us to believe that he is in control of the world,
that the church is weak, that God cannot use His redeemed and
transformed people through the power of His Spirit to advance
His purposes in time and in history. He hypnotizes us with the
unbiblical assertion that he is in control: of the world, that God’s
plans are on hold until God personally intervenes in history to reign
over the earth, But even this is not enough, for Dave Hunt tells us
that “the millennial reign of Christ upon earth, rather than being
the kingdom of God, will in fact be the final proof of the incorrigible
nature of the human heart.”®? Sin then is greater than God's efforts.
The devil, in principle, wins the game: Satan can laugh at God’s
efforts through eternity, always reminding Him that as long as the
devil is around, He just can’t succeed.

Building a New Civilization

When we as Christians advocate the building of a Christian
civilization, much of what we say and write seems to be similar to
what advocates of New Age humanism are espousing. But in fact
we are not imitating New Agers. They are imitating God and His
kingdom. The New Age kingdom is the counterfeit kingdom. In
effect, the New Age kingdom is a Johnny-come-lately kingdom
that cannot be sustained because man is its foundation (Dan.
2-3). Postmillennialism was the prevalent eschatological view of

5, (New York: Macmillan, 1946), p: 12. Emphasis added.
52. Hunt, Beyond Seduction, p. 250, Emphasis added.

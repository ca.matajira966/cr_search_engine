128 The Reduction of Christianity

words are used by various non-Christian groups and then filled
with new and sometimes sinister content, First it was sodomy,
then it became homosexuality, now a sodomite is described as
being “gay.” Being “gay” takes the verbal edge off the descriptive
and negative “sodomite.” The semantic abuse in the abortion
debate is even more clear. Abortionists do not call themselves
“pro-death.” Rather, they choose words that bridge religious and
political lines of thought. Most Americans believe that they ought
to have the right to make their own choices without interference
from government. The pro-abortionists chose “pro-choice” to put
the best face on their bloody business. The majority of Americans
who really do not know what happens during an abortion are often
fooled because the word pro-choice is so American.? The Commu-
nists’ use of the words “peace” and “détente” are other examples.
The words “humanist” and “humanism” were chosen centuries
ago by pagan philosophers and scientists to present a world view
that few people could disagree with by only hearing the words.
Most people equate “humanism” with humanitarianism or the hu-
manities. Historically, a humanist was someone whose studies in-
cluded “classical” learning.® So then, today, being anti-humanistic
means you must be anti-humanitarian, and you despise the hu-
manities. This is a very clever tactic. Find a word that has broad

8, Let’s assume that Christian parcnts wanted to describe their desire to put
their children in alternative schools as a “pro-choice” decision. Is their use of the
word different in content from that of the pro-choice abortionists? Of course. The
pro-choice abortionists are using their choice to snuff the life out of a defenseless
human being. Christian parents are asking for the freedom to choose so their
children will not be denied training in a comprehensive biblical world view that
the public schools deny them.

9, “The secular humanism that we meet today is not the same thing as the
Renaissance humanism which one sees in such men as Erasmua and Leonardo
dg Vinci. (Renaissance humanism, despite some murky streaks, was in essence a
plea for a rich and robust Christian culture.) Nor should we equate secular
humanism with the humanism professed by those who teach the hurianities pro-
fessionally; nor should we confuse it with the spirit of sympathetic concern for
others’ welfare which is often called humanism in these days.” J. I. Packer and
Thomas Howard, Christianity: The True Humanism (Waco, TX: Word Books,
1985), p. 16.

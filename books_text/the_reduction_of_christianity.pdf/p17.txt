Foreword xvii

regarding the Church's earthly future. The exegetical crisis of pre-
millennial dispensationalism is becoming evident, for dispensa-
tionalists have failed to recognize the enormous threat to their
theological system that Hunt's books have presented. That Dave
Hunt, a man with a bachelor’s degree in mathematics, is now the
most prominent theologian of the dispensational movement, as
immune from public criticism by dispensational theologians. as
Hal Lindsey was in the 1970s, indicates the extent of the crisis.
The amateurs give away the store theologically, and the seminary
professors say nothing, as if these paperback defenders had not
delivered mortal blows to the dispensational system.

He refuses to let go. In Tape Two of the widely distributed
_three-tape interview with Peter Lalonde, he announces that God
Himself ts incapable of setting up an earthly kingdom!

In fact, dominion—taking dominion and setting up the king-
dom for Christ—is an impossibility, even for God. The millennial
reign of Christ, far from being the kingdom, is actually the final
proof of the incorrigible nature of the human heart, because
Christ Himself can’t do what these people say they are going to
do...

Compare this with Hal Lindsey’s comment under “Paradise
Restored”: “God’s kingdom will be characterized by peace and
equity, and by universal spirituality and knowledge of the Lord.
Even the animals and reptiles will lose their ferocity and no longer
be carnivorous. All men will have plenty and be secure. There
will be a chicken in every pot and no one will steal it! The Great
Society which human rulers throughout the centuries have prom-
ised, but never produced, will at last be realized under Christ’s
tule. The meek and not the arrogant will inherit the earth (Isaiah
12." Or again, “That time is coming when believers in Jesus

12, Hal Lindsey, The Late Great Planet Earth (Grand Rapids, MI: Zondervan,
{1970] 1973), p. 177.

7
GUILT BY ASSOCIATION

Too often, a doctrine is judged because of its association with
heretical groups that seem to hold the same doctrinal position. In-
stead of evaluating the doctrine on its own merits or demerits by
using the testimony of Scripture as the “touchstone” of truth, the
critic maintains that the position must be wrong because anti-
Christian groups hold a similar position. This frequently happens
in elections when a candidate holds to a controversial position,
and it is learned that an extremist group holds a similar position.
“John Jones supports work-fare. We’ve just learned that the Ku
Klux Klan holds a similar position. That's typical of the KKK;
they're racists anyway. Since John Jones advocates a position simi-
Jar to that of the KKK, our organization is withdrawing its support
from John Jones.” The possible merits of work-fare are obscured by.
the association with the deserved negative press that follows the
KKK. Work-fare should be evaluated on its own merits.

Millions of Americans have owned Volkswagens. Adolf Hitler
pushed for the production of a “peopie’s car,’ the Volkswagen;!
therefore, anybody who drives a Volkswagen is a Nazi.

Dave Hunt has implied that those who hold. to a “dominion
theology” are being seduced by a New Age philosophy. For Dave
Hunt, the idea of dominion “opens the door to a marriage with
New Age beliefs.”? Ifa New Ager talks about the threat of nuclear

1, William L. Shirer, The Rise and Fall of the Third Reich (New York: Simon and.
Schuster, 1960), p. 266.

2. Dominion: A Dangerous New Theology, Tape #1 of Dominion: The Word and New
World Order, Ontario, Canada, 1987.

142

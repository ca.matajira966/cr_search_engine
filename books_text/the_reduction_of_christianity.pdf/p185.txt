Guilt by Association 145

manists and humanist organizations that paint a gloomy picture
of our earthly future.” David Wilkerson is predicting a nuclear
holocaust, and so did a prominent New. Ager, author of the cult
book The Mayan Factor:

Mr. Arguelles from Boulder, Col., [is] an art historian by
training but.a “millennialist” by inclination, by divine direction,
by the dictates of reincarnation. . . .

Mr. Arguelles says the choice between a “new age” and all-out
destruction is ours, and we had better decide within the next
eight weeks. A new beginning can be assured only if enough peo-
ple gather at sacred spots around the globe like Machu Picchu,
Peru—on Aug. 16 and 17 [1987].8

Arguelles was counting the days until the New Age would
dawn. He even drew on the biblical literature, asking his sup-
porters—1i44,000 of them--“to go to places like the Pyramids,
Machu Picchu and even Idaho.”® And it was all to begin on
August 16, 1987, a day when just about nothing noteworthy hap-

7, The original Global. 2000 Report to the Prasident wos a. frightening look into the
future. It was the work of globalists and humanists. Two paragraphs summarize
the “Major findings and Conclusions” of Global 2000:

If present trends continue, the world in 2000 will be more crowded,
more polluted, less stable ecologically, and more vulnerable to disruption
than the world we live in now. Serious stress involving population, re-
sources, and environment are clearly visible ahead. Despite greater ma-
terial output, the. world’s people will be poorer in many ways than they
are a
For bandreds of millions of the desperately poor, the outlook for food
and other necessities of life will be more precarious in 2000 then it is
now—unless the nations of the world act decisively to alter current
trends (p. 1).

Global 2000 reads like Hal Lindsey’s chapter, “Polishing the Crystal Ball,” The
Late Great Planet Earth (Grand Rapids, MI: Zondervan, [1970] 1973), pp. 180-86.
Is Lindsey in with the Humanists and Giobalists that put together the Global 2000
Report to the President? We don't think so.

8, Meg Sullivan, “New, Age Will Dawn in August, Seers Say, And Malibu Is
Ready,” The Wall Street Journal (June 23, 1987), p. 1, This sounds very much like
David Wilkerson when he writes: “America is going to be destroyed by fre! Sud-
den destruction is coming and few will escape. Unexpectedly, and in one hour, a
hydrogen holocaust will engulf America—and this nation will be no more.” Set
The Trumpet to Thy Mouth (Lindale, TX: World Challenge, 1985), p. 1.

9. Sullivan, “New Age Will Dawn,” p. 1.

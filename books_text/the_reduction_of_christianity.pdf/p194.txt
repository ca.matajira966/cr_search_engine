154 The Reduction of Christianity

in this sense in John 1:12-13: “But as many as received Him, to
them He gave the right to become children of God, even to those
who believe in His name, who were born not of blood, nor of the
will of the flesh, nor of the will of man, but of God.”

Thus, Paul does not mean that believers will enter the king-
dom as disembodied souls. They will enter the final kingdom with
resurrected, spiritual bodies. Jesus ate with His disciples after His
resurrection (Luke 24:40-43). His disciples were able to touch and
see Him. He even called attention to His “flesh and bones” (Luke
24:39), Yet, He was raised with a spiritual body (1 Cor. 15:45-46).
A spiritual body is not a vapor or a mist. It is a body controlled by the Holy
Spirit. Those who enter the final kingdom will have bodies, but
they will not be weak, corruptible, and depraved “fleshly” bodies.
Men must be transformed to inherit the kingdom. They must be
raised with spiritual bodies.?

What, then, does this passage actually teach about the timing
of the kingdom? It does not teach that there is no kingdom in his-
tory. It teaches that men must be transformed to inherit the king-
dom of God. This is true in the present, as well as in the future. If
we are to be subjects of the kingdom of God now, we must be
spiritual, not fleshly. In principle, we are already spiritual. We
have been baptized into Christ, and therefore we are “freed from
sin” (Rom. 6:1-7). In Romans 7:5, Paul says, “For while we were
in the flesh, the sinful passions, which were aroused by the Law,
were at work in the members of our body to bear fruit for death.”
Note that Paul tells Christians that they were in the flesh. In a
sense, then, Christians are already Spiritual, though we are not
perfectly Spiritual; we have already put off “flesh and blood,” and
now live in the “newness of the Spirit and not in oldness of the let-
ter” (Rom. 7:6). Thus, what Paul says about the final kingdom in
1 Corinthians 15:50 is already true of Christians today. And, if

9. ‘This resurrection has already taken place in principle because we already
share in Christ’s resurrection. Having been baptized into His death, we are rained
in the likeness of His resurrection, to walk in newness of life (Rom. 6:1-11). See
Norman Shepherd, “The Resurrections of Revelation 20,” Westminster Theolagical
Journal 37 (Fail 1974): 34-43.

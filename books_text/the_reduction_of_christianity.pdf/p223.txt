Dave Hunt's Heavenly Kingdom 183

less than discipling all the nations of the earth, The mission is to
bring the world under the dominion of Christ, in the power of His
Spirit, and through the ministries of teaching and baptizing.

How Big Is the Gospel?

Other passages make it clear that the message of the gospel it-
self includes more than a message of individual preparation for
heaven. In Acts 20:18-35, we find Paul’s farewell message to the
Ephesian elders. Paul repeatedly declares that he has fulfilled
completely his apostolic mission in the Ephesian church. It is in-
teresting to note the various ways that he describes that mission,
He declared everything profitable (v. 20). He testified of “repent-
ance toward God and faith in our Lord Jesus Christ” (v. 21). The
mission he had received from Christ was to witness to “the gospel
of the grace of God? (v. 24). Among the Ephesian Christians, he
“preached the kingdom’ (v. 25) and declared “the whole purpose
of God? (v. 27).

A careful reading of this passage will show that these phrases
are parallel to one another and are closely interconnected. They
are all different ways of describing what Paul had taught and
preached among the Ephesians. For our purposes, it is important
to note that preaching the “gospel of grace” is simply another way
of declaring “the whole purpose of God.” Paul knew nothing of a
narrow gospel; to preach the gospel was to preach the whole
counsel of God. The gospel affects man in his totality. It speaks to
every area of life. This does not mean that Paul was unable to
make distinctions between central and peripheral elements of the
gospel. The point is that, for Paul, all elements of the gospel were
important, and the gospel was the whole counsel of God.¥

Thus, practically, when an individual becomes a Christian,
there is more that the Lord wants him to do. He is to live out the
implications of his confession in his whole life. He is to live in obe-

30. For these comments, we are indebted to lectures on Pauline theology by
Richard B. Gaffin, Jr., Westminster Theological Seminary, Philadelphia, Spring
1986.

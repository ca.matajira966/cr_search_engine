210 The Reduction of Christianity

is his people’s saviour and promiser.** Hence, the ultimate goal of
Christ’s rule is God’s glory and good pleasure. The failure of the
Old Testament kings had led Israel to doubt whether God truly
ruled. The nations had seen Israel's sinfulness and oppression,
and they blasphemed God (Isa. 52:5; Rom. 2:24). How could
God really be King if His people were constantly oppressed and
enslaved? Christ delivered His people so that God’s honor would.
be vindicated and His name glorified, so that the ends of the earth
would know that the Lord is God indeed.

Of course, Christ’s deliverance and His rule were different
from what many Israelites had expected. Rather than throwing
off the chains of Rome, Christ broke the chains of sin and death,
He conquered the Enemy behind the enemy, In contrast to the
conquests of the Old Testament judges and kings, Jesus’ conquest
was not over external enemies, but over the invisible accuser and.
oppressor of men. In extending His kingdom, He does not con-
quer enemies by the sword of iron, but by the sword that comes
out of His mouth (Rev. 19). Also, Christ did not deliver Israel
only. In fact, He spent a lot of time telling Israel that the Old Cov-
enant people would be judged, and He delivered men and women
from every nation and tribe and tongue. Finally, Christ. became
King through His self-sacrifice on the Cross. He performed His
visible redemptive work not as the majestic Son of David, but as
the Suffering Servant. Likewise, His kingdom grows not by anex-
ercise of brute force, but by the selfless service of His people.

Christ's Conquest of Satan

Christ's miracles were, among other things, signs of His con-
quest of Satan and His establishment of the rule of God. Jesus
drove out demons by the Spirit of God as a sign that “the kingdom
of God has come upon you” (Matt. 12:28-29). Even the demons
recognized why Jesus had come: “What do we have to do with
You, Jesus of Nazareth? Have You come to destroy us? I know
who You are—the Holy One of God” (Mark 1:24). Jesus bound

4. Ridderbos, The Coming of the Kingdom, p. 22.

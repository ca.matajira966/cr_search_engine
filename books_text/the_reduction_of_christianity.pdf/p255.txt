The Kingdom is Now! 215

heaven and with the peoples of the earth” (Dan. 4:34-35). He
“forms light and creates darkness.” He “brings prosperity and
creates disaster” (Is. 45:5-7). Yet, it is clear that Satan continues
to operate on earth ( Job 1-2). Thus, though the Father has ruled
since creation, He also progressively vanquishes the forces of
darkness. On the last day, the Son wili deliver the kingdom to the
Father, so that He may be ail in all (1 Cor. 15:28).

From Heaven to Earth

It may be helpful at this point to discuss the relationship of the
“kingdom” to heaven. It is true that Jesus and the New Testament
writers sometimes use the word “kingdom” to refer to the eternal
state in heaven. This is especially apparent in those passages
where Jesus talks about the kingdom as an inheritance (Matt.
25:34) or a reward (Matt. 20:1-16). In many passages, Paul warns
that the wicked will not inherit the kingdom, implying that it is a
future reality (1 Cor. 6:9-10; Gal. 5:21; Eph. 5:5). In 2: Timothy
4:18, Paul refers to Christ’s “heavenly kingdom.” Matthew uses
the phrase “kingdom of heaven” in the same way that the other
evangelists use the phrase “kingdom of God.”

Nevertheless, this usage of “kingdom” should not lead us to the
conclusion that the kingdom is exclusively in heaven, or that the
kingdom has no impact on the history of the earth. Just as the
King becomes incarnate on earth and enters history, so also His
kingdom enters the world of human affairs. In Christ and by His
work, heaven comes to earth. As Vos says, “the Kingdom of God be-
comes incarnate.” John Bright agrees: “In the person and work
of Jesus the Kingdom of God has intruded into the world.”

This does not mean that Christ’s rule is “earthly” or “fieshly,”
like the kingdoms of the world, It’s realm is not limited to earth,

“10. Geerhardus Vos, Biblical Theology: Old and New Testaments (Grand Rapids,
MI: Eerdmans, [1948] 1975), p. 376.

11. John Bright, The Kingdom of God (New York: Abingdon-Cokesbury, 1953),
p. 216. Neither Vos nor Bright, however, would agree with all of the implications
that we draw from this fact. We quote them simply to show that we are not alone
in insisting that the kingdom is a reality on earth.

216 ‘The Reduction of Christianity

but includes heaven as well. It is everlasting. It is ruled on differ-
ent principles and is established by different methods than earthly
kingdoms, It is Spiritual in the full biblical sense, namely, that
Christ rules through the Holy Spirit. Nor does it mean that earth
will ever perfectly reflect the reality of heaven. But the kingdom
operates on earth, just as Christ lived on earth and still works by
His Spirit. And we are to strive and pray to make earth reflect and
image heaven,

The very nature of Christianity implies that the rule of Christ
affects earthly history. Biblical Christianity has always been histor-
ical. The early creeds of the Church are simply recitals of the his-
tory of Christ’s birth, death, and_resurrection. These all occurred
on earth, in history, It would be more than strange if the King had
come to earth, died on earth, and risen again in a spiritual body
so that He could establish a kingdom that has nothing to do with
earth. Why did Christ do this on earth? Why did He become in-
carnate and enter human history? The answer of Scripture is that
He came to redeem what was fallen. He came into the world to
redeem the world. He came into the world to establish His
redemptive reign among men on earth,

Moreover, several passages explicitly claim that Christ exer-
cises dominion on earth. Christ claimed that He had been given
all authority in heaven and on earth (Matt, 28:18-20).% Paul wrote
to the Colossians that Christ, the Creator of all things, had come
toearth to restore all things (Col. 1:16).3 Christ’s rule is as exten-
sive as creation itself, People, real historical people, enter the
kingdom (Col. 1:13). When Jesus gave Peter the keys of the king-

12. Some-dispensationalists say that Christ was given this authority at His res-
urrection and ascension, but hasn’t yet entered into the exercise of that authority.
But this does great violence to the text. Jesus does not make this statement in a
vacuum. Hc is saying this to His disciples to encourage them in their work of dis-
cipling the nations. It would have been a cruel joke indeed if Jesus had encour-
aged His disciples to take up the task of discipling the nations, sending them out
confidently to Judea, Samaria, and the uttermost parts of the earth, if He were
not actually reigning after all.

13, See Appendix D.

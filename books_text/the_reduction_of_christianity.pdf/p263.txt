The Kingdom is Now! 223

suffering and His kingly glory (Rom. 8:17). By enduring suffer-
ing, we also share in His reign (2 Tim. 2:12). God’s people, His
church, is the instrument by which the blessing of His reign is ex-
tended throughout the earth. As A. A. Hodge put it,

‘The special agency for the building up of this kingdom is the
organized Christian Church, with its regular ministry, providing
for the preaching of the gospel and the adminisiration of the sac-
raments. The special work of the Holy Ghost in building up this
kingdom is performed in the regeneration and sanctification of
individuals through the ministry of the Church.

This is part of what Jesus meant when He told His disciples,
who constituted the foundation of the church (Eph. 2:20), that the
kingdom would be given to them. As the Father had conferred the
kingdom on Christ, so also Christ conferred the kingdom on the
disciples, and by extension, on His church. In particular, the
church is given the authority to judge (rule) the twelve tribes of
Israel, and the related privilege of sitting at the King’s table
(Luke 22;29-30).2 Moreover, the keys of the kingdom were given
to the church. The church is the gateway to the kingdom of heav-
enly blessing, authority, and privilege (Matt. 16:19).

Taken together, these verses suggest that there is a very close
connection between the kingdom of Christ and the. church. The
church, the people of God, possesses the power, blessing, and

 

are not explicitly mentioned. Man has dominion over the visible creation. In
Hebrews 8, however, the quotation does not include the references to the visible
creation, and the comprehensive nature of Christ's dominion is emphasized.
Thus, Christ was not merely exalted to the same status of Adam, but given 4 more
comprehensive dominion. In Christ, we also are exalted, not only above the visi
ble creation, but above invisible powers and authorities (cf. Eph. 1:19-23; 2:6).
This is of the greatest importance, because it is only in this way that we are able
to battle our most dangerous enemies.

94. Hodge, Evangelical Theology, p. 256.

25, In this passage, we find that sharing in Christ’s reign is preceded by stand-
ing by Him in His sufferings (Luke 22:28). We also discover that service is the
means to ruling in a Christ-like way (vv. 24-28). The same pattern is reflected in
the letters to the seven churches at the beginning of the Revelation. Authority
and dominion are promised to those who “overcome,” those who persevere
through persecution (Rev. 2:26).

  

232 The Reduction of Christignity

Now if by the sign of the Cross, and by faith in Christ, death
is: trampled down, it must be evident before the tribunal of truth
that it is none other than Christ Himself that displayed trophies
and triumphs over death, and made him lose all his strength. . . .
Death has been. brought to nought and conquered by the very
Christ that ascended the Cross.?

Christ not only conquered death; He dealt a death blow to Satan.
As a result, “idols and spirits are proved to be dead.” The pur-
pose of Christ’s death and resurrection, moreover, was not simply
to deliver believers from death and the devil, but positively to
“create anew the likeness of God’s image for them,” the image that
they had lost when Adam sinned.”

Just as there were different views about the beginning of the
kingdom, there were different views about its future course."
Athanasius taught that the victory of Christ on Calvary had
effects on world history, effects that were already visible in his day.
He. quoted Isaiah’s prophecy that the nations “will beat their
swords into ploughshares,” and concluded that this prophecy was
being fulfilled already.

8. Athanasius, “On the Incarnation of the Word,” 29. In The Nicene and Post-
Nitene Fathers, 14 vels. Second Series, eds., Philip Schaff and Henry Wace (Grand
Rapids, MI: Eerdmans, [1891] 1980), vol. 4, p. 51.

9. Tbid., 31, p. 53.

10. Ibid., 20, p. 47.

11, For example, Irenaeus taught a kind of millenarianism. He believed that
Christ would return ‘bodily to establish His kingdom on earth for a thousand.
years. This millennial reign would be the fulfillment of the Old Testament proph-
ecies about peace and prosperity.

«+. when this Antichrist shall have devastated all things in this world,
he will reign for three years and six months, and sit in the temple at Jeru-
salem;.and then the Lord will come from heaven in the clouds, in the
glory of the Father, sending this man and those who follow him into the
lake of fire; but bringing in for the righteous the times of the kingdom,
that is, che rest, the hallowed seventh day. “Against Heresies,” V.XXX.4,
p. 560.
‘Justin apparently held to a similar view (see “Dialogue with Trypho,” chapters
LXXX-LXXXI, pp. 239-40), though he admitted that other Christians held
other opinions.

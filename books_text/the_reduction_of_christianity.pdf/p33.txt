PREFACE
by Gary DeMar

Why The Reduction of Christianity? There are at least three
reasons. First, defensive necessity; second, to’set forth a positive
agenda for Christians to influence their world with the life-
transforming effects of the gospel; and third, to show that as we
approach the end of the 20th century the “full purpose of God” has
been reduced to a shadow of its former glory.

Let me reflect for a moment on this third point, which accounts
for the title of this book. Dave Hunt, to whom we are responding,
has brought to light a real problem by exposing the demonic side
of the New Age Movement. It is a widespread and culturally ac-
cepted revival of paganism. Eastern mysticism is no longer counter-
culture, as it was in the 60s, but mainstream culture. The New
Age Movement needs to be confronted and battled. Mr. Hunt has
provided much valuable ammunition to help Christians deal with
New Age seduction.

In order'to battle the New Age, however, we must have a full
arsenal. And it is in this respect that we differ with Mr. Hunt, He
has discerned a problem, but has no solution. In fact, one of the
thrusts of his books is that there is really no solution. He sees no
way to combat a growing cultural malaise because he is operating
with a reduced gospel and a reduced Christianity. Hunt has no com-
prehensive Christian view of life to offer. He has no philosophy of
historical progress rooted in the sovereign operation of the Spirit
of God. And he cannot motivate Christians to action, because he
believes that there is no hope of comprehensive earthly success for
the gospel of the Lord Jesus Christ. Thus, he has robbed the

iii

Turing the World Right-Side Up 291

I have defrauded anyone of anything, I will give back four times
as much” (Luke 19:9).

Tf injustice is operating in the world to hurt others, can we sit
by and do nothing like the self-righteous Levite? Isaiah tells us,
for example, that tampering with monetary commodities affects
orphans and widows, those least able to care for themselves finan-
cially (Isa. 1:21-23). Is economics neutral? Apparently not. Notice
too that Israel, the people of God, were condemned because they
did nothing. In Matthew 23 Jesus indicts the Pharisees, the relig-
ious leaders of the day, for using the law to protect their own in-
terests, while ignoring the needs of others.

Marxists Fill the Gap

The Marxists in Central America have attacked the Christian
gospel on this very point. Liberation Theologians preach a gospel
that has something to do with the here and now. Liberation
‘Theology parades as a biblical system that supposedly brings jus-
tice to the masses. We are told that only in Marxism, the heart of
much Liberation Theology, is there a reliable struggle for
justice. Marxists “try to resolve the situation of exploitation. and
inequality. The new society they desire and the kingdom of God
are the same,” one priest said.2# A person only discovers the
meaning of the kingdom. of God by making this world a better
place. “In 1972 the bishops of two of Nicaragua’s largest diocese
declared their support for ‘a completely new order’ The new order
should iniclude the ‘preferential option for the poor and a ‘planned
economy for the benefit of humankind.’ ”*

This is very attractive to poor people, many of whom do not

23. Gary North, Liberating Planet Earth: An Introduction to Biblical Blueprints
(Ft. Worth, TX: Dominion Press, 1987).

24, Edmund and Julia Robb, The Betrayal of the Church (Westchester, UL:
Crossway Books, 1986), p. 119.

25, Ibid., p. 124,

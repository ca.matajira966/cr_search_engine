Building a Christian Civilization 313
themselves to blame for the rapid secularization of the West. If
political, industrial, artistic, and journalistic life, to mention only
these areas, are branded as essentially “worldly,” “secular,” “pro-
fane,” and part of the “natural domain of creaturely life,” then is it
surprising that Christians have not more effectively stemmed the.
tide of humanism in our culture??

God: created everything wholly good (Gen. 1:31). Man,
through the fall, became profane, defiled by’sin. Redemption re-
stores all things in Christ. Peter failed to understand the gospel’s
comprehensive cleansing effects. He could not believe the Gen-
tiles were “clean”: “What God has cleansed, no longer consider
unholy” (Acts 10:15; Matt. 15:11; Rom. 14:14, 20). The fall did not
nullify God's pronouncement that the created order “was very
good” (Gen. 1:31). The New Testament reinforces the goodness of
God's creation: “For everything created by God is good, and noth-
ing is to be rejected, if it is received with gratitude; for it is sancti-
fied by means of the word of God-and prayer” (1 Tim. 4:4, 5).

Scripture is our guide, not the Platonic view of matter as
something less good than the “spiritual” world, God “became flesh
and dwelt among us” (John 1:14). Jesus worked in his earthly
father’s shop as a carpenter, affirming the goodness of the created
order and the value of physical labor.

A Christian civilization should be built out of conviction, not
solely out of reaction to a dominant secularism.

Millennial Hope-ism

One way to have a Christian civilization is to wait until Jesus
returns to earth to establish one. In the meantime, Christians are
to wait. Evil is-inevitable. There is little if anything the Christian
can do to stop evil’s advance. In fact, the Christian is living in the
“last days” of man’s attempts to build any type of civilization.

History is filled with examples of generations of Christians
awaiting a cataclysmic eschatological event that would transform

21. Albert M. Wolters, Creation Regained: Biblical Basics for a Reformationat
Worldview (Grand Rapids, MI: Eerdmans, 1984), p. 54.

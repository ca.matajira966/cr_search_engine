Conclusion 339

than desert a good concept simply because it is misused, we
should seek to be reconstructionists within the biblical [i.e., pre-
millennial] eschatological framework.1!

Schnittger supports his conclusions by arguing that the phrase
“ast days” does not refer merely to the end of the world. Rather he
believes the events of the “last days” are “general conditions that
characterize the entire church age.”2 Though evil will not be pro-
gressively eradicated, there is still “the possibility of a progressive
growth in strength and influence of the true church.”!3 He even
presents statistics to show how the church has grown through the
centuries. His interpretations of the parables of Matthew 13 are
very much the same as our interpretations. *

From this theological basis, Schnittger outlines a “Christian
Reconstruction Agenda for the End of the Twentieth Century,”
including pro-life activism, the building of strong Christian
homes, Christian schools, and Christian involvement in law and
politics,*° In addition to this kind of activism, he emphasizes that
Christians should always be at work building strong and loving
churches, supporting evangelism and missions, and engaging in
individual discipleship.'*

Here is a pretribulation premillennialist who thinks that Chris-
tians need to be involved in Christian reconstruction. In eschatol-
ogy, he believes that Christ will come to rapture His saints before
the tribulation begins. In other words, Christ’s people will escape
the worst period of history. It would seem that Christians have lit-
tle reason to be concerned about the state of the world; after all,
they will escape the terrors of the tribulation. Moreover, he be-
lieves that this tribulation period is inevitable. The most logical
question in the world seems to be, “Why polish brass on a sinking
ship?” Yet, Schnittger-criticizes the pessimism of most pretrib pre-

11. Idem,

12. Zbid., pp. 10-12.
13. Idem,

14, Ibid., pp. 1244.
15. Ibid., chapter 4.
16. ibid., p. 24.

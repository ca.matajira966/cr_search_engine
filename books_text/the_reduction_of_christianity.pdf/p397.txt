This World and the Kingdom of God 357

We must admit, therefore, that the kingdom of.Christ is not
merely internal and other-worldly. It has external expression on
earth at the present time. “The kingdom of God and His righteous-
ness” makes provision for every detail of life (Matt. 6:31-33). It is,
as Paul taught, “profitable for all things, holding promise for the
life that now is, as well as for that which is to come” (I Timothy
4:8). In a famous kingdom-parable, Christ authoritatively ex-
plained that the field (the kingdom) is the world (Matt. 13:38). In
the perspective of Scripture, God’s redeemed kingdom of priests
—the church (I Peter 2:9)—presently “reigns upon the earth”
(Revelation 5:9). Our confidence, calling, and prospect is encap-
suled in the wonderful song, that “the kingdom of the world has
become the kingdom of our Lord and of His Christ; and He shall
reign for ever and ever” (Revelation 11:15). The Messianic king-
dom must be seen, then, as this-worldly, external, and visible—
not merely internal to man’s heart and other worldly.

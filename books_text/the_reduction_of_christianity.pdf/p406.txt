366 The Reduction of Christianity

never been anyone. quite like him in the history of Christianity.
His book contains chapters on: Calvinism as a life-system, Cal-
vinism and religion, Calvinism and politics, Calvinism and
science, Calvinism and art, and Calvinism and the future. He
was very influential in the thinking of Cornelius Van Til. Obvi-
ously, he was writing long before Christian Reconstructionism
appeared,

Gary North, Backward, Christian Soldiers?: An Action Manual for
Christian Reconstruction. Tyler, Texas:. Institute for Christian Eco-
nomics, 1984, This popularly written paperback book is divided
into five sections: the war, the enemy, strategy, tactics, and the
duration. It summarizes the issues dividing humanists from
Christians and then goes on to demonstrate the nature of the con-
flict. Christians need a vision of victory and a specifically biblical
concept of law in order to replace the humanists in the drivez’s
seat of society. The section on tactics offers practical suggestions
on how to operate a newsletter ministry, a cassette tape ministry,
and the use of personal computers in Christian social action.

Gary North, Dominion and Common Grace: The Biblical Basis of
Progress. Tyler, Texas: Institute for Christian Economics, 1987.
This easily read book deals with one fundamental question: If
things are going to get better as the gospel spreads and serves as
the foundation of a Christian civilization, why will there be a mas-
sive rebellion by Satan’s human followers at the end of a millen-
nium of peace? North answers that in order for a successful
satanic revolt to take place, there first has to be a Christian civili-
zation to revolt against. The book answers numerous other ques-
tions, such as: Why are non-Christians able to be productive?
What is the relationship between biblical law and progress? What
went wrong in Van Til’s version of common grace? Why can’t
there be a “secret Rapture of the saints” before Christ returns in fi-
nal judgment?

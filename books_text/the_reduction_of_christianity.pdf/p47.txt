Orthodoxy: Setting the Record Straight 7

Qhristian faith? Historically, the boundaries of orthodox teaching
have been established by the Christian creeds. Historian J. N. D.
Kelly notes that the creeds that were formulated by church coun-
cils in the 4th century were “tests of the orthodoxy of Christians in
general” and “touchstone[s] by which the doctrines of Church
teachers and leaders might be certified as correct.” This is true
ecumenism, which, one author notes, is defined in some diction-
aries as “‘the doctrine or theology of the ecumenical councils.’ "4

Today many churches claim to be creedless. But in fact, every
church, whether it admits it or not, has a creed. As John Frame
writes,

If we have the Bible, why do we need a creed? That's a good
question! Why can’t we just be Christians, rather than Presbyter-
ians, Baptists, Methodists, and Episcopalians? Well, I wish we
could be. When people ask what I am, I would like to say, quite
simply, “Christian.” Indeed, I often do. And when they ask what I
believe, I would like to say with equal simplicity “the Bible.”
Unfortunately, however, that is not enough to meet the current
need. The trouble is that many people who call themselves Chris-
tians don’t deserve the name, and many of them claim to believe
the Bible. . ... We must ei! people what we believe. Once we do
that, we have a creed.

Indeed, a creed is quite inescapable, though some people talk
as if they could have “only the Bible” or “no creed but Christ.” As
we have seen, “believing the Bible” involves applying it. If you
cannot put the Bible into your own words (and actions), your
knowledge of it is no better than a parrot’s. But once you do put it
down into your own words (and it is immaterial whether those
words be written or spoken), you have a creed.#

12. J. N. D. Kelly, Early Christian Creeds (New York; David McKay, 1972), p.
205. Doctrine is not, of course, the only mark of a true church. An organization
may be theologically conservative, but if it does not administer the sacraments, it
is no church. Our emphasis here is on doctrinal orthodoxy, but we believe that
orthopraxy—biblical practice—is equally important.

13. J. Marcellus Kik, Ecumenism and the Evangelical (Philadelphia, PA: Presby-
terian and Reformed, 1958), p. 2.

14. John Frame, The Doctrine of the Knowledge of God (Phillipsburg, NJ: Presby-
terian and Reformed, 1987), pp. 304-5. Frame’s entire discussion on tradition and
creeds is helpful (pp. 304-314).

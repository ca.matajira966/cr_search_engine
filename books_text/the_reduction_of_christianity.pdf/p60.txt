20 The Reduction of Christianity

tually defined what he and others have already criticized. This is
where most of the confusion lies with those who had never even
heard of Christian reconstruction until they read Dave Hunt's
books, listened to him on a three-tape interview with Peter
Lalonde, or watched him on Rev. Jimmy Swaggart’s television
program “A Study in the Word.” Those who link Christian recon-
struction with the New Age Movement, Manifest Sons of God,
and aberrant theological views that are coming from the fringes of
charismatic teaching do not have a definitional handle on what re-
constructionists believe. Because reconstructionists are sometimes
listed with these other groups solely because of their victory-
oriented gospel message, it’s assumed that agreement can be found
on many points.* This simnply is not true. There is no organizational
or common theological tie. Even. Dave Hunt belatedly agrees that
Christian reconstructionists should not be linked with these groups.

Peter Waldron in his interview with Dave Hunt wants to drive
home this important point for his listeners. Hunt criticized the
views of certain leaders in segments of the charismatic movement,
but Waldron interrupted:

Peter Waldron: “Let's be careful. I am familiar with Dr. Rush-
doony. He's not teaching this.”

Dave Hunt: “Right.”

Peter Waldron: “Gary North is not teaching that.”

Dave Hunt: “Right.”

Peter Waldron: “Neither is Gary DeMar or any of the other peo-
ple who are often identified as the philosophical foundation of the
reconstruction movement.”

Dave Hunt: “Right. Right.”

Before evaluation takes place, terms must be defined. Many
critics take the straw man approach to debate, that is, forming “an
argument against a view that the opponent does not actually hold,
which, perhaps, no one actually holds.”> Albert James Dager, for

4, For example, Albert James. Dager, “Kingdont Theology, Part II,” Media
Spotlight (July-December 1986), pp. 8-20.

5. John Frame, The Doctrine of the Knowledge of God: A Theology of Lordship (Phil-
lipsburg, NJ: Presbyterian and. Reformed, 1987), p. 324.

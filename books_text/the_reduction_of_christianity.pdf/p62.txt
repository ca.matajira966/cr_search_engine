22 The Reduction of Christianity

charismatic groups?? The answer is very simple. Politics has be-
come the savior of the people. Reconstructionists write about poli-
tics and civil government in order to call Christians and non-
Christians back to their only Savior, the Lord Jesus Christ, be-
cause the State is not “the order of man’s salvation.”” We will
quote Gary North, a prominent Christian reconstructionist to
make our point:

Because the humanists have made the State into their agency
of earthly salvation, from the ancient Near Eastern empires to
the Greeks to Rome’s Empire and to the present, Christians need
to focus on this battlefield, but we must always remember that
political battles are important today primarily because our theolog-
ical opponents have chosen to make their first and last stand on the political
batilefield. Had they chosen to fight elsewhere, it would not appear
as though we are hypnotized with the importance of politics.
Christian reconstructionists are not hypnotized by politics; hu-
manists and pietists are hypnotized by politics. Nevertheless, we
are willing to fight the enemy theologically on his chosen ground,
for we are confident that God rules every area of life. He can and
will defeat them in the mountains or on the plains (1 Kings
20:28); in politics and in education, in family and in business."

‘This emphasis runs through all Dr. North’s writings. But Mr.
Dager creates a caricature of Christian reconstruction and domin-
ion theology when he writes that the “central doctrine of all, how-

9. “The Bible is replete with references to government and its rightful place
under God, with Daniel noting that God Temoveth kings and setteth up kings’
(Dan. 2:21) and appointeth over it [i-e., the kingdom of man] whomsoever he
wil? (5:21)... . Is the Lordship of Jesus Christ in American Government a
dream? Not if I can help it!” Donnie Swaggart, “The Lordship of Jesus Christ in
American Government,” Judgment in the Gate, ed., Richie Martin (Westchester,
IL: Crossway Books, 1986), pp. 80 and 89.

10. R. J. Rushdoony, The Nature of the American System (Nutley, NJ: Craig
Press, 1965), p. vii.

ii. North, “Editor's Introduction,” in George Grant, The Changing of the Guard:
Biblical Principles for Political Action (Ft. Worth, TX: Dominion Press, 1987), p. xx.

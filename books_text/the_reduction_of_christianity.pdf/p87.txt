Crying Woif? 47

These cautions are necessary. If a segment of the church of
Jesus Christ is drifting into the swift currents of doctrinal error,
then life rafts must be sent out to rescue them. Doctrinally mature
Christians should call the immature back to the truth, not sink
them in their struggle.

But Hunt’s books must be read on two levels. On the first level
he critiques “positive and possibility thinking,” “healing of memor-
ies,” “self-help philosophies,” and “holistic medicine,” and their as-
sociation with sorcery, scientism, shamanism, and aspects of the
burgeoning New Age Movement. Most of what Hunt writes
about these errors is quite accurate and should be taken to heart.

It is possible, however, that many of those who hold these
views are not consciously rejecting the orthodox faith.* Of course,
this does not lessen the damage that can be done. A number of
these ministers have little theological training.? Moreover, they
are rarely students of the history of theological debate. Their “no
creed but Christ” has gotten them into doctrinal hot water. Other

6, Robert Schuller, however, is one-who self-consciously rejects the reforma-
tional understanding of sin and grace. He tells us that to preach about sin and
man’s need of redeeming grace is part of the “old reformation.” Today, he says,
we need a gospel where man has a higher view of himself. Man needs a better
self-image and more self-esteem. This perspective is worked out in his view of
ethics. On “The Larry King Show,” he told the viewing audience that he knows of
no Bible verse that condemns homosexuality. See Gary DeMar, “Homosexuality:
An Illegitimate, Alternative Deathstyle,” The Biblical World View, Vol. 3, No. 1
(January 1987).

7. This is not to demean their ministries. The observation arises from their
evident lack of familiarity with well-known and respected Bible scholars, histor-
ians, and theologians. Seminary training has ruined many a fine and eager min-
ister of the gospel, but there is a corpus of literature available that seems to be ig-
nored by a large segment of the church. We pray that this book will intreduce this
material to a larger audience,

8. The “litde gods" controversy would not have arisen if time had been taken
to study the Council of Chalcedon (4.p. 451). R. J. Rushdoony writes: “The

_ Council of Chalcedon met in 451 to deal with the issue as it came to focus at the
critical point, in Christology. If the two natures of Christ were confused, it meant
that the door was opened to the divinizing of human nature. If the human nature
of Christ were reduced or denied, His role as man’s incarnate savior was reduced
or denied, and man’s savior again became thie state, If Christ’s deity were re-
duced, then His saving power was nullified. If His humanity and deity were not
in true union, the incarnation was then not real, and the distance between God

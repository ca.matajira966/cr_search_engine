52 The Reduction of Christianity

A Deafening Silence

A shift in eschatology has taken place. In general terms, there
has been a shift from pessimism to optimism.” For most of the
twentieth century, orthodox Christians who have held a premil-
lennial position have remained relatively silent regarding social
issues. One reason is that, as John Walvoord, former president
and now chancellor of Dallas Theological Seminary, writes, they
“know that our efforts to make society Christianized are futile
because the Bible does not teach it.”8 Much of this attitude has to
do more with current events than with interpreting the Bible.
There is also a reaction to 19th-century theological liberalism that
spawned the “Social Gospel” era. It too was optimistic, Today,
some dispensational premillennialists equate postmillennialism
with liberalism and the “Social Gospel.”

Hal Lindsey writes of postmiliennialism:

There used to be a group called “postmillennialists.” They believed
that the Christians would root out the evil in the world, abolish
godless rulers, and convert the world through ever increasing
evangelism until they brought about the Kingdom of God on
earth through their own efforts. Then after 1000 years of the insti-
tutional church reigning on earth with peace, equality, and right-
eousness, Christ would return and time would end. These people
rejected much of the Scripture as being literal and believed in the

17. “Pessimism” and “optimism” may not be the best terms to describe the
Christian’s hope. These words are sometimes used to describe a view of the
future that is based solely on the trends of the present. Thus, an optimist turns
pessimist when disaster strikes. By contrast, we mean by the phrase “optimistic
Christian” a Christian who, trusting in the promises of Scripture, is confident
that Christian civilization will wiumph visibly and institutionally in history. A
“pessimistic Christian” is one who believes that Scripture does not promise an
earthly and historical victory for God’s people, Because we are in the midst of a
transition, however, many Christians are optimistic about the future, but have
not yet formulated an eschatology that matches their outlook and activism. In
time, these Christians will conclude that the Bible promises long-term victory to
the church, or they will drift back into pessimism.

48. “Our Future Hope: Eschatology and Its Role in the Church,” Christianity
Today (February 6, 1987}, p. 5-I. But does the Bible teach that our efforts to
Christianize society are futile? This has not been proven biblically to our satisfaction.

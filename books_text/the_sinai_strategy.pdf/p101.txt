Sabbath and Dominion 75

originally creative sovereign. It would have meant that Adam ac-
cepted his position as a creature. The restriction placed upon Adam
by God meant that divinity is forever closed to man, Adam refused
to accept this. He could not abide in his God-given rest, precisely because it
was God-given. He wanted rest on his own terms. He wanted rest.as an
originally creative sovereign. He wanted his rest at the end of man’s
week, for God had inaugurated a day of rest at the end of His week.?

Resting the Land

On the seventh day, God rested. Adam should also have rested
(his first full day). Thus, for one day in seven, the land is to rest.
There was to have been no direct personal attention of man,or God
to the care of the land. The general personal sovereignty of God un-
dergirds all reality, but there was to. have been no visible manage-
ment of the land on that day. It, too, was to have rested. It, too, was
to have been free to develop apart from constant direct attention by
another. In this sense, nature was analogous to Adam, for God had
departed and left him physically alone.

This should have pointed to man that he was not ultimately sov-
ereign over nature. The land continued to operate without man’s ac-
tive supervision. If man rebelled against God, the land would come
under a curse, but if Adam remained ethically faithful and enjoyed
his rest, the land would suffer no damage from its day of solitude.
‘The forces of nature were never intended to be autonomous from
man, but they were nevertheless not entircly dependent on man.
This pointed to another source of nature’s daily operations: a law-
order created by God which did not require man or God to be
physically present for its continued operation.

After the fall of man, nature was cursed (Gen. 3:17-18), The
Mosaic Jaw imposed an additional form of sabbath on Israel: every

1. Gary North, The Dominion Covenant: Genesis (Tyler, Texas: Institute for Chris-
tian Economics, 1962), ch..5. I have subsequently come to the conclusion that Adam
sinned on the sabbath, rather than’on the day following the sabbath, in contrast to
the arguments I presented in the first edition of Genesis. The -“cighth-day
covering’ ~ the cighth-day circumcision of all Hebrew males (Lev. 12:3) and the
eighth-day scparation from the animal mother of the firstborn male (Ex. 22:30)—
inakes sense if we regurd the evening of the day as the beginning of the next day.
“And the evening and the morning were the first day” (Gen. 1:5b). When God came
at the end of the seventh day, He judged them and then covered them, in prepara-
tion for their departure from the garden. Whey would spend the evening and night of
the cighth day outside the protection of the garden. Thus, their second day (God's
eighth day) was their first day of labor outside the garden, the curse placed on their
assertion of autonomy.

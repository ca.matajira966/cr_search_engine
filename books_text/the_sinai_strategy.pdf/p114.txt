88 THE SINAI STRATEGY

life—life beyond the grave was manifested. On the evening of the
day of resurrection He took communion with His disciples. He spent
two days im the grave, and then He visibly triumphed over death.

A “day” of rest, and a “day” of resurrection life: this is what the seventh
millennium appears to offer. Furthermore, beyond it lies the eighth
“day,” which points to the culmination of creation: the new heavens
and new earth. The new creation was definitively established by
Christ's “eighth-day” resurrection. The church’s switch in the day of
rest-worship from Seventhday to Firstday meant that Kighthday is
the day of the new creation. In short, the symbolism fits together.
This will become increasingly apparent to a growing number of
Christians as the year 2000 draws near.

The idea that the six days of the week and six millennia are
linked symbolically was common opinion in the very early church.
Barnabas wrote concerning the sixth day of God’s creation: “Attend,
my children, to the meaning of this expression, ‘He finished in six
days.’ This implieth that the Lord will finish ail things in six thou-
sand years, for a day is with Him a thousand years. And He Himself
testifieth, saying, ‘Behold, to-day will be as a thousand years,’ There-
fore, my children, in six days, that is, in six thousand years, all
things will be finished,” It is not clear to: me whether he believed
that the final judgment would come 6,000 years after Christ’s era, or
that the whole period of fallen man’s life on earth lasts a total of
6,000 years. What is clear is that he believed that the 6,000-year
period is significant. Then comes the eighth day: “. . . when, giving
rest to all things, I shall make a beginning of the eighth day, that is, a
beginning of another world. Wherefore, also, we keep the eighth day
with joyfulness, the day on which Jesus rose from the dead.””

We should expect to find that people will begin to listen to
biblical teaching concerning such themes as death and resurrection,
work and rest, six days and one day, decline and dominion, defeat
and victory, Old Covenant and New Covenant, as we approach
what I call the sabbath millennium and millennial sabbath, Christians need
to be in a position to explain the nature of the transition to a new
stage in the manifestation of Christs new world order, which was
established during His lifetime, and was made visible by the fall of

16. The Epistle of Barnabas, ch. XV, in The Ante-Nicene Fathers, edited by Alexander
Roberts and James Donaldson, 14 vols. (Grand Rapids, Michigan: Eerdmans,
1973, reprint), L, p. 146.

17. Ibid., p. 147.

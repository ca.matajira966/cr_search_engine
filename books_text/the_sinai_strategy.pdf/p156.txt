130 THE SINAI STRATEGY

economic growth. Each partner can rely on the assistance of the
other, as well as the compassion of the other in times of crisis. This
frees up the minds of both partners, for each knows that the other is
there to help. What would otherwise be “uneven plowing” by one is
smoothed out by the effect of the “yoke”: the family goes forward,
day by day, despite the occasional failings of either of the partners.
While yoked together, neither partner can stray far without the
other; neither can go his or her own way without regard for the
other.

One of the most eloquent affirmations of the social value of mar-
riage comes from George Gilder. “he short-sighted outlook of pov-
erty stems largely from the breakdown of family responsibilities
among fathers. The lives of the poor, all too often, are governed by
the rhythms of tension and release that characterize the sexual ex-
perience of young single men... . Civilized society is dependent
upon the submission of the short-term sexuality of young men to the
extended maternal horizons of women. This is: what happens in
monogamous marriage; the man disciplines his sexuality and ex-
tends it into the future through a woman’s womb. The woman gives
him access to his children, otherwise forever denied to him; and he
gives her the product of his labor, otherwise dissipated on temporary
pleasures. The woman gives him a unique link to the future and a vi-
sion of it; he gives her faithfulness and a commitment to a lifetime of
hard work. If work effort is the first principle of overcoming poverty,
marriage is the prime source of upwardly mobile work.”

Gilder also reports that when marriages fail, the now-
unencumbered husband may revert to the lifestyle of singleness.
“On the average, his incorne drops by one-third and he shows a far
higher propensity for drink, drugs, and crime.” Thus, he concludes,
“The key to the intractable poverty of the hardcore American poor is
the dominance of single and separated men in poor communities.”6
Crime and social pathology in general increase when family cohe-
sion decreases. This has been documented in literally thousands of
sociclogical studies.’ The problem for the conventional social scien-
tist is that there are no generally acceptable measures that the civil
government can take that will increase the stability of the family. As

5, George Gilder, Wealth and Poverty (New York: Basic Books, 1981), p. 70.

6. ibid., p. 71.

7. Urie Bronfenbrenncr, “Origins of Alicnation,” Scientific American, Vol. 231 (Aug.
1974).

132 THE SINAL STRATEGY

What about low-income blacks—not just the hard-core poor?
These are people with incomes equal to or up to 25% above the
defined poverty level. “In 1959, low-income blacks lived in families
very much like those of low-income whites and, for that matter, like
those of middle- and upper-income persons of all races. Barely one
in ten of the low-income blacks in families was living in a single-
female family. By 1980, the 10 percent figure had become 44
percent.” This was higher than the percentage common among poor
whites. 5

Murray’s conclusion is eloquent, and it gets right to the point:
the presence of long-term poverty is not primarily a function of fam-
ily income. It is a function of morality, time perspective, and faith
regarding economic causes and effects. “Let us suppose that you, a
parent, could know that tomorrow your own child would be made an
orphan. You have a choice. You may put your child with an ex-
tremely poor family, so poor that your child will be badly clothed
and will indeed sometimes be hungry. But you also know that the
parents have worked hard all their lives, will make sure your child
goes to school and studies, and will teach your child that independ-
ence is a primary value. Or you may put your child with a family
with parents who have never worked, will be incapable of overseeing
your child’s education—but who have plenty of food and good
clothes, provided by others, If the choice about where one would put
one’s own child is as clear to you as it is to me, on what grounds does
one justify support of a system that, indirectly but without doubt,
makes the other choice for other children? The answer that ‘What we
really want is a world where that choice is not forced upon us’ is no
answer. We have tried to have it that way. We failed. Everything we
know about why we failed tells us that more of the same will not
make the dilemma go away.”!6

The defenders of modern socialism, or the welfare State, have
closed their eyes for three generations or more to the testimony of the
Bible, and also to the testimony of the statisticians. They cling to a
demonic view of stewardship, with the pseudo-family of the State at
the head of the financial household. The result has been the destruc-
tion of families and the productivity and social peace produced by
the family.

15. Idem.
16. Tbid., p. 233.

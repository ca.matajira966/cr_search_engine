The Yoke of Co-operative Service 133

Binding Contracts and Economic Growth

Covenants are binding. If men refuse to accept this truth, the
possibilities for economic development in a society are thereby re-
duced. The historic link between the biblical idea of binding cove-
nants and the West’s idea of binding contracts ‘is obvious enough.
The covenant of marriage supports the institution which was the first
to implement the division of labor. Without the predictability associ-
ated with contracts, the division of labor is hampered. Contracts in-
volve the sharing of the fruits of combined labor. Nowhere is this more
apparent than in the family unit. The basis of the idea of a contract,
like the idea of a covenant, is personal faithfulness. It begins with the
Trinity, extends to the relationship between Christ and His church,
undergirds ‘the family, and makes long-term economic co-operation
possible, The covenant is binding analogous to the way that a vow is
binding. A contract, which does not have the same authority as a
covenant or a vow to God, neveriheless is analogous. If the model of
permanence for contracts, namely, the vow or the covenant, is denied
true permanence, then how much less permanent are.contracts!

When J. D. Unwin examined the relationship between monog-
amy and cultural development, he found that in every society that he
studied, the absence of monogamy guaranteed the eventual stagna-
tion or rétrogression of that society.!7 The Bible provides us with the
information concerning man that allows us to understand why such
a relationship between monogamy and culture should exist. The
promise of external blessings is held out to those societics that cove-
nant themselves with God, and which enforce the terms of that cove-
nant, biblical law. The archetypal symbol of the rejection of God’s
covenant is adultery. The old business rule is close to the truth: “A
man who cheats on his wife will probably cheat on anybody.” It may
not hold true in every single instance of adultery by a businessman,
but when a society accepts adultery as “business as usual,” business
will not long retain its character as an'énterprise marked by binding
contracts. Honest business will become increasingly unusual, and liti-
gation costs will rise, as men seek to enforce contracts. This represents
needless waste — needless from the point of view of the dominion cove-
nant. Lawyers prosper and multiply —a sign of a collapsing culture.

17, J.D. Unwin, Ser and Culture (New York: Oxford University Press, 1934), Cf.
Unwin, “Monogamy as’a Condition of Social Energy” The Hibbert foumal, XXV
(July 1927); reprinted in The Jounal of Christian Reconstruction, IV (Winter 1977-78).

136 THE SINAI STRATEGY

tion: “Is God powerful enough to make a rock so heavy that He can-
not lift it?” The libertarians ask: “Is man sovereign enough to make a
contract so binding that he cannot break it?” The theist is not partic-
ularly bothered by the real-life applications of the God-rock para-
dox, but the libertarian faces several paradoxical problems that are
only woo real, First, how lang is a contract really binding, if lifetime
contracts are illegitimate? Forty years? Four years? Four weeks?
When does the absolute sovereignty of a man to make a binding con-
tract come into conflict with the absolute sovereignty of a man not to
be bound by any permanent transfer of his own will? Lifetime slav-
ery is immoral and illegal in a libertarian framework. A libertarian
must argue that such a contract should always be legally unenforcea-
ble, But what about a ten-year baseball contract? Second, and more
to the point, what about marriage?

Murray Rothbard, the most consistent and innovative of the lib-
ertarian economists, has stated his position with his usual clarity:
“.. a man cannot permanently transfer his will, even though he
may transfer much of his services and his property. As mentioned
above, a man may not agree to permanent. bondage by contracting
to work for another man for the rest of his life. He might change his
mind at a Jater date, and then he cannot, in a free market, be com-
pelled to continue working thereafter. Because a man’s self-owner-
ship over his will is inalienable, he cannot, on the unhampcred mar-
ket, be compelled to continue an arrangement whercby he submits
his will to the orders of another, even though he might have agreed
to this arrangement previously.”” In the footnote to this final sen-
tence, he adds: “In other words, he cannot rake enforceable con-
tracts binding his future personal actions. . . . This applies also to
marriage coniracts. Since human self-ownership cannot be alicnated, a
man or a woman, on a free market, could not be compelled to con-
tinue in marriage if he or she no longer desired to do so. This is re~
gardless of any previous agreement. Thus, a marriage contract, like
an individual labor contract, is, on an unhampered market, termin-
able at the will of either one of the parties.”®

The libertarian concept of absolute self-ownership as the founda-
tion of all economic exchanges sinks into oblivion when it hits the

22. Murray N. Rothbard, Man, Economy, and State (New York: New York Univer-
sity Press, [1962] 1975), I, p. 142. (J am using the original Van Nostrand edition,
which was printed in two volumes.)

23. Ibid., I, pp. 441-42; footnote 35. Emphasis in original,

The Rights of Private Property 161

how economics would relate to such criteria. (Biblical economists,
not being morally neutral, are not equally hampered in answering
these questions.)

Property, from this perspective, is basically a “bundle of rights.”
Again, citing Demsetz: “When a transaction is concluded in the
marketplace, two bundles of property rights are exchanged. A bun-
die of rights often attaches to a physical commodity or service, but it
is the value of the rights that determines the value of what is ex-
changed.”® The control over such rights necessarily involves the
right to exclude others from the value of the rights over time. It is here
that the civil government must take special care: rights are not abso-
lute, but they should be sufficiently familiar to acting men that these
men can make valid predictions concerning the future —the future ac-
tions of competitors, as well as of the civil government. The reduction
of uncertainty is of paramount importance. As Cheung writes: “The
transfer of property rights among individual owners through con-
tracting in the marketplace requires that the rights be exclusive. An
exclusive property right grants its owner a limited authority to make a
decision on resource use so as to.derive income therefrom. To define
this limit requires measurement and enforcement. Any property is
multidimensional and exclusivity is frequently a matter of degree,
But without some enforced or policed exclusivity to a right of action,
the right to contract so as to exchange is absent.”2! The civil govern-
ment must protect property.

The Market for Knowledge and Uncertainty

The establishment of property rights is therefore fundamental in
any system of voluntary exchange. Men rely on the division of labor
to increase their own economic output, and therefore their income.
Of critical importance is the exchange of information, including the ex-
change of uncertainty. Those who want to buy more uncertainty, and
therefore open up to themselves the opportunity for greater profit,
are enabled to do so by purchasing higher-risk property from those
who are willing to settle for a more guaranteed return.*? Some peo-

30. Demsetz, “Toward a Theory of Property Rights.” American Economic Review
(1967}; reprinted in Economics af Property Rights, p. 31.

31, S. Gheung, “The Structure of a Contract and the Theory of a Non-Exclusive
Resource,” Journal of Law and Economics (1970); reprinted in ibid., p. 27.

32. Temploy Frank Knight's distinction between risk and uncertainty, Risk is a sta-
tistically calculable class of future cvents, such as the deaths within a particular age
group. Mortality tables used by life insurance firms are examples of statistical calcu-

  

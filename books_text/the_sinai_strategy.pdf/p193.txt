The Rights of Private Property 167

communal owners can reach an agreement that takes account of
these costs.”* Then Demsetz offers a stunning insight into the social
function of an owner of a private property right: the owner as broker
between generations. “In effect, an owner of a private right to use land
acts as a broker whose wealth depends on how well he takes into ac-
count the competing claims of the present and future. But with com-
munal rights there is no broker, and the claims of the present genera-
tion will be given an uneconomically large weight in determining the
intensity with which the land is worked. Future generations might
desire to pay present generations enough to change the present in-
tensity of land usage. But they have no living agent to place their
claims on the market,”*

By its very nature and time perspective, familistic capttal is privately
owned capital. Privately owned capital necessarily involves the
defense of private property. The stewardship of resources should be super-
vised by the most intensely committed social unit, the family. It is not the only
legitimate institution of ownership,* but it is unquestionably the
most universally recognized ownership institution historically, and it
is the social unit to which God originally announced the dominion
covenant. By establishing a tight (though imperfect) relationship be-
tween costs and benefits, private property rights encourage men to
count the costs of their actions. The counting of costs is a biblical re-
quirement (Luke 14:28-30), If a man overworks his soil, he or his
heirs will pay the price. If his animals overgraze the land, -he or his
heirs will suffer reduced income later. He cannot pass on his costs so
easily to those outside his family, which therefore encourages him to
examine the effects, including long-run effects, of his present deci-
sions. He seeks a profit—an excess of income over outgo—and he
cannot ignore costs. He will waste fewer of God’s resources because
of the profit incentive, compared to the communal ownership or
State ownership systems, where each man is offered direct incentives
to waste the common asset while profiting personally from the im-
mediate use of the asset. There can be commitment to the goals of
other social units besides the family, but no institution historically

44, Idem.

45, Did., pp. 38-39.

46. The corporation is another important institution for holding property, but
corporate shares of ownership are held by heads of households primarily, or by
agents of heads of households: banks, retirement funds, mutual funds, etc. Thus,
these are delegated sovereignties,

The Rights of Private Property 171

house? Is the argument which is levelled at the tax collector equally
applicable to my distant factory? Competition, confiscation, and co-
operation are sometimes very difficult concepts to distinguish—not
always, or even usually, but sometimes.

The Bible provides us with an example of “spillover effects” and
what to do about them, If a man starts a fire on his property, and the
fire spreads to his neighbor’s property, the man who started the fire is
responsible for compensating his neighbor for the latter’s losses (Ex.
22:6). Obviously, this invasion of property is- physical, rather than
merely competitive and economie in nature, and therefore the
fire-starter is liable. The destruction of property in this instance is
physical and immediate; the victim actually loses part of his crop.
But what about noise pollution; where the man’s house is not burned,
but its market value drops as a result of his neighbor’s noisy factory?
This would secm to be. covered by the casc-law on fire, since sound
waves are physical phenomena, just as sparks are. But when the loss
is exclusively economic, without physical invasion, the Bible is silent:
there is no law that would require the successful innovator to com-
pensate those who lost money because of the introduction of new
production techniques or new products. Alchian’s analysis would
seem to apply: “Although private property rights protect private
property from physical changes chosen by other people, no immun-
ity is implied for the exchange value of one’s property.”

Is it fair, then, to equate the economic effects of the State's collec-
tion of taxes and the industrialist’s pollution? It depends on the level
of taxation. If the State attempts to extract taxes greater than 10% of
income, thereby equating its sovercignty with God's sovereignty (the
tithe), then the answer is yes, the two should be equated. Both forms
of economic redistribution rest on illegitimate violence. The tax callec-
tor extracts money or goods from the citizen upon. threat.of im-
prisonment or outright confiscation of capital assets. Thus, when the

53. Armen Alchiun, “Some Economics of Properiy Rights,” 1d Politico (1965); re-
printed in Alchian, Economic Forces al Work (Indianapolis, Indiana: Liberty Press,
1977), p. 131, His conclusion, however, that ty making pornographic pictures and
selling them must be:free from legal restraint follows only if you assume that 1)
there are no absolute standards of morality, 2) no God, and 3) no social conse-
quences for immoral behavior —in shart, no consequenecs imposed on many mem-
bers of soviety by God’s judgment, Most economists erroneously make all three as-
sumptions, When we speak of the legitimacy of imovation, we must always have in
mind this qualification: ". . . assuring the innovation or Lansiction is not singled out
by the Bible as being defined by God as perverse, and also illegal.”

 

   

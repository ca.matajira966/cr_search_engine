Other books by Gary North

Marx's Religion of Revolution, 1968

An Introduction te Christian Economics, 1973

Foundations of Christian Scholarship (editor), 1976
Unconditional Surrender, 1981

Successful Investing in an Age of Envy, 1981

The Dominion Covenant; Genesis, 1982

Government By Emergency, 1983

The Last Train Out, 1983

Backward, Christian Soldiers?, 1984

75 Bible Questions Your Instructors Pray You Won't Ask, 1984
Cained Freedom: Gold in the Age of the Bureaucrats, 1984
Moses and Pharaoh: Dominion Religion Versus Power Religion, 1985
Negatrends, 1985

Conspiracy: A Biblical View, 1986

Unholy Spirits: Occultism and New Age Humanism, 1986

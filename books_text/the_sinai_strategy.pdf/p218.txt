192 THE SINAI STRATEGY

makes necessary the conveying of information concerning new op-
portunities in effective packaging. Sellers cannot force buyers to buy. 1

Write a newspaper column about a new book, unless it is a book
review in a major publication that caters to the book-buying public,
and few sales will result. Publish a well-designed ad for the same
book in the same newspaper or magazine, and sales could be con-
siderable. Why the difference? Critics of advertising ignore the ob-
vious: people read ads with minds open to motivation; they seldom read
newspaper columns in such a way.

Then there is the filtering process of the mind. Men screen out
vastly more data than they notice, let alone absorb, or even less, act
upon. Habit screens out new opportunities. So do many other mental
processes that we do not understand yet. Some way to “punch
through” the mental veil of indifference must be found. This is what
advertising is all about. It is not economically sufficient merely to in-
form people concerning opportunities; advertising must motivate
them to act. We are not hypothetical Greek rationalists, who always
do the right thing if we have sufficient knowledge. We are not saved
by knowledge, nor are we exclusively (or even mainly) motivated by
sheer intellectual awareness, We are motivated by other aspects of
human personality: fear, greed, joy, hope, love, humor, imagina-
tion, respect, and the desire to be the first person on the block to own
one. We are motivated by altruism, too, but you will receive far
more donations to “save the children” if you include a picture of a
waif and include a brief description of the wait’s plight. People re-
spond to real-life situations, or perceived real-life situations. They
respond to emotions, to empathy, to the concrete—not to the
abstract. They are not so ready to respond to statistical summaries of
disaster-laden foreign nations. They want stories and photographs.

The Non-Primacy of the Intellect

The intellectual’s favorite myth, the primacy of the intellect, is
seldom taken seriously by advertisers, because advertisers know that
human beings are multifaceted creatures, not just austere, pristine

10. The ability of consumers to resist the persuasion of advertisers is admired by
Galbraith: “he power td. influence the individual consumer is not, of course,
plenary, It operates within limits of cost... , That the power to manage the in-
dividual consumer is imperfect must be emphasized.” Galbraith, Economics and the
Public Puipose (Boston: Houghton Mitflin, 1973), p. 138. The title of the chapter,
however, tells the story: “Persuasion and Power.”

xxii THE SINAI STRATEGY

Conclusion

The ten commandments are divided into two sections of five
commandments each. The first section is priestly, while the second is
kingly or dominical. Both sections reflect the same five-part aspect of
the Deuteronomic covenant structure.

Is this structure permanent? Sutton traces it back to Adam,
Noah, and Abraham. He traces it forward to David, Malachi, and
Jesus’ Great Commission. I find it also in the temptation of Christ by
Satan, and in the trial of Jesus by the Jewish leaders.

Satan’s Temptation of Jesus

The devil came to Jesus in the wilderness, and in three different
challenges, he tempted Him (Matt. 4).

First Commandment: transcendence/immanence: Satan asked Jesus to
worship him. This would have been a violation of the command-
ment to have no other gods before the God of the Bible. Jesus re-
fused, citing Deuteronomy 6:13.

Second Commandment:  hierarchy/authority. Satan asked Jesus to
throw Himself off the top of the temple. Satan cited a promise of
God as justification (Ps. 91:11). Jesus’ reply: do not tempt God (Deut.
6:16). This is a matter of obedience. The next verse in Deuteronomy
6 is significant: “Ye shall diligently keep the commandments of the
Lorp your God, and his testimonies, and his statutes, which he hath
commanded thee” (6:17). Again, He answered Satan’s request with
God’s requirement to obey God by obeying God's law. He honored
the covenantal principle of hierarchy/authority.

Third Commandment: ethics/dominion: Satan asked Him to com-
mand stones to turn into bread, This would have been a transgres-
sion of the third commandment: misusing the spoken word to gain
power over creation, which is an imitation of God. He tempted Jesus
to become a magician. Would He, as perfect humanity, feed Himself
in His hunger by means of a word of magic? The answer was appro-
priately ethical: man lives by the word of God, a citation of Deuteron-
omy 8:3, the chapter which restated the terms of the covenant to
Israel before they left the wilderness and entered the promised land.

Fourth Commandment: judicial/evaluational: Satan cited Psalm 91:11
in his temptation at the temple. He said, “for it is written.” This was
a variation of his temptation of Eve, where he also partially cited the
word of God. Was he citing God’s word correctly? He was promising

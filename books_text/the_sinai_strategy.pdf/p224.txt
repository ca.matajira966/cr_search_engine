198 THE SINAI STRATEGY

Downward Social Mobility

Another aspect of this jealousy is overlooked by most commen-
tators. Covetousness can also be directed downward, toward those who
have fewer goods, and therefore fewer responsibilities. This can be
seen in the social phenomenon known as the drop-out mentality. In the
late 1960's, for example, the sons and daughters of the middle classes
and the wealthy were on the road, all over the world. They adopted
the dress codes of poor people, wearing the faded blue denim jeans
of field hands.? They would even bleach their new, dark blue jeans,
to give them an instant fade.* Blue jeans became so associated with
Western culture that they commanded a high price —a black market
price —in Iron Curtain nations, especially the Soviet Union. Young
people adopted the life style of nomads—unwashed drifters who
refuse to face the responsibilities of dominion. Those with wealth
and responsible callings became “primitive,” in an attempt to escape
the burdens associated with economic stewardship. They wanted
others to take the risks and bear the responsibilities.

The Bible prohibits men from escaping lawful callings, unless
they are upgrading their responsibilities. A slave is authorized to
take his freedom, if and when it is voluntarily offered by his master,
either free of charge or by sale (I Cor. 7:21). The idea is to extend
God's rule into every area of life, and men are not to turn their backs
on this task simply because a particular calling looks as though it
would involve too much responsibility. /¢ is tmporiant for each person in-
volved to evaluate his own capabilities accurately, and then to maich those
capabilities with his calling before God—his highest, most productive calling.
God calls men to be imitators of His son, Jesus Christ, to conform
themselves to Christ’s image (I Cor. 15:49). They are to work out the
salvation that God gives them, and they are to do this with fear and
trembling (Phil. 2:12), This kind of steady improvement involves up-
ward mobility: spiritual improvement, at the very least, but also eco-
nomic and social mobility. The individual may not see himself ad-
vancing economically, but over generations, the spiritual heirs of a

3, The original blue jeans were sold by the Levi Strauss Company during the
gold rush days in California in the 1850's. Hence the almost generic name, “Levis.”
The pants were marketed as being especially durable, a desirable feature in the
opinion of gold miners.

4. The 1970's brought a fusion of symbols: “designer” jeans. ‘hese were blue
denim jeans that bore the name of famous rich people or famous designers, and
brought three or four times the price of a pair of normal blue jeans.

 

212 THE SINAI STRATEGY

ance of the basic principles of the ten commandments is both neces-
sary and sufficient for the creation of a capitalist economy. (Humanistic
free market economists reject the first assertion — “necessary” — since
they want a free market without God, while “Christian” socialist
theologians reject the second —sufficient — since they want God with-
out a free market.) Whenever the ten commandments are enforced
by all agencies of human government, men will gain freedom. Eco-
nomic freedom of contract and freedom from excessive taxation and
bureaucratic interference produce that social order which we call the
market society. This is why the Christian West was the first society to
create national and regional economies called capitalistic. This is
why long-term economic growth has come only in the West, and in
those nations that trade with the West and have imitated some of its
institutional and legal arrangements, most notably Japan. But if the
goal of the Bible is social peace under God’s covenants, and if the
free market economy has been not only the logical result of the ten
commandments but also the historic product of Christianity, then a
controversial conclusion follows: biblical social order and free market capt-
talism are a “package deal.” Socicties carmot attain the kind of long-
term, compounding expansion which is required by the dominion
covenant without the social, moral, and legal foundations that are
established by law in the ten commandments. Humanistic free mar-
ket economists refuse to believe this, and so do “Christian” socialists.

The Ten Commandments and Capitalism

The ten commandments as a unified whole offer mankind the
moral basis of a progressive society. I am not arguing that it is only
the eighth commandment, with its prohibition against theft, which
sets forth such a view of private ownership. All ten commandments
have provided mankind with the faith which has produced Western
prosperity:

God as the sovereign owner of the creation
Faith in the healing power of God's law
Personal stewardship before God and other men
Legal responsibility for onc’s actions

Faith in predictable, pérmancnt laws

Faith in economic cause and effect

Faith in ethical power over magical power
Faith in work rather than luck

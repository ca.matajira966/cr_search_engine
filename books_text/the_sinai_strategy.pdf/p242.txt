216 THE SINAL STRATEGY

Revels comments are on target: “Not only do democracies today
blame themselves for sins they have not committed, but they have
formed a habit of judging themselves by ideals so inaccessible that
the defendants are automatically guilty. It follows that a civilization
that feels guilty for everything it is and does and thinks will lack the
energy and conviction to defend itself when its existence is threat-
ened. Drilling into a. civilization that it deserves defending only if it
can incarnate absolute justice is tantamount to urging that it let itself
die or be enslaved.”” This guilt-induced self-flagellation is made even
easier for humanism-influenced “radical Christians,” for self-flagella-
tion has been characteristic of Anabaptist groups from the begin-
ning, as have both pacifism and defeatism. By failing to understand
and rest upon the doctrines of definitive sanctification and progres-
sive sanctification, they have become guilt-ridden and impotent.
Definitive sanctification teaches that Jesus’ perfect moral life is im-
puted to His followers at the point of their conversion. Progressive
sanctification teaches that converted people are required by God to
work out their salvation with fear and trembling in terms of biblical
aw, even though they are imperfect in and of themselves. Their im-
perfect work is accounted righteous because of their definitive sancti-
fication. It builds up. over time, until the day of final judgment and
final sanctification .®

But “radical Christians” do not understand these doctrines. They
are visibly overwhelmed with guilt concerning their own ineffective-
ness, and the ineffectiveness of “Christianity” in not putting a stop to
the “moral evil” of capitalism. They have also been overwhelmed by
the seeming impossibility of godly dominion. After all, we live in a
sinful world. We are sinful. So how can we—pitiful, guilt-ridden
worms that we are—take dominion? Aren’t we sinful perpetrators
of injustice? Aren’t we the sinful religious accomplices of the evil elite
which rules (and profits from) the greedy and corrupt capitalist sys-
tem? Oh, let us escape to the communal farm, where the morally
polluted efficiency of mass-producing, price-competitive industrial-
ism is kept out of our sight (even though we benefit from it 24 hours
a day)! Oh, let us refuse to fight in wars to defend our miserable
freedoms, even if the Communists should invade. (This is the paci-

7. Jean-Francois Revel, How Democracies Perish (Garden City, New York: Double-
day, 1984), p. 10.

8. Gary North, Unconditional Surrender: God's Program for Victory (2nd ed.; Tyler,
‘Texas: Geneva Divinity School Press, 1983), pp. 43-47.

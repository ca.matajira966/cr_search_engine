The Economie Implications of the Sabbath 235

in trade. Sucli sabbath violations would have been difficult to detect.

What ‘about the use of such sabbath-produced milk by the fam-
ily? This is an important question. If personal family use of the eco-
nomic output of sabbatical “acts of mercy” (to the cows) is legitimate,
then the definition of what constitutes profit-seeking must be nar-
rowed. Engaging in commercial trade would be prohibited, but en-
gaging in. intra-household trade would not; one family member
milks the cow, another cooks the food, another washes the dishes,
and so forth. From the point of view of human action—exchanging
one set of conditions for another set—the intra-family exchange
seems to be equally profit-seeking, but perhaps not from.the point of
view of Old Testament sabbath legislation. ‘I'he milk could be sold
the next day. Wouldn’t this constitute a violation of the sabbath? It
certainly appears that way. But to consume the milk directly thereby
increases ‘the family’s consumption as surely as the income gained
from the sale of the milk would increase it. What is the economic
difference, in terms of family income? More to the point, what is the
biblical difference, in terms of the specific application of the law of
the sabbath?

The strict sabbalarian would have to argue that the milk should
be given away. Such a person is a defender of what Lewis Bulkeley
has called “the marathon sabbath.” But is it the sabbath which God
required of His Old Testament saints, let alone His New Testament
saints? Unquestionably, the Old Testament did not prohibit output
of effort as such; cows deserved to be milked, as an act of mercy, an
act of giving rest (Ex. 23:12). But what about income which was the by-
product (i.e., unintended product) of such merciful labor? Should it
have been given to the poor, or to houschold animals, bul kept away
from human family members? Or is giving food to_one’s own family
itself an act of mercy?

Tf giving milk to onc’s own family or domestic animals is an act of
mercy, then it is an act of mercy which has unintended economic
consequences, namely, an increase of consumption which is not paid
for by. increased output (more milking) or more thrift (reduced con-
sumption) during the days preceding the sabbath. Feeding one’s
family or animals with milk produced by sabbath milking would
then be understood as being fundamentally different from gathering
sticks for a fire on the sabbath, for sticks had to be gathered during
the workweek and stored up for use on the sabbath. But wouldn’t
this “anti-stick-gathering” requirement have applied equally to milk-

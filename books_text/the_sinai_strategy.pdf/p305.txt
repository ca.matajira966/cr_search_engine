The Economic liplications of the Sabbath 279

Rescheduling Worship

It is not normal in a Christian nation to find that most occupa-
tions of necessity involve labor on seven consecutive days. In fact,
most of them are five-day occupations, leaving time for goofing off
Saturday (that terrible Roman word for Seventhday), to watch
televised sporting events all day in violation of God’s one-six pattern
for the workweek. But a few members are called to occupations that
require Sunday work at least occasionally. And, by the grace of God,
most pastors say nothing if the practice does not get out of hand.

Rescheduling Passover

We find a parallel in the case of the Israelite who was not able to
celebrate the Passover in the specified month, the first month of the
year.

Speak unto the children of Israel, saying, If any man of you or of your
posterity shall be unclean by reason of a dead body, or be in a journey afar
off, yet he shall keep the passuver unto the Lorp, The fourteenth day of the
second month at even[ing] they shall keep it, and eat it with unleavened
bread and bitter herbs (Num. 9:10-11}.

Why would any man be on a journey? What would a Hebrew be
doing outside the nation? He might be on some sort of a foreign
policy mission, serving as an ambassador of the king. He might have
been an evangelist. More likely, he would have been a merchant.
His occupation kept him away from Jerusalem in this important
month, For those who had a legitimate excuse, the Passover could be
celcbrated in the second month of the year. Not mariy people would
have had a legitimate excuse. This was no license for missing the
Passover feast. “But the man that is clean, and is not in a journey,
and forbeareth to keep the passover, even the same soul shall be cut
off from among his people: because he brought not the offering of the,
Lorp in the appointed season, that man shall bear his sin” (Num.
9:13). The penalty was excommunication from the congregation.
The first-month Passover was normally binding, but those on
journeys were exempted.

‘The law of God provided a means of satisfying the requirement
of the Passover in certain instances when, through no fault of the in-
dividual, it was impossible or unlawful for him to enter into the cele-
bration. The Old Testament law was not perfectionist in nature. It

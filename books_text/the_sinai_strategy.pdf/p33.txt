Introduction 7

Proof Texts, Blueprints, and Economic Antinomianism

What the ten commandments set forth is a strategy. This strategy
is a strategy for dominion. ‘The general principles of the ten command-
ments summarize the whole of biblical law. The case-law applica-
tions of Exodus 21-23 go on to illustrate ways in which the ten com-
mandments are to be applied.” But the Decalogue itself is the master
plan, the blueprint for biblical social order. These laws have very defi-
nite economic implications. This sort of thinking is foreign to virtu-
ally all modern Christian social and economic thinkers, whether
conservative or liberal, Protestant or Catholic.

If I were to offer a single sentence of warning with respect to the
misuse of the Bible by modern scholars, it would be this: beware of
doubletalk and outright gibberish. 1 will put it even more bluntly: if you
cannot understand what a theologian writes concerning a perfectly
plain passage in the Bible, trust your instincts; you are probably be-
ing conned by a professional. These hypocrites for over two hundred
years have made a lifetime occupation out of hiding their radical
ideas behind a mask of orthodox language. They want to be low-risk
revolutionaries, fully tenured, with their salaries provided by un-
suspecting Christian sheep. Furthermore, they are, almost to a man
(person?), desperate for public acceptance by secular scholars. They
are humanists by conviction, even though they operate in the
churches. H they forthrightly proclaimed the doctrines of the historic
Christian faith without compromise, they would be ridiculed by hu-
manist scholars. They fear this above all. So they write endless
reams of convoluted language in order to hide the academic ir-
relevance of their concepts. (German theology is especially afflicted
by this verbal constipation.) Their concepts are dangerous to or-
thodoxy and irrelevant to humanism, except as a tool of confusing
the faithful. Liberal theologians are simply examples of what Lenin
used to call “useful idiots.”* They are middlemen for the humanists
in a great deception of the faithful. They have been described best by
David Chilton: “Modern theologians are like a pack of dogs who
spend most of their time sniffing each other’s behinds.”

7. James B. Jordan, The Law of the Covenant: An Expasition of Exodus 21-23 (Tyler,
Texas: Institute for Christian Economics, 1984).

8, John P. Roche, The History and Impact of Marxist-Leninist Organizational Theory:
“Useful Idiots,” ‘Innocents’ Clubs,” and “Transmission Belts” (Cambridge, Massachusetts:
institute for Foreign Policy Analysis, 1984).

308 THE SINAI STRATEGY

first attention?” Jesus’ response headed off all such questions, The sec-
ond principle is analogous to the first. Men are made in God’s image.
We should therefore love our fellow man. But how much love is
proper? Certainly, not the love we show to God, We owe him every-
thing. But a good test of how much we love another creature is to esti-
mate how much we love ourselves. Jesus assumes that each man
wants to do his best for himself. Men are always “looking out for
Number One.” So, He said, look out for your neighbor just as you
look out for yourself. You are a man; he is a man; both of you deserve
the same consideration, for both of you are made in God’s image.

Love and the Law

The question related to the law. The answers spoke of love, Are
these two in opposition? Obviously not. Jesus always dealt faithfully
with the questions of his questioners.. This is why they were always
struck dumb. They were incapable of replying, precisely because
Jesus’ answers were flawless. There was never anything more to say
without either agreeing with Him or winding up in opposition to the
Old Testament. Therefore, when Jesus answered the lawyer's ques-
tion concerning the greatest of the laws, He was saying clearly and
unmistakably that all the laws of God are a working out of the princi-
ple of love—theocentric love first of all, and neighborly love second.
If these laws are applications of the principle of love, then how can
they be in opposition to love?

The lawyer recognized this. He did not reply. By focusing on the
loving aspect of love, Jesus removed the question from the realm of
legalistic debate. You love God with everything you are and have;
therefore, you also must love your neighbor as yourself, But how do
we love our neighbors? Clearly, by treating them as faithfully as we
treat ourselves. By giving them the same “benefit of the doubt” in a
dispute that we give ourselves. In short, this is the so-called golden
rule: “Therefore all things whatsoever ye would that men should do
unto you, do ye even so unto them: for this is the law and the proph-
ets” (Matt. 7:12). This is the biblical version of the more common
phrase (which is not found in the Bible): “Do unto others as you
would have others do unto you.”

The Sermon on the Mount

Jesus’ sermon on the mount is a commentary on God’s “sermon”
on the mount to Moses. This is what modern Christians have failed

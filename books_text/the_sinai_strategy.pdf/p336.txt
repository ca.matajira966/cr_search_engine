310 THE StNAI STRATEGY

Notice His frame of reference here. Your enemies. Those who use
you.despitefully. Jesus was not saying that the enemies of Gad should
be allowed to escape the lawful punishment of their crimes against
man and God. He was not repudiating the ten commandments, that
He had affirmed categorically a few moments before (vv. 17-19),
What He was saying is this: in your dealings with all men, treat
them as you would treat your friends, If your friends violate God's
law, you do not repudiate the law. If your friend commits murder,
you do not allow that murder to go unpunished, if you have informa~
tion that would convict him. To do so would be to become an ac-
complice to the crime. Jesus is taking this principle of law enforce-
ment right to the heart of each man. If you yourself commit murder,
you must turn yourself in to the civil authorities, just as you would
turn in your worst enemy. You must honor God’s law. Paul announced
this principle forthrightly when he was in court: “For if I be an
offender, or have committed any thing worthy of death, I refuse not
to die” (Acts 25:11a). To refuse not to die is to love God, and to love
the righteousness of God, more than you love your own life. This is
the essence of conversion: “He that findeth his life shall lose it: and
he that loseth his life for my sake shall find it” (Matt. 10:39),

The New Testament tells us to love our neighbors as ourselves.
We are to deal with them in terms of God’s law. We owe them such
fair dealing. “Love worketh no ill to his neighbour; therefore love is
the fulfilling of the law” (Rom. 13:10). But what is “il”? It is
unrighteous dealing. How do we test what is “iil” and what isn’t? By
the standard of the law of God. This is why love fulfills the law. It is
not that love overcomes the law, or annuls the law, or abrogates the
law, Love fufflls the law, just as Jesus Christ fulfilled the law. He did
not go on to deal unlawfully with men. How could He? He was the
author of the \aw. Nor should we go on to deal unlawfully with men.

Jesus was not denying the legitimacy of biblical law. On the con-
trary, He was affirming biblical law, We love God first; God com-
mands us to keep His word; therefore, we must enforce the law on
ourselves. We start with ourselves because we have more knowledge
of ourselves and more responsibility over ourselves. This is the
meaning of progressive sanctification. Jesus was not calling us to ignore
biblical law; He was calling us to enforce it first on ourselves, before
we enforce the same laws on others.

Judge not, that ye be not judged. For with what judgment ye judge, ye
shall be judged: and with what measure ye mete, it shall be measured to

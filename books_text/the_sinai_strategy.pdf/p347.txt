Covenantal Law and Covenantal Love 321

God!’s imputation to them of Christ’s perfect keeping of this law is the
only foundation of their salvation. ‘They are to judge their own acts,
both internal (mental) and external, in terms of this standard. They
are to judge the external acts of other people by this same standard.
What other standard could regeneraie men possibly use? We must constantly
ask ourselves, and endlessly ask the critics of the New Testament au-
thority of Old Testament law: By what other standard? If we love
Christ, we will keep His commandments ( John 14:15).

Only if Christ's commandments were different from the com-
mandments God gave to Moses could we legitimately conclude that
the love of Christ is different from the love of God. Only then could
we conclude that obedience to Christ is. different from obedience to
God. But there can be no difference; the God who created every-
thing is the divine Logos, who was incarnated as the perfect human,
Jesus Christ (John 1). Thus, any attempt to create a dualism be-
tween God’s Old Testament law and Christ’s New Testament law is
simultaneously an attempt to offer a two-God theory of history, with the
Old Testament God different from a New 'lestament God. “Lhis was
attempted by Marcion in the second century, and he was condemned
as a heretic. An implicit two-God theory has been proclaimed for cen-
turies by Christian mystics and Anabaptists, and also by modern
fundamentalists and evangelicals. The results have been culturally
disastrous: the anfi-dominion principle in action.

There is no contradiction between the ten commandments and
the sermon on the mount. God's love is manifested to us in the law,
which is the law of life, There is grace in God's law.

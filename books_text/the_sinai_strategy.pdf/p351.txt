Social Antinomianism 325

two criticisms. Troost argues from this passage in the following man-
ner: “Thus did the practice of this church confirm the preaching of
the gospel with signs and powers. Property relations were set free
from their natural self-willed self-assertion and employed for loving
service of God and neighbor” (p. 33). Now what are we to conclude
from all of this? The Bible, Troost has argued, does not give us any
“data, points of departure or premises from which to draw logical
conclusions relevant to’ modem society’s socio-economic problems,
including property relations.” Nevertheless, we are now told that the
early Christians “put ownership and property rights back in the
place where they belong,” and Troost obviously expects us to take
this example seriously. But on his grounds—on the presuppositions
upon which he began his analysis ~ why should we pay any attention
to what the early church did?. Troost wants us to make an application
of the church’s practice in today’s world, but why should we, if the
Bible is not relevant to present-day economic and social problems?
Does he mean that we should create a society in which property is
held in common (socialism) and yet at the same time believe that we
are not living under socialism (since ‘property, he says, was not
“abolished”)? The whole argument is vague, but it appears that this
is Troost’s conclusion. If it is not, then I do not understand what he is
talking about.

He refers to the fact that the early church “did not abolish prop-
erty, nor yet the means of production (c.g., landed estates).” Private
property was preserved in the sense that it was not sold to the State,
true enough, They sold some of their fixed assets to non-Christians
and deposited the wealth in the common treasury. They also gave
some of their other goods directly to the Christian community, But
this means that in order to follow their example in our day, we must
sell our goods to unbelievers, thus making ourselves perpetual wage-
earners and salaried laborers, It means that as private individuals,
we can no longer own fixed capital goods like land and especially
machinery. We are to become, in other words, a sort of huge Chris-
tian co-operative movement, at best ernployed by each other, but
more probably employed by the unregenerate world. And if we are
not to draw such conclusions, then why did Troost bring up the sub-
jéct in the first place?’ Either it is a concrete example to be followed,
or else the whole incident is irrelevant. Again, we can admit that
under social conditions comparable to ‘those faced by the carly
church, something like this might be necessary, but as a prescription

348 THE SINAI STRATEGY

consumer sovereignty, 164-65
consumption, 235
contentment, 203, 204
continuity, 47, 48, 96, 98
contract, 67-68

covenants &, 133, 137, 316

knowledge, 162

labor, 250

libertarian, 135-37

prediction, 315
convocation, 231
cooking, 82, 282, 290-91
co-operation, 163, 203-7
co-operative, 325
co-ordination, 190
corporate state, 152
corporation, 289
cosmic impersonalisrn, 106
cosmic personalism, 18, 227
costs, 67, 77-78, 104, 165, 167, 257
Coulanges, Fustel de

see Fustel
Council of Laodicea, 249
court, 55
courts, 14
Courville, Donovan, 42n
covenant

contracts &, 65-69, 133, 137, 316

curse, 56

evaluation, 40, 211

outline, x

rituals, xiii

sabbath, 246

sixth day, 86

Sutton, ix

transgression, 53

treaty, 211
covetousness, xxi, 195-210
Gox, Harvey, 219
craftsmanship, 182, 185
craftsmen, 272
creation, xi, 330
Creator, xi
Creator-creature, 32, 33
credit card, 258n
creditors, 208
creed, xiii

crime, 121-22, 130, 152

Crimean War, 299

crime wave, 147

Cromwell, Oliver, 153, 293, 294
Crusoe, Robinson, 163
cruxifixion, xxiii

curse, 2, 52-55, 56, 58, 75, 97

Dark Ages, 224
Darwinism, 105-6, 109, 209
death, 30
death penalty (see capital punishment)
debt, 85, 207
decadence, 174
Decalogue, sce ten commandments
decapitalization, 114, 176
decentralization, 12, 319
de-develapment, 222
default, 227
deism, xii
delinquent, 67
deliverance, 81-83, 85, 126
Demetrius, 32
democracy, 189, 216
demographics, 95, 96
demons, xiv, 30, 32, 38
Demsetz, Harold, 160-61, 166, 169
Dennison, J. 'T., 264n, 274
dependence, 115
depersonalization, 124
discipline, 126
desecration, 239
deterrence, 120
dialectics, 19
diaspora, 29
Diehl, William, 10n
diet, 254, 253
Diggers, 153
discontent, 203, 205
dispensationalism, 320
divinization, 13
division of labor
brands, 185
covetousness vs., 203
international, 268-69
knowledge, 162
law-order, 206

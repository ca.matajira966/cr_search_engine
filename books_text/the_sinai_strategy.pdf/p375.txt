marriage, 127
one-world State, 285
output, 69
Reformation, 69
rural Israel, 236-38
simple life style, 209, 222
specialization, 69
divorce, 207
dominion
family, 176
“judge not,” 312
language, 61-63
law &, xii
Passover, 84
private property, 139
radical Christians, 216-17
rivalry, 45
Spirituality &, 3
strategy, 7, 221, 223-24
subordination, 73, 224
tool of, 50
urider Ged, 15
dominion covenant
escape from, 52
family, 96
fulfillment, 102
God's capital, 140
graven images, 29
long life, 96
Puritans, 273
ritual vs., 281
sabbath, 92
separation, 29
dominion religion, 73, 317
donkeys, 127
Dooyeweerd, Herman, 13
antinomian, 331
negative critique, 331
radicalism, 322
doubletalk, 7
dreams, 189
drop-outs, 198
Drucker, Peter, 950
drunkard, 67
drunkenness, 83, 84
dualism, 1, 2, 321
O.T. vs N.T., 220

Index 349

Duhem, Pierre, 225
duty, 96-97, 112

early church, 248-49, 324-25
earnest, 54, 92, 95, 120
Easter, 84, 294
Ebed, 181
ecologists, 292
economic growth, 102-3
economics
biblical, 21
Egypt, 22
law &, 141
Edom, 33
education, 112
efficiency, 103, 206
Egypt
economics, 22
firstborn, 81-82
growth rate, 46
taxation, 176, 202
tyranny, 5
eighth day, 75n, 78, 88, 89, 243
elders, 253, 262, 305
elitism, 106, 108
Elizabeth I, 293
emergency, 260, 262, 282, 283, 285
Emmaus, 83, 84
emotion, 192
Emperor, 337
employee, 65
empty boxes, 332
enemies, 309, 310
Engels, Frederick, 22
entrepreneurs, 160, 164
entry, 98-101
envy, xx
Christian intelfectuals, 143
covetousness, 204
decapitalization, 114
de-development, 222
defined, 195-96
Macaulay, 154
middle class, 158
polities of, 143
poor, 146
resentment, 204

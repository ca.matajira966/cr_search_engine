356 THE SINAI STRATEGY

God, 140
libertarian, 135
marriage, 129
responsibility, 116
self, 135
social function, 165
State, 139
stewardship, 140, 176

ox, 122

oxen, 127

packaging, 192
paganism, 47, 214
Palmerston, 297
pantheism, xii
parachurch, 70
parents
honoring, 26
park, 232
parks, 297
Parliament, 275, 292, 293, 295
Passover, 74, 81, 84, 129, 279-80
pathology, 130
peace, 206, 209-10, 211, 314
peace treaty, 27
Peake, Frederick, 300
Pella, 168
penalty, 71
Penielism, 327
pensions, 208
Pentecost, 84
perfection, 61, 122, 309, 312
performance, 182
personalism, 127
pessimism, 224
Peter, 83, 179
Pharaoh, 29
Pharisees, 179, 180, 309
parents, xvii
Philips, Lion, 47n
Philistines, 317
physician, 20-21
physicians, 235
Pick, Franz, 208
pickpockets, 124
pietism, 226, 311, 320, 324
planning, 65, 201, 203

poetry, 271
point of contact, 30
pole, 33
police power, 125
political covetousness, 199-201
politics, 26
pollution, 170-72
polygamy, 319
polytheism, 27, 128
Poole, Matthew, 205
population, 95, 96, 157
pornography, 62, 63, 17in
Post Office, 296
Potiphar, 315
poverts, 223, 320
poverty, 98, 130, 131-32, 141, 292, 223
power
autonomy &, 73
God's, 18, 71
judgment &, 313
‘misuse of, 54
name, 178
quest for, 52
responsil
sovereignty &, 313
worship of, 30
(see also quest for power)
power religion, 63, 71, 313
preaching, 144, 203
present-orientation, 174
Price, 164
price ceilings, 150
price competition, 99-101, 110
prices, 102, 150
Priests
Christians, 304
“honorary,” 284
industry, 284-85
sabbath, 239-41, 281-82, 284
primacy of intellect, 193
primogeniture, 97, 99
principles, 1, 3
prisons, 121
private property
defined, 159-63
early church, 324
exclusion, 139
sabbath legislation, 296

  

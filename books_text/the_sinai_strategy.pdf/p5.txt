This book is dedicated to
Lewis Lehrman

philosopher, businessman, and

politician, who understands that
long-term, meaningful change must be
grounded in permanent moral principles.

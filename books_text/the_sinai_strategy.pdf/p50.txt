24 THE SINAL STRATEGY

and then His presentation of the ten commandments, makes this
connection between freedom and biblical law inescapably clear. The
Christian economist who takes God’s word seriously has a responsi-
bility to begin to examine the case-law applications of God’s law to
see where economic issues are involved, and what requirements God
sets forth for economic relationships. To abandon faith in the
reliability of God’s law in economics is to abandon faith in what the
Bible proclaims as the only basis of liberation, namely, liberation
under the sovereign power of God, who sustains the universe and
calls all men to conform themselves to His ethical standards in every
area of life, in time and on earth.

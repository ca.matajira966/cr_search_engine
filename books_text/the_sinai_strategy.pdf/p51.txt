2
GRAVEN IMAGES AND COMPOUND JUDGMENT

Thou shalt not make unto thee any graven image, or any likeness of any
thing that is in heaven above, or that is in the earth beneath, or that is in
the water under the earth. Thou shalt not bow down thyself to them, nor
serve them: for I the LORD thy God am a jealous God, visiting the ini-
quity of the fathers upon the children unto the third and fourth generation
of them that hate me; and showing mercy unto thousands of them that love
me, and keep my commandments (Ex. 20:4-6).

The second commandment is divided into two sections. The first
section deals with the prohibition against graven images. The second
section deals with the punishment and mercy of God. It is not ini-
tially clear just how these two sections are linked together. Possibly
because of this confusion, the Lutheran Churches combine this com-
mandment with the first commandment, so that the prohibition
against worshipping other gods, the prohibition against graven im-
ages, and the promise of judgment and mercy are all considered as a
single commandment. To get ten commandments, they divide the
tenth, the prohibition against covetousness, into two: coveting the
neighbor's house, and coveting the neighbor’s wife, servants, and
work animals. This handling of the tenth looks strained, but the
handling of the first two by other Protestant groups also initially
looks strained. They do seem to be one unit, rather than a one-part
cormmandment followed by a two-part commandment.

My treatment rests on my belief that the traditional Reformed
and Anglican division is closer to the truth than the Lutheran. Deu-
teronomy 4:13 speaks of the necessity of obeying His covenant,
which is ten commandments. The emphasis is on obedience to all

1. “The Ten Commandments,” under “Lutheran Creeds,” in John H. Leith (ed.),
Creeds of the Churches: A Reader in Christian Doctrine (Chicago: Aldine, 1963), pp. 11314.

25

TABLE OF CONTENTS

  
 
 
 
  
  

 

 

 

 

Preface ..0 60ers ix
Introduction 0.6.6... nts 1
1, Liberation Economics.............. 17
2. Graven Images and Compound Judgment . - 25
3. Oaths, Covenants, and Contracts .....
4, Sabbath and Dominion........
5. Familistic Capital ..... .
6. God’s Monopoly of Execution ..
7. The Yoke of Co-operative Service .
8. The Rights of Private Property .
9. The Value ofa Name ...... :

40, Covetousness and Conflict ...... 6.06.6. e eee eens 195
Conclusion 2.00... ene 21h
APPENDIX A—The Economic Implications of the Sabbath. . . 228
APPENDIX B—Covenantal Law and Covenantal Love ...... 306
APPENDIX C— Social Antinomianism...... 0.000.600 eee 322
SCRIPTUREINDEX ..... 20.0 c cence ene 339
GENERAL INDEX . :
WHAT IS THEICE? .... 2... eee eee 363

vii

48 THE SINAI STRATEGY

ernment, they fear that all will be lost.4¢

Even a successful revolution is threatened by institutional con-
tinuity: lethargy, corruption, bureaucracy, This has been the fate of
the Soviet Union.’ To overcome these results, communists have ar-
gued for the necessity of continual revolutions. Trotsky? and Mao?9
both called for a continuing series of revolutions, echoing the in-
struction given to Communist proletarians by Karl Marx in 1850:
“Their battle cry must be: The Revolution in Permanence.”*? Bill-
ington has traced the idea back to the Bavarian Iluminati,*!
(Jefferson used similar language: “What signify a few lives lost in a
century or two? The tree of liberty must be refreshed from time to
time with the blood of patriots & tyrants. It is its natural manure.”#?
He was writing of Shays’ rebellion, the reaction against which
became one of the main motivations of the Constitutional conven-
tion. Yet here was Jefferson, writing: “God forbid we should ever be
20 years without such a rebellion.”)

God's people, on the contrary, should have faith in both time and
continuity.*3 God governs both, The steady efforts of the godly man
accomplish much. God’s word does not return to Him void (Isa.
55:11). Through the covenantal community, over time, each man’s

36, Lenin wrote @ secret message from his hiding place to the Bolshevik Central
Commitice on Oct. 8, 1917, a few days before the Communists captured Russia. Ic
outlined the tactics for the capture of power, He ended his letter with these words:
“The success of both the Russian and the world revolution depends on two or three
days’ fighting.” “Advice of an Onlooker,” in Robert C, Tucker (ed.), The Lenin Anthol-
ogy (New York: Norton, 1975), p. 414.

37. Konstantin Simis, USSR: The Corrupt Society: The Secret World of Soviet Capital-
ism (New York; Simon & Schuster, 1982); Michael Voslensky, Nomenkiatura; The
Soviet Ruling Class (Garden City, New York: Doubleday, 1984).

38. The Age of Permanent Revolution; A Trotsky Anthology, edited by Isaac Deutscher
(New York: Deil, 1964).

39. “Revolution was the proper occupation of the masses, Mao believed, for only
through perpetual revolution could he realize his vision of an egalitarian collective
society.” Dennis Bloodworth, The Messiah and the Mandarins; Mao Tietung and the Iron-
ies of Power (New York: Atheneum, 1982), p. 187.

40. Marx, “Address of the Central Committee to the Communist League” (1850),
in Karl Marx and Frederick Engels, Selacted Works, 3 vols. (Moscow: Progress Pub-
lshers, [1969] 1977), I, p. 185. A’similar call was made by Tolstoy: “. . . the only
revolution is the one that never stops.” Cited by James Billington, Fire in the Minds of
Men: Origins of the Revolutionary Faith (New York: Basic Books, 1980), p. 417.

41, Billington, of. cif., p. 597, note 309.

42. Jefferson to William §. Smith, Nov. 13, 1787, from Paris; in Thomas Jefferson:
Writings (New York: Library Classics of America, 1984), p. 911.

43. See North, Moses.and Pharaoh, ch. 12: “Continujty and Revolution”

 

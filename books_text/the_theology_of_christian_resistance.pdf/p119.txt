PACIFISM AND THE OLD TESTAMENT 79

warning: political institutions may be essential to the exist-
ence of human society, but they cannot be equated with the
Kingdom of God” (p. 81). From this sad history we learn that
the Church is not a state and thus can never be promoted by
war (p. 82). Doubtless this is a legitimate moral Jesson, but it
is nonetheless morally problematic. The Church is not propa-
gated by adultery either; should God have created an adul-
terous Church so that its failure would stand as an object
lesson? If war is evil, then the state is itself always evil, just as
adultery is evil.

This being the case, it would seem that an Anabaptistic
retreat from citizenship is the only proper course of action.
Craigie argues that the Christian is inevitably a citizen of both
Church and state, and is thus caught in a dilemma between
good and evil. This is because “the fundamental principles of
the Kingdom are love and non-violence” (p. 108), a ques-
tionable assertion in the light of the doctrine of hell. We shall
return to Craigie’s attempt to resolve this problem below.

The last chapter of his book is entitled “Some Conclu-
sions.” First, Craigie attempts to deal with the problem of
God and war. Essentially he tries to dissolve the problem in
mystery (a popular tact with dialectical theologians and those
influenced by them). “The first point which it is important to
stress in this context is the nature of human language when it
is applied to God” (p. 94). Our limited language points to the
fact that “God participates in human history” (p. 95), but His
participation in warfare points “not to his moral being but to his
will and activity” (p. 96). The Bible, however, presents holy
war as a reflex of God’s righteousness.

Secondly, Craigie deals with the problem of inscriptura-
tion. Why was all this bloodshed recorded for us to read, and
for the orthodox churches to misinterpret? Craigie is unable
to deal with the historicity of the narratives (though he does
not deny them, nor does he deny God's revelation through
them). All he is able to do is turn Old Testament history into
a parable, so that although his view of the facts is evangelical,
his theology is parabolic and dialectical. “To use an analogy,
the Old Testament’s treatment of war may be seen as a
parable, but the whole parable must be read if the message is
to emerge” (p. 97), This “parable” shows two things: first,
that violence is of the essence of sinful man’s nature, war
being one manifestation thereof; and second, that any

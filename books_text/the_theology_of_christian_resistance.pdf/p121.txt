PACIFISM AND THE OLD TESTAMENT 81

no pause for thought that God should hate Esau, or me, or
any other man; what astounds me is that God should love
Jacob.” And so it is, Craigie, like Spurgeon’s assailant, seems
in this book to have no conception of the holiness and wrath of
God, and no conception of the depravity and just deserts of
man. We should marvel that God loves any of us, and should
be astounded that there is any let up in His holy warfare.

Jacob C. Enz, The Christian and Warfare:
The Roots of Pacifism in the Old Testament.
Scottdale, PA: Herald Press, 1972. 95 pp. $1.95,

The back cover of this short book states that “the objective
of the author is not to formulate a biblical doctrine of pacifism
but to reexamine the biblical concepts to see just what is
there.” In his preface, Dr. Enz says, “If pacifism is not found
in the very fabric of biblical thought, no amount of proof-
texting will be convincing.” Thus, the intent of these six
chapters, originally lectures delivered at Bethel College in
Kansas, is to provide a Biblical-Theological argument for
pacifism. At the time of publication, Dr. Enz was a Professor
of Old Testament and Hebrew at Mennonite Biblical
Seminary, Elkhart, Indiana.

There is virtually nothing in this book to attract the or-
thodox Christian thinker. First of all, a low view of Scripture
pervades Enz’s discussion. The Bible is seen as contradictory:
“When it [love] narrows down to a nationalistic motivation as
in Psalm 2, the New Testament, in addition to Ruth and
Jonah, corrects it? (p. 15). “Both accounts of creation in

Genesis...” (p. 42). “Hence in the earliest material in the
Pentateuch .. . ” (p. 45). “What could ‘be more haughty and
brutally nationalistic than the two best known Messianic
Psalms... * (p. 70)? [Emphasis added to all quotations.]

Each of these assertions is gratuitous, betrays a low view of
the normativity of God’s holy Word, and indicates influence
from liberal higher-critical methodologies.

Second, the view of the atonement set forth in this book is
not Christian but gnostic.! On p. 39 we find, “Christ nailed

1, On gnostic-anabaptist thinking, see James B. Jordan, “The Moral
Majority: An Anabaptist Critique,” in The Failure of the American Baptist
Culture, Christianity and Civilization 1 (Tyler, TR: GDS Press, 1982).

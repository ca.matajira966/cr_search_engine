PACIFISM AND THE OLD TESTAMENT 85

Exodus-Deuteronomy are ignored, though they have much to
say to the issue of pacifism. The atonement is seen as expia-
tion (p. 118), never as propitiation of the wrath of God. In-
deed, Eller tells us that man is not God’s enemy (p. 31). Thus,
a fundamental lack of theological orthodoxy once again colors
everything said about the movement of redemptive history.
“Violence” is not defined and circumscribed by God-given
law, but by what seems to us to be right. Eller rightly points
out, repeatedly, that many crusaders for pacifism are as
“violent” as those they oppose, but he never allows the Bible’s
own laws to define the proper use of force.

In chapter 1, Eller points out that man was created to take
dominion, and this implies “fighting” or wrestling with the
creation. When Eve was brought to Adam, however, it was
not for fighting but for companionship. Then came the Fall.
In historic orthodox Christianity the following propositions
are taken as Biblical and true: 1. God and man are now
enemies, and God’s wrath is against all men (Rom. 1-3), and
His hatred against those outside Christ (Psalm 5). 2. God
poured out His wrath upon Christ, instead of on His covenant
people. Those covenant people are now enlisted as His do-
minion agents, and enthroned over those who will not con-
vert. 3. God has privileged righteous men to execute His
judgments on the earth, to a limited extent. The limits are
prescribed in His law: certain capital crimes, certain occa-
sions of warfare. 4. Evangelisn is by persuasion, never by the
sword. 5. Nationalism is idolatry, and most wars are unjust,
though not all.

Now, because of Eller’s theological position, we cannot ex-
pect him to take this ground, but we do have a right to expect
him to argue against it. He does not, however. “God does not
fight against man; man is not the enemy,” he writes (p. 31).
“The enemy is ix man, but man is not the enemy” (p. 31), If
we understand “man” as manfind, we might agree in pari
with Eller, but he will not permit that view. “Man” for him
means all men. Eller does not want to be counted as a univer-
salist, as he makes clear toward the end of the book, but he
does want.to leave that open as a possibility.

The next step in the discussion is the city of Cain. Eller
says that Cain’s basic motivation was a search for security (p.
32, and p. 147). This, however, is again a fundamental
theological error. The basic motivation for fallen man is

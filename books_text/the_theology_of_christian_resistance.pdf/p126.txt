86 CHRISTIANITY AND CIVILIZATION

hatred for God. Cain’s walled city was built first of all to make
a name for himself against God (Gen. 4:17; 11:4), as an act of
defiance, Cain’s problem was moral, not metaphysical, first
and forernost. Modern theology says that man’s basic motive
is fear of non-being, of chaos, so that his basic drive is to find
security, This is based on the being/non-being dualisra of all
pagan thought. In Christianity, however, man’s problems are
ethical. There is no such thing as non-being, since after death
men continue to exist. Man’s basic fear is not non-being, but
judgment. Man only tries to hide from this, and cover it up,
by pretending that he really fears non-being.

Now Eller does say, rightly, that war is not first of all a
political problem, but a theological one. Because, however,
his view of God is askew (wrathless), his understanding of the
nature of the theological problem is also askew.

What other conclusion can we come to when we read in
chapter 2, “Joshua and company had a true perception re-
garding war and peace but misapplied it (and it takes the
remainder of the Bible and Jesus to get that matter straight-
ened out)” (p. 40)? Further, “Now goodness knows, we find
plenty of early Hebrew actions that strike us as being very in-
consistent with what Yahweh’s war actually is all about; and
the remainder of the Bible will work at correcting those incon-
sistencies. Nevertheless, all the evidence suggests that these
people were doing the very best they knew how in getting their
lives hallowed in accordance with Yahweh’s will” (p. 52). I
have emphasized “strike us” because I think it is revealing.
Eller, however, emphasized the phrase “the very best they
knew how.” This is false on two counts. First, God repeatedly
judged Israel, even in Joshua’s time (at Ai), for nof doing the
best they knew. Second, what they did do right was in accor-
dance with God’s revealed law, which required “violence” in
holy war. Eller is saying that Joshua and company were right
in siding with God, but wrong in killing other people. God,
however, condemned Saul and Ahab for not killing people (I
Sam. 15; I Kings 20), and made a covenant of peace (yes,
peace) with Phineas because he did kill some people (Num. 25).
But for Eller, “It simply is impossible to reconcile the

3. See Arthur O. Lovejoy, The Great Chain of Being (Cambridge:
Harvard, 1936); and Rushdoony, The One and the Many (Fairfax, VA:
Thoburn Press, 1978).

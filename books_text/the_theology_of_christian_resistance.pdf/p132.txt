92 GHRISTIANITY AND GIVILIZATION

ment seriously and worked out a pacifistic world-view from it.
If Biblical Theology is to lead us to pacifism, it must do a bet-
ter job. Invariably a less than orthodox theology is brought in
to undergird the pacifist position, and this has been the case
from the early church (with its wholesale borrowing from
Greek philosophy, most notably in Lactantius) until today.

Second, God is a God of wrath, and the blessings of the
Gospel are the blessings of the the implementation of His
justice, and justice means the punishment of the wicked. The
great psalm of peace, Psalm 122, argues that it was the setting
up of God's throne of justice which led to peace {v, 5). In
short, capital punishment and just war are essential founda-
tions of peace.

Third, a false antithesis runs through much of this
writing, an antithesis between. violence and non-violence. In
the Bible, this is never the antithesis. The righteous take the
kingdom by violence, we are expressly told (Luke 16:16). The
antithesis is between obedience to Gud’s law and disobe-
dience. The unregenerate and disobedient tend to be violent
in the wicked sense, because of their hatred of God and of His
image in other men. The righteous, however, are called by
God’s law to exercise a holy “violence” against certain of
the wicked, thereby manifesting God’s wrath. In essence,
and despite all qualifications, the antithesis “violence/
non-violence” reduces the Gospel from theological to political
terms,

Fourth, while the Bible forbids street fighting, and any
taking of the law into one’s own hands (Ex. 21:18-25), it does
allow for self defense in situations where the officers of law
cannot be called on for help (Ex. 22:2, “If a thief is caught
while breaking in, and is struck so that he dies, there will be
no bleodguiltiness on his account.”). Since the entire premise
of the Sermon on the Mount is that Jesus came not to change
but to “fulfill” (enforce) the law, and since much of the
Sermon consists of turning the people away from Pharisaical
accretions and back to Moses,’ nothing in the sermon on the
Mount can properly be read as opposing this principle of
judicious self defense. The principle of just war is but an
extension of this.

7. In this respect, Jesus was doing what all the prophets had done, and
was bringing the prophetic tradition to its culmination.

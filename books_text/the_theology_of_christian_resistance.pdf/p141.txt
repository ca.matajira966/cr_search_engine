CHRISTIAN RESISTANCE TO TYRANNY 101

Law, to Pagan Naturalistic Natural Law.?*

The vast majority of American lawyers today are taught a
philosophy of law, but not legal philosophy, much less the true
philosophy of law, God’s law revealed in Scripture. They are
taught Pagan Naturalistic Natural Law as the fundamental
fact of law and as a relatively new discovery of modern legal
“science” or “political science,” although, as any reader of
Plato’s Dialogues or the Bible (Gen. 3:5; 10:8-10) should know,
the fundamental philosophy or this “new discovery” is
thousands of years old. But it is precisely because American
lawyers—and American students in general—are kept ig-
norant of the fundamental issues in legal and political thought
(for the two are obviously bound together) that Christians
should be aware of the issues and history of legal thought.
Ideas have consequences: especially when they have the
power of civil government behind thern.

Law means “binding.” All four philosophies of law con-
sider their laws to be authoritatively binding on men. But
three of the four kinds of legal thought can ultimately give no
valid reason why laws passed in accordance with their theories
should be, or are, binding on men, and at least the two ex-
plicitly Humanistic kinds of legal thought lead directly to
statism and tyranny. Now, if law is without ontological basis
in the structure of God’s created and providentially sustained
and directed world, if law has no basis in knowledge, since it
cannot be objectively known by man, it can have no moral
basis and no true basis of authority. Hence, such law can have
no binding authority, can be based only on the vicissitudes of
force and deceit, and in its own terms can admit of no true
right to resist the state, however ungodly, and at the same
time can admit no basis in right for obeying the state, cither.
Law without foundation thus opens the way to both tyranny
and anarchy. In order to see why this is so, as well as to un-
derstand the natural and historic drift of Western and
American legal thought, and to attain a fuller understanding
of the consequences of legal thought, it will be helpful to
outline the distinctives of these bodies of legal thought.

21. We shall refer to Pagan Normative Natural Law simply as Pseudo-
Normative Natural Law, and to Pagan Naturalistic Natural Law simply as
Naturalistic Natural Law. It should go without saying that these theories are
anti-Biblical and pagan.

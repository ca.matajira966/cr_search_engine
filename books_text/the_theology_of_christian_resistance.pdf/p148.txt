108 CHRISTIANITY AND CIVILIZATION

In the manner later to be followed by Americans in the War
for Independence, and to be sanctioned in the intentions of the
Framers of the Constitution by the supposedly arch-centralist,
Alexander Hamilton, in Federalist No. 28, Calvin advocated
resistance to preserve the existing constitutional, customary,
and godly order, against centralization, abuses of power, and
violation of rights and liberties by a tyrannical central power,
when initiated and led by lesser “powers that be,” by lesser
magistrates.*9 Calvin’s pervasive concern was for legitimacy
and the rule of law, a concern which led him to qualify and’
limit the Christian’s duty of obedience to God’s appointed civil
authorities. Like the later American colonists, the goal of
Calvin and his followers was not the revolutionary overthrow of
the existing order, but rather the preservation of revealed and
historically given law against the usurpations of tyrants.5¢

Moreover, the allegedly “radical” followers of Calvin did
not break with Calvin’s political and legal thought:

For not only did most of Calvin's allegedly radical and politicized
followers rely heavily on the basic directions that Calvin himself
had given them . . . but Calvin in turn was himself relying to a
large extent on formulations and theories of his own followers.
Rather than trying to discourage their theories, he heartily
welcomed them and incorporated them into his own arguments
for the legitimization of resistance.3”

Although the duty of obedience to civil government was
Calvin’s primary concern, his Biblical commitment led him
both to formulate and to practice a Biblical theory of
resistance to ungodly rulers. In Calvin, as in Beza, Hotman,
Mornay?® and the mainstream of medieval political and legal
thought, recognition of the continuing validity of God’s
Biblical Law? led to Christian social, legal, political, and
even military action to resist godless rule.

This tradition was continued in Scotland, in Samuel

35. In this, Calvin was at one with the main thrust of medieval theory as well.

36. Jungen, p. 187 and passim.

37. Idem.

38. For a recent translation of the main works of these men, see Julian H.
Franklin, ed, and trans., Constitutionalism and Resistance in the Sixteenth Century:
Three Treatises by Hotman, Beza and Mornay (N. Y.: Pegasus, 1968).

39. Note that in Calvin, as in others, Biblical Law and Natural Law were
both intertwined and equated: as in the thought of earlier and later Chris-
tian writers, there was no fundamental separation between the two.

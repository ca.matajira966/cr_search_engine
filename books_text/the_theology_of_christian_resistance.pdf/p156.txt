116 GHRISTIANITY AND CIVILIZATION

law of man: there is no neutral ground between the two.33

Philosophically, a number of difficulties are added to those
stemming from man’s ethical rebellion against God. First,
there are problems stemming from man’s finitude: Since all
men are finite, no one can know everything. Being finite, man
must start somewhere, in order to acquire a knowledge of
things in general and of law in particular. What is the key to
evaluating his experience? How is he to make sense of the sta-
bility and flux with which his senses seern to confront him? He
must start somewhere. It would seem easiest to be a ra-
tionalist and begin from universals, in order to see the order
in the flux of experience, and unity and purpose in the
multiplicity of being. But how can finite man know universals
on his own? The rationalist must presuppose the existence of
universals, but autonomously presupposed universals are not,
and cannot be, proven universals, so rationalism is inade-
quate as a source of knowledge and law. ,

Perhaps autonomous empiricism (the “scientific method”)
would be better. But empiricism seeks to go from individual,
particular things to universals. Unless, however, he knows all
there is to know~the total picture—the empiricist cannot be
sure of the significance of any particular thing. This problem
is derived from the fact that the empiricist seeks to go from a
knowledge of the evidence which is seemingly presented to his
mind or reason by his senses (physical evidence) to a knowl-
edge of universals in terms of which (he hopes) the particular
things he observes (or thinks he observes) through his senses
are ordered. But until he observes everything he cannot truly
know all the factors affecting anything. Thus, he cannot truly
know anything until he knows everything— which, we must
admit, is difficult! Furthermore, empiricism limits man to the
study of that which he observes (or thinks he observes)
through his senses—or through gadgets which extend the
range of his senses. But (assuming that he can truly know that
the evidence of his senses is accurate) since he is limited to
that which he observes through his senses, he can never know
that there is not something which his senses cannot detect

53. Again, pagan thinkers, living in God's world and being given con-
uciences by their Creator, may acknowledge some of God's laws, but their
rebellious hearts will never let them admit the Truth and Goodness of the
fulness of God’s law.

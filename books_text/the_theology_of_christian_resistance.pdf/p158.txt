118 CHRISTIANITY AND CIVILIZATION

sitions (the “Is”) and commands (the “Ought” ) are so radically
different, no secular thinker has ever been able to bridge
Hume's Gap.*® Hence, “autonomous” man, lacking the abil-
ity to go from the “Is” of experience to the “Ought” of law’s
commands, cannot possibly autonomously know “Natural
Law.”

Finally, Ghristian theories of Natural Law face the prob-
lem noted by Robbins:

Tf, as natural law theorists hold, man can discover ethical truths
by his own efforts, then what need have men of revelation? The
hypothesis of God and the necessity of his commands becomes
superffuous or positively detrimental: superfluous, because if
God is reasonable, He can simply and only command those
things which we can discover on our own anyway; and detri-
mental, because He may command things that we cannot dis-
cover using our own reasons and even things that may be con-
trary to our own reasons. God can only be superfluous or irra-
tonal.57

It is the continuing danger of Christian theories of Natural
Law that they will forget that God's created universe is also His
providentially sustained and directed universe (Heb. 1:1-3; Acts 17;
Col. 1:17). Nature operates according to discernible laws not
because nature is autonomous in origin or operation, but
rather precisely because the visible creation is sustained, in all
its minutest details, by the providential activity and decrees of
the sovereign God of Scripture. A fundamental danger of
Natural Law theory in general is that it tends to conceive of
nature as being deistically independent of God’s sovereign
providential control. Hence, the “laws of nature” are seen as in
nature, not over nature.°® Thus, many Christians believe in an
unbiblical “common ground” of both Christians and pagans,
in the form of laws inherent in a created but now essentially
autonomous nature, to be discovered by the supposedly
neutral man or reason of man. The fact that this conception of
nature is an essentially pagan concept, derived from the
Greeks via Thomas Aquinas and other would-be synthesizers
of revelation and autonomous reasoning, of Jerusalem and

56. Robbins, p, 18. The reference is to David Hume, the philosophical
destroyer of “autonomous” empiricism.

57. Robbins, p. 16.

58. Robbins, p. 17.

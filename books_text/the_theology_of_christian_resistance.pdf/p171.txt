CHRISTIAN RESISTANCE TO TYRANNY 131

the natural world has been the denial of the rationality of
nature, the denial of man’s rationality, the denial of true
yaoral absolutes, and the rise of modern anarchism and
totalitarianism in law and politics.8? The consequences of
these philosophical, intellectual, and legal developments have
been massive enslavement of nations and mass murders on a
scale unprecedented since the dark ages of pagan times,
together with the crisis of liberty both civil and ecclesiastical in
America and the West. Christian theories of “Natural Law,”
by adopting Pagan assumptions as foundation stones, have
led to compromise with Pagan “Normative” theories of “Nat-
ural Law,” which in turn, because of their own manifold
weaknesses, have naturally led from Pseudo-Norms down the
slippery slope of human autonomy into the pit of Naturalistic
“Natural Law.”

If the originally intended function of the doctrine of “Nat-
ural Law” was to place rationally known limits on the exercise
of political and legislative power, and to preserve individuals’
and groups’ liberties against tyranny, then the attempted
revival of “Natural Law” is a noble effort but also an abject
failure. Autonomous “Natural Law” theory is a slippery
slope, which leads naturally downward to its awn destruction,
and the destruction of the cultures and nations which follow
its way. It cannot be revived by men who, with one of its key
historians and proponents, Michael B. Crowe, seek to avoid
the problems of its at best ambiguous definitions of “nature”
and “law” by giving it a “variable content,” which would sup-
posedly allow it “to function more coherently as a non-
arbitrary, objective norm to judge government power and
daw.83

Variable content means changing standards of law and
ethics; it means that there is no rule of law, no bedrock of ab-
solute standards for man in a world of flux, no clear rules for
rulers or ruled, and nothing clear but the way to pagan abyss or
moral and legal relativism and legal and political absolutism.

82, Hallowell’s Main Currents in Modern Political Thought (N. Y.: Holt,
Rinehart & Winston, 1963) traces these developments masterfully.

83. “Natural Law: A Twentieth Century Profile?” in The Changing Profile
of Natural Law (The Hague: Martinus Nijoff, 1977), pp. 246-290; sum-
marized in Literature of Liberty, of. cit., pp. 34-35. Even the title of the book in
which the essay appears is indicative of the fundamental relativism and in-
tellectual bankruptcy of the position.

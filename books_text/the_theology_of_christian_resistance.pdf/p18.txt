xvili CHRISTIANITY AND CIVILIZATION

responsibility to work for specific political changes. This is not
what the dedicated pew-sitters want to hear, since their com-
mitment is still to the institutions of this world, in which their
capital emotional, financial, and philosophical —is invested.
Their response will be analogous to the response of the
Hebrew kings, court prophets, and masses when the prophets
preached to them about repentance: to ignore them or to
persecute them for bringing news of the righteousness of God
and His standards for human society.

Mobilizing the Remnant

At this point, we should be trying to identify the remnant, those
who will go through the turmoil and persecution without buck-
ling under. Reconstruction is a long-term task, and we have to
identify those who are emotionally and financially committed
ta it. This process of identification is preliminary to launching any
series of comprehensive resistance strategies. Ad hoc resistance
projects may be attempted, as necessity requires (defending
this or that Christian school, going into court to protect the in-
tegrity of a particular church’s records from IRS harassment,
etc.), but these are not substitutes for long-term planning.

The next step is to idenitfy those resistance projects that are still
legal and potentially effective. The common law tradition of the
United States and Britain allows citizens considerable latitude
in protesting and challenging the social evils of our day. It is
foolish to call people to risk everything on high-risk, illegal
projects, when there are still lower-risk, effective resistance pro-
grams to get involved in. Furthermore, Christians have, for
three or four generations, ignored the political and judicial pro-
cesses by which the humanists have created today’s social
order. To arrive at long last on the American political scene,
near the end of humanism’s social experiments, and to recom-
mend self-immolating acts of defiance—“setting ourselves on
fire on the steps of the Capitol building” when we still have
less risky and more effective avenues of resistance open to us, is
almost as suicidal as the original neglect of politics which has
marked twentieth-century fandamentalism.!3

 

15, George Marsden, Fundamentalism and American Culture: The Shaping of
Twentieth-Century Evangelicalism: 1870-1925 (New York: Oxford: University
Press, 1980), See also the “Symposium on Social Action,” The Journal of
Christian Reconstruction, VIUL (Summer, 1981).

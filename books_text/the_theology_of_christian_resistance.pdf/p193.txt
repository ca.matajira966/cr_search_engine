CHRISTIANITY AND POLITICS 153

“Culture, however, does not include religion. The notion
that it does is the basic error of practically all our cultural an-
thropologists, which fact may be ascertained by perusing
casually any standard work on anthropology by such authors
as Vander Leeuw, Malinowsky and others. But the basic
assumption underlying this position negates Christianity and
is thoroughly naturalistic. For the position of the cultural an-
thropologist is that religion is simply a projection of the
human spirit, an attempt to manipulate the unseen by magic,
or, in any case, that man creates the gods of his own image,
thus making it a cultural achievement. This is also the general
attitude of the religious liberal, who uses religion for achiev-
ing man’s ideal goals such as world peace... .

“The reason religion cannot be subsumed under culture is
the fact that whereas man as a religious being transcends all
his activities under the sun, culture is but one aspect of the
sum total of these activities and their results in forming
history. To divide life into areas of sacred and secular, letting
our devotions take care of the former while becoming secular
reformers during the week, is to fail to understand the true
end of man,”6

We need to understand that a man never can lay down his
religion and act as a religionless person. He can have his
religion changed, but he cannot act or think in a religionless
manner, because religion is the governing principle of all he
does, is, and says. Religion is like the “heart,” out of it flow
the issues of life, prejudicing all areas of human experience
and thought (Proverbs 4:23). This is re-affirmed by Jesus,
when He taught that it is the heart that influences the thinking
and behavior, and not vice versa (Mark 7:20). The implica-
tion, then, is clear: The Christian may never think or live as
anything other than a Christian governed by the Word of
God. He is never his own person, being “bought with a price.”
He may not claim sovereign rights over his own opinion or
behavior. He is a person under authority, who is striving to
“bring every thought captive to Christ,” even his political
thoughts.

To attempt to act “neutrally” in any field of endeavor is to
make the mistake of Eve, who assumed that after God had

6. H. Van Til, The Calvinistic Concept of Cuiture (Nutley, NJ: Presbyterian
and Reformed Publishing Co., 1972) p. 27f.

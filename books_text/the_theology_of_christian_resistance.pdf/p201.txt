THE EARLY CHURCH AND PACIFISM
A Review by Allen G, Guelzo

Jean-Michel Hornus. It [s Not Lawful For Me To Fight: Early
Christian Attitudes Toward War, Violence, and the State (Scottdale,
PA: Herald Press, 1980). 226pp. Appendix, postscript, notes,
systematic tables of primary and secondary sources. $13.95.

HRISTIANITY has, for centuries, sanctioned the use

of the “just war”: that is, in certain instances, it has been
permissible to suspend the general injunction to love one’s
enemies, and defend oneself or punish evildoers. For that rea-
son, Christians have not only been able to serve in armies,
but have, on occasion, used armies as a means of protecting
or spreading Christianity. The problem is that it is not easy to
sort out a jumble of motives, and declare one set of reasons as
proper grounds for war, and condemn another as “aggression.”
It is easy to see where some “just wars” were just so much
righteous pillaging—one thinks in this regard of the
Crusades, which ended up by destroying, not the Infidel, but
the Christian civilization of Byzantium. But war tends to
bring out the worst even in the best of men and causes, and
even a conflict so heavily laden with righteous overtones as
the Puritan civil wars in England had its moments (and con-
siderably longer periods of time, too) of depraved conduct and
hypocritical politicking. A holy war and a just war are not
necessarily the same—in fact, hardly ever are.

That swings the pendulum back the other way, to
pacifism, where (it is declared) that no war is ever just, no
violence ever justifiable for Christians. That.is the position of
the Mennonites, Moravians, and Quakers (to name a few)
and that is largely the background from which this book
springs. Ostensibly, it is a book of history, detailing early
Christian attitudes toward war, participation in war, and to-
ward the Roman state, and in that respect it is an interesting
contribution to patristic studies. But flavoring the history at

161

186 CHRISTIANITY AND GIVILIZATION

those necessities. Calvin, on the other hand, not only brought
the knowledge of God, freedom from the effects of the curse
through Jesus Christ, and consequent communion with God,
but he also activated his disciples and motivated them to
change the world. His theology involved not only a descrip-
tion of contemporary experience as being unacceptable and
unnecessary (that is, he showed a way to escape the effects of
the curse), but at the same time he rejected a merely personal
salvation. Rather, he called for the salvation of the world by
Jesus Christ.? It is, therefore, a mistake to limit the thought of
Jobn Calvin to the ecclesiastical realm; he had much to say
about the political sphere.

The second consideration has to do with the progression of
political thinking from the time of the Roman Empire up
through the Middle Ages. During the Roman Empire, the
average citizen had a very narrow sense of political duty. He
was ready to perform any public tasks for which he might be
made responsible by birth or by appointment. But he had no
sense of public vision, no idea of the state reformed, no partic-
ular political purpose. His aim in his office was nothing more
than an honorable performance. He had very little political
imagination, and could discover no ideal to pursue patiently
and systematically. During the Roman Empire, citizenship
had lost its meaning, and all men had become, in one way or
another, subjects, whose political existence had but one essen-
tial characteristic: that they obeyed impersonal, more or less
legal commands. *°

‘The collapse of the Empire and her universal sovereignty
shattered even this politics, subjecting men to a frightening
variety of extra-legal commands and forcing them to make
private and personal arrangements. The feudal system that
eventually emerged from these arrangements virtually
precluded political relations. For the formal, impersonal,
legal, anc functional-rational ties established by a conven-
tional political system, it substituted the extended family and
the private treaty, relations intensely personal and in
substance natural, patriarchal, and affective. For the

9. Greg Bahnsen, “The Prima Facie Acceptability of Postmillennialism,”
The Journal of Christian Reconstruction, Vol. II, No. 2, ed. Gary North
(Vallecito, CA: 1976), pp. 48-105.

10, Michael Walzer, The Revolution of the Saints: A Study in the Origins of
Radical Politics (New York: Atheneum, 1976), pp. 5&.

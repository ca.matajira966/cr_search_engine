JOHN CALVIN’S THEOLOGY OF RESISTANCE 187

interests and ideals that bound men together in the pursuit of
political goals, it substituted the bonds of personal loyalty,
kinship, and neighborhood. For the rational consideration of
political methods, it substituted blind adherence to customary
ways. Men came to imherit not merely their lands and posses-
sions, but also their social place and their moral and personal
commitments. Reverence for tradition paralleled the
reverence for fathers and lords, and similarly precluded im-
personal devotion to ideas, parties, and states. Familial and
dynastic aggression or retreat replaced political activity. Dis-
tant and largely powerless kings retained some vestiges of
authority and some claim to dominate the world of feudal
arrangements only by invoking divine right and acting out the
magical rites of religious kingship. But if this somewhat in-
creased the respect with which the monarchy was regarded, it
also intensified the apathy of the citizens ~ leaving the kings
no dependable supporters except God and their relatives. The
religion of the Middle Ages didn't help any either. Under the
synthesis of pagan cults and Christianity, politics became tied
to the realm of magic and religion. Ordinary man lived in a
narrower world, tied to family, village, and feudal lord, and
forgot the ideas of citizenship and the common good. Religion
reinforced the philosopher's advice: politics and religion don’t
mix, As Michael Walzer notes, “the traditional world-view of
medieval man, with its conception of an unchanging political
order, hierarchical and organic, and its emphasis upon per-
sonal and particularistic relations, probably precluded any
sort of independent political aspiration or initiative.”!

All that the Middle Ages could muster, even from a more
Biblical perspective, were men, not movements. The world-
view and cultural outlook of the people precluded any sort of
breadly based movement. The most important example of
this is Savonarola, the Florentine Reformer. It was his
endeavor, he wrote, to “make Florence virtuous, create for
her a state that will preserve her virtue.”!? This might have
provided an ideal around which to shape political activity and
organize a party of zealots. But the single motor force of the
Savonarolan reform was Savonarola himself. He exerted a

11. Walzer, p. 8.
12. Quoted in Roberto Ridolfi, The Life of Girolamo Savonarola (New York,
1959), p. 105,

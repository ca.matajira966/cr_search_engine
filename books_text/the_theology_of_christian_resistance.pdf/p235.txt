JOHN CALVIN’S THEOLOGY OF RESISTANCE 195

position of owing supreme allegiance to their Creator, and ul-
timately responsible directly to Him.

The Civil Government and Its Laws

“Next to the magistracy in the civil state come the laws,
stoutest sinews of the commonwealth, or, as Cicero, after
Plato, calls them the souls, without which the magistracy can-
not stand, even as they themselves have no force apart from
the magistracy. Accordingly, nothing truer could be said than
that the law is a silent magistrate; the magistrate, a living
law.”#2 So Calvin introduces his section on the civil govern-
ment and its laws.

Tt has been shown that Calvin had a very high view of the
magistracy. The logical question at this point then might be,
“Ts it not enough that rulers have been set over the people in a
way that whatever they say is the law that has to be obeyed?
Would it not detract from their authority to say that the law is
something different from the power of the magistrate himself,
a Jaw that he himself would then be responsible to?” The pro-
ponents of absolutism answered affirmatively, but that is just
what Calvin opposed. The quotation above makes that clear.

Human society, according to Calvin, includes both sub-
jects and magistrates, over which God alone is sovereign. The
means by which a commonwealth is to be governed is by law,
law that has its origin only in God, regardless of whether it is
conceived of as divinely revealed, natural, or “man-made”
positive law. “Therefore it is not the magistrates as such, but
the law that holds the organism of human society together.
The civil order is ordered by laws and the state cannot be re-
garded to be such unless it is actually constituted by law.’45

For Galvin, law has three levels: divine, natural, and
positive. As might be guessed, his concept of divine law is the
inscripturated Word of God. Although it may be contested by
some, Calvin also has a very clear teaching regarding natural
law.“ But for him, nature is the divinely instituted creation
order.** Although it has suffered through the fall, as a whole it

42. Institutes, TV, xx, 14.

43. Jungen, p. 30.

44, Institutes, II, ii, 12-17; Commentary on Romans 2:14ff.
45. Institutes, I, v, Sf.

JOHN CALVIN’S THEOLOGY OF RESISTANCE 203

King on that basis.”” In Calvin’s view, Christ does not exer-
cise any power apart from his redemptive purpose, which the
Church participates in. The Kingly office is therefore particu-
lar, and is exercised pro nobis, for us, and its purpose is His
people’s participation in the heavenly life.” When Calvin,
therefore, speaks of “spiritual”, he is not talking about some
element in a Platonic dualism, but about the work of the Holy
Spirit, which is particular and “restricted” as the work of Christ.

One mast not misunderstand Calvin’s position and place
some modalistic contruction upon it. He does not imply that
there is a separation between the dominions of Christ and of
God the Father. In this respect, he is a thorough-going
trinitarian. As Jungen notes, Calvin sees neither identity nor
disunity, but he does distinguish among three kinds of life in
this world:’9 “The first is animal life, which consists only of
motion and the bodily senses, and which we have in cornmon
with the brutes; the second is human life, which we have as
the children of Adam; and the third is that supernatural life
which believers alone obtain. And all of them are from
God.780

The three levels of life might be better conceived as three
concentric circles in which the wider circles in some sense ex-
ists for the sake of the narrower one. Thus the cosmos exists
for the sake of humanity and humanity ultimately for the sake
of the church, This would explain Calvin's insistence that civil
government should protect the outward worship of God and
the position of the church.*!

Regardless of how this is characterized, it is clear that for
Calvin the kingly rule of Christ belongs to the innermost cir-
cle, for this rule is concealed from the flesh.?? The reign of
Christ is a spiritual reign that is governed by the Word and
spirit. “The kingship of Christ is therefore a rule that has been
delegated to the son by the Father and that in the present age
is restricted to the government of the church through the
Word and spirit,”83

77. Institutes, Il, xv, 3.

78. Institutes, IL, xv, 4.

79, Jungen, p. 56.

80, Commentary on Ephesians 4:18.
81. Jungen, p. 56.

82. Commentary on Luke 19:12.
83. Jungen, p. 57.

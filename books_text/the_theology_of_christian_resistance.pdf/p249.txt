JOHN CGALVIN’S THEOLOGY OF RESISTANCE 209

though the wife and the husband behave in an unseemly way.
However men go astray, the end fixed by God is unchanged in
its place.”105

Two important points must be noted here. First of all, a
magistrate exercises his power only as long as God allows him
to. God has ordained his authority, and even though he rules
badly and tyrannically, he rules by God’s decree. In addition,
the exercise of authority is by the grace of God, and always by
the grace of God. Even though a man is a tyrant, God does
not completely withdraw His grace from him. It is God who
makes sure that there is no tyranny in which, however cruel
and unbridled, does not appear some of the justice and equity
which he wants magistrates to maintain. In any case, some
kind of government is still better and more beneficial than
anarchy.!°* As Calvin writes in another place, if magistrates
still turn out to be unprofitable to us, we should seek the fault
with ourselves and attribute their tyranny to the wrath of God
against our sins rather than to the good ordinance of God it-
self.107

The second point that must be kept in mind is that Calvin
makes a clear distinction between the divinely ordained office
that a man holds and the office-bearer himself. The
office-bearer is fully responsible for his actions, and just as
guilty because of his sin. In other words, for Calvin, the
obligation of a ruler to his subjects, and more importantly for
this discussion, the obligations of subjects to the ruler, never
imply that government only exists as long as the office of
magistrate is properly fulfilled, but is maintained even when it
degenerates into something less than ideal.

It is time now to look at what Calvin considers the
grounds for legitimate resistance actually are. Surprisingly
enough, he does not hesitate to point them out. Hc repeatedly
makes clear that authority is legitimate that keeps within the
bounds divinely prescribed for it. These “boundaries” are
defined using the distinction previously discussed under the
heading “the Two Kingdom Doctrine.” Calvin clearly states
that the limit to obedience comes when the honor and worship
of God, or God's direct authority over us is in danger of being

105. Commentary on Ist Peter 2:14,
106. Commeniary on 1st Peter 2:14.
107. Commentary on Romans 13:3, and 1. Timothy 2:2.

212 CHRISTIANITY AND CIVILIZATION

place. In other words, resistance is not to be carried out
against the existing order, but rather in the interests of the ex-
isting order. Resistance is carried out against the particular
tnagistrate in office, and not against the office itself. Although
it is a fine line, it spells the difference between revolution and
an act of Christian resistance.

The Agenis of Resistance

It has been established that, for Calvin, there are cases in
which it is lawful for a ruler to be resisted, but it has not been
shown what concrete forms such resistance can take, and who
are the lawful agents in such a resistance. Just because the cir-
curustances allow for some form of resistance, does not mean
that anyone is automatically authorized to resist a tyrant by
whatever means seems good.

Building upon his principle derived from God’s being a
God of order, Calvin insists that any resistance against a
tyrant must be done in accordance with the legitimate laws of
that particular order. An illegitimate and tyrannical ruler
does not warrant an illegitimate reaction on the part of the
people, but rather resistance is to seek to restore the status quo
and thus must be proceeded with lawfully.

Private Citizens: What has already been noted regarding the
magistrate is also true for the private citizen. Society is made
up of both the rulers and the ruled, both subject to God’s law.
Each is to maintain his specific calling, and may not il-
legitimately overstep his boundaries.

What this implies for private citizens, then, is utrnost
restraint in public matters, so “that they may not deliberately
intrude in public affairs, or pointlessly invade the magistrate’s
atice, or undertake anything politically.”11+ Even if offences
are committed against them, private citizens do not have any
independent rights except those allowed by law, and even
these may not be pushed privately, but only through the due
pracess of law.445 Calvin violently disagrees with the modern
concept of democracy in which all power is seen as ultimately
in the hands of the people. For him, God is the only sover-
eign, and delegates power to whom He wills. In Calvin's

114, Institutes, IV, xx, 23; ef. Commentary on I Thessalonians 4:11; Com-
mentary on Psalm 101:5.
115. Commentary on Psalm 18:48.

 

4

i
z
«
‘
i
i
L

 

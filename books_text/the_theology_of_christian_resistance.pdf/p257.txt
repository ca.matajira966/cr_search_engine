JOHN CALVIN’S THEOLOGY OF RESISTANCE 217

create disorder for the purpose of overthrowing the reigning
government. An act of Christian resistance works to bring the
country back to the status que, and when that is done, the
resistance stops, even if all the wrongs haven't been righted.
Those unrighted wrongs are then approached through the
proper channels via the lesser magistrate.

Finally, although there is not time to go into detail, with
the popularity of Francis Schaeffer's A Christian Manifesto, it is
important to touch upon the relationship of the Scottish Re-
formed tradition and the Continental tradition which was
molded largely by John Calvin. Schaeffer has highlighted one
work from the Scottish tradition, Lex, Rex (translated -—“The
Law and the Prince”) by Samuel Rutherford, and pointed to
John Knox as a preeminent example of Christian resistance
in Christian history. There is, however, a genuine divergence
of views between the Continental and the Scottish reformed
tradition. The Scots, under Knox, John Ponet,!?8 and Chris-
topher Goodman,’ endorsed such things as the right of
private citizens to depose an evil governor by force and even
to kill a tyrant.48° Goodman appealed ia private citizens to
remove an evil ruler from their midst lest they become
polluted and guilty of his sins. Knox, who was a student of
Calvin’s at Geneva, expressed his most radical views in his fa-
mous Trumpet Blast Against the Monstrous Regiment of Women,
where he argues that resistance is legitimate because of an un-
biblical government (a female monarch). His view received
an even more permanent place in the Scot’s Confession of
Faith (1560), where “repressing tyranny” is listed under the
heading of “Good Works.”!51 As can be seen in this very brief
view, the Scottish tradition is considerably more radica! than
Calvin’s 132

128, John Ponet, On Politicke Power (1356; reprinted 1972, Theatrum
Orbis Terrarum Lid, Amsterdam).

129, Christopher Goodman, How Superior Powers Ought ta Be Obeyed (1558;
reprinted 1931, New York).

130, See Ponet, On Politioke Pauer.

131, Chapter XIV, “The works which are counted good before God.”

132. See Richard L. Greaves, Theology and Revolution tn the Scottish Refor-
mation (Grand Rapids: Eerdmans, 1980),

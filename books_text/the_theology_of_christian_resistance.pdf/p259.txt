DEFENSIVE WAR IN A JUST CAUSE SINLESS 219

walls of that ancient city of Jersualem. But when Sanbailat
the Horonite, Tobiah the Ammonite, and Geshem the Ara-
bian heard that there were men come to seek the welfare of the
Jews, they were filled with indignation: therefore, in scornful
language, they bring a state-accusation against them, saying:
“What is this thing that ye do? Will you rebel against the
king?!” However, though they treated the Jews with scorn
and insult, yet their labour became a subject of conversation.
Sanballat once speaking on the occasion, Tobiah makes a
reply to this effect, viz. “Tush, Sanballat, it is not worth your
notice, nor should you give yourself the least concern about
these feeble wretches, they build indeed, but if a fox in his
meanders was to ascend their stone wall, and only give a few
scratches, it would fall down.” These scornful insults were
spoken that the Jews might hear them, and be discouraged;
but when they saw that the work went on with rapidity, they
were filled with the highest indignation, and resolved, if bitter
taunts, these swords of their mouths, would not discourage
them, their swords of steel should compel them to cease from
their work: therefore Sanballat, Tobiah, the Arabians, the
Ammonites and Ashdodites all conspired iogether, to come
with their united force against Jerusalem. Their design was
made known to Nehemiah; and, as all should do, especially in
distress, he lifts up his eyes to heaven, and makes his supplica-
tion to the Lord of hosts; nor does he think his preservation
shall be effected in neglect of the use of means; therefore he
sets a watch against them day and night, and addresses him-
self to all ranks of people in these spirited and excellent words,
viz. Be not ye afraid of them: Remember the Lorn, which is great and
terrible, and fight for your brethren, your sans and your daughters, your
wives and your houses.

In the words observe,

i. A caution against cowardice, or fear of an enemy un-
justly enraged. Be not ye afraid of them.

It is of great importance in war to be delivered from fear of
the enemy; for soldiers in a panic generally fall a victim in the
dispute.

1. These three gentlemen were governors, and consequently pensioners.
It is common for such to profess great loyalty to kings, when in reality it is
their pension they love, and not their king. They speak in court language,
“Will ye rebel against the king?”

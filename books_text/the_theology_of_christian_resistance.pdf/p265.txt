DEFENSIVE WAR IN A JUST CAUSE SINLESS 225

suitably qualified to rule according to his own will and
pleasure, so that when man assumes to rule arbitrarily, he sets
himself in the temple of Gop, shewing himself that he is Gop;
for none is qualified for that seat, but him that is infinitely
wise, just and holy. We see, when these words are taken in a
limited sense, there is no evil consequence follows, but if we
understand them to bind us to obey all that taskmasters re-
quire, then it is plain that we condemn King Henry VIII, for
refusing obedience to the pope of Rome. Many bulls were
thundered out against him, but he and the parliament resolv-
ed that they were a complete legislative body in themselves,
paying no regard to the threats of Rome. If we were to em-
brace any other exposition of these passages of scripture, we
should absolutely condemn the Reformation; nay, we would
condemn all England, who refused obedience to King James
II because he would not be subject to the higher powers, but
endeavoured to subvert the constitution, and to reign ar-
bitrarily. Such a sentiment, if pursued, would lead back to all
the horrors of popery and despotism; nay, it would even con-
demn the blessed martyrs, who refused obedience to arbitrary
and wicked laws.

Some will object, that these laws respected religion, which
makes an essential difference; for when religion is affected,
one may withstand a kingdom. Calvin and Luther were of
this faith. It is granted, that there is great difference between
state affairs and religion; for Christ says, “his kingdom is not of
this world,” none dare impose laws in matters of religion on his
subjects, without being guilty of a daring insult: but the in-
stances above do not all respect religion, therefore are suitable
on the present occasion; and thase that do respect religion,
were brought to prove that the words are to be taken in a
restricted sense. I have met with none, but acknowledge these
texts must be so understood, as to justify opposition to popish
tyranny. And pray, my countrymen, what better is protestant
tyranny than popish? Is there any essential difference between
being robbed by a protestant or a papist? Is it not the very same
thing? Tyranny is tyranny, slavery is slavery, by whomsoever
it is imposed. Names change not the nature of things. If
despotism is bad in a papist, it cannot be good in a protestant.
If it may be resisted in one, it ought to be in the other.

But to proceed: There is another objection, which good
people make against war of any kind, viz. “That war is not

THE DEBATE OVER FEDERAL SOVEREIGNTY 277

destroyed; and thus also they guarded against all abridgment,
by the United States, of the freedom of religious principles
and exercises, and retained to themselves the right of protec-
ting the same, as this, stated by a law passed on the general
demand of its citizens, had already protected them from all
human restraint or interference; and that, in addition to this
general principle and express declaration, another and more
special provision has been made by one of the amendments to
the Constitution, which expressly declares, that “Congress
shall make no laws respecting an establishment of religion, or
prohibiting the free exercise thereof, or abridging the freedom
of speech, or of the press,” thereby guarding, in the same
sentence, and under the same words, the freedom of religion,
of speech, and of the press, insomuch that whatever violates
either throws down the sanctuary which covers the
others, —and that libels, falsehood, and defamation, equally
with heresy and false religion, are withheld from the
cognizance of federal tribunals. That therefore the act of the
Congress of the United States, passed on the 14th of July,
1798, entitled “An Act in Addition to the Act entitled ‘An Act
for the Punishment of certain Crimes against the United
States, ” which does abridge the freedom of the press, is not
law, but is altogether void, and of no force.

4. Resolved, That alien friends are under the jurisdiction
and protection of the laws of the state wherein they are; that
ne power over them has been delegated to the United States,
nor prohibited to the individual statcs, distinct from their
power over citizens; and it being true, as a general principle,
and one of the amendments to the Constitution having also
declared, that “the powers not delegated to the United States
by the Constitution, nor prohibited to the states, are reserved
to the states, respectively, or to the people,” the act of the
Congress of the United States, passed the 22nd day of June,
1798, entitled “An Act concerning Aliens,” which assumes
power over alien friends not delegated by the Constitution, is
not law, but is altogether void, and of no force.

5. Resolved, That, in addition to the general principle, as
well as the express declaration, that powers not delegated are
reserved, another and more special provision inserted in the
Constitution from abundant caution, has declared, “that the
migration or importation of such persons as any of the states
now existing shall think proper to admit, shall not be pro-

312 CHRISTIANITY AND CIVILIZATION

Christian political party of Calvinist background and true to
Biblical teachings. In 1969 he was elected to represent this
party, the Reformed Political League, in the Dutch Parlia-
ment (House of Commons) and held a seat until a few years
ago. Because of his clear and outspoken Biblical views on na-
tional and foreign policies, he was regularly interviewed on
national radio and television networks. The public’s apprecia-
tion far his work is witnessed by a popular recent biography.

Mr. Jongeling now enjoys his retirement from an active
life with his family, visiting his three sons and six daughters
with his wife and life-long companion.

Mr. Jongeling has a monthly column, “Reflections” in the
Christian family magazine Reformed Perspective, available from
Box 12, Transcona Postal Station, Winnipeg, Manitoba,
Canada R2C 225.

* * * *

The Christian Nation of Holland before 1940

During the last part of the 19th century and the beginning
of the 20th century, Holland saw.a great development of
Christian organizations. In part, this was a result of two
preceding reformations of the church and inspired by the
genius of Dr. Abraham Kuyper (1837-1920), a theologian and
statesman at the same time, who put his stamp on the
Reformed people in Holland by his influential and productive
writings.

For many years, a fierce political battle had been waged to
gain equal rights for Christian education in basic schools with
respect to state-supported education. In principle, this battle
was won in 1920 and Christian schools were rapidly growing
in number. Christians of Calvinist tradition established their
own Free University, which turned out to be very successful.

A whole network of daily newspapers and periadicals,
owned and managed by Christians, covered the nation, the
most well-known being “De Standaard” (“The Standard”).

On the political scene, two political parties emerged, both
of Protestant origin, the Anti-Revolutionary! Party (A.R.P.)

1. Translator’s note: “Anti? should be taken in the sense of “instead of”
and not as “counter.” ‘The motto of this party was “Instead of Revolution,
the Gospel.”

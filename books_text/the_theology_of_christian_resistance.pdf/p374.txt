334 CHRISTIANITY AND GIVILIZATION

have been hardened to think of other men as worthy of exter-
mination, To entrust peace and freedom to the military establish-
ment, to trust a person with powerful weapons to be morally self-
critical, places greater and more unjustified confidence in human
character than does any kind of pacifism” (p. 113). Yoder may
overstate the case here, and we should certainly argue that
militarism and pacifism are not the only two alternatives in this
area, but we must grant that his point is well taken, especially in
light of the push toward a rencwed and largely uncritical na-
tionalism in fundamentalistic circles today (as in the Accelerated
Christian Education curriculum with its red, white, and blue school
uniforms; and to a lesser extent the Foundation for Christian Self
Government).

Yoder also scores some points in defending appeasement as a
political policy. The British and French appeasement of Hitler in
1938 is viewed with horror today, but as Yoder points out, “What
were Franklin D. Roosevelt’s agreements with Joseph Stalin if not
‘appeasement’ on the other side? In return for the support of a tyrant
the U.S. not only did not stop him but in fact conceded to him con-
trol over nearly half of Europe. The allies, in order to win a war,
gave away to totalitarianism far more territories and populations
that didn’t belong to them than Munich gave to Hitler” (p. 44).

Of course, running through the book are assumptions which we
should challenge. We may select one to notice in this brief review.
On pp. 48f; “Now that the experience of Gandhi and Martin
Luther King has demonstrated that in some kinds of situation [sic]
nonviolent methods can effectively bring about social change in the
desired direction. .. .” The question is this: Were Gandhi’s and
King's methods non-violent? Their method was to interfere and
block the normal processes of society, to impede the flow. Ifa man is
running a race, with his eyes on the tape, and another man sticks
out his leg and trips him, who is guilty of the violence? The man in
motion, or the man whose leg is standing still, but whose action is
calculated to disrupt the expected, normal flow of things? Surely the
latter; yet he may say, “Well, [ didn’t do anything but just stand
there. The runner ran into my leg. I didn’t hit him. If he chooses to
tun into my leg, that is his fault.” Similarly, King and others argued
that when they blocked automobile traffic, they were non-violent,
and that any disorder was due to the traffic. Obviously, though, on
close inspection we can see that both Gandhi and King were quite
violent in their methods. The question then becornes simply one of
who gets to wrap himself up in the righteous-sounding language of

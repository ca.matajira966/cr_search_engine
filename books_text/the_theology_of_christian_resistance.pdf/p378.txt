338 GHRISTIANITY AND CIVILIZATION

by faith, which presupposes a legal theology and a high view of
Divine Jaw as the standard of judgment, is virtually unknown in
American evangelicalism. While the reader must be careful not to
impute every heretical consistency of Denck and Hut to modern
American evangelicalism, the resemblances are nonetheless there,
and a reading of Packull’s study will be of help to orthodox Chris-
tians as they try to reassert the Biblical faith in an ocean of subjec-
tivism.

The Christian and the Public Schools, by George Van Alstine.
Nashville: Abingdon, 1982. 144 pp., $5.95. Reviewed by Lonn
Oswalt.

For several years the publication scales of evangelical works on
education have been tipped most dramatically in favor of those
books advocating the establishment of separate, Biblically-based
schools for the training of evangelical children. This position has
been argued in opposition to a stance advocating evangelical claim-
ing or reclaiming of the public school system. While much of the
argumentation for the Christian school option has been overlapping
and therefore superfluous, many evangelicals have nonetheless been
persuaded that withdrawal from public education and subsequent
establishment of Christian alternatives is the true educational cor-
ollary to evangelical profession.

Enter George Van Alstine with his The Christian and the Public
Sechaols. Van Alstine states clearly that he is an evangelical (p. 10),
yet states just as clearly that evangelicals who believe in a separate
Christian school system are headed in the “wrong direction” (p. 10).
His book disturbs me in two major ways. These major disturbances
are, first, that evangelical book stores presumably will now be
stocked with this visible reminder that Christian school advocates
have not yet scored a total victory in their own camp. It appears that
as a Christian school fan(atic) I must come to grips with the fact that
not all evangelical resistance to Christian schools can be relegated to
financial or geographical reasons; there are, in fact, ideological
reasons for public school support. This bothers me because the case
for Christian schools is, to me, as strong a case as exists in the
theological world.

{am disturbed, secondly, because f am afraid the book is just at-
tractive enough that people who are not committed to Christian
schools will now have an official rack on which to hang their hats. It
is not that the book is flawlessly argued (it is not), but rather that the

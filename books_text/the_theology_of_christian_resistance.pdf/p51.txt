CHRISTIAN RESISTANCE {1

save himself and defend himself by protest and the employ-
ment of constitutional means of redress. Rutherford illus-
trated this pattern of resistance from the life of David.

By Christian resistance is not meant that Christians
should take to the streets and mount an armed revolution. In
the modern state, such an action would be ill-advised. The
state’s technology affords it the power to crush armed revolt at
virtually any time. Moreover, there is no cxample in the Bible
of any man of God who set out with the design to overthrow
his government by violence. The emphasis in Scripture, as il-
lustrated by Pcter and Paul, is that by fulfilling the law of
God, without regard for the consequences, a true cultural
revolution ‘will occur.

Just as armed revolution would be a strategic error by the
church, so flight may be also. First of all, due to the immense
power of the modern state, there may be no place to flee. The
Pilgrims could escape tyranny by fleeing to America. But
where do we go to escape? The so-called safety zones, such as
Switzerland, are fading. We live in a shrinking world. Se-
cond, I would not advocate flight at this time because the
church has been taking flight spiritually for the last one hun-
dred years. As I stated earlier, the church must get its head
out of the sand and stop retreating and take a stand and fight
the slide toward totalitarianism.

At this time in history, protest is our most viable alter-
native. This is because the freedom yet exists that will allow
us to utilize protest to the maximum. We must, however,
realize that protest is often a form of force. For instance, Peter
in Acts 5 was ordered not to preach Jesus in the temple. He
ignored such illegitimate commands and re-entered the tem-
ple to preach salvation to the Jews. This is force or compelling
others to listen to something they do not want to hear.

There does come a time when force, even physical force, is
appropriate, The Christian is not to take the law into his own
hands and become a law unto himself. But when all avenues
to flight and protest have closed, force in the dafensive pasture is
appropriate. ‘This was the situation of the American Revolu-
tion. The colonists used force in defending themselves. Great
Britain, because of its tyranny, was a foreign power invading
America. Note that the colonists did not cross the Adantic
Ocean and mount a physical attack against Great Britain it-
self. They defended their homeland. As such, the American

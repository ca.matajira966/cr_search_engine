 

:
:
zg

HUMANISM VERSUS CHRISTIANITY 17

When Dr. C. Everett Koop, my son Franky, and I worked
on Whatever Happened to the Human Race? and we began to talk
about infanticide, people really thought we were way out in
left field somewhere, but now it has been declared legal in the
case in Indiana where a baby, this Down’s syndrome child,
was allowed to die upon the basis of the parents’ wishes.

And the enthanasia of the aged will also come with speed. If
you think this is extreme, think back on the change from abor-
tion to infanticide and with what speed that came. What is in-
volved is not only a loss of the baby that is killed, but it is a
loss increasingly of a whole view of the unique value of human
life; and id ts the loss of compassion. The compassion this country
never has had enough of, but for which it has been somewhat
known, will not stand if we continue to go down this humanis-
tic road, because.there is no real value for people intrinsically
and as such it is not only the babies that can he killed with
abortion and infanticide, but there is no real reason for com-
passion toward other people.

I would plead with those of you who are in government not
to allow other things to sidetrack you in the need to stand
against this downplaying of the value of human life. Many
things might distract you, but you must realize, for those of
you who are Christians especially, that there ts an absolutely un-
breakable link between the existence of the infinite-personal God of the
Bible and the unique value of human life. And if you allow the uni-
que value of human life to be played down legally, as it is at
the present time in this country, not only does it lead on to
other things such as the euthanasia of the aged, but the whole
basis of compassion is lost. For those of you in government
who think this is a small issue that should be swept under the
rug, I plead with you to think again of the fact of how crucial
and central this issue is. This is a vital issue, which the Judeo-
Christian final reality gives us, and we‘are losing it with great”
speed. It is where humanism in medicine and in law have
come together at the point of human life, and it naturally
favors abortion, and equally it has no way to put any em-
phasis on the value of human life when we consider it
politically in other countries.

The First Amendment, of course, has been stood on its
head. The First Amendment was that there should be no na-
tional state church for the thirteen colonies, and that the fed-
eral government should never interfere with the free expres-

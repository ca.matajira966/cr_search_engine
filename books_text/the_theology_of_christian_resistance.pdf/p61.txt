2
HUMANISM VERSUS CHRISTIANITY at

automatically and logically oppress is nothing less than lack of
Christian love. This is why ] am not a pacifist. J am not a
pacifist, because pacifism in this poor world in which we live, this lost
world, means that we desert the people who need our greatest help. As
an illustration: I am walking down the street. I see a great big
burly man that is beating a little tiny tot to death—beating
this little girl, beating her, beating her. I come up and I plead
with him to stop. If he won't stop, what does love mean? Love
means I stop him in any way I can including, quite frankly,
hitting him, and to me this is necessary Christian love in a
fallen world. What about the little girl? If I desert the little girl
to the bully, I have deserted the true meaning of Christian
love, and responsibility to my neighbor. And we have in the
last war the clearest illustration you could have with Hitler’s
terrorism. That-is, there was no possible way to stop that
awful terror that was occuring in Hitler’s Germany except by
the use of force. There was no way. As far as 'm concerned,
this is the necessary outworking of Christian love. The world
is an abnormal world, because of the Fall it is not the way
God meant it to be. There are lots of things in this world
which grieve us, and yet we must face them.

Now I want to read tonight a portion from a letter that I
wrote to a certain person that I like very much but whe was in
danger of being confused at this very point:

“We all grieve at any war and especially atomic war, but ina
fallen world there are many things we grieve at, yet nevertheless
we must face. From the time of the last World War onward, it is
the Europeans more than the Americans who have wanted the
protection of atomic weapons and have demanded it. They un-
derstood the reality of what Winston Churchill said immediately
after the last war—that is that with the overwhelming forces of
the Soviets they could easily dominate Western Europe to the
Auantic Ocean if it were not for the fact of being deterred by the
United States having the atomic weapons. We have come to a
crazy place undoubiedly, with far too many of the atomic
weapons on both sides, and where there must be open discussion
and reduction concerning this. But the initial factor is not
changed. Europe would even more today than in Winston
Churchill's day be subject to either military or political domina-
tion of the Soviets if ii were not for the addition of the factor of
the existence of the atomic weapon.

“Further than this, Europe’s economic wealth, to the level that
some European nations now have a higher standard of living

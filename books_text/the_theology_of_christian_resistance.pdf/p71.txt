WHAT THE WAR IS REALLY ABOUT 31

some invisible doctrine of his own.

Now let’s look at the first clause of the second sentence of
his footnote, in which he speaks disparagingly of the “fact”
that the Church has “only fifteen members.” In the first place,
Church counsel Whitehead told the Court that the Church is
composed of fifteen families, not individuals. Pastor Dykema
explains, “We are a covenantal Church, which thinks in terms
of famnilies, like The Bible.” A membership of fifteen families
means that we are talking about fifty people or more. But sup-
pose the Church of Christian Liberty were in fact composed of
only fifteen individuals. Would it cease to be a church? How
many members rnust a church have? Jesus said that where as
few as two people — yes, two—- gather in His name, He will be
there. Presumably this means that 2 membership of two can
constitute a church. If you are a Christian, you believe this,
What Dumbauld is, your obedient servant doesn’t know, but
he obviously doesn’t.

Indeed, many churches are founded in somebody's living
room or basement, with considerably fewer than fifteen
members. Dumbauld insinuates that they are somehow il-
legitimate; and that government should and will determine
how many members there must be.

Finally, in-the footnote, Dumbauld complains that “a
throng of spectators occupied the back rows of the courtroom
during argument of the case.” In other words, first he’s wor-
ried because the Church has “too few” members; now he’s
worried because there are “too many” spectators in court.
What does the number of spectators have to do with the issues
of the case? Does the Court belong to the people? Should the
spectators not have been there? If not, why does the Court
provide those rows so that the spectators can sit?

This concludes our tour of page two, On page four, Dum-
bauld quotes 26 U.S.C. 501(c)(3), in which the Internal
Revenue Code describes those organizations that are tax-
exempt. It begins: “Corporations. ,. organized and
operated exclusively for religious . . . purposes... .” Dum-
bauld adds the following thoughts: “Is the organization prop-
erly organized, with a corporate structure suitable for carrying
out religious purposes?

“In this connection the church’s certificate of incorpora-
tion would be relevant and material. Apparently the LR.S.
has already been furnished this document. Although perhaps

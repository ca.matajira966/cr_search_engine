CONFIRMATION, CONFRONTATION, AND CAVES 47

of execution. He had been given an opportunity to reinstate
the law of God as the civil law of Israel. Elijah promised him
that the rain at last would come (v. 41).

Once again, Israel was offered a new beginning, as she
had been offered by Moses at Mt. Sinai. Elijah ran before the
king, as he returned to Jezreel (v. 46). The king and the peo-
ple had come down from Mt. Carmel, the “fruitful place.”
Carmel was a re-creation of the Garden of Eden, which had
also been a garden on a hill (for a river Howed from Eden into
the garden, and from there four rivers flowed out, indicating a
drop in elevation: Gen. 2:10). Once again, Israel would aban-
don its new beginning. Once again, there had to be a confron-
tation.

Caves

The king was a weak man. Like Adam, he was under the
domination of his wife. But this woman, unlike Eve, did not
sin in ignorance. She was like Babylon, the great whore, a
representation of the “strange woman” of Proverbs 5, seli-
consciously evil. When he told her what Elijah had done to
the prophets, she sent a message to Elijah telling him that he
would die the very next day, just as the court prophets had
died (19:2).

Here was another confrontation. But this time, it was not
a confrontation with a weak-willed king. It was with his deter-
mined wife, who did not fear God or God’s people. She had
the political power at her command to execute him. He could
continue the confrontation, or he could run for cover. He ran
(v. 3).

Why? He had demonstrated the power of God in front of
the assembly. They had co-operated with him in the slaying of
the court prophets. The king had returned to Jezreel, a
broken man. They had all seen that Elijah’s word was reliable
concerning the advent and the ending of the drought. Why
should the word of this woman bother him? What did she rep-
resent that the others did nol?

What she represented was Satan’s self-confident determination
to smash the society of God. Like Pharaoh, she did not learn from
experience. The others in Israel were pragmatists. They
would follow those who seemed to possess power. Jezebel was
not a pragmatist. She was a consistent, disciplined enemy of

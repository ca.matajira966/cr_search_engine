30 CHRISTIANITY AND CIVILIZATION

battle to the Philistines. Ahab was to lose a final battle to the
Tyrians (I Ki. 22), The revolution did not come from below;
it came from above, and from an invader. But it was the
prophet who became the earthly witness-bearer to the transfer
of sovereignty from one king to another.

Jn the era of the New Testament, every converted man
bears the office of New Testament prophet, one who pro-
claims the truth of the gospel, the coming of Christ's kingdom,
The church is the institutional means of maintaining and
defending the continuity of this message. over time—a
message revealed in the Bible, the completed revelation of
God to man. Therefore, the church serves institutionally as
the anointing agency. In the United States, High Federal offi-
cials take their oath of office by placing their hands on a Bible.
In England the monarch is anointed with oil by the Arch-
bishop, This need not be a visible anointing. Elisha did not
publicly ordain Hazael. He simply announced his prophecy of
Hazael’s coming kingship. The civil government is always de-
pendent on God, and on the continuing support of the church
as a prophetic ministry.

Court prophets were necessary to Israel to lend a sense of
deep spirituality to the State. The king knew it was a fraud.
When he wanted counsel concerning an alliance between
Judah and Israel against Syria, Ahab knew who a true
prophet was, Micaiah, and he knew the man would prophesy
evil against the venture. When the prophet said, “Go, and
prosper, for the Lord shall deliver into the hand of the king”
(22:15b), the king knew it was a lie, and demanded that the
prophet tell him the truth, which the prophet did. Sure
enough, it was bad news. Israel would be scattered like sheep
without a shepherd. “And the king of Israel said unto
Jehoshephat, Did I not tell thee that he would prophesy no
good concerning me, but evil?” (v. 18).

The civil government is never independent of the ecclesi-
astical government. Every society appeals to some sort of priesthood
for support. In modern times, the priesthood is made up of hu-
manistic intellectuals. They are highly favored by the State,
as court prophets always are.* The civil government needs an
anointing. In times of revolution, meaning civil confrontation,

4. Murray N, Rothbard, For a New Liberty: The Libertarian Manifesto (rev.
ed.; New York: Collier, 1978), p, 544.

 

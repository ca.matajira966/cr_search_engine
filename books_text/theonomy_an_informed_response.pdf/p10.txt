x THEONOMY: AN INFORMED RESPONSE

I think it is reasonably safe to say that what had protected
the people of Israel from being oppressed by demonic spirits
was their judicial status as God's covenant people. Historically,
after their return from Babylon, they did not again worship the
older Canaanite gods. They turned to other forms of rebellion,
especially theological: what became known after the fall of
Jerusalem as Talmudic reasoning, i.e., a false hermeneutic
based on the principle of salvation by obedience to man’s laws.
Nevertheless, they did not openly worship demons. Thus, the
demons had wandered away. But they would soon return, Jesus
warned — not one by one, but eight by eight. Demons that had
long dwelt outside the judicial boundaries of the Promised
Land would soon return in force.

What has this got to do with a book on theonomy? A great
deal. Cornelius Van Til warned us, as no Christian philosopher
ever had before him, of the great danger of employing a false
hermeneutic to defend the faith. He warned against the myth
of neutrality, There can be no neutral facts. Like the swept
house in Jesus’ metaphor, the assumption of neutrality is a trap.
Unless Jesus Christ has swept the house and has installed a lock
and burglar alarm system, the original demon and his seven
companions will return. We cannot sweep the culture clean by
means of our own autonomous brooms. They are not neutral.
There is no “tabula rasa” — no cleanly swept intellect with which
we begin our lives by interpreting the world around us on a
covenantally neutral basis, We learn either as covenant-keepers
{e.g., Jacob) or covenant-breakers (e.g., Esau), We need special
grace: a clean sweep and a dead-bolt lock and burglar alarm.

‘Theonomists have been coming before the public ever since
1973 with a covenantal broom (Van Til's apologetic) and a
dead-bolt lock and burglar alarm system (biblical law). We have
warned any Christian or non-Christian who would listen that
(1) there can be no neutrality and (2) the so-called Christian
West’s apparent status as a covenant-keeping society is as mythi-
cal as Israel’s was in Jesus’ day. The demons will return.

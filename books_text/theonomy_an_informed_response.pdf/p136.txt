116 THEONOMY: AN INFORMED RESPONSE

considered this an uncomplicated and simple matter.® Far from
being “unwilling to admit” the difficulty (as Longman paints
me), I quite freely do so. What disturbs me, of course, are those
who insist it is an impossible task to perform — or something we
ought not even fry to do. If theonomic publications sometimes
give the impression that the application of the penal code is
simple and clear-cut, I trust that this literary shortcoming does
not, however, invalidate our arguments for the need to apply
the law of God to the pressing issues of crime and punishment
in our day. If we do not apply this, what better standard can be
offered?

A Sound Basis for Disagreement?

For the most part, then, Longman’s basic perspective is not
far from that of theonomic ethics. Where he does turn to dis-
agree with theonomic penology in a more focused or limited
area, readers will certainly recognize that his argumentation
becomes very weak. In fact, sometimes Longman disagrees with
the theonomic view without any argumentation or proof at all.”
At other times his thinking rests upon frivolous considerations
~ such as that theonomic penology would, mirabile dictu, require
“a new Mishnah” (p. 50)! Can’t we just hear early church here-
tics “refuting” the doctrine of the Trinity by pointing to the
dreadful evenwality that this doctrine will require systematic
theologies which are three-volumes long!

Sometimes Longman pursues non sequiturs - as when he
draws the conclusion that Christian legal judgments should be

6. This is recognized by Longman’s co-author, Vern Poythress, in another article
appearing in the same book: “Effects of Interpretive Frameworks on the Application
of Old Testament Law” (chapter 5).

7. For example, ke simply dismisses as “unsuccessful” with a wave of his hand
(stroke of his pen) the detailed and exegetically based theonomic argument against
the idea that Jesus departed from the Old Testament view of divorce and revoked
the civil penalty for adultery (p. 53). There is no demonstration even attempted by
Longman.

 

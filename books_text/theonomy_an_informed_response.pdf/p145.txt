Westminster Seminary on Penology 125

We should also remember that Johnson claimed the Mosaic
kind of penal sanction was necessitated (“entailed”) by the high
privilege enjoyed by Israel as the covenant people of God.
Johnson rightly identifies the church as the covenant people of
God today. But at this point Johnson proceeds to contradict his
own reasoning and now denies the necessity of that entailment
- claiming that the high privilege of being the covenanted
people of God no longer necessitates the penalties prescribed
by Moses! (The death penalty has been put aside.) This is arbi-
trary and inconsistent.

Moreover, according to biblical teaching, the people of God
under the New Covenant have an even greater privilege and
even greater responsibility than those under the Old. The
“entailed” penal sanction should then be at least, if not more,
demanding today than it was previously."® Johnson does not
explain the inconsistency in his pattern of reasoning.

Nor does he explain away the discrepancy in his claim that
the “cutting off” which only the covenant community may use
has passed from Old Testament Israel to the New Testament
church, This is a conspicuous equivocation since the Old Testa-
ment cutting off is taken by Johnson to be something involving
“physical force” imposed “to maintain the community’s purity
and integrity” — and yet the New Testament cutting off is taken
by Johnson to be a declaration of excommunication (without
physical force). The church does not carry out the same penal
sanction as Old Testament Israel at all; it is certainly not the
same in substance, on Johnson's hypothesis, but only perhaps
in aim. Accordingly, when Johnson's overall theory claims that
the Old Testament penal sanction has now passed over to the

12. ‘Those who tend to base their reasoning in creative, typological argurmenta-
tion must be careful just here not to respond thoughtlessly: “Well, of course, the New
Covenant sanction is more demanding because it involves eternal cutting off from
God.” Such an answer would relieve their logical problem at the heavy theological
expense of deuying that people in the Old Testament faced that same eternal judg-
ment for their sins,

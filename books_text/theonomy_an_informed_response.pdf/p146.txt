126 THEONOMY: AN INFORMED RESPONSE

church, Johnson’s own particular statements contradict that
part of his overall theory (by equivocation).

There are other problems with Johnson’s attempt to argue
his case about the Old Testament civil penalties from analogy
with the category of “sins which require cutting off” — not the
least of which would be that “cutting off” does not itself appear
to have denoted (even if sometimes including) a civil or even
humanly-inflicted penalty in the first place, if we do a careful,
exegetical study of the expression in Scripture.” Accordingly
Johnson would lose altogether any basis for arguing by way of
analogy from the penalty of “cutting off” to other kinds of civil
penalties in the Mosaic code.

Johnson’s proposed line of argument against the theonomic
view of the Old Testament penal sanctions is thus without a
reliable exegetical foundation and is internally flawed with
inconsistency and arbitrariness. These systemic weaknesses,
added to our original five questions, cause the argument to
crumble when any weight is placed upon it.

Appeal to 1 Corinthians 5

In his essay Johnson later observes Paul’s use of the expul-
sion language found in some Old Testament penal passages,
“Put away the wicked from among yourselves” (1 Cor. 5:13).
Johnson notes that here excommunication is the ecclesiastical
expression of that objective. But is this in any way contrary to
what one would expect based upon a theonomic perspective on

13, See Gordon J. Wenham, The Book of Leviticus, The New International Com-
mentary on the Old ‘Testament (Grand Rapids: Eerdmans, 1979), pp. 125, 241-42,
285-86. The punishment of cutting off was threatened against some sins which, by
their nature, were secret and thus difficult to prosccule, God Himself is sometimes
said to be the exccutor of the punishment, nol some human agent. In at least one
case cutting off is set in contrast to judicial punishment. Wenham argues that cutting
off does not generally mean excommunication from the covenant community because
that treatment is reserved for “uncleanness,” rather than criminality Cutting off
referred to “a threat of direct punishment by God, usually in the form of premature
death.”

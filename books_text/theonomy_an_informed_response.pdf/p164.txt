144 THEONOMY: AN INFORMED RESPONSE

applies prohibitions of murder and adultery to hatred and lust.
But does this mean that murder and adultery in their original,
physical sense are no longer condemned?

In the remainder of this chapter, I will retrace Johnson’s
survey of the New Testament references to Mosaic penal sanc-
tions. This will not only expose debilitating weaknesses in John-
son’s treatment, but will cast light on a proper exegesis of these
important passages. I bear in mind Johnson’s worthy maxim:
“We will need to exegete carefully any New Testament passage
in which a particular judicial law may be quoted, alluded to, or
commented on” (p. 175). In the next chapter, I will turn my
attention to his primary argument from Ilebrews.

Worst-Case Scenarios Based on Capital Sanctions

Before I actually engage his New Testament survey, I must
say a word or two regarding one of Johnson’s assertions that I
believe to be in error regarding the death penalty laws. This is
important, for all too often, antipathy to theonomic ethics is
engendered through worst-case-scenario analyses. These sce-
narios are usually generated through a misguided treatment of
the capital offenses in God’s Law.

Johnson suggests the possibility that the Lord’s warning in
Exodus 22:22-24 threatens civil proceedings: “You shall not
afflict any widow or fatherless child. If you afflict them in any
way, and they cry at all to Me, I will surely hear their cry; and
My wrath will become hot, and I will kill you with the sword;
your wives shall be widows, and your children fatherless.” John-
son comments: “It seems likely that this curse envisions the
Lord’s use of a human intermediary in executing his wrath, but
it is not clear whether this human instrument is to be the regu-

10. The classic illustrations of this are H. Wayne House and Thomas D. Ice
Dominion Theology: Blessing or Curse (Portland, Oregon: Multnomah, 1988) and Hal
Lindsey The Road to Holocaust (New York: Bantam, 1989).

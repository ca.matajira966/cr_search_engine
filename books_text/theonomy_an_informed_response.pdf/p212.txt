192 THEONOMY: AN INFORMED RESPONSE

idolatry (criminalizing such actions as cultural subversion and
public mayhem).

Conclusion

In the final analysis, we let Johnson undermine his own case:
“At issue is not divine justice in the abstract as a model of politi-
cal jurisprudence, but the Lord’s just expectation in view of his
people's covenantal obligations to him” (p. 187). That being the
case, how can Hebrews undermine the theonomic view of penal
sanctions? By the same parity of reasoning, since children are
commanded to be obedient to their parents in Paul’s cpistle to
the covenant community at Ephesus, may we legitimately argue
that children of parents outside the covenant are not to be obedi-
ent to their parents? This reductio could be applied across the
board to ail features. of the Ten Commandments, since they,
too, are given to the covenant community of Israel.

In Johnson’s conclusion, he notes that the New Testament
“calls us, even as we pursue evangelism and justice in the pres-
ent age, to look ahead to the only perfect disclosure of the
kingdom's righteousness, at the coming of the King, Jesus
Christ” (p. 192). As with so many who assail the theonomic
option, we are left wondering what is the nature of justice? What
is the standard by which we may judge civil law? May we even
seek civil justice in the present age, since it is only at the com-
ing of King Jesus that we may ever expect a perfect disclosure
of it? It is a tragedy of much contra-theonomic argumentation
that even if the arguments were valid, the Christian would be
lefi without any biblically rooted civil directives regarding civil
justice.

63. The false prophet in Deuteronomy 18:5 is not just a foolish monther of
error, but is a focus point for agitating che masses to rebellion. The prophets of Israel
“demanded that same obedience to their words as was due to the Law of God.” E. J.
Young, Introduction to the Old Testament (Grand Rapids: Eerdmans, 1964), p. 34. The
false prophets would tend to mirror the cultural function of the true prophets, and
were, thus, dangerons as conspirators.

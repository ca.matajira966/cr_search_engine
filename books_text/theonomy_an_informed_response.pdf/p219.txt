Editor’s Introduction to Part HIT 199

basically tyrannical, which is why I argue that Marcionism has
outlived Marxism as a system of historical interpretation.)

God sends covenant-breakers to hell. He resurrects these
people only to put them into an even worse condition for eter-
nity. He torments them (i.e., tortures them) forever. But when a
theonomist suggests that God’s covenantal agent, the civil mag-
istrate, can lawfully execute a convicted criminal in God’s name,
thereby transferring the criminal into God’s heavenly court, the
theonomist is identified as theologically deviant.

God provides an earnest (down payment) in history to both
covenant-keepers and covenant-breakers. The good get richer,
and the bad get poorer, long term. The wealth of the sinner is
laid up for the just. This is the covenantal argument of the
postmillennialist. The righteous will inherit the earth in history.

Another way that God extends His earnest in history is by
having the civil magistrate lawfully execute those who commit
certain crimes. A crime is defined biblically as a sin that the civil
magistrate is empowered by God to punish. The civil magistrate
does not make people good; he merely imposes penalties on
evil acts that can be proven in court to have taken place. I ask
three questions. Under whose covenantal authority? By what
judicial standard? In terms of what covenantal oath?

The Westminster faculty steadfastly refuses to answer these
three questions. They fudge. In doing so, they systematically
ignore the implications in history of the inescapable covenantal
status of the family. They are willing to do this because they are
determined to deny the covenantal status of the state.

The Question of the Church

What is the proper response of the church of Jesus Christ to
the issue of theonomy? Is it a heresy, as several dispensational
authors of paperback books have said? Is it outside of the Re-
formed theological heritage, as former pro-abortionist, former
dispensationalist, and former Westminster Theological Semi-
nary professor Bruce Waltke has argued in Theonomy: A Re-

Editor's Introduction 3

previously produced only three symposia from the school’s
inception in 1929, beginning with The Infallible Word in 1946,
why did they decide to write this one? I still find it revealing
that not one of the other three symposia was devoted to a refu-
tation of a particular theological movement. The faculty must
be publicly scratching where the itching is most intense. But
why is their itching so intense? Lurking here is the making of
a doctoral dissertation in the sociology of knowledge.

Pandora’s Box vs. the City on a Hill

In the story of Pandora and her opening of the closed box,
we learn that only one good thing came out of it: hope. The
Westminster faculty must have had hope regarding their collec-
tive effort. This is hard to believe in retrospect, but they must
have. They did not need to write their book. In writing it, they
revealed themselves as theologically unprepared, individually
and institutionally, to deal with the formidable theological issues
they let loose. In responding to their book, the theonomists
keep pointing to the numerous theological issues that the West-
minster faculty has left unresolved. These are not peripheral
issues; they are covenantal issues. The Westminster faculty had
been content for six decades to leave these issues unresolved,
but in 1990 they chose to reveal in public the extent to which
these issues have been left unresolved. Now they are being
called to account in public. It still baffles me: Why did they do
this? Why did they open the box? What did they hope to gain?

When Christian scholars reveal through public debate -
when they actually are willing to debate, which is rare — that
they do not accept another movement’s position, they must be
ready to offer a systematic biblical alternative to the rejected
position. It is not sufficient to register a few theological warn-
ings or a handful of claims, especially unsubstantiated claims.
The more detailed and comprehensive the opposing position is,
the more detailed and comprehensive the critics’ position ought
to be, They need to keep the rules of debate in mind.

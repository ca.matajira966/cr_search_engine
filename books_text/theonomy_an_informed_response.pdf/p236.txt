216 THEONOMY: AN INFORMED RESPONSE

tation of a progressively spreading cultural victory diminish the
reality of past and present salvific victory (our cternal standing
with God), except by the fallacy of equivocation regarding the
term “victory” in view?

Continuity and Discontinuity

Elsewhere Gaffin objects to postmillennialism that the “fu-
ture must be in continuity with and an unfolding of the eschat-
ological reality already present and operative in the church” {p.
205). But again there are problems with this assertion as an
objection to postmillennialism.

In the first place, we must ask: How is the progressively
unfolding victory of postmillennialism discontinuous with the
“eschatological reality’? The postmillennialist holds that the
cultural victory expected is the developmental fruition of the
kingdom-seed confidently planted and carefully nurtured by
Christ. Seeds grow. Slowly. Gaflin’s objection would better be
urged against premillennialism than postmillennialism.

Secondly, on Gaffin’s own view the completed victory of Christ
at the ascension will, nevertheless, allow the dramatic eschatologi-
cally significant occurrence of post-ascension events. He particu-
larly mentions the destruction of Jerusalem in A.D. 70, even
though it occurs forty years after the ascension (p. 204). Why
will he not allow the progressive conquest of the world by the
gospel through history as a developmental unfolding of the
implications of Christ’s past victory? He writes: “Certainly with
the first, ‘already’ installment there is room for different stages
or phases — marked off by cpochal events like Jesus’ baptism,
his death, resurrection, ascension and Pentecost, and the fall of
Jerusalem” (p. 204). If he can allow this based on the Scriptural
record, why cannot we do the same with the future prospects of
gradual conquests in history by Christianity, continuous with
the present and also based on the Scriptural record? One problem
with Gaffin’s article is that he has not dealt with the exegetical

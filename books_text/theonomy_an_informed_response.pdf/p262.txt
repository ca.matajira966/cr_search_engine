242 THEONOMY: AN INFORMED RESPONSE

because he quotes it later in his article. Listen to his accusation,
however, against Christian Reconstructionism. He writes:

It is critical to recognize all of these causes [of poverty]. None
of the reconstructionist writers seem to speak much of “calami-
ty,” the constant stream of factors in fallen nature (Romans
8:18ff.) that will bring poverty into our lives. (There are plenty
of poor people who are neither sluggards nor oppressed.)*

Come now, Dr. Keller, are you asking your readers to be-
lieve that “none” of the Reconstructionist writers discuss other
causes for poverty and particularly calamity? Dr. Keller knows
better because he quotes from the very piece of mine that gives
a wide range of explanations for poverty.

The Causes of Poverty

In 1985, I identified three different reasons why people are
poor: (1) those who are poor because of a special vow of pover-
ty as in the case of the Nazirite, (2) those who are poor because
they are being tested by God, such as Job, and (3) those who
are poor as a “result of God’s judgment or discipline. Two
examples of the second cause of poverty are given, Ruth and
the ‘beggars’ of the Bible.”® I go on to point out that in both
cases there was not necessarily culpability involved. Ruth, for
example, had done nothing particularly wrong to find herself
in need of begging. And of specific kinds of beggars, I say the
following:

In the New Testament, we find several classic beggars. There
is the blind beggar (John 9:8-9), Bartimaeus (Mark 10:46-52),
the beggar at the Gate Beautiful (Acts 3:1-11), and Lazarus
(Luke 16:19-31; not to be confused with Lazarus of Bethany). So

8. Keller, pp. 265-66.
9. Ray R. Sutton, “The Theology of Poverty,” The Geneva Papers (March 1985),
pd

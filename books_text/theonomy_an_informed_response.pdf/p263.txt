Whose Conditions for Charity? 243

what about these beggars? In each case, the Bible speaks favor-
ably... . We must take note that their plight is due to physical
handicaps. They are not like the sluggard who can but won't
work. . . . The laws of charity provided for them. Definitely the
poor tithe should have been used on people whe were forced to
beg for an existence. Yet, they still continued to go hungry.
Why? ‘The fault lay with the religious and political leaders. They
were not living by the Bible."

Significantly, Dr. Keller does not quote me on these important
points. Why, to hear him speak of me, one would think that I
did not recognize any of these classifications of the poor! Dr.
Keller has a problem: selective quotation. What he leaves out
would destroy his case; this is why it gets left out.

The Key Issue: Conditionality

What bothers Dr. Keller in particular, however, is the con-
cept of conditionality when it comes to helping the able-bodied
poor. He misleads the reader in two ways regarding this prin-
ciple. First, he implies that | am arguing for full obedience to
the Law of God before helping someone. He says, “[Sutton
believes that] we should have conditions — obedience to the law
and the covenant — before we give any aid.”"’ Instead, what I
advocate is a limited conditionality to test the intent of the
person asking for help. I suggest finding out if the person in
need goes to church or is willing at least to attend. George
Grant even has people pick up trash on the property. These
are limited conditions. They are not asking for perfect obedi-
ence. But they are conditions.

Second, Keller speaks in his article as though the application
of conditions is contrary to George Grant. As a matter of fact,

10. ibid.
LL. Keller, Theonomy, p. 274.

8 THEONOMY: AN INFORMED RESPONSE

The Westminster faculty has chosen to reject a theology
which insists that there is ethical cause and effect in history be-
cause God brings positive and negative sanctions in history in
terms of His covenant law.'° In short, they have rejected the
doctrine of the covenant in general, but especially in the civil
realm. They are willing to give lip service to an undefined
covenant — I stress the word undefined'' — as it applies to eccle-
siastical affairs. Yet we are still wailing to sec them present a
detailed treatment of the church covenant’s stipulations and
appropriate sanctions: Who gets placed under which sanctions,
why, and after how many years of appeals? They are willing,
with great fear and trepidation, to assert the existence of a
family covenant that is explicitly Christian. (Where is the West-
minster faculty’s published treatment of the key problems of
divorce: Who gets the children, who gets the assets, who gets to
remarry . . . and how soon?) But when it comes to the civil
covenant, they have put a big sign on the seminary’s front lawn:
“No Restrictive Covenant.” That is to say, “No Restrictive Chris-
tian Covenant.” There is always some restrictive civil covenant.

today, it’s hermeneutics.

10. The reader needs to understand that the theonomists’ view of God’s corpor-
ate sanctions in history is tied to our view of the sacraments. (I am nol necessarily
speaking here regarding Rushdoony’s view of the sacraments, which is at best
problematical.) Theonomists reject any view of the sacraments that says thal they are
mere memorials or symbols. We believe the sacraments have judicial content (God's
law) and that appropriate sanctions are attached to them, We believe that God is
present judicially with His people in a unique way in the sacraments, meaning that
He is with them historically. For example, the Lord's Supper is more than a meal or
the symbol of a mcal. God brings sanctions in history against those who participate
unlawfully in the Lord's Supper (I Cor, 11:27-32). The burden of theological proof
rests on those who deny that God brings predictable sanctions in history (Deut, 28).

  

11. The place to have presented a unified, agrocd-upon definition of the coven-
ant, and hence covenant theology, was in Themnomy: A Reformed Critique. That the
Westminster Seminary faculty once again chose to remain silent on this crucial point,
despite my pointed exposing of Calvinists’ theological nakedness in this regard (in my
Publisher's Preface to Ray Sutton’s That You May Prosper: Dominion By Covenant, 1987),
is indicative of the faculty’s underlying problem. They are not agreed on what the
biblical covenant is. They remain silent. Yet they are Calvinists: covenant theologians.

Hermeneutics and Leviticus 19:19 267

arises: Was Leviticus 19:19 itself an economic curse? Second, is
it still in force? If it is, then isn’t our high per capita wealth
today — seemingly a great blessing from God — judicially illegiti-
mate? If it is, how can a theonomist explain this anomaly?
More specifically, is the defender of free market capitalism
forced into an untenable ideological position if he also defends
the continuing authority of biblical law? Is the modern world’s
wealth an example of God’s perverse blessing on antinomian
Christianity and humanism, generation after generation? Or is
the modern world’s abandonment of Leviticus 19:19 legitimate?
Is Leviticus 19:19 a case law that was annulled by the New
Covenant? If it is, then is this fact itself theological justification
for announcing the annulment of all Old Testament case laws?

The Enclosure Movement

Before we pursuc these topics, one development must be
remembered: the enclosure movement. There is no doubt that
the genetic specialization of herds and crops was made possible
economically by the steady enclosure of the medieval common
fields or commonly tilled soil. The enclosure movement began
early in England, certainly by the thirteenth century.” It ac-
celerated in the sixteenth century.’ After 1760, Parliament
authorized specific enclosure by private acts.** The steady par-
titioning of the common fields made possible the so-called agri-
cultural revolution in England. (A revolution that takes well
over a century is evolutionary by modern standards, though
perhaps not by pre-modern standards.) Ashton writes: “Prog-
ress in agriculture was bound up with the creation of new units
of administration in which the individual had more scope for
experiment; and this meant the parcelling out and enclosure of
the common fields, or the breaking up of the rough pasture

88. W. E. Tate, The Enclosure Movement (New York: Walker, 1967), pp. 60-61.
34, Bid. ch. 6.
35. ibid. p. 48.

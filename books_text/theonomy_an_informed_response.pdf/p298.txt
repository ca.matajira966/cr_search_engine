278 THEONOMY: AN INFORMED RESPONSE

cities, where the jubilee laws did not apply (Lev. 25:32-34). Levi
was separated, until Shiloh came.

Leviticus 19:19 is part of the Old Covenant’s laws governing
the preservation of the family’s seed (name) during a particular
period of history. It was an aspect of the necessary preservation
of genetic Israel. The preservation of the separate seeds of Is-
rael’s families was basic to the preservation of the nation’s legal
status as a set-apart, separated, holy covenantal entity. This
principle of separation applied to domesticated animals, crops,
and clothing.

Animals

Let us begin with the law prohibiting the mixing of cattle.
Did it refer to bovines only? The Hebrew word is transliterated
behemah, the same word that we find transliterated as behemoth
in Job 40:15. In every reference to cattle in Leviticus, this He-
brew word is used. Did this law apply only to cattle? What
about other domesticated species? A case can be made both
ways. Nevertheless, I believe that caile in this case refers to all
domesticated animals. ‘The parallel prohibition against mixing
crops was generic. Also, the Hebrew word behemah is used gen-
erically for all domesticated animals in the laws against bestiality
(Lev. 18:23; 20:15). This prohibited activity was less likely to be
performed with bovines than other, smaller beasts.

Another reason for translating behemah broadly as domestic
animals in general is found in the law identifying the Levites as
a special tribe, God’s firstborn. In setting aside the Levites as a
separate, holy tribe in the midst of a holy nation of priests, God
also designated their animals as representatives of all the ani-
mals in Israel. At that first census of Isracl, the people did not
have to make a payment for the firstborn animals as part of the
required sacrifice of the firstborn males (Num. 3:41, 45). The
Hebrew root word for cattle in this verse is behemah. The pay-
ment to the temple in Numbers 3:49-51 does not mention a
payment for the animals. This absence of payment indicates

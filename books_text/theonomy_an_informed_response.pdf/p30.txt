10 THEONOMY: AN INFORMED RESPONSE

are unwilling to defend the idea of a biblically covenanted civil
government. They have allowed the humanists to provide this
leg. Give humanists this leg, and they'll break your arm.

I ask the faculty: On what judicial, covenantal basis can
Christian political pluralists mount a successful defense against
covenant-breakers’ rival civilizations? How do they expect to be
able to defend institutionally the covenantal sovereignty of the
Christian church and the Christian family when they deny the
legitimacy of the Christian state? They refuse to address this
obvious problem — the problem that theonomists have been
dealing with in print for almost two decades. The faculty wrote
Theonomy. A Reformed Critique without once dealing with this
problem. They seem to think that they can save themselves
(and all the rest of us) from the self-conscious, well-financed
(with Christians’ tax money) onslaught of covenant-breaking
civilization. How? By negotiating a temporary cease-fire with
the humanists: publicly abandoning Christ’s judicial claims in history
as King of kings. They seem to think that this temporary cease-
fire is the equivalent of a permanent peace treaty that is gov-
erned by a team of neutral referees, But there are no neutral
referees. There are therefore no permanent treaties — dare I
say the word? covenants — between covenant-keepers and coven-
ant-breakers. The intellectual defense of this conclusion was
Van Til’s legacy to Westminster: his rejection of natural law
theory. This legacy has been abandoned by the present faculty.

Conclusion

The covenantal issues that have been raised by the theono-
mists are not merely technical issues of biblical exegesis. They
are issues of life and death, as Ree v. Wade makes clear. These
issues cannot successfully be bottled up in a seminary classroom
and treated as if they were academic exercises for graduate
students. Yet the Wesminster Seminary faculty attempted to do
just this. They dealt with theonomy as if the theonomists were
a group of fairly bright graduate students who are in need of

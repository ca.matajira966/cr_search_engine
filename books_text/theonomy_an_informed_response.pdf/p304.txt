284 THEONOMY: AN INFORMED RESPONSE

people: they had to wear linen whenever they served before the
table of the Lord. They had to put on linen garments when
they entered God’s presence in the inner court, and remove
them when they returned to the outer court. No wool was to
come upon them (Ezek. 44:15-19). The text says, “they shall not
sanctify the people with their garments” (Ezek. 44:19). Priestly
holiness was associated with linen."*

Additionally, the laws of leprosy were associated with linen
and wool. The test to see whether leprosy was present was to
examine wool or linen garments (Lev. 13:47-48, 52, 59). No
other fabric is mentioned. The question arises: Why linen and
wool? Why were they singled out? More to the point, why were
they spoken of together in this prohibition?

No Sweat

Wool is produced by sheep, while linen is a product of the
field: flax. Why? It probably had something to do with sweat as
man’s curse (Gen. 3:19). Linen absorbs moisture. The priest
was required to wear a garment of pure linen. He was to wear
a garment that absorbed sweat. His judicial covering was to
reduce the amount of sweat on his body. Wool, in contrast, is
produced by the same follicle that produces sweat in a

50, On this point I disagree with James Jordan and all of the authorities he cites,
both gentiles and Jews. Their argument is that because the high priest’s clothing was
colored, it had to be a mixture of wool and linen because linen is difficult to dye.
Jordan cites Exodus 28:5-6. But this passage says that even the thread had to be
linen (w, 6). I can find no passage that indicates thar the priests wore anything but
jinen when they brought sacrifices before God, ‘This includes Exodus 39:29, which
Jordan also cites, This is unquestionably the case in the post-exilic period. I think it
is safer to go with the language of the texts than with a theory of ancient dyeing
techniques. Jordan and several of the authorities he cites claim that the mixture of
fabrics was itself holy, so non-priests could not lawfully wear such mixed clothing. T
argue the opposite: pure linen was holy, so the wooF-linen mixture was forbidden.
See James Jordan, “The Law of Forbidden Mixtures,” Biblical Horizons Occasional
Paper No. 6, pp. 3, 6. In any case, this issue was holiness. It had to do with the
separation of priests from non-priests: within the land of Israel and between the
priestly nation of Israel and the non-priestly nations.

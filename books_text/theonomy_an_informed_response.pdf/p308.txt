288 THEONOMY: AN INFORMED RESPONSE

What Did the Verse Mean?

Specialized breeds of animals could be imported and used by
the Israelites. These breeds could not be lawfully produced by
design or neglect (unrepaired fences) in Israel. Their use was
legal; their production was not. In contrast, the mixed fiber
cloth could be produced in Israel but not worn by an Israelite.
It had to be sold to resident aliens, exported, or used for pur-
poses other than clothing. The language of the clothing law was
specific: “neither shall a garment mingled of linen and woolen
come upon thee.”

These differences in the laws point to different symbolic
meanings. Leviticus 19:19 is a case law that illustrated a single
principle: the necessity of separation. First, the separation of the
tribes of Israel: the prohibition against (1) genetic mixing of
animals and (2) the simultaneous planting (mixing) of more
than one crop in a single field.

Second, section three illustrated the holy (separated) condi-
tion of Israel as a nation of pricsts: mandating the separation of
wool and linen in an Israelite’s garment. These two fibers are at
cross purposes with respect to man’s curse: sweat. They were at
cross purposes ritually with respect to priestly sacrifices. There-
fore, they could not be cross-woven into clothing intended for
use by Israelites. The cloth could be exported to non-priestly
nations. It did not matter what they did with it. No lawful
sacrifices could be offered in their lands.

Third, the clothing law also was part of the tribal separation
laws because this law separated commoners from the Levites
during formal services, especially in the post-exilic period. But
its primary function was to separate a priestly nation from non-
priestly nations. The separation was therefore primarily coven-
antal, not tribal.

The first two laws governed what was done in a man’s fields.
The fields were under his control. Thus, whatever separation
the breeding laws required had to be achieved by establishing
boundaries inside a man’s property. If there was a functional

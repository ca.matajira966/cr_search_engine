A Pastor's Response 297

especially the teaching and preaching of the Reformers and
Puritans. I also recognize that there is much exegetical work
necded to be done to learn how Old ‘Testament Law applies to
our contemporary situation, and I am not aware of anyone who
is claiming to have all the answers, though it might be per-
ceived that way. All this is meant to serve as background for the
following... .

A Church’s Struggle and Response to
Theonomy: A Reformed Critique

The church’s struggle did not begin with the issue of theon-
omy per se. When J was called to the pastorate, I openly and
honestly answered questions concerning theonomy, and the
pulpit committee and elders had no difficulties at the time with
my position. The struggle began with church discipline.

Recognizing that the three marks of the church are and have
been commonly recognized as the preaching of the Word, the
proper administration of the sacraments, and church discipline,
the session began to make efforts (admittedly imperfect efforts)
at restoring and reclaiming some within the congregation who
had strayed. Many in the congregation appreciated what we
were doing and understood the biblical requirement and teach-
ing concerning church discipline. But there were those few who
had serious objections, and their reasons for opposing the
implementation of church discipline are familiar to any pastor
who has endeavored to be faithful in this area: “we have never
done it this way before,” “discipline is unloving,” “discipline is
legalistic,” and so on.

The vocal opponents to the implementation of church disci-
pline had to find some reason the pastor and session were
being “so mean and harsh.” Theonomy became the convenient
scapegoat. As diligent as the session and I were in trying to
convince people that Matthew 18:15-20 and I Corinthians 5
were the reasons for doing church discipline, not theonomy,
our opponents were convinced otherwise, and they began to

12 THEONOMY: AN INFORMED RESPONSE

publicly abandoning what little remains on campus of Prince-
tonian or Dutch Calvinism’s world-and-life view and Van Til’s
apologetic legacy. Frankly, I do not think they will respond in
print. But if they remain publicly silent, as if the Insticute for
Christian Economics had not published three volumes of de-
tailed responses to Theonomy: A Reformed Critique, then their
brighter students and the seminary’s more theologically astute
donors will know what the faculty’s strategy was: hit and run. If
that was their strategy - Kline’s strategy in 1978 — it has now
backfired: the victims survived and are now pressing charges.

Why they went into print with Theonomy: A Reformed Critique
is beyond me. They are now into the theonomic tar baby up to
their elbows. It will be interesting to sce how they attempt to
get free. The theonomists will be watching. So will the semin-
ary's theologically competent students.

I will say one more thing regarding Westminster's strategy.
The faculty did not in their wildest dreams imagine, when they
began working on their book, that Ray Sutton, the victim of an
astoundingly misleading and intellectually incompetent attack
by Timothy Keller, would become president of Philadelphia
Theological Seminary exactly one year after Theonomy: A Re-
formed Critique appeared in print. I argued in Westminster’s Con-
fession that the Westminster faculty for over a quarter of a cen-
tury treated theonomists as if we were so far out on the theo-
logical fringe that we were hardly worth considering. Some
chapters in their book almost casually dismiss theonomy. Such
an unprovoked attack looks idiotic when one of the victims then
assumes the presidency of a Reformed seminary that is more
than a century old — half a century older than Westminster.

(I wonder if Dallas Theological Seminary will publish a sym-
posium on theonomy. Maybe they can ttle it, Oil, the Middle
East, the Antichrist, and Theonomy. They can follow that one with
another: The Middle East, Oil, Theonomy, and the Antichrist. And
then: The Antichrist, Theonomy, and Middle Eastern Oil. This can
go on for years. Don’t take my word for it; ask John Walvoord.)

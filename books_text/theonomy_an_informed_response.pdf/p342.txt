322 THEONOMY: AN INFORMED RESPONSE

Seminary has now abandoned Van Til’s legacy. In its rejection
of theonomy, Westminster has returned to a vague natural law
theory: a retrograde move if ever there has been one in the
history of the church. So, it is my goal to call into question the
wisdom of such a move backward. Van Til’s legacy is too pre-
cious to surrender. The church waited for almost two thousand
years to escape from Athens. Van Til accomplished this. I am
willing to go to some extra trouble and expense to defend Van
Til and his system from those who are eager to abandon it.
Van Til stripped Christian apologists of any valid reason to
appeal to man’s hypothetically neutral reason in a philosophical
defense of the faith. But he did more than this: he stripped
Christian social theorists of any valid reason to appeal to pagan
man’s natural law theory. Once he had done this in the name
of Calvinism, he opened the door to a revival of interest in
biblical law, whether he appreciated this revival or not (and he
did not). By pushing Calvinists back to the Bible in philosophy,
he necessarily also pushed them back to the Bible in ethics.
This book has enabled us to make a fundamental point:
Calvinism is inherently theonomic, It is not simply that theonomy
has in the past been Calvinistic in perspective; it is that Cal-
vinism is inescapably theonomic if Van Til’s approach to philos-
ophy is correct. By responding to the Westminster faculty, we
have been given an opportunity to make this point. The faculty
may reject it, but to make this rejection plausible, they must do
one or more of the following, and do it in public: (1) reject Van
Til’s apologetic system in favor of evidentialism or rationalism
or a mixture of the two; (2) spell out in detail where Van Til
went wrong theologically and logically; (3) openly revive some
clear, straightforward version of religiously neutral natural law
theory; and (4) give a theoretical defense of political pluralism,
meaning a Bible-based defense, which has never been done in
church history. In short, to respond to this book and the two
previous books we have produced in response to Theonomy: A
Reformed Critique, the Westminster faculty will have to move

Editor's Conclusion 345

that the theonomists are correct in their assessment of the
church’s possibilities in fulfilling the dominion mandate in
history (Gen. 1:26-28). Similarly, a Reformed, non-dispensa-
tional seminary that refuses to acknowledge publicly that Gene-
sis 1 imposes a mandatory blueprint for the discussion of the
origins of the universe and mankind’s place in it is not likely to
acknowledge publicly that the Pentateuch imposes an equally
mandatory blueprint for social reconstruction.

it boils down to this: a Calvinist who believes that the origin
of the universe is chronologically indeterminate, and who also
believes that the cultural failure of Christianity in history is
eschatologically determined in a wholly predestined universe, is
not in a strong position to lead Christians into the battle for the
minds of men in a time of crisis. He will not readily march into
the machine gun nests and barbed wire of humanism. If you
doubt the accuracy of this negative assessment, then I suggest
that you read Theonomy: A Reformed Critique.

It is time for Calvinist seminaries to allow thconomists to
come on campus and engage in public debate with faculty
members in front of the assembled student bodies. If the vari-
ous faculties are unwilling to do this, as they have been ever
since Bahnsen was fired by Reformed Theological Seminary,
then the students need to recognize the hit-and-run tactic that
has been adopted by the various faculties. They have all sub-
stituted this tactic for their earlier one: academic blackout. First,
the blackout failed at Dallas Seminary. Dallas professor H.
Wayne House decided to join Rev. Tommy Ice in writing Dom-
inion Theology: Blessing or Curse? Dr. House is now employed
elsewhere. Next, the blackout failed at Westminster Seminary.
The faculty decided to write Theonomy: A Reformed Critique. This
tactic has now proven equally disastrous. What next? Public
debate? Will we get to see Bahnsen and Gentry vs. two (or
more) members of the Westminster Seminary faculty? “More
fun to watch than world championship tag-team wrestling!”

Gentlemen, it is time for you to make some hard decisions.

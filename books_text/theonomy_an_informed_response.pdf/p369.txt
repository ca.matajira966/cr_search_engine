The Sorry State of Christian Scholarship 349

the American news media.° We generally expect cover-ups and
the rigging of the game by humanists. A comprehensive Los
Angeles Times study found “that the press often favors abortion
rights in its coverage, even though journalists say they make
every effort to be fair.””

Consider the creation-evolution debate. When they want to
slam creationists, evolutionists are quick to turn to the media
because they recognize its secular bias. “Following minor crea-
tionist media victories in late 1981, Samuel P. Martin of the
Department of Anthropology, University of Illinois, felt com-
pelled to suggest in a letter to Science (‘Confronting Creation-
ism,’ February 5, 1982) that large science organizations such as
the AAAS [American Association for the Advancement of Sci-
ence} and the National Academy of Science (NAS) use their
funds ‘to initiate a major media assault on the creationists now
~ before the cracks in the dike turn to fissures.’ ”*

The evolutionists sing a different tune, however, when they
are confronted in open debate with their creationist antagonists.

On October 13, 1981, Duane Gish of the Institute for Cre-
ation Research debated biologist Russell Doolittle of the Uni-
versity of California, According to spokesmen for the scientific
community itself, Gish routed Doolittle. Unfortunately for the
biologists, the debate was recorded for broadcast on national
television at a later date. In the November 6, 1981, edition of
Science Roger Lewin was making excuses beforehand. Members
of the NAS and the National Association of Biology Teachers
(NASBT) were “appalled” by the debate. They describe Gish’s
presentation as “slick” and “timed to the last second.” Doolittle’s

6. Marvin Olasky, Prodigal Presi: The Anti-Christian Bias of the American News Media
(Westchester, Illinois: Crossway Books, 1988).

7. David Shaw, “Abortion Bias Seeps Into News,” Reprinted from Las Angeles
Times (July 1, 1990), p. 1.

8. William R. Fix, The Bone Peddlers: Selling Evolution (New York: Macmillan,
1984), p. 314.

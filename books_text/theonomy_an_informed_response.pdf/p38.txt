18 THEONOMY: AN INFORMED RESPONSE

Rushdoony saw what Van Til was unwilling to admit, in
public or in private: if Van Til is correct in his assertion that
only the Bible is an authoritative standard in history, then we
have to turn to the Bible in our quest as Christians for an alter-
native to humanist social and legal theory. This means not only
that Christian philosophy must be reconstructed; all of man’s
institutions need to be reconstructed. Van Til was unwilling to
recommend this broadly conceived application of his narrowly
circumscribed technical task, and he was suspicious of both
Rushdoony and me in our attempt to apply biblical law in social
theory. But he kept quiet in public about his suspicions.

Rushdoony recognized early that Van Til’s apologetic revolu-
tion was new wine in very old wineskins, but he was not ready
until 1973 to offer a published alternative. From the publication
of By What Standard? in 1959 until the publication of The Insti-
tutes of Biblical Law in 1973, he did not provide readers with a
detailed application of what he knew in theory is mandatory: a
reconstruction in social theory based on biblical law. He began
his lectures on biblical law in 1968; they were not available to
the reading public until 1973.

Gary North

I was converted to predestinarianism by a dispensational
Bible teacher, Jack Arnold (Th.D., Dallas Seminary), who
taught a Bible class at the University Bible Church in West-
wood, California, home town of UCLA, in the early 1960's. The
church’s pastor, Milo F. Jamison, in 1934 had been the first
pastor to be thrown out of the Presbytcrian Church USA for
orthodoxy. He had refused to participate in the newly formed
off-campus religious ministry at UCLA. His presbytery insisted
that Jamison bring his popular Bible study’s 200 students into
the ecumenical organization. He refused. The Presbytery then
erased his name from the Presbytery’s roles without a public
hearing or even a closed-door discussion with him, He was soon
in contact with J. Gresham Machen, and he joined in 1936

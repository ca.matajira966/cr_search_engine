370 THEONOMY: AN INFORMED RESPONSE

future of, 200
growth, 202
keys, 81-82
Machen on, 92
past-orientation, 217-18
privilege, 125
sanctions, 118, 125, 161
sovereignty, 10
suffering, 220, 223-24
vague (1970's), 66
circumcision, 270-71, 286
civil disobedience, 59, 64
civil magistrate, 81, 84, 100,
101, 103, 132
civil religion, 35-36, 36
civilization, 9-10, 208
clothing, 270, 283-87, 289
Clowney, Edumnd, 81-84, 87,
197
college, 326, 334-35
Colson, Chuck, 38n, 75-76
comings, 218
common grace, xx, 31, 45n
conditionality, 243, 246-49
confession, 289
Congress, 95
consensus, 69
conservatism, xiv-xv
conspiracy, 191
Constantine, 198
continuity, 120-21, 140, 298
controversy, 347
costs, 291, 338
court prophets, xix
courts, 34n
covenant
cease-fire, 10
Christendom, 85

Church, 8
civil, 7, 9, 84, 85
conflict, 17
contrast, 169
family, 8
inescapable concept, 8,
8-9
Johnson, 140, 165-66
Lord of, 121
mediation of, 170
model, 82
neutral, 196
people, 125
restrictive, 8
signs, 20
theology, 8, 82-83
treaty, 10
undefined, 8
Unitarian, 85
visible, 247
Westminster Seminary,
82
creation, xx, 72, 271
creationism, xiv, 82, 330, 344,
349-50
credentialism, 333-41
creeds, xiv
crime, 6, 34, 199
crisis, xiv, xviii, 341-42, 345
Cromwell, Oliver, 7, 335
crop rotation, 290
Curry, Dean, 24

Dallas Seminary, 11
dark age, 352
Darwin, Charles, 329
daughters, 272
David, 272-73

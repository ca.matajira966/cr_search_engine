Jones, Stuart, 208
Jordan, James, 284n
Joseph, 251-52
jubilee, 259, 290
Judah, 271, 272-273, 276
Judaizers, 270
judgment, 145, 148
Judgment Day, 187
judicial theology, 20-22
jurisdiction, 315, 320
justice
biblical law, 139
debate over, 88
definitions, 42
eternal, 156
Jesus on, 147
magistrate, 139, 156
millennial, 87
Mosaic law, 147
pluralists talk, 36
Schaeffer’s view. 62-64
standard of, 88
unchanging, 131
undefined, 136, 192
universal, 132n

Keller, Timothy
Chilton, 234-35
conditions, 246
false witness, 233, 245
Grant, 236-37
healing state, 197
message, 253
North, 234-36
poverty’s causes, 234,

241-42
selective quotation, 245
theonomy, 233

375

uncharitable, 241
Kennedy, John F., xi
keys, 81-82, 195
Kevorkian, Jack, 72
kingdom of God

Augustine, 45

Christendom, 324

civilization, 198

gradualism, 214

Hodge on, 31

keys, 195

progressive, 211-12
Kline, Meredith G.

abortion, 74-75

absence of, 2

attack on Bahnsen, 1

covenant signs, 20

dispensational, xx

followers of, 343

link to Gordon-Conwell,

xix

sleight of hand, xx

sweetheart deal, 2

theonomy & confession,

27n, 205
Kuhn, Thomas, xi, xviii
Kuschke, Arthur, 330
Kuyper, Abraham
common grace, 45n
complete?, 39-40
natural law politics, 249-
50

repair manual, 40

rhetoric, 41

sphere sovereignty, 41,

44

lake of fire, 315-16

376 THEONOMY: AN INFORMED RESPONSE

land
boundarics, 267-68
curse, 286-87
daughters, 272
debt &, 268
enclosure, 267-68
family &, 271-72
field, 273
inheritance, 272
levirate, 272
name &, 272
primogeniture, 268
Promised, 275, 277
seed &, 277, 281
specialization, 268
tribes &, 273-74

languages, 337

last bastion, xiii-xiv

Laurel, Stan, xviii

law
apostasy, 166-68
authority, 196
Calvinism &, 54-55
ceremonial, 258
changes in, 260
civil, 76
debate over, 83
dispensationalism, 51
emeritus, 196
equality before, 34
evangelism, 132n
eye for eye, 146
flexible, 113-14
function, 258
functions of, 143
“harsh,” 305-7
heart, 148
hearts, 53

inheritance, 271-72
Jesus on, 146
justice, 139, 183-84
Levitical, 176-77
Machen on, 90-91, 93,
95
Mosaic & natural, 102
natural, 196, 255, 260,
322, 333
natural revelation, 101
ordinances, 154-55
particulars, 59-60
pluralism, 33-34
pluralism &, 46
priesthood, 130, 178
priesthood &, 128-29,
174-75, 275-76
Puritans &, 6-7
resurrected, 7
rival systems, 46
sacraments &, 259
sanctions, 76-77, 184
sanctions &, 6
Schaeffer’s view, 59-64
seed, 277
seeds, 275
“separation,” 261, 269
source of, 43-44
tables of, 99-101
Westminster Seminary,

96
worship, 51
Lee, F. N., 330

 

Lenin, xviii

leprosy, 284

Levi, 277

levirate marriage, 272-73
Levites, 273-74, 277-78, 283-84

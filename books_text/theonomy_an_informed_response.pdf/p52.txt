32 THEONOMY: AN INFORMED RESPONSE

many, part of a “smorgasbord ethic.” Pluralism is the new catch
phrase of those within and without the Christian community.
Of course, the term means different things to different people.
This is its danger. Groothuis writes:

Pluralism refers to a diversity of religions, worldviews, and
ideologies existing at onc timc in the same society. We are social-
ly heterogeneous. One religion or philosophy doesn’t command
and control our culture. Instead, many viewpoints exist. We have
Buddhists and Baptists, Christian Reformed and Christian Scien-
tist — all on the same block, or at least in the same city. This can
have a levelling effect on religious faith.”

With the levelling of religion, we are seeing the levelling of
morality. All lifestyles are permitted in the name of diversity
and pluralism. In nearly every case, Christians are the losers.
Pluralism is the bait for Christians to throw caution to the wind
as we are called on to “trust” secular and religious advocates of
pluralism. Christians are encouraged to set aside only a few of
the distinct doctrines of the faith, those that are inherently
“religious.” Once these are discarded, the friendly pluralists tell
us, Christians are then free to speak.

The call for Christians to adopt pluralism is just another way
of diluting the truth. Pluralism becomes a club to pound flat
the theological bumps that make Christianity unique among all
the religions of the world. And what is the fruit of the “new and
improved” pluralist worldview? Harold O. J. Brown writes:

‘As soon as the words “Our pluralistic socicty will not permit

.” are uttered, Nativity scenes are dismantled, Christmas
vacation becomes Winter Holiday, and a moment of silence in
public schools is no longer merely a vain illusion but a prohibit-
ed sin against pluralism. But say “Our pluralistic society requires
...” and homosexual activists receive affirmative action support

20. Douglas Groothuis, “The Smorgasbord Mentality,” Eternity (May 1985), p. 32.

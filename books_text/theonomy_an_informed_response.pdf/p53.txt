Theonomy and Calvinism’s Judicial Theology 33

for job demands, parents need not be notified ofa minor daugh-
ter’s intention to abort their grandchild, and Rotary Clubs and
saunas are gleichgeschaliet into unisex. Whether or not one cn-
dorses pluralism seems to be a litmus test for whether one is
persona grata in the modern world.”

Christian pluralists have abandoned the very doctrines that can
make a fundamental difference in this world: the uniqueness of
Jesus Christ and the uniqueness of God’s written revelation.

Is pluralism biblically defensible? Should the Christian in
principle back off, giving equal opportunity to other competing
minority or majority positions in the name of pluralism, when
those positions advocate unbiblical and anti-Christian lifestyles?
Do we allow abortion for competing systems when its advocates
claim the “pluralist” model in defense of their position? Should
the State allow “homosexual” marriages? Should the Mormons
be permitted to practice polygamy, which the Mormon hierar-
chy has never publicly renounced as a religious ideal?*
Should Satanists be permitted to worship according to the
“dictates of their own conscience”?

The Bible teaches pluralism, but a pluralism of institutions
under God’s single comprehensive law system.” Scripture

21. Harold O. J. Brown, “Pluralism in Miniature,” Chronicles (May 1988), p. 13.
R. C. Sproul’s discussion of pluralism is helpful. Sce his Lifeviews: Understanding the
Ideas that Shape Society Today (Old Tappan, New Jerscy: Fleming H. Revell, 1986), pp.
119-27.

22. The Supreme Court declared that polygamy was out of accord with the basic
tenets of Christianity: “It is contrary to the spirit of Christianity and the civilization
which Christianity has produced in the Weslern world.” Late Corporation of the Church
of Jesus Ghrist of Latter Day Saints v. United States, 136 U.S. 1 (1890). A year earlier the
Court declared that “Bigamy and polygamy are crimes by the laws of all civilized and
Christian countries. . . . To call their advocacy a tenet of religion is to offend the
common sense of mankind.” Davis u. Beason, 133 U.S, 333, 341-42 (1890). Cited in
John Eidsmoe, The Christian Legal Advisor (Milford, Michigan: Mott Media, 1984), p.
150, Pluralism’s operating doctrine has now opened the door for the ACLU to
abolish restrictions on the marriage vow. Under ACLU pluralism, polygamy ought to
be allowed. And why not?

23. Gary DeMar, Ruler of the Nations: Biblical Blueprints for Government (Ft. Worth,

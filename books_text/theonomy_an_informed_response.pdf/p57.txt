Theonomy and Calvinism’s Judicial Theology 37

“rights” groups want full and unrestricted freedom to practice
their “alternative lifestyle.” How does the doctrine of pluralism
answer these requests for Icgal and civil legitimacy?

Pluralism was not set forth as an option while I was a stu-
dent at RTS until theonomy came along and attempted to put
wings on the Reformed-Calvinist “plane.” It was a commitment
to Reformed theology that led me to embrace the principles of
Christian Reconstruction and theonomic ethics.

Students at RTS were always told that the Calvinist biblical
world-and-life-view plane would fly. Rarely, however, did we
ever see a modern Calvinist plane with wings or engines. We
never saw the plane actually fly. This was a frustrating experi-
ence. When the distinctives of theonomy actually put the plane
in the air, the control tower would call us back for modifi-
cations to the fuselage. Yes, the plane was in the air, but we
were told that it was unstable with the theonomic wings. We
were told that the wings had been tried before but met with
little success. And so the Calvinist plane sits on the tarmac with
no place to go,

No Credible Alternative

What happened at RTS that led me to become a theonomist?
Why was there such a negative reaction by numerous profes-
sors, the administration, board members, and pastors to the
“theonomic attraction” that many of us saw as simply the work-
ing out of Reformed distinctives that we were taught in the
classroom?

I really couldn’t understand what the fuss was all about. The
plane was flying. Isn’t this what we were being told should
happen when the Bible was applied to every area of life? We
were even able to shoot down the dispensational air force with
a consistent barrage of biblical fire power. The humanist air
force did not have a chance, once we forced the secularists to
be consistent with their man-centered, naturalistic presupposi-
tions. When we shut off their fuel supply (which they were

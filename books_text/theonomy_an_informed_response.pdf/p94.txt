74 THEONOMY: AN INFORMED RESPONSE

revelation will be interpreted in different ways depending on
what ideology is in vogue, A prevailing atheistic regime will
interpret general revelation one way, while a New Age human-
ist will put another slant on it. In each case, the church’s pro-
phetic ministry is depreciated.

It is amazing to read critics of theonomy who maintain that
general revelation is depreciated by theonomists. As an inde-
pendent ethical system, yes.”” The Westminster Confession of
Faith clearly states that the “whole counsel of God, concerning
all things necessary for his own glory, man’s salvation, faith,
and life, is either expressly set down in Scripture, or by good
and necessary consequence may be deduced from Scripture”
(Li).

Of course, there are a number of things thal are not “ex-
pressly set down in Scripture,” but these too “are to be ordered
by the light of nature and Christian prudence, according to the
general rules of the Word, which are always to be observed” (L,vi). But
“just weights and measures” is “expressly set down in Scrip-
ture” as Muether admits (Deut. 25:15). Then how can he
square his view with the Confession and the Bible? He can’t,
and he doesn’t. This would bother me if I were assessing the
legitimacy of the theonomic position in terms of what the critics
say about it.

Are we to argue the pro-life/anti-abortion position in the
same way? Anti-Reconstructionist Meredith G. Kline and dis-
pensationalist H. Wayne House” turn to precise exegetical

87. The insights of unbelievers “are accurate because their presuppositions
concerning the proper ‘givens’ of economic analysis are in fact the same ‘givens’ set
forth by the Scriptures. They are correct, as Van ‘Til says about secular philosophers,
only insofar as they operate in terms of borrowed premises. But these men are to be
preferred in their explanations of how an economy functions to those economists
who borrow even fewer of the Bible’s premises.” North, Jnéreduction te Christian
Economics, p. xi.

38. Meredith G. Kline, “Lex Talionis and the Human Fetus,” fournal of the Bvan-
gelical Theological Society (September 1977), pp. 193-201.

39. H. Wayne House, “Marriage or Premature Birth: Additional Thoughts on

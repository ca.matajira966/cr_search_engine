Life and Liberty: The American Experience 93

Being on the public agenda is not enough in itself to
bring ahout widespread social change, however. Something
more is needed—an incident to galvanize the concern of
the public. In the good providence of God, just such an
incident occurred in New York just days afier St. Clair’s ar-
ticle appeared in the Times.

The body of a beautiful young woman was discovered
inside an abandoned trunk in a railway station baggage
room. A police autopsy determined that the cause of death
was a botched abortion.

The Times provided stark details:

This woman, a full five feet in height, had been
crammed into a trunk two feet six inches long. Seen
even in this position and rigid in death, the young girl,
for she could not have been more than eighteen, had a
face of singular loveliness, But her chief beauty was her
great profusion of golden hair, that hung in heavy folds
over her shoulders, partly shrouding her facc. There was
no mark of violence upon the body, although there was
some discoloration and decomposition about the pelvic
region, Ir was appareni that here was a new victim of
man’s lust, and the life destroying arts of those abortion-
ists, whose practices have lately been exposed.4

For several days, readers were spellbound as the tragic
story began to unfold. They followed the tortured move-
ments of detectives and investigators with horrified fascina-
tion. The chain of events was carefully retraced. The
young girl was identified. Her seducer—who had made
the clandestine arrangements for her fatal surgcery—was

 

revealed. Even the perpetrator of the wretched crime was
tracked down.

When at last the suspect was apprehended—a promi-
nent Fifth Avenue physician named Dr. Rosenzweig—the
entire city was startled once again by St. Clair’s revelations.
During his investigations the previous month, he had acta-
ally interviewed Rosenzweig in the doctor’s office:

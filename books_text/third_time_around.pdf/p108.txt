96 THE SECOND TIME AROUND

Against Nature,” “A Damnable Deed,” “The Curse of Amer-
ican Society,” “The Terrible Sins Which Vanity and Fashion
Lead Their Devotees to Commit,” “A Long Record of In-
famy,” “Child Murder in Massachusetts,” and “The Demon
Doctor.”

Though the medical community was still fragmented,
disorganized, and disconnected, virtually every professional
medical association began to decry any and all forms of
child-killing. The fledgling American Medical Association,
for instance, circulated a gynecological report emphasizing
the necessity of protecting “the safety of the child” at all
times, and denouncing “the perverted views of morality”
underlying abortion.!? It resolved to expel from its ranks
all abortionists—who were but “modern Herods,” “edu-
cated assassins,” and “monsters of iniquity,” who presented
“a hideous view of moral deformity as the cvil spirit could
present.”!3 The organization’s professional periodical, the
Journal of the American Medical Association, published a scath-
ing critique of abortion’s death ethic noting that from the
moment of conception:

The unborn child is human, and at all periods differs in
degree and not in kind from the infant and the adult.
‘Therefore, we must regard it as a human being with an
inalienable right to life, and that iis destruction is homi-
cide.!¢

Even reticent politicians and barristers began to take
notice and to take action.!5 Tougher restrictive legislation,
more efficient local enforcement, and strict sentencing
guidelines were put into place all around the country, and
the prosperous physician-killers were driven to desperate
resources. Eventually every state in the Union passed laws
making their morbid arts illegal. Many went so far as to
affirm that the abortion of “any woman pregnant with child
is an assault with intent to murder.”!6 With penalties rang-
ing from seven years of hard labor in the penitentiary to
life in prison, the crime was clearly and legally vilified.!”

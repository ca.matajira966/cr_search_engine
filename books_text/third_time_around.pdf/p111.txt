Life and Liberty: The American Experience 99

This assembly regards the destruction by parents of their
own offspring, before birth, with abhorrence, as a crime
against God and against nature; and as the frequency of
such murders can no longer be concealed, we hereby
warn those that are guilty of this crime that, except they
repent, they cannot inherit eternal ile. We also exhert
those that have been called to preach the gospel, and all
who love purity and truth, and who would avert the just
judgments of Almighty God from the nation, that they
be no longer silent, or tolerant of these things, but that
they endeavor by all proper means to stay the Moods of
impurity and cruelty.22

Besides the fiery evangelist D. L. Moody, perhaps the
most popular preacher in America immediately following
the war was the Congregationalist John Todd. In 1867, he
added his voice to the groundswell of pro-life sentiment in
an article entitled “Fashionable. Murder.” In an interview
sometime later, he reiterated the importance of the issue:

Murder is, of course, heinous of its own accord. But the
murder of a mother’s own flesh within the womb is a
crime against heaven that is the very essence of sin and
inimicable with the Christian religion. Left alone, such a
crime would sunder the whole fabric of our families, of
our communities, of our churches, of our markets and
industries, and finally of our nation. We have rid our-
selves of the blight of Negro slavery, affirming that no
man may be considered less than any other man. Now
let us apply that holy reason to the present scandal.*®

Summarizing the overall response of the Christian com-
munity to the abortion issue, Arthur Cleveland Cox, the
renowned Episcepal bishop of the diocese of New York,
said that:

Though physicians and journalists have laid much of the
groundwork for the exposure of this awftil crime against
God and man, it was the Christian churches, all of the
churches across the broad spectrum of denomination
and sect, that have brought hope and help to the inno-

 

7

ABOMINATIONS
AND ADMONITIONS:
THE MAKING OF MODERNISM

extinctus amabitur idem *

 

As always happens to miraculous things, the virtue has all gone out
with the lapse of time.

Hilaire Belloc

If the world grows too worldly, it can be rebuked by the church; but if
the church grows too worldly, it cannot be adequately rebuked for
worldliness by the world,

G. K. Chesterton

hen Marx and Engles began their Communist Man-
ifesto with the eloquent sentence, “A spector is
haunting Europe,” they were at least partly right.
A spector was indeed haunting Europe; in fact, it was
haunting the whole earth. But, it was not the spector that
they had imagined. Instead, it was the primal spector of
resurgent abortion, It was the aboriginal spector of child-
killing.

Some battles just don’t stay won. Especially when they
are rooted in the very character of fallen man.

 

dS

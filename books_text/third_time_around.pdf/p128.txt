116 THE THIRD TIME AROUND

‘Theodore Roosevelt, writing on the eve of America’s
entry into the First World War in 1917, exhorted his fellow
citizens to be on guard against that ancient speetor:

The world is at this moment passing through one of
those terrible periods of convulsion when the souls of
men and of nations are tricd as by fire. Woe to the man
or to the nation that at such a time stands as once
Laodicea stood; as the people of ancient Meroz stood,
when they dared not come to the help of the Lord
against. the mighty. In such a crisis the moral weakling is
the enemy of the right, the enemy ol life, liberty, and the
pursuit of happiness.2

That was a remarkable warning from a remarkable
man—a man of unbounded energics and many careers. Be-
fore his fiftieth birthday, Roosevelt had served as a New
York state legislator, the under-secretary of the Navy, police
commissioner for the city of New York, U.S. civil service
Commissioner, the governor of New York, vice-president
under William McKinley, a colonel in the U.S. Army, and
two terms as the president of the United States. In addi-
tion, he had written nearly thirty books, run a cattle ranch
in the Dakota Territories, and conducted scientific expedi-
tions on four continents. He was a brilliant man who read
at least five books every week of his life. He was a physical
man who enjoyed hunting, boxing, and wrestling. He was a
spiritual man who took his faith very seriously and for years
taught Sunday school in his Dutch Reformed Church. And
he was a family man who lovingly raised five children and
enjoyed a lifelong romance with his wife.

He was, indeed, a remarkable man.

And he lived in a remarkable time—at the sunset of
one century and the dawning of another.

The United States had grown from sixteen states in
1800 to forty-five in 1900, from nine hundred thousand
square miles to almost four million, and from a population.
of five million to scyenty-six million. That century had pro-

 

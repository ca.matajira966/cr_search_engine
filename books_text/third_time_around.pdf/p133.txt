Abominations and Admonitions: The Making of Modernism 121

Henry Cabot Lodge and Charles Lindbergh, Sr. valiandy
tried to carry on Progressivism’s tradition—as informed by
Christian conviction, patriotism, and family values. In Eu
rope, once Belloc left his scat in the House of Commons,
Cecil Chesterton and Maurice Baring likewise attempted to
uphold Progressivism’s legacy. In both cases, however, the
bulk of the once mighty movement simply dissipated.

The fact is, among the movement’s disparate factions
and interests, there were a large number of irreverent and
impatient young radicals who were informed by a very dif
ferent ideological standard: the revolutionary philosophies
and ideals of the humanistic enlightenment. They pushed
the bulk of Progressivism into the fringes of Western poli-
tics and culture, establishing dozens of groups intent on
the utter decimation of the Christian vision of life, liberty,
and the pursuit of happiness: the Mugwumps, the Anar-
chists, the Knights of Labor, the Grangers, the Single-Tax-
ers, the Suffragettes, the International Workers of the
World, the Populists, and the Communists. But perhaps the
most dangerous and influential offshoots of Progressiv-
ism’s new humanistic fraginentation was Malihusianism,

Thomas Malthus was a late eighteenth century profes-
sor of political economy whose mathematical theories con-
vinced an entire generation of scientists, intellectuals, and
social reformers that the world was facing an imminent
economic crisis caused by unchecked human fertility. Ac-
cording to his scheme, population grows exponentially
over time, while food production only grows arithmetically.
Thus, poverty, deprivation, and hunger are simply evi-
dences of a population problem, A responsible social policy
will, therefore, address population growth as its most press-
ing priority, In fact, Malthus argued, to attempt to deal
with endemic needs in any other way simply aggravates
things all the further.

Maithus made the dystopic implications of his thinking
crystal clear. In his definitive work, An Essay on the Principle
of Population, first released in 1798, he wrote:

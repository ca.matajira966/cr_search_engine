ian

Abominations and Admonitions: The Making of Modernism 127

The Friends church asserted:

On. religious, moral, and humanitarian grounds, we ar-
rived at the view that it is far betcer to end an unwanted
pregnancy than to encourage the evils resulting from
forced pregnancy and childbirth. We therefore urge the
repeal of all laws limiting either the circumstances under
which a woman may have an abortion or the physician’s
freedom to use his or her best professional judgment in
performing it.5?

The American Baptist churches baptized the Malthus-
rhetoric saying:

We grieve with all who struggle with the difficult circum-
stances that lead them to consider abortion. Recognizing
that cach person is ultimately responsible to God, we en-
courage women and men in these circumstances to seek
spiritual counsel as they prayerfully and conscientiously
consider abortion.

The largest Lutheran communions worldwide reat

firmed the principles of their Nazi collaboration by stating:

In the consideration of induced abortion, the key issue is
the status of the unborn fetus. Since the fetus is the or-
ganic beginning of human life, the termination of its de-
velopment is always a serious matter. Nevertheless, a
qualitative distinction must be made between its claims
and the rights of a responsible person made in God’s
image who is living in relationships with God and other
human beings. This understanding of responsible per-
sonhood is congrucnt with the historical Lutheran teach-
ing and practice whereby only living persons are bap-
tized. On the basis of the evangelical ethic, a woman or
couple may decide responsibly to seek an abortion.*+

Even a large number of prominent evangelical leaders

yielded to the Balak temptation. Meeting under the aus-
pices of Christianity Today magazine and the Christian Medi-

cal

Society, and Ied by the highly esteemed Carl F. H.

Henry, the evangelicals engaged in debate and exchanged

8

RESCUE

THE PERISHING:
THE NEW PRO-LIFE
EMERGENCE

dum vita est spes est’

 

A man who knows that the earth is round but lives among men who
believe it to be flat ought to hammer in his doctrine of the earth's
roundness up to the point of arrest, imprisonment, or even death.
Reality will confirm him, and he is not so much testifying to the
world as it is—which is worth nothing—as to Him who made the
world, and Who is worth more than all things.

Hilaire Belloc

You cannot escape the revelation of the identical by taking refuge in
the illusion of the multiple.

G. K. Chesterton

All hough the story of the pro-life movement is a
story of courage, conviction, and compassion, it is
also the story of commonness. For twenty centu-
ries it has been composed primarily of ordinary people.
People with jobs and families. People with hopes and

  

139

146 THE THIRD TIME AROUND

abortion positions including the single largest evangelical
denomination—and arguably the most influential—the
Southern Baptist Convention.

Before the end of the decade, virtually every evangelical
leader in America had publicly committed himself in some
way to upholding the sanctity of life in both word and
deed, As Chuck Swindoll confessed, “Remaining silent is
no longer an option.”*

Armed with this new sense of accord, and with appar-
ently committed pro-life Presidents in the White House for
over a decade, most Christians seemed certain that the
abortion consensus would at last be rolled back. Sadly, a
series of events would seriously minimize the impact of the
pro-life forces:

« In 1978, serious infighting erupts between the veteran
groups and the various upstarts. Ideological, method-
ological, and theological differences explode into full-
scale animosity.

« In 1980, new conflicts arise between Catholic and Prot-
estant groups.

« In 1982, advocacy of two conflicting pro-life bills on
Capitol Hill—the Hatch and the Helms measures—
cripples any hope of congressional action and exposes
both the disarray and the naiveté of the movement.

+ In 1984, a spate of abortion clinic bombings rocks the
pro-life community.

e In 1987, Planned Parenthood launches a full-scale neg-
ative publications campaign to discredit and close the
more than three thousand alternative crisis pregnancy
centers around the country.

¢ Rescue missions, beginning in New York and Adania in
1988, sweep across the country reinvigorating many,
but once again deeply dividing movement forces and
drawing the ire of recalcitrant church leaders.

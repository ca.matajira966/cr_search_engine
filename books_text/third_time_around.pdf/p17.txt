Introduction: Round and Round 5

Just as a loss of memory in an individual is a psychiatric
detect calling for medical treatment, so too any commu-

nity which has no social memory is suffering from an
3

 

illness.

Lord Acton, the great historian from the previous gen-

eration, made the same point saying:

History must be our deliverer not only from the undue
influence of other times, but from the undue influence
of our own, from the tyranny of the environment and
the pressures of the air we breathe.+

This book—which I admit with chagrined irony had its

genesis on a live television program—is offered as a hum-
ble beginning to what I pray may be an historical correc-

tive. It is off

  

ved so that we may indeed recover from our

present illness and escape our present tyranny.

Deo soti gloria. Jes juva.

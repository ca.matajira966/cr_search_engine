174 THe NEXT TIME AROUND

specter of imminent judgment—nothing must be allowed
to deter or detour us from the need for patience.

Conclusion

Pro-life efforts have been an integral aspect of the work
and ministry of faithful believers since the dawning of the
faith in the first century. Through all the convulsions of
the patristic era, into the upheaval of the medieval epoch,
on toward the Renaissance and Enlightenment, through
the great missions movement and the emergence of Amer-
ica, and into the modern period, the true church has always
stood for the sanctity of all innocent life—in contradistinc-
tion to the pagan consensus for abortion, infanticide, aban-
donment, and euthanasia. Admittedly, there have been
dark days when the institutional church failed to uphold its
covenantal responsibilities, but, thankfully, those days have
been short-lived aberrations.

Whenever believers have successfully defended the help-
less, their efforts have adhered to a predictable pattern—a
covenantal pattern. The elements in that pattern, like the
commitment to life itself, has remained remarkably consis-
tent: an emphasis on the necessity of orthodoxy, the cen-
trality of the church, the indispensability of servanthood,
the importance of decisiveness, and the primacy of pa-
tience.

In the present struggle to uphold our two-thousand-
year-old heritage, it would stand us in good stead to pay
heed to this pattern—this legacy—and to reclaim it.

After all, there is no need to reinvent the wheel. As
historian David R. Carlin has said:

The best way to develop an attitude of responsibility to-
ward the future is to cultivate a sense of responsibility
toward the past. We are born into a world that we didn’t
make, and it is only fair that we should be grateful to
those who did make it. Such gratitude carries with it the
imperative that we preserve and at least slightly improve

In the Valley of the Shadow of Death U1

(the study of men) is the thanatos factor. With entirely non-
Freudian implications, the ‘hanatos syndrome is simply the
natural sinful inclination to death and defilement. All men
have morbidly embraced death (see Romans 5:12).

At the Fall, mankind was suddenly destined for death
(see Jeremiah 15:2). We were all at that moment bound
into a covenant with death (see Isaiah 28:15). Scripture
tells us, “There is a way that seems right to a man, but its
end is the way of death” (Proverbs 14:12; 16:25, NKJV).

Whether we know it or not, we have chosen death (see
Jeremiah 8:3). It has become our shepherd (see Psalm
49:14). Our minds are fixed on it (sec Romans 8:6), our
hearts pursue it (see Proverbs 21:6), and our flesh is ruled
by it (see Romans 8:2). We dance to its cadences (see Prov-
erbs 2:18) and descend to its chambers (see Proverbs 7:27).

The fact is, “the wages of sin is death” (Romans 6:23,
NkJv) and “all have sinned” (Romans 3:23, NKJV).

There is none righteous, not.even one; there is none
who understands, there is none whe seeks tor God; all
have turned aside, together they have become useless;
there is none who does good, there is not even one.
Their throat is an open grave, with their tongues they
keep deceiving, the poison of asps is under their lips;
whose mouth is full of cursing and bitterness; their feet
are swilt to shed blood, destruction and misery are in
their paths, and the path of peace have they not known.
There is no fear of God before their eyes. (Romans
3:10-18)

And, all those who hate God, love death, (Proverbs 8:36)

It is no wonder then that abortion, infanticide, expo-
sure, and abandonment have always been a normal and
natural part of human relations. Since the dawning of
time, men have contrived ingenious diversions to satisfy
their fallen passions. And child-killing has always been
chief among them.

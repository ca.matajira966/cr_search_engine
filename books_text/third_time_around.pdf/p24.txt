aos ‘THE FIRsT TIME AROUND

Virtually every culture in antiquity was stained with the
blood of innocent children. Unwanted infants in ancient
Rome were abandoned outside the city walls to die from
exposure to the clements or from the attacks of wild forag-
ing beasts. Greeks often gave their pregnant women harsh
doses of herbal or medicinal abortifacients. Persians devel-
oped highly sophisticated surgical curctte procedures. Chi-
nese women tied heavy ropes around their waists so excru-
ciatingly tight that they either aborted or passed into
unconsciousness. Ancient Hindus and Arabs concocted
chemical pessaries—abortifacients that were pushed or
pumped directly into the womb through the birth canal.
Primitive Canaanites threw their children onto great flam-
ing pyres as a sacrifice to their god Molech. Polynesians
subjected their pregnant women to oncrous tortures—their
abdomens beaten with large stones or hot coals heaped
upon their bodies. Japanese women straddled boiling caul-
drons of parricidal brews. Egyptians disposed of their un-
wanted children by disemboweling and dismembering
them shortly after birth—their collagen was then ritually
harvested for the manufacture of cosmetic creams.

None of the great minds of the ancient world—from
Plato and Aristotle to Seneca and Quintilian, from Pythago-
ras and Aristophanes to Livy and Cicero, from Herodotus
and Thucydides to Plutarch and Euripides—disparaged
child-killing in any way. In fact, most of them actually rec-
ommended it. They callously discussed its various methods
and procedures. They casually debated its sundry legal ram-
ifications, They blithely tossed lives like dicc.

Abortion, infanticide, exposure, and abandonment
were so much a part of human societies that they provided
the primary lie’ motif in popular traditions, stories, myths,
fables, and legends.

The founding of Rome was, for instance, presumed to
be the happy result of the abandonment of children. Ac-
cording to the story, a'vestal virgin who had been raped

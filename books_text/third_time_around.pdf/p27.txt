In the Valley of the Shadow of Death 1s
those two ways—to choose between fruitful and teeming
life on the one hand, and barren and impoverished death
on the other (see Deuteronomy 30:19).

Apart from Christ it is not possible to escape the snares
of sin and death (see Colossians 2:13). On the other hand,
“If any man be in Christ, he is a new creation; old things
have passed away; behold, all things have become new”
(2 Corinthians 5:17).

All those who hate Christ “love death” (Proverbs 8:36);
while all those who receive Christ are made the sweet savor
of life (see 2 Corinthians 2:16).

The implication is clear: The pro-life movement and.
the Christian faith are synonymous. Where there is onc,
there will be the other—for one cannot be had without the
other.

Further, the primary conflict in temporal history always
has been aud always will be the struggle for life by the
church against the natural inclinations of all men every-
where.

Conclusion

Death has cast its dark shadow across the whole of human
relations. Because of sin, all men flirt and flaunt shame-
lessly in the face of its spector. Sadly, such impudence has
lcd to the most grotesque concupiscence imaginable: the
slaughter of innocent children. Blinded by the glare from
the nefarious and insidious angel of light (see 2 Corinthi-
ans 11:14), we stand by, paralyzed and mesmerized.

Thanks be to God, there is a way of escape from these
bonds of destruction. In Christ, there is hope. In Him
there is life, both temporal and eternal. In Him there is
liberty and justice. In Him there is an antidote to the
thanatos factor. In Him, and in Him alone, there is an an-
swer to the age-long dilemma of the dominion of death.

2

HOW FIRM
A FOUNDATION:
THE APOSTOLIC CHURCH

a maximis ad minima '

 

Faith begins to make one abandon the old way of judging. Averages
and movements and the rest grow uncertain. The very nature of
social force seems changed to us. And this is hard when a man has
loved common views.

Hilaire Belloc

Faith is always at a disadvantage; it is a penpetually defeated thing
which sureives all conquerors.

G. K. Ghesterton

t was into a family of great wealth and distinction
that Basil of Caesarea was born midway through
the fourth century. Of all the great dynasties that
mankind has seen emerge out of the sea of convention and
commonality, perhaps none has left as permanent an im-

 

press on the course of history as that of the Cappadocian
Valenzias.2 The Habsburgs, the Romanovs, and the Medicis
were prodigious in their accomplishments. The Stuarts, the
Bourbons, and the Mings were remarkable for their im-
pact. The Warburgs, the Rothschilds, and the Carlyles gave

I7

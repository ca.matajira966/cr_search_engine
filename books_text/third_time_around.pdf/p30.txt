18 ‘THE First TIME AROUND

shape to one gencration after another. But, the Valenzias
outshone them all.

Basil’s grandmother, Macri of Lyyra; his father, Battia
of Cappadocia; his mother, Emmelia of Athens; his sister,
Macrina of Pontus; and his two younger brothers, Gregory
of Nyssa and Peter of Sebastea, were all numbered among
the early. saints of the church. One of his other brothers,
Paulus of Ry, became Chief Counsel to the Emperor and
still another, Stephen of Alexandria, became the Imperial
Governor of Byzantium’s largest colonial region. Three of
his descendents eventually bore the Imperial Scepter and
two others ascended to the ecumenical throne. For more
than a thousand years some member of this remarkable
family held a high position in Constantinople’s corridors of
power and influence.

But as great as his family was, Basil was himself greater
sul.

Renowned for his encyclopedic Jearning, he studied in
all the great schools of his day—in Caesarea, Constan-
tinople, Athens, and Rome. For a short time he practiced
law with an eye toward a public career, but he determined
to heed a call into the ministry instead. He helped to estab-
lish a Christian community in Annesi where he distin-
guished himself as a man of extraordinary charity and bril-
liance. It was not long before his reputation reached the
farthest edges of the Empire: He was a quick-witted adver-
sary to the heretical’ Arians; he was a valiant defender of
orthodoxy, he had a productive theological pen; and he
was a man who combined a deep and sincere piety with a
tough and realistic practicality. In short order, he was
called to the very prominent parish ministry of Caesarea.

Diligent to perform his pastoral duties, Basil soon
found himself overwhelmed with busyness. He led eighteen
services every week—except just before Christmas and Eas-
ter when there were more—in addition to his work of cate-
chizing the young, visiting the sick, and encouraging the
distressed. We also kept up a heavy correspondence and

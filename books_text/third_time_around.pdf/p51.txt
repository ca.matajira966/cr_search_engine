A New Consensus: The Medieval Church ao

couraging the faithful to care for the unwanted and to give
relief to the distressed:

Concerning the care of unwanted and abandoned chil
dren spared the murderous designs of the abortionist by
legal restraints but still subject more to the wiles of dogs
in the street than to the sops of mercy in the home: it
has scemed best to make expeditious provision for adop-
tion—even as heaven made for us. Anyone who finds an
unwanted child should immediately notify the church
from whence the pastor shall announce the same from
the altar. If after this very careful disposition, anyone
who exposed the child maliciously shall be discovered,
he is to be punished with the civil and ceclesiastical sanc-
tions for murder?

About a decade later, another council met at Arles
where the Vaison edict was repeated, provision by provi-
sion, but with one important addition:

An heritage is by no means Lo be scorned or spurned,
children being the greatest heritage of all. Therefore,
any and all means must be effected to safeguard their
well-being—from their quickening in the womb to their
assumption of powers.®

Tn the tenth century, a council met at Rouen which not
only encouraged Christians to provide positive alternatives
to crisis pregnancy situations, but it also enjoined them to
guard irmocent life with their own lives:

The eternal rewards to bencfactors of the helpless are
manifold, but the blood-guiltiness of the stiffnecked will
be published abroad. Forbear not to deliver them that
are drawn away unto death and them which are ready to
be staughtered.?

In the eleventh century, when Burchard of Worms
compiled an encyclopedic collection of ecclesiastical can-
ons, he found only consensus and concurrence with Vai-
son, Arles, and Rouen:

Castles in the Air: The Renaissance and the Enlightenment 6d

nium of medievalism, fallen cultures—like fallen men—de-
scend the downgrade whenever the basic principles of the
Christian life as revealed in the Scriptures are neglected,
ignored, or usurped.

Such was the case with the Renaissance and the En-
lightenment. Because that era was marked by a néw appre-
ciation for the philosophies of pagan antiquity, the culture
soon reverted to the morals of pagan antiquity, including
the desecration of life.

Once again, however, Christians responded to the ur-
gent need to protect life. They mobilized efforts on behalf
of women in crisis situations, they protected the innocent,
and they centered their work in the redemption of Christ.
In short, as they revived and renewed the church, they re-
vived and renewed the pro-life movement as well—a move-
ment as old as the gospel itself, but as fresh and new as the
dew of the dawn. As before, this revival was rooted in five
basic principles:

Orthodoxy

The church was only effective in its task of protecting inno-
cent life when it remained steadfast in doctrinal purity and
Scriptural fidelity, Whenever it began to slide into the com-
fortable heresies of the day, it became compromised and
impotent. But whenever it would “earnestly contend for the
faith once and for all delivered to the saints” (Jude 3), God
blessed its efforts gloriously. Whencver and wherever it
took its cvery cuc from the dictates of the sovereign God, it
was remarkably successful.

The Church

As the old pagan practices began to creep back into socie-
ties during the Renaissance and the Enlighténment, only
the sacramental church was able to withstand their subtle
profanities. Only the church, with its ingenious balance be-
tween hierarchy and egality, was able to manifest an au-

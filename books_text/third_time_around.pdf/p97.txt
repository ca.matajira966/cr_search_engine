Yo the Uttermost: The Great Missions Movement 8&5

helped the once-murderous king frame Christian laws
prohibiting abortion, infanticide, abandonment, and
cuthanasia.

In 1869, Friedrich Ramseyer and his wife Victoria, went to
be missionaries at Kumassi on the west coast of Africa.
Because their preaching was perceived to be a threat to
the Narideggi chicfiain, Prempch and his hoard of
witchdoctors, the couple was kidnapped and held for
four years. Finally rescued in 1874, the Ramseyers left
Africa to recover in England. For twenty-two years, the
Ramseyers prayed that God would provide a way for
them to return. Meanwhile, the denizens of Kumassi
groaned under the oppression of the bloodthirsty king.
Prempeh made a practice of killing a slave cach night
following a lavish feast, solely for his own perverse en-
lertainment. Once a year during a festival of yams, be
immolaied six hundred of his subjects in a grisly cele-
bration. He had even gone so far as to slaughter four
hundred virgins in order to give the walls of his palace
a rich red color by mixing their innocent blood with
the mortar. Yielding to the Ramseyers’ incessant lobby-
ing and cajoling, the British returned to the region in

 

1895, They immediately deposed the tyrant-king. Soon
after, the residents of Kumassi welcomed the missionar-
ics back as long-departed friends, where they worked
for another twelve years restructuring the Narideggi so-
ciety and developing an indigenous law code that
would, at long last, fully respect the sanctity of life.

“Expect great things from God, attempt great things for
God” was the famous principle of action first enunci-
ated by the pionecr English missionary, William Carey.
Serving as both village pastor and cobbler, in 1792 he
wrote An Inquiry into the Obligations of Christians to Use
Means for the Conversion of the Heathens as a response to
the popular pseudo-Calvinism of the day—which held
that God would convert the lost when He wished and
that nothing men did could possibly alter His timing.
Thai book became the catalyst for a number of new
evangelistic endeavors. When the Baptist Missionary So-

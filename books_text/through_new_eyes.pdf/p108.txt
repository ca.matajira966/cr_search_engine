96 Turoucu New Eves

Under the instigation of Satan, part of the animal kingdom
rose up against human dominion. The serpent’s seduction of
Eve and Adam in Genesis 3 is the story that tells of this. After
the fall of man, all animals have the potential of becoming man’s
enemy. Even the peaceful ox, if it rises up and gores a man, is
counted an unclean animal (Exodus 21;28-29).! Thus, it would
be necessary for the Seed of the woman to exert dominion over
the rebellious animals, by crushing their head, their leader
(Genesis 3:15).

A marvelous picture of this is given in Daniel 7, where the
Son of Man, the Seed of the woman, takes dominion over the
four beasts of the sea. The sea represents the Gentiles, and the
four beasts represent the idolatrous nations of the world: the
lion-eagle is Babylon; the bear is Persia; the leopard is Alex-
ander’s Greece, and the nightmare beast is Rome. The Son of
Man, however, takes dominion over them all, subduing Satan’s
beasts permanently. Just so, Mark tells us that Jesus “was in the
wilderness forty days being tempted by Satan, and He was with
the wild beasts . . .” (Mark 1:13a).2 With these remarks we have
arrived at a discussion of the symbolism of animals, and so to
that subject we must now turn.®

Animals as Symbols

The Bible presumes.an analogy between men and animals
from the beginning. Animals image human life more closely
than do any other of the other aspects of the creation. This is
especially true of land animals, which were made on the same
day as man. In Genesis 2, we find that God brought animals to
Adam to name, or describe. Adam gave names to them, and in
the process noticed that all the animals came in sexual pairs.
He might have reasoned from this that he was simply different
from the animals in this regard. Instead, however, he rightly
observed that if animals had mates, he should also (Genesis
2:18-20). This reasoning could only take place on the basis of a
perceived analogy.

Let us now briefly overview the ways in which the Bible sets
forth animals as imaging human life. Elijah Schochet has pro-
vided a fine introduction to our present considerations, and his
remarks are worth reprinting in full.

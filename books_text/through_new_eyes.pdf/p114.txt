102 Turouci New Eves

1:2; 119:103; Jeremiah 15:16). To be clean, an animal must both
chew cud and wear proper shoes (hooves that are split) perhaps
so that the animal can “distinguish” between the things with
which he comes in contact, and so that he can travel on high
places: the holy mountain (Leviticus 11:2-8, 27; Psalm 18:33).
Fish must also be shod. In their case, it means having scales.
Scales are like armor that keeps the fish from contact with his cn-
vironment (cp. 1 Samuel 17:5). The clean fish must-also have
fins, enabling him to make purposeful movement through the
water. The man of God, symbolized by the clean fish, does not
drift with the tide (Leviticus 11:9-12),

Clean birds are those that are careful and particular about
where they land--where they put their feet (Genesis 8:9).
Unclean birds will land on anything, especially on rotting car-
casses (Leviticus 11:13-23),

Finally, animals that swarm around in the dust and that in-
vade homes are unclean. They attack the woman's domestic en-
vironment, spreading death to her kitchen utensils, The woman
is at enmity with them (Genesis 3:15; Leviticus 11:29-38).

All unclean animals resemble the Serpent in the Garden.
They are boundary transgressors who break into the domestic
garden and bring death. ‘hey crawl in the dust. They eat dust.
In these ways they image the life of the Serpent—but in only
these ways. The unclean lion, as we have seen, is also a noble
and mighty beast.

In the New Covenant, of course, this distinction is removed
(Mark 7:19; Acts 10-11). Christ has cleansed the world, once and
for all.15 We can go barefoot in the dirt, and wear shves to
church. We can eat the flesh of any animal we desire. We don’t
need to break kitchen utensils when we find a dead lizard or
mouse lying on them (Leviticus 11:32-35).

Conclusion
As we look at the world through new eyes, we must be care-
ful not to bring the clean/unclean distinction into play, since we
now live in the New Covenant. All the same, the meaning of
that distinction is still relevant. We need to be careful to walk in
the ways of righteousness and meditate on God’s word. We need
to avoid the environment of the serpent.

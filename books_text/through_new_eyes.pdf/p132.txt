120 Trroucu New Eyres

gave commands to fish, birds, beasts, and men, These were to
be the rulers of the world, and they were under His orders.
Thus, God’s Word is always simultaneously both promise and
command, both the grant of His Kingdom and rules to obey
concerning it (cf. Genesis 1:28-30).

Notice that the order is gift and then rules, promise and then
law. God gives the Kingdom, and then gives us rules to live by.
The order is never Law and then Gospel. God’s Word comes to
us first as a Tree of Life, giving us grace, and then afterwards as
a Tree of Knowledge of Good and Evil, giving us rules.

Fourth, God evaluated His work, This is noted in the text
where it says, “God saw what He had made and it was good,”
and in 1:31: “God saw all that He had made and it was very
good.” Initial evaluation is preliminary to consumption or full
enjoyment. Before eating there is tasting, Thus, when mother
makes a soup and distributes a bowl to each member of the fam-
ily, the first taste elicits an evaluation. “Well, how do you like it?”
That question comes not at the end of the meal, but after the
first taste or so.

Fifth, God enjoyed His work. God’s sabbath rest on the sev-
enth day was not apart from the creation, but in it. God’s temple,
where He rested enthroned, was always set up in the world—
for instance, in the midst of the Israelite camp, or in the center of
the land. Having tasted His work and found it good, God re-
laxed and enjoyed it. Similarly, if the soup is good, we enjoy a
whole bowl of it, and maybe a second.

These five simple actions are very ordinary, and are inesca-
pable. It is, or should be, encouraging and invigorating to real-
ize that the imaging of God is not focally the performance of
great, heroic acts, but the carrying out of very ordinary activi-
ties. For instance, for me to give you a glass of water means:

1. I take hold of a glass in the cabinet, and take hold of the
faucet.

2. I restructure the cabinet by removing the glass. Just as
God separated the waters from the waters by putting “fir-
mament” between them, so I separate one glass from the
rest, putting space between them, Also, I separate walter
from the pipe into the glass, dividing water from water. I

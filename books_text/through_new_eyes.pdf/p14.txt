2 "TrroucH New Eves

1:14), because “the heavens declare the glory of God, and the
firmament shows His handiwork” (Psalm 19). While the Biblical
perspective does not invalidate telescopic investigation of the
starry heaven, could it be that.we are not seeing all we should see
when we look at the stars? Do we need new eyes?

A philosophy of history is extremely important for man, espe-
cially today after Hegel and Marx, Toynbee and Spengler. When
we go to the Bible for a philosophy of history, however, we encoun-
ter covenant renewals, sabbaths, festivals, Jubilee years, and the
Day of the Lord. If we intend to apply the Bible to this modern
problem, we shall first have to acquire the Biblical perspective.

Geography for us is a study of maps, some marked with con-
tour lines; others with natural resources; others with political di-
visions. While the Bible is obviously not ignorant of these things,
the Biblical worldview speaks of such things as the “four corners”
of the earth, and of “holy ground.”

Modern science assumes that the world is governed by im-
personal natural forces, such as gravity, coriolis, and electro-
magnetism. Such forces explain the actions of winds and waves.
In the Bible, however, trust in such natural forces is called “Baal-
ism.” The Bible encourages us to see God and His angels at work
in the winds and waves. Is this mere poetry, or does it give us a
perspective badly needed in our modern world?

We wrestle with the problems of church and state, but the Bible
gives us priests and kings. It gives us the relationship between
seers and judges, and between prophets and monarchs. It gives
us blood avengers and kinsman redeemers. It gives us kingly
palaces and priestly sanctuaries, Are we familiar enough with the
Biblical worldview to apply these categories to modern concerns?

We are concerned about law, and we distinguish between.
state law and church law. We speak of “moral” law, of “criminal”
law, of “civil? law, of “canon” law. And when we turn to the
Mosaic Law, we expect to find these categories, but we don’t. We
find what look like “moral, civil, and ceremonial laws” all mixed
up together, In fact, we find that the Mosaic Law is not law at
all, in the modern sense, but Torah, something radically different
and more profound. And what should we do with this Mosaic
Torah? Should we try to apply it to modern circumstances, or
should we ignore it?

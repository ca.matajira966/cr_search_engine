Man: The Agent of Transformation 139

Amos 3:7 is worthy of citation on this point: “Surely the Lord
God does nothing unless He reveals His secret counsel to His
servants the prophets.”

Understanding that the prophet is someone God consults
with shows us why Adam had no “prophetic task” in the Garden.
He had not yet matured to the point of being made a Council
member, He was a priestly guard and a kingly shepherd, but not
yet a prophetic counselor. I believe that Adam would have be-
come a prophet by eating of the Tree of Knowledge.!°

Certain Old Testament saints stand as striking examples of
Council member/prophet. One is Abraham. When God was
about to destroy Sodom, He asked Himself, “Shall I hide from
Abraham what I am about to do?” (Genesis 18:17). God pro-
ceeded to tell Abraham His plans, and asked Abraham what he
thought. The remainder of the story is familiar to everyone.
Abraham gave his advice and counsel to God, though always in
a deferential manner, respecting the hierarchy. It is in the light of
this that we can understand Genesis 20:7, where God told
Abimelech regarding Abraham: “Now therefore restore the
man’s wife, for he is a prophet, and he will pray for you, and you
will live.” As a Council member, Abraham the prophet could
bring petitions before the Council.

The second great example is Moses, who is the exemplary
prophet of the Old Covenant (Numbers 12:6-8), and the greatest
before John the Forerunner. Moses not only received informa-
tion from the Council and passed its decisions on to the people,
as he ascended and descended Mount Sinai, he also actively
argued before the Council when he felt it necessary, even
“changing God’s mind” on occasion (Exodus 32:7-14, 30-35;
Numbers 14:13-19).

This is not the common idea of what a prophet was. There is
no ranting and raving here. Nor is the prophet simply someone
soberly possessed by God so as to become His mouthpiece.
Rather, it is as. a Council member that the prophet announces
the Council’s decisions to the people.

Vandervelde sums it up:

[The prophets] are not only privy to the divine council (1 Kings
22:19-23; Isaiah 6:1-5), they are participants in God’s plans.
When God announces judgment, the prophet is not afraid to

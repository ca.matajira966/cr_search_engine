Eden: The World of Transformation 155

land, and finally rebuilding the Garden-Temple under Haggai
and Zechariah, Joshua and Zerubbabel (Haggai 1-2; Ezra 1-5).

Of course, regardless of where they lived symbolically, men
always had access to God in heaven. Thus Noah worshipped
God under the open sky, as did converted Gentiles of all ages.
Abraham worshipped God in the land, as did all devout Jews.
After Moses, the Israelites worshipped God in the courtyard of
the sanctuary, the garden. Only the Aaronic priests, however,
were permitted to worship God in the Tabernacle and Temple.
All of this shows that the fullness of access to God was restricted
until the coming of the Messiah.

In the New Covenant, men have immediate and full access
to God in heaven. There are no longer any symbolic restrictions
(Hebrews 7-10). Nonetheless, in the way of cultural movement,
we find that when Christians first penetrate a pagan culture,
they have to meet in homes and even catacombs. When the cul-
ture has been permeated by Christian influence, and becomes a
Christian homeland, then the great and beautiful Garden-
Charches (cathedrals).can be built. So it was with Rome. So it
was with Europe. So it must be in our day.

Our cathedrals have been defiled, and our homes are under
assault as officials of the secular humanist government seek to
close down Christian schools and invade Christian homes.
Thus, ours is not a day of cathedral-building, but a day of cul-
tural permeation. Faithfulness must come first, and only then
will glory come.

High Ground

Rivers flow downhill, which means that the Garden out of
which they flowed was on high ground. Not the highest, how-
ever, because the river arose in the homeland of Eden, which
means that the Garden was lower than other parts of Eden, This
is, perhaps, not what we should expect; but it is reiterated in
Psalm 125;2, “as the mountains surround Jcrusalcm, so the Lord
surrounds His people,” which draws on the fact that the moun-
tains surrounding Zion were actually taller than she. Indeed,
Mount Moriah, where the Temple was built, was lower than
Zion proper, where the city of Jerusalem was built. In fact, as
the waters in the Garden of Eden flowed from the Land of Eden,

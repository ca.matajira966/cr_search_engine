172 TuHroucH New Eves

Kline goes on to note that the three stories of the Ark corre-
spond with the heavens above, the earth beneath, and the waters
under the earth. The lowest deck may be associated with the
crawling things, for they burrow in the earth to some extent; or
it may be that the lowest deck is associated with the water “under
the earth” simply because it was submerged the deepest. As we
have seen, there is a rough correlation between the three land
environments and the three spatial environments, so that crawl-
ing things are parallel to sea creatures (and birds to domestic an-
imals, with wild animals parallel to land animals in general).
Kline writes clearly,

the window of the ark is the counterpart to “the window of
heaven,” referred to in this narrative (7:11; 8:2). Appropriately,
the window area is located along the top of the ark, as part of
the upper (heavenly) story. One is naturally led then to com-
pare the door of the ark with the door that shuts up the depths
of the sea, holding back its. proud waves. (For this cosmological
imagery see Job 38:8-11.)°

Even though God did not tell Noah to put the birds in the
top story, beasts in the middle, and creeping things in the lowest;
yet every time the animals are mentioned they are listed broken
down into categories, creating an in-context conceptual parallel
(Genesis 6:7, 20; 7:8, 14, 21, 23; 8:17, 19; 9:2).?

Another aspect of the Ark as world model is the fact that God
dictated its dimensions to Noah, something only done when a
world model is being set up (Exodus 25ff.; 1 Chronicles 28:16;
Hebrews 9:5; Ezekiel 40ff.; Revelation 21:10ff.). Thus, the Ark
was a world model, and as such has always been regarded as a
type of the Church, God’s new creation.®

Additionally, the Ark was made of wood. What else would it
have been made of? God’s command to make it of wood seems
unnecessary and superfluous, except for the spiritual associa-
tions of wood with trees. In Chapter 7 we noted that the various
periods of history are associated with various trees. The “gopher”
wood of the Ark is peculiar to it. In fact, no one knows what
“gopher” wood is! Like the Tabernacle and Temple, the Ark of
wood was a picture of God’s Edenic Grove.

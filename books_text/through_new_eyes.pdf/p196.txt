184 Trroucu New Eyes

ment in Christ’s abandonment of Jerusalem, and thus of the
whole old world.
The sequence of events in the exodus is this:

1. Some threat, some aspect of sin or of the curse, drives God's people
from their home. Adam was driven from paradise. Famines
drove Abram to Egypt (Genesis 12:10), Isaac to Philistia
(Genesis 26:1), the Hebrews to Egypt (Genesis 43:1), The
disaster at Sodom drove Abraham to Philistia (Genesis
19:28; Genesis 20). Wicked oppressors drove Jacob to
Mesopotamia (Genesis 27:42-43) and David to Philistia (1
Samuel 20:31). Personal sin put Lot in Sodom (Genesis
13:7-13). Conquest removed the Ark to Philistia (1 Samuel
4) and Israel to Babylon (2 Kings 24-25). Love for His
people caused our Lord to leave Heaven to save us.

2. During the sojourn in captivity, Eve is assaulted by the Serpent, who
wishes to use her to raise up his own wicked seed, There was
intermarriage before the Flood (Genesis 6:2). Pharaoh and
Abimelech attacked Sarah (Genesis 12:13; 20:2). Lot's
daughters were corrupted (Genesis 19:30-38). Abimelech’s
people threatened Rebekah (Genesis 26:10). Laban disin-
herited Rachel and Leah (Genesis 31:14-16). Pharaoh killed
the boy babies and kept the girls for his people (Exodus
1:15-22)}, Amalek attacked David’s wives in the wilderness
(i Samuel 30:5). Esther was taken by Ahasuerus during
the exile (Esther 2). Demons ravaged Israel during the
ministry of our Lord. The bride of Christ was assaulted
continually by the Jews in the book of Acts.

3. The righteous use “holy deception” to trick the. serpent and protect
Eve. The serpent had deceived Eve in the beginning (1
Timothy 2:14); and eye for eye, tooth for tooth, it becomes
the woman’s trick to deceive the serpent. Thus, Abraham
called Sarah his sister on two occasions; and Isaac called
Rebekeh his sister, because they knew that an honest ruler
would not simply seize their women without negotiating
with them.*® (Of course, the tyrants seized them anyway.)
Jacob tricked Laban to recover his wives’ dowries (Genesis
30:37-43). The Hebrew midwives lied to Pharaoh and saved
Israelite boys, and were blessed by God for doing so (Exodus
148-21), David feigned madness in Philistia, and pretended
to serve the Philistines (1 Samuel 21:13; 1 Samuel 27, 29),

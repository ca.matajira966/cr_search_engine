198 ‘TaroucH New Eves

Just as the social polity was forced to change, so was the sym-
bolic polity. Living in cities during the period of slavery (Exodus
1:11), the Hebrews were not able to establish worship-oases. As a
result, they began to worship at special tents set aside for the
purpose. There is a clear reference to a special tent for God, a
“tent of meeting,” in Exodus 33:7-11. This passage cannot be re-
ferring to the Tabernacle because it had not yet been built.

These two changes in social and symbolic polity anticipated
the new covenant that was to come, though in only a very rough
way. As the Hebrews dreamed of freedom, they doubtless envi-
sioned a return to the garden-oases of their fathers. God had
something else in mind, something far more glorious, something
they could not have envisioned. God would organize them as a
nation around elders and judges, who at last would be able to
serve as true magistrates. God would set up a symbolic polity in
the form of a glorious tent of gold and precious tapestries.

Thus, God laid hold on the Hebrews, and broke them down in
the fires of His refinement (Exodus 3:2,7). He then restructured
them into a nation, giving them a new name (Israel instead of
Hebrew), and revealing a new name for Himself (the Lord, the
One who keeps the promises made to the fathers, Exodus 6:3-8).

The Mosaic Law

The Mosaic establishment, since it entailed a change in
priesthood, also entailed a change in law (Hebrews 7:12). The
sacramental law of the patriarchal establishment was cireumci-
sion. The center of the Mosaic sacramental law was Passover,
though circumcision continued. Stemming from the Passover
were all kinds of other sacramental laws.?

The center of the Mosaic social law was the Ten Command-
ments. Stemming from the Ten Commandments were all kinds
of other social laws. One major change in social law instituted
with Moses was an expansion of the laws of incest. Formerly,
only cross-generational incest had been forbidden (Genesis
2:24; 19:30-38). Abraham had married his sister, just as Cain
and Seth obviously had married theirs (Genesis 20:12). Jacob
had married two sisters (Genesis 29:18-30). Now both brother-
sister marriages and marrying two sisters were forbidden (Lev-
iticus 18:6-18).

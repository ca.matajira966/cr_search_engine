The World of the Tabernacle 213

world model that transferred itself to the Tabernacle. When the
people left Mount Sinai, they took the Mountain with them.”

God's cloud covered the top of the mountain, thus establish-
ing it as a Most Holy Place. Moses and Moses alone was allowed
to enter this place, just as later on only the High Priest would be
allowed to enter the Most Holy of the Tabernacle (Exodus
19:19-24). At the top of the mountain God gave the Ten Com-
mandments, which were later put in the Most Holy Place of the
Tabernacle.

Midway down the mountain was the Holy Place. Only the
elders of Israel were allowed to go into this area, and there they
ate a meal with God. These elders were the “sun, moon, and
stars” of the nation, and correlate with the lampstand. The meal
they ate correlates with the table of shewbread. The elders them-
selves correlate with the Aaronic priests, who alone might enter
the Tabernacle Holy Place. While the elders ate, “they saw the
God of Israel; and under His feet there appeared to be a pave-
ment of sapphire, as clear as the sky itself” (Exodus 24:10). The
blue sapphire pavement is equivalent to the veil that separated
the Holy Place from the Most Holy.

The courtyard of the mountain was marked off with a
boundary, and anyone who trespassed was put to death (Exodus
19:12}. Inside this boundary was placed an altar, and only certain
select young men might approach it (Exodus 24:4, 5; cp. 19:22,
24). The priests at this point were the firstborn sons, who had
been saved by God at Passover. When they fell into sin at the
golden calf, they were replaced by the Levites and the sons of
Aaron, who thereafter took care of the sacrifices at the altar (Ex-
odus 32:28-29; Numbers 8:14-18). The boundary around the
mountain correlates to the boundary inside the courtyard that
kept the people from approaching the altar.)

In this way, then, the Tabernacle (and later the Temple) were
models of the ladder to heaven, of the holy mountain. Israel did
not need to go back to Mount Sinai, or regard it as anything spe-
cial, after the Tabernacle was built. The Tabernacle was God’s
portable mountain.

The Tabernacle as Symbol of the Body Politic
More than this, however, the Tabernacle symbolized hu-
manity as God’s true environment. The Tabernacle was a sym-
bol of the Israelite body politic (1 Corinthians 3:16). For this rea-

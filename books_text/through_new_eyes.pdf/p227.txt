The World of the Tabernacle 215

The three zones of the Tabernacle complex symbolized three
groups of people. If the High Priest sinned, blood had to be
sprinkled on the Ark in the Most Holy Place (Leviticus 16:11-14).
Ifa regular priest sinned, blood was sprinkled on the veil and on
the golden altar in the Holy Place. Thus, the Holy Place furni-
ture was closely associated with the priests as the “heavens” of
Israel. If the congregation as a whole sinned, this also defiled the
Holy Place, which meant that the congregation as a whole was a
“heavenly people” to the nations of the world (Leviticus 4:1-21).
Ifa citizen sinned, blood'was put on the courtyard altar, and this
was also the case if a civil leader sinned (Leviticus 4:22-35).
Now, all of the above were for “unintentional” sins, Once a year,
atonement was made for the “high-handed” sins of the congrega-
tion; and on this occasion, blood was put on the Ark in the Most
Holy Place (Leviticus 16:15-16).

What emerges from this is a series of associations, a societal
worldview. The congregation is associated with the highest heav-
ens, with the firmament-heavens, and with the courtyard. They
are both stars and dust, both heavens and earth, both cherubic
veil and altar of earth. The High Priest, as supreme spiritual
ruler, is associated with the Most Holy; the lesser priests, with
the Holy Place; and. civil leaders, with the courtyard. The priests
rule in “heavenly things” and the leaders rule in “earthly things.”

Once we. understand that the Tabernacle was a symbol of
Israelite society, there are-all kinds of correlations that can be
made. The Inner Veil of the Most Holy has to do with God’s
angelic guardians. The Outer Veil of the Holy Place has to do
with the priestly guardians. The goat’s hair tent curtain over the
Tabernacle has to do with the courtyard and the Levites. The
red ramskin caver that was on top‘of the goat’s hair tent curtain
is to be associated with Passover, and thus with all Israel, who
were claimed at Passover. The dolphin leather cover has to do
with the Gentiles—dolphin being a sea creature.?5 Interesting as
it would be to go on with this, looking at the gold and bronze
utensils and other features of the Tabernacle complex, we must
move on, We shall consider the altar as a symbol of the body pol-
itic in more detail in Chapter 16. The point that has been estab-
lished and ilhustrated is that the Tabernacle complex symbolized

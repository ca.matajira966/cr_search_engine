Interpreting the World Design il

the light day, and the darkness He called night” (Genesis 1:5, em-
phasis added). Day is “lighttime.” That is its fundamental mean-
ing. The use of the word “day” for. the whole twenty-four-hour
period (see the second half of verse 5) is an extended meaning,
and shows that day (light) is the more basic component of the
period. The (twenty-four-hour) day begins in dark, and: moves
(eschatologically) to light (day), Moreover, the use of “day” for
an age or eon of time is also an extended meaning.

Some have argued that the first three days might have been
longer than twenty-four hours, since the sun was not made to
measure days until the fourth day. This, however, puts the cart
before the horse, The day as a period of time already existed,
and the sun was made to fit it. The book of Revelation shows us
that even after the sun is gone, the daylight of God’s glory will
continue to shine (Revelation 22:5).

Another alternative is the “framework hypothesis.” Some
have called attention to the structure of the six days, as six panels
in a larger picture. They argue that the days are not spans of
time, but only a literary convention for presenting a six-fold cre-
ation, The fundamental problem with this view is that it need-
lessly opposes.a theological interpretation to a literal one. The
observations about the interrelationships among the six panels
or days are valid, but that does not change the fact that the
Bible presents the events as taking place over the course of a
normal week. If we allow this kind of interpretive procedure, we
can get into real trouble: Shall we deny the physical resurrection
of Christ just because we have come to understand its theological
meaning? We dare not pit the historical aspect against the theo-
logical aspect.

Moreover, and to me this is the important point, the theolog-
ical dimension of creation in six days lies precisely in its being a
temporal sequence, as we shall see in Chapter 10 of this book.
God had no reason to make the world in six days, except as a
pattern for His image, man, to follow. Where the Bible later uses
a three-day, or six-day, or seven-day pattern theologically, it is
always in terms of the flow of time from a beginning to an end.
The “framework hypothesis” Platonizes the time sequence into a
mere set of ideas. In its attempt to be theological, the “frame-
work hypothesis” misses the whole theological point,®

The World of the Temple 237

The Northern Kingdom, however, slid downhill very fast,
and was taken captive by Assyria. The Southern Kingdom en-
dured longer, but God eventually judged it as well. The last pre-
liminary judgment is recorded in 2 Kings 24. Nebuchadnezzar
conquered Jerusalem and carried off the nobility, including such
persons as Daniel and Ezekiel. He also carried off the gold of the
Temple, which was symbolically equivalent to these leading men.

Israel refused to repent, however, and so Nebuchadnezzar
returned. This time he blinded the king, burned the city, burned
the Temple, and carried off all the people. After being told of the
deportation of the people, we are told that he broke up the
bronze pillars and took away all the bronze “service” utensils of
the Temple (2 Kings 25:11-17). These small bronze vessels were
the symbolic equivalent of the ordinary people, while the break-
up of the pillars, to which special attention is called, is to be asso-
ciated with the destruction of the king.?*

Summary
The Kingdom establishment can be set out as follows:

New Names:

God: No new name is really highlighted,
but in David’s prayer of 2 Samuel 7,
when the Davidic Covenant was
made, the name used is Lord God,
which in Hebrew is “the Master, the
Lord.” Since the condition of the
Kingdom was that the human King
recognize the Lord as Supreme
King, this phrase “Master, Lord”
scems eminently appropriate.

People: The House of David is a new name
here. Also, the name Israel comes to
be associated with Northern Israel,
and thus with apostasy; while the
name Judah comes to signify the
relatively more faithful Southern
Kingdom, Yet another name that
comes into play is “Remnant,” denot-
ing the faithful in times of apostasy.

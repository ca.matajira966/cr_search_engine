242 THroucn New Eves

continued to have Temple duties, there was a shift away from
them at the synagogue level.

Third, in terms of social polity, both Judah and Northern
Israel were repeatedly conquered and vassaled by powers.to the
North and South. This anticipated the world imperial system
that would come in with the Restoration Covenant. Once the
people went into Babylonian captivity, God completely broke
down the Davidic establishment. There was no king, and the
people were directly under the imperial government. There was
no Temple, and the people had to get used to the idea of a
Spiritual Temple. There was no regular synagogal structure,
and the people had to make do without Levitical leadership.

The new polity was thus anticipated, but the full nature of it
could never have been envisioned by the people living during
Israel’s decline, In exile they still envisioned that a renewed cov-
enant would be much like the old Davidic covenant. The New
Covenant, however, was far more glorious than the previous one.

First, God wanted the World Imperial era because it facili-
tated evangelism. The Jews (their new name) had been told to
settle in Babylon and work for the good of their new cities ( Jere-
miah 29:4-7). As a result, the faith was spread throughout that
land. In the Restoration Covenant, God’s Spirit would be given
in greater measure, and the Jews would travel land and sea,
making Gentile converts.

Second, God wanted non-Levitical synagogues, because
these brought out the spiritual gifts of laymen, and anticipated
the New Covenant Church.

Third, God wanted a smaller Temple. With the Restoration
Temple we have great shift in meaning. Moses had seen the pat-
tern on Mount Sinai, and had built it below. The Mosaic Taber-
nacle symbolized both the nature and the glory of the Mosaic es-
tablishment. Similarly, David had been given the directions for
the Temple. The Solomonic Temple symbolized both the nature
and the glory of the Davidic covenant. This time Ezekiel was
given a vision and blueprint for the post-exilic Temple, but it
was a temple so vast and huge that it could never be built. Eze-
kiel’s visionary Temple symbolized both the nature and the glory
of the restoration establishment, but the Temple actually built
by Ezra was a small affair. Ezra’s Temple symbolized the nature,
but not the glory, of the new restoration covenant.

The New World 263

Mount itself, Jesus said concerning adultery that if your eye
offends’ you, pluck it out; and if your hand offends you, cut it off
(Matthew 5:27-30). Concerning justice, Jesus said not to resist
him who is evil, and to give to him who asks of you (Matthew
5:38-42). Those who wish to become pacifists and take Jesus “lit-
erally” on the subject of not resisting evil, must also take Him lit-
erally on chopping off hands and ripping out eyes. Of course, no
one does the latter, and the Church has always recognized the
wisdom-paradox nature of Jesus’ teachings here.

A second realm of paradox is seen in the parables, which
were both clear and deliberately obscure. Jesus made it plain
that He used parables in order to instruct the righteous and to
confuse the wicked (Matthew 13:10-17), This conception of truth
and teaching is utterly opposed to the Greek rationalistic tradi-
tion in Western thought, which assumes that unaided “reason” is
able to apprehend truth. Jesus says the opposite, maintaining
that the truth is only finally reasonable to the elect, while the
wicked can ultimately never regard it as reasonable. This applies
not only to the parables, but also to the whole of truth. It is a
fundamental aspect of Christian epistemology.

A third realm of paradox lies in the area of reward. Speaking
to the children of Israel, God (Leviticus 26) and Moses (Deuter-
onomy 28) held out rewards for faithfulness and punishments for
disobedience. In general, the rewards had to do with prosperity
and the punishments with affliction. By the time of Solomon,
wisdom had begun to perceive a more mature view of reward
and punishments. In Job, the wise man found that the righteous
sometimes suffer for no cause of their own. In Ecclesiastes, the
wise man found that simply looking at rewards and punishments
gets you nowhere in evaluating the world.

In the New Testament, however, we find highlighted such
thoughts as these:

My son, do not regard lightly the discipline of the Lord, nor
faint when you are reproved by Him; for those whom the Lord
loves He disciplines, and He scourges every son whom He re-
ceives. It is.for discipline that you endure; God deals with you
as with sons; for what son is there whom his father does not dis-
cipline? But if you are without discipline, of which all have be-
come partakers, then you are illegitimate children and not sons
(Hebrews 12:5-8).

278 THroucH New Eyes

plored six of them in this book.)!® The imagery used to describe
each church is drawn from the stage of history appropriate to it.
This is outlined in Diagram 18.2.19 What we learn from this is
that we can draw parallels between our present churches and
civilization to specific times in the Old Covenant, analogies that
will help us understand our present predicament. I shall make
an attempt to do just that in Chapter 19.

Conclusion
The New Covenant establishment can be set out as follows:

New Names:
God: Father, Son, and Holy Spirit; Jesus
Christ
People: Christians

Grant: The New Jerusalem, which is the
Church and the kingdom; and the
world as the place New Jerusalem is
to permeate,

Promise: I will be with you, even until the
end of the world.

Stipulations:
Sacramental: Water baptism and holy communion,

Societal: The entire Biblical law, transformed
through wisdom and paradox into
the New Covenant, as illustrated
but not exhausted in the Epistles.

Polity:

Church: Temple sacraments and synagogue
preaching are rolled together. There
is no longer any blood line of
priests. The Church is the first form
of the Kingdom, around which a
new culture develops.

State: Romans 13 says that the civil magis-
trate is set up by God to be an
avenger of blood. Under Christian
influence, the magistrate is per-

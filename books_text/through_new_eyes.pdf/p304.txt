292

it.

12.
13.

15.

Notes To Paces 14-29

. For a full discussion, see Jordan, “Saul: A Study in Original Sin,” The Geneva

Papers 2:11 (July, 1988; Tyler, Texas: Geneva Ministries).

. Roderick Campbell, Israel and the New Covenant (Tyler, TX: Geneva Ministries,

[1954] 1983), p. 60. Also see Othmar Keel, The Symbolism of the Biblical World
Ancient Near Eastern Iconography and the Book of Psalms, trans. Timothy J. Hallett
(New York: Seabury Press, 1978).

David Ghilton, The Days of Vengeance: An Exposition of the Book of Revelation (Fort
Worth; Dominion Press, 1987), p. 33.

Ibid., pp. 3448.

Ben C. Ollenburger, Zion, City of the Great King: A Theological Symbol of the Joru-
salem Cult (Sheffield, England: Sheffield Academic Press, 1987), pp. 19-20.

. In the course of this book I shall have occasion to note several such studies. I

wish to advise the reader that my endorsement of such works is highly selective.
The most useful discussion of the difference between Biblical typology and
Greek allegory in the Church Fathers is Jean Daniélou, From Shadows te Reality:
Studies in the Biblical Typology of the Fathers, trans. Wulstan Hibberd (Westminster,
MD: Newman Press, 1960). On the history of typology, see Richard M. David-
son, Zppology in Scripture: A Study of Hermencutical TYPOS Structures (Berrien
Springs, MI: Andrews University Press, 1981).

Chapter 2—The Purpose of the World

tL.

2.

7.

8.
9.
10.
11.

12,
13.

Note written by Luther inside a book of Pliny. Martin E. Marty, Health and
Midisine in the Lutheran Tradition (New York: Crossroad Pub., 1983), p. 27.

‘The best discussion of the philosophy of “Being” versus Christianity is Rousas
J. Rushdoony, The One and the Many (Tyler, TX: Thoburn Press, [1971] 1978).

. Herman Bavinck, The Doctrine of God, trans, William Hendriksen (Edinburgh:

The Banner of Truth Trust, [1918] 1977), p. 91.

. Ibid, p. 88.
. Quoted in ibid.
. The finest single essay on this subject is Cornelius Van Til, “Nature and Scrip-

ture,” in Paul Woolley, ed., The Jnfallible Word, 34 ed. (Phillipsburg, NJ: Pres
byterian arid Reformed Pub. Co., 1967). Van Til shows that natural revelation
is necessary, authoritative, sufficient, and clear. From an different theological
tradition, see the remarks of Alexander Schmemann, For the Life of the World
(Crestwood, NY: St. Vladimir's Seminary Press, 1973), pp. 117-134.

John M, Frame, The Doctrine of the Knowledge of God (Phillipsburg, NJ: Presby-
terian and Reformed Pub. Co., 1987), p. 230.

Bavinck, p. 88.

Thid., p. 89.

Van Til, in Woolley, p. 273.

Bavinck, pp. 86-7, Bavinck continues, ‘Further, human actions are ascribed to God,
as, knowing, Genesis 18:21; trying, Psalm 7:9; thinking, Genesis 50:20... .”
He fills half a page with these,

Ibid., p. 87.

Tbid., p. 97.

Chapter 3— Symbolism and Worldview

1,

While any survey of the history of philosophy can be consulted for more infor-
mation on this, for our purposes the best introduction is Rousas J. Rushdoony,
The One and the Many (Tyler, TX: Thoburn Press, [1971} 1978). On the subject

20.
21.

22.

23,
24,

25,
26.

Notes To Paces 213-222 307

A fuller exposition of this theme is found in Rodriguez, “Sanctuary Theology”
On the changes in priesthood, and for a detailed study of the boundaries and
their guards, see Jordan, “Death Penalty in the Mosaic Law,” chap. 3

For a complete study of the yeils and their symbolism, see Jordan, “From Glory
to Glory,”

I have argued for these correlations in ibid.

A very useful study of Jesus’ association of the Temple with His body is Lucius
Nereparampil, Destroy This Temple: An Exugetico-Theological Study on the Meaning of
Jesus’ Termple-Logion in Jn 2:19 (Bangladore, India: Dharmaram [College]
Publications, 1978).

Kline, Zmayes, chap. 1.

On'the heart and mind, and other aspects of the hunan person in the Bible, see
Hans Walter. Wolff, Anthropology: of the Old Testament, \ans, Margaret Kohl
(Philadelphia: Fortress Press, 1974).

Chapter 16—The World of the Temple

1.

ye

. 1 Samuel

A comparison of this genealogy with others, and with the chronology of the per-
iod, shows that there were doubtless more than ten generations involved.

David was born 406 years after the entrance into Canaan (1 Kings 6:1; 2 Sam-
uel 5:4), Perez was born about two hundred years before the Exodus, assuming
that the Hebrews lived in Goshen 215 years, dating the 430 ycars of Egyptian
captivity from the time Abraham moved out of Canaan into (Egyptian)
Philistia, after the destruction of Sodom (Genesis 19-20). Judah was about
forty-two when the Hebrews moved to Goshen, and Perez was his grandson.

Thus, the story of Judah is Genesis 38 has been recorded out of chronological
order, for theological reasons, and actually took place after the Hebrews had
relocated their homes to Goshen, At any rate, this puts about six hundred years
between Perez and David, which is a rather long time for ten generations. Also,
Nahshon was Aaron’s age (Exodus 6:23), and Salmon was Rahab’s husband
(Matthew 1:5). Their marriage took place about four hundred years before
David's birth, We know that Jesse was David's true father, and since David was
his eighth son, we can put Jesse’s age at about fifty, and thus Jesse’s birth about
356 years after the Conquest. The genealogy is Salmon—Boaz— Obed — Jesse.
We know that Boaz was the true father of Obed (Ruth 4). Thus, there is & con-
siderable gap either between Salmon and Boaz, or between Obed and Jesse, or
bath. I have argued. elsewhere that for Biblical thealogical reasons the birth of
Obed might well be correlated with the births of Samson and Samuel. Uf this
guess be correct, then Obed was Jesse’s true father. This means that the gap
would be between Salmon and Boaz, On the birth of Obed, see James B. Jor-
dan, Judges: God's War Against Humanism ("Tyler, TX: Geneva Ministries, 1985),
pp. 239-240,

. The expression “there was no king in Israel” refers to the Lord’s kingship, not to

a lack of human kingship. This is clear from the entire context of Judges, which
sets the Lord’s kingship against human kingship at this stage of history, For ex-
tended comments on this theme in Judges, see Jordan, Judges, passim.

22, 14:52; 23:13; 24:2; 25:13; 2 Samuel 15:18; 20:7. See James B.
Jordan, "The Israelite Militia in the Old Testament,” in Morgan Norval, ed.,
The Militia in 20th, Century America: A Symposium (Falls Church; VA: Gun
Owners Foundation, 1985), pp. 34-36.

 
  

      

 

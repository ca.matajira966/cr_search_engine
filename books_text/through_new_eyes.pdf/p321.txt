Nores ro Paces 230-244 309

17, For instance, the word “socket? or “pedestal” in Song of Solomon 5:17 occurs in
Job 38:6 referring to the foundation of the earth, and the only other use of this
word is for the sockets that held up the pillars of the Tabernacle, in connection
with which it is used fifty-two times in Exodus 26, 27, 35-40, Numbers 3, 4.
The comparison of Solomon to a pillar correlates with the bronze Boaz pillar of
the Temple, which represented the King.

18. Pyalin 45 is generally taken as the key to Song of Solomon. I highly recommend
G. Lloyd Carr, The Song of Solomon: An Introduction and Commentary (Downers
Grove, IL: Inter-Varsity Press, 1984), though I put more stock in the tradi-
tional typological aspects than he does. Compare for instance the call of the
bride in Song of Solomon 8:14 with the call of the Bride in Revelation 22:17-21.

19, The Most Holy of the Tabernacle was a cube ten cubits on a side, or one thou-
sand cubic cubits, In the Temple it was twenty cubits on a side, or eight thou-
sand cubic cubits. The Holy Place in the Tabernacle was twenty by ten by ten,
or two thousand ‘cubic cubits. In the Temple the Holy Place was forty cubits
long, twenty cubits wide, and thirty cubits bigh, for twenty-four thousand
cubic cubits.

20. On the names of Jachin and Boaz, see 1 Chronicles 24:17; 2 Kings 11:14; 2
For a full study of this, see James B. Jordan, “Thoughts on Jachin and Boaz”
{available from Biblical Horizons, P.O, Box 132011, Tyler, TX 75713).

21, The number 666 is hardly included by accident here. See my discussion in
James B. Jordan, “Revelation Made Practical,” a six-tape series with a thirty-
seven-page study syllabus, available from Biblical Horizons, P.O. Box 132011,
Tyler, TX 75713.

22. The Open Bible (Nashville: Thomas Nelson, 1985), p. 28

23. For more on this, see Jordan, “Jachin and Boaz.”

 

Chapter 17— The Worlds of Exile and Restoration

1, Generally speaking, the post-exilic period is overlooked in books on the history
of the covenants. It is often viewed as a time of weakness rather than of
strength, which is the opposite of my contention here. Additionally, the vi-
sionary Temple of Ezekiel is usually referred to the New Covenant, instead of
first and foremost to the Restoration. But see the works referred to in note 5
below,

2. This is an inference from such passages as 1 Kings 18:4; 20:35; 2 Kings
6:1. “Sons” of the prophets means “disciples,” and implies apprenticeship. Obvi-
ously, somebody was conducting routine pastoral ministry in Israel. Who else
could it have been?

3. The approach of older Christian commentators, such as John Calvin, to the
subject of prophecy is a valuable corrective to the modern evangelical ap-
proach, which gencrally leaps over the post-exilic period, In his remarks on Jer-
emiah 23:7-8, Calvin writes that “Christians have been too rigid in this respect;
for passing by the whole intermediate time between the return of the people
and the coming of Christ, they have too violently turned the prophecies to spiri-
tual redemption.” Calvin on jeremiah, trans. John Owen (Grand Rupids: Baker
Book House, reprint 1979) 3:149.

4, When Ezekiel’s wife died, be was forbidden to mourn, a condition levied only
on the High Priest (Ezekiel 24:16-18; Leviticus 21:10-12). He entered his minis-
try, like any other priest, ‘in his thirtieth year (Ezekiel 1:1).

 

 

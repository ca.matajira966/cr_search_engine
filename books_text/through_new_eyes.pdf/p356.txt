344 Turoucu New Eves

Kenites, 202

keys of the Kingdom, 105, 113, 264
king, 221, 224, 307

King’s Friend, 41

kingly task, 133ff.

Kishon, 236

ladder to heaven, 84, 87., 158, 16If.,
19if., 251, 304

Lamech, 169

lampstand, 86f., 206, 211f., 216f., 222,
231, 234, 253, 268, 270, 312

last days, 274

aver, 161, 206, 222, 249, 252, 267

law, 2, 119f., 2600, 31L
change of, 225f.
Mosaic, 198f.

leaven, 313

leviz, 98

Levites, 203, 213

light, 11, 12, 43, 54

lily, 84, 231

literature, 13f.

liturgy, 15, 37, 128F.

locust, 64

Lord (name), 198

Lord of Hosts, 255

Lord’s Supper, 125f.

Lot, 183, 304

Lucifer, 75

Luz, 191

man, 1338,
symbol of God, 238.
manna, 74, 109f., 216f., 312
marriage, 136
Mary Magdalerie, 268f.
maturation, 161, 283f%.
measuring, 137
medicine, 82
Melehizedck, 82, 176
memorials, 35
mercy seat, 207, 269
Midianites, 202
millennium, 313
miracle, 107ff., 185
mixtures, 297
Mizpah, 223
monsters, 13, 99
moon, 54
see astral bodies
Mordecai, 251

Moriah, 75, 85, 155f., 174, 228, 246

Mosaic establishment, 20247.

Mosaic law, 226f.

Moses, 70, 135, 139

Most Holy, 206, 231,, 244, 268, 309

mountain, holy, 85, 88, 102, 117, 1558,
161, 168, 174, 183, 192, 206, 212F.,
249f., 269, 272, 301, 304
and men, 163, 301
bronze, 251, 253, 254
see Avarat, Carmel, high ground,
Horeb, Moriah, pyramids, Sinai,
ziggurat, Zion

murder, 174

music, 13, 37

mustard, 92

mmuzzling ox, 98, 297

myrile, 92, 252

names, 298

Nathanicl, 87

natural law, 107ff.

natural revelation, 22, 292

 
 

Nazis

Nebuchadnezzar, 15, 87, 154, 237, 245,
285

nephilim, 175

Nero, 15f.

New Heavens and Earth, 270f.

New Jerusalem, 77, 118, 158, 269f., 294,
312

Newton, John, 68

Nicene Creed, 112

Nicodemus, 56

night visions (Zechariah), 252

Nimrod, 177f., 302

Noah, 1674, 183

Noahiv covenant, 109, 1731.

Nob, 225

north and south, 148ff., 183, 228, 246,
301

oak, 90, 92
oasis-sanctuary, 190, 193, 203
oblation, 298

oil, 86, 191f.

Old covenant, 259ff.

olive, 86, SIE.

Olivet, 96, 156, 183, 296, 301
onyx, 73

opera ad extra, 31

Subject Index 345

ox and ass plowing, 98, 297

palace, 222
palm, 88, 908.

Papacy, 287

parables, 263, 286

peradox, 262, 311

passover, 198, 252, 304, 305
Paul, 16

pearls, 77

Peleg, 304

Pentecost, 64f., 270, 274
personality: of God, 22

Peter, 73

Pharaoh, 57

Pharisees, 261

Philistia, Philistines, 183, 192, 223
Philo, 294

philosophy, 3

piety, 127

pillars, 147, 191f., 231, 251, 254, 309
pillar of cloud, 48

Pishon, 150.

planting of God, 302

Plato, 17

Pleiades, 212

Potipherah, 305

preaching, 126f.

priest, 133, 136i, 203, 225f.
priest-king, 175f., 202
procession of the Holy Spirit, 43
prophecy, 134

prophet, 1388,

psalter, 14f.

pyramid, 88, 117, 1571., 231, 272

rabbinic tradition, 199
rainhow, 46, 49, 76f., 85, 174f., 177
reed, 86
Reformation, 268
Remnant Church, 312
replacement of firstborn, 188
rest, 120
Restoration establishment, 24iff.
Restoration temple, 241,
restructuring, 119, 198
revelation of God, 208.
rivers, 148, 152, 153
Ezekiel’s, 249, 2858.
rabe, 174, 300, 303
Rock, 69M, 295
rod, 85f,

Rolling Stones, 13

sabbath, 153, 169, 200, 204, 306
plants, 297
sackcloth, 65
sacraments, 33f,
sacrifices, 200
Ezekiel, 310
sacrificial system 99
Samson, 56, 223
Samuel, 71, 86, 203, 223
sapphire, 46
Sardis, 312
Satan, 137, 270, 312
Saul, 8&8
scales, 102
science, 2, 9f., 12, 107#f., 134, 291
sea, 144£., 271
bronze, 99, 222, 231, 249
Gentile, 96, 100, 146, 153, 312
heavenly, 161
seer, 141, 226
separation, 119
seraphim, 112
serpent, 101, 102, 137
service, 135
Seth, Sethites, 170
Seven Churches of Asia Minor, 2751f.
Seventy Nations of the World, 175
shade, 72, 82
shoes, 101f.,, 214
showbread, 212, 267, 208
silver, 74
Sinai, 85, 156, 212f.
sister marriage, 199
six days, 9ff., 118
six-fold action, 121
slavery, 185, 198
sleep, deep, 299
soil, cursed, 100
see dust
Solomon, 16, 30, 54, 75, 134, 154, 225
son of man, 244
Song of Moses, 69f.
sons of God, 170, 302
south
see north
spoils, 186
stars, 105, 189, 212, 215, 270
fall of, 57,
see astral bodies

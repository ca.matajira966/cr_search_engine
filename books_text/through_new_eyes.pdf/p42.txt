30 Trroucn New Eves

Very often this approach to symbolism is brought over, un-
intentionally, into our view of the Bible. It is assumed that the
symbolism in the Bible is ultimately arbitrary, not grounded in
creation design. More liberal commentators assume that the
men who wrote the Bible used the man-generated symbolism of
their day to:express their ideas.. More conservative commen-
tators assume that God just decided arbitrarily to use this or
that item to symbolize this or that truth. Such an approach,
however, implicitly denies the doctrine of creation, as we have
seen it in Chapter 2 of our study. Symbolism is never arbitrary
or secondary.

In the Western world for several centuries, men have assumed
that the proper way to express truth is by means of abstract, phi-
losophical language. Wherever we find imagery, parable, symbol-
ism, or typology, we ought to translate such language into proper
abstractions. This, however, is not how Gad chose to reveal Him-
self to us. To be sure, some parts of the Bible are written in
abstract language, especially the epistles of St. Paul. Most of the
Bible, however, is written in stories, histories, pocms, symbols,
parables, and the like. As far as God is concerned, this way of re-
vealing truth is equally as important as abstract philosophizing.?

Notice, for instance, the way in which our confessions of
faith and catechisms are written. They are virtually devoid of
imagery. Solomon: wrote Proverbs to instruct youth, but for cen-
turies Christians have used catechisms that consist basically of
definitions of terms: What is justification; what is prayer; what is
meant by the fourth petition; etc. The contrast of approaches is
quite startling. It illustrates for us the problem we have in recov-
ering the Biblical worldview.

The Primacy of Symbolism

Symbolism, then, is not some secondary concern, some
mere curiosity. In a very real sense, symbolism is more im-
portant than anything else for the life of man. As we have seen,
the doctrine of creation means that every created item, and also
the created order as a whole, reflects the character of the God
who created it. In other words, everything in the creation, and
the creation as a whole, points to God, Everything is a sign or
symbol of God.

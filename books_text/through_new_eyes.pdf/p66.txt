J4 Turoucu New Eyes

The first thing said about the astral bodies is that they are lights. We
have seen that light is an aspect of God’s glory-cloud, and it is as
a reflection of God's glory that these heavenly bodies are made as
lights. They represent glory, and so the Bible can say of the glo-
rified saints that “the righteous will shine forth as the sun in the
kingdom of their Father” (Matthew 13:43), Similarly, Solomon
wrote, “Who is this that grows like the dawn, as beautiful as the
full moon, as pure as the sun?” (Song of Solomon 6:10). Or as St.
Paul wrote, “There is one glory of the sun, and another glory of
the moon, and another glory of the stars; for star differs from
star in glory” (1 Corinthians 15:41).

It is because the heavenly bodies show God’s glory that we
delight in looking at beautiful pictures of them in astronomy
books. We live in a happy age, to have access to photographs of
such wonders as the Ring Nebula, the Crab Nebula, and the
great spiral nebula in Andromeda.

As lights, the astral bodies are glorious. But, second, they were
given for signs, or symbols. As we have seen, all created things point
back to God; but all things also symbolize particular things, and
in this case, the astral bodies symbolize rulers and governors.
The lights are positioned in the firmament, called heaven.
Heaven rules the earth.. Thus, those things positioned in the
firmament symbolize rulers of the earth, as we shall see shortly.

Third, they are said to be for seasons, or, more literally, for festival
times. This applied to the Old Covenant, which was regulated by
these creational clocks. It was particularly the moon, regulator of
months, that governed. the Israelite calendar. The moon estab-
lished which day was the first of the month, and which was the fif-
teenth. Such festivals as Passover, Pentecost, and Tabernacles
were set on particular days of the month (Leviticus 23:5-6, 34;
Numbers 28:11-14; 2 Chronicles 8:13; Psalm 81:3). The moon, of
course, governs the night (Psalm 136:9; Jeremiah 31:35), and in
a sense the entire Old Covenant took place at night. With the
rising of the Sun of Righteousness (Malachi 4:2), the “day” of the
Lord is at hand (Malachi 4:1), and in a sense the New Covenant
takes place in the daytime.! As Genesis 1 says over and over, first
evening and then morning. In the New Covenant we are no
longer under lunar regulation for. festival times (Colossians
2:16-17). In that regard, Christ is our light.

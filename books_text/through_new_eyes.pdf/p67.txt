Sun, Moon, and Stars 55

Diagram 5.1
Sun, Moon, Stars

If we compare Genesis 1:14 and 16 we see employed the literary device known
as chiasm. A chiasm is a literary device in which parallel ideas or terms are pres-
ented in a sandwich form instead of normal parallelism; that is, ABCDDCBA instead
of AABBCCDD. The use of chiasm, which helps ring out the particular symbolic
associations of sun, moon, and stars:

A 14, Let them be-for signs
B 14, And for festival times
C 14, ’And for days and years
C’ 16. The greater light to govern the day
B’ 16, And the lesser light to govern the night
A‘ 16. The stars also

The stars (A) are primarily associated with astral symbolism. The maon (B) is a5-
sociated with appointed festivals, which began in the evening and were removed in
the New Covenant. The sun (C) is associated with days and years.

Fourth, they are said to be clocks for days and years. Long before
our mechanical clocks and watches were invented, people told
time by the position of the sun, the occurrence of solar equinoxes
and solstices, and the precession of the equinoxes. Particularly
mentioned are days and years, which are regulated not by the
moon but by the sun.

Fifth, they are said to rule over day and night, to govern time. Here
again the emphasis is on rule. The astral bodies signified those
who are glorified and exalted. While this is true of all the saints,
it is also true of all human rulers as well. Revelation 1:20 says
that the rulers of the church are like stars, and Jude 13 says that
apostate teachers are “wandering stars.” Long before this, in
Genesis 37:9-10, Joseph had seen the rulers of his clan as sun,
moon, and stars. We see this even today. The flag of the United
States of America has fifty stars, for the fifty states of our nation.
The flags of oriental nations include the rising sun. The flags of
Near-Eastern countries feature a crescent moon. Sun, moon,
and stars are symbols of world powers.

Sixth, they are associated with the heavenly host, the angelic and
human array around the throne of God. This ‘also follows from the fact
that they are positioned in heaven. They represent the angelic
host in Judges 5:20, Job 38:7, and Isaiah 14:13. They represent
the human host of the Lord as well, as we see from the promise

38 Taroucy New Eves

Abraham’s Stars

Tn Genesis 15, when God cut the covenant with Abraham,
He took him outside and told him to “tell” the stars, “if you are
able to tell them; thus shall your seed be” (v. 5). Bible exegetes
differ on exactly what Abraham was being asked to do. Generally
it is assumed that he was being asked to count up the number of
the stars, and that his descendants would be like the stars of the
heavens for multitude, even as they would be like the sand of the
sea. Some have pointed out, however, that the Hebrew verb
translated “count” can also be translated “tell” in the sense of
“evaluating” (Hebrew saphar, as in Psalm 56:8). This is not clearly
the case, however, since the verb often just means “count up.”

All the same, two alternative interpretations have been sug-
gested. The first is that of M. Barnouin. Barnouin points out
that the patriarchs in Genesis 5 and fi lived lives of curious
numerical lengths. Enoch, for instance, lived 365 years, the
length of a solar year. Kenan lived 910 years, ten times a stan-
dard quarter year of 91 days. Lamech lived 777 years, which is
the sum of the synodical periods of Jupiter (399 days) and
Saturn (378 days) (Genesis 5:23, 14, 31). Is it possible that God
was saying to Abraham that his seed would be like the great
patriarchs of old, the faithful godiy men who were blessed and
preserved before the Flood, and in the years after the Flood?

Barnouin suggests that when Abraham looked at the stars,
he was considering the planets and how they govern time (Genesis
1:14), and making an evaluation based on this. The years of the
patriarchs corresponded to the time-governing periods of the
planets and other heavenly bodies. Abraham’s seed would be
like this. They would be a heavenly people, gathered around
God’s heavenly throne. Their history would mark time.

Barnouin sees this fulfilled in the censuses of the book of
Numbers, in which these same astral periods recur. In Numbers
1, ali the men twenty and older were enrolled in the Israelite
militia, God’s army. As God’s army, Israel was in one sense a
“heavenly host,” captained by the Lord of heaven. In this
respect, they are spoken of as stars in Deuteronomy 1:10, and as
a heavenly host they are commanded by an angel, the Angel of
the Lord ( Joshua 5:13-6:2; Exodus 23:20-21). Thus, it would not
be surprising if the numbering of that heavenly host had some

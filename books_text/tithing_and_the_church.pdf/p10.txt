x TITHING AND THE CHURCH

Lorp of hosts, if I will not open you the windows of heaven, and
pour you out a blessing, that there shall not be room enough to
receive it. And I will rebuke the devourer for your sakes, and he
shall not destroy the fruits of your ground; neither shall your
vine cast her fruit before the time in the field, saith the Lor of
hosts. And all nations shall call you blessed: for ye shall be a
delightsome land, saith the Lorp of hosts (Mal. 3:8-12).

Most pastors today do not believe Malachi’s warning. Of
those who do believe it, there are not many who will go into
the pulpit and preach it. Of those who do preach it, they do
not preach it often. Of those who preach it often, they find that
most members pay no attention except to suggest that the
minister preach on something “less worldly.”

No church or denomination today is willing to bring sanc-
tions against members who refuse to tithe. Preaching God's law
for the church without the ability to enforce it ecclesiastically is
an exercise in futility. It is not surprising that pastors refuse to
tackle this topic.

Even if they did, tight-fisted members could comfort them-
selves with this thought: “Well, he’s not an impartial witness. If
everyone started paying his tithe, the church’s income would
rise, and the pastor might get a raise.” The grumblers see self-
interest as primarily economic, It never occurs to them that a
pastor might preach on tithing because he is afraid that God’s
warning through Malachi is still in force.

Here is the problem today: most Christians agree with all
humanists regarding God's predictable, covenantal, corporate
sanctions in history, namely, such sanctions do not exist. But
they do exist, which is one reason why I wrote this book. I fear
these sanctions. Even if I pay my tithe, I may come under
God’s corporate negative sanctions. Jeremiah and Ezekiel were
carried into captivity by the Babylonians, despite the fact that
they had preached the truth to doomed people who paid no
attention to the threat of God’s predictable, corporate, covenan-

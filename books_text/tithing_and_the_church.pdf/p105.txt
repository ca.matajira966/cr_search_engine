Primary Sovereignty: Church or Family? 91

our day. There is nothing unique about Christians today who
dismiss as “legalism” any suggestion that they owe ten percent
of their net income to God. But Rushdoony, as the co-founder
of Christian Reconstruction, could hardly take this antinomian
approach to the question of the tithe. The Bible is clear about
the tithe’s mandatory percentage: men owe ten percent of their
net income to God.° The argument is not over the tithe’s per-
centage; the argument is over which agency (if any) possesses
the God-given authority to collect it and then distribute it. The
debate within Christian Reconstruction is over this question:
Where is the locus of God's delegated sovereignty over the allocation of
tithe: In the tither or the institutional church? I answer: with the
institutional church. Rushdoony answers: with the tither.

From 1965 until today, Rushdoony has.sporadically attempt-
ed to cobble together a doctrine of the institutional church in
order to support his view of the tithe. His view of the tithe is
that Christians can lawfully send the tithe anywhere they wish;
therefore, the institutional church has no Jawful claim to any
portion of the tithe, or at least not above the tenth of a tenth
that went to the Aaronic priesthood under the Mosaic law. He
has needed a doctrine of the church in order to defend such a
thesis theologically. In this section of the book, I examine the
connections between his view of the tithe and his view of the
institutional church.

This has not been an easy task. Rushdoony has never written
a book on the doctrine of the church, nor do I expect him to,
for reasons that will become clear as you read this section. (This
is even more true of his defense of the continuing authority of
the Mosaic dietary laws: not so much as one full page of exege-
sis devoted to’ the topic, despite its great importance for him
personally as a distinguishing mark of his theology.) There

9. There is a subordinate question: the third-year tithe and the poor tithe. Were
these separate, additional tithes? Rushdoony argues that they were, Rushdoony
Institutes, p. 53.

10. He never comments on I Corinthians 8: “Howbeit there is not in every man

102 TITHING AND THE CHURCH

sions of Volume 1 of The Institutes of Biblical Law, would destroy
the covenantal basis of Rushdoony’s theology and therefore also
his social theory.

The fact is, his three-fold error came late in his career. This
shift in theology began shortly before he left the Orthodox
Presbyterian Church in 1970, but it was not completed until the
early 1980's. In other words, what Volume 1 of The Institutes
hath given, Volume 2 need not take away. Only small traces of
his error are visible in Volume 1; this error can and must be
separated from that foundational book. Because of this, I find
it necessary to challenge the book that he and Edward Powell
co-authored, Tithing and Dominion (1979). The chapters are
identified as to which author wrote which. I refer here only to
Rushdoony’s chapters. (Rushdoony broke decisively with Powell
shortly after he broke with me and Jordan.)

Tithing and Dominion

With respect to the task of dominion, the Bible teaches, first,
that the dominion covenant was established between God and
the family. God has assigned to the family the primary domin-
ion task in history (though not in eternity): to be fruitful and
multiply (Gen. 1:26-28) — a biological function.‘ Second, as we
shall see, the tithe is a mandatory payment from man to God
through a covenantal institution: the church. Therefore, if the
tithe were the basis of dominion, God’s law would mandate a
tithe to the family, the agency of dominion. But there is no
God-specified mandatory payment to the family, i.e., no legal
entitlement. On the contrary, it is the productivity of individu-
als, families, and other voluntary associations that is the source
of both tithes and taxes. This is inevitable: the source of the fund-

8. Edward A. Powell and Rousas John Rushdoony Tithing and Dominion (Valle-
Cito, California: Ross House, 1979),

4, Gary North, The Dominion Covenant: Genesis (2nd ed; Tyler, Texas: Institute for
Christian Economics, 1987), ch. 8,

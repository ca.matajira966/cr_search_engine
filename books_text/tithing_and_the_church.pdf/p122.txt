108 TITHING AND THE CHURCH

Lord’s Supper as a divinely empowered covenant-renewal ceremony of
the institutional church, a ceremony that invokes God’s positive
and negative sanctions in history and eternity.

The institutional church has only one ultimate means of
discipline: excommunication, ie., excluding a person from the
rite of the Lord’s Supper. Without the positive sanction aspect
of the Lord’s Supper, the negative sanction of exclusion is
judicially meaningless. Such a nominalist view" of the Lord's
Supper strips the institutional church of its disciplinary authori-
ty. Rushdoony has not heeded Calvin’s warning when Calvin
wrote that “it is certainly a highly reprehensible vice for a
church not to correct sins. Besides, I say our Lord will punish
an entire people for this single fault. And therefore let no
church, still not exercising the discipline of the ban, flatter itself
by thinking that it is a small or light sin not to use the ban
when necessary.”” Nor, with respect to local church member-
ship and faithful weekly attendance, did Rushdoony pay atten-
tion personally, from at least 1970 until late 1991 - assuming
the Chalcedon Foundation is in fact a church - to Calvin's next
warning: “But this is not to say that an individual is justified in
withdrawing from the church whenever things are contrary to
his will." Calvin did not defend the individual’s autonomy in
relation to the institutional church. Calvin fully understood
what the sole basis of a declared Christian's judicial separation
from the institutional church has to be: excommunication.

Excommunication can be of two kinds: excommunication by
the institutional church and excommunication by the former
church member, ie., self-excommunication.

19. The nominalist acknowledges no judicial authority beneath the words that
define the sacraments. Thus, the sacraments become a mere mentorial. This was
Zwingli’s view of the Lord’s Supper: It is also the Baptist view.

20. John Calvin, “Brief Instruction for Arming All the Good Faithful Against the
Errors of the Common Sect of the Anabaptists” (1544), im Treatises Against the Anabap-
lists ond Against the Libertines, edited by Benjamin Wirt Farley (Grand Rapids, Michi-
gan: Baker, 1982), p. 65.

21. Idem.

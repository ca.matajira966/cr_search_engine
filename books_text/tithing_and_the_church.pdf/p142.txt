128 TITHING AND THE CHURCH

Biblical; it is pietism, a surrender of Christ's kingship over the
world. The purpose of the tithe must be to establish that king-
ship.”*

It is clear why Rushdoony refuses to cite the texts in Num-
bers which established the legal basis of the claim of the Levites
to the tithe. These passages explicitly link the tithe and the
office of ecclesiastical guardian. It was not the Levites’ social
services that entitled them to the tithe; it was their boundary
service as the temple’s agents of execution: guardians of what
was sacramentally holy.

Rushdoony makes a valid Protestant point: the kingdom of
Christ is larger than the institutional church. As he says, limit-
ing the kingdom to the institutional church is indeed the es-
sence of pietism. But he has created great confusion in his own
mind and his followers’ minds by equating the tithe and charita-
ble giving to the broader kingdom. This view of the tithe is equally
pietistic: it limits the financing of the kingdom. The kingdom of
Christ in history is comprehensive. It must be extended by
every bit of productivity at the disposal of covenant-keepers.®
When a Christian makes a profit or earns a wage, all of this is
to be earmarked for extending the kingdom of Christ, broadly
defined: education, entertainment, the arts, leisure, capital
formation, etc.

The kingdom of Christ is not extended primarily by charita-
ble institutions. The kingdom of Christ is extended through
dominion, and this is financed by Christians’ net productivity.
Rushdoony understands this “net productivity” principle with
respect to taxation: the State may not lawfully tax capital, only
net income. This is why he has long opposed the property tax
as anti-Christian.” But he does not acknowledge that this

8, Idem.

9, Through common grace, it is extended even by covenant-breakers. North,
Dominion and Common Grace.

10. He wrote in 1967: “The property tax came in very slowly, and it appeared
first in New England, coinciding with the spread of Deism and Unitarianism, as well

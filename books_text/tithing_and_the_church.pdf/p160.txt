146 TITHING AND THE CHURCH

on the good deeds that churches perform in society. This is the
U.S. government's view of non-profit status, the liberal’s only
reason for allowing the church to escape the tax man.

Rushdoony’s view of the church is libertarian. He views the
church strictly as a voluntary society. In his view, the church is
not founded on a self-maledictory oath before God, for such an
oath would transfer judicial. authority to church officers as
God’s monopolistic agents. They could then lawfully compel
payment of the tithe by members.

His view of church authority creates a divine right of the
individual church member. The individual alone supposedly is
God's designated agent who lawfully controls the distribution of
the tithe rather than the church's ordained authorities. Beyond
him there is no ecclesiastical appeal.

The alternative to a Christian view of society that places the
church covenant at the center of its social theory is either a
Statist view of society or a patriarchal view of society. Rush-
doony, faithful to an Armenian heritage that did not survive the
second generation of immigrants — his generation — has chosen
the latter view. Patriarchalism cannot survive for even three
generations in a society that prohibits arranged marriages and
allows easy divorce.

It also cannot survive the biblical view of marriage. It was
Roman law, with its intense patriarchalism, that kept the clans
alive. The English common law heritage was, from the twelfth
century onward, utterly hostile to the revived Roman law's view
of marriage and family authority, which steadily gained new
respect and power on the Continent.” That Rushdoony
should be regarded as soft on divorce, which in some cases he
is,"' is ironic: nothing undermines a patriarchal society — the
family as sacramental — faster than the widespread acceptance

10. Alan Macfarlane, Marriage and Love in England: Modes of Reproduction 1300-
1840 (Oxford: Basil Blackwell, 1986), ch. 7: “Who Controls the Marriage Decision?”
11. See Chapter 10.

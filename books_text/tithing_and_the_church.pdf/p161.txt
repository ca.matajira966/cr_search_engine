Sacraments or Social Gospel? 147

of divorce on demand. His own sad experience with his first
marriage, like the similar experiences of his brother and his
sister, should have warned him.

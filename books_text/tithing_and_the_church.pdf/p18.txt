4 TITHING AND THE CHURCH

toward the church. I begin as Jesus did in several of His para-
bles: with men’s pocketbooks, which they understand far better
than they understand theology or social theory.

As far as the Bible reveals, the tithe began with Abraham’s
payment to Melchizedek, the priest of Salem (peace). The tithe
is an aspect of point two of the biblical covenant model:
hierarchy-authority-representation.* The tithe is owed to God
through a representative agency: the institutional church. The
sacraments are an aspect of point four: oath-sanctions.’ They
are dispensed by this same agency. Tithing is unbreakably
connected to the institutional church because the sacraments
are unbreakably connected to the institutional church. This is
why I have titled this book, Tithing and the Church.

Part 1 is divided into five chapters. They parallel the five
points of the biblical covenant model. The structure of Part 1 is:
church sovereignty, church authority, church membership stan-
dards (boundaries), monetary sanctions, and the war over in-
heritance — church ys. State.

Any attack on the God-delegated authority of the institution-
al church to collect the tithe is.an attack on the God-delegated
monopoly source of the sacraments in history. Taking the sacra-
ments in a local church without paying a tithe to that church is
a form of theft. Any refusal to take the sacraments because you
are unwilling to pay your tithe to a local church is a form of
excommunication: self-excommunication. To create your own
home-made church as a means of giving yourself the sacra-
ments while paying yourself the tithe is not only selfexcommu-
nication, it is theft as well. A word to the wise is sufficient.

4. Ray R. Sutton, That Yu May Prosper: Dominion By Covenant (2nd ed.; Tyler,
‘Texas: Institute for Christian Economics, 1992), ch. 2.
5. Ibid., ch. 4.

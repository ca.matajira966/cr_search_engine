Conclusion ta Part 2 167

God’s church of its God-given authority to excommunicate
people and to pray the imprecatory psalms against its enemies.
When will we see a reversal of this political trend? Only when
we see a reversal of this anti-ecclesiastical trend. When the self
excommunicated enemies of God’s church stop complaining
publicly against “Churchianity,” and join the local church as
humble men under the visible authority of others, they can
begin to saw the shackles of the State from off their ankles. Not
before.

Sacramental Sovereignty

The Levitical cultural and social services that Rushdoony lists
as the basis of the Levites’ reception of the tithe were all subor-
dinate aspects of their primary judicial function: to guard the
sacramental boundary around the tabernacle- temple. Second-
arily, Levites were to declare God’s law and to help the priests
administer some of the sacrifices and some of the liturgies of
worship — what Rushdoony dismisses as mere “rites.” The text
in Numbers is clear: the tithe was based on the Levites’ sacra-
mental separation from the people - in other words, their
holiness. “And, behold, I have given the children of Levi all the
tenth in Israel for an inheritance, for their service which they
serve, even the service of the tabernacle of the congregation.
Neither must the children of Israel henceforth come nigh the
tabernacle of the congregation, lest they bear sin, and die”
(Num. 18:21-22).

The New Testament has not abrogated the Old Testament
(Matt. 5:17-20).' The church’s hierarchical authority is ground-
ed on the same judicial foundation that the Levites’ authority
was under the Mosaic law: its God-ordained service as the
guardian of a sacramental boundary. The requirement of each
church member to tithe exclusively to the institutional church

1. Greg L. Bahnsen, Theonomy in Christian Ethics (Qnd ed.; Phillipsburg, New
‘Jersey: Presbyterian and Reformed, [1977] 1984), ch. 2.

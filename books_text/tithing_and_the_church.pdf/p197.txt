Conclusion 183

were under his covenantal authority as a household priest (Gen.
15). But he had first paid his tithe to Melchizedek, who had
final earthly authority over access to the communion meal.
God’s delegation of partial sacramental authority to Abram was
based on Abram’s subordination to Melchizedek.

In the Old Covenant under the Mosaic law, the priests had
the ultimate earthly authority to include and exclude people
with respect to the communal feasts, but a portion of this au-
thority was delegated by God to the larger tribe of Levi. The
“outer circle” of exclusion ~ a sacramental boundary — was the
Levites’ God-assigned task to police. This was a judicial function,
not an economic function.

Rushdoony has rested his case for the authority of the
church on the twin pillars of charity and education, in the same
way that the modern humanists do. The United States Internal
Revenue Service (known in Jesus’ day as “the publicans”) grant-
ed tax exemption to Chalcedon in the same way, and for the
same reasons, that it grants tax-exempt status to any organiza-
tion calling itself a church: its charitable or educational func-
tions. Rushdoony offers an economic interpretation of the church.
This view is shared by most humanists.

In 1966, Rushdoony affirmed: “The church is the ministry of
the word, the sacraments, and of true discipline. Without these,
there is no true church, even though an institution may call
itself a church.”* But year by year, this element of church disci-
pline came into conflict with his theory of the tithe: “We are
told that God's penalties for failure to tithe are severe (Mal. 3:8--
12), but no human agency is given any right to enforce the
tithe.’ Year by year, his ecclesiology abandoned all traces of
church authority, including the authority to police the Lord’s
“Table. The Lord’s Supper in Rushdoony’s ecclesiology went

4. Chalcedon Report No, 14: iid., p. 574.
5. Rushdoony “Inferences and the Law,” Chalcedon Position Paper No. 118
(Nov. 1989); ibid., p. 452.

mummy factory?, 100, 117
nurture, 107

oath, 42, 112

oath &, 8

offerings, 13
officers, 171-73
parachurch &, 50-51
pluralism &, 6
preaching, 6
productivity?, 106-8
protection by, 107
responsibility, 17
retreat of, 79

Rushdoony (see Rushdoony:

church)

sacramental, 98-99, 107

sanctions, 10, 11, 46-47,
104-11, 126

shape up !, 13

sovereignty, 8, 28, 104

sovereignty (primary), 60-
61

splits, 71, 73

standards, 7

State &, 66, 67-68, 69-71

stealing it, 29-30

storehouse, 5, 26

subversion of, 28

tax exempt, 71-72

tax exemption, 67-68, 79

tax-immune, 67-68

timid, 22, 75-76

tithe &, 3, 4, 22 (see also
tithe)

unproductive?, 103

voluntarism, 6-7, 14, 176

voting, 29-30

Welfare State &, 43

195

church membership
democratic, 28
Lord’s Supper, 38
pastors, 38
screening, 38
Social club?, 42
standards, 35-40
two-tiered, 42
churchianity, 101
circumcision, 32, 85
citizen (second class), 16, 26
citizenship, 32
clans, 96, 177
colleges, 28, 30
commission structure, 19-22
common grace, 10
common law, 146
Communism, 33, 165-66
con artist, 20-21
confessions, 38-39, 42
confirmation, 35
conservative, 94-98
Constitution, 97, 105n, 120
contracts, 14
Coray, Henry, 151
Couch, W. T, 151
Council of Constantinople,
174-75
covenant
breakers, 23
church, 6
creed, 38-39
dominion, 102-3
faithfulness, 26
hierarchy, 16
institutions, 78
Kline &, 24
Mosaic, 2, 12-13

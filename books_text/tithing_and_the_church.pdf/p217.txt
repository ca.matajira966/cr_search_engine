Index

taxes
acceptance, 22
allocation, 131-32, 143-44
autonomy &, 63
exemption, 67-68
immunity, 64
Judicial supremacy, 65
ministerial, 132
revolt, 79
sovereignty, 78
sovereignty &, 61
tithe &, 5, 62
tyranny, 22

telegrams, 54-55

temple, 68, 88, 98, 139

theft, 87

Thoburn, David, 156-60

Thoburn, Dolly, 163

Thoburn, Dolly (Flanagan),
156-60

tithe
Abraham &, 4, 42
allocation, 130-32, 142
authority, 88-89
autonomy &, 25
Calvin on, 137-38
charity, 128
church &, 3, 22
commission, 19-22
covenant &, 4, 53
dominion, 103, 123
duty, 16
excommunication, 123
guardians, 128
increase, 124
Israel, 15
leadership &, 184
Levites, 15, 27, 87-88, 128

203

Lord’s Supper, 14
mandatory, 75-76
ministerial, 132
Mosaic?, 2
oath &, 105
payer, 22-23
productivity, 103
rejection, 2
resentment, 21-22
Rushdoony on, 127
sacraments, 98-99, 123, 168
sacraments &, 4, 79, 83
sanctions, 46-47
sanctions &, 11
Satan’s, 20-21, 22
sovereignty, 78-79, 91
sovereignty’s mark, 3
storehouse, 5
tax or, 62
taxes &, 5
theft &, 87
unpaid, 12
voluntarism, 5-7, 15
voting, 40, 43
wealth &, 15-16

tools, 16

touchstones, 63

Troy, 98

Twist, Oliver, 49, 53

tyranny, 22, 61, 63, 69-70

Van Til, Cornelius, 25
Volker Fund, 151
voluntarism, 5-7, 14, 15, 50
voting (see franchise)

wallet, 23, 28
wallets, 6

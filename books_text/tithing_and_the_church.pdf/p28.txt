14 TITHING AND THE CHURCH

person holds? I will tell you. Postmillennialists are not persuad-
ed that the present “down market” in the number of converts
is permanent; pessimillennialists are persuaded. This means
that their eschatology reinforces “buyers’ market” mentality. It
also affects their churches’ discipline: gutting it.)

There was a time, three centuries ago, when Christians
believed that there are only three ways out of the church:
death, excommunication, and letter of transfer. They no longer
do. Excommunication is old fashioned. Letters of transfer only
carry weight when receiving churches sanction them, rejecting
the visitors’ request for membership, if only for the sake of
creating respect for their own letters of transfer. But in a buy-
ers’ market for voluntary donations, churches are rarely choosy.
They have become beggars. Beggars can’t be choosers.

Conclusion

The churches no longer hold the hammer. They dropped it
over a century ago. Why? Because they applied the philosophy
of nominalism to the church itself: a world of contracts, not
binding covenants under God. When Holy Communion became
in most Protestants’ thinking a mere memorial, the church
covenant became a contract in their thinking.

The sacrament of the Lord’s Supper is no longer taken
seriously. While the following development may not be predict-
able in every instance, it is familiar enough to be considered
highly probable. When weekly communion goes to monthly
communion, and monthly communion goes to quarterly com-
munion, and grape juice is substituted for wine, tithes become
offerings. Nominalism undermines tithing because nominalism
undermines men’s fear of church sanctions: faith in God’s
predictable covenantal sanctions in history whenever church
and State fail to enforce His law by means of the law’s mandat-
ed sanctions.

When the churches stopped preaching the mandatory tithe,
the State adapted the idea and multiplied by four: taxes.

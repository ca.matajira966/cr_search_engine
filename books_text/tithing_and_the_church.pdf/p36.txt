22 TITHING AND THE CHURGH

to believe that land and labor arrived by way of eons of cosmic
evolution. Many of them do believe this. They do not see them-
selves as indebted to God. They do not see themselves as God’s
sharecroppers. So, they look at the 90-10 arrangement and do
not conclude: “The greatest commission structure in history!”
Instead, they conclude: “God is trying to get into my wallet.”

Who Lawfully Collects the Tithe?

The civil magistrate collects taxes. Paul identifies him as
God’s minister (Rom. 13:4). He is collecting taxes in God’s
name, whether he names God or not. God has ordained him,
He is a subordinate to God. In his capacity as the representa-
tive of God to men through the State, he lawfully collects taxes.
Men complain about today’s level of taxation, as well they
should - it constitutes tyranny (I Sam. 8:15, 17) — but they
rarely rebel. They do not blame God. They accept their burden
as members of a democratic political order. They fully under-
stand that they do not possess the authority as individuals to
determine where their tax money should go. They dutifully pay
the tax collector.

Then who lawfully collects the tithe? The minister of God.
But this minister is not a civil officer; he is an ecclesiastical
officer. He comes as God's designated, ordained agent and
insists on payment. That is, he should do this. In fact, he is too
timid to do this in our day. Why? Because he has adopted — or
at least acceded to - a modified view of Satan’s offer: “Pay
whatever seems fair to you. God has no legal claim on ten per-
cent after business expenses.”

This outlook transfers authority over the distribution of the
tithe to the tithe-payer. This transfer of authority is illegitimate
for two reasons. First, the giver defines the tithe’s percentage as
he sees fit, but somehow this figure is usually less than ten
percent. Second, he reserves to himself the authority to distrib-
ute this tithe to those organizations that he approves of. This
violates God’s system of hierarchical authority. The tithe-payer

Church Membership and the Tithe 35

Two-Tiered Church Membership: Boundaries

The reigning philosophy of mass democracy has captured
the minds of most Protestant Christians. They have structured
their churches so as to avoid any criticism of being “undemo-
cratic.” The problem for non-hierarchical churches is that there
is now no effective way to keep new members from exercising
church authority. Unlike the Roman Catholics, Episcopalians,
and other denominations where bishops are in authority and
whose top-down hierarchical structures serve as barriers against
institutional capture by those not approved by the bishops,
independent churches and Presbyterian churches face this
problem every time a visitor decides to join. Only because the
local church in our day is so weak, ineffective, underfunded,
and culturally impotent can it temporarily defer dealing with
the problem of the “naturalized immigrant.”

Sacramental Boundaries

Baptism is correctly seen as analogous to Old Testament
circumcision by most churches. Baptized adults are usually
granted church membership. Communion is another problem.
While the Lord’s Supper is vaguely understood as analogous to
the Passover, very few churches really acknowledge the full
extent of this Passover-communion link.

Any circumcised male could attend Passover (Ex. 12:48), but
not every baptized individual is allowed to take communion in
today’s church. The modern church has erected a major barrier
to full participation in the life of the church. Some churches
require children to be a certain age before partaking in the
Lord’s Supper. Other churches require “confirmation” of teen-
agers. Still others restrict baptized adults from the Lord’s Sup-
per until they have gone through some sort of introductory
theology class,

Such restrictions were not imposed on circumcised believers
under the Mosaic Covenant. There was almost no way for a

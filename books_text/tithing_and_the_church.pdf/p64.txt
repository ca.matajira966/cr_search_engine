50 TITHING AND THE CHURCH

Church and Parachurch

Debt-ridden, tight-fisted churches have become the mone-
tary black holes of Christendom: money that goes in stays in.
This has led to the rise of the parachurch ministries. In earlier
days, these ministries supplemented the work of the churches.
‘Today, they have too often replaced the work of the churches.
And much of the blame rests on the churches.

Pastors often complain about parachurch ministries, with
good cause. These rival ministries absorb donors’ tithe money,
but they are not accountable to any organization, say the pas-
tors (frequently pastors of local, autonomous churches that they
run personally). But they have a good point about institutional
accountability or lack thereof: he who pays the piper calls the tune.
The donors to parachurch ministries provide the economic
votes of confidence that sustain these ministries. Giving within
the churches is supposedly voluntary. Pastors therefore do not
preach or enforce the tithe. Thus, the church becomes just one
more beggar among many, like Oliver Twist.

Parachurch ministries have accepted the reality of voluntar-
ism, and have adopted scientific fund-raising techniques that
local pastors cannot successfully mimic. This places churches at
a disadvantage in the begging profession.

If the churches would demand the tithe from their voting
members, parachurch ministries would see their funds begin to
dry up. Then the churches could begin to support those para-
church ministries that perform kingdom services that are diffi-
cult for the churches to perform. The churches would thereby
invoke the division of labor (I Cor. 12). This would better pro-
mote the kingdom of God, and it would also put churches back
into positions of authority.

He who pays the piper calls the tune. The reason why almost no
one plays tunes that the church wants to hear is that the church
refuses to pay the highly competitive pipers of this world. It
decries the lack of accountability of other ministries, yet it refus-
es to insist on accountability from its own members. “We're

Maimonides’ Code: Is It Biblical? 1001

either in the U.S. or in the state of Israel today," the average Chris-
tian has no idea. Few Christians have heard that there is a third
branch, Oriental or Yemenite Judaism (North African), members of
which have long complained that they are discriminated against
politically in the state of Israel.

Christians are unaware that the medieval Jewish body of litera-
ture known as the Kabbalah (“tradition”) is not only mystical but
closely tied to numerology and occultism.'* They do not know that
the mystical-magical tradition of the Kabbalah had its roots in the
Talmud.’ They have never read anything about the history of Zion-
ism, either pro’ or con.!”

To the extent that the Bible-believing Christian thinks about
Reform Jews generally, he assumes that they are something like
Unitarians: politically liberal, skeptical about the Bible, and essen-
tially humanistic. (Orthodox Jews also view Reform Jews in much
the same way.) Christians, however, tend to think of almost all Jews
in this way, which turns out to be a statistically correct political

13, I refer to the “state of Israc]” rather than “Isracl” out of respect for the terrni-
nology of Orthodox Jews, who sharply distinguish the two,

14. “Kabalah,” in Lewis Spence (ed.), An Encyclopaedia of Occultism (New Hyde
Park, New York: University Books, [1920] 1960). An example of popular (though
underground) magical literature based on the Kabbalah, which has been reprinted
generation after goneration, is ‘The Sixth and Seventh Books of Moses. Sec also Arthur
Edward Waite, The Holy Kabbalah: A Study of the Secret Tradition of Israrl (New Hyde
Park, New York: University Books, 1960 reprint); Denis Saurat, Literature and Oceult
‘Tradition, trans. Dorothy Bolton (Port Washington, New York: Kennikat, [1930]
1966), Pt. HEI, ch. 2. The pioneering modern Jewish studies of the Kabbalah arc by
Gershom G. Scholem: Major Tiends in Jewish Mysticism (3rd ed; New York:
Schocken, 1961) and On the Kabbalah and Its Symbolism (New York: Schocken, [1960]
1965). The primary source of Kabbalah is The Zohar, 5 vols. (London: Soncino
Press, 1934). On the influence of the Kabbalah on the gentile world, sce Frances A.
Yates, The Occult Philosophy in the Elizabethan Age (London: ARK, [1979] 1983) and
A.E. Waite, The Brotherhood of the Rosy Cross (New Hyde Park, New York: University
Books, 1961 reprint).

15. Gershom G, Scholem, Jewish Gnosticism, Merkabah Mysticism, and Talmudic
Tradition (2nd ed.; New York: Bloch, 1965).

16, Walter Laqueur, A History of Zionism (New York: Holt, Rinchart & Winston,
1972); Ronald Sanders, The High Walls of Jerusalem: .A History of the Balfour Declaration
and the Birth of British Mandate for Palestine (New York: Holt, Rinehart & Winston,
1983).

17. Gary V. Smith (ed.), Zionism: The Dream and the Reality, A Jewish Critique (New
York: Barnes & Noble, 1974); Rabbi Elmer Berger, The Jewish Dilemma: The Case
Against Zionist Nationalism (New York: Devin-Adair, 1945). The major published
English-speaking critic of Zionism is Alfred M. Lilienthal: What Price Israel?
(Chicago: Regnery, 1953); There Goes the Middle Fast (New York: Devin-Adair, 1957);
The Zionist Connection: What Price Peace? (New York: Dodd, Mead, 1978).

 

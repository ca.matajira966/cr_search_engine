What Are the Case Laws? 93

1:8). He placed the Hebrews in permanent slavery. He attempted to
replace the God of the Bible as the sovereign lord of the Hebrews. As
a self-proclaimed divinity, Pharaoh asserted the right to rule over
them without answering to the God of Abraham. Thus, the early
chapters of Exodus are devoted to the story of God’s subordination of
Egypt to Himself through the judgment of cursing— plagues, death,
and military defeal—and the subordination of Israel to Himself
through the judgment of blessing: their deliverance from bondage.”
The old order was marked by the Hebrews’ enslavement by
Pharaoh; the new order was to be marked by their service to God.

Exodus is the Bible’s premier “book of the covenant” (Ex, 24:7).
The Book of Exodus is itself structured in terms of the five-point cov-
enant model. First, transcendence: Who is the sovereign God over
nature and history, God or Pharach? Answer: the Creator God who
delivered His people from Egypt (chapters 1-17). Second, hierarchy:
What is the proper mode of judicial organization that reflects Gad’s hi-
erarchical chain of command over His people? Answer: a bottom-up
appeals court structure (Ex. 18). Third, what are the laws by which
God governs mankind and God’s authorized representatives govern
the covenantal institutions of family and civil government? Answer:
the Ten Commandments (Ex. 20) and the case laws (Ex. 21-23:43).
Fourth, how is the covenantal oath between God and His people mani-
fested? Through a covenant meal (Ex. 23:14-19), What is the judg-
ment that God brings on those who rebel against Him? National
destruction: deliverance into the hands of the enemy (Ex. 23:20-33).
Exodus 24 records the covenantal oath that Israel made with God.
“And Moses came and told the people all the words of the Lorp, and
all the judgments: and all the people answered with one voice, and
said, All the words which the Lorp hath said will we do” (v. 3). Fifth,
what is the sign of inheritance or continuity? Answer: the tabernacle
that will go with them through the wilderness, and then into the
promised land, Its blueprint appears in Exodus 25-31; its actual con-
struction is described in Exodus 35-40.

Exodus 32-33 deals with Israel's rebellion with the golden calf
and God's judgment of them, a recapitulation of Adam’s Fall. In Ex-
odus 34, God re-establishes Israel’s covenant with Him, with Moses
acting as the representative or intermediary in this hierarchy. Ex-
odus 34 is therefore a section on covenant renewal, an aspect of histori-
cal continuity.

10. Gary North, Moses and Pharaoh: Dominion Religion us. Power Religion (Tyler,
Texas: Institute tor Christian Economics, 1985).

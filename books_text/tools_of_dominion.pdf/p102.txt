94 TOOLS OF DOMINION

The second reason why slavery becomes the initial focus of con-
cern in the case laws is that the Israelites had just been delivered out
of bondage. The whole book deals with thé theme of deliverance
from bondage into sabbath rest.!’ Thus, having just been delivered
from slavery, God caught their attention by beginning the case law
section with laws governing servitude. He confronts people “right
where they are” in life. Where the Israelites were was in the wilder-
ness, in transition spiritually and culturally from Pharaoh’s slavery to
God’s servitude. Biblical servitude is one of God’s authorized modes
of transition from wrath to grace (blessing), both personally and cul-
turally. Pagan slavery, in contrast, is one of God’s ethically unau-
thorized but historically imposed modes of transition from grace to
wrath (cursing) for His people: bondage in Egypt, Assyria, and
Babylon. Becoming a slave-master over God’s people is prohibited,
yet God raises up such tyrants as a form of judgment against His
people and the tyrants themselves (Jer. 25). What the New Testa-
ment says of Judas applies to slave-masters generally: “And truly the
Son of man goeth, as it was determined: but woe unto that man by
whom he is betrayed!” (Luke 22:22).

Liberals Protest

Because the case law section begins with bondservice, liberal
scholars are immediately repulsed by it. In general, they react nega-
tively to biblical law as a whole. It is not that they ignore the law.
Liberal theologians have produced a large number of detailed
studies of Old Testament law. What is notable about these academic
studies is their almost self-conscious uselessness. Specialized schol-
arly journals in the field of Old Testament studies have been created
by the dozens to serve as outlets for essays so narrow in focus, so ir-
relevant in conclusions, and so boring in style that not even pub-
lishers of scholarly books are willing to print them. The extent of the
uselessness of these highly rarified, heavily footnoted studies cannot
readily be appreciated by the average Christian, who reads his
Bible, and then does his best to take its teachings seriously. Even in
the world of formal academic scholarship, which specializes in the
production of painstakingly documented irrelevance, Old Testament
scholars are regarded by their colleagues as highly specialized,
multi-lingual masters of useless historical details and even more

11, Jordan, Law of the Covenant, p. 75.

1024 TOOLS OF DOMINION

Maimonides intends to explain forbids that they be explained in
public, or to the public; they may only be explained in private and
only to such individuals as possess both theoretical and political wis-
dom as well as the capacity of both understanding and using allusive
speech; for only ‘the chapter headings’ of the secret teaching may be
transmitted even to those who belong to the natural elite. Since
every explanation given in writing, at any rate in a book, is a public
explanation, Maimonides seems to be compelled by his intention to
transgress the Law.”®? Maimonides was quite forthright about this
need for secrecy:

For my purpose is that the truths be glimpsed and then again be concealed,
so as not to oppose that divine purpose which one cannot possibly oppose
and which has concealed from the vulgar among the people those truths
especially requisite for His apprehension. As He has suid; The secret of the
Lord tx with them that fear Him Ps, 25:14], Know that with regard to natural
matters as well, it is impossible to give a clear exposition when teaching
some of their principles as they are. For you know the saying of [the Sages],
may their memory be blessed: The Account of the Beginning ought not to be taught in the
presence of tooo men [Babylonian Talmud, Hagigah, 1b]. Now if someone ex-
plained all those matters in a book, he in effect would be éeaching them to
thousands of men. Hence these matters too occur in parables in the books
of prophecy. The Sages, may their memory be blessed, following the train of these
books, likewise have spoken of them in riddles and parables, for there is a

 

close connection between these matters and the divine science, and they too
are secrets of that divine science.”

In speaking about very obscure matters il is necessary to conceal some
parts and to disclose others. Sometimes in the case of certain dicta this ne-
cessity requires that the discussion proceed on the basis of a certain prem-
ise, whereas in another place necessity requires that the discussion proceed
on the basis of another premise contradicting the first one. In such cases the

 

90. Strauss, “How to Begin to Study Phe Guide of the Perplexed,” in Maimonides,
Guide of the Perplexed, p. xiv. Strauss argues that Maimonides overcame this restriction
by adopting literary techniques that made the Guide itself a secret writing: p. vx. It
was Maimonides’ ernphasis on secrecy and rigorous writing that influenced the Jewish
political theorist Strauss and his followers, of whom Pines is one. Political philaso-
pher and former U.S. Senator John P. East insisted that Strauss “cast himself in the
role of a modern Maimonides”; this can be seen in Strauss’ book, Persecution in the Art
of Writing (Westport, Connecticut: Greenwood, [1952} 1973). Gf, John P. East, “Leo
Strauss and American Conservatism,” Modern Age, XXI (Winter 1977), p. 7; Archie
P, Jones, “Apologists of Classical Tyranny: An Introcuctory Critique of Suraussian-
ism,” Journal of Christian Reconstruction, V (Summer 1978), pp. 112-14.

91. Maimonides, Guide 3b-4a; pp. 6-7.

1062 TOOLS GF DOMINION

cial revolution without a change in a particular society's legal order;
without such a transformation, a so-called revolution is merely a coup
détat. It takes more than one generation to consolidate a revolution,
and the primary manifestation of this consolidation is always legal.”
If it is true, as Berman believes, that we are approaching the end of
an era,” then it is incumbent on Christians to begin to rethink their
covenantal heritage. They must begin to offer an alternative to the
present collapsing social order, and this alternative must be self-
consciously judicial, Christians must become judicial revolutionar-
ies, not simply defenders of the present legal order.2° If we remain
on the deck of this sinking ship, claiming that it is in principle con-
formable with biblical principles, we shall go down with it.24° Stick-
ing with the status quo means sure death by drowning.

207. Ibid., p. 20.

208, ibid. p. v.

209. Gary North, When Justice Is Aborted: Biblical Standards far Non-Violent Resistance
(Ft. Worth, Texas; Dominion Press, 1989).

210. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Feonomics, 1989).

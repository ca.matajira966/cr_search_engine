The Hoax of Higher Criticism 1067

post-war era, biblical criticism was an important aspect of Protestant
colleges and seminaries. No longer. “Given a predominant concern
with the present and its seemingly urgent practical problems, which
claim almost exclusive attention,” he writes, “historical criticism and
exegesis have come to take very much a back place.”"

Burying the Dead

Why, then, should I devote an appendix to this topic? Because of
a parallel process: while modern humanism has visibly begun to
fragment, taking with it modern liberal theology, there has been a
recovery of interest within the evangelical world of real-world ques-
tions that are best summarized under the general heading, “Chris-
tian worldview.” The implicit dualisms of modern fundamentalism —
Old Testament vs. New Testament, law vs. grace, letter vs. spirit,
church vs. state, Israel vs. the church, eternity vs. history, heart vs.
mind, dominion vs. rapture, culture vs. kingdom — have begun to be
either discarded or at least seriously criticized from within the camp. '?
The Christian world’s recovery of a vision of ethical unity, of a com-
prehensive world-and-life view, is basic to any workable strategy of
Christian reconstruction, In this intellectual and emotional process
of recovering Christianity’s lost unity of vision, we are required to
return to the original source of the problem: men’s loss of faith in the
unity of God’s Word.

There is an old political slogan, “You can’t beat something with
nothing.” Throughout the twentieth century, the Christian world has
found itself in the position of battling something — self-confident hu-
manism— with nothing: a philosophy of ethical dualism, a kind of
Christian gnosticism.!* This was obvious to everyone after the Scopes’
“monkey” trial of 1925.!4 (In the early church, this dualistic philoso-

H, fbid., p. t.

12. On the Israel-church dichotomy, see William E. Bell, A Critical Evaluation of
the Pretribulation Rapture Doctrine in Christian Eschatology (Ph.D dissertation,
New York University, 1968). See also John F. MacArthur, The Gospel According to
Jesus (Grand Rapids, Michigan: Zondervan Academie, 1988). This book sold aver
100,000 copies in hardback within a year of ils publication. The survival of the older
dualism is best represented by Dave Hunt, Whatever Happened to Heaven? (Eugene,
Oregon: Harvest House, 1988).

13. Douglas W. Frank, Less Than Conquerors: How Evangelicals Entered the Twentieth
Century (Grand Rapids, Michigan: Eerdmans, 1986)

14, George Marsden, Fundamentalism and American Culture: The Shaping of Twentieth-
Century Evangelicalism, 1470-1925 (New York: Oxford University Press, 1980), ch, 10:
“The Great Reversal.”

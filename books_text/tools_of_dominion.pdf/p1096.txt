1088 TOOLS OF DOMINION

A Fork in the Road

We know that in making any decision, we must forfeit many
things that might have been but will never be— indeed, a whole life-
time of things that might have been—but we never know exactly
what. Every decision, moment by moment, is to some extent the
proverbial fork in the road, We cannot predict the next twenty
moves and counter-moves in a chess game— moves that will become
reality in part because of the next move —so it is safe to say that we
cannot know what life has in store for us because we do one thing to-
day rather than another,

If you read this essay, it is because you think it will be “worth
your time.” But what is your time worth? What is your time worth
right now? It is worth whatever is the most valuable use to which you
can put it. What is the cost of spending your time one way rather
than another? The most valuable use foregone. So, what is your de-
cision? “To read or not to read, that is the question!”

Decisions, decisions. Once our decision is made, we put the past
irrevocably behind us. “The moving finger writes, and having writ-
ten, moves on.” We then face the consequences of our decision. But
these consequences — these costs—are imposed on us after the deci-
sion, not before. They are costs, but they are not costs that affected
the original action. Expected costs affected the original action, not the
actual costs that we in fact subsequently experience. Is this unclear?
Ask the person who married the “wrong” spouse to explain the differ-
ence between expected costs and resulting costs. Economist James
Buchanan distinguishes between two kinds of costs: choice-influencing
costs and choice-influenced costs.*

Unmeasurable Costs

Choice-influencing costs are inherently unmeasurable by any
scientific standard. The economist insists that, like beauty in the
eyes of the beholder, these economic costs exist only in the mind of
the decision-maker. They are subjectively perceived, and only subjec-
tively perceived. And yet, and yet... there really are beautiful
women and ugly women, and just about everyone can discern the
difference, including the respective women (especially the women).

 

4, James Buchanan, Cost and Choice: An Inquiry in Economic Theory (University of
Chicago Press, 1969), pp. 44-45. Buchanan won the Nobel Prize in cconomics in
1986.

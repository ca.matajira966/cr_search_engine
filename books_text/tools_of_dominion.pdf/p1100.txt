1092 TOOLS OF DOMINION

decision. . . . cost cannot be discovered by another person who eventually
watches and records the flow of those things along those channels.” Then of
what objective use are accountants? Why was the advent of double-
entry bookkeeping such a revolutionary event in the history of civili-
zation?!? He does not say.

Furthermore, what does such a view of budgeting do to the idea
of the free market as a social institution for producing economic or-
der — objective economic order? What does such a view do to the idea
of the stock market, since money prices for shares are the means by
which decision-makers evaluate the past performance of all other
participants in the market? What does the price of a share of corpor-
ate stock have to do with expected future performance of that corpor-
ation’s management? What is the link, if any, between present share
prices and future economic performance? How do we get from sub-
jective value to objective share prices and back again? How do we
preserve our capital? For that matter, how do we measure our capi-
tal? How can we bridge the gap between the world of purely subjec-
tive costs and objective market prices? Buchanan insists: “Only prices
have objective, empirical content. . . .”*% Then precisely what empirical
content does a price possess or reveal, and how do we discover it
or make effective use of it— subjectively and objectively, personally
and socially?

In short, what does an objective price have to do with individual
subjective value? What is the economic meaning of a price — individ-
ually and socially, subjectively and objectively? (This is the number-
one epistemological problem that has beset modern economics since
the 1870's.)

The Realm of Possibility

Another example; Buchanan makes this statement: “Any profit
opportunity that is within the realm of possibility but which is rejected
becomes a cost of undertaking the preferred course of action.”"* But
Buchanan neglects any consideration of the economics of a rejected
opportunity that is not in fact — objective fact— within the realm of pos-
sibility. We normally call such an opportunity a /oss. Wouldn’t avoid-

11, Thitlby, “Subjective Theory,” ibid., p. 31

12. Ludwig von Mises, Human Action: A Treatise on Economics (3rd ed; Chicago:
Regnery, 1966), p. 230.

13. Buchanan, Cost and Choice, p. 85.

14. Ibid, p. 28.

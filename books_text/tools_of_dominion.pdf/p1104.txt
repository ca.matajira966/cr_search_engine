1096 TOOLS OF DOMINION

There are no measures at all. There may be discrete, permanent
numbers — even this is highly speculative” —but there are no mea-
sures, Everything is on a continuum, nothing is discrete.?* This
absence of measures leads, step by step, to radical subjectivism and
radical relativism. Heraclitus’ river of flux is at last definitively erod-
ing Parmenides’ fixed shore line. Chaos looms.”

Having said this, the economist nevertheless resists making the
obvious conclusion regarding the relativity of all measurement: the
denial of the possibility of relevant sctentific precision. Vainly, he protests:
“There are economists who have propounded the relativity of mea-
sure. Apparently, they failed to see that this view saps the entire
foundation upon which the economic science rests.”® Sap! He, too, is
inescapably one of these epistemologically short-sighted economists.

Consider the question of environmental pollution. The consistent
cconomist — an exceedingly rare creature— must conclude: “One man’s
polluted stream is another man’s profit for the fiscal year, and there is no
conceivable scientific way to say which is better for society in general,
for there is no scientific way of identifying such an entity as society in
general.” To admit this, however, would be to commit methodologt-
cal suicide in public. Modern economics has in fact committed sui-
cide, but it has done so in private. Economists do not leap from tall
buildings during the lunch hour. They much prefer to do away with
themselves in private— through an overdose of qualifications.

The Great Debate

In The Dominion Covenant: Genesis, I discussed the problem of ob-
jective and subjective value at considerable length. I analyzed the
important critique of Cambridge Professor A. C. Pigou by London
School of Economics Professor Lionel Robbins, and then the subse-
quent debate between Robbins and Roy Harrod.*! To review very
briefly, Pigou, in his pioneering study of welfare economics, had
argued that since each additional monetary unit’s worth of income is
worth less to a man than the previous unit, the value of one addi-
tional unit of income to a millionaire will necessarily be less than its

27. Poythress, “A Biblical View of Mathematics,” op. cit.

28. Nicholas Georgescu-Roegen, The Entropy Law and the Economic Pracess (Cam-
bridge, Massachusetts: Harvard University Press, 1971), ch. 3.

29. James Gleick, Chaos: Making a New Science (New York: Viking, 1987).

30. Georgescu-Roegen, Fnimpy Law, p. 11.

31. Gary North, The Dominion Covenant: Genesis (Ayler, Texas: Institute for Chris-
tian Economics, 1982), ch. 4.

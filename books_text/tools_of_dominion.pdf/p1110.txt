102 TOOLS OF DOMINION

had given the problem of social cost its traditional framework. This
discussion was categorized under the general rubric of “externalities.”
The term refers to the imposition of a firm’s costs of operation on
those who are not owners of the stream of future income generated
by the production process. In other words, these victims are external
to the firm or production unit, but not external to its costs of opera-
tion. Almost without exception, the economists’ discussion of exter-
nalities ended with a consideration of what government measures
are appropriate to reduce or eliminate these externalities. The con-
clusions reached by most economists, based on Pigou’s analysis in
The Economics of Welfare (4th ed., 1932; originally published in 1920),
were as follows, Coase summarized: the producer of pollution
(smoke, noise, etc.) should 1) pay damages to those injured, or 2)
have a tax imposed on his production by the civil government, or 3)
have his factory excluded from residential districts.°° Coase’s article
broke with this tradition.

Aaron Levine summarizes Coase’s theoretical breakthrough:
“Assuming zero transaction costs and economic rationality, Coase, in
his seminal work, demonstrated that the market mechanism is capa-
ble of eliminating negative externalities without the necessity of gov-
ernmentally imposed liability rules.”*? Furthermore, the theorem
leads to the conclusion that “if transactions are costless, the initial as-
signment of a property right will not determine the ultimate use of
the property.” Free market economists of the “Chicago School”
have increasingly sided with Coase. (What is also rather startling is
that traditional Jewish law had adopted the basic features of the
Coase theorem many centuries earlier; English law had not.)°?

The problem is, of course, that there are and always wiil be transac-
tion casts.®¢ Or, T should say, this is a problem. The major problem is
that this theorem assigns zero economic value—and therefore zero
relevance—to the sense of moral and legal right associated with a
willful violation of private ownership. It ignores the economic rele-

56. Coase, “Social Cost,” p. 1.

57, Aaron Levine, Free Enterprise and Jewish Law: Aspects of Jewish Business Ethics
(New York: Ktav Publishing House, Yeshiva University Press, 1980), p. 59.

58. Posner, Economic Analysis of Law, p. 7.

59. Yehoshua Liebermann, “The Coase ‘Theorem in Jewish Law,” Journal of Legal
Studies, X (June 1981), pp. 293-303.

60. For a brief introduction to the question of transaction costs, see Oliver E.
Williamson, “Transaction-Cost Economies: The Governance of Contractual Rela-
tions,” Journal of Law and Economics, XXII (October 1979), pp. 233-61.

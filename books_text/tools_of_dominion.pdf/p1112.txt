1104 TOOLS OF DOMINION

theorem. Richard Posner, an economist and a judge in the U.S. Ap-
peals Court (Seventh Circuit), admits that the Coase theorem applies
only to zero transaction cost situations, yet he has devoted much of
his acadernic career to pursuing the economic implications of the
Coase theorem in the field of law. He knows that Coase’s initial as-
sumption—that transaction costs are zero—cannot be true in the
real world. Posner writes:

The economist does not merely decree that absolute rights [of owner-
ship—G.N.] be created and then fall silent as to where they should be
vested. To be sure, if market transactions were costless, the economist
would not care where a right was initially vested. The process of voluntary
exchange would costlessly reallocate it to whocver valued it the most. But
once the unrealistic assumption of zero transaction costs is abandoned, the
assignment of rights becomes determinate. If transaction costs are positive
(though presumably low, for otherwise it would be inefficient to create an
absolute right), the wealth-maximization principle requires the initial
vesting of rights in those who are likely to value them most, so as lo
minimize transaction costs. This is the economic reason for giving a worker
the right to sell his labor and a woman the right to determine her sexual
partners. If assigned randomly to strangers, these rights would generally
{not invariably) be repurchased by the worker and the woman; the costs of
the rectifying transaction can be avoided if the right is assigned at the outsct
to the uscr who values it most.*!

Posner openly admits that in some cases, even where transaction
costs are low, the worker or the woman in his example would not
(i.e., could not afford to) repurchase these rights of ownership. This
follows from his definition of value: “The most important thing to
bear in mind about the concept of value is that it is based on what
people are willing to pay for something rather than on the happiness
they would derive from having it... . The individual who would
like very much to have some good but is unwilling or unable to pay
anything for it— perhaps because he is destitute — does not value the
good in the sense in which I am using the term ‘value.’ ”° The con-
clusion is obvious, and he does not hesitate to draw it: “Equivalently,
the wealth of society is the aggregate satisfaction of those preferences

61. Posner, Economics of Justice, p. 71. For a critique of Posner’s approach to the
law, see Buchanan, “Good Economics— Bad Law,’ Virginia Law Review, op. cit. See
also the biting and incisive essay by Arthur Allen Leff, “Economic Analysis of Law:
Some Realism About Nominalism,” ibid., pp. 451-82.

62. ihid., pp. 60, 61.

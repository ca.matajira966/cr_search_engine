1110 TOOLS OF DOMINION

tional economic losses when the civil government refuses to make catile owners
responsible for the damage their animals cause, Both assumptions are im-
plicit to Coase’s thesis, and both are categorically incorrect. Coase
begins with an unreal world in which transaction costs are defined
away, and from this he draws his equally unrealistic conclusions.”*

I say that his conclusion initially appears to be correct—that ina
zero-cost world, the outcome of the bargaining process would be the
same, the value of cattle vs. the value of crops. Yet in a perceptive
essay by Donald Regan, we learn that Coase has no warrant for
making this conclusion. Coase assumes that the bargaining process
will produce the same economic results, but why should he? Regan
says that Coase offers no model of how this bargaining process would
inevitably produce such identical results in the absence of specified
and enforceable property rights. For example, sometimes a bar-
gainer makes economic threats of non-cooperation that must be oc-
casionally enforced in order to persuade the other party that he
should take such threats seriously, even if the actual carrying out of
the threat may injure the threat-maker in the short run. How does
Coase know what the short-run or long-run outcome of a bargaining
process will be? He doesn’t.” This is simply another way of saying
that we cannot confidently make social and economic evaluations of
real-world events by abstracting economic theory from temporal
reality —i.e., by creating a mental world in which there are no costs,
no ignorance of present or future opportunities, and no need of
threats to achieve our goals.

Coase states clearly what he thinks the economic problem is,
“The economic problem in all cases of harmful effects is how to max-
imize the value of production.” Furthermore, he is no fool. Later in
the essay, he drops his essay’s initial assumption of zero transaction

74, Writes Jules L. Coleman: “No term in the philosopher’s lexicon is more
imprecisely defined than is the cconomist’s term ‘transaction costs.’ Almost anything
counts as a transaction cost. But if we are to count the failure to reach agreement on
the division of surplus as necessarily resulting from transaction costs (I have no
doubt that sometimes ii does), then by ‘transaction cost’ we must mean literally any~
thing that threatens the efficiency of market exchange. In that case, it could hardly
come as a surprise that, in the absence of transaction costs so conceived, market ex-
change is efficient.” Golernan, “Economics and the Law: A Critical Review of the
Foundations of the Economic Approach to Law,” Ethics, 94 (July 1984), p. 666.

75. Donald H. Regan, “The Problem of Social Cost Revisited,” journal of Law and
Economies, XV (October 1972), pp. 428-32.

76. Coase, “Social Cost,” p. 15.

The Epistemological Problem of Social Cost Mit

costs, perfect competition, and zero information costs.”’ Of course in
real life there are transaction costs to settle disputes. For this reason,
there is a role for civil government in settling costly disputes.” “All
solutions have costs,” including solutions imposed by the civil gov-
ernment.” But one underlying presupposition distorts all of Coase’s
analysis—a presupposition which is all too common (and unstated)
in Chicago School economic analysis: the legitimacy of leaving aside
issues of right and wrong, of justice, of equity. “Of course, if market
transactions were costless, all that matters (questions of equity
apart) is that the rights of the various parties should be well-defined
and the results of legal actions easy to forecast.”® Problem: How can
we discuss “the rights of the various parties” if we leave aside ques-
tions of equity— questions of right and wrong? In short, how can we
discuss “rights” apart from what is right?

Discounting Moral Outrage to Zero

Questions of equity apart: here is a continuing assumption in the
“value-free, morally neutral” economic hypotheses of modern free
market economists. They apparently think that questions of equity,
being questions of opinion and morality, cannot be dealt with scien-
tifically, nor can economists, as scientists, put a price tag on viola-
tions of moral principle. They conveniently ignore the inescapable
conclusion of subjectivist economics and methodological individ-
ualism, namely, that there is no scientific way to “measure” costs and
benefits of any kind, since interpersonal comparisons of subjective
utility are impossible for mortals to make. They naively believe that
there is a neutral, value-free science of economics, but not of moral-
ity. They are correct about the impossibility of neutral morality; they
are incorrect about the existence of a value-free economics. Econom-
ics deals with value, and there is no value-free value. The moment

77. There is always the nagging suspicion that once these formal theoretical
assumptions are dropped, the whole intellectual performance becomes nothing mare
than a scholarly puzzle game. Will any of the conclusions concerning the world of
the theoretical model stil] remain accurate, let alone applicable, once we begin to
discuss the empirical world? And how can we know for sure? Only through intuition
—a nonrational, nonlogical category. See Gary North, “Economics: From Reason
to Intuition,” in North (ed.), Foundations of Christian Scholarship, op. cit. See also
North, Dominion Covenant: Genesis, pp. 350-53.

78. Coase, “Social Cast,” pp. 13-19.

79, Ibid., p. 18.

80. Zbid., p. 19.

 

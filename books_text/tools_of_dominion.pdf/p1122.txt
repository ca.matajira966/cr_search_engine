1114 TOOLS OF DOMINION

initial limiting condition is impossible—zero transaction costs—
nobody could produce a model that could prove that his allocation is
off the mark.

How would this utopian task best be accomplished? Calabresi
combines the false precision of the economist with the real obfusca-
tion of the lawyer to produce this problematical conclusion: “This
question depends in large part on the relative cost of reaching the cor-
rect result by each of these means (an empirical problem which
probably could be resolved, at least approximately, in most in-
stances), and the relative chances of reaching a widely wrong result
depending on the method used (also an empirical problem but one as
to which it is hard to get other than ‘guess’ type data). The resolution
of these two problems and their interplay is the problem of accom-
plishing optimal resource allocations.”® It surely is!

So, the allocation problem for welfare economics is merely an
empirical problem. But this so-called empirical problem cannot be
solved scientifically, logically, or technically, for there is no way for
the scientific economist to deal with the key epistemological prob-
Jem: the impossibility of making scientific interpersonal comparisons
of subjective utility. Yet the Chicago School economists babble on in
their journals as if more precise measurements could somehow solve
what they admit is the allocation problem. It is as if a gunnery sergeant
were attempting to hit a target at the edge of the universe by adding
just a bit more gunpowder to the load. It is simply a technical prob-
lem, you understand. It is as if a sprinter were trying to reduce his
time in the hundred meter race to one second flat by shaving a tenth
of a second off his time in each preliminary heat. It is an empirical
problem, you understand. If he could just get better shoes or a track
with better traction!

Calabresi knows all this. He acknowledges that the decision
which would be reached if the transactions were costless is an “un-
reachable goal.”*6 He also acknowledges that “the gains which reach-
ing nearer the goal would bring are not usually subject to precise de-
finition or quantification. They are, in fact, largely defined by
guesses. As a result, the question of whcther a given law is worth its
costs (in terms of better resource allocation) is rarely susceptible to
empirical proof . . . It is precisely the province of good government

85. Idem.
86. Idem.

1118 TOOLS OF DOMINION

Therefore, the orchard might be worth far more to him than the $100,000
in damages. . . .

‘The love of the farmer for his orchard is part of a larger difficulty for the
Coase-Demsetz doctrine: Costs are purely subjective and not measurable
in monetary terms. Coase and Demsetz have a proviso in their indifference
thesis that all “transaction costs” be zero, If they are not, then they advocate
allocating the property rights to whichever route entails minimum social
transaction costs. But once we understand that costs are subjective to each
individual and therefore unmeasurable, we see that costs cannot be added
up. But if all costs, including transaction costs, cannot be added, then there is
no such thing as “social transaction costs,” and they cannot be compared. . . .

Another serious problem with the Coase-Demsetz approach is that pre-
tending to be value-free, they in reality import the ethical norm of “effi-
ciency,” and assert that property rights should be assigned on the basis of
such efficiency. But even if the concept of social efficiency were meaningful,
they don’t answer the questions of why efficiency should be the overriding
consideration in establishing legal principles or why externalities should be
internalized above all other considerations.”

In an earlier essay, Rothbard presents perhaps the most compre-
hensive challenge to the whole economics profession that has ever
been written. The reason why I quote him at length is that he is a
very clear writer, and he is willing to follow the logic of subjectivist
economics to great lengths — not to a biblical reconciliation of objective
and subjective value, but at least to the far extremes of subjectivism.
In a remarkable essay, “The Myth of Efficiency,” Rothbard rejects
not only social costs but the idea of efficiency:

 

. . . there are several layers of grave fallacy involved in the very concept of
efficiency as applied to social institutions or policies: (1) the problem is not
only in specifying ends but also in deciding whose ends are to be pursued;
(2) individual ends are bound to cunflict, and therefore any additive con-
cept of social efficiency is meaningless; and (3) even each individual’s ac-
tions cannot be assumed to be “efficient”; indeed, they undoubtedly will not
be. Hence, efficiency is an erroneous concept even when applied to each in-
dividual's actions directed toward his ends; it is a fortiori a meaningless con-
cept when it includes more than one individual, let alone an entire socicty.

Let us take a given individual. Since his own ends are clearly given and
he acts to pursue them, surely at least Ais actions can be considered effi-
cient? But no, they may not, for in order for him to act efficiently, he would

91. Rothbard, “Law, Property Rights, and Air Pollution,” Cato Journal, 11 (Spring
1982), pp. 58-59.

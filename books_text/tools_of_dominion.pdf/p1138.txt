1130 TOOLS OF DOMINION

science against the Kantian ideal of personality.!2 It is the mathe-
matical ideal against the freedom ideal. It is the world of science
against the world of purpose.!® It is Kant’s phenomenal against
Kant’s noumenal,“! Ethical dualism once again raises its ugly,
Janus-faced head. 132

The Christian economist who acknowledges the validity of Van
Til’s epistemology (and who also understands its application) sees
no hope in the quest either for a rational ethics—an ethics sup-
posedly derived from value-free presuppositions (which are mythical
anyway)—or the quest for a reliable hypothetical mental construct
which in any way relies on the idea of man, the omniscient. A wholly
rational methodological construct along the lines of Parmenides’ un-
changing logic— with or without mathematics —is apostate man’s at-
tempt to find coherence in a changing world apart from God, Gen-
eral equilibrium theory cannot serve as a reliable “limiting concept”
that will serve as a basis for judging the performance of a real-world
economy of change, responsible decision-making, and uncertainty.
But it is understandable that apostate men wish to believe in the
potency of such an intellectual tool. As Ludwig Lachmann wrote as
early as 1943; “Economists, not unnaturally, prefer to do their field-
work in a pleasant green valley where the population register is ex-
haustive and everybody is known to live on either the right or the left
side of an equation. Only on rare occasions—and scarcely ever of
their own free will—do they embark on excursions into the rough
uplands of the World of Change to chart the country and to record
the folkways of its savage inhabitants; whence they return with grim

129, Van ‘Lil, The Doctrine of Scripture, vol. 1 of In Defense of Biblical Christianity (den
Dulk Foundation, 1967), pp. 97-98.

130. Van Til, The Case for Calvinism (Nutley, New Jersey: Craig Press, 1964), p. 81.

131. Ibid., p. 89.

132. Writes philosopher Richard Kroner: “The mutual dependence of subjectiv-
ity and objectivity rests upon the split of man’s consciousness into the consciousness.
of nature, i.e., the objective world and the consciousness of his own self and the
realm of persons. It is because of morality and freedom (hat this split cannot and must
not be overcome. The duality of science and action must be preserved at all costs.”
Kroner, Kant’s Weltanschauung (University of Chicago Press, [1914] 1956), p. 75.

133. I do not think that Douglas Vickers, a Keynesian economist who claims to
follow Van Til's epistemology, fully understands Van Til’s writings or their proper
application in the discipline of economics. See his book, A Christian Approach to Eco-
nomics and the Cultural Tradition (New York: Exposition Press, 1982), which is a follow-
up to his earlier book, Economics and Man (Nutley, New Jersey: Craig Press, 1976).
For a critique of Vickers, see Ian Hodge, Baptized Inflation: A Critique of “Christian”
Keynesianism (Tyler, Texas: Institute for Christian Economics, 1986).

 

The Epistemological Problem of Social Cost 1133

cial organizations are judged in history, isn’t it equally true that
others are blessed by God in history? The obvious example is the his-
torical church. Isn’t it blessed in history? Of course. On what basis?
On the basis of its covenant with God, which includes permanent
standards of ethical performance: biblical law. The church historical
has been sanctified by God, i.e., set apart morally for His purposes.
Therefore, we conclude that certain social institutions in history
have also been definitively sanctified, progressively sanctified, and
will be finally sanctified at the day of judgment. Without this threc-
fold model of sanctification, how else could we argue for the continu-
ing and guaranteed existence of the institutional church as a cove-
nantal institution throughout history?

Jesus’ perfect fulfilling of the law has effects in history. These
effects are personal, but they are also institutional, since institutions
are under the terms of the covenant as well as individuals. They de-
velop or contract, are blessed or cursed, in terms of the specific terms
of God's covenant, which are revealed in biblical law. Institutions,
like individuals, cannot “earn” their salvation. They are granted
their salvation, or healing, by the grace of God. Men covenant
together to perform certain works, and God imputes the moral per-
fection and moral accomplishments of Jesus Christ to these newly cove-
nanted institutions. How else can we explain the success or failure of
familics? How else can we explain why God visits the iniquity of cer-
tain familics onto the members of the third and fourth generations
(Ex, 20:5)? People make explicit covenants with God or rebel
against His implicit covenants, such as the dominion covenant given
to all men through our parents, Adam and Eve, and again with our
other parents, Noah and his family. They succeed or fail in terms of
covenantal moral standards. They advance or fall away in time, they
grow or decay, progressively over time. This process is ethical and coue-
nantal, not biological.

 

Providential Covenantatism

Here is the biblical solution of the question of social change.
Here also is the biblical solution to the dualisms of metaphysical
speculation: statics vs. dynamics. It is the 28th chapter of Deuteron-
omy, with its covenantal structure of social blessings and cursings,
which is the ethical standard for social science, including economics.
This is the biblical alternative to the timeless world of general equi-
librium theory, “peopled” with inhuman omniscient beings, passively

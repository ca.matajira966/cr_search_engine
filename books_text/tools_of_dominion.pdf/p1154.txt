1146 TOOLS OF DOMINION

behavior. Why not follow the logic of the argument? Why not con-
clude in print that there is no theoretically valid economic difference
between profit-seeking activities and criminal acts; there are only
differences in net social utility? But he does not go this far. It is al-
most as if some last remaining trace of common sense and moral val-
ues has kept Dr. Becker from pursuing the logic of his position.

His followers have not been so reticent: “An individual decision
to commit a crime (or not to commit a crime) is simply an applica-
tion of the economist’s theory of choice. If the benefits of the illegal
action exceed the costs, the crime is committed, and it is not if costs
exceed benefits. Offenders are not pictured as ‘sick’ or ‘irrational,’ but
merely as engaging in activities that yield the most satisfaction,
given their available alternatives.” Then what of the warning of
God in Proverbs? “All they that hate me love death” (8:36b). Of
course: just redefine suicidal criminal behavior in terms of the
criminal’s subjective preference for death, assume the existence of
subjective ordinal (or even cardinal) utility in his subjective value
preference scale, and economic analysis still holds! Common sense
disappears, but economic analysis, like the smile of the cheshire cat,
remains. (In all honesty, this kind of economic analysis goes back to
the mid-nineteenth century. Jeremy Bentham used a very similar
approach based on net pleasure or pain. Mercifully, the academic
world had not yet discovered cithcr economctrics or multivariate
regression analysis, so his essays were literate and coherent.)

184, This is how professional economists assess Bocker’s argument. Writes Paul
H. Rubin: “Becker essentially argued that criminals arc about like anyone else—
that is, they rationally maximize their own self-interest (utility) subject to the con-
straints (prices, incomes) that they face in the marketplace and elsewhere. Thus the
decision to become a criminal is in principle no different from the decision to become
a bricklayer or a carpenter, or, indeed, an economist, The individual considers the
net costs and benefits of each alternative and makes his decision on this basis. If we
then want to explain changes in criminal behavior over time or space, we examine
changes in these constraints. The basic assumption in this type of research is that
tastes are constant and that changes in behavior can be explained by changes in
prices.” But we all know that tastes do change. This is economically irrelevant, say
the economists. Why? Because economics cannot yet deal with changes in taste.
“Tastes are assumed to be constant because we have absolutely no theory of changes
in tastes, . . .” Rubin, “The Economics of Crime,” in Ralph Andreana and John J.
Siegfried (eds.), The Economics of Crime (New York: Wiley, 1980), p. 15.

185. Morgan O. Reynolds, “The Economics of Criminal Activity” (1973),
reprinted in Ralph Andreano and John J. Siegfried (eds.), Zhe Bconomics of Crime
(New York: Wiley, 1980), p. 34.

  

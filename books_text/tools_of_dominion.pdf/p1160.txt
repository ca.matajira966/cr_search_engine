1152 TOOLS OF DOMINION

of another man’s property rights, The Bible says plainly that restitu-
tion shall be paid with “the best” of the violator’s field, ‘and of the
best of his own vineyard.” To argue, as Coase does, that as far as so-
ciety is concerned, it is economically irrelevant to the total economic
value accruing to society whether the victim (farmer) builds the
fence at his expense or the cattleman (violator) does at his expense is
to place zero price on the rights of ownership. When free market econo-
mists place zero economic value on the rights of ownership, they have given away
the case for the free market. This is precisely what Coase and the many
academic “economics of law” specialists have done. They have pre-
ferred the illusion of value-free economics to the ideal of private
property — our legal right to exclude others from using our property.

Theft as a Factor of Production

Coase explicitly argues that the ability fo cause economic injury is a
factor of production. Therefore, the State’s decision to deny a person
the right to exercise this ability involves a social cost: the loss of a fac-
tor of production. “If factors of production are thought of as rights, it
becomes easier to understand that the right to do something which
has a harmful effect (such as the creation of smoke, noise, smells,
etc.) is also a factor of production. Just as we may use a piece of land
in such a way as to prevent someone else from crossing it, or parking
his car, or building his house upon it, so we may use it in such a way
as to deny him a view or quiet or unpolluted air. The cost of exercis-
ing a right (of using a factor of production) is always the loss which is
suffered elsewhere in consequence of the exercise of that right—the
inability to cross land, to park a car, to build a house, to enjoy a
view, to have peace and quiet or to breathe clean air.”#9 Coase sim-
ply ignores the crucial free market concept that legal right to exclude
others from invading your property is a far more crucial factor of produc-
tion— the factor of personal confidence in the honesty and reliability
of the civil government. Without this confidence, the free market is
steadily reduced to little more than black market operations.

Coase wants us to “have regard for the total effect” of such uses of
our so-called capital, namely, the right to pollute the environment.?°
But “total costs” are precisely what he has deliberately chosen to ig-
nore: the right to determine whether or noi another person can invade my

199, Goase, “Social Cost,” p. 44.
200. idem.

1166 TOOLS OF DOMINION

The newer apartments in the USSR have running water, which
indicates the existence of a policy to force people into apartments —a
housing policy that is common in socialist nations.” If we include
apartment buildings in the “urban housing fund,” then 73 percent of
the residential units had running water, and approximately 70 per-
cent were connected to sewers in the late 1960’s.%* On the collective
farms, under 3 percent had running water, and under 2 percent on
the State farms.”

By the end of the 1980's, the Western press began to report on the
sorry condition of the Soviet economy. The Soviet economy is much
weaker than Western experts had estimated.” It has run massive
budget deficits that had not shown up in official figures or in Western
estimates (with a few exceptions).2”7 The Soviet economy may even
be facing something like a crash.?8 Richard Grenier’s description of
the USSR hits home: “Bangladesh with missiles.”

Conclusion

The modern socialist State has not demonstrated that it is capa-
ble of dealing with the growing problem of pollution in a technologi-
cal society. The free market creates incentives for people to protest
against those who are transferring part of their production costs to
private citizens who do not share in the benefits. It allows the crea-
tion of independent knowledge-distribution media that can mobilize
people. It allows private citizens to challenge polluters. Socialist
monopolies are not so easily challenged by private citizens or associ-
ations in socialist commonwealths.

23. On this policy in Sweden, see Roland Huntford, The New Totatitarians (New
York: Stein & Day, 1972), ch. 12,

24. Goldman, Spoils, pp. 107-8.

25. Ibid., p. 108.

26. Nichaolas Eberstadt, “he Soviet Economy: Worse Than We Thought,” New
York Times (Nov. 23, 1988).

27. Igor Birman, “Kremlin Red Ink (And You Thought We Had a Deficit Pro-
blem),” Wall Street Journal (Nov. 15, 1988).

28. Judy Shelton, The Coming Soviet Crash: Gorbachev's Desperate Pursuit of Credit in
Western Financial Markets (New York: Free Press, 1989)

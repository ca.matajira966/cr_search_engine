Appendix F
VIOLENT CRIME IN THE UNITED STATES, 1980

The Model Penal Code [of the American Law Institute, 1962] requires
the judge to employ “generally accepted scientific methods.” Until at least
1978, the consensus of the criminology establishment was that offenders
could be rehabilitated in prisons and also in the community under the
tutelage of probation officers. This opinion prevailed even though irrefut-
able statistics revealed that at least two thirds of all offenders upon release
from prison or discharge from probation commit other offenses.

The goals and standards embodied in the Model Penal Code are really
little more than vague concepts which at one time were found palatable hy
the criminology and jurisprudence establishments. They do not provide
precise modalities of treatment or clear instructions to the sentencing
Judge. It is interesting and significant that the word “punishment” is not
used nor is the concept of making whole the victims of crime any part of
the purposes of sentencing. Indeed, the victim of crime is not even men-
toned except in a passing reference in Section 7 thai a fine should not be
imposed if it would prevent restitution. Neither restitution nor reparation
is included in the purposes of sentencing.’

This short appendix focuses on violent crime in the United

States. Three observations are necessary. First, the year 1980 seems
to have been a peak year for violent crime in the U.S. Subsequent
data indicate that rates dropped in many areas. This may be due to
the aging of the U.S. population, since young unmarried men com-
mit the largest proportion of crimes. Second, the rates for murder
began to rise in the mid-1980’s, probably because of drug-related
criminal behavior. Third, the growth in criminal activity is a West-
ern phenomenon, not just national. In Canada between 1970 and

1. Lois G. Forer, Criminals and Victims: A Trial Judge Reflects on Crime and Punishment

(New York: Norton, 1980), pp. 77-78.

1167

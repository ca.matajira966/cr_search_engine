76 TOOLS OF DOMINION

seem to have for continuing this reduction in crime in the near future,
apart from a religious revival, is that with a falling birth rate, the
number of young men, especially unmarried young men, ages 18-24,
as a percentage of population, will fall. Older men commit fewer
crimes. They get married, and marriage reduces crime. Gilder points
out that about 3 percent of criminals are women; only 33 percent are
married men. “Although single men number 13 percent of the popu-
lation over age fourteen, they comprise 60 percent of the criminals
and commit 90 percent of major and violent crimes.”** In short,
there is litle evidence thal tinkering with the criminal-investigation
system will bring relief to the victims. The causes of crime are too
complex.

By the late 1980's, major U.S. cities began to experience a rapid
escalation of violent crime, especially murder, as the drug culture
began to be organized on a highly businesslike basis.*5 An estimated
50,000%° to 80,0007 youths in the Los Angeles area now belong to
gangs. Homicides per year peaked in Los Angeles County at 350 in
1980, fell to about 200 in 1982, and then rose again, beginning in
1984, to about 400.3%

Biblical Law and Social Order

Modern criminology is a recent and very inexact science. It has
been dominated by the ideology of political liberalism, which in turn
is deeply committed to environmental determinism. Criminologists
have had very few scientific studies available to support their opin-
ions concerning the relationship between poverty and crime, or
overcrowded urban life and crime. As Harvard University political
scientist James Q. Wilson has pointed out: “It was not until 1966,
fifty years after criminology began as a discipline in this country and
after seven editions of the leading text on crime had appeared, that
there began to be a serious and sustained inquiry into the conse-
quences for crime rates of differences in the certainty and severity of
penalties. Now, to an increasing extent, that inquiry is being fur-

34. George Gilder, Naked Nomads: Unmarried Men in America (New York: Quad-
rangle/New York Times Book Co., 1974), p. 20.

35, “Dead Zones,” U.S. News & World Report (April 10, 1989).

36. “Los Angeles Seeks Ultimaie Weapon in Gang War,” Wall Street Journal
(March 30, 1988).

37. “Turf Wars,” thd, (Dec 29, 1988).

38. Idem.

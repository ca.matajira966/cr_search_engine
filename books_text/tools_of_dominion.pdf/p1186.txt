1178 TOOLS OF DOMINION

sentences reduced by getting criminals to plead guilty to lesser
crimes. “The bigger the backlog, the lighter the sentence,”#”

There will be no plea bargaining on the day of final judgment,
Justice will be perfect then. We must content ourselves with imper-
fect justice until then.*!

40. Former New York City District Attorney Robert Morgenthau; quoted by
U.S. Senator James Buckley, “Foreword,” to Frank Carrington, The Victims (New
Rochelle, New York: Arlington House, 1975), p. xv.

41, Gary North, Moses and Pharaoh: Dominion Religion os. Power Religion (Lyler,
Texas: Institute for Christian Economics, 1985), ch. 19: “Imperfect Justice.”

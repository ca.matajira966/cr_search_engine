Selling the Birthright: The Ratification of the U.S, Constitution 1195

Section IX, A quorum of the house of representatives shall consist of two-
thirds of the whole number of members elected and having met and chosen
their speaker, shall, each of them, before they proceed to business, take and
subscribe, as well the oath of fidelity and allegiance herein after directed, as
the following oath or affirmation, viz.

I do solemnly swear, by the ever living God,
(01, I do solemnly affirm in the presence of Almighty God) that as a member of this as-
sembly, I will not propose or assent to any bill, vote, or resalution, which shall appear
to me injurious to the people; nor do or consent to any act or thing whatever, that shall
have a tendency to lessen or abridge their rights and privileges, as declared in the Consti-
tution of this State; but will, in all things, conduct myself as a faithful, honest represen-
tative and guardian of the people, according io the best of my judgment and abilities.

And each member, before he takes his seat, shall make and subscribe
the following declaration, viz.

J do believe in one God, the Creator and Governor of the universe, the rewarder of
the good and punisher of the wicked. And I do acknowledge the scriptures of the ald and
new testament to be given by divine inspiration, and own and profess the protestant religion,

And no further or other religious test shall ever, hereafter, be required
of any civil officer or magistrate in this State. @

The Constitutional Convention’s Judicial Revolution

At the Constitutional Convention of 1787, Edmund Randolph
defended a national oath of allegiance. He said that the officers of the
states were already bound by oath to the states. “To preserve a due
impartiality they ought to be equally bound by the Natl. Govt. The
Natl. needs every support we can give it. The Executive & Judiciary
of the States, notwithstanding their national independence on the
State Legislatures are in fact, so dependent on them, that unless they
be brought under some tie to the Natl. system, they will always lean
too much to the State systems, whenever a contest arises between the
two.”!” He added this comment as debate progressed: “We are erec-
ting a supreme national government; ought it not be supported, and
can we give it too many sinews?””

A Loyalty Oath for U.S. Officials
It is to Hamilton’s explanation on the need for this loyalty oath

that we must turn in order to see what was really involved. He was

18. Idem

19, Max Farrand (ed.), Records of the Federal Convention, I, p. 203; extract in The
Founders’ Constitution, TV, p. 637.

20. Records, 1, p. 207; idem.

1206 TOOLS OF DOMINION

when speaking of early American politics: “Officers of the federal
government, president and congress, worshipped as an official body,
but without preference extended to a single church.”** In February,
1860, the House of Representatives invited the first rabbi to give the
invocation, only a few years after the first synagogue was established
in Washington, using a New York rabbi, since no officially ordained
rabbi was yet in Washington.® It took no Supreme Court decision to
make this covenantal denial of a judicially Christian culture a real-
ity. It was not the product of nineteenth-century freemasonry. It was
the product of late-eighteenth-century freemasonry, It was an out~
working of Article VI, Section 3.

That a President might, as Washington did (and George Bush
did two centuries later) swear his non-religious oath of office with his
hand on a Masonic Bible, is legally and covenantally irrelevant,
(That this same copy of the Bible was used by four other Presidents
at their inaugurations is surely symbolically significant.)°° An oath,
to be judicially binding, must be verbal. It must call down God’s
sanctions on the oath-taker. This is what is specifically made illegal
by the U.S. constitution. Any implied sanctions are secular, not
divine. Without this self-maledictory aspect, a symbolic gesture is
not a valid biblical oath. Rushdoony knows this, which is why he has
invented the myth of the Levitical and Deuteronomic “almost-oath.”
The Presidents have thrown a sop of a symbol to the Christians —
one hand on a Bible while taking an explicitly and legally non-Christian
oath—and the Christians have accepted this as being somehow
pleasing in God’s eyes.

Covenants and Sanctions

Every covenant has sanctions. Without sanctions, there is no
covenant. Rushdoony knows this, which is why he invokes Leviticus
26 and Deuteronomy 28: they set forth God’s sanctions in history.
The Constitution is a covenant document. He writes that “the Con-

54. Idem.

58, Bertram W. Korn, ‘Rabbis, Prayers, and Legislatures,” Hebrea Union Collage
Annual, XXIII, Part II (1950-51), pp. 95-108. Part of the reason for.this delay was
that there had not been a Jewish congregation in Washington, D.C. until 1852, and
they warshipped in homes until 1855, Those pastors asked to pray before Congress
were usually local pastors (p. 109). The rabbi who gave the prayer was Dr. Morris J.
Raphall of New York City.

56. Life (Feb. 1989), p. 8.

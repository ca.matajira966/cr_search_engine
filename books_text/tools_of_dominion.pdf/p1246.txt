1238 TOOLS OF DOMINION

Chapter and verse, 103
Character, 738
Charity
gleaning, 819-20
loans, 709-16 (see Charity loan)
local, 820
Jost animal, 782-83
mandatory, 703, 707-9
morally required, 820
positive injunction, 686-88
sanctions &, 690
social order &, 641
something for nothing, 726n
voluntary, 687
Charity loan, 709-16
bondservice, 714-16
compulsory?, 782
definition, 713-14
Chastity, 666
Cheops, 885-87
Chernobyl, 1160
Cheung, Steven, 567n, 576, 1135
Chicken, 695
Children
apostate, 271
baptism, 267n
dowry, 264, 266
Ham, 230
redemption of, 225
slaves, 122
slave trade, 333
support of, 273
Chilton, David, 113, 172n
Chinese, 978-79
Choice
costs &, 1088-93
mathematics vs., 1129
time &, 1185
uncertainty &, 1121
Christ (see Jesus)
Christian colleges, 787
Christianity
abolitionist, 185-89, 186
dominion, 271
dwelling, 357
ecumenical, 900
offense of, 171

progress &, 871
replacement principle, 189
revolution &, 189
Christianity Today, 66
Christian Reconstruction
discontinuity, 7
feedback, 64
no license, 1189
politics &, 14
practical applications, 22-23
Schaeffer &, 53
social theory, 64
suicide of, 1212
verbal stones, 1
Christian Reformed Church, 954-56,
996
Chronology, 975n

Church
adoption, 267, 276
advisor, 52

arbitration, 687
architecture, 897
bride, 227-28

courts, 791-93
covenant, 193
covering, 215n

debt, 473

dependent economically, 872
dowry, 267

family &, 271
fragmented, 900

free woman, 267
importance of, 895
impotent?, 690n
liability, 474n

joans to, 707
membership, 460, 792, 842
new wife, 268
representative, 270
rewards?, 852
sacraments, 872
sanctified, 1133
sanctions, 687
screening, 268
trespass offering, 620
virgin, 228

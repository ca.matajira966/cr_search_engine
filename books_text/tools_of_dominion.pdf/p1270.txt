1262 TOOLS OF DOMINION

pluralism, 354-56
power, 992
power &, 972
practical, 91
predictability, 765, 1049
predictable, 281, 397-98
prices, 665
procedure, 104-6, 107, 293
progress &, 27
protection of, 672, 704
purpose of, 704
rabbinical, 1058
reconstruction, 980
religion, 90-91
restoration, 400
resurrected, 47, 90
revolution &, 34-36
ridicule of, 531
Roman, 116-17, 1200
salvation by, 1207
sanctions, 16, 17, 18, 29, 31-32,
130, 914
social order, 611
sovereignty of, 18
spirit and letter, 664
standard, 355, 392
subordination to, 814
success, 972
suppressing evil, 1207
tabernacle &, 887-88
tool, 61-62
tool of dominion, 42
transcendent, 763
trustworthy, 763
undermined, 1157-58
warfare, 1207
welfare vs., 686
‘Western, 96
wisdom &, 832
wisdom only?, 678
work of, 849, 972, 978-79
Law of large numbers, 556
Lawyers, 347, 766
Lawyer's paradise, 1190
Leadership, 991-92, 99¢
Leah, 254

Learning, 1118, 1120, 1121
Leaven, 189
Lee, F. N., 82
Lee, Robert E., 357
Legalism, 385
Legal positivism, 1157-58
Legal predictability, 767, 772-73
Legal procedure, 104-6, 107
Legalism, 385
Legitimacy, 354
Lehrman, S. M., 1018
Lending, 818
Leniency, 282
Lenin, 203
Leprosy, 910
Lerner, Ralph, 1041
Letwin, William, 1129
Levi, Edward, 526-27
Levine, Aaron, 32n, 549n, 1102, 1116
Levirate, 228, 298
Levites, 229, 910
Lewin, Peter, 600n, 1107, 1139-40
Lewis, C.S., 409n, 422-93, 990,
1071, 1075
Lex talionis (eye for eye)
atonement, 536-37
bribery, 790
defined, 386
false witness &, 633
ignored, 413-14
Titeral?, 417, 418-21
negligence, 626
punishment fits crime, 381
rabbinic Judaism vs., 1013
restitution &, 386
substitution, 413-26, 449
victim's rights, 424
Liability
Jewish, 466
knowledge &, 646
limited, 471-78, 483, 636-37
safekeeping, 613-14
strict, 561, 595-96, 640
Liability insurance, 476
Liability rules, 1112, 1115, 1116,
AL?
Liberals, 94, 108

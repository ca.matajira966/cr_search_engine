Nomads, 896
Noonan, J. T., 789-91

North Carolina, 1193-94
Norway, 845n,

Nothing vs. something, 14-15
Nouns, 520n

Nuclear power, 478

Nuclear war, 34

Nuisance, 488, 552, 559
Numbering, 860, 903, 905, 906

O.K. Corral, 1153-55
Oath
caretaker’s, 635
Christian, 1192-95
confession before, 623
Constitution, 1191
false, 393, 620, 625, 631
Masonic, 391-92, 1204
neighbors, 616
representative, 1201
required, 616
sanctions, 616, 1206-10
secret, 1198
seduction &, 642
U.S. officials, 1195-97
verbal, 1206
see also Vow
Obadiah, 797
Occultism, 353, 613n, 681
O'Driscoll, Gerald, 574, 1108n
Offerings, 874
Officers, 351, 392, 883
Olasky, Marvin, 381n
Oligopoly, 685
Oman, 332
Omniscience, 397-98, 562, 698,
700-1, 762, 1125, 1130
One-many, 99
Onesimus, 167
Oppression
criminal?, 679-80
courts, 400
definition, 680, 702
envy &, 767-70
false witness, 768
feasts vs., 828

Index 1267

guidelines, 683-84
judicial, 785
knowledge &, 688
legistation, 684
percentages, 694
protection from, 723
sanctions vs., 130
sexual, 680-82
summary, 827
usury &, 705
victims, 669
Optimality, 600-1, 607
Optimism, 989, 994
Oral law, 33, 1008, 1012, 1025
Orphanages, 406
Orphans, 669, 685
Orthodox Judaism
Christians’ view of, 1002, 1011
erosion of, 1029-34
evolutionary, 1029
Mishnah &, 1011
name, 1032
process theology, 1029
state of Israel, 100In
Outsider, 770-71
Overproduction, 564
Ownership
autonomous, 706
bundle of rights, 513, 517-19,
639-40
Coase Theorem, 1102-3
common, 544, 547-50, 555
costs of, 548
delegated, 706
disownership &, 549
entitlement, 782
exclusion, 517, 693, 801, 1151-52,
1154
God’s, 706
kidnapping, 517-18
knowledge &, 500
lawful title, 517-19
mark of, 781
responsibility, 462, 485, 549,
554-55, 602
rights of, 775-76
risks & 459

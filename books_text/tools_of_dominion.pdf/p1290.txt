1282 TOOLS OF DOMINION

Stigler, G., 1123, 1153-54
Stolnitz, George, 863
Stolen goods, 512-14, 515, 516-19
Stone monuments, 926
Stoning, 44-45, 461
Strabo, 161
Strack, Hermann, 1008-9
Stranger
church, 22
heart, 813-15
Israel’s civil law &, 673
loans to, 709-10
oppression of, 669
religious freedom, 401
rest, 815
sabbath &, 811-12
Strauss, Leo, 1014n
Stream of income, 787
Stringfellow, Thornton, 3710
Stripes, 443
Stripped field, 821-22
Stroup, Richard, 787n
Style of writing, 5-6
Subheads, 6
Subjective utility, 601, 606-7, 1085,
1096-99, 1107-8, 1118-19
Subordination
bridegroom, 270, 756-57
bride price, 653
circumcision, 835
Hebrews’, 153-55
inescapable, 168
mutual, 653
protection, 209
Subpoena, 285, 297
Subsidies
crimes, 626
evil, 705
politically skilled, 584-87
seduction, 667
sin, 134, 310, 315, 657
Substitutes, 195
Subversion, 797
Success, 916-17
Suffrage (see Voting)
Suffragettes, 846

Sugar, 146, 243

Suicide, 286, 960, 962, 1096
Suicide missions, 1147n

Sunk costs, 1090

Supreme Court, 80in

Surrender, 122

Sutch, Richard, 375-77

Sutton, Antony, 701n

Sutton, Ray, 22, 67, 1i4, 290, 313n
Sweat, 963

Swimmers, 550

Swimming pool, 488

Switzerland, 423

Swords, 673

Symbolism, 519-25

Symbols (inescapable concept), 897

Tabernacle
army &, 905
awesome, 923
construction, 920
fundamentalist sermons, 892
glory cloud, 879
house, 894
Jesus, 876
judgment, 892
Kline, 894-95
monumental?, 923
permanence, 897
place of judgment, 892-93
place of sacrifice, 895
portable, 923-24
presence, 923
pyramid &, 885-88
re-creation, 878
sense of order, 895
slaves of, 152
symbol of permanence, 896
symbolism, 892-93
tablets & 887-88, 895, 910
tax?, 906
wilderness, 924
Tabernacle society, 899
Tabernacles, 830
Tablets, 105, 883, 910
Talbot Seminary, 17n

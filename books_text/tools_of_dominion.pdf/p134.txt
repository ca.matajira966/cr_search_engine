126 TOOLS OF DOMINION

civil law as a form of debt, just as debt was treated as a form of servi-
tude: the same ycar of release applied to both. A person could there-
fore keep from having to lease his land for a cash payment in advance,
but to receive this morally mandatory, economically unsecured, zero-
interest loan from his Hebrew neighbor, he had to become a
bondservant until the next sabbatical year. An emergency might re-
quire this; also, failure to repay a debt might require this.
Hebrew servant-owners were not permitted to oppress their
Hebrew servants, At the end of the term of bondage, the master was
required to provide the departing servant with capital: “And when
thou sendeth him out free from thee, thou shalt not let him go away
empty: Thou shalt furnish him liberally out of thy flock, and out of
thy floor, and out of thy winepress: of that wherewith the Lorn thy
God hath blessed thee thou shalt give unto him” (Deut. 15:13-14). By
providing the ex-bondservant with capital, the master was encourag-
ing the man to become economically independent. The man would
not be forced back into servitude simply because he could not fi-
nance whatever skills he had developed during his years as a ser-
vant. He had capital; he had been given what was called in colonial
American history his “freedom dues.”3+ Therefore, the ex-bondservant

34. The customs governing indentured servitude in colonial America in the
seventeenth and eighteenth centuries limited the term of service to a maximum of
seven years, and required the master to provide the departing servant or apprentice
with the tools of the trade he had Jearned, These “freedom dues” generally consisted
ofa suit or two of clothes, a set of tools, a rifle, and, prior to the mid-eighteenth cen-
tury, sometimes 50 acres of land. See Oscar Theodore Barck, Jr. and Hugh
Talmadge Lefler, Colonial America (New York: Macmillan, [1958] 1964), p. 297. John
‘Van Der Zee reports that both Virginia and Maryland granted 50 acres of land to all
immigrants, but the land-owners who paid the indentured servants’ fare generally
wound up as the owners. Between 1670 and 1680, of the 5,000 indentured servants
entitled to receive 50 acres, only 1,300 actually collected, and of these 900 immedi-
ately sold their land. Only 241 took warrants for land. “In all, less than 4 percent of
the people who entered the colony as servants finished out their time and settled as
freeman.” Van Der Zee, Bound Over: Indentured Seroitude and American Conscience (New
York: Simon & Schuster, 1985), p. 38. President James Madison’s forefather, John
Maddison, brought in indentured servants to Virginia and collected their land: 800
acres in 1657, 300 in 1662, 200 in 1664: idid., p. 40.

For a popular narrative history of indentured servitude, see Clifford Lindsey
Alderman, Colonists for Sale: The Story of Indentured Seroants in America (New York:
Maeruillan, 1975). He provides evidence that servants were not always rewarded
with much upon their release: pp. 57-38, 74-75, 88-89. Cf. Warren B. Smith, White
Servitude in Colonial South Carolina (Columbia: University of South Carolina Press,
4901); Cheeseman A, Herrick, White Seroitude in Pennsylvania (Philadelphia: McVey,
1926); Abbott Emerson Smith, Colonists in Bondage: White Seruttude and Convict Labor in
America, 1607-1776 (Chapel Hill: University of North Carolina Press, 1947),

A Biblical Theology of Slavery 127

would normally have had no excuse for a return to servitude except
his own incompetence. It would not be his former master’s fault. As
James Jordan has said, “The purpose of slavery . . . is to tratn irresponsi-
ble men into productive covenant members.”3°

It is important to understand that this grant of material capital
was not the primary basis of the ex-servant’s future economic inde-
pendence. The major form of capital that he was to take from his
place of former servitude was ethical and psychological. He then
would return to his family inheritance, his land. He had learned to
discipline himself under the threat of physical punishment, just as a
child learns. He had been given an opportunity to lengthen his time
perspective. He had been under the direction of a successful man-
ager, someone who could afford to purchase a servant. Being man-
aged by a good manager is one of the best ways to become a good
manager. Also, those wha serve others faithfully learn to lead others
effectively, Cost-effective consumer-oricnted service is the basis of
economic success in a free society.*° Thus, short-term indentured
servitude was designed to produce long-term independence, just as
slavery in Egypt was part of God’s plan to make the Hebrews the
conquerors of Canaan.

Anyone who argues that the sabbatical year of release only applied
to the seventh year, and then the charity debt’s obligation was reim-
posed in the eighth year, faces a very difficult exegetical problem:
how to avoid the parallel conclusion, that indentured servitude also
was reimposed in the eighth year. I see no way out: anyone who
affirms the reimposition of the previous debt rust also affirm the re-
imposition of previous indentured servitude. Only the jubilee year
could therefore bring permanent release from the indentured servi-
tude of the charitable loan. But if Jesus did away with the jubilee
year by fulfilling it and then by transferring the kingdom to the in-
ternational church (Matt. 21:43), as I argue, then what temporal
limit, other than the death of the person in bondage, is placed on
either debt or servitude? There is no indication that Jesus annulled
the principle of the sabbatical year, but if the sabbatical year does not
permanently release the debtor or the servant, and the jubilee year is
gone, there appears to be less economic liberation in the New Testa-

35. James B. Jordan, The Law of the Covenant; An Exposition of Exodus 21-23 (Tyler,
‘Texas: Institute for Christian Economics, 1984), p. 77. Emphasis in the original.

36. Ludwig von Mises, Human Action: A Treatise on Economics (3rd ed.; Chicago:
Regnery, 1966), pp. 269-72.

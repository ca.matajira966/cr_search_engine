130 TOOLS OF DOMINION

This means that if the master had decided not to pay him any
daily wages, the only visible sanction against his slothful behavior
was his very posilion as a bondservant. But this was his condition
whether he was slothful or not. Thus, the master needed additional
sanctions—“sanctions at the margin’—to compel better service.
Without them, the wage-less servant would have had no externally
imposed incentives to obey. Biblical law does not say what additional
sanctions could be lawfully imposed, but the law always allows the
imposition of sanctions. A governing principle of biblical law is this:
“Without sanctions, there is no law.” (The sanctions sometimes are
applied in history and eternity by God rather than by human gov-
ernments in history — for example, the law against economic oppres-
sion [Ex. 22:21-24].)?? Thus, I conclude from the law’s very silence
that the master could have taken a recalcitrant bondservant before
the local civil magistrates. They were implicitly authorized to flog
servants who violated the laws of the household. Surely this was the
case, since they were required to try and then execute rebellious
adult sons (Deut. 21:18-21). The implicit judicial restriction against
corporal punishment —the requirement to treat all Hebrew jubilee-
release bondservants as hired laborers—was placed on the master
acting autonomously, but not on the magistrates acting corporately,
Also, the master could sell a slothful servant to a resident alien, a pub-
licly visible downward move by the latter on the Israelite social scale.

In summary, what this means is that a poor Hebrew could law-
fully capitalize the expected future value of his family’s land and also
the expected future value of his family’s labor services, although his
children could escape this latter obligation at age 20, when they be-
came legal adults. This is another way of saying that the poverty-
stricken Hebrew could lawfully sell his future labor, including his
family’s future labor, for a discounted cash price.

The degree of servitude required by the Hebrew bondservant, as
well as the appropriate level of pay (initial purchase price) he was
entitled to, were both to be equivalent to what was owed from and to
the hired day laborer. The hired servant was to be paid a daily wage
(Lev. 19:13). Competitive market conditions would determine the
appropriate pay scale. Thus, the imputed or estimated stream of
future income (discounted by the prevailing market rate of interest)
that was to be used by the purchaser in order to establish the capital-
ized price was this level of wages.

37. Sec Chapter 22: “Oppression, Omniscience, and Judgment.”

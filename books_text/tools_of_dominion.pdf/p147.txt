A Biblical Theology of Slavery 139

own slave merchants. Resident aliens in Israel could legally sell
themselves and their descendants into slavery. The jubilee legisla-
tion was emphatic:

Both thy bondmen, and thy bondmaids, which thou shalt have, shall be of
the heathen that are round about you; of them shall ye buy bondmen and
bondmaids. Moreover of the children of the strangers that do sojourn
among you, of them shall ye buy, and of their families that are with you,
which they begat in your land: and they shall be your possession. And ye
shall take them as an inheritance for your children after you, to inherit
them for a possession; they shall be your bondmen for ever: but over your
brethren the children of Israel, ye shall not rule over one another with
rigour (Lev. 25:44-46).

Because pagan slaves could be purchased for a lifetime of service,
and because their children would become the property of the owner’s
heirs, they would have commanded higher purchase prices than
Hebrew indentured servants, The present price of any asset is its ex-
pected net return over its expected term of service, discounted by the
prevailing market rate of interest, The longer its expected net
return, the higher the price. The pagan slave could legally produce a
lifetime of service; his market price would have reflected this fact.
Add to this the future value of his heirs’ productivity, and we can
safely conclude that pagan slaves would have commanded a higher
market price than Hebrew indentured servants. An indentured ser-
vant could legally produce a stream of income for a much shorter
period unless he voluntarily sold himself into permanent servitude
(Ex. 21:5-6), something that the buyer could not have safely pre-
dicted at the time of purchase. There was no long-term market for
non-criminal Hebrew servants. (Hebrew criminals could be pur-
chased for price sufficiently high to pay their victims; thus, they
could be placed into lifetime slavery, just as if they were pagan
aliens. Still, there were greater risks associated with bringing a con-
victed criminal into a household. This would have depressed their
prices somewhat.)

Robert L. Dabney, the Calvinist theologian of Virginia, appealed
to the Levitical law of permanent heathen slavery in his defense of
the South’s slave system, published after the war.5° “There was to be

50, Robert L. Dabney, 4 Defence of Virginia [And Through Her, of the South} (New
York: Negro University Press, [1867] 1969), pp. 117-19. It had originally been writen
during the war. He had submitted the manuscript to the Confederate government,

162 TOOLS OF DOMINION

The State became an owner of slaves and the marketer of excess
slaves. If the State had not become dependent on slaves, either as
workers or as sources of revenue, slavery in the classical world would
not have become a widespread phenomenon, for the land and the
available technology could not easily support large concentrations of
slaves. It took coercion by the State to mobilize the men and re-
sources that made a true slave society possible, both militarily and
economically. The State centralized political and economic power,
but its power atrophied when men’s faith in the future departed. Its
wars of empire became defensive. In the case of Rome, slavery be-
came serfdom; men were subsequently tied legally to the land, but
not owned. Their productivity could no longer be capitalized
through sale or purchase, except (rare) by sale or purchase of the
land they tilled.

The case of the heathen peoples in Israel was not an exception to
this rule of State-created slavery. Israel’s State became the primary
mobilizer of slaves during its brief era of slavery. A system of forced
labor had been adopted by David: “. . . and Adoram was over the
forced labor” (II Sam. 20:24, NASB). Solomon later forcibly drafted
153,000 resident Canaanites into service on his huge State con-
struction projects, including the temple (II Chron. 2:18). These
heathen peoples had remained as residents in Israel,1!9 although
they had originally been designated by God for annihilation. Solo-
mon enslaved them, though only temporarily, to labor on his huge
public works projects, including the construction of storage cities for
his chariots and cavalry (I Ki. 6:19) — offensive weapons that violated
God’s law (Deut. 17:16). This paralleled his legal multiplication of
wives, also prohibited to a king, “that his heart not turn away”
(Deut. 17:17). This terminology — turning away the heart— the Bible
also uses with respect to Solomon’s later years (I Ki. 11:2, 4). All of
this was of a single piece: vast public works projects, a coerced
heathen labor force, the multiplication of offensive weapons, the
multiplication of wives, theological apostasy, and the subsequent
judgment of God (I Ki. 11:11). This creation of an army of slaves was
not the product of a free market economy.

In the generation after Solomon, the advisors to his son Rehoboam
recommended policies that led to a revolt and the destruction of the
kingdom (I Kgs. 12), Generally, this is explained as a tax revolt, but

119. Josh. 16:10; 17:12; Jud. 1:28, 30.

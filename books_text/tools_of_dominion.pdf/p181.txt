A Biblical Theology of Slavery 173

Fishers of Men

In a clever use of this familiar New Testament phrase, economic
historians Robert Paul Thomas and Richard Nelson Bean describe
the profession of slave trading.#® The New Testament condemns the
profession: “We know that the law is good if a man uses it properly.
We also know that law is made not for good men but for lawbreakers
and rebels, the ungodly and sinful, the unholy and irreligious; for
those who kill their fathers or mothers, for murderers, for adulterers
and perverts, for slave traders [kidnappers] and liars and perjur-
ers...” (1 Tim. 1:8-10, NIV). Kidnappers are condemned, but the
Greek word can easily be translated as slave traders.

In a very real sense, the slave trader did serve as a kind of per-
verse imitation of the New Testament evangelist: he harvested men’s
bodies, just as Christians are supposed to harvest both bodies and
souls (John 4:35), The Old Testament allowed the importation of
heathen slaves as a means of evangelism. With the gospel’s breaking
of national Israel's old wineskin, a new means of foreign evangelism
began. New Testament evangelists are to go to foreign lands as ser-
vants, not as slave-masters or their economic agents, slave traders.
They are to warn men and women to submit to God’s rule voluntar-
ily. God serves as the agent of cosmic coercion; Christians are to
warn men of the true nature of slavery to sin, for it leads to the eter-
nal cosmic whip. Christians are to bring the message of liberation
which Jesus announced in Luke 4.

There is no explicit Old Testament condemnation of the slave
trade, when that trade was confined to the importation of foreign
slaves. It is clear, however, that the traders’ services could be used
only to import heathen slaves, never to sell Hebrew slaves to foreign
nations (Ex. 21:8). The unstated implication is that the heathen slave
and his children were also protected from resale to heathen foreigners, since the
whole theological justification for enslaving a heathen was to bring
him under God’s visible covenantal rule. He had been made a part
of a Hebrew family’s household, even to the extent of being circum-
cised (Gen. 17:13), God was the owner of all Israel, including
heathen slaves. Thus, to turn therm out of the land, except as a civil
sanction against a criminal act, was to symbolize God's casting men
out of His covenantal jurisdiction, a most unlikely symbol. Richard

140. Thomas and Bean, “Phe Fishers of Men: The Profits of the Slave Trade,”
fournal of Economic History, XXXIV (1974), pp. 845-914.

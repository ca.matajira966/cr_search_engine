186 TOOLS OF DOMINION

flourish in the Christian West until the early nineteenth century?
Why was there never an organized anti-slavery movement anywhere
in the world until the Quakers of the mid-eighteenth century finally
took steps to cleanse their ranks of this evil??!° Christian evangelicals
were the ones who successfully pushed laws through the British
Parliament that abolished the slave trade (1808) and then slavery
(1833),2"! but the Quakers had pioneered the abolition movement.
Why the long delay?

William Westermann spent his academic career in pursuit of an
answer. “One problem remains which presses insistently for an an-
swer, however tentative it may be. It arises out of a conviction which
has often been expressed both by students working in the field of the
development of the early church and by several of the scholars who
have interested themselves specifically in the slave system as it oper-
ated in the final centuries of the Roman Empire. This is the belief
that the tenets of Christianity should have led to the overthrow of the
slave institution, perhaps in the sixth century. If not then it must
have occurred eventually at some unspecified period in which the
conjunction of circumstances became favorable to that outcome.”?!2

‘That final sentence is crucial. We need to ask his simple question:
Why must abolition have occurred eventually at some unspecified per-
iod in which the conjunction of circumstances became favorable to
that outcomc? We can answer generally, “in the providence of God,”
but that is not the answer we need as historians. We need a far more
specific answer, What was it specifically about Christianity that had to
lead to the abolition of slavery? More to the point, what kinds of his-

_torical circumstances were so vital to the development of abolition-
ism that it took eighteen centuries after the death of the Founder of
Christianity for the abolitionist movement to appear? And why were
the Quakers the original proponents of abolition? Why not the Puri-
tans, for example? Why so late in history —late in our view, that is?

210. Jordan, White Over Black, 194-95, 197-98, 271-76.

211. A very well written chapter on Wilberforce’s career appears in Charles Col-
son’s book, Kingdoms in Conflict, despite the book’s self-conscious ethical dualism.
‘The picture he presents on Wilberforce the Christian politician in Chapter 8 is in
stark contrast to his favorable view of pluralism in Chapter 9. He cites these books
on Wilberforce’s career: Robin Furneaux, William Wilberforce (London: Hamilton,
1974); John Pollack, Wilberforce (New York: St, Martin’s, 1978); Ernest Marshall
House, Saints in Politics: The Clapham Sect (London, George Allen & Unwin, 1974);
and Garth Lean, God's Politician: William Wilberforce's Struggle (Colorado Springs:
Helmers & Howard, [1980] 1987).

212, Westermann, Slave Systems, p. 159.

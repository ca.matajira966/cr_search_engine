12 TOOLS OF DOMINION

without naming its leaders or any of our books. This will not work
either, although it does delay the day of ideological reckoning, I call
this strategy ‘hide and don’t seek.” The critic hides all specific
references to our books, and hopes that his followers will not locate
the unmentioned original sources.”

Our critics would much prefer to live in a world where they are
not forced to deal with public issues in terms of a specific definition
of Christian ethics, meaning specific Old Testament civil laws with their
accompanying public sanctions. They wish that theonomists would go
away and leave them in their ethical slumber. We won't. That is what
the 1980’s demonstrated: theonomists will not go away. We will not shut
up. Our critics can ignore us no longer and still remain intellectually
respectable. We have written too much, and we continue to write.
Fifteen years after the publication of R. J. Rushdoony’s Institutes of
Biblical Law (1973), over a decade after the publication of Greg L.
Bahnsen’s Theonomy in Christian Ethics (1977), there was still only one
brief book-length academic reply from any critic in any theological
camp: Walter Chantry’s.” It has become apparent that the professional
theologians have been playing a game of “hide and go sleep.” This
tactic was adopted for a decade and a half, from 1973 to mid-1988, It
did not work. We are still here. But to change this tactic at this late
date, our critics must now respond to one hundred volumes of books
and scholarly journals, not to mention newsletters. They are unwill-
ing to do this. It would be too much work. What now? More silence.

22, An example of this tactic is found in Charles Colson’s defense of pluralism
and ethical dualism, Kingdoms in Conflict, co-published by William Morrow (secular
humanist) and Zondervan (fundamentalist) in 1987. He mentions the theonomist
movement, but never names any of these “utopians,” as he calls us (pp. 117-18). Why
not name us? If the targets of your atiack are “doomed lo failure” (p. 117), why not at
least identify us? If we are dead, then give us a decent Christian burial!

23. In late 1988, two critical books appeared: Dave Hunt, Whatever Happened to
Heaven? (Eugene, Oregon: Harvest House), and H. Wayne House and Thotnas Ice,
Dominion Theology: Blessing or Gurse? (Portland, Oregon: Multnomah Press), to which
Greg Bahnsen and Kenneth Gentry wrote a reply: House Divided: The Break-Up of Dis-
pensational Theology (Tyler, Texas: Institute for Christian Economics, 1989). A third
critique appeared in 1989: Hal Lindsey, The Road to Holocaust (New York: Bantam),
refuted by Gary DeMar and Peter J. Leithart, The Legacy of Hatred Continues (Tyler,
‘Texas: Institute for Christian Economics, 1989), Gary DeMar alrcady has co-authored
one book replying to earlier criticisms by Hunt: The Reduction of Christianity (Ft. Worth,
‘Texas: Dominion Press, 1988). A second book replies to issues raised in the Apri, 1988,
debate: Hunt and Ice vs. DeMar and North: Gary DeMar, The Debate Over Christian
Reconstruction (Ft, Worth, Texas: Dominion Press, 1988). A third book by DeMar,
which replies to numerous specific criticisms of Christian Reconstruction, is You
Have Heard It Said (Tyler, Texas: Institute for Christian Economies), forthcoming.

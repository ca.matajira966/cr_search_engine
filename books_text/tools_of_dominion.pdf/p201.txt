A Biblical Theology of Slavery 193

select another to rule over him. Once someone is adopted by God
(John 1:12), he is a son forever (Rom. 8:28-35). Second, the civil cove-
nant: men are always under a civil government, but they can move
away from one by subordinating themselves to another. Third, the
marriage covenant: legally, this is permanent until the death of one of
the partners, including covenantal (legal) death.?2” Fourth, the
church covenant. Men can leave local representatives of the church
(congregations) but not the spiritual church. Once adopted by God,
there is no escape from membership in the church. Church membership
is the only permanent covenant that carries into eternity, Nevertheless, people
have been granted the legal power to leave the local church, or cease
attending any church, even though it is a great sin to do so. The eccle-
siastical covenantal bond is still binding in God’s court, but a positive
compulsion to attend church is not the province of any civil court.??8

Thus, a covenant is a legal bond, from which we derive the word
bondage. In business, a long-term debt instrument is called a bond.
This covenantal terminology reflects the biblical origins of modern
capitalism. Biblically, there are only three public bonds because
there are only three covenantal institutions: church, State, and fam-
ily. All other legal bonds are in fact contracts, not covenants. When
men endow any human relationship other than church, State, and
family with the status of a covenant, they have violated God’s law.??9
The only forms of lifetime servitude that are legal under biblical law
are familistic: the permanent subordinate relationship of wives to
husbands, and the voluntary subordination of servants to masters
(Ex. 21:5-6).?° People can escape covenantal bondage to either the
institutional church or the State by revoking their vows and leaving.
God does not allow the revocation of the family’s covenantal bond

227, Ray R. Sutton, Second Chance: Biblical Blueprints for Divorce and Remarriage (Ft.
Worth, Texas: Dominion Press, 1987).

228. It is biblically legitimate for a civil government to revoke the right to vote
from those who have broken one or more institutional covenants. The State can law-
fully establish positive criteria for a citizen's exercise of judicial authority (voting,
jury duty), even though it is not authorized by God to impose economic or physical
sanetions for not attending church.

229, North, Sizat Strategy, ch. 3: “Oaths, Covenants, and Contracts.”

230. It could be argued that lifetime servitude by a criminal in order to make res-
titution is also an exception. ‘This argument is incorrect. The period of a criminal's
bondservice is limited by a specific sum of money owed to a victim, or owed to the
buyer who has repaid the victim, not by a specific period of time owed to the buyer.
The criminal bondservant can legally buy his way aut of bondage, or another can
buy his freedom for him.

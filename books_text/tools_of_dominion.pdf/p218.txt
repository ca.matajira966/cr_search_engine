210 TOOLS OF DOMINION

Exodus 21:2-4 presents the case law governing indentured ser-
vant marriages, God had just delivered a slave people out of bond-
age. He had removed them from the visible tyranny of Egypt, and
He was preparing them for long-term service to Him in the prom-
ised land. It was not that servitude was being abolished; it was
rather that a new Master had appeared on the historical scene. God
had delivered them out of Pharach’s household as intact families;
God was now bringing them into His household as His servants. He
was making Israel His bride.

The maximum legal period of the most rigorous form of non-
criminal indentured servitude in Israel was a little over six years.
This was the form of servitude in which the master had the right of
corporal punishment, and the form in which the servant had to be
provided with capital upon his release. During the seventh year,
sometimes called the sabbatical year by Bible commentators, these ser-
vants went free in Israel, and simultaneously all zero-interest
charitable debts were cancelled (Deut. 15). It is noteworthy that the
year of release was also the year when the law was read to the assem-
bied nation at the feast of the tabernacles (Deut. 31:10-13). God’s law
is to be understood as the means to freedom for those who obey it.?

2. I should mention here that the Jewish scholar Maimonides asserted in 1180
A.D, that a Hebrew can legitimately sell himself to another Hebrew for more than six
years, but not beyond the jubilee year. Moses Maimonides, The Book of Acquisition,
vol. 12 of The Code of Maimonides, 14 vols. (New Haven, Connecticut: Yale University
Press, 1951), “Treatise V, Laws Concerning Slaves,” Chapter Two, Section Three, p.
250. On the other hand, if the court sells him into servitude, which Maimonides
says can only take place because the man is a thief who cannot afford to make resti-
tution (Chapter One, Section One, p. 246), he can be required to work only six
years (Chapter Two, Section Two, p. 249). I argue in this commentary that there
were five different forms of Hebrew bondservice in Israel, See Chapter 4, pp.
123-36.

A major problem with the Cede is its sparse or absent arguments and explana-
tions for controversial assertions. In reading the Code, we must remember that
Maimonides distinguished between a code and a commentary: “In a monolithic
code, only the correct subject matter is recorded, without any questions, without an-
swers, and without any proofs, in the way which Rabbi Judah adopted when he
composed the Mishnah.” A commentary records opinions, debates, and identifies
sources and persons, he said: letter to Rabbi Phinehas ben Meshullam, judge in
Alexandria: reproduced in Isadore Twersky, Introduction to the Code of Maimonides
(Mishneh Torah) (New Haven, Connecticut: Yale University Press, 1980), p, 33. The
Gode was basic to Maimonides’ thinking. Twersky writes: “The Mishne Torah also be-
comes an Archimedean fulcrum in the sense that he regularly mentions it and refers
correspondents and inquirers to it. The repeated references convey the impression
that he wanted to establish it as a standard manual, a ready, steady, and uniform re-
ference book for practically all issues” (p. 18).

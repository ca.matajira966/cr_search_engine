230 TOOLS OF DOMINION

In New Testament times, any slave must be regarded legally as an
indentured servant. Involuntary lifetime servitude was abolished when
Jesus fulfilled the jubilee year; the only other form of servitude author-
ized by the Bible is indentured servitude. A slave in New Testament
times is therefore entitled to be treated as a Hebrew servant was to have
been treated in the Old Testament commonwealth, with his release de-
layed by no more than seven years, except in cases of criminal sanctions,
His children must be freed upon reaching their maturity at age 20.

A Long History of Self-Serving Bible Interpretation

Purchasing lifetime slaves from pagan nations or resident aliens
was biblically legitimate prior to Christ’s fulfillment of the jubilee
year, meaning prior to the abolition of its land tenure provisions. As
we have seen, after Christ’s death and resurrection, the Christian is
to understand that slave-owning is for the purpose of liberating peo-
ple from bondage, buying them out of demonic covenanis. It is il-
legal to compel any male to remain in bondage beyond seven years,
except in the case of criminals paying off debts to victims,

This abolition of permanent slavery was long ignored or unrec-
ognized by Bible commentators. It took Christians and Jews over
1,800 years to come to the conclusion that lifetime slavery is illegiti-
mate. The myth that the “curse of the children of Ham” refers exclu-
sively to blacks was adopted by Jews, Christians, and Muslims in
the Middle Ages.*! (There had been a curse: Noah cursed Canaan,
the son of Ham, but this curse was covenantal, not racial, and it was
generally fulfilled by the conquest of the land of Canaan by the
Israelites, and the subjection of the remnant as slaves.)*? Winthrop

41. David Brion Davis, Slavery and Human Progress (New York: Oxtord University
Press, 1984), p. 87. (Davis is incorrect when he writes that the doctrine originated in
the Middle Ages.) As late as 1867, Robert L, Dabney, the American South’s greatest
Galvinist theologian in the late nineteenth century, appealed to Genesis 9 and the
curse of Canaan to justify the legitimacy of the idea of slavery in general; “. . . it
gives us the origin of domestic slavery. And we find that it was appointed by God as
the punishment of, and remedy for (nearly all God's providential chasliserents are
also remedial) the peculiar moral degradation of a part of the race.” He did not
argue that blacks are necessarily under this same curse, although he hardly denied
it; “Te may be that we should find little difficulty in tracing the lineage of the present
Africans to Ham. But this inquiry is not essential to our argument.” Dabney, A
Defence of Virginia (New York: Negro University Press, [1867] 1969), pp. 103, 104.
42. Prof. Davis appeals to the liberal higher critic Von Rad to argue thal “the
original Yahwistic narrative had nothing to do with Shem, Ham, and Japheth, and
the ecumenical scheme of nations which follows, It was rather an older story, limited
to the Palestinian Shem, Japheth, and Canaan. . . .” Davis, ‘Slavery and Sin: The
Cultural Background,” in Martin Duberman (ed.}, The Antislavery Vanguard: New Essays
on the Abolitionists (Princeton, New Jersey: Princeton University Press, 1965), p. 50.

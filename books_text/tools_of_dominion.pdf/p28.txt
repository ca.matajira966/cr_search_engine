20 VOOLS OF DOMINION

and fan the evangelicals’ moral fervor, 1820-65, which in turn allowed
them to capture the whole country for the Unitarian worldview from
the 1860’s onward.*° In short, American Christians ignored their so-
cial responsibilities by ignoring the Quakers’ moral challenge re-
garding chattel slavery (1760-1820), for they did not recognize or
acknowledge the judicial authority of the New Testament on this
question." As a result, they became institutionally and intellectually
subordinate to those who hated Christianity (1820-1865).

Simultaneously, a parallel phenomenon took place with the rise
of the state school systems, another Unitarian reform in the United
States. Funded by Christian taxpayers, the schools have been oper-
ated in terms of an alien worldview.*? The escape religion led to the
triumph of the power religion. It always does. Dominion religion invari-
ably suffers. This defeat of dominion religion is the temporal goal of
the power religionists and the escape religionists, of Pharaoh and the
enslaved Israelites. They always want Moses to go away and take his
laws with him.

‘These two evil consequences of natural law theory —retreat from
social concerns and the co-opting of Christians by non-Christian so-
cial reformers — have been the cursc of natural law theory for almost
two millennia. Dabney could have protested until kingdom come—
or until Sherman’s army came— against the anti-Constitution agenda
of the northern Abolitionists,*3 but his own commitment to natural
law philosophy undercut his theological defense. He did not under-
stand that when a law-abiding Christian adopts a hostile attitude to-
ward the case laws of the Old Testament, he necessarily also adopts
an attitude favorable to natural law theory, which is inescapably phi-
losophical humanism: common-ground philosophy, common-
ground ethics, and the autonomy of man.** Dispensationalist theo-
logian and natural law philosopher Norman Geisler is simply more
forthright regarding this necessary two-fold commitment: anti-Old

40. R. J. Rushdoony, The Nature of the American System (Fairfax, Virginia: Thoburn
Press, [1965] 1978), ch, 6: “The Religion of Humanity.”

41, See Chapter 4: “A Biblical Theclogy of Slavery.”

42. R, J. Rushdoony, The Messianic Character of American Education (Nutley, New
Jersey: Graig Press, 1963).

43. Defence of Virginia, Conclusion.

44. Archic P. Jones, “Natural Law and Christian Resistance to Tyranny,” Christi-
anity and Civilization, No. 2 (1983), pp. 94-132.

276 TOOTS OF DOMINION

latter cases, women recognize more clearly how vulnerable they are
legally and economically.

Conclusion

The Old Testament authorized two forms of marriage contracts:
free marriage and concubinage. ‘The free wife brought a dowry into
the marriage; the concubine did not. Both forms of marriage were
lawful, but concubinage was less desirable. It left wives far more vul-
nerable to divorce or neglect by husbands.

The bride price was a requirement for marriage. If the father used
the money to endow his daughter, she entered the marriage as a free
woman. If he kept the bride price for himself, she entered as a con-
cubine. The system allowed poor girls to escape from a life of pov-
orty in their fathers’ households.

The basis of Old Testament marriage was adoption. In effect, it
was a symbol of the new birth, which is also a covenantal adoption
(John 1:12). ‘The bridegroom adopted the girl into his family. He had
to gain the cooperation of her father in this transfer of family mem-
bership from her family to his. Fathers used the bride price system to
screen out bridegrooms who were morc likely to be economically
irresponsible. When fathers transferred to bridegrooms their cove-
nantal office as God's representative for their daughters, they wanted
some visible sign that the recipient would be responsible. ‘The pay-
ment of the bride price was a manifestation of the bridegroom’s com-
petence and also a symbol of his subordination to the girl’s family.

The New Testament annulled the bride price system by trans-
ferring the marital adoption process to the church. There are no law-
ful concubines today, Christ’s payment of the bride price to God the
Father at Calvary marked Him as the Bridegroom to the true bride,
the church. ‘The church is today the appropriate agency of the cove-
nantal adoption process of marriage. Like God, who found aban-
doned Israel as an infant and raised her, and later married her
(Ezek. 16), so the church baptizes children and then later sanctions

 

ployment obligations. Sce Barbara B. Hirsch, Living Tigether (Boston: Houghton
Mifflin, 1976). Something like 3 million couples ate presently unmarried in the U.S.
reports Gary S. Meyers, “Unmarriage contrac?’ can help protect cobabitanty
rights,” Dallas Times Herald (Feb. 15, £987), In 1978, the figure was about 1.1 million
out of 48 million husband-wife households: Robert Reinhold, “Census Finds Un-
married Couples Have Doubled From 1970 to 1978,” New York Times (June 27,
1979). ‘The growth in cohabitation is accelerating rapidly.

 

 

Victim's Rights vs. the Messiante State 301

surely be put to death.” As mentioned earlier, the Hebrew phrase is
what scholars call a pleonasm: “dying, he shall surely die.” It is em-
phatic language. We find it in Exodus 21:12: “He that smiteth a man,
so that he die, shall surely be put to death.” James Jordan com-
mented in 1984: “The emphasis means that the death penalty cannot
be set aside by any payment of money.”” But because of a series of
problems in interpretation, he subsequently changed his mind about
the meaning of this pleonasm.”

What Is the Problem?

Why should the interpretation of this pleonasm of execution be
such a problem? Because the same phrase appears in the case of
crimes that we normally would not think would involve automatic
capital punishment. These include crimes that have no immediate
human victims: sabbath-breaking (Ex. 31:14-15) and bestiality (Ex.
22:19; Lev. 20:15-16). These also include crimes in which no one
dies: assaulting parents physically (Ex. 21:15) or verbally (Ex. 21:17),
adultery that involves another man’s wife (Lev. 20:10), blasphemy
against God (Lev. 24:16), and wizardry and witchcraft (Lev. 20:27).
One crime to which this pleonasm is attached is often regarded by
modern societies as a capital crime: kidnapping (Ex. 21:16).3!

To survey the nature of the exegetical problem, let us consider in
greater detail the case of adultery that involves a man with another
man’s wife; “And the man that committeth adultery with another
man’s wife, even he that committeth adultery with his neighbour's
wife, the adulterer and the adulteress shall surely be put to death”
{Lev. 20:10). The pleonasm of execution appears here: “shall surely
be put to death.” Capital punishment for both of the adulterers can
legitimately be imposed at the insistence of the victim, the woman’s
husband. Why? Because the government of the covenantal family was broken
by adultery, The injured party, meaning the head of the household, is
the lawful covenantal representative of God. He is authorized to
bring charges against the adulterers as the injured party and also as
the head of the family unit. Because the Bible specifies adultery as a
civil crime, he also brings this lawsuit in civil court.

The victimized husband can lawfully file the covenant lawsuit in
up to three covenantal courts: family, church, and State. A covenant

29. Jordan, Law of the Covenant, p. 96n.
30. They are not the same objections that I raise in this chapter.
31. See Chapter 8: “Kidnapping.”

24 TOOLS OF DOMINION

again, though not his specific answers. (I have also dropped his
italics.) I too have heard variations of these objections repeatedly.

Objection I: “You have written too many Books already: Who do you
think hath so little to do as to read them all?”

Objection IL: “Your Writings differing from the common judgment
have already caused offence to the godly.”

Objection III: “You should take more leisure, and take other mens
judgement of your Writings before you thrust them out so hastily.”5?

And in response, I can do no better than to close with Baxter's
summary comments. Indeed, if I were to issue a challenge to the
critics of me in particular and Christian Reconstruction in general,
this would be it:

In summ, to my quarrelsome Brethren I have two requests, 1. That in-
stead of their unconscionable, and yet unreformed custome of backbiting,
they would tell me to my face of my offences by convincing evidence, and
not tempt the hearers to think them envious: and 2. That what I do amiss,
they would do better: and not be such as will neither laboriously serve the
Church themselves, not suffer others: and that they will not be guilty of
Idleness themselves, nor tempt me to be a slothful servant, who have so lit-
tle time to spend: For I dare not stand before God under that guilt: And
that they will not joyn with the enemies and resisters of the publication of
the Word of God.

And to the Readers my request is, 1. That whatever for Quantity or
Quality in this Book is an impediment to their regular universal obedience,
and to a truly holy life, they would neglect and cast away: 2. But that which
is truly Instructing and Helpful, they would diligently Digest and Practice;
And I encourage them by my testimony, that by long experience I am
assured, that this PRACTICAL RELIGION will afford both to Church,
State and Conscience, more certain and more solid Peace, than contending
Disputers, with all their pretences of Orthodoxness and Zeal against Errors
for the Truth, will ever bring, or did ever attain io.

I crave your pardon for this long Apology: It is an Age where the Objec-
tions are not feigned, and where our greatest and most costly services of
God, are charged on us as our greatest sins; and where at once I am accused
of Conscience for doing no more, and of men for doing so much: Being really

A most unworthy Servant of so good a Master

52, Richard Baxter, A Christian Directory: Or, A Summ of Practical Theologie, and Cases
of Conscience (London: Robert White for Nevil Simreons, [1673] 1678), unnumbered
pages, in Advertisements.

316 TOOLS OF DOMINION

Conclusion.

All sins are against God and God's law. All sinners are criminals
in the hand of a temporarily merciful Victim, God sits on His throne
as final Judge and even temporal Judge (e.g,, He slew Ananias and
Sapphira: Acts 5:5, 10), But to sin against God, men usually must sin
against something in the creation. The Bible provides case laws
that define those sins against any aspect of the creation which consti-
tute civil, familial, or ecclesiastical infractions. Where a sin does
constitute an infraction, the victim must represent God by becoming a plaintiff
against the sinner. He upholds the integrity of the injured party and
also seeks restitution. In some cases, restitution is made only to the
victim; in other cases, it must also be made to God through a pay-
ment to His church (Lev. 7).

The Bible provides five remedies for criminal behavior: 1) flogging
(up to 40 lashes), 2) the slashing of a woman’s hand; 3) economic res-
titution, which can be large enough to require 4) up to a lifetime of
bondage, and 5) execution. The goals of these penalties include: 1) up-
holding God's interests by enforcing His law (civil worship)*®; 2) pen-
alizing criminal behavior, sometimes by removing the criminal from
this world (vengeance); 3) warning all people of the eternal judg-
ment to come (evangelism); 4) protecting civil order (deterrence);
and 5) protecting the interests of victims (justice). Ultimately, all of!
these goals can be summarized in one phrase: upholding God's covenant.

Notice that there is no mention of imprisonment, Hirsch wrote a
century and a half ago: “Punishments of imprisonment, with all the
attendant despair and moral degradation that dwell behind prison
bars, with all the worry and distress that it entails for wife and child,
are unknown in Torah jurisprudence. Where its power holds sway,
prison for criminals does not exist. It only knows of remand custody,
and even this, according to the whole prescribed legal procedure,
and especially through the absolute rejection of all circumstantial
evidence, can only be of the shortest duration.”*”

The law upholds the victim’s interests. The criminal is to make
restitution to his victim. The victim has the right to extend mercy,

 

45. An exception could be mental sins, yet in a sense even these are sins against
the creation: a misusc of man’s gift of reason.

46. If civil magistrates ure ministers, as Paul says they are (Rom. 13:4), then
there is an element of worship in their enforcement of God's law. Sanctions are im-
posed in God’s name.

47. Hirsch, Fxndus, p. 294: at Exodus 21:6.

 

322 TOOLS OF DOMINION

Nevertheless, the crime of kidnapping goes beyond the question
of the image of God in man. Kidnapping is more than an assault
against God’s image in man. It is not simply man’s blood that is in-
violate (Gen. 9:6); it is also his life’s calling. It is not simply his image
that commands respect from other men; it is also his God-ordained as-
signment in life. Perhaps it would be better to argue that man’s imag-
ing also includes the calling. God is revealed in Genesis 1 as a God
who works and who judges. Man images this God. Kidnapping is
therefore an assault on both of these aspects of man’s imaging.

Who is the true owner of the kidnapper’s victim? God is. God
owns the whole world (Ps. 50:10). Nevertheless, stealing a privately
owned animal is not a capital crime (Ex. 22:1), Why the special case
of a man? The answer is found in man’s special position: subordi-
nate under God and possessing authority over the creation, Man is
made in God’s image (Gen. 1:27; 9:6). By interfering with a man’s
God-given calling before God, the kidnapper disrupts God’s re-
vealed administrative structure for subduing the earth. Each man
must work out his salyation—or, presumably, work out his damna-
tion — with fear and trembling (Phil. 2:12). The kidnapper asserts his
presumed autonomy and illegitimate authority over the victim, as if
he were God, as if he possessed a lawful right to determine what
another man’s responsibilities on earth ought to be.

The Death Penalty

The Bible recognizes that there are two potential criminals in-
volved in kidnapping: the actual kidnapper and the person to whom
he sells the victim. The international slave trade did exist. (White
slavery — kidnapping of white girls who are then sold into the Middle
East or other foreign areas — still appears to exist.) The passage deals
with both types of criminal: “And he that stealeth a man, and selleth
him, or if he be found in his hand, he shall surely be put to death.”
Both the kidnapper and the recipient of the stolen victim are subject
to the death penalty.>

The obvious problem with a universally mandatory death penalty
is that a crime whose effects are less permanent than murder bears
the same permanent penalty that murder does. Consider the case of
kidnapping. The kidnapper has a strong incentive to kill the victim if
he thinks that the authorities are closing in on him. The victim may

5. Dale Patrick, Old Testament Law (Atlanta, Georgia: John Knox Press, 1985), p. 74.

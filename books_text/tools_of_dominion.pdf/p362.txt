354 ‘TOOLS OF DOMINION

predictably degenerated into violence and lawlessness within a few
years. It was officially disbanded in 1869, and when local “dens” per-
sisted, it was stamped out by the U.S. military. An imitation. of the
old Klan rose again to national political prominence in the 1920's,
only to fade nationally in the 1930’s and in the South in the 1940's.
Today, numerous local Klan-type groups exist, but they have little
influence.*” But the Klan’s former power testifies to the fact that
when civil courts fail to dispense justice and therefore lose their legit-
imacy in the eyes of large numbers of citizens, societies will eventu-
ally see the rise of private dispensers of “people's justice.”

Without a sense of legitimacy, the authority of public courts is
threatened. The courts need legitimacy in order to gain the long-
term voluntary cooperation of the public, meaning self-government
under law, without which law enforcement becomes both sporadic
and tyrannical. No legal system can afford the economic resources
that would be necessary to gain full compliance to an alien law-order
in a society whose members are unwilling to govern themselves vol-
untarily in terms of that law-order.* If the courts do not receive as-
sent from the public as legitimate institutions, they can maintain the
peace only by imposing sentences whose severity goes beyond peo-
ple’s sense of justice, which again calls into doubt both legitimacy
and legal predictability.

Judicial Pluralism and Social Disintegration

A civil government that refuses to defend a law-order that is seen
as legitimate by the public is inviting the revival of the duel, the
feud, and blood vengeance. If the public cannot agree on standards
of decency, then the courts will be tempted to become autonomous.
Widespread and deep differences concerning religion lead to equally

46, Tt was the victory of an anti-Klan candidate for governor in the Republican
Party's primary in the state of Oregon which led the Klan to jump to the Democratic
Party. They elected the Democratic candidate, plus enough members of the legisla-
ture to pass a law mandating that all children between the ages of eight and sixteen
attend a government-operated school. Chalmers, Hooded Americanism, p. 3. This law
was overturned by the U.S. Supreme Court in 1925 in a landmark case, Pierce t.
Society of Sisters, which has remained the key Court decision in the fight for Christian
schools.

47. As one southerner described the Klan: “It is made up mainly of gasoline sta-
tion attendants and FBI informers. The members can easily spot the informers: they
are the only ones who pay their monthly dues.”

48. Gary North, Moses and Pharaoh: Dominion Religion os. Pawer Religion (Tyler,
Texas: Institute for Christian Economics, 1985), pp. 291-94.

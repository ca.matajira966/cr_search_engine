382 TOOLS OF DOMINION

this passage was controversial in Christian circles: the judicial re-
quirement of “an eye for an eye.” The abortion aspect of the argu-
ment was not controversial, for the practice of abortion was illegal
and publicly invisible. A physician who performed an abortion could
be sent to jail.? It was clearly understood by Christians that anyone
who caused a premature birth in which the baby died or was in-
jured had committed a criminal act, despite the fact that the person
did not plan to cause the infant’s injury or death. The abortion de-
scribed in the text is the result of a man’s battle with another man, an
illegitimate form of private vengeance for which each man is made
fully responsible should injury ensue, either to each other (Ex.
21:18-19) or to innocent bystanders. If this sort of “accidental” abor-
tion is treated as a criminal act, how much more a deliberate abor-
tion by a physician or other murderer! Only when pagan intellec-
tuals in the general culture came out in favor of abortion on demand
did pro-abortionists within the church begin to deny the relevancy of
the introductory section of the passage.

This anti-abortion attitude among Christians began to change
with the escalation of the humanists’ pro-abortion rhetoric in the early
1960's. Christian intellectuals have always taken their ideological
cues from the humanist intellectuals who have established the pre-
vailing “climate of opinion,” from the early church's acceptance of the
categories of pagan Greek philosophy to the modern church's accep-
tance of tax-funded, “religiously neutral” education. As the human-
ists’ opinions regarding the legitimacy of abortion began to change in
the early 1960’s,3 so did the opinions of the Christian intellectual
community. Speaking for the dispensationalist world of social
thought, dispensationalist author Tommy Ice forthrightly admitted

2. Julims Hammer, the millionaire physician father of (later) billionaire Armand
Hammer, in 1920 was sent to Sing-Sing prison in Ossining, New York, for perform-
ing an abortion in 1919, The woman had died from the operation. Hammer was con-
victed of manslaughter. (If all women died after an abortion, there would be fewer
abortions performed.) Predictably, several physicians protested the law, but to no
avail. Armand Hammer, Hammer (New York: Putnam’s, 1987), pp. 74-82. Contrary
to Hammer’s glowing tribute to his father, the press was hostile to Julius Hammer.
Sec Joseph Finder, Red Carpet (New York: Holt, Rinehart & Winston, 1983), p. 18.
(This book was reprinted in paperback by the American Bureau of Economic
Research, Ft. Worth, Texas, in 1987). Julius Hammer had been a member of the
Socialist Labor Party, a precursor of the American Communist Party. He became a
millionaire by trading in pharmaceuticals with the USSR. He actually served as
commercial attaché for the USSR in the United States. Jéid., pp. 12-16.

3. Olasky, The Press and Abortion, chaps. 10, 11

Criminal Law and Restoration 399

2. Legal Predictability

Second, there is the crucial issue of legal predictability. “There is
probably no single factor which has contributed more to the prosper-
ity of the West than the relative certainty of the law which has pre-
vailed here.”*? He makes a very important point in this regard. The
certainty of law is important, not just in cases that come before the
courts, but also in those cases that do not lead to formal litigation be-
cause the outcome is so certain. “It is the cases that never come be-
fore the courts, not those that do, that are the measure of the cer-
tainty of the law.”*? In the United States, there is seemingly cndless
litigation, precisely because of the unpredictability of the courts.
Men go into the courts seeking justice because they do not know
what to expect from the courts. If they knew what to expect, fewer
people would bother to litigate. They would settle out of court or
perhaps even avoid the original infraction.

The law of God establishes the “eye for eye” principle. Men can
assess, in advance, what their punishment is likely to be if they
transgress the law. They can count the potential cost of violence,
This is a restraining factor on all sin. A person can imagine the costs
to his potential victim of losing an eye or a tooth. If convicted, the
criminal will bear a comparable cost.

Rulers must be aware that the Jex ialtonis principle is not simply
limited to crimes by private citizens. Judgments fall on nations, both
blessings and cursings (Judges, Jonah, Lamentations). The list of
promised national cursings in Deuteronomy 28:15-68 is a detailed
extension of the list of promised blessings in verses 1-14. When na-
tions defy God in specific ways, they will be judged in specific ways —
mirror images of the promised blessings to covenantally faithful na-
tions. Instead of going out in war (a national endeavor, not private)
and scattering their enemies, they will go out to war and be scattered
by their enemies, Instead of lending to their enemies, they will be-
come debtors to their enemies. The principle of “cye for eye” is
essential to all of life. From him to whom much has been given,
much is expected (Luke 12:47-48).

52. Hayek, Constitution of Liberty, p. 208.
53. Ide,
54. Macklin Fleming, The Price of Perfect Justice (New York: Basic Bouks, 1974).

406 TOOLS OF DOMINION

Parallel tax-supported institutions were developed during this same
era: the insane asylum, the orphanage, the reformatory for youthful
delinquents, and the large-scale public almshouse.” It was also the
era of the first “religiously neutral” (humanistic) tax-supported day
schools in the United States.”3

In Israel, there was no prison system. Egypt had prisons; Isracl
did not.7* Why not? Because prisons do not offer adequate oppor-
tunities for criminals to repay their victims. A prison restricts the
criminal’s ability to make restitution, and restitution is the very essence of bibli-
cal punishment. Prisons restrict men’s ability to repay; they also make
it difficult for men to exercise dominion over nature.

In a sense, the prison is analogous to the final judgment. There is
no restitution to victims by those in hell or in the lake of fire. There is
permanent restitution to God, but not to man. In this sense, hell is
outside history and the process of restitution and restoration. Hell is
described as a debtors prison in Jesus’ parable of the unjust debtor,
The debtor is cast into prison until every last payment is made
(Matt. 18:23-35). The debtor could get out only if someone clse paid
his obligations. Clearly, this is a picture of Christ’s payment of His
people’s ethical debts to God, as their kinsman-redeemer. This sub-
stitute payment is available to mankind only in history. Thus, the
prison is illegitimate because it represents a denial of history and the
opportunities of history. That Egypt should have prisons is under-
standable; Egyptians had a static view of time. Israel did not. Old
Testament law did not allow imprisonment.”

72. David Rothman writes: “Americans in the colonial period had followed very
different procedures, They relieved the poor at home or with relatives or neighbors;
they did not remove them to almshouses. They fined or whipped criminals or put
them in stocks or, if the crime was serious enough, hung them; they did not conceive
of imprisoning them for specific periods of time. The colonists left the insane in the
care of their families, supporting them, in casc of need, as one of the poor. They did
not erect special buildings for incarcerating the mentally ill. Similarly, homeless
children lived with neighbors, not in orphan asylums. . . . The few institutions that
existed in the eighteenth century were clearly places of last resort. Americans in the
Jacksonian period reverscd these practices. Institutions became places of first
resort, the preferred solution to the problems of poverty, crime, delinquency, and in-
sanity.” David J. Rothman, ‘he Discovery of the Asylum: Social Order and Disorder in the
New Republic (Boston: Little, Brown, 1971), p. xii.

73. The two leaders in this self-consciously anti-Christian public school move
ment were Horace Mann and James G, Carter: R. J. Rushdoony, The Messianic
Character of American Education; Studies in the History of the Philosophy of Education
(Nutley, New Jersey: Craig Press, 1963), chaps.

74, Rushdoony, dnstitutes, pp. 514-16.

75. Samson Raphael Hirsch, The Pertatewch Translated and Explained, tvanslated by
Isaac Levy, 5 vols., Exodus (3rd ed.; London: Honig & Sons, 1967), p. 294: at Exodus

 

 

 

 

 

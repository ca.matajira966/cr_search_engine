34 TOOLS OF DOMINION

on the other hand, which was applied by the Reformers to what they
called ‘Old-Timers’ or ‘Old-Believers’ (Altglaubige), was taken from
the sphere of Christian theology and does not fit Judaism at all, in
which the main stress is laid on action or law and not on ‘faith’, as the
Greek term orthodox would express. Nevertheless, once the word
‘Orthodoxy’ had been thrown at Hirsch and his followers in a derog-
atory sense, he accepted the challenge with the intention of turning
that word into a name of honour.” Notice his assertion regarding
Judaism that “the main stress is Jaid on action or Jaw and not on
faith.’” This is indeed the main stress of orthodox Judaism, which
nevertheless has an underlying theology: salvation by law, Writes
Robert Goldenberg: “Classical Judaism, drawing indirectly on its
biblical antecedents, tends to emphasize act over intention, bchavior
over thought. Righteousness is chiefly a matter of proper behavior,
not correct belief or appropriate intention.” In contrast, Christian-
ity stresses salvation by faith in Christ, But this faith means faith in
Christ's representative perfect obedience to God's perfect law, Christian ortho-
doxy should never lead to a denial of the validity and moral authority
of that perfect law which Christ obeyed perfectly.

Revolution and Law

Tam convinced that both the West and the Far East are about to
experience a major transformation. The pace of social change is
already rapid and will get faster. The technological possibility of a
successful Soviet nuclear strike against the United States grows
daily;15 so does the possibility of chemical and biological warfare; 1®
so does the threat of an AIDS epidemic. None of these threats to civ-
ilization may prove in retrospect to be devastating, but they are cer-
tainly perceived today as threats. Added to these grim possibilities is

13. 1. Grunfeld, “Samson Raphael Hirsch—The Man and His Mission? in
Judaism Eternal: Selected Essays from the Writings of Seanson Raphael Hirsch (London: Son-
cina Press, 1956), p. xlvii.

14. Robert Goldenberg, “Law and Spirit in Talmudic Religion, in Arthur Green
(ed.), Jewish Spirituality: From the Bible Through the Middle Ages (New York: Crossroad,
1986), p. 232.

15. Angelo Codevilla, While Others Build: The Commonsense Approach to the Strategic
Defense Initiative (New York: Free Press, 1988); Quentin Crommelin, Jr., and David
S. Sullivan, Soviet Military Supremacy (Washington, D.C.: Citizens Foundation,
1985),

16. Joseph D. Douglas and Neil ©, Livingstone, America the Vulnerable: ‘the Threat
of Chemical and Biological Warfare (Lexington, Massachusetts: Lexington Books,
1987).

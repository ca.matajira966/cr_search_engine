422 TOOLS OF DOMINION

selling himself into indentured servitude, with the buyer paying the
victim. But perhaps the convicted man would prefer to lose the use
of part of his body rather than becoming a bondservant. He could
reject the demand of the victim for economic restitution and insist
instead on his legal right under biblical law: to suffer the same physi-
cal mutilation that he had imposed on the victim.

The Right to Punishment

Each of the parties in this judicial dispute has biblically specified
legal rights. The victim has the right to insist on the biblically speci-
fied maximum physical sanction: eye for eye. He also has the right
to offer the criminal an alternative, one which appears to be less
severe than the biblically specified physical sanction. If the alterna-
tive offered to the criminal is not regarded by him as less severe, then
he has the legal right to insist on the imposition of the biblically spec-
ified maximum sanction, He therefore possesses the right to be pun-
ished by the specified biblical sanction, His punishment is limited by the
extent of the injury which he imposed on his victim, The punish-
ment fits the crime.

It is basic to the preservation of liberty that the State not be
allowed to deny to either the victim or the criminal his right of pun-
ishment. While this principle of the right to punishment is at least
vaguely understood by most people with respect to the victim, it is
not well understood with respect to the criminal. The right to be
punished is a crucial legal right, one which Paul insisted on at his
tial: “For if I be an offender, or have committed any thing worthy of
death, I refuse not to die: but if there be none of these things whereof
these accuse me, no man may deliver me unto them. I appeal unto
Caesar” (Acts 25:11).

If the State can autonomously substitute other criteria for de-
served punishment, such as personal or social rehabilitation, then
society loses its right to be governed by predictable laws with pre-
dictable judicial sanctions. The messianic State then replaces the
judicially limited State. Neither the victim nor the criminal can be
assured of receiving justice, for justice is defined by the State rather
than by God in the Bible. Ef punishment is not scen as deserved by the
criminal, and therefore his fundamental right, then he is delivered into
the “merciful” hands of elitist captors who are not bound by written
law or social custom. No one has described this threat more elo-
quently than C. S. Lewis: “To be taken without consent from my

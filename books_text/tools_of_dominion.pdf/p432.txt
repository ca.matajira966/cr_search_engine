424 TOOLS OF DOMINION

the universal victim of all crime to which is owed universal restitu-
tion by criminals and taxpayers alike, it has asserted its own divinity.

Benefits of Alternative Sanctions

The proposed economic solution to the dilemma of the dex talionis
offers at least three very real benefits. The first benefit is judicial: the
victim has the right to specify the appropriate punishment. This punishment
is limited only by the maximum penalty specified by biblical law, eve
for eye. The biblical principle of victim’s rights is upheld by the
judges. If the victim believes that the criminal’s act was malicious,
and if he wishes to inflict the same damage on the criminal which he
himself suffered, this is his legal option.

To take this retributive approach, however, he necessarily forfeits
all the economic advantages he might have received from a restitu-
tion payment from the criminal. He can exercise his legitimate
desire for vengeance —his desire to reduce the criminal to a physical
condition comparable to his own ~but this desire for vengeance has
a price attached to it. He is made no better off financially because of
his enemy’s suffering. In fact, he could be made slightly worse off:
he, as a member of the economic community, loses his portion of the
other man’s lost future productivity, assuming the man cannot over-
come the effects of his lost eye or limb. Vengeance in the Bible's judicial
system has a price tag attached to it, This inevitably reduces the quantity
of physical vengeance insisted on by victims, for biblical civil justice
recognizes the judicial legitimacy of a fundamental economic law:
“The higher the price of any economic good, the less the quantity
demanded.”

The second benefit of this interpretation of lex talionis is also judi-
cial: the criminal who is about to lose his eye or tooth is permitied to make a
counter-offer. He has the right to be punished to the limit of the written
law, but he also can suggest a. less onerous punishment—less
onerous for him, but possibly more beneficial to his victim. He can
legally offer money or services in exchange for the continued preser-
vation of his unmutilated body. The system puts him in the position
of being able to pay in order to retain his limbs. He places a price tag on
his body.

This price tag makes it costly for the victim to pursue an emotion
which, had there been no crime, would be called envious: the desire
to tear another person down, irrespective of the direct benefits to the

426 TOOLS OF DOMINION

Insurance for Criminals?

Should the victim be denied the option of specifying the form of
vengeance? Does it thwart justice to set up a judicial system where a
rich criminal can offer to “buy his way out”? Worse, what if his rich
insurance company can offer to buy his way out?

If criminals could escape the likelihood of physical violence by
means of monetary restitution, they might start buying insurance
contracts that would enable them to escape the economic penalty of
inflicting physical violence. This could be regarded as licensing
criminal behavior. No one is going to co-ingure another man’s cye
with his own eye, but the public has already set up co-insurance for
monetary claims. Thus, by allowing economic restitution for crimes of
violence, criminal behavior might be made less costly to the criminals.

One answer to this objection is that insurance companies are un-
likely to insure a person from claims made by victims if the man is a
repeat violator. The risk of writing such contracts is too high. Private
insurance contracts are designed to be sold to the general public, and
to keep premiums sufficiently price competitive, sellers exclude peo-
ple known to be high risks. Low-risk buyers do not want to pay for
high-risk buyers. Furthermore, insurance policies often specify that
the coverage is for civil damages rather than criminal acts. This. is
true of most automobile insurance policies. Policies specify exactly
what is to be covered—the famous insurance industry principle of
“the large print giveth, but the fine print taketh away.”

Policies actually designed by criminals to co-insure would be ex-
tremely unlikely. Violent criminals seldom think ahead. They do not
work well with others. They are essentially anti-social people. A sys-
tem of insurance company-subsidized crime could not last very long
without government financial aid.

The Auction for Human Flesh

By allowing the substitution of an economic payment for actual
physical disfigurement, the judges unquestionably do authorize an
auction for human flesh. If a convicted criminal is allowed to pay the
victim in order to avoid physical mutilation, he is participating in an
auction. Such an implicit auction may sound crass, but so does pok-

19. If the criminal could “buy his way out” by bribing the judges, then justice
would be thwarted, But judges in a biblical system represent the victims, nol the
State. If they represent a victirn who wishes to be “bought off,” where is the injustice?

36 TOOLS OF DOMINION

of traditional Roman Catholic and Protestant college instructors and
magazine columnists still visibly cling to one or another of these tax-
idermic specimens, each proclaiming that his specimen is still alive.)
Thus, there is nowhere for Christians to turn for guidance in devel-
oping a believable social theory and workable social programs except
to the case laws of the Old Testament. Once the myth of neutrality is
abandoned —really abandoned, not just verbally admitted to be a
myth — then the inevitable question arises: By what standard? Chris-
tians who have abandoned faith in the myth of neutrality have only
one possible answer: “By éhis standard: biblical law.”

The Conflict Between Two Kingdoms

What I am attempting to do with my life is to publish Christian
worldview materials that will lead to the steady replacement of the
humanist intellectual foundations of modern civilization. The arena
of conflict is nothing less than world civilization. The issue is the
kingdom of God, both in heaven and on earth (Matt. 28:18). There
are many books that deal with the kingdom of God, but my view of
the kingdom of God as it is visibly manifested in history is simple: it
is God’s authorized and morally required civilization. It is simultan-
eously internal (world-and-life view), ethical (a moral law-order),
and institutional (covenantal judicial relationships), Raymond Zorn
begins his book on the Kingdom of God with these words: “In the
broadest sense God’s Kingdom refers to the most extended reaches
of His sovereignty. As Psalm 103:19 puts it, ‘The Lord hath prepared
his throne in the heavens; and his kingdom ruleth over all.’”2! The
kingdom of God is all-encompassing, in the same sense that a civili-
zation is all-encompassing.” I agree in principle with the Jewish

20. Greg L. Bahnsen, By This Standard: The Authority of God's Law ‘Today (Tyler,
Texas: Institute for Christian Economics, 1985).

21. Raymond Q. Zorn, Church and Kingdom (Philadelphia: Presbyterian and
Reformed, 1962), p. 1, Zorn, an amillennialist, stresses the kingdom as the reign of
God rather than the sphere or domain of His rule (p. 1), Greg Bahnsen’s response to
this sort of argument is correct: it is ridiculous to speak of the reign of a king whose
kingdom has few if any historical manifestations that are as comprehensive in svope
as his self-proclaimed sovereignty. Such a limited definition of God’s kingdom and
kingship is in fact a denial of God’s kingdom. Bahnsen, “The World and the
Kingdom of God” (1981), reprinted as Appendix D in Gary DeMar and Peter J.
Leithart, fhe Reduction of Christianity: A Biblical Response to Dave Hunt (Ft. Worth,
‘Texas: Dominion Press, 1988).

22. The reader should not misinterpret what I am saying. T am not saying that
the kingdom of God is (he primary theme in the Bible, or in the message of Jesus.
His primary theme is the same as the whole Bible's primary theme: éhe glory of Gud, I

  
 

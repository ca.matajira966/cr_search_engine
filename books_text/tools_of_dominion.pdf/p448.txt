440 TOOLS OF DOMINION

servant's physical loss. Second, there could also be loss as a result of
the mistreated bondservant’s resentment. He will not perform as ex-
pected. The market's forces af profit and loss restrain the bondservant-owner,
The civil authorities can presume that the bondservant-owner is not
going to mistreat his bondservant physically to the extent that the
bondservant’s performance will be seriously impaired. Because of the
competitive market for the bondservant’s economic output, civil authorities can
more safely delegate authority to the bondservant-owner, This decentralizes
power in the society, The competitive market, through the self-interest
of the bondservant-owner, serves as an institution that restrains the
illegitimate use of power. The economic costs of lawless behavior are
borne by the bondservant-owner. This is true of all capital resource
ownership. This is why the bondservant’s economic position as a
capital asset protects him.

Bondservants are understood to be potentially rebellious. This is
clearly true in the case of criminals who are sold to masters in order
tu raise money for the restitution payment to the victims. But rebel-
lion is not limited to criminals. Men are by nature rebellious. They
resist authority, both lawful and unlawful. Adam rebelled against
God; bondservants rebel against masters. Without a means of en-
forcing lawful authority, no form of external government could exist.
The bondservant system is an aspect of family government in the
Bible. Thus, the bondservant-owner possesses the legitimate author-
ity to inflict limited physical punishment. What the Bible restrains is
punishment that inflicts permanent physical damage.

There are scveral reasons that we can presume for this prohibi-
tion. First, men are made in God’s image, and therefore they deserve
protection. Second, interpersonal relationships between people are
threatened when one person has sccmingly unlimited power to im-
pose his will on another. Punishment is supposed to increase respect
for the law, the master, and God on the part of the bondservant, not
foster an urge to revenge because of the outragcous nature of some
type of punishment, Evil calls forth evil. Third, permanent injuries
generally restrict people’s ability to excercise dominion. Punishment
is not to thwart the dominion covenant. Fourth, a man’s spirit can be
broken by continual, ruthless beatings. Without the protection of
law, the victim may see himself as exploited and without hope. This
alse conflicts with the psychology of dominion. The law provides
him with an area of safety. He is to increase his dominion by his sub-
servience to God’s law, This, in fact, is one of the functions of inden-

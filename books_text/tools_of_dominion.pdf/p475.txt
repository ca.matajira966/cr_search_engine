The Ransom for a Life 467

poses liability to pay the value of such infants on humans only.” Be-
cause the ox did it, and is not a human, its owner is exempt; the trans-
fer of liability upward to the owner is cut short, because the ox cannot
be held responsible. He did admit that if the ox gores a pregnant bond-
woman, and the same thing happens, the owner is financially liable
in this case, “for this is as if the ox gored a she-ass about to foal.”*
Oxen are responsible for damaging other animals, so this responsi-
bility is transferred upward to owners, unlike the previous case.

On the other hand, Maimonides was very hard on the animal as-
sociates of a condemned criminal ox. “If its trial has been concluded
and it then becomes mixed with other oxen—even with a thousand
others— all must be stoned and buried and are forbidden for use, as
is the rule concerning any animal condemned to be stoned.”*5 Own-
ers of friendly oxen were forewarned by Maimonides: don’t let your
law-abiding beasts fall in with bad company!* (After reading
Maimonides’ Code in detail, this gentile begins to suspect that pre-
modern Rabbinic reasoning regarding the case laws is very different
from his own.)

We know that an ox that had gored another ox had to be sold by
its owner to a third party (Ex. 21:35). Thus, to be the owner of an ox
that had been convicted of goring, he would have had to go out and
repurchase the offending ox, or else he is the person who bought the
offending ox. In either case, he had taken active steps to buy a
known offender. To have done this, and then to have refused to take
active measures to restrain it, should make him legally vulnerable to
the charge of negligence.

Would other evidence rather than a prior conviction be a suffici-
ent warning? What if neighbors had reported the beast to the au-
thorities? If the authorities had issued a formal warning to the
owner, would this serve as evidence of its status as a habitual

23, Ibid.; Chapter Eleven, Section Three, p. 40.

24. Idem., Section Four.

25. Jbid., Chapter Eleven, Section Ten, p, 41,

26. What Maimonides and the rabbis failed to understand is this: the guilt of a
murderous animal is covenantal, not metaphysical. The evil that the animal has
committed is not passed to other animals by mere physical contact or proximity. The
evil act of the animal was rebellion against the fear of man that God places in every
animal's heart (Gen. 9:2). It had trespassed the moral boundaries that God placed in
ils heart. Maimonides was more concerned about the boundary between the con-
victed animal and other animals than with the boundary inside the animal between
it and mankind, and the physical boundary between the animal and his last three
human victims.

 

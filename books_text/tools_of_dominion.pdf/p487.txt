The Ransom for a Life 479

public utilities that possess regional monopolies. The “Price-Anderson”
legislation of the 1950’s sets relatively low ceilings for financial liabil-
ity by such firms — $560 million per accident**— and then the federal
government collects the prernium money. By limiting its liability, the
federal government forces residents who live near nuclear power
sites to co-insure against a disaster, since there is a maximum pay-
out per accident. The larger the local population that could be
affected, the more each resident co-insures, for the lower the per
capita payments would be. Taxpayers also co-insure: in case of an
accident, the tiny federal nuclear accident insurance fund could not
pay off more than 2 percent of a single $560 million damage suit.
Money taken from the federal government’s general fund would
have to make up the difference. Because of this federal legislation,
public utilities have been able to expand the use of nuclear power
generation, In this sense, today’s nuclear power industry has not
been the product of a free market economy; it has been the product
of special-interest legislation in the form of liability maximums and
compulsory State insurance coverage.”

The Free Market's Response

Liability insurance is another example of a free market, scientific
development that protecis the victims without bankrupting those
who are personally responsible. The victims receive more money

46, Idem.

47, Anti-nuclear power advocates tend to be anti-free market, and usually blame
the free market for the nuclear power industry. Nuclear power proponents usually
are pro-free market, so they seldom talk about the statist navure of the subsidy, But
when the chips are down, the pro-nuclear power peuple accept federal subsidies to
their program as being economically and ideologically valid. Writes nuclear power
advocate Petr Beckman: “Yes, the American taxpayer has paid §1 billion to research
nuclear safely, and I consider that a good investment, . . .” Beckman, The Health
Hazards of NOT Going Nuctear (Boulder, Colorado: Golem Press, 1976), p. 54. He
also argues that the Price-Anderson insurance program makes money for the federal
government because power companies pay premiums to Washington, along with
money sent to private insurance pools, “You call that a subsidy?” he asks rhetorically
(p. 156). Of course it is a subsidy. The premium rates are far belaw market rates,
even assuming private firms would insure against a nuclear power plant disaster,
which is doubtful. The maximum liability is fixed by law far below what would be
demanded in a court if some major nuclear accident took place in a populated area.
This is why the Price-Anderson legislation was cnacted in the first placc: to subsidize
the power industry by reducing its legal liability and its insurance rates. Further-
more, taxpayers are co-insuring: the fund which is Lo cover all power companies na-
tionally was $8 million, as of 1976; the liability was $560 million per accident. Tax-
payers would have to make up the difference.

 

 

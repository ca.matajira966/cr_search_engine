502 TOOLS OF DOMINION

lowing the Mishnah," he listed the wolf, lion, bear, panther, and
leopard. He also added snakes, but strangely enough, only those
that have bitten in the past.!# These species would today be classified
as “exotic animals.” Most communities in the United States place
legal restrictions on the private, non-institutional ownership of such
animals, and in many cases such ownership is banned by law. To
these species should probably be added species of dogs that have
been bred to be fighters. The very possession of such breeds places
the owners at risk. The individual animal may not be known to be
dangerous, but it can be presumed in advance by the owner to be
dangerous, and therefore also by the court retroactively.

Limited Knowledge

The court's knowledge is limited, yet it has to have evidence to
make a judgment. The only evidence sufficiently reliable to allow the
court to presume guilt on the part of a beast is the beast’s previous
public record. Why must the court presume guilt? Because there is
no way for the court to determine guilt with the same degree of accu-
racy that must prevail in deciding human transgressions of the law,
where the innocence of the accused is presumed. '>

First, let us consider the case of the goring of a prize-winning
beast by a previously peaceful ox. The prize-winning beast’s owner
has to bear the increased risks associated with ownership of a cham-
pion beast. He has to assess the risks of putting it in close contact
with other beasts, Neither he nor the owner of the previously tame
beast had special knowledge of the future behavior of either beast.
Neither owner possessed a uniquely inexpensive way to gain such
knowledge. Therefore, neither owner is to be assessed by the court
with special burdens of responsibility, since the knowledge of each is
presumed to be the same. It might have been the champion beast
that was the potential killer.

Second, in the case of the owner of a known renegade beast, the
court can presume that he had access to better knowledge concern-
ing the behavior of his beast than the dead beast’s owner had with
respect to either beast. Becausc the owner of the renegade had greater

11. Baka Kamma 1:4, Mishnah, p. 332.

12. Torts, “Laws Concerning Damage by Chattels,” Chapter One, Section Six,
p. 5.

13. I am speaking here of common taw societies. Napoleonic Code societies da
presume that the accused is guilty unless proven innocent.

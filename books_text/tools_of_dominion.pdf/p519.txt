Proportional Restitution Sul

In short, the present price of any scarce economic resource
already includes its expected future price, discounted by the appli-
cable period’s market rate of interest.?!

I. The Economics of Restitution

Having said this, we now consider the economics of restitution.
The task of the judges in estimating a morally legitimate restitution
payment is easier than it seems. Judges can safely ignore the ques-
tion of just how much the future value of a stolen asset might be.
The best experts in forecasting economic value—entrepreneurs—
have already provided this information to the judges, all nicely dis-
counted by the market rate of interest. The judges need only use ex-
isting market prices in order to compute restitution payments.

A restitution payment is normally twice the prevailing market
price of the asset. When the stolen ox is returned by the authorities
to the owner (the thief neither slaughtered it nor sold it), the thief
pays double restitution. “If the theft be certainly found in his hand
alive, whether it be ox, or ass, or sheep; he shall restore double” (Ex.
22:4), Rushdoony follows the traditional rabbinical interpretation
when he argues that this 100 percent penalty above the market price
is the minimum amount by which the thief expected to profit from
his action.” The thief must return the original beast, plus his ex-
pected minimum “profit” from the transaction, namely, the market
value of the stolen beast. He forfeits that which he had expected to
gain. Maimonides wrote of the requirement that the thief pay dou-
ble: “He thus loses an amount equal to that of which he wished to
deprive another.”2? Akedat Yizhak concurs: “The thief is treated
differently from the one who causes damage. The latter who caused
damage through his ox or pit did not intend to deprive his fellow of
anything. He is therefore only required to make half or total restitu-
tion. The thief who deliberately sets out to inflict loss on his fellow
deserves to have a taste of his own medicinc—to lose the same
amount that he deprived his fellow of. This can only be achieved

21, The prevailing rate of interest for loans of any given duration, like the pre-
vailing price of any asset, is the product of the hest guesses of entrepreneurs (specu-
laters) concerning the future of interest rates of that duration.

22. Rushdoony, /nstiiutes, p, 460.

23. Moses Maimonides, The Book of Torts, vol. 11 of The Code of Maimonides, 14
vols, (New Haven, Connecticut: Yale University Press, [1180] 1954), “Laws Con-
cerning Theft,” Chapter One, Section Four, p. 60.

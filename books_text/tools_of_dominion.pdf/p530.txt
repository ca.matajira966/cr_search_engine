522 TOOLS OF DOMINION

cerning his adultery with Bathsheba, wife of Uriah the Hittite,
Nathan proposed a legal case for David to judge. A rich man steals a
female lamb from a poor neighbor, and then kills it, “And David's
anger was greatly kindled against the man; and he said to Nathan,
As the Lorp liveth, the man that hath done this thing shall surely
die: And he shall restore the lamb fourfold, because he did this
thing, and because he had no pity” (II Sam. 12:5-6). Then Nathan
replied to him, “Thou art the man.” Uriah had been the neighbor;
Bathsheba is the ewe lamb who, biblically speaking, has been killed,
the death penalty being applicable in cases of adultery (Lev. 20:10).

David recognized that the four-fold restitution was applicable in
the case of stolen and slaughtered sheep. But in fact, Nathan was not
talking about a lamb; he was talking about a human being. He used
the symbol of the slaughtered sheep for the foolish woman who con-
sented to the capital crime of adultery. The woman had been en-
titled to protection, especially by thc king. Instead, she had been
placed in jeopardy of her life by the king. The king had proven him-
self to be an evil shepherd.

What was the penalty extracted by God? First, the infant born of
the illicit union would die, Nathan promised (II Sam. 12:14). On the
seventh day, the day before its circumcision, the child died (v. 18).
The next section of Second Samuel records the rape of Tamar by
David's son Amnon. Absalom, her brother, commanded his servants
to kill Amnon, which they did (II Sam. 13:29). Absalom revolted
against David and was later slain by Joab (II Sam. 18:14). Finally,
Adonijah attempted to steal the throne, but Solomon was anointed
(I Ki, 1), and Adonijah tried again to secure the throne by asking
Solomon to allow him to marry David’s bed-warmer, Solomon rec-
ognized this attempt to gain the throne through marriage, and had
him executed (I Ki, 2:24-25). Thus, four of David's sons died, fulfill-
ing the required four-for-one punishment for his adultery and his
murder of Uriah.*

34, The Jewish scholar Brichto recognizes the connection between Exodus 22:1
and the death of four of David’s sons, His comment on the fourth of the four-fold
penalty that God imposed on David is pertinent: “The exccution of Adonijah, oc-
curring after David's death has, in this contest, escaped general notice: even of
scholars, who have becn conditioned not to count as significant (for hiblical man)
what happens to a man’s son(3) after his demise.” Herbert Chanan Brichta, “Kin,
Cult, Land and Afterlife— A Biblical Complex,” Hebrew Union Gollege Annual, XLIV
(1973), p. 42.

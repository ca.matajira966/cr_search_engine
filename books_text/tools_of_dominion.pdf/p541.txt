Proportional Restitution 533

Jesus was not departing from the biblical view of judicial sanc-
tions when He warned: “Fear him which is able to destroy both soul
and body in hell” (Mart. 10:28b). It is eternal punishment which is to
serve as the covenantal foundation of all judicial sanctions. Civil
government is supposed to reflect God’s government. Public punish-
ments deter evil. They remind men: better temporal punishment
that leads to repentance (personal and national) than eternal punish-
ment that does not lead to repentance (personal). Repentance is pos-
sible only in history.

Capital Punishment

Phillips is consistently incorrect when he writes: “Modern theories
of punishment are therefore totally inapplicable when considcring
reasons why ancient Israel executed her criminals, for the punishment
was not looked at from the criminal’s point of view. This extreme
penalty was not designed to deter potential criminals, nor as an act
of retribution, but as a means of preventing divine action by appeas-
ing Yahweh’s wrath.”*! If criminal law was “not looked at from the
criminal’s point of view,” then why does the Bible repeatedly refer to
the fear of external punishment by the civil authorities as a means of
leading men to fear God and to obey His law? “And all Israel shall
hear, and fear, and shall do no more any such wickedness as this is
among you” (Deut. 13:11). Deterring future crimes is certainly one of
the functions of capital punishment in a biblical law-order. Capital
punishment is also an act of retribution and restitution. And, yes, it
is also “a means of preventing divine action by appeasing Yahweh's
wrath.” It is erroneous to argue exclusively in terms of “either-or”
when considering the potential social motivations for capital punish-
ment or any other required civil sanction in the Bible.

51. Phillips, Ancient Israel's Criminal Law, p. 12.

52. I do not want to give the reader an inflated opinion of Phillips’ importance,
He is just another obscure liberal theologian toiling fruitlessly in the barren wilder-
ness of higher criticism, I have included this brief survey of some of his ideas as an
example of just how intellectually sloppy liberal theology can be, not because he is
an important thinker, He is simply a convenient foil. He is all too typical of a small
army of liberal theologians whose works would be immediately forgotten if they had
ever been read in the first place. These scholars will eventually make full restitution
to God for their efforts to deceive their readers concerning the Bible.

Liberal scholars are always looking for a new angle lo justify the publication of yet
another heavily foomoted, utterly boring, totally useless book, especially books like
Phillips’, which is a rewritten doctoral dissertation—the most footnoted, boring, and
useless academic exercise of all, Doctoral dissertations should be interred quietly, pref-
erably in private, with only the author and closc family in attendance, If such interment
must be public, then it should be as a summary published in a scholarly journal, where
the remains’ enrombment will seldom be disturbed again. Ashes to ashes, dust to dust.

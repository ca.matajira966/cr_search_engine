542 TOOLS OF DOMINION

performance. The ultimate economic issue is each person’s stewardship
over property in history and God’s judicial response, in history and at
the final judgment. The temporal institutional issues of ownership-
stewardship are covenantally related to this ultimate issue.

These verses make plain at least three facts. First, the Bible
affirms the moral and legal legitimacy of the private ownership of the
means of production. Fields and cattle and crops are owned by pri-
vate individuals. Second, private property rights (legal immunities
from action by others) are to be defended by the civil government.
The State can and must require those people whose activities injure
their neighbor or their neighbor’s property to make restitution pay-
ments to those injured. Third, owners are therefore responsible for
their own actions and for the actions of their subordinates, including
wandering beasts.!

This combination of 1) privately owned property, 2) personal
liability, and 3) predictable court enforcement of private property
rights is the foundation of capitalism. It surely was a major aspect of
the West's long-term economic growth.? But as this chapter argues,
this property ownership arrangement is also important for the reduc-
tion and allocation of pollution.

We begin with the case of the wandering animal. it wanders from
its property and invades another man’s corn field. It eats some of
this corn, The owner of the beast owes the victimized neighbor the
equivalent of whatever has been destroyed.? The owner of the beast
must not short-change the victim; he pays from the best of his field.
The legal principle is that the injured party is entitled to the replace-

1, Hammurabi’s Code penalized a man who neglected to repair a dike on his
property, which in turn broke and allowed his neighbor's property to be flooded:
GH, paragraph 53. If he allowed water to flow through his canal and onto his neigh-
bor’s property, he was liable: CH, paragraph 95. Ancient Near Eastem Texts Relating to
the Old Testament, edited by James B. Pritchard (3rd ed.; Princeton, New Jersey:
Princeton University Press, 1969), p. 168.

2, Nathan Rosenberg and L. E. Birdsell, Jr., How the West Grew Rich: The Eco-
nomic Transformation of the Industrial World (New York: Basic Books, 1986), ch, 4: “The
Evolution of Institutions Favorable to Commerce”

3. Maimonides made this peculiar exception: “If an animal eats foodstuffs harm-
fal to it, such as wheat, the owner is exempt because it has not benefited.” Moses
Maimonides, The Book of Torts, vol. 11 of The Code of Maimonides, 14 vals (New Haven,
Connecticut: Yale University Press, [1180] 1954), Chapter Three, Section Three, p.
12. That the victim must suffer an economic loss just because his neighbor's animal
did not profit biologically from its invasion of the former's property is a principle of
Justice that needs a great deal of explaining. Maimonides provided no further dis-
cussion; he just laid down this principle of Jewish law, and went on,

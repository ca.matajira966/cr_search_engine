Pollution, Ownership, and Responsibility 558

that the fire which 2 man starts is his responsibitity. He cannot legally
transfer risks to his neighbor without his neighbor's consent.

The Bible is not talking here about some shared project in which
both men expect to profit, such as burning fields to get rid of weeds
or unwanted grass. In such a mutually shared project, the case-law
example of the man who rents his work animal to a neighbor, but
who stays with the animal the whole time, is applicable, The neigh-
bor is not required to pay anything beyond the hiring fee to the
owner (Ex. 22:14-15). If the animal is hurt or killed, the neighbor
owes nothing. (If the two men start a fire that spreads to a third
party’s property and damages it, English common law holds both of
them responsible, though not necessarily in equal economic por-
tions, since the victim can collect more money from one than
another!6— what we might call “deeper packers jurisprudence.” Such
a logal tradition makes joint activitics between rich men and poor
men less likely; the rich person, if he is aware of the law, knows that
he will be required by the court to pay the lion’s share of any joint
restitution, simply because he can pay it more easily.)

There is no doubt that the fire-starter is responsible for all subse-
quent fires that his original fire starts. Sparks from a fire can spread
anywhere. A fire beginning on onc man’s farm can spread to thou-
sands of acres. Fire is therefore essentially unpredictable. Its effects
on specific people living nearby cannot be known with precision. I
adopt the principle of uncertainty, meaning the unpredictability of the
specific, individual consequences of any fire, as the governing princi-
ple of my discussion of restitution fur damage-producing fires, as
well as laws relating to the regulation of fire hazards.

What about pollution? Specifically, what about the uncertainty
aspect of pollution? A Christian economist should argue that a man
must not pollute his neighbor’s property without making restitution
to him for any new damaging effects. If existing pollution is discov-
ered to be more harmful medically or ecologically than had been
understood before, the polluter should be required to reimburse
those who are suésequently affected adversely by the pollutant after the
information concerning the danger is made public by the State or be-
comes known within the polluting industry. (I will consider the legal
and economic problems associated with retroactive responsibility in
a subsection of this chapter, “Undiscovered Risk.”)

16. Posner, Economic Analysis of Law, pp. 171-73.

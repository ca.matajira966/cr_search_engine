Pollution, Ownership, and Responsibility 555

ing the costs and benefits of their actions. There is no escape from
this economic responsibility. “No decision” is still a decision. If an
asset is squandered, the owner loses.

The chief failure of what is commonly referred to as collective
ownership is that no individual can be sure that his assessment of the
costs and benefits of a particular use of any asset is the same assess-
ment that those whom he represents would make. The tendency is
for individuals who are legally empowered to make these represen-
tative decisions to decide in terms of what is best for them as indi-
viduals. There is also a tendency for the decision-maker to make
mistakes, since he cannot know the minds and desires of the commu-
nity as a whole.

The common property tends to be wasted unless restraints on its
use are imposed by the civil government. The “positive feedback”
signals of high profits for the users are not offset by equally con-
straining “negative feedback” signals. Users of a scarce economic re-
source benefit highly as immediate users, yet they bear few costs as
diluted-responsibility collective owners. Thus, in order to “save the
property from exploitation,” the civil government steps in and regu-
lates users. This leads to political conflicts.

The biblical solution to this problem is to establish clear owner-
ship rights (legal immunities) for property. The individual assesses
costs and benefits in terms of his scale of values. He represents the
consumer as an economic agent only because he has exclusive use of the
property as egal agent, He produces profits or losses with these assets
in terms of his abilities as an economic steward. The market tells
him whether he is an effective agent of the competing consumers.

The legal system simultaneously assigns responsibility for the ad-
ministration of these privately owned assets to the legal owners. It
becomes the owners’ legal responsibility to avoid damaging their neigh-
bors through the use of their privately held property. The specific
biblical classification of fire damage governs pollution in general.

There is no doubt that living close to a source of pollution in-
creases the risk of suffering economic losses. The market reveals this
by forcing sellers of polluted or nearly polluted land to offer dis-
counts to buyers. This leads us to conclude that if a person has
bought a piece of property at a discount because of its proximity to a
known source of pollution, the buyer has no legal claim against the
polluter unless the latter adds to the level of pollution or else new
dangers regarding the pollution itself are subsequently discovered.

 

Pollution, Ownership, and Responsibility 559

Men know about fire’s general potential for creating damage. It is
a dangerous tool. In contrast, a particular form of pollution may not
be known to be dangerous generally, although it is known to be a nutsonce
specifically, The nuisance factor is what provides the victim with his
discount when he buys the neighboring property —a discount appro-
priate to the known side-effects of the polluting process. The limited
but known effects of the polluting activity can be dealt with by the
victims. They receive compensation in adyance in the form of dis-
counted land purchase prices for relatively predictable damages.

The problem of uncertainty concerning pollution has increased
since the end of World War II. The development of the petro-
chemical industry has created new problems associated with toxic
wastes. The physiological effects of today’s forms of pollution may
not be well known. Uncertainty increases, making some forms of
pollution more like the example of uncontrolled sparks than like
smoke, whose effects are not lethal, The modern legal system has
struggled with the implications of the new technology:

However, modern chemicals are suspected of causing physical injuries,
such as cancer, and certain emotional dysfunctions having etiologies that
are little understood by science or medicine. One of the most significant
characteristics of the development of these types of diseases is their latency,
the time between exposure and expression of the disease. For example, a
few types of cancer have a latency period of 20 to 30 years while some
mutagenic diseases may take a generation or more to become evident.
Moreover, chemicals suspected of causing such discases often function at
low concentrations, ¢.g., parts per billion, or perhaps a single molecule. In
addition, pollution injuries, unlike common traumatic injuries, may be in-
flicted on many persons located far frum the pollution source.

Particularly baffling is their unpredictability. If a heavy beam falls upon
a worker, the injury will be much the same regardless of who is struck. Ex-
posure to identical concentrations of a given pollutant, however, may pro-
duce reactions varying from no observable ailment to a life-threatening
emergency.

These characteristics create unprecedented uncertainty, thereby chal-
lenging the ability of the judicial system to perform its traditional role of
balancing the availability of compensation for individual injury against the
social benefits of the injury-causing agent.”

 

22. Robert K. Best and James I. Collins, “Legal Issues in Pollution-Engendered
Torts," Cato Jounal, I (Spring 1982), pp. 104-5. Scc also Peter Huber, Liability: The
Legal Revolution and Its Consequences (New York: Basic Books, 1988), pp. 67-70, 112-14.

564 TOOLS OF DOMINION

the generally known but highly unpredictable specific, individual produc-
tion costs of fire. Economic analpsis must begin with the Bible's assessment of
personal responsibility for a man’s actions, Tt must begin with the presup-
position of the rights (legal immunities) of private property. These
rights must be protected by civil law and custom.

The act of polluting someone else’s environment is a crime in
those cases where either production costs or consumption costs” (in-
cluding risks) that are known to a polluter but unknown to the victim
are deliberately imposed on the victim, It is also a crime when someone
begins a new and previously unpredicted polluting process without
getting permission from future victims. In both cases, it is an at-
tempt on the part of a beneficiary to “externalize” his costs of produc-
tion or consumption by passing them along to others who do not
profit directly from the production process or consumption activity.
He lowers his costs by transferring a portion of these costs to inno-
cent victims.

We can grasp the economics of pollution quite easily in the case
of a manufacturer, Polluting allows him to retain a greater net in-
come whet he sells the goods, and it eventually allows him to increase
output until his personally borne marginal costs equal his personally re-
cewed marginal revenues, i.¢., until he arrives at that level of output at
which he loses money by producing one more item. But the total costs
of production are higher than his personally borne marginal costs.
These additional costs—costs above his personally borne costs—are
involuntarily borne by the victims of his pollution. So he continues
to expand production above the level of output that he would have
produced had he borne the full costs of his production process. If he
is not required by law to share these marginal benefits with victims
(restitution), and if he is also allowed to continue to pass on some of
his production costs to them, then the law has created an incentive to
overproduce this particular product.

It must be understood that there are many beneficiaries of this
overproduction — overproduction that is subsidized by the victims of the
pollution, Obviously, the owner of the firm benefits. Another group
of beneficiaries is his customers, who can buy more goods at the same
price, or the same number of goods at a lower price, than before the
pollution process began. Third, there are employees of his company.

28. An example of a consumption cost which produces net losses for a neighbor
would be the keeping of pets that bark or bite or otherwise disturb the neighbor.

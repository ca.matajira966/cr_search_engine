Pollution, Ownership, and Responsibility 583

they are wealthy.°> As men’s per capita income rises, they tend to
worry less about where the next meal is coming from and more
about their “quality of life,” meaning their physical environment,
‘The West does pollute the environment, but as men get richer, they
tend to buy more services than goods. As national wealth increases,
capital shifts to the service sector, and to high-technology, low-pollution
production. Yet as thie level of pollution may be dropping —or shift-
ing from, say, horses to autos, from manure-filled streets and flies to
smog and stinging eyes5*—people’s concen about pollution may be ris-
ing. As they become financially capable of reducing pollution levels,
they demand action, even in the face of less dangerous forms of
pollution than before. The smoke-filled skies of the great steel towns
of the late-nineteenth century are sometimes smog-filled today, Are
we so confident that we suffer from more pollution today? Women
can safely hang clothes out to dry on a clothes line in Pittsburgh to-
day; in the 1930's, the clothes and even curtains in their homes—
would often be covered with soot in a few hours.5? (Of course, most
women use clothes driers today, which were not available to consumers
in the 1930's.) The main reason why Pittsburgh’s air is cleaner today
is that so many steel mills have shut down due to foreign competition.

Regional Standards

When it comes to the problem of reducing the costs (increasing
the efficiency) of assessing the effect of injuries, /ocal civil govern-
ments are best equipped to enforce pollution (cleanliness) standards.
‘The larger the administrative or geographical unit, the more difficult
it is to assess costs and benefits. Only when conflicts across political
or jurisdictional boundaries are involved—county vs. county, state
vs, state — should higher levels of civil government be called in to re-

55. Lester Lavo, “Health, Safety, and Environmental Regulations,” in Joseph A.
Pechman (cd.), Setting National Priorities: Agenda for the 1980s (Washington, D.C.:
Broukings Institution, 1980), pp. 134-35.

56. Milwaukee, Wisconsin, in 1907 had a population of 350,000 people, and a
horse population of 12,500. The city had to dispose of 133 tons of horse manure
daily. In 1908, when New York City’s population was 4,777,000, it had 120,000
horses. Chicago in 1900 had 83,300 horses. This was in the early era of the streetcar
and automobile. There were still three and a half million horses in American cities
and seventeen million in the countryside. Joel A. Tarr, “Urban Pollution— Many
Long Years Ago,” American Heritage, XXU (October 1971).

57. Ted Q. Thackery, “Pittsburgh: How One City Did It,” in Marshall L. Gold-
man (ed.), Ecology and Economies: Controlling Pollution in the 70s (Englewood Clitfs,
New Jersey: Prentice-Hall, 1972), pp. 199-202.

 

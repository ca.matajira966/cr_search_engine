592 TOOLS OF DOMINION

R. H. Goase quite properly called attention to the problem of
State enterprise and responsibility for damages. He remarked that
“it is likely that an extension of Government economic activity will
often lead to this protection against action for nuisance being pushed
further than is desirable. For one thing, the Government is likely to
look with a benevolent eye on enterprises which it is itself promot-
ing. For another, it is possible to describe the committing of a
nuisance by public enterprise in a much more pleasant way than
when the same thing is done by private enterprise... . ‘(here can
be little doubt that the Welfare State is likely to bring an extension of
that immunity from liability for damage, which economists have
been in the habit of condemning. , . .”7

A proper analysis of ownership, pollution, and responsibility
quite properly begins with F. A. Harper's observation that if I do not
have the right to disown an asset, I do not really own it. Murray
Rothbard extends Harper’s comment and applies it to the question
of who really owns “public” property. Very important, Rothbard
concludes, is the short-run perspective of government owners.

While rulers of government own “public” property, their ownership is not
secure in the long run, since they may always be defeated in an election or
deposed. Hence government officials will tend to regard themselves as only
transitory owners of “public” resources. While a private owner, secure in his
property and its capital value, may plan the use of his resource over a long
period of time in the future, the government official must exploit “his” prop-
erly as quickly as he can, since he has no security of tenurc. And even the
most securely entrenched civil servant must concentrate on present use, be-
cause government officials cannot usually sell the capitalized value of their
property, as private owners can. In short, except in the case of the “private
property” of a hereditary monarch, government officials own the current use
of resources, but not their capital value. But if a resource itself cannot be
owned, but only its current use, there will rapidly ensue an uneconomic ex-

78. R. H. Goase, “The Problem of Social Gost,” Journal of Law and Economics, IX
(Oct. 1960), pp. 25-27. A classic example of this unwillingness of the federal govern-
ment to police its own agencies is the case of the radioactive waste disposal sites that
are believed to be leaking wastes into local environments. Seventeen of these nuclear
weapon production facilities in 12 states are owned by the U.S. Department of
Energy. Congressman Albert Bustamante of Texas admitted: “Anytime we get into a
problem like now, nobody on the committee knows what is what. We just delegate
things to the Department of Encrgy.” Fox Butterworth, “Trouble al Atomic Bomb
Plants: How Lawmakers Missed the Signs,” Naw York Times (Nov. 28, 1988).

 

   

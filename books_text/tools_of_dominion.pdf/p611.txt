Pollution, Ownership, and Responsibility 603

on his environmental lifestyle, society will perish. This is the great
danger of class-action suits by one person in the name of an unspeci-
fied number of others in a supposed “class” of victims. Each person
can sue a company, which may be operating within the law, thereby
imposing endless legal fees on the firm. This could tie up a firm’s le-
gitimate operations. Such suits could be brought by anyone for al-
most any perceived infraction: automobile safety, national defense,
and on and on.® Those who bring class-action suits that are deter-
mined by a jury to constitute unwarranted harassment of a business
must be put “at risk” for their actions. Everyone must become re-
sponsible for his actions, not just producers.!°

Ours is not a perfect world, and any attempt to impose perfect
standards on it, without acknowledging the limits imposed by scar-
city, and therefore the costs involved, is demonic. The whole com-
munity will be harmed. “As costs rise for persons who must treat
more and more of their wastes so that other persons can enjoy more
and more purity, it will become apparent that the party who wants
pure water is hurting the environment for the party who wants food,
clothing, and shelter.”1%

Any civil government that attempts to reduce pollution to anywhere
near zero is messianic, The results of a quest for zero pollution will
be similar to the results of a quest for perfect justice: bankruptcy of
the treasury, bankruptcy of producers, judicial arbitrariness, and an
increasing number of economic disruptions. !?

The following piece of legislation, Senate Bill 2770, passed by the
U.S. Senate by a vote of 86 to 0 in 1971, is indicative of this sort of
messianic role for the State: “This section establishes a policy that
the discharge of pollutants should be eliminated by 1985, that the
natural chemical, physical, and biological integrity of the Nation’s
waters be restored, and that an interim goal of a water quality allow-
ing fish propagation and suitable for swimming should be reached
by 1981. The states are declared to have the primary responsibility
and right to implement such a goal.”! At least the Senate was wise

99. fbid., p. 91.

100. On the legal problems associated with class-action suits, see the critical com-
ments by Murray N. Rothbard, “Law, Property Rights, and Air Pollution,” Cato
Joumal, IL (Spring 1982), pp. 93-97. Cf. Huber, Liability, ch. 5.

101. Hite, Economics of Environmental Quality, p. 91.

102. Macklin Fleming, The Price of Perfect Justice (New York; Basic Books, 1974).

103. Cited in Hite, op. cit., p. 92.

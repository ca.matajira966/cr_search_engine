Safekecping, Liability, and Crime Prevention 613

The social atomization of the typical modern urban neighborhood,
in which people do not know the names of their next-door neighbors,
or the neighbors two houses down the street, favors the thieves.!!

Accepting Responsibility

So common was the entrusting of goods to neighbors in Israel
that the case laws established rules governing the practice. The case
law’s provisions still govern similar relationships today. When a man
accepts the task of guarding his neighbor's property, he thereby ac-
cepts a considerable degree of personal liability. Control is in-
escapably tied to ownership. !? Yet, in this case, the controller is not
the legal owner, This places certain disadvantages on him.

We must distinguish here between legal responsibility and eco-
nomic responsibility, lest there be any confusion about the nature of
the responsibility of the safekeeper. Ownership is inescapably con-
nected to economic responsibility. Ownership is a social function; it
is a stewardship function.'* Owners must decide, moment by mo-
ment, what to do with the assets they own. Moment by moment,
others are bidding in the market for the services, animate and inani-
mate, that each person owns. The moment this bidding process
ceases, the price of the asset in question falls to zero, and therefore it
ceases to be a scarce economic resource. The existence of a price tes-
tifies to the existence of the competing bids for ownership. There is

11, A profitable tactic for thieves in urban America is to buy, hire, or steal a large
moving van, paint counterfeit company symbols on its sides, drive up to a house
while the family is away, load the van with the family’s household goods, and drive
away. So impersonal are most American neighborhoods that the neighbors seldom
report this activity to the police as it is taking place. They simply assume that the
family is moving away. They do not regard it as remarkable that the departing fam-
ily never said goodbye to anyone. American families seldom say hello to anyone liv-
ing next door or across the street, year after year.

12. This is why the fascist states of the 1930’s were really socialist economies.
Ownership of industry was officially retained by private individuals, but control
over industrial assets was placed in the hands of State bureaucrats. Cf. Guenter
Reimann, The Vampire Economy: Doing Business Under Fascism (New York: Vanguard,
1939). It is remarkable that after half a century of books, monographs, and doctoral
dissertations, there is stil! not a single book by a well-known economist or economic
historian that surveys the actual operations of the German economy of the 1930s,
despite the fascination of all things Nazi by the book-buying public. (There is also
no full-length book by a professional historian on the occult theology and occult
practices of the Nazi Party, 1920-45.)

18. Gary North, An Introduction to Christian Economics (Nutley, New Jersey: Graig
Press, 1973), ch. 28: “Ownership: Free But Not Cheap.”

 

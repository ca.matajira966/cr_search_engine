Safekeeping, Liability, and Crime Prevention 617

to reveal everything he knows about the facts of the case when asked
specific questions under cross-examination. The court needs ac-
curate information in order to render honest judgment. The court is
legally entitled to accurate information from all witnesses who are
called by law to testify. The compulsory oath is God’s authorized
means of lowering the court’s cost of attaining such knowledge.

The witness is required tu swear an oath before God, and not just
before the earthly judges. He invokes God’s name and therefore in-
vokes God’s sanctions. The civil court-imposed oath is therefore a
true covenantal oath, for all covenantal oaths are self-maledictory
under God.'* By invoking Godls sanctions by taking a judicially
valid oath, the witness faces negative sanctions, not just from the
court in case his perjury is detected, but from God who knows all
hearts. The witness is reminded by the oath that God will condemn him
if he gives false testimony, for God knows the thoughts of men. This is why
offering false testimony under oath places a man under God’s sanc-
tions, and why the sinner owes a trespass offering to the church,
God’s agency of excommunication, rather than to the State, God's
agency of the sword: “And he shall bring his trespass offering unto
the Lorp, a ram without blemish out of the flock, with thy estima-
tion, for a trespass offering, unto the priest” (Lev. 6:6).

 

Hammurabt’s Code

The oath was also used in Hammurabi’s Babylon. Speaking of
the seignior, or aristocrat, the law states: “If a seignior deposited
grain in a(nother) seignior’s house for storage and a loss has then oc-
curred at the granary or the owner of the house opened the storage-
room and took grain or he has denied completely (the receipt of) the
grain which was stored in his house, the owner of the grain shall set
forth the particulars regarding his grain in the presence of god and
the owner of the house shall give to the owner of the grain double the
grain that he took.”!9 The one who was said to have stored the grain

 

accurate information from the midwives regarding the birth of male Hebrew children.
‘They could legitimately lie to Pharaoh because they were under covenant to a different
God who was in the process of bringing Pharavh and his society under judgment.
Cf. Gary North, Moses and Pharaoh: Dominion Religion us. Power Religion (L'yler, ‘Vexas:
Institute for Christian Economics, 1985), ch. legitimate State Power.”

18. Gary North, The Sinai Strategy: Economics and the Ten Commandments (‘Tyler,
Texas: Institute for Christian Economics, 1986), ch, 3.

19. Hatnmurabi Code, paragraph 120, Ancient Near Eastern Texts Relating to the Old
Testament, edited by James B. Pritchard (3rd ed.; Princeton, New Jersey: Princeton
University Press, 1969), p. (71.

  

 

The Restoration of Biblical Casuistry 55

ultimately on the doctrine of se/f-government under God, with God’s
Jaw as the publicly revealed standard of performance.™ It is the hu-
manists’ view of society that promotes top-down bureaucratic power.

The basis for building a Christian society is evangelism and mis-
sions that lead to a widespread Christian revival, so that the great
mass of earth’s inhabitants will place themselves under Christ’s pro-
tection, and voluntarily use His covenantal laws for self-govern-
ment. Christian reconstruction begins with personal conversion to
Christ and self-government under God’s law, then it spreads to
others through revival, and only later does it bring comprehensive
changes in civil law, when the vast majority of voters voluntarily
agree to live under biblical blueprints.

Let’s get this straight: Christian reconstruction depends an majority rule.
Of course, the leaders of the Christian Reconstruction movement
expect a majority eventually to accept Christ as savior. We believe in
postmillennialism.“' Those who do not share our confidence con-
cerning the future success of the gospel, as empowered by the Holy
Spirit, believe that an earthly kingdom must be imposed by force
from the top down (premillennialism),® or else they do not believe

60. DeMar, Ruler of the Nations, ch. 2.

61. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth,
Texas: Dominion Press, 1985); Roderick Campbell, fsrac! and the New Covenant
(Tyler, Texas: Geneva Divinity School Press, [1954] 1981); R. J. Rushdoony, Tay
Kingdon Come: Studies in Daniel and Reodation (Fairfax, Virginia: Vhoburn Press,
[1971] 1978).

62. Dave Hunt writes: “During His thousand-ycar reign, Christ will visibly rule
the world in perfect righteousness from Jerusalem and will impose peace on all na-
tions, Satan will be locked up, robbed of the power to tempt, Justice will be meted
out swiftly.” Hunt, Beyond Seduction: A Return a Biblical Christianity (Eugene, Oregon:
Harvest House, 1987), p. 250. If Satan is unable to tempt mankind, then any evil
that calls forth Christ’s justice must be man-based evil. In a taped interview with
Peter Lalonde, released in early 1987, Hunt said: “Christ hiraself is physically here.
And He has us, the redeemed in our resurrection bodies, that nobody can kill us.
And we are helping Him to maintain order. He is forcing this world to. behave, and
He gives a restoration of the Edenic state, so that the desert blossoms like a rose, and
the lion lies down with the lamb, and you've got paradise on earth, once again, with
Christ Himself maintaining it and, even better than the garden of Eden, Satan is
locked up for a thousand years.” Dominion and the Cross, Tape One of Dominion: The
Word and the New World Order, op. vit., 1987.

It should be pointed out that Hunt’s argument that resurrected saints will return
to rule with Jesus during the earthly millennium has long been rejected by dispensa-
tional theologians at Dallas Theological Seminary. Resurrected saints will be dwelling
in a place called the heavenly Jerusalem, argues J. Dwight Pentecost: “The Relation
between Living and Resurrected Saints in The Millennium,” Bibliotheca Sacra, vol. 117
(October 1960), pp. 335-37. See also John F. Walvoord, The Rapture Question (rev. ed;
Grand Rapids, Michigan: Zondervan Academie, 1979), pp. 86-87.

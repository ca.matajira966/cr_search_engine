630 TOOLS OF DOMINION

this in verse 13, where it says that when he can produce the torn re-
mains of the animal, “he shall not make it good.” “Making it good”
and “restitution” are identical words in Hebrew, and should be re-
garded as equivalents here.

Restitution in the context of the obligation of the negligent safe-
keeper is a payment equal to the value of what had been lost. The responsible
neighbor did not intend to profit from the theft. Indeed, he voluntar-
ily took on added responsibilities by agreeing to serve as a protector.
Negligence on the part of the safekeeper is not the same as criminal
intent on the part of the thief; therefore, the penalties are different.
The thief pays the victim an extra penalty equal to his hoped-for
profit: double restitution. There is no additional penalty payment
imposed on the safekeeper, for he had not hoped to profit by the
transaction. To make safekeepers responsible for large restitution
payments associated with criminal action would be to break down
the covenantal bonds of the community, since too high a risk factor
would be transferred to safekeepers. Men would no longer be so will-
ing voluntarily to accept the liabilities of safekeeping. This reduction
in voluntary safekeeping activities would tend to subsidize the
criminal class, which is certainly not the intent of biblical law.

The “hospitality of safekeeping” is designed to make theft more
difficult for professional thieves. Clearly, it makes theft easier for
previously honest neighbors. Nevertheless, the law has been given
by God. So, the focus of judicial concern has to be on the profes-
sional thief. A man delivers his inanimate goods to a neighbor, above
all, to keep them from being stolen. The recipient therefore must
take care to see to it that the property is not stolen. He cannot guard
against every conceivable loss, but he is required to make life more
difficult for thieves. The law makes this responsibility inescapably
clear: “And if it be stolen from him, he shall make restitution unto
the owner thereof” (v. 12). The safekeeper has to repay the depositor.
This motivates the safekeeper to seek to capture the thief.

If subsequently apprehended and convicted, the thief must pay
the victimized safekeeper double. The safekeeper has already had to
repay the depositor. It should be obvious that if the safekeeping
neighbor has been assessed a compensating restitution payment, he
has “bought” the missing beast from the original owner. Therefore,
half of what the thief has returned to him serves as compensation for
the loss he incurred by repaying the depositor. The other half of the
double restitution payment is his compensation for having been put
into a bind by the thief’s actions.

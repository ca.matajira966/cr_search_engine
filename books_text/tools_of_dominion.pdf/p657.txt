Seduction and Servitude 649

dowries) instead. What the law did was to establish a penalty price so
high that it discouraged seduction. It also discouraged false accusations
of whoredom.,

The threat of the imposition of the formal bride price was de-
signed to restrain the present-orientation of the couple— in this case,
the lure of instant sexual gratification. The bride price jumped auto-
matically to 50 shekels of silver in such instances. This economic
threat forced marriage arrangements into specific patterns as family-
authorized covenants, with the parents and older brothers of the girl
as the agents with primary authority to inaugurate or veto her deci-
sion. This threat also forced irresponsible, short-sighted young men
to save for the future, to develop good character traits. The normal
bride price was a covenantal screening instrument; the formal bride
price was a covenantal disciplining instrument.

The seducer placed himself outside the normal competitive posi-
tion of a suitor. He was in no legal position to bargain effectively
with the girl’s father. Shechem pleaded: “Ask me never so much
dowry and gift, and I will give according as ye shall say unto me: but
give me the damsel to wife” (Gen 34:12). The father of a seduced girl
was in a position to demand up to 50 shekels of silver from the young
man, which probably would have involved many years of servitude
on his part, unless his family was rich. The seducer could even be re-
quired to pay her father the 50 shekels of silver, and then not be
allowed to marry the girl.

Establishing the Formal Bride Price

The Jewish commentators agree that it was 50 shekels of silver,
although they do not always preciscly explain their line of reasoning.
Those who say that it was 50 shekels frequently connect this passage
to Deuteronomy 22:19. This passage provides rules for penalizing a
bridegroom who falsely accuses his new bride of not being a virgin.
A new husband in ancient Israel who falsely accused his wife of not
being a virgin at the time of marriage was obviously after two things:
1) permanent separation from the girl; and 2) the return of his bride
price, He may also have been after an additional penalty payment of
50 shekels from her father. I am assuming here that a bride price had
been paid before the marriage; if not, then by his accusation, he was

9. Nachmanides, Exndus, p, 256; Haim H, Cohn, “Sexual Offenses,” The Prin-
ciples of Jewish Law, edited by Menachem Elon ( Jerusalem: Keter, [1975?]), col. 485.

650 TOOLS OF DOMINION

trying to avoid paying it. I believe, however, the bride price was nor-
mally paid before the marriage, which is why Jacob worked seven
years for Laban before Laban was required to give him Rachel

(Gen, 29:18-20),

Why 100 Shekels?

The required penalty owed to the father of a falsely accused girl
was 100 shekels of silver (Deut. 22:19), The question then is: Does
this provide us with evidence that confirms my suggestion concerning
the size of the original bride price? We know that the Old Testament’s
authorized penalty payments were double damages, quadruple
damages (a slaughtered sheep),’° and quintuple damages (a slaugh-
tered ox). In this case, double damages were required. Half of a
hundred is 50. Why 50 shekels? Because this was the maximum
bride price that could be imposed by law. We must think through the
issue with 50 shekels as the starting point.

Notice that the girl was executed if she was convicted, but her
bridegroom was not executed if she was exonerated. This seems to
be opposed to the principle of Deuteronomy 19:15-21, which states
that the false witness must suffer the penalty that the falsely accused
person would have suffered if convicted, Instead, the bridegroom
paid a heavy penalty to her father. All he owed his bride was a life-
time guarantee of no divorce. What he owed her father, however,
was a lifetime of servitude, unless he was very rich. He became a
slave to her father twice over, for the formal price of the lifetime slave
for purposes of making a sanctuary vow was 50 shekels of silver
(Lev. 27:3).

This is the only instance in the Bible of a false witness who is not
subject to an equal penalty, as required by Deuteronomy 19:16. The
falsely accused bride was to receive lifetime economic support from
him rather than making her a divorcée by means of his execution.
This exception to Deuteronomy 19:16 may be because of the diffi-
culty in proving for certain either that she had or had not lost her
physical evidence of virginity by some means other than copulation.
The circumstantial nature of the required evidence—“tokens of vir-
ginity’— reduced the penalty for the false accuser, but it also made it

10. David insisted on the four-fold restitution payment when he heard Nathan’s
story, but in this case, the “ewe” was another man’s wife (If Sam. 12:6). His “slaugh-
ter” of Bathshebu was the result of their adultery, not his seduction of a virgin as an
unmarried man.

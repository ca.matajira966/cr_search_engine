Seduction and Servitude 661

prove that his daughter had not played the whore under his house-
hold administration.” Her father is therefore legally powerless to de-
fend her life, In fact, only by remaining silent can he demonstrate
publicly that his household is free from the bridegroom’s accusation
of whoredom, and that he is not a pimp. The bridegroom is the cove-
nantal agent of the holy community and also the covenantal agent of
a righteous father’s household. His public accusation allows her
father to preserve his family’s good name by implicitly supporting his
charge by not coming to her defense. He has replaced her father as
the covenantal head over her. He brings a covenant lawsuit against
her as a whore in the name of her father and the priestly nation.

Jesus Christ, the Bridegroom

Biblically speaking, Jesus Christ brought a covenantal lawsuit
when He charged Israel with spiritual whoredom. He was Israel's
divine Bridegroom, sanctioned by Israel’s Father, yet He caught
Israel worshipping false gods. He publicly called the rulers of Israel
“sons of your father, the devil” ( John 8:44).

Whoredom had been Israel's problem from the beginning, which
the entire Book of Hosea was written to illustrate, and which Ezekiel
16 was devoted to explaining. In God's eyes, as Israel’s Father, His
daughter was deserving of death as a whore. But Jesus Christ came
to pay the bride price for all mankind, including Israel. He paid it to
God the Father, as required. This restored God’s reputation among
His enemies as the cosmic Judge.” Without this payment, God’s au-
thority as cosmic Judge would have been compromised, for He
would be viewed as a God who cannot bring His word to pass in his-
tory. He would be viewed as a Father who cannot control the actions
of his promiscuous or adulterous daughter. His only other option
would have been to bring His daughter to the authorities for burn-
ing, as the fornicating daughter of a priest (Lev. 21:9). This is what
God did with national Israel in s.p. 70.

21. If her lack of physical evidence for her virginity was the result of something
other than previous sexual intercourse, then she would have to inform her father,
wha would in turn warn the prospective bridegroom before the betrothal, and get
fram him a signed statement or other suitable courtroom evidence of his acceptance
of this explanation in lieu of the physical tokens.

22. The family name of God is always the key motivation in God's decision to bring
judgment. Moses appealed to God to spare the Hebrews by appealing to Gods rep-
utation among His enemies (Ex. $2:11-14). Nathan reminded David that his adul-
tery and murder had given the cnemies of God a cause to blaspheme (II Sam. 12:14).

702 TOOLS OF DOMINION

these choices: 1) provide information, free of charge, of the treasure’s
whereabouts to bureaucratic officials of the State; 2) say nothing and
save themselves’a lot of trouble; 3) work out an illegal deal with some
official; or 4) steal the treasure. In the Soviet Union, predictably, the
final three possibilities are the ones people choose; the first choic
simply not taken seriously as a sensible alternative.*”

 

Gonclusion

The Bible forbids economic oppression, but only vaguely defines
it. Economic theory provides even fewer guidelines than the Bible.
About all that economic theory can say is that when the threat of vio-
lence is imposed on someone, there is oppression. But violence must
not be defined as a market participant's threat of refusing to trade
with someone else, unless the violation of an existing contract is in-
volved, or unless someone is being asked to commit a crime or immoral
act. Sharp bargaining is not automatically considered oppressive,
either in the Bible or economic theory.

Without specified infractions, it is very difficult to develop a sys-
tem of civil law. The law must specify the action that is being prohib-
ited. It must be sufficiently clear that juries can make judgments,
and that their judgments can be predicted with better than 50-50 ac-
curacy by most people, especially potential criminals. If the deci-
sions of juries are random, then the law will not protect innoccnt
people on a predictable basis. This means that civil law no longer
serves its God-given purpose of providing social order.

Defining oppression clearly is very difficult. Oppression must be
defined in such a way that the courts do not easily become tyrannical
or arbitrary in their decisions. But as I have said, a definition of eco-
nomic oppression that is both equitable and tyranny-resistant when
it is applied to a large number of cases over time has not yet been clis-
covered. This is why economic oppression rarely can be legislated
against without creating more harm than benefits for the potential
victims of oppression. The legislation itself becomes a major source
of oppression.*® The medieval notion of the “just price” is one of the
best examples of this problem in history, especially when interpreted
centuries later by civil magistrates who were not familiar with the late-
medieval Scholastic theologians’ distrust of government price-fixing.

47. Konstantin Simis, USSR: The Corrupt Society (New York: Simon & Schuster, 1982).
48. See Chapter 24: “Impartial Justice and Legal Predictability.”

 

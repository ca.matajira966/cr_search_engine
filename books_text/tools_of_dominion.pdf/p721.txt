The Prohibition Against Usury 713

thee for an inheritance to possess it” (Deut. 15:4). Does this mean
that these loan provisions would eventually be annulled? No. “For
the poor shall never cease out of the land: therefore I command thee,
saying, Thou shalt open thine hand wide unto thy brother, to thy
poor, and to thy needy, in thy land” (Deut. 15:11). Everything in
Deuteronomy 15 speaks of poverty and biblical law’s means of over-
coming it. Deuteranomy 15 is not dealing with business loans; it is dealing
with charity loans.

But let the reader be forewarned: biblical law is a broader cate-
gory than biblical civil law. There was no statute law that imposed
sanctions on anyone who refused to make an interest-free loan.

Defining Poverty by Statute

Why was this not a statute law? Because biblical civil law pre-
sents only negative injunctions. It prohibits publicly evil acts. Bibli-
cal civil law does not authorize the State to make men good. It does
not authorize the State to force men to do good things. It does not
authorize the creation of a messianic, salvationist State. The State
cannot search the hearts of men. God does this, as the Creator and
Jadge, so the State must not claim such an ability. The State
is only authorized by God to impose negative sanctions against pub-
licly evil acts. It is not authorized to seek to force men to do good
acts. In short, the Bible is opposed to the modern welfare state.

There is no way for biblical statute law to define what poverty is
apart from the opinions of those affected by the law, either as taxpay-
ers, charitable lenders, or recipients of public welfare or private
charity. “Poverty” is too subjective a category to be defined by statute
law. The State needs to be able to assign legal definitions to crimes,
in order that its arbitrary power not be expanded. Yet economic defi-
nitions of wealth and poverty that are not arbitrary are simply not
available to the civil magistrates for the creation of positive legal in-
junctions. Thus, God’s civil law does not compel a man to make a
loan to a poor person.

Nevertheless, the civil law does prohibit taking interest from
poor people. How can it do this without creating the conditions of
judicial tyranny through arbitrariness? If the magistrates cannot
define exactly what poverty is for the purpose of writing positive civil
injunctions, how can they define what a charitable loan is? How can
the State legitimately prohibit interest from a charity loan if the
legislators and judges cannot define poverty with a sufficient degree

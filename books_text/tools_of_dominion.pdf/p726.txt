718 TOOLS OF DOMINION

Who Are the Rich?

Who are the “rich,” judicially speaking? Those who are not judi-
cially poor. We have seen what constitutes poverty judicially: those
who go to the potential lender and 1) remind him of his moral obliga-
tion to lend to the deserving poor 2) at zero interest, and 3) offer to
go into bondage for as much as seven years to pay off the note if they
should default on the loan.

This formula therefore tells us who the rich are, judicially speaking:
all those people who are willing to sign a strictly voluntary, interest-
bearing debt contract that is collateralized by something other than
the threat of placing them in bundservice if they should default on
their obligation. If the lender extends them credit on the basis of
their signatures, or because they have offered him other collateral,
including their real estate, then they are not considered poor people
judicially. They come to him on the basis of a busincss opportunity,
not on the basis of his moral obligation to lend them an interest-
free loan.

What about the jubilee year? The jubilee law has been com-
pletely fulfilled in history by Jesus. This means, as I argue in
Chapter 4, that the Old Testament’s ten-generation slave system for
foreigners has been legally abolished. It also means that the land
tenure laws of ancient Israel are legally abolished. There is no longer
any legal obligation to return a piece of rural property to the original
owner or his heirs. Thus, a debtor can legitimately cotlateralize a loan with
his property. 1f he defaults on the loan, he loses his property unless he
buys it back later on. (While this revision of my views will not please
Mr. Mooney, I hope it will satisfy Greg Bahnsen, who once wrote
that he did not agree with “Gary North’s view of home mortgages.”!*}

This is not to say that the debtor should do this. It is a great em-
barrassment to a man if he loses ownership of his family’s property —
his home— even in an urban society. If he is evicted from his home,
he loses face. It is best if a man can own his home debt-free. He then
does not face the threat of eviction and the embarrassment associ-
ated with eviction. But it is his legal right biblically to sign a debt
contract to buy or refinance a home."

 

14. Greg L, Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg, New
Jersey: Presbyterian & Reformed, 1984), p. xix.

15. This does not mean that the State should subsidize this practice, as the U.S.
government does, by offering deductions from total income, for income tax repurt-
ing purposes, for interest paid on mortgages. It also does not mean that the govern-

 

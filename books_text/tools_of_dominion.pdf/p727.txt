The Prohibition Against Usury 719

A Millennium of Misinterpretation

Medieval Roman Catholics and early modern Protestants misin-
terpreted these verses. They interpreted them as if they were prohi-
bitions against all forms of interest, rather than prohibitions against
interest earned from charitable loans to fellow believers, as the Ex-
odus 22 text explicitly says: “If thou lend money to any of my people
that is fare] poor... .” The church’s hostile view of interest had its
origin in the teaching of Aristotle. Aristotle’s economic analysis,
rather than the explicit teaching of the biblical texts, always was the
unstated intellectual foundation of the church's prohibition on interest-
bearing business loans.

Aristotle taught that money is sterile—that it cannot increase by
moving from person to person over time —and therefore undeserving
of any return beyond the principal. Economist Joseph Schumpeter
writes af Aristotle: “He condemned interest— which he equated to
‘usury’ in all cases—on the ground that there was no justification for
money, a mere medium of exchange, to increase in going from hand
to hand (which of course it does not do}. But he never asked the
question why interest was being paid all the same. This question was
first asked by the scholastic doctors. It is to them that the credit
belongs of having been the first both to collect facts about interest
and to develop the outlines of a theory of it. Aristotle himself had no
theory of interest.”!6 Neither did the early church.

From the beginning, the West’s view of interest was clouded by
the association of interest rates and physical production. They are
not linked in economic theory. It was also clouded by the association
of interest with money, Furthermore, the Greeks were hostile to the

 

 

ment should create (or promise) deposit insurance for those who put their money in
savings institutions, with the legal right of immediate withdrawal, when the institu-
tions then use this money to lend on 30-yeur mortgages. The length of the loan must
be the same for both lender and debtor. The institutions must not be allowed by civil
law lo “borrow short" and “lend long.” President-elect George Bush in January of
1989 faced the bankruptcy of some 500 savings and loan companies and also the
bankruptcy of the Federal Savings & Loan Insurance Corporation (FSLIC), a non-
government or quasi-government insurance scheme, established in 1934, which had
failed. It was expected that the government would have to bail out the FSLIC by
granting it cash or credits of between $50 billion (o $100 billion. By August, the bill
had soared to $166 billion. The crisis is still not over. Bad economics eventually pro-
duces bad results,

16. Joseph A. Schumpeter, History of Economic Analysis (New York: Oxford Uni-
versity Press, 1954), p. 65.

  

 

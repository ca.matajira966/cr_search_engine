740 TOOLS OF DOMINION

century, however, a commercial bank’s most important from of col-
lateral in the United States is the legal backing of the federal govern-
ment, which stands ready to bail out bankrupt banks—a guarantee
which is ultimately backed up by the printing press money of the
Federal Reserve System, the nation’s central bank, We have guaran-
teed inflation by ignoring the warning against multiple indebtedness.

Fractional reserve banking is inflationary, for it creates credit
money—money which is backed only by faith. When a person
deposits his money on the condition that he can write a check and
spend it, the inflation is about to begin. The banker loans, say, 90
percent of this money to a borrower, The borrower then spends the
money. Whoever gets the borrower’s money then either spends it or
deposits it in Ais bank, and the process continues. As a theoretical
limit (though not in practice), for every dollar deposited in a banking
system with 10 percent reserves, nine additional dollars will eventu-
ally come into circulation.® Thus, fractional reserve banking is in-
herently inflationary.® It also creates inflationary booms and their
inevitable consequences, depressions."

Warehouse Receipts

Say that a person brings in ten ounces of gold to a warehouse for
safekeeping, and the warehouse issues a receipt for ten ounces of
gold. The owner pays a fee for storing the money, but he presumably
increases the safety of his holdings. The warehouse specializes in
protecting money metals from burglars. The depositor pays for this
specialized service. It is somewhat like a safety deposit box in a
bank, except that the warehouse issues a receipt.

The receipt may begin to function as money. If people trust the
warehouse, they will accept a receipt for all or part of this gold in
payment for goods and services. A piece of paper authorizing the
bearer to collect a specified amount of gold is just.about the same as
the actual ounce of gold. Besides, the gold is safer in storage, and
paper is a lot more convenient than pieces of metal.

But a problem threatens the system. What if the warehouse
owner recognizes that people in the community trust him? They

65. On the operations of the Federal Reserve System, see North, ibid., ch. 9.

66, The process is described, step by step, in a free book which is published by
the Federal Reserve Bank of Chicago, Modern Money Mechanics,

67. North, Honest Money, ch. 8.

68. Mises, Human Action, ch. 20.

752 TOOLS OF DOMINION

This raises the whole question of cost. What is the cost of any ac-
tion or any purchase? It is the value of whatever has to be forfeited,
ie., the value of the most valuable foregone use. If I do one thing with my
money, I cannot do something else with it. The value of whatever T
would actually have done but did not do is what it costs me to do
whatever I do.

The lender who transfers to another person the use of an asset,
monetary or nonmonetary, has given up whatever other opportuni-
tics might have been available to him. There are always other oppor-
tunities available. There is therefore always a cost to the lender of
lending money.

The master in the parable was being gracious to the servant. He
recognized from the beginning that the man was not very com-
petent. The master did not tell the servant that he had failed because
he had not made 100 percent on the money entrusted to him. He told
him only that he had failed because he had not earned an interest
payment. This is the least that the master could have expected.

The master probably could have doubled his money by entrust-
ing it to either of the first two servants. But he had sought greater
economic safety instead. He had adopted the principle of risk reduc-
tion through portfolio diversification. You get a lower rate of return but a
more sure return. But the master had been cheated. He could have
deposited his money directly with the money-lenders instead of giving
it to the servant. That would have been safer— greater diversifica-
tion through the bank - and it almost certainly would have produced
4 positive rate of return, however low. Instead, he received only his
original capital in return.

He had forfeited his legitimate interest payment because he had
transferred the asset to the slothful, risk-aversive servant. This ser-
vant is a model of wickedness, not because he was actively evil, but
that he was passively unproductive. He did nothing with that which had
been entrusted to him. Doing nothing is sufficient to get you cast
into hell, when doing the minimurn would at least quench the mas-
ter’s wrath. (Warning: only one man in history has ever performed
this minimum: Jesus Christ.)

Interest and Capitalization

Is interest-taking morally legitimate? This debate has been going
on since at least the days of Aristotle, who called money sterile and
interest illegitimate. But if money is sterile, why have men through-

754 TOOLS OF DOMINION

its rental fee (interest), He expects to make a profit of some kind on
the temporary exchange of control over it.

Summary

Non-fractional reserve banking and the taking of interest are
both biblically legitimate. The parable of the talents should be suf-
ficient proof for anyone who is not trying to make an overnight theo-
logical reputation for himself based on the promotion of the utterly
fantastic. We should take the Bible seriously in preference to Aris-
totle, and also in preference to the economics of love.*! The capital-
ization of long-term assets, including human services is biblically
legitimate.

Again, I acknowledge that men, in their quest for autonomy from
God, are willing to become slaves of sin, and therefore in principle
slaves of other men. I recognize the New Testament principle that it
is best to owe no man anything (Rom. 13:8a). I also recognize that
modern economics has promoted the ideal of perpetual debt for per-
petual prosperity, and that a world so constructed will eventually
collapse. But to place temporal limits on the judicial enforceability of
the discounting of future long-term human services, because the
Bible requires that we restrain man’s overconfidence about his long-
term future, is not the same as denying that there is an inescapable
discounting (capitalization) process between the present value of
present goods and the present value of expected future goods.

With respect to capitalized debt, if both the lender and the bor-
rower agree that a piece of collateral is acceptable in exchange for the
defaulted loan, then the debtor is not in debt, net. He has an offsetting
asset. He wants the money in cash; the lender would rather have the
money over time, The existence of the collateral reduces the likeli-
hood that the debtor will default. The debtor is therefore not a ser-
vant of the lender in this case. Nevertheless, if the loan involves the
potential loss of a man’s home, meaning his status and his own self-
evaluation, then he is in a form of bondage. But if he owns investment
assets (a house, for example) with a mortgage on it, and he risks losing
the house if he defaults, then this voluntary transaction is merely a
shifting of risk to the liking of both transactors. The lender feels better
about the future with a stream of income guaranteed by the value of

81. Sce Appendix F; “Lots of Free Time: The Existentialist Utopia of S. C.
Mooney.”

770 TOOLS OF DOMINION

Too many economic resources are wasted under social systems char-
acterized by judicial arbitrariness—scarce resources that might
otherwise be used to reduce uncertainty in forecasting uncertain
future consumer demand rather than uncertain future judicial deci-
sions. By reducing judicial uncertainty, biblical justice frees up re-
sources that can then be used to increase output per unit of resource
input. Nevertheless, biblical law should not be interpreted as the
product of capitalistic institutions; on the contrary, capitalism is the
historic product of a world-and-life view favorable to the kind of
legal predictability which is produced by respect for biblical law.*”

False Witness and Organized Envy

Individuals are commanded not to raise a false report. This is a
specific application of the law against bearing false witness (Ex.
20:16). Raising a false report is the equivalent of slander; God cuts
off the slanderer (Ps. 101:5). By raising a false report, wien endanger
their victimized neighbor, as well as the peace of the community. By
misleading the judges, and by luring them into making improper de-
cisions, the man who bears false witness endangers the trust which
other men place in the judges and the biblical system of justice. This
is why a stiff penalty is imposed on perjurers: the penalty that would
have been imposed on the victim of the falsehood (Deut. 19:16-19).

The focus of concern in this passage is with false witnesses, cor-
rupt judges, and the oppressed rich. The “oppressed rich”? Yes. The
law warns against upholding the poor man in his cause or lawsuit.
But if we are not to uphold the poor as such, then the poor man or
men must be bringing a case against someone or some group that is
not equally poor. This classification of “non-poor” included success-
ful strangers (v. 9), who were willing to remain as resident aliens in
urban areas. Economic success, or the hope of success, motivated
the stranger to remain. Once successful, he would be less likely to
return to his people and the society governed by the religion of his
people.2® The phenomenon of the successful outsider is a familiar

37. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
‘Texas: Institute for Christian Economics, 1986), especially the Conclusion.

38. It is worth considering the possibility that one reason for the economic and
academic success of Jews in the twenticth century is the combination of modern sec-
ularism and remnants of historic discrimination. Secularism assumes that religious
differences that are based on dogma or theology are irrelevant, or should be. This
has opened up universities, businesses, professions, and mast other institutions to

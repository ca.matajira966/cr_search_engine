Bribery and Judgment 809

The voluntarily accepted principle of “highest money bid wins” governs
the free market. The principle of the God-required tithe governs the
church. The principle of the coercive fixed percentage of net income
{income tax) should govern the civil government, or else a fixed per-
centage of market purchase price (sales tax or use tax).*® In short, a
monopolistic court is not an open competitive market. Both church
and State are monopolistic courts.

Conclusion

Noonan’s book on bribery is built around a single theme: that a
bribe is a form of reciprocity, Why is it, his book asks, that reciprocity
is basic to human life, yet in the case of bribery, it is condemned?
His book provides no real answer. The biblical answer is primarily
theocentric: God's dual character as Judge and also as Creator-Redeemer.
Secondarily, it rests on the difference between a monopolistic court
and an open market. The court does not operate in terms of eco-
nomic reciprocity; the market does. The court enforces the law of the
God who declares that which is criminal and who specifies appropriate
penalties. The reciprocity associated with a court is found in its im-
position of a restitution program. The criminal repays his victim. The
principle of reciprocity is enforced by the court on those who stand
before it, righting wrongs and restoring order. There is no reciprocal
economic relationship between the court and those being judged.

Men are wicked if they take bribes to pervert righteous judg-
ment. God’s laws are supposed to be every judge’s standard. He is
not to respect persons. He is not to favor one or the other. The court
is to imitate God as the cosmic Judge: “Wherefore now let the fear of
the Lorp be upon you; take heed and do it: for there is no iniquity
with the Lorp our God, nor respect of persons, nor taking of gifts”
(JE Chron, 19:7). Yet we are told that God does take gifts: “And many
brought gifts unto the Lorp to Jerusalem’ (II Chron. 32:23a). God
never takes a gift or bribe in His capacity as Judge: “For the Lorp
your God is Ged of gods, and Lorn of lords, a great God, a mighty,
and a terrible, which regardeth not persons, nor taketh reward”
(Deut. 10:17).

John Noonan writes, “As a believer in religion, I have asked how

 

46. The best tax is the gasoline tax, if the revenues are used exclusively to build
and repair roads in the region where the tax is collected. If you do not drive, you do
not pay the tax.

Sabbatical Liberty 813

Hebrews would have involved acknowledging symbolically his own
covenantal subordination to God. The issue of sabbath rest is in fact an
issue regarding God’s sovereignty, meaning covenantal subordination.?

The Heart of a Stranger

The Hebrews are told not to oppress a stranger because they
“know the heart of a stranger.” How can they know this? Because
they, too, had been strangers in the land of Egypt. This raises a
crucially important issue in philosophy, the issue of cpistemology:
“What can men know, and how can they know it?”

The question here is the question of empathy. It tells us that be-
cause we can look within ourselves, we can make judgments regard-
ing the feelings of others. What they feel is sufficiently close to what
we feel to enable us to make ethical judgments. This ability under-
girds the so-called golden rule: “Do unto others as you would have
others do unto you.” This phrase is one of those famous phrases at-
tributed to Jesus that He never quite said. What He said was, “There-
fore all things whatsoever ye would that men should do to you, do ye
even su to them: for this is the law and the prophets” (Matt. 7:12). It
is closely related to Paul’s words: “For all the law is fulfilled in one
word, even in this; Thou shalt love thy neighbour as thyself” (Gal.
5:14), This is the requirement of Leviticus 19:18, which Jesus cited in
Matthew 22:39-40: love your neighbor as yourself.

The humanist has a problem with this moral injunction. The
problem was best stated in George Bernard Shaw’s play, Man and
Superman (1903): “Do not do unto others as you would they should do
unto you. Their tastes may not be the same.” There is an implicit
lawlessness in this, as he says forthrightly in the same play: “The
golden rule is that there are no golden rules.” If each man is autono-
mous, and therefore utterly unconnected with other men by feelings

3. [have elsewhere argued that the New Testament places the locus of enforce-
ment regarding the sabbath with the individual conscience (Rom. 14:5-6; Col, 2:16).
Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler, Texas:
Institute for Christian Economies, 1986), ch, 4, Appendix A. To head off arguments
that | am now denying my former arguments by making sabbath observance an
issue of covenantal subordination, T need to point out that there are four biblical
cavenanis: personal, familial, civil, and ecclesiastical. The New Testament’s cove-
nantal subordination is directly personal under God, meaning that sabbath enforce-
ment is no longer a judicial responsibility of family (when dealing with legal adults),
church, or Slate, 4 person should not be disinherited, excommunicated, or executed
because of his or her failure to observe the sabbath

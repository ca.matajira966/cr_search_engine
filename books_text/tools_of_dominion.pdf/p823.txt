Sabbatical Liberty B15

vated in Israel, Because they shared a common covenant history, they
were under covenant law. God had told them centuries before when
they were slaves in Egypt not to forget to remind their heirs of this
experience.

How could later generations remember? In what way had they
been slaves in Egypt? How could God expect later generations to re-
member what had never in history happened to them personally?
Because life is cavenantal. In the same sense that all men have rebelled
in Eden’s garden, so were the Hebrews to regard themselves as hav-
ing served a term as slaves. That sense is covenantal— personal,
hierarchical, ethical, judgmental, and historical. God reminded the
generation of the exodus that they had been slaves, and that they,
meaning their heirs, would return to slavery in a foreign land again
if they disobeyed Him (Deut. 28:64-65). Their heirs were required
to remember this, too, long after that first generation had died in
the wilderness.

The stranger in the land wants rest from his labors. He needs
hope that at the end of his work, there will be rest. This is the equiv-
alent of saying that at the end of his period of bondage, there will be
liberty. This is the message of the Book of Exodus: liberty comes through
God's covenant blessings ta those who serve God and other men faithfully
through dedicated labor. It was the denial of hope in future rest or future
liberty that marked Pharaoh’s Egypt. It marks every bureaucratic
civilization,

Sabbath and Providence

The sabbath is an aspect of God’s grace to man and the creation.
It is the promise of rest and eventual liberty from bondage, primarily
the bondage of sin. The Bible is clear: what man hopes to be his ex-
ternal reward from God he must therefore offer to all those under his
authority. This includes not just people, but also animals and the
Jand itself. The discipline of ethics goes from the lesser case to the
greater. If man is to give even the land rest, then he is surely to give
rest to the animals of the land. If he is to give animals rest, then he
must surely be required to give strangers rest. If he is to give
strangers rest, then surely he must give his servants rest. And if he is
to give his servants rest, then he must surely give himself rest.

But how can he afford to give himself rest? Who is to guard the
garden while he is resting? Who is to care for the needs of his family,
his servants, strangers in the land, animals, and the land itself? With-

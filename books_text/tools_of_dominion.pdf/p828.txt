820 TOOLS OF DOMINION

God instructs owners to allow poor people to glean. The land is
His (Lev. 25:23); the whole earth is His (Ex. 19:5;7 Ps. 24:1). As the
permanent owner, God can tell his stewards how to administer His
property. But God is the disciplining agent. He acts as Kinsman-
Redeemer or as Blood Avenger, depending on the obedience or dis-
obedience of the landowner. The law is in the form of a positive
injunction, and biblical civil law is negative in scope: forbidding
public evil.

There is no doubt that this form of morally compulsory charity
on the owner’s part involved hard work on the part of the recipients.
They are be allowed to glean the corners and difficult places only
after the “easy pickings” have been gleaned by the hired harvesters.
They are invited into the open fields only in the sabbatical year in
which there has been no previous season's planting. They have to
earn every bit of the produce they collect. It is not a chosen profes-
sion for sluggards. But for those who are willing to work, they will
not perish at the hands of men who systematically used their compe-
titive advantage to create a permanent class of the poor.

There is another great advantage to this form of morally en-
forced charity: it brings hard-working, efficient poor people to the
attention of potential employers. There is always a market for hard-
working, efficient, diligent workers. Such abilities are the product of
a righteous worldview and a healthy body, both of which are gifts of
God. It always pays employers to locate such people and hire them.
In effect, the employers can “glean” future workers. Gleaning ap-
pears initially to be a high-risk system of recruiting, for it requires
landowners to forfeit the corners of their fields and one year’s pro-
ductivity in seven. Nevertheless, God promises to bless those who
obey Him. It really is not a high-risk system. Israel's gleaning system
made the charity local, work-oriented, and a source of profitable in-
formation regarding potential employees. Thus, the system offered
(and offers) hope to those trapped in poverty. They can escape this
burden through demonstrated productivity. This is how Ruth, a
stranger in the land, began her escape: she caught the attention of
Boaz (Ruth 2:5).

7. Horror of horrors! I have just discovered an economic verse in Exodus 19 — the
chapter that I skipped in Moses and Pharaoh, If you think I intend to add a whole
chapter ta that book, pay for new typesetting, and re-index it, then you have not
judged the cost-benefit ratio from the same subjective value framework as I have.

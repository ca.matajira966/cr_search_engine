846 TOOLS OF DOMINION

Humanist Citizenship

The modern humanist wants the political fruits.of ritual subordi-
nation to God, namely, the right to exercise civil judgment in society,
but without the roots: actual ritual subordination to God. He wants
the judicial fruits of lawful access to God's required feasts without ac-
tually having to attend them. He wants universal suffrage: a guaran-
tee of his continuing access to the office of judge, despite his public
denial of God’s authority over him. He insists on being allowed to
serve as a civil judge despite the fact that he is not under ecclesiasti-
cal discipline. If this demand is biblically legitimate, it means one of
two things: 1) that he can interpret and apply God’s revealed civil
law as well as a Christian can, despite the fact that he refuses to
honor the counsel of church officers by affirming the church cove-
nant and submitting to church discipline; or 2) that God's revealed
civil law—if such even exists—is irrelevant to civil affairs.

We need to understand what this means judicially and politically.
The humanists want a different covenant, with a different set of five
points: sovereignty (the General Will, the People, The Volk, the pro-
letariat, etc.), hierarchy-representation (the Party, the vanguard of
the proletariat, the Fiihrer, the Supreme Court, national plebiscites,
etc.), law (majority rule, evolutionary forces, Marxism- Leninism,
etc.), judgment (oaths to different sovereignties in order to gain
zenship, welfare rights and entitlements, etc.), and inheritance
(political citizenship). They have been successful in persuading vot-
ers, including Christian voters, of the supposed judicial necessity of
abandoning the biblical covenant model that long undergirded
Europe’s civil commonwealths.

Humanists have written civil covenants (constitutions) that make

 

citizenship the product of physical birth or of State adoption (“natu-
ralized citizenship”) rather than citizenship by ritual subordination
to the God of the Bible. In the twentieth century, for example, the
suffragettes got their wish: the right to votc. But the suffragettes were
radicals and humanists, not Christians. They wanted the right of all
women to vote because of their supposed birthright as human
beings. They saw political citizenship as a product of physical birth
in a modern secular democracy. But the Bible does not teach that
men and women have any birthrights, save one, They are born in
sin and corruption, and what they are entitled to, apart from God's
special grace, is a legal right to eternal death,

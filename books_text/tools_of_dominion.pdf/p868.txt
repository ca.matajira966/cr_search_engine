860 TOOLS OF DOMINION

Btological Cursings

God promised to heal them if they remained faithful to Him. But
if they refused to obey Him, He promised to bring them under the
negative biological sanctions that had plagued them in Egypt:

Tf thou wilt not observe to do all the words of this law that are written in
this book, that thou mayest fear this glorious and fearful name, THe Lorp
Tuy Gon; Then the Lorp will make thy plagues wonderful, and the
plagues of thy seed, even great plagues, and of long continuance, and sore
sicknesses, and of long continuance. Moreover he will bring upon thee all
the diseases of Egypt, which thou wast afraid of; and they shall cleave unto
thee. Also every sickness, and every plague, which is not written in the
book of this law, them will the Lorp bring upon thee, until thou be de-
stroyed. And ye shall be left few in number, whereas ye were ag the stars of
heaven for multitude; because thou wouldest not obey the voice of the Lorp
thy God (Deut, 28:58-62).

These negative national sanctions would be visible symbols of a
return to Egypt, a reversal of the exodus, the transition from grace to
wrath. The God who brings health as a corporaie covenantal blessing is also
the God who brings sickness as a corporate covenanial cursing. The text says
specifically that plague is a negative sanction used by God to call His
people back to Him as a covenant unit. This is why God judged
Israel with a plague that killed 70,000 people when He punished
David for illegally numbering the people. “So the Lor sent a
pestilence upon Israel from the morning even to the time appointed:
and there died of the people from Dan even to Beer-sheba seventy
thousand men” (II Sam. 24:15). Sickness in general is also a negative
covenant sanction, (That some Christians affirm the positive sanction
of health as being from God but simultaneously deny the negative
sanction of sickness testifies to their hostility to the biblical doctrine of
covenantal judgment. We must positively confess Christ as Healer
and negatively confess Christ as Plague-master. To refuse to do the
latter is the equivalent of preaching heaven but denying hell.)!*

18. Thave in mind here the so-called “positive confession” charismatics who refuse
to admit that God brings sickness and plagues as covenantal judgments. I suspect
that they have analogous views regarding the inappropriateness of public church
discipline. Televised Pentecostal healers of the 1980's also gained international repu-
tations as the front-page adulterers of the 1980's. (One turned out to be a bi-sexual
adulterer.) When ever-so-mild church discipline was imposed on two of the nos no-
torious of them, they resigned their denominational membership, thereby removing
themselves from further ecclesiastical sanctions. But not from God's sanctions.

 

The Curse of Zero Growth 863

infant mortality is why the statistics show that we live longer in this
century. “Comparison of life tables from various countries at various
times suggests that as life expectancy rises from 35 to 70, about four-
fifths of the increase is contributed by reductions in death rates
under 70... .”26

What has taken place in the industrializing nations during the last
century is simply unprecedented in man’s recorded history: babies
who are allowed by their mothers to survive do survive. (Mean-
while, there are between 35 million and 55 million abortions per-
formed worldwide each year.)?? As population scholar George
Stolnitz concluded in 1955, the rise in Western life expectancy during
the past century has probably been more far-reaching than the gains
of the previous two millennia.* In fact, it is even more remarkable
than this: most of the improvement in Western Europe and English-
speaking North America came between 1850 and 1900.” This is ad-
ditional evidence that the bulk of the West’s gain in life expectancy
since 1900 has come through the reduction of infant mortality, since
this period was marked by rapidly falling rates of infant mortality.
There has been a sharp average rise in life expectancy within the
West, meaning a remarkable decline of differences within the
region.” Today, “West” primarily means high technology and low
mortality rates, not geography, race, or religion.

Doesn’t this deny the premise of Exodus 23:25-26, namely, that
God rewards His covenant people with long life? No; it means that
He rewards those societies that obey His covenant’s external ethical require-
menis even if they do not adhere to the formal theological affirmation
of Trinitarian faith. Like Nineveh, which avoided God’s wrath by
repenting of its external sins, despite the fact that it did not affirm
the covenant,*! the modern world has adopted the Protestant work
ethic and the Puritan concept of time and thrift without accepting
Protestantism.

26. Fuchs, Who Shall Live?, p. 40.

27. World Population and Fertility Planning Techniques: The Next 20 Years (Washing-
tan, D.C. Office of Technology and Assessment, 1982), p. 63.

28. George J. Stolnitz, “A Century of International Mortality Trends,” Population
Studies (July 1955); reprinted in Charles B. Nam (ed.), Population and Society (New
York: Houghton Mifflin, 1968), p. 127.

29. Peterson, Population, p. 547.

30. Stoinitz, op. cit., p. 132.

31, If it had been converted, there would have been signs of covenantal continuity:
point five of the covenant, On the contrary, the Assyrian empire conquered Israel
and carried the nation into captivity.

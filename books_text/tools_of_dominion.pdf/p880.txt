872 TOOLS OF DOMINION

Conctusion

God brings His sanctions in history: cursings and blessings. He
delegates 10 heads of families the authority to dispense positive sanc-
tions to covenant-keeping children. The family unit is the heart of all
economic growth, and therefore the head of the family, as the one
who lawfully allocates the family’s assets, is entitled to grant positive
sanctions to those under his authority.

Church and State are not originally creative economically, but
only corrective and protective ethically. The State provides the insti-
tutional framework of property ownership, which in turn affects cco-
nomic productivity. The church declares God’s ethical standards,
and it provides access to the sacraments which alone make possible
God’s common grace in history. Without common grace, there could
be no economic growth for pagans, and there would be a drastically
reduced division of labor, which would also reduce the wealth of
Christians.** Both church and State are dependent economically on
the blessings of God and the productivity of private citizens because
these covenant institutions serve both God and private citizens.
They possess lawful authority as derived sovereignties—derived
from God and man— which means that they must derive their direct
economic support from those over whom they rule and therefore also
serve. Their authority cannot be separated from their economic de-
pendence on those over whom they exercise authority.

This is one reason why both the tithe and civil taxes are supposed to be pro-
portional to the net output and therefore the net income of those under their jurts-
diction, Civil and ecclesiastical judges are supposed to declare and
enforce God's law, so that the whole society can prosper. They
should be able to expand their income and influence only to the ex-
tent that they serve God and man in a covenantally faithful way. The
visible manifestation of their success or failure in this task is the per-
formance of the economy, including the ability of the economy to de-
liver effective medical services.

Dominion requires the mastery of every area of life in terms of
God’s revealed laws. This in turn requires faithful preaching of the
comprehensive effects of God’s redemption. Christ bought back
everything when He sacrificed Himself. What dominion produces is
order and growth, as well as orderly growth.

56. North, Dominion and Crmmon Grace, pp. 53, 58, 76, 245.

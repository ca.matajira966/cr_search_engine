880 TOOLS OF DOMINION

Recapitulating the Fall

The Israelites had “spoiled” the Egyptians before they left, taking
with them gold and jewels that had helonged to their former masters.
This had been God’s gracious restoration to them of the lost capital
that the Egyptians had extracted from them and their forefathers. !3
These goods offered them a new beginning economically. Tu this ex-
tent, the exodus was a restoration of Eden.

In Eden, God had departed from Adam and Eve for a while.
During his absence, they sinned. Moses also departed, climbing the
mountain of God. During Moses’ initial absence, the Israelites had
insisted to Aaron that they be allowed to sacrifice a portion of this
wealth in order to construct gods to go before them (Ex. 32:1). Aaron
used their gold to construct a calf, and the people then attributed
their victory over the Egyptians to these new gods that were repre-
sented by the calf (Ex. 32:8). They re-enacted the fall of man.

It is not surprising that the Hebrews turned to the sculpture of a
bull when they sought to represent polytheistic power. The Apis bull
was the single most important religious animal in Egypt. The birth
and death of each Apis bull were recorded in Egyptian records as
faithfully as the ritual ordination and death of each Pharaoh. In fact,
only these cvents were important enough in the eyes of the Egyptians
to maintain in official records, dynasty after dynasty.'* The Hebrews
demonstrated by the construction of the calf that their world-and-life
view was still dominated by the theology of Egypt. Though they had
been delivered physically and geographically from Egypt, they had
not yet been delivered spiritually. They still were under the influence
of the religion of their former captors. ‘They were still in spiritual
bondage. For this reason, that first generation of the exodus did not
enter the land of Canaan. They went out of Egypt, but they did not
come into the promised land, They could not return to the sin-filled
pseudo-garden of Egypt, just as Adam and Eve could not return to
the garden. Yet they refused to go forward on God's terms, so they
wandered until they died.

13. Gary North, Moses und Pharaoh; Dominion Religion vs. Pnver Religion (Tyler,
‘Texas: Institute for Christian Economics, 1985), ch. 6: “Cumulative Transgression
and Restitution.”

14. George Rawlinson, History of Ancient Fgypt, 2 vals. (New York: John B. Alden,
1886), II, p. 2.

God's Limits on Sacrifice 885

rifice and ritual, but not ethical regeneration, Here were gods of their
hearts and hands.

Pyramid and Tabernacle

In contrast to the calf that had been crafted by amateurs, with its
religion of professional debauchery, God’s tabernacle was detailed
and magnificent, yet portable. It moved with the people because
God moved before the people, guiding them. To build it, the people
had to dig deeply once again into what remained of their treasure. It
was to be a voluntary sacrifice. They responded enthusiastically (Ex.
35:21-22, 29). The craftswomen contributed the best that they had
(35:25-26), Bezaleel, a craftsman, was given special knowledge from
God to master the arts (35:31), as well as a special gift of teaching
(35:34). He and Aholiab, who also had been given the gift of teach-
ing, became the contractors who directed the building of the taber-
nacle (35:30-34). God imparted special skills to those who assisted
them (35:35). The people brought in their offerings daily (36:3). In
fact, they continued to bring in so much that there was an overflow
of materials (36:5), Moses had to tell them to cease their labors and
to stop bringing in their handicrafts (36:6-7).

A very different structure is the Cheops pyramid of Giza in
Egypt. It remains an architectural and technological wonder. It is
the last surviving edifice of the seven wonders of the ancient world.
Scholars have studied it in great detail. There is even a school of ar-
cane knowledge called “pyramidology,” which attempts to find in its
dimensions prophetic truths.”

No one knows how it was built, but the usual estimate is that
100,000 slaves and 40,000 skilled craftsmen had to work on it for 20
years. Not only is the pyramid a technological wonder—we stil!
have no clear idea of how it was built —it is a mathematical wonder.
This has been recognized by Western scholars for over a century.
John Taylor, editor of the London Observer, and a gifted mathemati-
cian, began playing with the measurements of the Great Pyramid
reported by Col. Richard Howard-Vyse. This was in the 1850's.
Taylor asked why only this pyramid had the angle of 51 degrees and
51 minutes. He found that each of the Pyramid’s four faces had the

23. How it supposedly can do this by means of different measuring systems is in-
deed a wonder.

24. Peter Tomkins, Secrets of the Great Pyramid (New York: Harper Colophon,
[1971] 1978), pp. 227-28. The figure of 20 years comes from Herodotus.

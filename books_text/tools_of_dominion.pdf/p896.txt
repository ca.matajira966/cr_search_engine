888 TOOLS OF DOMINION

man was not based on their ability to reproduce His world in a
model. His presence or absence was established by their covenantal
faithfulness. It was the law that was crucial, not measurements in
stone. It was man’s heart of stone that was his problem, not the de-
sign of the tabernacle. The temple no longer stands because God de-
stroyed it when it no longer served His covenantal purposes. He
would not tolerate those who treated His temple as a talisman.

Thus saith the Lorp of hosts, the God of Israel, Amend your ways and
your doings, and I will cause you to dwell in this place. Trust ye not in lying
words, saying, The temple of the Lorp, The temple of the Lorp, The tem-
ple of the Lorn, are these. For if ye throughly amend your ways and your
doings; if ye throughly execute judgment between a man and his neigh-
bour; If ye oppress not the stranger, the fatherless, and the widow, and shed
not innocent blood in this place, neither walk after other gods to your hurt:
Then will I cause you to dwell in this place, in the land that I gave to your
fathers, for ever and ever ( Jer. 7:3-7).

The Renaissance, with its fatal attraction to magic, misunder-
stood this. Frances Yates, who more than anyone else has opened
this academically closed door of the Renaissance, notes that Isaac
Newton, a dedicated alchemist, was fascinated with Solomon’s tem-
ple. She says that he was “determined to unravel the exact plan and
proportions of the Temple of Solomon. This was another Renais-
sance interest; the plan of the temple, laid down by God himself, was
believed to reflect the divine plan of the universe. For Renaissance
scholars, the theory of classical architecture was believed to derive
from the Temple and, like it, to reflect the world in human propor-
tions.”55 Newton even sketched the temple’s dimensions.

The Renaissance was treating the temple as if it were the Great
Pyramid. It was not. The religion of the Bible is covenantal and ethi-
cal, not metaphysical and magical. God is not to be manipulated;
He is to be obeyed.

Man’s Need of Limits

Limits were placed by God on their sacrifices. Moses did not ask
them to bring in all of their capital in a wave of sacrificial giving,

34, Frances A. Yates, Giordana Bruno and the Hermetic Tradition (New York: Vint-
age, [1964] 1969).

35. Frances A. Yates, Ideas and Ideals in the North European Renaissance, vol. II of
Coltected Essays, 3 vols. (London: Methuen, 1984), p. 270.

36, Frank E, Manuel, Isaac Newton, Historian (Cambridge, Massachusetts: Harvard
University Press, 1963), plate facing p. 148.

The Economics of the Tabernacle 893

cursing. God’s judgment was handed down in terms of the people’s
covenantal faithfulness to the revealed laws of God. Three times a
year the citizens of Israel were required to come before God and
offer sacrifices (Ex, 23:14-19). This meant that they would have to
face God in judgment, as individuals and as a nation. The tabernacle
was God's place of judgment and sanctions in history, To preach on the tab-
ernacle is therefore risky business, for it leads straight to the doctrine
of the covenant, with its five doctrines that so alienate modern evan-
gelicalism: the absolute sovereignty of a predestinating God; the
three hierarchical appeals courts: church, State, and family; the
Bible-revealed law of God that is supposed to govern the decisions of
the judges of all three courts; God’s sanctions in history; and the
disinheritance of covenant-breakers and the inheritance of covenant-
kcepers in history. This also raises the question of the Lord’s Supper
as the church’s covenant-renewing event that brings people into the
presence of God to receive His judgments in history.?

The goal of modern sermons on the tabernacle is to make judi-
cially irrelevant everything associated with the tabernacle in New
Testament times. The discontinuity of the cross has supposedly
made the tabernacle irrelevant today. As a building, this is unques-
tionably true, but this was true in Moses’ day, too. The building was
symbolic; what was symbolized was crucial. What was symbolized
was Jesus Christ as the coming Judge in history. “But Christ being
come an high priest of good things to come, by a greater and more
perfect tabernacle, not made with hands, that is to say, not of this
building; Neither by the blood of goats and calves, but by his own
blood he entered in once into the holy place, having obtained eternal
redemption for us” (Heb. 9:11-12). Thus, every sermon on the taber-
nacle is supposed to point to the relevance of Christ as Judge today.

Judgments in New Testament history? Ethical cause and effect in
New Testament history? Covenantal sanctions in New Testament
history? The authority of God’s law in New Testament history? Such
thoughts are not pleasant to shepherds who have denied all of this
throughout their careers. They have dedicated their lives to the prin-
ciple that Old Covenant history, with all its visible judgments, no
longer operates today. The tabernacle is supposed to become a prin-
ciple of the church’s cultural irrelevance today, for ours is a world
devoid of visible judgments based on covenantal cause and effect.

1, RayR. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: Insti-
tute for Christian Economics, 1987).
2. sbid., pp. 304-13.

 

The Economics of the Tabernacle 895

What Kline is arguing is that the testimony of God’s covenant
law and covenant sanctions in history was scrapped by God after
Christ’s resurrection from the dead. The visible sanctions of God do
not operate in New Testament times. Ethical cause and effect in today’s
culture is random. Christianity is therefore culturally irrelevant and
progressively impotent. The fact is, Kline’s assertion that visible events
are essentially random is a smokescreen that covers up his pessimis-
tic eschatological views. What he really believes is that things will get
worse for the church as time goes on. Ethical cause and effect in New
Testament history is not merely random; it is positively perverse.
This conclusion is basic to Kline’s amillennial eschatology.’ Once
again, we see that covenantal neutrality is impossible.

It should be clear that the tabernacle was not culturally irrele-
vant or impotent in its day. It was basic to the religious life of Israel
for almost half a millennium, until Solomon built the temple, 480
years after the Hebrews came out of Egypt (I Ki. 6:1). The tabernacle
was the resting place of the Ark of the Covenant, which contained
the tablets of the law (Ex. 25:10-22). God appeared at the tabernacle
in the form of a cloud-pillar (Ex. 33:9-40; Num. 12:5; Deut. 31:15).
The tabernacle was filled with gold, silver, jewels, and the finest ar-
tistic accomplishments of the people. It symbolized the majesty of
the supernatural King who was in their midst.®

A Symbol of Covenantal Continuity

These pilgrims in the wilderness were given a symbol of the pres-
ence of God—a fundamental aspect of the biblical covenant.’ They
had a stake in a covenantal society. The tabernacle gave them a place
of sacrifice. God is master of the universe, and men must acknowledge
their subordination to Him through sacrifice.*° The animal sacrifices
would take place at a particular place. The tabernacle could there-
fore serve as a focus for the community’s sense of order and permanence.
The tribes would be drawn together, overcoming the potential frag-
mentation of tribal society.

7. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989), ch. 3; “Halfway Covenant Ethics.”

8. Meredith G. Kline, Images of the Spirit (Grand Rapids, Michigan; Baker Book
House, 1980), pp. 35-42.

9. Sutton, That You May Prosper, ch. 1.

10, Iid., ch. 2.

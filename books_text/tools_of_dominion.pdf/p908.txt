900 TOOLS OF DOMINION

is anything but beautiful. There is a grim, ugly architectural style
that is common to government buildings throughout the West: huge
stones, few windows, marble or imitation marble. These make men
feel insignificant. The buildings dwarf people. This style was
pioneered in ancient imperial Rome. A similar theology of Empire
undergirds today’s structures.

The Soviet Union is the most self-conscious empire we have seen
in modern times. In the decades following the Revolution, the
Soviets produced grand monuments to poor taste. Malcolm Mug-
geridge’s autobiography recalls his stay in Moscow’s National Hotel
during the 1930's as a reporter for Britain’s Afanchester Guardian. “The
decor was in heavy marble and gilt, rather like the stations in the
Moscow underground [subway—G.N.], then under construction,
and to become a tourist show-place. Once, sitting with Mirsky in the
hotel lounge, I remarked upon its excruciating taste. Yes, he agreed,
it was pretty ghastly, but it expressed the sense of what a luxury hotel
should be like in the mind of someone who had only stared in at one
through plate-glass windows from the cold, inhospitable street out-
side. This, he said, was the key to all the régime’s artistic products —
the long turgid novels, the lifeless portraits and landscapes in oils,
the gruesome People’s neo-Gothic architecture, the leaden conser-
vatory concerts and creaking ballet, Culturally, it was all of a piece.
There is no surer way of preserving the worst aspects of bourgeois
style than liquidating the bourgeoisi 720

 

Restoring Cooperation

The theological and institutional fragmentation of the West’s
churches is visible today. The original ecumenical impulse of Chris-
tianity has dimmed. We should expect a future revival to bring new
unity, for the church is now visibly at war with humanist empires, as
it was from Christ's day to Constantine’s. A revival is morc likely to
unify Christians than split them, for there is a visible, threatening
common enemy. Thus, we should expect to see a new ecumenism of
Bible-believing people to rival and offset the collectivist ecumenism
of modernism. It will be a bottom-up ecumenicism, not a top-down
bureaucratic ecumenism.”!

20. Malcolm Muggeridge, Ghronicies of Wasted Time: The Green Stick (New York:
Morrow, 1973), p. 245.

21. Gary North, Healer of the Nations: Biblical Blueprints for Foreign Relations (Ft.
Worth, Texas: Dorainion Press, 1987), ch. ii.

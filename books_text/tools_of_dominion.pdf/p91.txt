What Is Covenant Law? 83

nantally faithful people. This, of course, is postmillennialism.* This
combination of covenant sanctions in history and postmillennial
eschatology is what distinguishes the Christian Reconstructionist
worldview from all others today.#!

Those who deny postmillennialism usually also deny the New
‘Testament reality of God’s law-governed historical sanctions. To this
extent, premillennialists and amillennialists have generally been so-
cial antinomians. They have erred in the development of their view
of God’s law and its sanctions in history. They have allowed their
eschatologies of historical defeat to shape their doctrine of law, i-e.,
making it impotent in its historical effects. This triumph of pessimis-
tic eschatological views over biblical ethics is one of the most devas-
tating theological problems that the modern church faces.

Thus, antinomianism is defined as that view of life which rejects
one or more of the five points of the biblical covenant as they apply
to God’s revealed law in history. They deny that God, the sovereign,
predestinating Creator, has delegated to mankind the responsibility
of obeying His Bible-revealed law-order, Old and New Testaments,
and promises to bless or curse men in history, both individually and
corporately, in terms of this law-order. This law-order and its histori-
cally applied sanctions are the basis of the progressive sanctification
of covenant-keeping individuals and covenantal institutions —fam-
ily, church, and State —~over time, and they are also the basis of the
progressive disinheritance of covenant-breakers.

Definitions and Paradigms

Some readers may not accept my definition of antinomian, but
every reader should at least understand how and why I am using the
term, The biblical definition of God’s law must include all five of the
points of the biblical covenant model. Deny any one of these five
doctrines, and you have thereby adopted an antinomian theology.
Deny them, and you necessarily must also deny the continuing au-
thority of Deuteronomy 28 in the New Testament era. Yet an im-

30. While Galvin did not see this as clearly as modern Reconstructionists do,
there were still elements of postmilleanialism in his theology. On this point, see
Greg L. Bahnsen, “Phe Prima Facie Acceptance of Postmillennialism,” Journal of
Christian Reconstruction, UL (Winter 1976-77), pp. 69-76. I argue that there were both
amillennial and postmaillennial arguments in Calvin’s writings: “The Economic
Thought of Luther and Calvin,” bid., If (Summer 1975), pp. 102-6.

31, Postmillennial Puritans generally shared (his view, which is why Reconstruc-
Uionists regard themselves as neo-Puritans.

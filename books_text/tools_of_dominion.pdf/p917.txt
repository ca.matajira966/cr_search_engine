Blood Money, Not Head Tax 909

references to compulsory payments in ancient Israel are the various
tithes and sacrifices—clearly ecclesiastical—and the census atone-
ment money of Exodus 30.

To overcome this embarrassment, Rushdoony offers a unique
theory of Old Testament civil order and its relation to the taxing au-
thority. “This failure to discern any tax law is due to the failure to
recognize the nature of Israel’s civil order. God as King of Israel ruled
from His throne room in the tabernacle, and ta Him the taxes were
brought. Because of the common error of viewing the tabernacle as
an exclusively or essentially ‘religious,’ i.e., ecclesiastical center, there
is a failure to recognize that it was indeed a religious, civil center, In
terms of Biblical law, the state, home, school, and every other
agency must be no less religious than the church. ‘The sanctuary was
thus the civil center of Israel and no less religious for that fact.”'7

A Question of Sovereignty

He systematically refuses to explore the startling implications of
this theory of the tabernacle as the only place where the Israelites
paid their taxes to God as King of Israel. The issue is clearly not the
“religiousness” of the civil order, for as he correctly says, all of soci-
ety’s institutions are equally religious—State, home, school busi-
ness, etc. But this is not to say that all institutions are equally cove-
nantal, for only three institutions — family, church, and State—bear
the marks of the covenant, namely, the legitimate imposition of a
self-maledictory oath. ®

Church and State collect their lawful payments from those who
are covenanted to each institution, though not necessarily to both in-
stitutions: churches collect tithes from church members, and civil
governments collect taxes from those under their jurisdiction. This
has nothing to do with the question of the “religiousness” of either or
both of these God-ordained covenant institutions, For example, pri-
vate businesses are not entitled to collect taxes from anyonc, except
as agents of the civil government. Yet according to Rushdoony, busi-
nesses are inescapably religious institutions.

Rushdoony’s argument throughout his carcer has been that all of
life is inescapably religious. Following Van Til, he argues that all
men are either covenant-keepers or covenant-breakers. “Neutral

17. Idem.
18. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute for Christian Economics, 1986), ch. 3.

Conclusion 929

concubinage. Egypt would marry Israel, God’s bride, steal the
bride’s God-granted dowry, and declare her a concubine.?

The second presentation of the Pharaoh’s covenant program ap-
pears in the Bible’s description of his enforcement of the infanticide
decree. To achieve this program of stealing the Hebrews’ inheritance,
Pharaoh (the self-proclaimed sovereign) assigned this task of infan-
ticide to representative agents, the Hebrew midwives (hierarchy).
He gave them a command: destroy the newborn males (law). They
disobeyed the command, but instead of being punished by Pharaoh
(negative sanction), God blessed them (positive sanction). And the
people multiplied (inheritance).

In response to this false Egyptian covenant, the sovereign God of
Israel announced to Moses that He was with His people, for He had
seen their afflictions and had heard their cries (Ex. 3:7). He then
raised up Moses, his representative agent, to serve as the earthly
leader of the nation (hierarchy). He gave Moses His laws (law). The
people made an oath to God, which they broke, and God brought
sanctions against them (oath/sanctions), They then repented, re-
newed the covenant, and built the tabernacle, which their sons later
carried into the Promised Land, the lawful inheritance which had
been promised to Abraham (inheritance/continuity).?

The Doctrine of Covenantal Representation

The conflict between Moses and Pharaoh was a representative
battle between God and Satan. It was a battle over the question of
ultimate sovereignty. It was a battle over lawful representation. It
was also a battle over the right to impose sanctions and the right to
collect the inheritance. But primarily Exodus is a battle over repre-
sentation; Mases vs. Pharaoh. Who would represent Israel in the
court of the gods or God of history, Moses or Pharaoh? Which repre-
sentative agent would manifest true covenantal authority in the
midst of time? The answer of the Book of Exodus is clear: Moses.
The Book of Exodus is, above all, a book about representative gov-

2. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Lyler,
Texas: Institute for Christian Economics, 1985), pp. 85-86, “The Slave Wife.”

3. Critics of Ray Sutton’s five-point covenant model can and do continue to deny
the appearance of this outline again and again in the Bible. T believe that this blind-
ness testifies to the inability of those who cling to an old paradigm to understand the
evidence of the new one. They, of course, will reply that those of us who see the cov-
enant structure clearly in the text are reading our invention into the text. ‘I'ime and
the final judgment will tcll whose view is correct.

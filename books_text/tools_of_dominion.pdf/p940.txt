932 TOOLS OF DOMINION

Who Is Our God?

Rushdoony writes that “in any culture the source of the law is the god
of that society.”> The source of biblical law is the God of the Bible. His
moral character is revealed in His laws —al! His laws, not just the
Ten Commandments. Without biblical law at the center of a
society’s legal order,® its legal order testifies falsely regarding the true
source of all morally valid laws, namely, the God of the Bible. It tes-
tifies falsely regarding God. A society is in rebellion against God to
the extent that its people refuse to acknowledge in the civil realm the
Bible-mandated terms of the civil covenant. There is a specific legal
order which God requires the State to uphold by force and the threat
of force. God is totally sovereign, as manifested by the presence of
His required laws and sanctions. A society that denies the continu-
ing judicial validity of Old Testament civil law in general thereby
refuses to acknowledge that this world was, is, and ever shall be a
theocracy. Ged rules. How does a nation testify in history to this fact?
Ged’s rules. To the extent that the legal order does not conform to the
legal standard that God announces in His Bible, to that extent is a
society in rebellion against God.

Renouncing a Tool of Evangelism

This is denied by virtually all Christian denominations and con-
gregations today. They deny that God reveals himself judicially to
men in New Testament times as clearly as He did in the Old Testa-
ment. Christians should ask themselves: Why would God choose to
reveal himself less clearly in the New Testament era by allowing
every society except Puritan New England to adopt a law-order that
is openly a renunciation of what He has revealed as judicially bind-
ing in the Old Testament? The theonomists have an answer to this
intriguing question. God allows this in order to reveal the visible
failure in history of all rival law-orders compared to the visible suc-
cess of His revealed law-order. This necessarily implies that at some
point in the future, there will be such a visible example. The visible
failure of rival civil law-orders, meaning rival gods, can then become
a worldwide tool of evangelism.

5. R. J. Rushdoony, ‘he Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 4.

6. At the center of Israel was the Ark of the Covenant. In the Ark was the law:
the two tablets

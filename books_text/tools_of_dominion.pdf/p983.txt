Common Grace, Eschatology, and Biblical Law 975

least the tares help keep the soil from eroding. Better tares than the
destruction of the field, at least for the present. They serve God,
despite themselves. There has been progress for both wheat and
tares. Greek and Roman science became static; Christian concepts
of optimism and an orderly universe created modern science. Now
the tarcs run the scientific world, but for how long? Until a war?
Until the concepts of meaningless Darwinian evolution and modern
indeterminate physics destroy the concept of regular law— the foun-
dation of all science?

How long can we go on like this? Answer: until epistemological
self-consciousness brings Christians back to the law of God. Then
the pagans must imitate them or quit. Obedience to God alone
brings long-term dominion,

Law and Grace

The dual relationship between common law and common curse
is a necessary backdrop for God’s plan of the ages. Take, for example,
the curse of Adam. Adam and his heirs are burdened with frail bodies
that grow sick and die. Initially, there was a longer life expectancy
for mankind. The longest life recorded in the Bible, that given to
Methuselah, Noah’s grandfather, was 969 years. Methuselah died
in the year that the great flood began.’° Thus, as far as human life
is concerned, the greatest sign of God’s common grace was given
to men just before the greatest removal of common grace recorded
in history.

This is extremely significant for the thesis of this essay. The exten-
sion of common grace to man—the external blessings of God that are
given to mankind in general —is a prelude to a great curse for the unregen-
erate. As we read in the eighth chapter of Deuteronomy, as well as in
the iwenty-eighth chapter, men can be and are lured into a snare by
looking upon the external gifts from God while forgetting the heav-
enly source of the gifts and the covenantal terms under which the gifts

10. Methuselah was 969 ycars old when he died (Gen. 5:27), He was 187 years
old when his son Lamech was born (5:25) and 369 years ald when Iamech’s son
Noah was born (5:28-29). Noah was 600 years old at the time of the great flood
(7:6). Therefore, from the birth of Noah, when Mcthusclah was 369, until the flood,
600 years later, Methuselah lived out his years (369 + 600 = 969). The Bible does not
say that Methuselah perished in the flood, but only that he died in the year of the
flood. This is such a remarkable chronology that the burden of proof is on those who
deny the father-to-son relationship in these three generations, arguing instead for an
unstated gap in the chronology.

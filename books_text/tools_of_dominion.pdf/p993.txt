Common Grace, Eschatology, and Biblical Law 985

punishes covenant-breakers in time and on earth—then you are
preaching a system of positive growth. You are preaching the domin-
jon covenant. Only if you deny that there is any relationship be-
tween covenant-keeping and external success in life—a denial made
explicit by Meredith G. Klince'*—can you escape from the postmil-
lennial implications of biblical law. This is why it is odd that Greg
Bahnsen insists — perhaps tor tactical reasons — on presenting his de-
fense of biblical law apart from his well-known postmillennialism.!”
Kline attacked both of Bahnsen’s doctrines in his critique of Theonomy,}®
and Bahnsen in his rebuttal essay did respond to Kline’s criticisms of
his postmillennial eschatology, but he again denies that eschatology
has anything logically to do with biblical ethics.'? But Kline was cor-
rect: there is unquestionably a necessary connection between a cove-
nanial concept of biblical law and eschatology. Kline rejects the idea
of a New Testament covenantal law-order, and he also rejects post-
millennialism,

Amillennial Calvinists will continue to be plagued by Dooye-
weerdians, mystics, natural-law compromisers, and antinomians of
all sorts until they finally abandon their amillennial eschatology.
Furthermore, biblical law must be preached. It must be seen as the
tool of cultural reconstruction. It must be seen as operating now, in
New Testament times. It must be seen that there is a relationship
between covenantal faithfulness and obedience to law ~ that without

16. Kline says that any connection between blessings and covenant-keeping is,
humanly speaking, random. “And meanwhile it [the common grace order] must run
its course within the uncertainties of the mutually conditioning principles of com-
mon grace and common curse, prosperity and adversity being experienced in a
manner largely unpredictable because of the inscrutable sovereignty of the divine
will that dispenses them in mysterious ways.” Kline, op. cit., p. 184. Dr, Kline has
obviously never considered just why it is that life insurance premiums and health in-
surance premiums are cheaper in Christian-influenced societies than in pagan
socicties. Apparently, the blessings of long life that are promised in the Bible are
sufficiently non-random and “scrutable” that statisticians who advise insurance com-
panies can detect statistically relevant differences between societies.

17. “What these studies present is a position in Christian (normative) ethics. They
do not logically commit those who agree with them to any particular school of exchato-
logical interpretation.” Greg L. Bahnsen, Ay This Standard: The Authority of Gud’s Law
Today (Tyler, Texas: Institute for Christian Economics, 1985), p. 8. He is correct:
logically, there is no connection. Covenantally, the two doctrines are inescapable: when
the law is preached, there are blessings; blessings lead inescapably to victory.

18. Kline, op. eit.

19. Greg L. Bahnsen, “M. G. Kline on Theanomie Politics: An Evaluation of His
Reply,” Journal of Christian Reconstruction, V1 (Winter, 1979-80), No. 2, especially p.
215.

 

 

24 Trespassing for Dear Life: What About Operation Rescue?

imperatives must be obeyed, and to obey it, the other imperative
must be disobeyed, This has precipitated a crisis.

There is a much larger group of Christians that pretends that
there is nothing inherently contradictory about these two signs.
There is nothing going on behind closed clinic doors that Chris-
tians have a moral imperative and judicial authorization from
God to get more directly involved in stopping. They prefer not to
think about the two signs. They see the first one and assume that
it has the highest authority.

There have been other “No Trespassing” signs in history. Out-
side of German concentration camps in 1943, for instance, But
Christians in Germany honored those signs. They forgot the
words of Proverbs:

If thou faint in the day of adversity, thy strength is small. If
thou forbear to deliver them that are drawn unto death, and
those that are ready to be slain; If thou sayest, Behold, we knew it
not; doth not he that pondereth the heart consider it? and he that
keepeth thy soul, doth not he know it? and shall not he render to
every man according to his works? (Proverbs 24:10-12).

The Christian critics of physical confrontation have offered
many arguments to prove that non-violent interposition by Chris-
tians is always morally, legally, and even theologically wrong. Others
have argued that it is not always wrong, but it is wrong today.

The critics freely admit, as one of them proclaimed, “After
many years of opposing abortion in America, at the cost of mil-
lions of dollars and thousands of lives, nothing has changed.” This
is understated. It has been at the cost of millions of dollars and tens
of millions of lives, What is his conclusion? That Christians now
need to escalate their confrontations, to keep the pressure on?
That a decade and a half of peaceful picketing and political mobil-
ization has “tested the judicial waters,” and it is now time for
Christians to start swimming upstream in order to avoid going
over the falls?

No, indeed; rather, he concludes that Christians should now
abandon these direct physical confrontations, since peaceful con-

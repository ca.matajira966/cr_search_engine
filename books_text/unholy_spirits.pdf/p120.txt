il4 UNHOLY SPIRITS

manistic, resist the findings of parapsychology? In good Freudian
fashion, Eisenbud blames that old standby, the mother-child rela-
tionship. Modern scientism, being mechanistic, is a product of
men’s guilt. Men always hated their mothers, The all-powerful
mother, not the father, was the threat to man’s autonomy. To sup-
press guilt feelings, men construct for themselves a universe that
eliminates responsibility because it substitutes impersonal, mechan-
istic forces for human choice. Thus, parapsychology is a threat to
scientific orthodoxy.®* It threatens to dredge up the personal side of
science, such as the subconscious minds ability to project images.
Man’s “dark and sinister side,” as Eisenbud calls it, is being brought
into view by parapsychology. Man’s mind has power independent of
machines — or at least the subconscious mind does. Orthodox mech-
anistic science is repelled by such a notion. “It is hardly to be won-
dered that it automatically sees anything faintly suggestive of the
power of thought as superstitious nonsense to be rejected firmly and
out of hand.”8

it seems likely that other reasons are also involved in orthodoxy’s
rejection of parapsychology, or at least of the data of parapsychology.
Scientists see only too clearly that phenomena like these are not ex-
plainable in terms of any known view of man, and there is the scent
of the supernatural in the air. Furthermore, while there is a mechan-
istic tradition in all modern science, a point made clear half a cen-
tury ago in E. A. Burtt’s Metaphysical Foundations of Modern Physical
Science, there has always been a “personalist-indeterminist” strain in
scientific orthodoxy. Eisenbud and the other paranormal scientists
are trying to enlarge the field of science to include more of the “nat
yet known,” but their colleagues are convinced that this kind of “not
yet known” is just too close to the demonism that was only too well
known three centuries ago.

Eisenbud’s summary of the knee-jerk reaction of establishment
science to the troubling data of parapsychology is illuminating:
“Science, like a well-behaved compulsive neurotic, is committed to
following out blindly a conspiracy of denial and rejection that is bred
into its very marrow. As a result anthropologists automatically take
it for granted that the stories and legends of the occult they have
been collecting these many years from their primitive informants
have no basis whatever in fact. Psychiatrists and psychologists, for

 

85. Ibid., p. 319.
86. Ibid, p. 322.

The World of a Sorcerer 145

ily undercut Einstein's old-fashioned defense of an independently co-
herent reality. First, Kant triumphed over Newton, as men were told
that the innate categories of their minds provide order to the random
noumena) world “out there.” Then, after 1964, a world view close to
don Juan’s began to triumph over Kant, at least within a growing
segment of the student population, as well as inside the American
book-buying public. People began to take Castaneda’s anti-rational
proposition quite seriously: there is a separate reality. In fact, there
are many realities. Each autonomous man makes his own, in terms
of his own categories. The linguistic confusion of the tower of Babel
is now paralleled by the confusion of the “universal language” of
Kant’s categories. (This is the essence, I suspect, of the prevailing
epistemology of all those in hell: total autonomy and isolation.)
Kant’s faith that universal categories of the human mind necessarily
exist and give order to the world is being abandoned; it is “every
man for himself.” The acids of philosophical relativism had finally
eroded the confidence that intelligent Western people once had in the
distinctly absolutist world view of nineteenth-century philosophical
mechanism.

Don Juan’s vision rested on a philosophical premise: all is not
what it appears to our reason. The Western process of intellectual ra-
tionalization of the perceived world is explicitly rejected. “But to be a
sorcerer in your case means that you have to overcome stubbornness
and the need for rational explanations, which stand in your way.”
Again: “We men and all the other luminous beings on earth are per-
ceivers. That is our bubble, the bubble of perception. Our mistake is
to believe that the only perception worthy of acknowledgment is
what goes through our reason. Sorcerers believe that reason is only one
center and that it shouldn’t take so much for granted.”°

What the old man is attacking is the assumed sufficiency of phe-
nomenal knowledge. He forces Castaneda to look at the noumenal
side of perception; and unlike Kant, who simply used the noumenal
realm as a silent backdrop or limiting concept to deal with the prob-
lem of philosophical contradictions in human thought, don Juan
really believes that the noumenal realm is accessible. Not accessible
to reason, however, but accessible to the nagual: the actually existing
magical half of man’s dualistic existence. The nagual is the occult

66. Tales of Power, p. 193.
67. Ihid., p. 249

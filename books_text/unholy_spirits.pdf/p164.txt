158 UNHOLY SPIRITS

He said the rising certainty of the devil’s activity — along with the slight
drop in certainty about God—suggests people have difficulty seeing any
good purpose or reason in events and consider the balance of “good versus
evil to be tipped in the direction of evil.”

The study also brought out that those convinced of the devil’s reality are
much more likely than others to feel that threatening forces are at work in
modern life, and that things are likely to get worse.

Faith in God is slipping, but faith in demons is increasing. We
are back to Screwtape’s letter to Wormwood: the materialists are be-
ginning to believe in demons but not in God. This is the very heart
of the demonic cause. The culture that indulges itself in demonic ex-
perimentation faces the loss of its shields.

Power Religion and Dominion Religion

Again and again in my writings, I return to this theme. The
essence of biblical religion is ethics. The ethical self-government of
the redecmed man is the foundation of society. God’s law, not the au-
tonomous laws of the universe, or the mind of man, or the dreams of
men, is the basis of all order, including social order." It is from eth-
ics that we proceed to dominion.

This world view is future-oriented and confident. It sees man’s
primary struggles as ethical, not metaphysical. We struggle against
powerful forces, but we use biblical law as our guide, and call upon
God’s Holy Spirit to enable us to apply that law successfully in our
lives and institutions. Progress is ethical, intellectual, and also cul-
tural and external. Progress is real, but it is necessarily progress in
terms of a permanent standard: biblical law.'® Self-discipline is of
greater importance than precise ritual.

The world of the sorcerer is the mirror image of the dominion re-
ligion’s conception of God's world. It is a world inhabited by powers.
These powers battle against man in terms of ritual; any ritual error
on man’s part, or any flinching, leads to disaster. Men try to harness
these powers: by ritual, by subservience, or by calling even stronger
powers against them. Ethics is irrelevant.

If all is power, then the most powerful gets to impose his concept

117, R. J, Rushdoony, The Foundations Of Social Order: Studies in the Creeds and Coun-
ils of the Early Church (Fairfax, Virginia: Thoburn Press, [1969] 1978).

118. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
Texas: Institute for Christian Economics, (985).

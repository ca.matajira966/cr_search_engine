174 UNHOLY SPIRITS

master of the cosmos. The mysterious synchronicities (to use Carl
Jung’s term) of the universe serve as avenues for man’s mysterious
sovereignty over the universe (including over other men). The pow-
ers of occultism need not be accompanied by tappings, philosophies
of reincarnation, or creeping ectoplasm. Twentieth-century human-
ism serves occultism quite well.

Gerard Croiset

On January 6, 1957, a Dutch psychic, Gerard Croiset, met with
W.H. C, Tenhaeff, a professor of parapsychology at the University
of Utrecht, along with biologist L. H. Bretschneider and physicist J.
A. Smit. Croiset was handed a seating plan for a meeting to be held
25 days later. The room to be used for the meeting would contain 36
chairs. The guest list had already been made up. Croiset was asked
to sclect a chair. He chose chair #9. He was then asked to describe
the person who would sit there. He spoke into a tape recorder and
gave a dozen specific facts about the person, plus some additional
material relating to four of the twelve general featurcs. Some of the
facts included were as follows:

1. She would be a middle-aged woman. She is interested in
child care.

2. Between 1928-30, she was around a circus in the town of
Schevenigen.

3. As a child, she lived in a cheese-making district.

4, There was a fire on a farm in her childhood.

5. Three boys came to mind. One is working in British
territory.

6. She has been looking at a picture of a maharajah.

After the recording was finished, this session ended. On Feb. 1,
two packs of cards, numbered 1-30, were selected by the experiment-
ers (but not Croiset). As the meeting was about to begin, both sealed
packs were opened independently. One set was used to place a card
on each chair; the other was distributed on a random basis to the
guests who were in another room. The guests went in and sat down.
in chairs bearing the number corresponding to their cards. They
were instructed not to touch any other chair. Once seated, they
awaited Croiset, who entered a few minutes later. The tape was
played back to the audience. The lady seated in chair #9 admitted

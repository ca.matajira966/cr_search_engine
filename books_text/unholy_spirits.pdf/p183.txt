Psychics 7

These are documented cases. Dutch police departments have
used Croiset and other psychics working with Prof. Tenhaeff on
many difficult cases. Police officials are seldom willing to resort to
such aid unless they regard it as absolutely indispensable, since psy-
chic methods of detection are hardly what fellow Jaw enforcement
agents would call professional. This may not be the kind of evidence
which impresses scientific investigators of the paranormal, but gen-
erally scientists are so hostile to such phenomena that they are will-
ing only to accept negative findings. Policc departments do not con-
tinue to use potentially embarrassing assistants in order to produce
negative findings.

What is the source of Croiset’s abilities? He claims that they are
gifts from God. But there is a curious side to these abilities. They
come upon him while he is in a state of altered consciousness — “low-
ered consciousness,” as his biographer puts it. He obtains visual im-
ages of the person or event. With other psychics, this state of altered
consciousness may resemble a trance; with Croiset, it is barely no-
ticeable by an outsider.'? Sometimes these visions are intensely relig-
ious, and he places great importance on them. One of them is crucial
for an understanding of the source of his powers, for it indicates the
link between psychic visions and magic.

In one of these visionary states, at the age of thirty, there appeared to
him a hast of angels in a blaze of golden light. He also ‘saw’ two guardians
holding trumpets. The next day these guardians again appeared to him,
standing by a curtain which was slightly pulled aside. ‘I'hrough the opening
a stage was visible and a man clad in oriental dress, wearing a turban with a
splendid diadem, stepped forward. Suddenly, Croiset saw that the
Oriental’s face began to show Ais own features! “To Croiset, the Oriental sym-
bolized a paranormally gifted superman,” explains Dr. ‘lenhaetf, “Croiset’s
vision meant to him the promise that, in the future, he would accomplish
great things as a Magus—the more so becausc the Oriental’s face started to
show his own features. Croiset felt that he was receiving the call from a
Higher Power.””

Tt is not uncommon for people who exhibit psychic powers to
have developed them at a very early age. Sometimes an adult trains
the child in occult practices. (The grandmother of the little boy in
Thomas Tryon’s novel, The Other, has had real-life counterparts for

19. Hhid., p. 20.
20. Ibid., p. 27.

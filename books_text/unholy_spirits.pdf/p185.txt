Psychics 179

nore him completely. That is the safe approach; it is also the dishon-
est approach. But at least it had one healthy byproduct; up until the
shattering of faith in orthodox science which began afler 1964, pro-
fessional skepticism kept the Croiset-type phenomena from spread-
ing into the mass media. Skepticism served as a barrier to truth, but
it also served as a barrier to the highly personal and deeply hostile
demonic threat to man and culture. As a result, for many years
Croiset had few imitators with access to popular culture, at least in
the United States. The problem today, however, is that skepticism is
no longer an effective barrier, and once the public has concluded that
mere denial of such powers is not a valid critique, that same public ts
left without a guide as to what, in fact, the sources of such power
really are. Modern science had but one defense, skepticism, and that
defense no longer convinces millions of readers.

Peter Hurkos

Tf Croiset’s abilities came as a result of an overactive imagina-
tion, or if his overactive imagination came as a result of his discovery
of his powers, these powers were nevertheless developed gradually.
Not so with Pieter Cornelius van der Hurk. His paranormal psychic
abilities came almost in an instant. As a young man, he had assisted
his father as a house painter. In July of 1941, he was painting the out-
side of a building four stortes up. He slipped and fell to the carth,
breaking his shoulder and suffering a concussion. When he awoke,
he found that he had “another mind.” He could not remember many
facts about his past, but he could remember facts about other
people’s lives. Furthermore, he had other arcane abilities: psycho-
metry, astral projection, visions, and most significantly, veices. More
accurately, a single voice which spoke to him about secret things. As
he was to say years later, “I felt | had no mind of my own,””

Multimillionaire C, V. Wood, Jr., president of McCulloch Oil
Corporation and the developer of Arizona’s Lake Havasu, was co-
founder of an organization called Mind Science Foundation. He has
been associated with numerous research projects into psychic abili-
ties. He writes: “Within the Mind Science Foundation we have stud-
ied many persons who have had certain psychic abilities, and in
almost all cases the ability developed after cither experiencing a fall
or being hit on the head, or atter having an extremely high fever.”*

 

22. Browning, Hurkos, p. 48.
23. Bid., p. viii.

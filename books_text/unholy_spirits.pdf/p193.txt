Psychics 187

with vibrations. It is not just a catchy phrase with psychics. In fact, it
became a catch phrase in the late 1960’s only when the language of
the occult was transmitted by the counter-culture to the general pop-
ulation. The literature of occultism reveals a continuing theme: the
whole universe is composed of vibrations. Donald Hatch Andrews, a
professor of chemistry, has attempted to fuse this religious perspec-
tive with modern, post-Heisenberg physics, and he has concluded
that the universe is not so much a huge machine as an enormous
melody. He thinks that “the universe is composed not of matter but
of music.”* It is not surprising that his book was not published by a
scientific publishing house but by Unity Books, an organ of a promi-
nent science of mind type of organization.*!

Mrs. Dixon finds that vibrations can reveal many things about a
person's life. She once diagnosed the health of a friend of Ruth
Montgomery’s. “Ruth, I have picked up your friend’s vibrations, She
was suffering some pain in the intestinal region a few minutes ago,
which I believe is a chronic condition caused from nervous strain,”#
Her diagnosis proved completely accurate. But not only do vibra-
tions come from people, they come from objects, especially her old
pack of playing cards, also a gift of the mysterious gypsy: “The sweet
old gypsy gave me those cards when I was eight years old, and be-
cause she blessed them they carry good vibrations. I don’t know a
single thing about telling fortunes with cards. I simply have a person
hold them so that I can pick up his vibrations. It sometimes helps me
to pull out his channels.”

Houses also give off vibrations. These can sometimes be signifi-
cant. “The moment that I walked into that Nineteenth Street house I
seemed to feel God putting his arms around me. I knew it was for
me. All the vibrations were right.’* Even the future gives off vibra-
tions, which is why they can be used for prophetic purposes. Mrs.
Dixon said throughout 1948 that Truman would beat Dewey, and a
week before the election, she announced once again that Truman
would be the victor, She went down to campaign headquarters to
give a donation the Saturday before the election, and she said to a
friend who was working there, “Madeline, everyone thinks I’m

40. Donald Hatch Andrews, The Symphony of Life (Lee Summit, Mo.: Unity
Books, 1966), p, 9

41, Richard Rhodes, “Heaven on Earth,” Harper's (May 1970).

42. Montgomery, Gift, p. 25.

43. [bid., p. 26.

44. Did, p. 74.

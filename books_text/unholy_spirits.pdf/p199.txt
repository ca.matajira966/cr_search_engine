6
EDGAR CAYCE: FROM DIAGNOSIS TO GNOSIS

And as it is appointed unto men once to diz, but after this the judgment;
so Christ was once offered to bear the sins of many; and unto them that
look for him shall he appear the second lime without sin unto salvation.

Hebrews 9227-28

I first heard of Edgar Cayce [KAYsee] in the spring of 1964. His
son, Hugh Lynn Cayce, was promoting his newly published book
about his father’s work, Venture Inward. He was appearing on Long
John Nebel’s midnight-to-dawn radio show which originated on
New York’s WOR. Beamed all over the East Coast, Long John’s
program drew literally millions of listeners. A former carnival
barker, Nebel opened the microphones to every sort of occultist, fun-
damentalist, libertarian, renegade medical practitioner, and fringe
group member imaginable. There was only one requirement: they
had to tell interesting stories—five hours’ worth. A lot of them did.
None was more intriguing than the story of Edgar Cayce.

From 1901 until his death in 1945, Edgar Cayce won a following
of faithful believers and less faithful financial contributors as a result
of a unique occult facility. By putting himself into a trance, and by
assigning a “control” agent to ask him questions, he could diagnose
people’s physical illnesses. Not only diagnose them, but prescribe
remedies, many of them completely unorthodox and seemingly un-
related to the illness, that would be found to be helpful and very
often completely successful. He would simply loosen his tie, take off
his shoes, lie down on a couch, and go into his trance state. The con-
trol agent would then give him the name and address of the person
to be diagnosed. Then would come the familiar words: “Yes, we have
the body.” The diagnosis would then begin. A secretary would record
the diagnosis and recommendations, and these notes would then be
put into Cayce’s permanent files. By the time of his death, the Cayce

193

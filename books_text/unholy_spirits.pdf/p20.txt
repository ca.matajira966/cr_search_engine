14 UNHOLY SPIRITS

estantism, but the occultism of the Enlightenment went under-
ground into the various secret societies and revolutionary groups
that simmered throughout the nineteenth century.’* Only since 1965
have we seen the revival of visible occultism.

This revival of occultism marks the end of an older rationalist
civilization and points to the establishment of a new onc: a self-
conscious Christian civilization which is dominion-oriented. The
only other possible contenders are Communism, which is the power
religion of our era, and which is utterly bureaucratic, parasitic, and
destructive, or New Age humanism, the major escapist religion,
which is compromised by occultism and the theology of occultisrn.
Neither can lead to a new civilization.

The new occultism is not at odds with the new humanism. It is
only partially at odds with the older humanism. This is why the tran-
sition came overnight, 1964-65. This is why the political ideals of the
New Left can be fairly described as radical extensions of the main
political goals of the old Left. The solutions were the same: more
taxes on the rich, more government jobs (especially for college grad-
uates), more Federal spending on everything except national de-
fense, more welfare for the poor (administered by college graduates),
more sexual frecdom (including abortions and homosexuality), more
deficit spending for the sake of growth, and an end to the draft.
Above all, an end to the draft. Nisbet called the older liberalism’s
program “liberalism and six percent,” The New Left was simply
more consistent, more radical, younger, and had fewer responsibili-
ties—i.e., few children, 1965-70—than the old Left.

With this as background, the revised edition of my book on the
occult becomes more useful, Some of these developments were not
clear to me in 1976, especially the rise of the New Christian Right
and its distinct though hesitant rediscovery of biblical law and op-
timism toward the future. The most important chapter is chapter
ten, “Escape from Creaturehood,” but you should read the whole
book to find out why it is the most important chapter.

14. James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith (New
York: Basic Books, 1980).

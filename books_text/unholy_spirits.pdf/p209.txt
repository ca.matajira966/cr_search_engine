Edgar Cayce: From Diagnosis to Gnosis 203

both sexes asking “Who am I?”, but “Who was I?”

At first, Cayce rejected the whole idea of reincarnation. He was
convinced that the Bible rejected the concept. He was absolutely cor-
rect. But slowly and steadily, he changed his beliefs. He began to ap-
ply twisted interpretations to biblical passages (but never to He-
brews 9:27-28) in order to discover reincarnation. As one of his long-
time followers puts it: “The active membership of the A.R.E., as it is
usually called, is made up of people of all religious faiths and many
nationalities, including foreign countries. Strangely, they all seem to
be able to reconcile their faiths with the philosophy emerging from
the Cayce readings.”** Cayce set the pattern. The fact is that these
people do have one religion, the Cayce religion. They ignore the
teachings of their secondary, official religious memberships.

The importance of his self-conversion, if that is really the accur-
ate way to describe the relationship between his conscious mind and
the messages that proceeded from his trance-induced voice, cannot
be overstressed. One follower hit the nail on the head when she
wrote: “Had his physical readings not been proven accurate and use-
ful in all the years he had been giving them, he probably would have
turned from this new development in his psychic work and never
given another reading of any kind.”

Cayce knew his Bible. He had asked himself and his wife for
many years whether his work was of God or the devil. Since his phi-
losophy after 1923 denied the existence of the biblical devil, his fol-
lowers ignore the crucial question. Yet they should know that he
once asked it. He concluded, using the argument of pragmatic hu-
manism, that if good is accomplished, a work must be from God.
Healing people's sick bodies is obviously good. For thirteen years he
had been doing just that. The voice had never lied before. Always
before, he explained to Lammers, the voice had confined itself to
healing. Nothing was ever said about reincarnation. He had always
believed in the Christian view of life: “. . . souls born into the earth,

 

she had not agreed with The Search for Bridey Murphy: p. 10, She had herself hypnotized
in the carly 1970's in order to explore the possibility of past lives. She had often won-
dered where the vivid material for her historical novels had come from: pp. 18-19.
She had long been visited by a “nameless Presence.” She could only sense it, she
said. “Tt was often her guide late at night when she was writing; and when she was
traveling in some distant corner of the globe, it would often appear at her elbow and
remind her that this was the ground she had traveled before” {p. 20).

24, Bro, Religion, p. 264.

25, Agee, ESP, p. 31.

26, Sugrue, River, p, 147.

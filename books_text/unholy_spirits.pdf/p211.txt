Edgar Cayce; From Diagnosis to Gnosis 205

Where is Atlantis? Cayce’s followers find hints in the record that
would point to Bimini. Cayce prophesied a great series of cataclys-
mic geological upheavals for the period 1958-98. Edgar Cayce on Proph-
ecy is filled with specific prophecies, so far completely erroneous,
relating to these upheavals. Jess Stearn, as late as 1967, somehow
took Cayce’s rambling prophecies quite seriously. Part of Atlantis
will reappear soon, he said in 1940. Atlantis had broken into five
islands. One of them was called Poseidia. (Why anyone bothered to
name a handful of islands as they were breaking up is a mystery.) In
any case, a June, 1940 reading was quite specific: “And Poseidia will
be among the first portions of Atlantis to rise again. Expect it in
sixty-eight and sixty-nine. Not so far away.”?° Not so far away in
1940, but just far enough away so that nobody was likely to be able to
challenge the prophet in his lifetime. His geological predictions were
equally as specific and equally as accurate. Yet Stearn saw fit to
devote chapters 3-5 to earthquake predictions, plus chapter 13 to
Atlantis. Even more pathetically, there is some poor Cayce convert
who apparently has professional credentials as a geologist, who is
devoting his career to a search for Atlantis. He serves as the official
apologist for the geological portion of the Cayce readings. His name
is not given by Stearn (a sign of Stearn’s charity), but Stearn
reverently refers to him as “the Geologist.” The Geologist fully ex-
pects to see the fulfillment of Cayce’s prophecies that, among other
earth-shaking events, parts of the West Coast will break up and slide
into the sea, probably in the latter portion of the 1958-98 period.*!
Japan will also slide into the sea.3? The earth’s axis will tilt.3? The
Great Lakes will empty into the Gulf of Mexico.# In 1932, he
predicted a great catastrophe for 1936, which would involve a shak-
ing of world powers, both politically and geologically. In retro-
spect, it is not clear whether this prophecy referred to the annexation
by Italy of Ethiopia, the abdication of Edward VIII of England, the
coronation of King Farouk of Egypt, the exile of Trotsky from the
Soviet Union, the Presidential candidacy of Republican Alf Lan-
don, or the Spanish Civil War. Take your pick.

Two of the criteria of demon possession used by the Roman

30. Stearn, Prophet, p. 229.
31, dbid., p. 40,
32. Ibid., p. 27.
33. Ibid., p. 39.
34, Ibid., p. 34,
35. Ibid., p. 35

216 UNHOLY SPIRITS

relieve men of worrying about a final judgment —or would if it were
true. Furthermore, the voice affirmed that this karmic debt of self “is
true for every soul.”?2 Man saves himself, for he can cancel debts owed
to other souls by forgiveness, while he cancels his only other debt—
the self-owed karmic debt — by working out his salvation through his
own labor.

Salvation is therefore an evolutionary process. “Then, karmic forces ~if
the life application in the experience of an individual entity
GROWS to a haven of peace and harmony and understanding; or
ye GROW to heaven, rather than going to heaven; ye grow in grace,
in understanding.”

Q, Are souls perfect, as created by God in the first? If so, why is there
any need of development?

A. The answer to this may only be found in the evolution of life, in
such a way as to be understood by the finite mind. In the first cause or prin-
ciple all is perfect. That portion of the whole (manifested in the creation of
souls) may become a living soul and equal with the Creator. To reach that
position, when separated, it must pass through all stages of development, in
order that it may be one with the Creator. ’*

This is the theology of ancient gnosticism. It involves a steady
evolution of spirit back into God. Hugh Lynn Cayce, summarizing
his father’s faith, writes: “Through the guidance of the Christ-Soul
the earth has been made a ladder up which souls may return to a
consciousness of atonement with the Creator. Through a series of in-
carnations in matter in human form the soul can learn to cleanse it-
self of selfish desires blocking its more perfect understanding and to
apply spiritual law in relation to matter. Urges created in the mate-
rial plane must be met and overcome, or used, in the material
plane.”?5 This concern with matter, in contrast to ethical rebellion, is
characteristic of all gnostic sects.

Orthodox Christianity teaches that men will have their bodies re-
stored to perfection ~ material bodies. We shall be like Jesus, the in-
carnate son of God, in His perfect humanity (I Cor. 15). Men were
made in the image of Gad; they do not partake of God’s divinity. For

72, Hdem,

73. Ibid., p. 17.

74. Ibid., p. 255.

75. H. L. Cayce, Venture Inward, p. 75

The Crists of Western Rationalism 19

ing with occultism, magic, witchcraft, and paranormal science re-
flected the climate of opinion of the day. There were few potential
buyers of such books, it was thought (probably correctly), in the
United States, and cven fewer writers.

Almost overnight, the picture changed. One of the most radical
discontinuities ever recorded in American culture took place. There
was no one cause and no single zone of American life—also, West
European life--untouched by the multiple dislocations of 1964-70.
In music obviously, but also in clothing, deportmemt, entertainment
in general, sexual roles (especially among the young), theology,
church liturgy, academic curricula, politics, economics: change was
accelerating at a tremendous ratc.

To illustrate my point, try to imagine a history of the 1960’s that
would not include a section on the Beatles. It would not be too diffi-
cult to write a history of the preceding fifty ycars, decade by decade,
without mentioning popular music, except insofar as it was in some
way preparatory for the Beatles (Elvis Presley, for example). Their
impact, as cultural wedges, was enormous. The climate of opinion
was transformed.

The Collapse of Traditional Rationalism

Someone who was not on a university campus throughout the
1960's can scarcely imagine the extent of the change in outlook, espe-
cially among students and the younger faculty members. The Viet-
nam war helped to crystalize the new opposition to traditional politi-
cal liberalism, but far more was involved than politics. The whole
university structure was under fire: administration, tenure, the
methodology of the academic disciplines, dormitory life, dating
standards, everything. Anyone who had graduated in 1963 or cven
1964 would hardly have recognized his alma mater in 1967.

A pair of articles in The Saturday Evening Post, itself a casualty of
the 1960's, illustrates the nature of the change. The first one, pub-
lished in May of 1964, announced the following facts concerning the
University of California, Berkeley, America’s most prestigious state-
supported university (second, then as now, only to Harvard in the
evaluations of the nation’s academic establishment): “The larger
campuses, Berkeley especially, are smug in their conviction that they
already provide the best university education possible. They believe
no important reforms are necessary.” The President of the University
of California, Clark Kerr, the man who coined that monstrous and

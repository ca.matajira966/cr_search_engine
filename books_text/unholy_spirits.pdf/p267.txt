Demonte Healing 261

can seemingly work in an offensive fashion as well as defensively.

All things are fair in love and war, the slogan says. In epistemo-
logical confrontations, the rules of war prevail. While the psychic
healers would sometimes do a bit of fakery when the flesh was willing
but the spirit was weak, their rationalistic opponents were also not
above a little sleight-of-hand. Valentine reports on an experiment he
conducted. “With the help of a Filipino-American medical doctor
who was in Baguio when we were, I carried out an informal placebo
test on a clinic. Two genuine gallstones removed from a living pa-
tient were sent to a laboratory, but the clinic was informed that the
stones had been removed by psychic surgery. The report came back
without detailed analysis, but it carried a flat statement that the
stones were not organic.”* This, as might be suspected, was the in-
variable answer whenever researchers sent in products of admittedly
psychic operations. The specimen might be animal, vegetable, or
mineral, but it was never human.

Physicians predictably claim that the only possible healings re-
sulting from psychic surgery are psychosomatic illnesses. (“Take that
knife out of his eye, and I'll prove it!”} Again, there are indications of
just this kind of mental healing, but consider the case of Joseph
Ruffer. Ruffner had suffered an industrial accident in 1956. He had
been put on crutches permanently. In 1966 he visited Agpaoa. Here
is his description of what took place in 1966: “I was scared, but I
didn’t feel any pain. I saw him cut into me with his bare hands and
dig something out. I saw it open, I saw it close, and I saw blood. A
guy who said he was a doctor asked me if he could put his hand into
my wound, I said it was okay by me if it was clean. He put his hand
in [what—no washing with soap and water for 20 minutes?], and I
could feel it, When he drew it out, it was covered with blood. Then
Tony took his hands out, and the wound was instantly healed. He
said to me, ‘Get up and walk.’ I didn’t think I could—you know,
after surgery and all—but I did.”®

He never used crutches after that. Yet upon being X-rayed when
he returned to the United States, he learned that his bones were still
broken. He should not have been able to walk. He could walk,
though, and without any pain.® If this is psychosomatic healing, it is
a very peculiar kind.

64. Valentine, Pyychic Surgery, p. 160.
65. /bid., p. Sl.
66. fbid., p. 52.

Demonic Healing 269

accompanied by three to five collection offerings. (Rev. Gerald
Latal, an Orthodox Presbyterian Church minister, was asked in the
1940’s to co-sponsor the appearance of a subscquently well-known
but now-deceased healing lady. It was at the beginning of her career,
He was skeptical, but he and his elders met with her. She wanted her
organization to handle all the mechanics, including the promotion.
He insisted that if she appeared, any sponsoring churches had to be
given credit as co-sponsors, “All right! she replied to him. “But I'll take
the collections.” Rev. Latal wisely decided not to have anything to do
with the project. The lady kept taking those collections for 25 years.)

Koch makes some telling comments on the ministry of Oral Rob-
erts, who established himself as a faith healer. Today, Roberts’ televi-
sion programs are modeled along the lines of the more conventional
evangelists: a weekly “intimate” get together, with a small studio au-
dience, plus clean-cut “up-beat” singers. He has abandoned the mass
healings; he just bows his head and prays that God will heal people
in the audience. This is valid cnough. But in the early 1950's, before
he had joined the Methodist Church, before he Kad built a college
and a heavily recruited basketball team of NCAA tournament cal-
iber, he was on the sawdust trail. He did not have one-hour travelog
specials on Alaska in full color; he had live broadcasts from his tent.
The walking wounded hobbled up to be healed, and he laid hands
on them all—or at least those who were screened to come up onto the
stage. Koch comments on Roberts’ appearance at the World Con-
gress of Evangelism held in Berlin in 1966. Roberts was the leader of
a subcommittee.

There were about 300 delegates present including the Rev. Pagel, the
evangelist Leo Janz and myself. Roberts had been speaking on the subject
of healing when one of the Americans present asked him, “Mr, Roberts,
isn't it true that during your tclevision programs you have sometimes asked
the viewers to place a glass of water on the television during the actual
broadcast?” After receiving an affirmative answer, his questioner went on,
“And isn't it also true that at the end of the programs you have told the view-
ers to drink the water if they are seeking healing?” Again, in the presence of
the 300 or so delegates, Oral Roberts replied, “Yes.” That was honest of him,
But what type of healing is this? Occasionally during similar programs one
of the viewers has been asked to place his hand on the television set and
with his free hand to either touch or to form a chain with other viewers pres-
ent. But this is the sort of practice one finds in connection with spiritistic
table-lifting, when chains are formed in order to encourage the flow of med-

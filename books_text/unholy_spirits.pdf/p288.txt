282 UNHOLY SPIRITS

foreign aid, that is, government-to-government aid, Schoeck does
not exaggerate when he writes, “As a system of social control, Black
Magic is of tremendous importance, because it governs all interper-
sonal relationships.”

Power, Cause, and Effect

“The fundamental aim of all magic is to impose the human will
on nature, on man or on the supersensual world in order to master
them.” With these words, E. M. Butler introduces her book, Ritual
Magic (1949). The goals, however, are rigorously materialistic: fame,
power, fortune, success. Anton Szandor LaVey, the self-ordained
(1966) minister of the Church of Satan in San Francisco, is quite
open about the goals of Satanism: “Anyone who pretends to be inter-
ested in magic or the occult for reasons other than gaining personal
power is the worst kind of hypocrite.” Further, says LaVey:

White magic is supposedly utilized only for good or unselfish purposes,
and black magic, we are told, is used only for selfish or “evil” reasons,
Satanism draws no such dividing line. Magic is magic, be it used to help or
hinder. ‘The Satanist, being the modern magician, should have the ability
to decide what is just, and then apply the powers of magic to attain his
goals.

During white magic ceremonies, the practitioners stand within a penta-
gram to protect themselves from the “evil” forces which they call upon for
help. To the Satanist, it seems a bit two-faced to call on these forces for
help, while at the same time protecting yourself from the very powers you
have asked for assistance. The Satanist realizes that only by putting himself
in league with these forces can he fully and unhypocritically utilize the Pow-
ers of Darkness to his best advantage.!®

In short, concludes LaVey, “Satanism is not a white light relig-
ion; it is a religion of the flesh, the mundane, the carnal—all of
which are ruled by Satan, the personification of the Left Hand
Path.?16

The Left Hand Path: here is one of the familiar slogans of the oc-
cult underground. The Right Hand Path is the path of mystical il-
lumination, higher consciousness, white magic, and philosophical
monism. Members of these seemingly innocuous groups constantly

If. Anton Szandor LaVey, The Satanic Bible (New York; Avon, 1972), p. 51.
15. dbid., pp. 51-52.
16. dbid., p. 52.

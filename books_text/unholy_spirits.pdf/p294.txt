9
INVADERS FROM ...?

If I become President, T'l make every piece of information this country
has about UFO sightings available to the public, and the scientists.
Lam convinced that UFOs exist because I have seen one.
Candidate Jimmy Carter

This was Jimmy Carter, the honest man from Plains, Georgia,
the man who promised American voters, “I'll never lie to you.”
Would he have told this story in the middle of the race for the Presi-
dency unless he was telling what he believed to be the truth? In 1973,
he and 20 other people saw a large, bright light in the sky which kept
changing colors. They watched it for ten minutes. Once honest Jimmy
was elected, of course, he reneged on this promise, among many,
many others.! He never mentioned the topic again, as far as I ever
saw —and I watched. But the reporter for the supermarket tabloid
National Enquirer who investigated the story found out that others
were with Carter when he saw the UFO, and they too had seen it.
Several others said that Carter had told them the story.? By the
mid-197(0's, a flying saucer was still relatively odd news, but it cer-
tainly did not cost Carter many votes. Flying saucers were no longer
exclusively “cracked” saucers.

In the December 17, 1966 issue of The Saturday Evening Post,
Northwestern University astronomy professor J. Allen Hynek began
his feature article with this report: “On August 25, 1966, an Air
Force officer in charge of a missile crew in North Dakota suddenly
found that his radio transmission was being interrupted by static. At

1. A classic litle paperback book has now become a collector's item, and in retro-
spect, a barrel of laughs: Robert W. Turner's ‘Ti! Never Lie to You": jimmy Carter in his
Own Words, published by Ballantine Books in 1976, during the Presidential campaign.

2. National Enguiver (June 8, 1976), headline story: “Jimmy Carter: The Night 1
Saw a UFO, by Malcolm Balfour.

288

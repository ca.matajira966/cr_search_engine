292 UNHOLY SPIRITS

the future. The myth of evolution has many forms. It is perhaps the
oldest sustained myth which has rivaled biblical orthodoxy from the
beginning. When we find that people claim that they have been vis-
ited by creatures from outer space—creatures that have advanced
technologically, psychologically, and ethically far beyond mere man-
kind — we are alerted to one more example of this ancient rival faith,
Man the creature of the future, man the technologist of the future,
man the creature who is capable of achieving vast attainments in the
future if only he listens to the warnings and messages of creatures
from outer space. This theme is repeated so often in the literature of
flying saucers that it must be dealt with in this book.

Another continuing theme is the idea that man has failed to ad-
vance morally as rapidly as he has advanced technologically. This
idea is so familiar that i¢ has become commonplace, But the UFO
version offers this variation: outer space creatures who have been
able to advance morally to keep pace with their technological inno-
vations will soon reveal to mankind (or elite representatives of man-
kind) the moral secrets that have enabled them to avoid nuclear de-
struction. Thus, the promise of a more advanced technology (which
they supposedly possess) is accompanied by the promise of a more
advanced morality (which they seemingly must possess, since they
have not blown themselves to smithereens). Mankind is therefore
about to be given, free of charge, the secrets of the ages — not just an-
cient mysteries, but the secrets “of a galaxy far, far away in a time
long, long ago,” to paraphrase the introductory scroll of “Star Wars.”

A third theme is that some great catastrophe is fast approaching.
Unless people on earth soon repent, and adopt the higher moral
ways of the outer space invaders— who represent the forces of the
cosmos — there will be a period of horrors: bad weather, wars, and so
forth. But the contactee is assured that he has been selected for this
task of communicating this message to his fellow man. In case after
case, there is some version of the old radio warning, “please stay tuned.”

Thus, the appeal of UFO phenomena is similar to the appeal of
ancient mysteries. The believer joins the “cutting edge” of the next
phase of evolution. He joins the ranks of the “true believers” whose
wisdom, perception, and open minds have enabled them to perceive
what average people will not admit is possible. The initiate (especially
those who are not trained scientists, whose interest is more strictly
scientific) is promised to be on the “inside track” in a new evolution-
ary leap of being. He will be able to command the high ground mor-

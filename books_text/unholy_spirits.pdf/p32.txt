26 UNHOLY SPIRITS

the overall thesis of this book: that the rise of New Age humanism
and the revival of occultism since 1965 really does constitute a break
with over three centuries of Western Civilization. This break repre-
sents a threat to the continued existence of Western culture. It is a
battle for the minds and souls of men. Anyone who does not under-
stand why this challenge is significant, and also what preceded it,
will not be able to deal successfully with the challenge. (Never-
theless, you can still understand most of this book even if you skip
the remainder of this chapter. If you get too confused or bogged
down, just go on to chapter two.)

There are certain features of humanist philosophy that have
made our atheistic rationalist culture especially vulnerable to oc-
cultism. This vulnerability was not apparent to many in 1963, but it
has grown more obvious since then. We need to answer this ques-
tion: What was it in Western thought and culture that weakened the West's
resistance to the occult revival? Occultism always simmers under the sur-
face of culture. The war between good and evil, God and Satan,
always goes on until the day of judgment. But why an occult revival
now? Why the rise of occultism in the final third of the twentieth cen-
tury? Why not before? Why not later?

To answer this, we need to know about a series of crises and con-
tradictions in the history of Western philosophy. Because of the
breakdown in the self-confidence of humanist rationalists, we are
witnessing the revival of occultism. We need to know what these
weaknesses were, and why Christianity offers a valid alternative that
will not capitulate to the New Age magicians.

Reformation and Renaissance

The Reformation of the early sixteenth century and the Renais-
sance of the fifteenth century through the sixteenth century paral-
leled each other in certain areas, but diverged in two fundamental
respects: the attitude toward the Christian religion, and the attitude to-
ward time. This is not the place to go into great detail, but on the
whole, we can accurately summarize the differences between the two
rival civilizations as the difference between the idea of a world gov-
erned by God (a providential universe) and one governed by fate or
chance or political power (an impersonal universe), There were cer-
tain elements of magic and the occult in the Reformation, but these
steadily were abandoned;!” not so in the Renaissance, which was a

17. Keith Thomas, Religion and the Decline of Magic (New York: Scribner's, 1971).

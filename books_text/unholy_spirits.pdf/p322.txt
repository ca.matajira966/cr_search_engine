316 UNHOLY SPIRITS

technology of some kind of ‘spacemen.’ "67

Vallee argues that the first aspect of the UFO phenomena is phy-
sical, that the UFO does appear to be a true object operating in the
atmosphere. The second aspect is psychological, People perceive
some sort of space vehicle or flying object. The third aspect, he says,
is social. “Belief in the reality of UFOs is spreading rapidly at all lev-
els of society throughout the world.” He then asks a question. It is a
lengthy question but highly significant. “The experience of a close
encounter with a UFO is a shattering physical and mental ordeal.
The trauma has effects that go far beyond what the witnesses recall
consciously. New types of behavior are conditioned, and new types
of beliefs are promoted. The social, political, and religious conse-
quences of the experience are enormous if they are considered, not
only in the days or weeks following the sighting, but over the time
span of a generation. Could it be that such effects are actually in-
tended, through some process of social conditioning? Could it be
that both the believers and the skeptics are being manipulated? Is
the public being deceived and led to false conclusions by someone who
is using UFO witnesses to propagate revolutionary new ideas?”®

Vallee understands both the skeptics and the true believers. He
points out that the professional skeptics, especially within the scienti-
fic guild, ridicule anyone who claims to have seen a flying saucer or
who claims to have made contact with someone inside a saucer. On
the other hand, the followers of many of these UFO cults are often
persons who have lost faith in science and technology generally, They
may believe in the scientific and technological abilities of the beings
who inhabit the space vehicles, but they no longer trust the scientists
and technologists who inhabit the halls of Harvard University.

Here is Vallee’s conclusion, though stated early in the book: “I be-
lieve there is a machinery of mass manipulation behind the UFO phenomenon.
It aims at social and political goals by diverting attention from some
human problems and by providing a potential release for tensions
caused by others. The contactees are a part of that machinery. They
are helping to create a new form of belief: an expectation of actual contact
among large parts of the public. In turn this expectation makes mil-
lions of people hope for the imminent realization of that age-old
dream: salvation from above, surrender to the greater power of some

67. Ibid., p. 7.
68. Ibid., p. 8.
69. Idem.

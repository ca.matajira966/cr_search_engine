326 UNHOLY SPIRITS

higher level of technology, namely, the technology of psychological
control, the technology of shifting men’s consciousness. “We are
faced with a technology that transcends the physical and is capable of
manipulating our reality, generating a variety of altered states of
consciousness and of emotional perceptions. The purpose of that
technology may be to change our concepts of the universe.” They
have fooled Vallee, too. Technology is their illusion; the reality is
their theology: “Ye shalt be as God.”

The conventional scientists are in a jam—a jam created by the
collapse of the Newtonian world view which they themselves have
contributed to (quantum mechanics, light waves that are sometimes
particles, etc.). Vallee asks rhetorically, “Why were the scientists re-
maining silent?” He criticizes them, quite correctly: “. . . by deny-
ing the existence of the mystery, the scientific community was taking
serious chances with the belief system of the public. In my opinion,
such attitudes have contributed to the lorig-term loss of popular sup-
port and popular respect for science.” But if they had seriously pur-
sued the UFO “phenomena,” they would have discovered a chink in
the intellectual defensive armor against the invaders from the nou-
menal, invaders from hell. In either case, the public will lose respect
for the autonomous science of humanistic man, They are damned
methodologically if they seriously pursue the manifestations of the
occult, and damned publicly if they don’t. This is the fate of all au-
tonomous thought: damnation. The same applies to political officials
and other “agents of suppression.” They simply cannot win, no mat-
ter what happens, unless the public manifestations of the occult
should go away. In an age of transition in which millions have de-
parted from traditional Christian faith, this reduction in public oc-
cultism is unlikely.

J agree entirely with Vallee, who italicizes these ideas: ‘7 think
there is a general shifting of man’s belief patterns, his entire relationship to the
concept of the invisible, It is happening outside of any established structure, and
setence is not immune to it.” 1 also agree with C. S. Lewis’ prophetic
demon, Screwtape: “Our policy, for the moment, is to conceal our-
selves. Of course this has not always been so. We are really faced
with a cruel dilemma. When the humans disbelieve in our existence
we lose all the pleasing results of direct terrorism, and we make no

93. Ibid., pp. 153-54.
94, Ibid., p. 47.
95. Tbid., p. 114.

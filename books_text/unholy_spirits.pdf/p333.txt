Invaders from... ? 327

magicians. On the other hand, when they believe in us, we cannot
make them materialists and sceptics. At least, not yet. I have great
hopes that we shall learn in due time how to emotionalise and myth-
ologise their science to such an extent that what is, in effect, a belief
in us (though not under that name) will creep in while the human
mind remains closed to belief in the Enemy. The ‘Life Force, the
worship of sex, and some aspects of Psychoanalysis may here prove
useful. If once we can produce our perfect work —the Materialist
Magician, the man, not using, but veritably worshipping, what he
vaguely calls ‘Forces’ while denying the existence of ‘spirits’ then
the end of the war will be in sight."%*

Conclusion

Jacques Vallee in 1975 predicted that industria] nations would
soon have to put their best scientists on a program to study UFO's.”
They haven't. He predicted that the next major religious revival
could easily be centered around flying saucers.™ It has yet to hap-
pen. The invisible college of UFO-oriented scientists is still as invisi-
ble as ever. If anything, interest in UFO's has declined. Perhaps too
many people already believe. The shock value of UFO sightings is
over, Most important, the visitors have always lied. They keep
promising to show up. They do, too: in Kansas farm communities at
3.a.m. “Close Encounters of a Third Kind” became a Hollywood pro-
duction, So did *E.T.” Lots of fun, but not a new religion. Not yet.

Will the public’s faith in UFO's continue? Probably. Will this
make much difference? Probably not. Flying saucers are just one
more manifestation of the ever-present religion of humanism: evolu-
tionary, self-salvational, and. gnostic. Flying saucerism’s theology is
not that unique. Its pseudo-technology is impressive to those who
see it, but not many do see it up close, and in any case, major relig-
ious movements are not based on technology — influenced by it, yes,
but not based on it. They are based on specific presuppositional views
of God, man, law, and time. They are also based on a view, pro or
con, of final judgment.

Men find many reasons to believe that they can escape the threat
of final judgment. The doctrine of evolution is a major means of

96. C. S. Lewis, The Screwtape Letters and Screwtape Proposes a Toast (New York:
Macmillan, [1961] 1969), Letter VH, pp. 32-33.

97. Invisible College, p. 200.

98. Ibid., pp. 202-6.

336 UNHOLY SPIRITS

Alchemy

Alchemists believed in such a self-transcendence. Alchemy was
not simply limited to medieval Europe, but was a phenomenon also
common to India, Ghina, and Islamic cultures. It produced a huge
and virtually unreadable body of esoteric literature. There is even a
scholarly journal, Améix, devoted to alchemy’s history. The mental
image of the alchemist in the minds of those who have even heard of
it is that of a lonely investigator in his secret laboratory, painstakingly
searching for the chemical formula which will allow him to turn lead
into gold, Textbooks in European history may mention alchemy
briefly as the forerunner to modern scientific chemistry. The text-
book formula runs something like the following: take medieval
alchemy, add to it the principles of Cartesian logic and mathematics,
sprinkle in a bottle of Enlightenment philosophy, and shake gently
for three centuries: out pops modern chemistry.

The textbook account is misleading, It was the Reformation, not
the Enlightenment, that produced modern science.!? More impor-
tantly, it was not the methodology of alchemy which made scientific
investigation possible, but rather alchemy’s opposite. However sec-
retive scientists may be prior to publishing their findings, the meth-
odology of science is based upon open knowledge, publication, re-
peatable experiments, the international division of intellectual labor,
and the concept of regular law. In contrast, alchemy was above all
the knowledge of the secret initiates, and its goal was secret, esoteric
knowledge. It was the science of gnosticism.™ Its technique was
based on the idea that in the endless mixing of the same ingredients
—chemical opposites—the chemicals would somehow transcend
themselves after a hundred or a thousand identical operations. No
one could know in advance when or how this transformation would
take place. No one attempting to repeat this process could be assured
of success. The discovery of the so-called Philosopher's Stone which
would allow the alchemist to turn lead into gold was the product of
the alchemis(’s very soul. The crucial fact to bear in mind is this: al-
chemy was fundamentally a religious procedure. Eliade, summariz-
ing the findings of the psychologist C. G. Jung, writes: “The aim of

13. Robert K. Merton, Sscial Theory and Social Structure (New York: Free Press,
1969), ch. 18.

14. H. J. Shepard, “Guosticisn and Alchemy,” Ambix, VI (1958), pp. 140-48;
Shepard, “he Redemption ‘Theme in Hellenistic Alchemy” Ambix, VIT (1959), pp.
42-76.

354 UNHOLY SPIRITS

the stylus of the stereo does not ‘cause’ the music filling our room. To
the extent that hallucinogens are effective, they act as catalysts re-
leasing latent images stored in the mind.”5! Huxley hopes for built-in
restraints from chemistry. He cannot have them. All he can hope for
is that other users of such drugs, even the “perfect drug,” will have
pleasant, uplifting, and even educational escapes from reality, It will
be like a day in the countryside, assuming the users like and appreci-
ate the countryside. But what if the users like and appreciate mass
murder. What if the drug users are the members of Charles
Manson’s family? Who will serve as the straight man in a society of
Mansons?

Cosmic Consciousness

The battle over consciousness is the supreme human battle. Karl
Mars called for class consciousness. Women’s Liberation groups call
for consciousness raising. R. M. Bucke, in a mediocre turn-of-the-
century book which has somehow become a kind of classic, calls for
Cosmic Consctousness (1901). The Western occultists call for Christ con-
sciousness. The doors of perception are governed by first principles
of interpretation, and each religious or ideological group wants its
first principles sovereign in regulating other men’s conscious (or un-
conscious) perception.

“The Saviour of man is Cosmic Consciousness — in Paul's langu-
age—the Christ.”>? So writes R. M. Bucke, who has assembled se-
lections from the more famous historic figures who believed in cos-
mic consciousness (or who Bucke thinks so believed), His is another
version of monism, as are most of the systems of cosmic conscious-
ness. Like so many late-nineteenth-century illuminists, Bucke be-
lieved fervently in evolutionary progress, socialism, and mystical il-
lumination.5° The goal of cosmic consciousness is self-deification:
“Finally the basic fact in cosmic consciousness is implied in its name
—that fact is consciousness of the cosmos —this is what is called in
the East the ‘Brahmic Splendor,’ which is in Dante’s phrase capable
of transhumanizing a man into a god.”5* Clearly, Bucke’s own brief
experience of cosmic consciousness did nothing to improve his sen-
tence structure. He quotes Walt Whitman: “Divine am I, inside and

51. Idem.

52, R. M. Bucke, Cosmic Consciousness (New York: Dover, [1901] 1969), p. 6.
93, Ibid., pp. 4, 22.

54, Ibid., p. 1%

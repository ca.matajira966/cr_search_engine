356 UNHOLY SPIRITS

in which the direct confrontation with structure and meaning are
clear-cut. The observer may train himself to say “chaos” when he ex-
amines his surroundings, but no one, other than a lunatic, can con-
vince himself of this for lengthy periods of time. Reality and time are
burdens, for they are cursed. Those who are in ethical rebellion feel
the weight of reality, and they seek to flee into a more chaotic realm.
In chaos and meaningless they believe they can escape God, escape
their own creaturehood, and escape final judgment. They are
wrong, but this is their hope.

Another Dead End

Not all Eastern thinkers are committed to the cultivation of states
of altered consciousness. One contemporary Zen Buddhist master
has written: “An ancient Zen saying has it that to become attached to
one’s own enlightenment is as much a sickness as to exhibit a mad-
deningly active ego. Indeed, the profounder the enlightenment, the
worse the illness.... My own sickness lasted ten years."
Krishnamurti, a modern Indian religious teacher, has said much the
same thing: “Meditation is not the mere experiencing of something
beyond everyday thought and feeling nor is it the pursuit of visions
and delights. An immature and squalid little mind can and does
have visions of expanding consciousness, and experiences which it
recognizes according to its own conditioning. This immaturity may
be greatly capable of making itself successful in this world and
achieving fame and notoriety, The gurus whom it follows are of the
same quality and state. Meditation does not belong to such as
these,”

One of the significant aspects of Aldous Huxley’s life was that he
finally abandoned hope in mystical “higher” consciousness at the
very end of his life. A weck before he died, at a time when he be-
lieved that LSD could put a man in touch permanently with the
Tibetan Buddhists’ “Clear Light of the Void,” he realized, as
Zaehner puts it, “that this was probably a gross misrepresentation of
what was actually happening.” While he was under the influence of a
sedative, his wife made a tape recording of his discussion. “he
whole thing has been very strange because in a way it was very good
—but in a way it was absolutely terrifying, showing that when one

59. R. C. Zaener, Zen, Drugs and Mysticism (New York: Pantheon, 1972), p. 98.
60. fbid., p. 115.

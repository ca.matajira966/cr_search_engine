Escape from Creaturehood 357

thinks one’s got beyond oneself, one hasn't. . . . [ began with this
marvelous sense of this cosmic gift, and then ended up with a rueful
sense that one can be deceived. . . . It was an insight, but at the
same time the most dangerous of errors . . . inasmuch as one was
worshipping oneself”®! (author's italics omitted). There would be no
salvation through soma drugs in some brave new world, no final crea-
tion of an electric Tibet.

Despite these protests, those who have promoted Eastern techni-
ques of mystic illumination have unquestionably gained followers on
the basis of a promised entry into “higher” consciousness. Western
men want results, they want action, they want bright lights and
communion with God. Furthermore, they want these things in no
more than three easy lessons, Maturity is not what most of them are
after, especially Eastern maturity. Why import a foreign maturity
when Christianity at least delivers the compound growth rates? If
Zen Buddhism requires hostility to all reality, including altered con-
sciousness, then it is easier to flirt with a less consistent form of rela-
tivism and world-denial.

Elitism os. Common Faith

A religion like Zen Buddhism is totally elitist. {t is not simply
that only a few people ever get involved. If that were all there were to
it, Americans searching for avant-garde religious commitments
would have far less trouble. The intensity of Zen Buddhism’s train-
ing in total irrationalism necessarily limits its members. Abbot
Zenkei Shibayama, a modern Zen master, has admitted this: “It may
not be difficult to talk about the experience of awakening to ‘Self-
nature’ or “True Self’ which we have deep at the bottom of our per-
sonalities, but to come to this realization experientially as the fact of
one’s own actual experience, is not easy at all. It is so very difficult
that it cannot be easily attained by ordinary people.” In short, “not
everyone can be expected to have the training required for the at-
tainment of the exquisite moment of sator1.”©

In contrast to these elitist religions, so obviously individualistic
as far as the promised blessings are concerned, Christianity has
always been a religion of the average man. We must become as little
children, Christ said, to enter the kingdom of God (Matt. 18:3). The

61. Ibid., p. 108.
62. Ibid., p. Bl.
63. Ibid., p. 116.

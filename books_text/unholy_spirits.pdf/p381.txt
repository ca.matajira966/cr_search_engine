Escape from Creaturehood 375

families, churches, and civil governments. This represents a major
break with the traditional pessimistic eschatology of fundamental-
ism, called dispensationalism. These charismatic leaders have not
self-consciously made the break from premillennialism to postmil-
lennial optimism, but the term “dominion” implies it. Again, the re-
constructionists are the only Protestant theologians to have forth-
rightly preached postmillennialism after 1965. (R. J. Rushdoony
was the pioneer here.)®3 Thus, the ideas of the reconstructionists
have penetrated into Protestant circles that for the most part are un-
aware of the original source of the theological ideas that are begin-
ning to transform them.

The Christian reconstructionists are still on the fringe. Indeed,
they are on the fringe of a fringe (politically self-conscious conser-
vative church members). Though it is difficult to trace their influ-
ence, their ideas are spreading. They are the ones who have blue-
prints. They are the ones who have based their case on the Bible,
which orthodox Christians claim to honor. They have available to
them a broad base of tens of millions of registered voters in the
United States who are confessed Christians.

The New Agers have a toehold in the public schools—a fading
institution — and also in certain areas of the media. Occultism is un-
questionably spreading. But they are facing an unprecedented politi-
cal and cultural dacklash of conservative people who are darkly toler-
ant of today’s humanism, and who have been seriously compromised
by it, but who will react negatively and rapidly, once the economic
and political order built by the humanists begins to crumble. The
New Agers, like the Christian reconstructionists, are predicting just
such a crumbling. The New Agers believe that they will permanently
“pick up the pieces.” Some of those fundamentalist Christians who
hold to a pessimistic eschatology also think the New Agers will pick
up the pieces, at least until Jesus returns physically to establish His
millennial kingdom. This prediction is not going to come true.
Christians eventually are going to pick up the pieces, before Jesus
returns physically, just as they picked up the pieces of the Roman
Empire, when that “eternal” institution crumbled.

The top-down vision of the traditional power religionists is fad-

&3. R. J. Rushdoony, Thy Kingdom Come: Studies in Daniet and Revelation (Fairfax,
Virginia: Thoburn Press, [1971] 1978); cf. Chilton, Paradise Restored.

84. For example, the ten-volume “Biblical Blueprints” series published by Do-
minion Press‘in 1986.

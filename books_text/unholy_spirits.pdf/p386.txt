380 UNHOLY SPIRITS

mistakenly joined the ranks of “the escapist religion,”’ thinking that
it is the only alternative to the power religion. The biblical alter-
native is not the escapist religion; it is the dominion religion. It in-
volves optimism concerning the future, faith in the infallibility of the
Bible, recognition that God’s required tool of dominion is biblical
law, and that God has ordained the victory of His people.”

Communists have adopted a secular version of this four-part out-
look: confidence in their religion, scientific Marxism; confidence in
the future (the victory of the proletariat); confidence in socialist law;
and confidence in historic inevitability. Revolutionary Moslems
have an analogous faith. So did late-nineteenth-century science.

This four-part theology has been absent from Western Christian-
ity for over a century. Few denominations, even Bible-believing
ones, profess more than one of these points. No denomination pro-
fesses all four. Yet without all four, the ability of Christians to chal-
lenge the humanist world is drastically weakened.® It has only been
since 1980 that a growing number of charismatic (“tongues-speaking”)
ministries have at least unofficially adopted three of the four: the
self-attesting authority of the Bible (anti-“natural law”), the reliabil-
ity of “biblical principles” (they are still afraid to say “Old Testament
biblical law”), and optimism concerning the future, “dominion” (they
are not yet ready to call themselves postmillennialists).

Eschatology

This vision of eschatological victory has been called historic postmil-
Jennialism. There is a major revival of this eschatological viewpoint
taking place in the United States, especially in several charismatic
Pentecostal circles. It is not being widely publicized, but hostile
critics from the dispensational camp have recognized it.

It is necessary here to survey briefly the four major eschatological
positions. (“Eschatology” = the doctrine of last things.) They are:

5. Ibid., Introduction.

6. A good account of this retreat is found in George Marsden’s book, Fundamen-
talism and American Culture: The Shaping of Twentieth-Century Evangelicalism, 1870-1925
(New York: Oxford University Press, 1980).

7. Gary North, Unconditional Surrender: God's Program for Victory (3rd ed.; Tyler,
Texas: Institute for Christian Economics, 1988).

8. Gary North and David Chilton, “Apologetics and Strategy,” Christianity and
Civilization, 3 (1983).

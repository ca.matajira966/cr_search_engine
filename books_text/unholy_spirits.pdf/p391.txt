fs This the End of the World? 385

1. The Christian-led battle against abortion.

2. The sacrificial giving of Christians to create an alter-
native to the public schools with Christian day schools.

3. The return of Christians to the political arena after half
a century of antinomian slumber.

4. The coming of the Christian television networks and
popular Christian programs such as “Che 700 Club.”

5. The coming of decentralized publishing technology
which is making possible a new Reformation by means
of a “technological end-run” around the humanist Estab-
lishment’s media monopoly.

6. The battle against local sales of pornography.

These are “prophetic” activities, just as surely as passing out
tracts on street corners is prophetic. Furthermore, they involve a lot
more Christian maturity, a lot more planning, better communica-
tions skills, and more money than passing out tracts on street cor-
ners. Is Rev. Wilkerson unimpressed by such “worldly” pursuits?
Are they useless? Does God dismiss them automatically as “too little,
too late”? Rev, Wilkerson certainly does not call his readers to get in-
volved in any of these actions. He does not say that he is getting in-
volved in such activities. What, then, is he doing about the sins of
our day? “I intend to stay on the streets, preaching to junkies and
harlots.”5

He proved his commitment in this regard in 1980 by selling his
ministry's magnificent ranch and buildings in Lindale, Texas, con-
servatively appraised at $4 million at the time, to Youth With A Mis-
sion, for ten cents on the dollar. (The real estate agent who had origi-
nally intended to handle the transaction told me that his normal
commission would have been as much as Rev. Wilkerson got for the
entire property.) This East Texas property is only one of many that
YWAM operates throughout the world.

From 1960 until 1985, Youth With A Mission grew from nothing
to become the largest Protestant missionary organization in the
world, with some 5,000 full-time missionaries in the field, Its
charismatic founders are holding the organization’s 25th anniversary
at the very time I am writing this chapter, planning for the next 25
years of missionary activity, in which they hope to grow to tens of

15. Wilkerson, Set che Trumpet, p. 28.

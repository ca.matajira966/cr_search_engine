Index

Rachmaninov, Sergei, 93
radar, 289, 300
radiation, 105
radio, 165-68
Randi, James, 116n, 229
randomness
cat and mice, 117
ESP, 117-18
methodology, 175
power, 85
progress &, 258
routines vs., 137
science, 76, 117, 178
spreading, 43
UFO's, 301-2
see also chaos, indeterminacy,
irrationalism
Rapture, 38, 372n, 379, 381, 382,
390, 394
Rasputin, 236
RaTa, 211-12
rationalism
Castaneda vs., 128, 146-50
confidence, 255
evangelism, 249
irrationalism &, 39-41, 45, 104, 144
materialism, 258
post-Kant, 243
public’s abandonment of, 100
revelation &, 379
shield of, 155-56, 260
sorcery vs., 145
“treaty,” 3f, 39-41, 45
rats, 100, 241
razor blade, 95-96, 97
Read, Leonard, 2
Readers Digest, 243, 228
reality

 

conflicting views, 119
defining, 17, 263

drugs, 129

man-made 36
nonordinary, 129, 137-39
paradigm, 366
perception of, 263, 351-52
realism, 33

relative, 144

424

subduing (magic), 130
reason, see rationalism
rebellion, 63
re-enchantment, 321
Reformation, 13, 26, 247
reincarnation
Buddhism, 344
Caldwell, Taylor, 202
Cayce's readings, 202-3, 208-20
counter-culture, 278
Dixon, Jeane, 188-89,
final judgment vs. 64
hypnosis, 93
occult theme, 60
spiritism, 253-54
relativism
campus, 24
evolution, 224
Heisenberg, 43, 76, 121
moral, 283-B4
physics, 43, 76, 144-45
sorcery, 144, 154
Renaissance, 4, 5, 26, 27, 82, 130, 162,
211-12, 306
repeatability, 89, 95, 99, 103n, 105,
108, iit, 114, 123
resistance, 12, 26, 64
responsibility, 60, 351-2
Reston, James, 240
restoration, 62, 373, 376, 383
resurrection, 390
Retallack, Dorothy, 103
retreat, 374
Revel, J. F, it
“Rev. Ike,” 172
revolution
intellectual, 17, 366
Marxian, 62
scientific, 24-24, 122-23, 242-43,
247
Rhine, J. B., 111, 119-20, 161
rhythm, 83-84
Ricks, J. Bernard, 250-51
Rifkin, Jeremy, 366
ritual, 134-37, 142, 195, 158, 270,
276, 285
ritual evil, 155

The Crisis of Western Rationalism 37

How do men make moral decisions in this world? If the a priori
categories of logic are universal, then in what does our moral free-
dom consist? Furthermore, how do men know for certain that the
“stuif out there” is not personal, active, and independently powerful?
God, “things in themselves,” morality, the transcendental human
ego, freedom, and contingency are assigned by Kant to an impotent
and logically unknowable realm of noumenal reality. The phenomenal
realm is the only one we can ever know logically — the world of inter-
preted sense data. In short, the noumenal realm of freedom and
moral choice, as well as the world of God, demons, angels, and “stuff
out there,” is nothing more than a hypothetical limiting concept, that is,
an intellectual device to avoid answering the fundamental questions
that autonomous human reason has found itself unable to answer.

C. S. Lewis’ Warning

Men live by ideas, and no idea in man’s history produced more
evil than this one: the sovereignty of man. It is humanism’s chief presup-
position. Man, the god. Man, the predestinator. Man, the central
planner. Man, the director of the evolutionary process. Man, the
maker and shaker of things on earth and in the heavens. As Karl
Marx’s partner, Frederick Engels, put it over a century ago, “man no
longer merely proposes, but also disposes.” But most important of
all is this promise: Man, the savior of Man.

This vision is inescapably religious. The impulse lying behind it
is religious. Some have called it the religion of secular humanism.
Others have called it the will to power (Nietzsche). But no one has
described its implications better than C. 8. Lewis. “What we call
man’s power is, in reality, a power possessed by some men which
they may, or may not, allow other men to profit by. . . . From this
point of view, what we call Man’s power over Nature turns out to be
a power exercised by some men over other men with Nature as its in-
strument.... Man’s conquest of Nature, if the dreams of some
scientific planners are realized, means the rule of a few hundreds of
men over billions upon billions of men, Each new power won by man
is a power over man as well. Each advance leaves him weaker as well
as stronger. In every victory, besides being the general who triumphs,
he is also the prisoner who follows the triumphal car. . . . For the
power of Man to make himself what he pleases means, as we have

38, Frederick Engels, Herr Eugen Diihring’s Revolution in Science [4 nti-Dihring] (Lon-
don: Lawrence & Wishart, [1877-78] 1934), p. 348.

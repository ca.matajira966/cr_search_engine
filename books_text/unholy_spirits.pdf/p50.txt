44 UNHOLY SPIRITS

been found to be a massive delusion.

The breakdown of the secret treaty between rationalism and irra-
tionalism has also brought a hole in the philosophical defenses
against occultism and demons.

To Catch a Ghost

Clearly, old-fashioned Kantian men believed that they could
safely forget about a God who reveals Himself in the Bible and in
history, or demons that grant power to their human subordinates.
Men must forget about discovering any earthly manifestations of the
supernatural, “Supernatural” is defined as not influencing the natural.
Such a view, however, leads to a whole series of problems — problems
that have led, especially since 1965, to a revival of occultism and the
abandonment of Kantian rationalism.

A hundred years ago, the line which separated the ranks of the
credulous, some of them respected scientists (before they began pub-
licly to hunt ghosts), from the ranks of the professional skeptics, was
fairly clearly drawn. The rationalists had spent the nineteenth cen-
tury trying to squeeze the last traces of God out of the universe, and
Darwinism was understood to have accomplished the task. There
would be no more elements of meaning or directionality in the uni-
verse, except that which man would supply. The last traces of “final
causation,” or ultimate directionality in the processes of biological
evolution, had been replaced with random biological adaptation to
randomly changing environments over whatever quantity of time
was deemed sufficient to give random change sufficient elbow room
to bring our world into existence. The God of orthodox Christianity
had supposedly lost His last remaining tochold on the phenomenal
realm. From now on, God would have to content himself with dab-
bling with the noumenal—a safe enough place for Him, really, since
nothing in the noumenal realm is able to influence the phenomenal
(except, logically enough, autonomous man and his man-centered
morality). The noumenal, the “something-in-itself,” was chained up
by mechanical Newtonian law, forever impotent to invade the realm
of the logical.

But then late-nineteenth-century ghost hunters and table rappers
began to break the treaty. Serious British investigators—even
respected scientists—created the Society for Psychical Research in
1882. Their stated goal was to study such phenomena ~ phenomena?!

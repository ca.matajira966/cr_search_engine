2
THE BIBLICAL FRAMEWORK OF INTERPRETATION

... for Satan himself is transformed into an angel of light.
HI Corinthians 11:14

We live in terms of presuppositions. We cannot know everything
exhaustively, so we use ideas, stories, and principles to “sort out” the
“facts” and eliminate the “irrelevant.” What is irrelevant is sometimes
referred to as noise. We need principles of interpretation to differen-
tiate “signal” from “noise.” The myths, ideas, and principles of a par-
ticular civilization determine what sorts of facts are, in fact, relevant,
or are, in principle, relevant. Every society (or subgroup) necessarily
operates as follows: what is not in principle relevant is discarded as
not being in fact relevant. We prefer familiar myths and familiar stor-
ies to difficult facts that undermine our confidence in our myths. It is
too painful to rethink our myths, for that would involve rethinking
all of our facts, too. So we ignore painful facts.

My favorite example of a reigning myth in U.S. history is the
story of Mrs. O’Leary’s cow. This cow supposedly kicked over a lan-
tern in a Chicago barn on the night of October 8, 1871, and the re-
sulting fire burned down the city. A popular movie was made about
this terrible event. Every schoolchild in my generation has heard of
Mrs. O’Leary’s cow. It is a charming story, and while nobody could
really prove it, nobody had to. It was just a story, and so everyone
decided it was all right to believe it. The truth has never been put
into the textbooks. That truth is too painful.

On October 8, 1871, a mysterious giant fireball struck the Mid-
west. There had been drought for three months. The prairie was like
a tinderbox. At 9:30 p.m., the fireball struck. All over the Midwest,
fires broke out. In Wisconsin, 1,500 people died in a 400 square mile
area, As many as 750 died immediately by the suffocating cloud that
descended with the fireball. Nine towns in four counties were burned

50

The Biblical Framework of Interpretation 67

more is known of the concept of divine kingship, the king as god,
and the god as king, as the divine-human link between heaven and
earth. The god-king represented man on a higher scale, man ascended,
and the worship of such a god, i.e., of such a Beal, was the assertion
of the continuity of heaven and earth. It was the belief that all being
was one being, and the god therefore was an ascended man on that
scale of being. The power manifested in the political order was thus
a manifestation or apprchension and scizure of divine power. It rep-
resented the triumph of a man and of his people. Moloch worship
was thus a political religion. . . . Moloch worship was thus state
worship. The state was the true and ultimate order, and religion was
a department of the state. The state claimed total jurisdiction over
man; it was therefore entitled to total sacrifice.”

A revival of occultism in our day, or in the era of the Renaissance,
can and must bring with it a revival of the politics of ancient pagan-
ism, A state which claims total jurisdiction is demonic. It cannot pro-
vide law and order, but only order, and that order is man-centered
and oppressive. It was this development which the Hebrew prohibi-
tions on occultism were intended to prevent: the rise of the total state.
Increasingly, this is what the modern world faces, for both secular, ra-
tional humanism and occult humanism deny that there is any voice of
authority outside of man who cares for man. Secular humanists may
wish to manipulate the world through state power, and occult human-
ists may wish to manipulate the powers of the “beyond” for their own
benefit, but the quest for power leads to the construction of a massive
instrument of power. That instrument is the modern state. It is the

 

planning state, the forecasting state, the macroeconomically program-
med state, Rushdoony is quite correct: “The Moloch state simply rep-
resents the supreme effort of man to command the future, to predes-
tine the world, and to be as God. Lesser efforts, divination, spirit-
questing, magic, and witchcraft, are equally anathema to God. All
represent efforts to have the future on other than God’s terms, to have
a future apart from and in defiance of God. They are assertions that
the world is not of God but of brute factuality, and that man can some-
how master the world and the future by going directly to the raw mater-
ials thereof.”!5 The supposed death or nonexistence of the God of crea-
tion is in fact the death of human freedom and human personality.

14. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 32.
13. Ibid., p. 35.

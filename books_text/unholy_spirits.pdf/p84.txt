78 UNHOLY SPIRITS

Most of us who read about Gauquelin’s supposed discoveries
may be initially skeptical. Yet of all the “astrological” findings ever
offered to the world, his are unquestionably the most difficult to dis-
prove. He is a trained statistician, and he has devoted decades ta ex-
ceedingly thorough research of local birth records. So thorough has
his research been that the various conventional and orthodox scienti-
fic organizations that devote time and money to debunking pro-
occult experiments have steadfastly refused to criticize Gauquelin.
The American Humanist Association, publishers of The Humanist,
has even suppressed the results of their own study which corrobor-
ated his findings, So did the Belgian Committee Para.® The Human-
ist Association had promised to publish the results no matter what,
but when they came out positive, the Association insisted on another
group of tests.® It is the same old game: only negative results are
deemed scientific.

Gauquelin earned a doctorate in psychology at the Sorbonne in
Paris. He also did extensive work in statistics. He had been inter-
ested in astrology as a youth, but he became increasingly skeptical as
he began to investigate such claims as “professional saldiers are often
born under Aires or Scorpio, and rarely under Cancer.” He found no
statistical support for such generalities. But he continued his detailed
studies of birth records.’ What he did discover was the oddity that
famous people from various occupations, such as acting and medi-
cine, are more often than statistically random likely to have becn
born when the planets were in particular positions.*

His first investigation involved 576 members of the French Acad-
emy of Medicine. He selected physicians who had attained distinc-
tion. He used objective criteria for this initial selection, He discov-
ered that eminent doctors tended to be born when Mars and Saturn
had just risen or had just passed midheaven.? We are speaking here
of the actual planets in relation to the earth’s horizon, not astrologi-
cal charts. He divided the heaven into 12 sectors, so that at the point
of birth, 16.7% of those born will be in any given planetary sector.
More than 16.7% of the famous members of a given profession are
born under the sectors of Mars or Saturn, cither rising or just

5. The Committee for the Scientific Investigation of Alleged Paranormal Phe-
nomena.

6, Eysenck and Nias, Astrology, pp. 196-200.

7, Bbid., p. 182.

8. fbd., p. 183.

9. Tbid., p. 184.

92 VICTIM’S RIGHTS

autonomy of man is affirmed by the ritual practices of the duel
and brawl. Wyatt-Brown writes of the antebellum (pre-1861) Ameri-
can South: “Ordinarily, honor under the dueling test called for
public recognition of a man’s claim to power, whatever social level
he or his immediate circle of friends might belong to. A street fight
could and often did accomplish the same thing for the victor.
Murder, or at least manslaughter, inspired the same public ap-
proval in some instances. Just as lesser folk spoke ungrammati-
cally, so too they fought ungrammatically, but their actions were
expressions of the same desire for prestige.”

Under biblical law, injured bystanders are protected from
deliberate violence on the part of other people on an “eye for eye”
basis.'* An injured loscr who walks again is entitled to full com-
pensation. But in the case where the loser dies, the judges are
required to impose a capital sentence on a surviving fighter. When
the loser cannot “walk abroad,” the victor must not be “quit.”
At best, he would have to pay an enormous fine to the family of
the dead man, but even this would seem to be too lenient, since

13, Bertram Wyatt-Brown, Southem Honor: Ethics and Behavior in the Old South (New
‘York: Oxford University Press, 1982), p. 353.

14, A somewhat different problem is raised if a person defends himself from
another person who has initiated violence. What if, in defending himself, a person
injures a bystander? Clearly, it was not the bystander’s fault. I'he person responsible
for inflicting the injury should pay damages. Should it be the persan who initiated
the violence or the defender who inadvertently harmed the bystander? For cxample,
what if a man attacks another man, and the second person pulls out a:gun and fires
at the attacker, hitting a bystander by mistake? A humanistic theory of strict liabilicy
would produce a judgment against the defender, for his defense was misguided, or
excessive, or ineffective, But what if che attacker had grabbed the defender’s “shoot-
ing band,” causing him to fire wildly? The injury to the bystander would seem to
be the fault of the attacker. On the other hand, if the original attacker was using
only his fists, and the defender had pulled out a gun and started shooting ~ a
seemingly excessive response — would this make the original attacker a defender
when he attempted lo grab the weapon? Judgment is complicated, for life is compli-
cated.

The Bible places restraints on violence. ‘The goal of the God-fearing man should
be to reduce private physical violence. ‘Thus, if the attacker uses fists, and the
defender has a weapon, the attacker should be warned to stop. The victim does have
the right to identify the atiacker and press charges. The civil government should
inflict the penalty, But if the attacker still challenges the person with the weapon,
then the person has the right to stop the attacker from inflicting violence on him.

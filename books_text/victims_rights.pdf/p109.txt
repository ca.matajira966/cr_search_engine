The Costs of Private Conflict 97

to their old ways. The Revolution consumes its own children, The
prophet is again put on the run (I Ki. 19).

The humanist courts of our day appeal to religious pluralism,
yet they are creating judicial tyranny.¥ The anti-feud, anti-clan,?*
anti-duel ethic of once-Christian Western bourgeois cul-
tures — societies in which social peace has fostered economic
growth —is being undermined by judges who are creating law:
lessness in the name of a purified humanist legal system. Judicial
pluralism must be replaced, but not from the top down, and not
from the vigilante’s noose outward. The satanic myth of legal
pluralism must be replaced by the power. of the Holy Spirit in the
hearts of men. The Holy Spirit is the enforcer in New Testament
times.

Conclusion

Social order requires a degree of social peace. When biblical
law .began to influence the civil governments of the West, an
inctease of social peace and social order took place. This, in turn,
led to greater economic growth and technological development.”

Christian culture is orderly, The Christian West steadily abol-
ished or redirected the chaos festivals of the pagan world, until
the growth of humanism-paganism began to reverse this process.”
Legal systems became predictable, as the “eye for eye” principle

23. Carrol D. Kilgore, judicial Tyranny (Nashville, Tennessee: Nelson, 197).

24. Weber wrote: “When Christianity became the religion of these peoples who
had been so profoundly shaken in all their traditions, it finally destroyed whatever
religious significance these clan ties retained; perhaps, indeed, it was precisely the
weakness or absence of such magical and taboo barriers which made the conversion
possible. The aften very significant role pliyed by the parish community in the
administrative organization of medieval cities is only one of the many symptoms
pointing to this quality of the Christian religion which, in dissolving clan ties,
importantly shaped the modieval city.” He contrasts this anti-clan perspective with
that of Islam. Max Webex, Economy and Society: An Outline of Interpretive Sociology, edited
by Guenther Roth and Claus Wittich (New York: Bedminster Press, [1924] 1968),
p. 1244,

95, Gary North, The Sinai Strategy: Fomomics and the Ten Commandments (Lyler,
‘Texas: Institute for Christian Economics, 1986), pp. 223-26.

26. Peter Gay aptly titled the first volume of his study of the Enlightenment, The
Rise of Modern Paganism (New York: Knopf, 1966). The two-volume study is titled, Zhe
Enlightenment: An Interpretation (New York: Knopf, 1966, 1969).

 

     

 

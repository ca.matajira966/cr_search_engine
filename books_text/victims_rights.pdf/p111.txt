5
LEGITIMATE VIOLENCE

If men strive, and hurt a woman with child, so that her fruit
depart from her, and yet no mischief follow: he shall be surely
punished, according as the woman’s husband will lay upon him; and
he shall pay as the judges determine. And if any mischief follow, then
thou shalt give life for life, eye for eye, toath for tooth, hand for hand,
Soot for foot, burning for burning, wound for wound, stripe for stripe
(Hx, 21;22-25).

The theocentric principle here is that man is made in God’s
image and therefore must be protected by civil law. The husband
of the victimized woman rcpresents God the Judge to the con-
victed criminal. The State is required to impose sanctions speci-
fied by the husband. The violent person who has imposed on the
woman and the child the risk of injury or dcath must compensate
the family, The judges do retain some degree of authority in
specifying the appropriate sanction. The criminal must pay “as
the judges determine.” In the absence of actual physical harm,
there is no rigorous or direct way to assess the value of this risk
of injury or death, so the State does not allow the busband to be
unreasonable in imposing sanctions.

Where physical damage can be dctcrmined objectively, the
criminal must pay on an “eye for eye” basis. This is the judicial
principle known as the éex éalionis. The punishment must fit the
magnitude of the violation; the violation is assessed in terms of
the damages inflicted.

Controversy Over Abortion

Exodus 21:22-25 has recently become one of the most contro-

99

120 VICTIM'S RIGHTS,

bondservant, He could reject the demand of the victim for eco-
nomic restitution and insist instcad on his legal right under bibli-
cal law: to suffer the same physical mutilation that he had im-
posed on the victim.

The Right to Punishment

Each of the parties in this judicial dispute has biblically speci-
fied legal rights. he victim has the right to insist on the biblically
specified maximum physical sanction: eye for eye. He also has the
tight to offer the criminal an alternative, one which appears to
be less severe than the biblically specified physical sanction. If the
alternative offered to the criminal is not regarded by him as less
severe, then he has the legal right to insist on the imposition of
the biblically specified maximum sanction. He therefore possesses
the right to he punished by the specified biblical sanction. His punishment
is limited by the extent of the injury which he imposed on his
victim. The punishment fits the crime.

Tt is basic to the preservation of liberty that the State not be
allowed to deny to either the victim or the. criminal his right of
punishment. While this principle of the right to punishment is at
least vaguely understood by most people with respect to the vic-
tim, it is not well undcrstood with respect to the criminal. The
tight to be punished is a crucial legal right, one which Paul
insisted on at his trial: “For if I be an offender, or have committed
any thing worthy of death, I refuse not to die: but if there be none
of these things whereof these accuse me, no man may deliver me
unto them. I appeal unto Caesar” (Acts 25:11).

If the State can autonomously substitute other criteria for
deserved punishment, such as personal or social rehabilitation,
then socicty loses its right to be governed by predictable laws with
predictable judicial sanctions. The messianic State then replaces
the judicially limitcd State. Neither the victim nor the criminal
can be assured of receiving justice, for justice is defined by the
State rather than by God in the Bible. If punishment is not seen
as deserved by the criminal, and therefore his fundamental right, then
he is delivered into the “mercifil” hands of elitist captors who are
not bound by written law or social custom. No one has described

122 VICTIM'S RIGHTS.

convicted criminal “pay his debt to society,” then the messianic
State has arrived. God has specified that the victim is His repre-
sentative in criminal cases, not the State, unless the victim is
legally unable to represent himself, in which case the State acts
as his trustee. Only if the State is the victim can it lawfully
demand restitution. When the State presents itself as the universal
victim of all crime to which is owed universal restitution by
criminals and taxpayers alike, it has asserted its own divinity.

 

Benefits of Alternative Sanctions

The proposed economic solution to the dilemma of the lex
talionis offers at least three very real benefits. The first benefit is
judicial: the victim has the right to specify the appropriate punishment.
This punishment is limited only by the maximum penalty spcci-
tied by biblical law, eye for eve. The biblical principle of victim’s
rights is upheld by the judges. If the victim believes that the
criminal’s act was malicious, and if he wishes to inflict the same
damage on the criminal which he himself suffercd, this is his legal
option.

To take this retributive approach, however, he necessarily
fortvits all the economic advantages he might have received from
a restitution payment from the criminal. He can exercise his
legitimate desire for vengeance — his desire to reduce the. criminal
to a physical condition comparable to his own — but this desire
for vengeance has a price attached to it. He is made no better off
financially because of his enemy’s suffering. In fact, he could be
made slightly worse off: he, as a member of the economic commu-
nity, loses his portion of the other man’s lost future productivity,
assuming the man cannot overcome the effects of his lost eye or
limb. Vengeance in the Bible’s judicial system has a price tag attached to
it. This inevitably reduces the quantity of physical vengeance
insisted on by victims, for biblical civil justice recognizes the
judicial legitimacy of a fundamental economic law: “The higher
the price of any economic good, the less the quantity demanded.”

The second benefit of this interpretation of dex talionis is also
judicial: the criminal who is about to lose his eye or tooth is permitted to
make a counter-offer. He has the right to be punished to the limit of

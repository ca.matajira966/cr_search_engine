130 VICTIM'S RIGHTS

T'll accept 500 ounces of gold instead of his tooth.” By lowering
his new demand, he would be admitting that his initial offer had
been higher than his minimal demand. In short, the injured vic-
tim must know in advance that by making an excessive initial
financial demand, he might “pricc himself out of the market”; he
therefore has to be reasonable if he is really after moncy. He might
wind up with nothing except the pain and disfigurement of the
criminal as his reward. He must ask for less moncy in order to
increase his likelihood of collecting anything.

‘Lhe judges would present the victin’s specified choices to the
criminal, and the criminal would have the option of refusing to
pay the 1,000 ounces. The judges would then have the physical
penalty imposed,

The man condemned by the victim to permanent physical
mutilation would have the option of making a countcr-proposal
if the victim had offered no option to mutilation. The victim could
then consider it. Again, the criminal would be allowed only one
offer; if the victim still says no, and the criminal then makes a
higher offer, he can be presumed to have given false witness when
he made the first offer. By limiting the victim to presenting the
criminal with only one sct of options, and by giving the criminal
the opportunity to make a single counter-offer only when no
alternative option has been offered by the victim, the judges can
obtain honest offers from the beginning.

The court would allow only one form of second-chance bids.
If the criminal is unwilling to pay the victim the money payment
demanded, but he is willing to pay in some other way than
money, he would have the opportunity to present the alternative
or group of alternatives for the victim to choose from. But if the
victim turns this counter-offer down, the criminal will then have
to undergo mutilation. He is governed by the equivalent rule that
governs the victim: honest bidding. He offers his highest price or
best bid. If it is rejected, he must suffer the physical consequences.

The Authority of the Judges

The integrity of society’s covenantal civil judges is fundamen-
tal to the preservation of social order. The Bible warns rulers and

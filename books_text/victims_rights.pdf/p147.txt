 

The Ransom for an Eye 135

Jaw upholds his right to demand the punishment spcciticd by
God. Bargaining is legitimate, but both the victim and the crimi-
nal can insist on the specified penalty. If the victim insists on
physical multilation, the criminal has no choice. If the criminal
insists on physical mutilition, the victim has no choice. Bargain-
ing, however, is likely.

By establishing the three-way system of establishing penalties
—judges, victim, and convicted criminal —the judicial system
receives a means of making odjective approximations of the inescap-
ably subjective “eye for eye” standard — subjective to both victim
and criminal. By permitting subjective estimations of loss by both
the victim and the criminal, the judges find a way to offer compen-
sation to the victim that he believes is comparable to the crime.
The criminal, however, is allowed to counter-offer a different,
economic form of restitution penalty if he believes that the cost of
a physical penalty is too high.

Conclusion

My discussion of the possible outworkings of the “eye for eye”
passage should not be understood as the last word on the subject.
Tt is, however, a “first word.” I want readers to understand that
the biblical justice system is just, workable, and effective. The lex
ialionis should not be dismissed as some sort of peculiar juridical
testament of a long-défunct primitive agricultural society. What
the Bible spells out as judicially binding is vastly superior to
anything offered by modern humanism in the name of civic jus-
tice,

The problems in dealing with the actual imposition of the dex
talionis principle are great. The history of the people of God testi-
fies to these difficultics. We have few if any examples of Christian
societies that have attempted to impose the “eye for eye” principle
literally. The basic principle is clear: the punishment should fit the
crime, By allowing the victim to demand restitution in the form
pleasing to him, and by allowing the criminal to counter-offer
somcthing more pleasing to him, the penalty comes close to match-
ing the effects of the crime, as assessed by the victim.

Each party gets to make one offer. If the victim offers a choice

 

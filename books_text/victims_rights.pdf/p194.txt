182 VICTIM’S RIGHTS

no matter what his inner feelings are. The State should lawfully
enforce this. But it cannot enforce repentance, for this would
involve the God-prohibited claim of the State to be God. The
State is not God; its officials cannot know what is in men’s hearts.
Nevertheless, when a criminal today is sentenced, a reporter will
sometimes add these words: “He showed no signs of remorse.”
This indicates that even in modern “sccular” times, we potential
victims expect criminals to show signs of repentance. Second, the
criminal is morally required by God to repent, and to declare
himself completely at the mercy of God. The penalty for failing
to do this is an eternity of punishment in God’s fiery prison, from
which there is no escape. No human government can lawfully
enforce this required repentance, as we have seen. ‘Third, in re-
sponse to both external restitution to the victim and internal
repentance before God, God restorcs the sinner to wholeness.
This is the gift of God’s grace,

It must be stressed that the State cannot legitimately require
the internal act of repentance; officers cannot know the criminal’s
heart. Men cannot and must not try to enforce repentance; our
laws carmot legitimately be written in terms of the internal state
of a person’s mind. The State also cannot legitimately require a
public statement of theological faith from all residents in a society.
The “stranger within the gates” may believe what he wants about
God, man, and law. This does not mean that the State cannot
legitimately require 4 statement of faith from those who seek
citizenship and therefore the right potentially to serve as judges
“within the gates.” If a person is not covenantally under Jaw, he
should not be allowed to become a judge —voter, juror, civil
magistrate —who places others under that law. The covenant is
hierarchical; to rule we must also serve. To enforce a law-order,
we must be under it.

This may sound strange to Christians who for some reason
still believe in the humanist myth of morally and religiously neu-
tral law? but everyone in the United States is governed by a
humanist version of this covenantal principle of hierarchy and

2. Gary DeMar, Ruler of the Nations: Biblical Blueprints for Goverement (Ft, Worth,
Texas: Dorninion Press, 1987), ch. 3: “Plural Law Systems, Phural Gods.”

190 VICTIM'S RIGHTS

Christian Theology

Restitution, repentance, and restoration are equally funda-
mental concepts in Christian theology. Without Christ’s restitu-
tion payment to God for the sins of mankind, there could have
been no history from the day Adam fell. Without repentance, the
individual cannot claim to be free from the requirement to make
the restitution payment to God. Eternal judgment is God’s lawful
vengeance on all those who have not made restitution, meaning
all those who have not placed themselves at the mercy of God by
claiming to be under Christ’s general repayment. ‘The righteous-
ness of God is demonstrated by His eternal punishment of those
who have not made full restitution. The punishment fits the crime
of ethical rebellion against a sovereign, holy God.

Restitution in Recent American Jurisprudence

Various forms of restitution have been adopted by civil gov-
ermments for centuries.'° Experiments by state and local govem-
ments in the United States since the mid-1970’s also indicated
that such a system can provide significant benefits to victims. The
state of Minnesota began its experiment in October of 1973.
Based on one year’s data, researchers made a study of opinions
and results. Restitution was a condition of probation of the crimi-
nals in one-fourth of all probation cases. “Restitution was used in
a straightforward manner by most courts. Full cash restitution
was ordered to be paid by the offender to the victim in more than
nine out of ten cases. Adjustments in the amount of restitution
because of limited ability of the offender were rare. In-kind, or
service, restitution to the victim or community was ordered in
only a few cases... .”"!

 

capital punishment at the bands of the court, because Scripture says, And if a man
come presumpluously upon kis neighbor (Exod. 21:12). Needless to say, one is not put to
death if he kills a heathen.” Moscs Maimonides, The Book of Torts, vol. 11, The Code
of Maimonides, 14 vols. (New Haven, Conneticut: Yale University Press, [1180]
1954), Chapter Two, Section Eleven, p. 201.

10. J. A, Gylys and F, Reidy, “The Case for Compensating Victims of Crime,”
Atlania Economic Review, XXV (May/June 1975).

IL. Stenmary Report: The Assessment of Restitution in the Minnesota Probation Services,
prepared for the Governor's Commission on Crime Prevention and Control (Jan. 31,

1976), p. 1.

Introduction 9

community. By disrupting the delicate nexus of ties, formal and
informal, by which we are linked with our neighbors, crime atom-
izes society and makes of its members mere individual calculators
estimating their own advantage, especially their own chances for
survival amidst their fellows,”

The Old ‘lestament’s law-order, when enforced by the judges,
overcame these criminal tendencies toward social atomization.
Old Testament criminal law was designed by God to protect the
community by defending the rights of innocent victims. Today,
we have seen the rise of the messianic State, whose sel{-appointed
task is-to heal society through a program of salvation by legisla-
tion. Humanism substitutes the concept of salvation by man’s law
for salvation by God’s grace. It also substitutes its own sanctions
for the Bible’s sanctions, It promises to redeem (rehabilitate)
criminals, but then neglects to defend their victims.. Until quite
recently, it focused exclusively on rehabilitation rather than resti-
tution, not undcrstanding that without restitution there can be no reha-
bilitation, socially or psychologically, Without Jesus Christ’s restitution
payment to God for the sins of man, there could never have been
rehabilitation cosmically, for even with it, the whole world came
under a curse (Gen, 3:18). Society's institutions of justice are
supposed to reflect the judicial terms of this cosmic redemption.
When they do not, we can confidently expect God’s historical
negative sanctions to reform the institutions (Deut. 28:15-68).

The Negative Function of Biblical Civil Law

The function of civil law, according to the Bible, is not to save
men’s souls but to restrain their evil public behavior. Biblical law
points to Ged and His judgment, and so can become a means of
special saving grace (Rom. 7:7-12), but the function of all biblical
civil law is negative, not positive. Biblical civil law imposes nega-
tive sanctions only. Tt denies the presupposition that man can be
saved by law, let alone by legislation. The builders of the modern
messianic State have forgotten this, and as a result, we are now
engulfed in wave aftcr wave of tyrannical legislation: the never-
ending quest to redeem man by. remaking his social and economic

 

20, Wilson, Thinking About Crime, p. 2t.

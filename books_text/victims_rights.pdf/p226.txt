24 VICTIM'S RIGHTS

ized buyer whatever the buyer paid him for the stolen animal.
Why? Because the victimized buyer may later discover that he
has purchased a stolen beast. If he then remains silent, he breaks
the law. He is a recipient of stolen goods. He has become an
accomplice of the thief, His silence condemns him. Additionally,
he may feel guilty because he is not its legal owner.

How can the defrauded buyer escape these burdens? He can
go to the original owner who has already received full restitution
from the thief (or from the person who has purchased the thief
as a‘slave), and offer to sell the animal back to him, Once the
victimized buyer identifies himself, the thief now owes restitution
to the defrauded buyer: double restitution, minus the purchase
price that the defrauded buyer receives from the original owner,
The thief has stolen from the buyer through fraud. As is the case
with any other victim of unconfessed thefi, the defrauded buyer
is entitled to double restitution from the thief. Therefore, as soon
as the thief gets through paying his dcbt to the original owner,
he then must pay the victimized buyer the penalty payment.

Tf the original owner declines to buy the beast, the buyer
becomes its legal owner. The original owner does not want it
back. He has also been paid: restitution from the thief. But the
defrauded buyer remains a victim. He keeps the beast, but he is
also entitled to restitution from the thief equal to the original
purchase price charged by the thief.

If the thief confesses before the trial, he can avoid the risk of
the extra payment to the defrauded buyer. Even if the victim
demands four-fold or five-fold restitution, by paying it, the thief
thereby becomes the owner of the beast. The criminal’s act of timely
confession, plus his agreement to pay full restitution to the victim, atones
judicially for the theft.

But what about the defrauding of the buyer? I think the
confessed thicf would owe the buyer a restitution payment of 20
percent of the purchase price because he had involved the buyer
in an illegal transaction. Having repaid both owner and buyer,
he has legitimized the new ownership arrangement. The buyer

 

4, Obviously, T am speaking here only of the earthly court. Atonement means
“covering.”

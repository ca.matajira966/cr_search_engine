216 VICTIM'S RIGHTS

Considering an Alternative Arrangement

Tf there were no risk to the thief attached to remaining silent,
what would be the thief’s incentive to tell the owner that he knows
where the stolen beast is? Assume that the thief owes no manda-
tory penalty payment to the defrauded buyer once he has paid
restitution to the victim, He pays full restitution to the owner, and
the defrauded buyer then hears about this, realizes that he has
purchased stolen property, and comes to the owner. He offers to
sell back the missing beast to the owner at the market price the
beast was worth to the owner when the beast was stolen (presum-
ably, the price he paid to the thief), Tf the thief owes nothing to
the defrauded buyer, he is still out only five-fold restitution by
having concealed evidence.

What is wrong with this interpretation of the restitution stat-
utes? Answer: the thief has entangled the buyer in an illegal
transaction that was inherently filled with uncertainty for the
buyer. The latter might have been convicted of being a “fence” —a
professional receiver of stolen goods. He has therefore been de-
frauded by the thief. He deserves restitution.

What if the original owner says that he does not want to buy
the beast from the defrauded buyer? The buyer has now in effect
purchased the beast from its rightful owner. He now owns the
“bundle of rights” associated with true ownership. But the thicf
has nevertheless cxposed him to the discomfort of being involved
in an illegal transaction. Shouldn’t the thief still owe the seller a
100 percent restitution payment? My assessment of the principle
of victim’s rights leads me to conclude that biblical law does in
principle allow the defrauded buyer to come to the judges and
have them compel the thief to pay him 100 percent of the price
he had paid the thief. This has nothing to do with whether he has
sald the beast to the original owner or whether the owner has
allowed him to retain legal possession of it.

Transferring Lawful Title
Why must we regard the sale of the animal as fraudulent?

Why can the authorities legitimately demand that the purchaser
return the animal to the original owner? Because the thief implic-

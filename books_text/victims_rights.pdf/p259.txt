Personal Responsibility and Personal Liberty 247
Insurable Risk

‘The existence of fire insurance does not invalidate this analy-
sis of “the economics of specific effects.” While it is sometimes
possible for a person to buy fire insurance, the reason why fire
insurance is available at all is because companies insure many
different regions, thereby taking advantage of “the law of large
numbers.” They can insure specific propertics cconomically only
because fires: have known effects in the aggregate. If there were
no known statistical pattern to fires in general, insurers would not
insure specific properties against fire damage.

This is not to say that the following arrangement should be
prohibited by law. A person who wishes to begin a business which
is known to be dangerous approaches others who could be af-
fected. “I’ll make you a deal,” he says. “I will pay for all increases
in your insurance coverage if you let me begin this business in the
neighborhood.” If they agree, and if the insurance companies
agree to write the policies, then he has met his obligations. He
has made himself economically responsible for subsequent dam-
ages. Instead of paying for damages after the fact, he has paid in
advance by providing the added insurance premiums necessary
to buy the insurance.

What if somc resident says “no”? The prospective producer
of danger can then offer to buy him out by buying his property.
If the offer is accepted, the prospective danger-producer can then
either keep the property or sell it to someone who is willing to live
with the risk, if the discount on the land’s selling price is suffi-
ciently large. But if the original owner refuses to sell, and also
refuses to accept the offer regarding insurance premiums, then the
first man should not be allowed to force out the original owner.
If he begins the dangerous production process, the existing prop-
erty owner can legitimately sue for damages. The court may
require a money payment from the danger-producer to the poten-
tial victim. The court need not necessarily prohibit the activity
altogether.

This decision by the judges requires that judges do the best
they can in estimating the costs and benefits to the community,
including the perceived value io citizens everywhere of the preservation by the

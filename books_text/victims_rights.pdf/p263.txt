Personal Responsibility and Personal Liberty 251

made in terms of carlicr knowledge. With greater knowledge comes
greater responsibility (Luke 12:47-48). If society tries to impose
damages retroactively on actions that were taken yesterday based
on yesterday’s information, it would destroy the legal foundation
of progress.

There can be no life without risk and uncertainty. We must
not strive to build a zero-risk world. What we must do is to
restrain those who would impose added known risks in the lives
of neighbors without the latter’s permission. We find the legal
tule that provides this restraint in Exodus 22:5-6.

Externalities

A man should not be prosceuted for polluting, defacing, or
otherwise lowering the value of his own land, so long as his actions
do not have measurable physical effects on anyone else’s life,
health, or property. Because it is his own Jand, he has internalized
the costs of operation. (By “internalize,” I do not mean simply a
mental calculation; I mean that his property alone suffers from his
mistakes.) He risks starting a fire on his own property, or he runs
a herd of cattle on his own property, The man making the esti-
mate of benefits is the same person who makes the estimate of
costs; it is the same man who will reap what he sows.

Once he sells a section of his land, he no longer internalizes
costs and benefits on the section that was sold. Another person is
now involved: his neighbor. The first man must not be allowed
to pass on to his neighbor the risks of living next door to a person
who sets fire on his property. The fire-startcr cannot legally trans-
fer to his neighbor the generally known but highly unpredictable
Specific, individual production costs of fire. Economic analysis must
begin with the Bible’s assessment of personal responsibility for a man’s
actions. It must begin with the presupposition of the rights (legal
immunities) of private property. These rights must be protected
by civil law and custom.

Conclusion

By assigning to individuals the cconomic and. legal responsi-
bilitics of ownership, God imposes on individuals the burden of

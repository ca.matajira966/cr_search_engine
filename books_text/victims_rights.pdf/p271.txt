Binding the State 259

preserves all three.

1. General Rules

First, with respect to general rules, Hayek writes that these
rules must distinguish private spheres of action from public spheres,
which is crucial in maintaining freedom: “What distinguishes a
free from an unfree society is that in the former each individual
has a recognized private sphere clearly distinct from the public
sphere, and the private individual cannot be ordered about but
is expected to obcy only the rules which are equally applicable to
all. It used to be the boast of free men that, so long as they kept
within the bounds of the known law, there was no need to ask
anybody’s permission or to obey anybody’s orders. It is doubtful
whether any of us can make this claim today.”! If men must ask
permission before they act, society then becomes a top-down bureau-
cratic order, which is an appropriate structure only for the military
and the police force (the “sword”).!! The Bible specifies that the
proper hierarchical structure in a biblical covenant is a bottom-up
appeals court structure (Ex. 18).'2

Adam was allowed to do anything he wanted to do in the
garden, with only one exception. He had to avoid touching or
eating the forbidden fruit. He did not have to ask permission to
do anything else. He was free to choose.'3 This biblical principle
of legal freedom is to govern all our decisions.4 This is stated
clearly in Jesus’ parable of the laborers who all received the same
wage. Those who had worked all day complained to the owner
of the field. The owner responded: “Friend, I do thee no wrong:
didst not thou agree with me for a penny? Take that thine is, and

10. F, A, Hayek, The Constitution of Liberty (University of Chicago Press, 1960),
pp..207-8,

11. Ludwig von Mises, Bureaucracy (Cedar Falls, Iowa: Center for Futures Educa-
tion, [1944] 1983), ch. 2. Distributed by Libertarian Press, Spring Mills, Pennsylvania.

12. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 2.

13. Milton and Rose Friedman, Free to Choose: A Personal Statement (New York:
Harcourt Brace Jovanovich, 1980).

14. Grace Hopper, who developed the.computer language Cobol, and who served
as an officer in the U.S. Navy until she was well into her seventies, offered this theory
of leadership: “It’s easier to say you're sorry than it is to ask permission.”

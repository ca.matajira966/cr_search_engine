17
EMPTYING THE PRISONS, SLOWLY

Then his lord, after that he had called him, said unto him, O
thou wicked servant, I forgave thee all that debt, because thou desiredst
me. Shouldest not thou also have had compassion on thy fellowservant,
even as I had pity on thee? And his lord was wroth, and delivered him
to the tormenters, till he should pay all that was due unto him. So
likewise shall my heavenly Father do also unto you, if ye from your
hearts forgive not every one his brother their trespasses (Matt. 18.32-
35).

Debtors prison: one of the horrors of any humanist age. The
governments of the West closed them only in the final third of the
nineteenth century. They had obviously existed as early as Jesus’
era. But they did not exist in the Old Testament.

Why did Jesus use the debtor’s prison as His example of
God’s eternal punishment? Was He sanctioning the creation of
an institution unknown to Old Testament Israel? No. On the
contrary, He was demonstrating that until the day of judgment,
God is merciful to men, allowing them to make restitution to their
victims and to God for their sins. God keeps open the door for
men to affirm the only restitution payment suitable in God’s court
of final judgment: personal faith in the atoning (restitution-
paying) work of Jesus Christ on Calvary. Just as the rich lord
allowed his servant time to pay off his enormous debt, so should
this servant have allowed his debtor time to pay off a much
smaller debt.

There is no doubt what this passage teaches: once you are
thrown into the cosmic debtor’s prison of hell, there is no escape.

265

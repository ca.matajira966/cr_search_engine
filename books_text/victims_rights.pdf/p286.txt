274 VIGTIM'S RIGHTS

cient to stop the spread of his evil. The pile of stones was intended
rather to serve as a covenantal reminder. Each pile of stones
testified to the reality of covenant sanctions, a monument to God's
judgment of cursing in history, just as the stones from the River
Jordan were made into a memorial of God’s judgment of the
deliverance of Israel (Josh. 4:7-8).

Public stoning forces citizens to face the reality of the ultimate
civil sanction, execution, which in turn points to God's ultimate
sanction at judgment day. Stoning also faithfully images the prom-
ised judgment against Satan: the crushing of his head by the
promised Seed (Gen. 3:15). Because most people, including Chris-
tians, do not want to think about God’s final judgment, they
prefer to assign to distant unknown executioners the grim task of
carrying out God’s judgment in private. This privatization of
execution is immoral; it is itself criminal. It is unjust to the
convicted criminal, and it is unjust to the surviving victims, who
do not see God’s justice done in public. The systematic impersonalism
of capital punishment is the problem, not capital punishment as
such. This deliberate impersonalism has corrupted the entire pe-
nal system today.

Public stoning would allow a condemned man to confront the
witnesses and his executioners. The idea of a private execution
where the condemned person cannot have a final word to those
who have condemned him is anything but liberal-minded. It was
long considercd a basic legal privilege in the West for a con-
demned person to have this final opportunity to spcak his mind.
The sign of the intolerance of the “liberal” French Revolutionarics
was their unwillingness to allow King Louis XVI to speak to the
crowd at his execution. The judges had ordered drummers to
begin drumming the moment he began to speak, which they did.*?

Whereas men uscd to be flogged in public or put in the stocks
for a few days, we now put them in hidden jails that are filled
with a professional criminal class, as well as with ATDS-carrying
homosexual rapists. This impersonalism of punishment has been
paralleled by a steady bureaucratization and institutionalization

33. Leo Gershay, The French Revolution and Napoleon (New York: Appleton-Century-
Grofts, 1933), p. 238.

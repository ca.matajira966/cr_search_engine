294 VICTIM'S RIGHTS

or treated as misdemeanors. Of the 16,318, 56 percent resulted
in felony pleas by the defendants of guilty; 16 percent resulted in
misdemeanor pleas; 13 percent in trials leading to a verdict; and
12 percent in dismissals.*! In short, criminals are rarely sent to
prison for any particular criminal act.

The criminals know for certain what the public suspects:
crime does pay.°? The risks of being caught for one crime are low.
The risks of a repeater’s being caught are high, The risks of being
convicted and serving a lengthy period in prison are minimal.
Prof. Walter Burns, a political scientist at the University of Toronto,
has summarized the problem: “Between 1966 and 1971 the U.S.
murder rate increased by 52%, and the crime rate as a whole
rose by 74%, as reported in Grime in the United States: Uniform Crime
Reports, 1971. Crimes of violence (murder, forcible rape, robbery
and aggravated assault) went up 80%. In 1971 there were 5,995,200
index crimes (crimes catalogued by the FBI) reported to the
police, and everyone knows that a large number of crimes are
never reported to the police. The proportion of arrests to crimes
reported was only 19%, persons charged 17%, persons convicted
as charged 5%, and persons convicted of lesser offenses .9%. All
of which means that punishment was meted out in only 5.7% of
the known cases of crime. The conclusion is inescapable: crime

31, These statistics appear to be precise. This is an illusion. The confusion in New
York City police and court records is legendary. See the article, “Police in New York
City Turning to Computers to Untangle Records,” New York Times (Feb. 27, 1982).
For every arrest, 15 different forms have to be filled out, and paper work is scattered
throughout the city. “According to the police, about 2,000 of the 100,000 or so
persons arrested on felony charges Last year will have been tried. They say they want
to know what happened to the other 98,000 case: ” This raises another prob-
lem: Who will have access to the computerized files? Will the security system resist
intrusion? No such system has been-devised so far.

 

32. Economists and economics-influenced legal scholars, especially those of the
so-called “Chicago School,” have used economic theory to produce some remarkable
conclusions in this regard, especially Gary Becker, Richard Posner, and Gordon
Tullock. For an introduction to this literature, see Posner’s speech, The Eoonomic
Approach te Law, published in 1976 by the Law and Economics Center of the Univer-
sity of Miami, Coral Gables, Florida. Posner’s textbook is also important, Economic
Aspects of Law (Boston: Little, Brown, 1986). The journal of Law and Economics and The
Journal of Legal Studies, published by (he University of Chicago, are important outlets
for this research.

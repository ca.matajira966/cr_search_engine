ownership, 143, 167-68, 170
personal, 179
stewardship, 243
upward and downward, 139-40

(see also liability)

restitution,

best goods, 244
bloody sacrifice, 186
confession, 70
defrauded buyer, 214-15
deterrence, 201
double, 231
economics, 209-18
experiments, 190-91, 201-2
eye for eye, 105
fines, 175-76
fitting the crime, 205
five-fold, 193-95, 200
four-fold, 194
goals, 282-84
God and, 25
hell, 266
meaning, 8
prison and, 266
purposes, 180-81
rehabilitation and, 9
restoration, 181, 283
“resurrection,” 283
Rushdoony, 210
Rushdoony on; 205
sacrilege, 68-69
seven-fold, 254-55
slavery, 276
substitute, 204
technology and, 128-29
two-fold, 199
vengeance, 109
victim, 59

restoration, 180-81, 263, 283

resurrection, 41, 55

revolution, 65

rights (bundle), 212

tisk
calculating, 75-78

General Index 313

crime, 294-95
criminal, 69
detection, 230
drunk drivers, 177
insurabie, 247
insurance costs, 162
kidnapper, 75-77
kidnap victim, 67-69
owner, 139-40
rich, 74
slave trade, 79
social, 157
speeding, 258
transferring, 246-48
victims, 258
zero, 156-57, 172-73, 174-75
Rome, 87-98
rope, 156
Rothbard, M., 207-8
Rothman, David, 267-68
Rousseau, J. J., 56
Rushdoony, Rousas J.
civil oath, 240
etemity, 236
divorce, 28
eunuchs, 140
incorrigible sons, 272
juvenile delinquents, 52-53
limited liability, 158n
restitution, 205, 210
sacrilege and theft, 68
stealing freedom, 83
victim’s rights, 84

sabbath, 55n
sabbatical year, 40
sacrifice, 244
sacrifices, 239
sacrilege, 68
safety (see risk)
sanctions,
absence, 56
collective, 220-21, 225-26
criminal’s right, 120

36 VICTIM’S RIGHTS

ized to initiate a covenant lawsuit against the suspected criminal.
This restriction on State’s authority to initiate a covenant lawsuit
is an implication of the doctrine of victim’s rights. The victim
possesses the right to forgive. The State is not authorized to ignore
or supersede this right,

The interests of the community are upheld by identifying the
criminal or member of the criminal class. Remember, God is the
primary victim of crime; He has authorized representatives to defend
the integrity of His name. If a community refuses to do this — if
church, State, and family governments break down — God threat-
ens to bring His negative sanctions through other agencies: war,
pestilence, and famine (Deut. 28:15-68). This is why an unsolved
murder in a field required a public blood sacrifice by the nearest
city’s civil magistrates, not the priests (Deut. 21:1-9).?

A Legal Claim

Who acts as God’s authorized agent in the bringing of a
covenantal civil lawsuit? The victim, the witnesses, or those wha
are authorized agents of the civil government. If the initiator of
the lawsuit is the victim, he is not acting primarily on his own
behalf, but as an agent of God because of his position as the
victimized intermediary between the criminal and God, the ulti-
mate victim. He is acting secondarily in his own behalf, for any
restitution payment will go to him. Similarly, witnesses who bring
evidence to the State for use in prosecuting the covenant lawsuit
are acting as representative agents of God through the civil gov-
ernment. They do not act on their own behalf, for they have no
legal claim on the resources of the person who is being charged
with the crime, should he be convicted. Witnesses are not victims.
They are acting in the name of God as authorized and oath-bound
agents of the State when they testify in a civil court. Where there is
no direct legal claim, there is no direct covenantal relationship. Thus,
witnesses are acting as indirect agents of God as participants in
the civil commonwealth.

Because crimes are always crimes against God, the State has
a law-enforcement role to play, for the State possesses God’s

2. Glearly, the Book of Hebrews has annulled this practice today.

The Death Penalty 37

authorized monopoly of the sword: the imposition of physical
sanctions. The State in turn implicitly delegates the office of wit-
ness to those who view a crime or who have information relevant
to the State’s prosecution of a covenant lawsuit. (This is the
judicial basis of what in English common law is known as “citi-
zen’s arrest,” although it is seldom invoked today.) This is why
the State can lawfully compel honest testimony from a witness:
the witness is under the authority of the State. It is in fact unlaw-
ful to withhold evidence of a crime when subpocnaed. While the
State may offer a reward for the capture and conviction of a
criminal (a positive sanction: blessing), this is at the discretion of
the State. he witness who seeks an announced reward has a
claim on the State, not on the criminal.

The most important example in history-of a reward-seeking
witness is Judas Iscariot, who collected 30 picces of silver from
the Jewish court to witness against Jesus Christ. He later returned
the money, not because it is inherently wrong to accept money
as an honest witness, but because he knew he had been a false
witness in a rigged, dishonest trial. The Jewish leaders self-
righteously replicd, “What is that to us?” (Matt. 27:4b). They felt
no sense of guilt, so why. should he? They also recognized the
tainted nature of thé moncy, which was the price of blood, and
as true Pharisces, they refused to accept his repayment (Matt.
27:6). Committing murder by rigging a court was irrelevant in
their view, a means to a legitimate end; getting paid for false
witness-bearing, however, was seen by them as a sin. This is the
essence of Pharisaism, the classic historical example of Pharisaism
in action. They were happy to serve as the most corrupt court in
man’s history, but they judiciously refused to accept money for
their efforts. (What is not recognized by most Christian commen-
tators is that the testimony of a witness in a Jewish. court was
invalidated, at least by the law of the Pharisees, if he had received
payment for testifying.)

What is my conclusion? Only that witnesses have no legal
claim on the criminal. The authorized agents of God in the prose-

3. Bekhoroth 4:6, in The Mishnak, edited by Herbert Danby (New York: Oxford
University Press, [1933] 1987), p. 534.

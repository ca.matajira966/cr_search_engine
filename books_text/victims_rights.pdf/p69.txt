The Death Penalty 57

to govern their theory of the State, defenders of the “Noahie covenant”
theory have implicitly granted judicially unlimited power to the modern
State, no matter how much they protest against such a develop-
ment. They may be political conservatives personally; it makes
no difference. Their personal political preferences become just
that: personal preferences. Their personal political preterences are
self-consciously and explicitly unconnected with any biblical-
theological system of social ethics and political theory.*

Such a view of Noah’s low-content covenant grants enormous

 

authority to self-proclaimed autonomous man and his representa-
tive, the messianic State. The power-seeking covenant-breaker is
as pleased with such a view of the State as the responsibility-
flceing Christian pictist is. This is why there is now and always
has been an implicit judicial alliance between antinomian Christians and
humanist statisis. Here is an ideal way to silence Christians in all
judicial matters except murder: insist that “The Bible doesn’t offer
a blueprint for civil law!” With this judicial affirmation, anti-
nomian, responsibility-fleeing Christians sound the retreat, and
secular humanists and othcr covenant-breaking power-seckers
sound the attack. The victim is in principle victimized ever furthcr
by this view of Noah’s drastically restricted covenant, and the
messianic State is unchained by it. AH this is accomplished in the
name of a “higher” view of theistic ethics than the Mosaic law supposedly
offered to the Israelites.

This supposed dichotomy between Noah's covenantal sanc-
tions and Moses’ covenantal sanctions, and also between Moses’
covenantal sanctions and Jesus’ covenantal sanctions, cannot sur-
vive a careful cxamination of the biblical principle of victim’s
rights, which is also the principle of the judicially limited State.
The biblical judicial principle is this: victims of criminal acts
possess the God-granted legal right to specify no penalty or any
penalty up to the maximum limit allowed by God’s Bible-revealed
law. Neither the State nor the humanistic sociologist is entitled

24, I stadied systematic theology under John Murray. In private, he was an
anti-New Deal conscrvative. In public, he was politically mutc. Both Wayne House
and Tommy Ice are political conservatives. In torms of a developed social and
political theory, however, they are equally mute.

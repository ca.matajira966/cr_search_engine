The Death Penalty 61

cial questions should they abandon the principle of victim’s rights.

Addendum: Cases to Which the Pleonasm Is Attached

1 have put in bold face those case laws in which the State in
Old Testament Israel was required to initiate the prosecution,
and therefore those cases in which the convicted criminal had to
be put to death.

He that smiteth a man, so that he die, shall be surely put
to death (Ex. 21:12).

And he that smiteth his father, or his mother, shall be surely
put to death (Ex, 21:15).

And he that stealeth a man, and selleth him, or if he be found
in his hand, he shall surely be put to death (Ex. 21:16).

And he that curseth his father, or his mother, shall surcly be
put to death (Ex, 21:17).

Whosoever lieth with a beast shall surely be put to death
(Ex, 22:19).

Ye shall keep the sabbath therefore; for it is holy unto
you: every one that defileth it shall surely be put to death:
for whosoever doeth any work therein, that soul shall be cut
off from among his people (Ex. 31:14).

Six days may work be done; but in the seventh is the
sabbath of rest, holy to the Lorn: whosoever doeth any work
in the sabbath day, he shall surely be put to death (Ex. 31:15).

Again, thou shalt say to the children of Israel, Whoso-
ever he be of the children of Israel, or of the strangers that
sojourn in Israel, that giveth any of his seed unto Molech;
he shall surely be put to death: the people of the land shall
stone him with stones (Lev. 20:2).

For every one that curseth his father or his mother shall be
surely put to death: he hath cursed his father or his mother; his
blood shall be upon him (Lev. 20:9).

And the man that committeth adultery with another man’s
wife, even he that committeth adultery with his neighbour's wife,
the adulterer and the adulteress shall surely be put to death (Lev.
20:10).

And the man that lieth with his father’s wife hath uncovered

72 VIGTIM’S RIGHTS

intends to pay every cent of the money to anyone who will tell him
the name of the person who kidnapped his son. He offers to pay
the accomplices to the crime. He reminds the kidnapper of the
risk of relying on the reliability of his accomplices. He then points
to the money and declares to the kidnapper, “This is as close to
this money as you'll ever get.” When he returns home, his neigh-
bors are outraged. They throw rocks through his window. He had
not shown filial piety. He deserves to be an outcast. But at the
end of the movie, his son is returned to him. The kidnapper was
fearful of being turned in for the reward.

What the movie’s hero did was to place a greater priority on
bringing the criminal to justice than he placed on public acceptance
of his act. (The statistical risk to his son, he had been told, was
the same, whether he paid the ransom or not.) By using the
ransom money in a unique way — as a reward that would increase
the likelihood of someone’s becoming an informant — the father
increased the odds in favor of his son’s survival. (The majority of
crimes are probably solved as a result of informants.) He relied
on the threat of punishment more than he did on the good will of
the criminal in honoring the terms of the transaction, his son’s life
for a cash payment. He turned to the law for protection, not to
the criminal’s sense of honor.

In 1973, the grandson of J. Paul Getty, one of the world’s
richest men, was kidnapped in Italy. The kidnapping received
worldwide attention. The kidnappers demanded over a million
dollars as the ransom.'® Getty publicly refused to pay. He said
that if he did, this would place his fourteen other grandchildren
in jeopardy. By not paying, he- said, he was telling all other
potential kidnappers that it was useless to kidnap any of his
relatives. The kidnappers cut off the youth's ear and sent it to his
mother. Still the grandfather refused. Privately, he lent $850,000
to the boy’s father to pay the ransom ~at 4 percent, of course.
Getty never missed an opportunity for profit.” The gamble paid

15, Edward Powell, “The Coming Crisis in Criminal Investigation,” Journal of
Christian Reconstruction, TT (Winter 1975-76), pp. 81-83,

16. ‘The price of gold was then about $100 an ounce,

17. Fellow billionaire industrialist Armand Hammer refers to him as “that tight

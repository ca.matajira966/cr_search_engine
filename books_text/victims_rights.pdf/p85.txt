Kidnapping 73

off the kidnappers released him.'® No other Getty relatives be-
came victims.!9

Equal Penalties or Equal Results?

The Bible does not forbid the victim’s family to pay a ransom,
but the threat of the death penalty makes the risk of conviction
so great that few potential kidnappers would take the risk, except
for a very high return, The average citizen therefore receives
additional but indirect protection because of this biblical law. The
penalty to the convicted kidnapper is so high that the money
which the middle-class victim’s relatives could raise to pay the
ransom. probably would not compensate most potential kidnap-
pers for the tremendous risk involved. Presumably, kidnappers
will avoid kidnapping poorer people.

In effect, the threat of the death penalty increases the likelihood that
members of very rich families or senior employees of very rich corporations
will be the primary victims of kidnappers. Also, in cases of politically
motivated kidnappings, the famous or politically powerful could
become the victims. They seem to be discriminated against eco-
homically by. biblical law: high penalties make it more profitable
for kidnappers to single their families out for attack. On the other
hand, these people possess greater economic resources, making it
more likely that they can more easily afford to protect themselves
and their relatives,

From the point, of view of economic analysis, the stiff penalty
for kidnapping protects society at large, though not always the
actual victim of the crime, and it protects the average citizen

 

old weasel.” Armand-Hammer (with Neil Lyndon), Hammer (New York: Putnam’s,
1987), p. 386. Hammer did respect him as an entrepreneur, however.

18, The grandson later suffered a stroke as a result of aleohol and drug abuse,
and is paralyzed and blind, Time (March 17, 1986), p. 80.

19. Thave instructed my wife never to pay a ransom for me under any conditions.
T have also told her that I will not pay a ransom for her or any of our children. The
goal is to reduce the risk of kidnapping before it takes place, not to increase the
likelihood of the victim’s survival. The evil of kidnapping should not be rewarded.
It should be made devastatingly unprofitable, The same should be true for terrorist
Kidnappings. ‘The policy of the state of Israel regarding terrorist kidnappings is
correct: a kidnapper-for-victims exchange before any victim is harmed, but no com-
promise thereafter.

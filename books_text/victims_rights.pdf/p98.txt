86 VICTIM'S RIGIITS

The mere possession of superior strength or combat skills is not
to be an advantage in the resolution of personal disputes.

We see a similar perspective in the Hittite laws: “If anyone
batters a man so that he falls ill, he shall take care of him. He
shall give a man in his stead who can look after his house until
he recovers. When he recovers, he shall give him 6 shekels of
silver, and he shall also pay the physician’s fee. If anyone breaks
a free man’s hand or foot, he shall give him 20 shekels of silver
and pledge his estate as security. If anyone breaks the hand or
foot of a male or a female slave, he shall give 10 shekels of silver
and pledge his estate as security.”| Men must pay the costs of
restoring the injured party to physical wholeness.

Winners and Losers

These cconomic restraints on victors remind men of the costs
of injuring others. Thcre are economic costs borne by the physical
confrontation’s loscr. There are also costs borne by society at
large. A man in a sickbed can no longer exercise his calling before
God. He cannot labor efficiently, and the products of his labor are
not brought to the marketplace. If he is employed by another
person, the employer's operation is disrupted. By forcing the physi-
cal victor to pay for both the medical costs and the alternative
costs (forfeited productivity on the part of the loser), biblical law
helps to reduce conflict. ‘The physical victor becomes an economic
loser. The law also insures society against having to bear the
medical costs involved. ‘lhe immediate family, charitable institu-
tions, or publicly financed medical facilities do not bear the costs.

The Mishnah, which was the legal code for Judaism until the
late nineteenth century, establishes five different types of compen-
sation, First, compensation for the injury itself, meaning damages
for permanent injury that results from the occurrence. Second,
compensation for the injured person’s pain and suffering. Third,
compensation for the injured person’s medical expenses. Fourth,

1. “The Hittite Laws,” paragraphs 10-12, in Ancient Near Eastern Texts Relating to
the Old Testament, edited by James B. Pritchard (3rd od.; Princeton, New Jersey:
Princeton University Press, 1969), p. 189. Paragraphs 13-16 continue the restitution
theme: monetary penalties for biting olf noses and cars of free men or slaves.

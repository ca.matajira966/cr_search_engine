Was Calvin a Theonomist?

millennial views, while continental Calvinism after
1700 adopted his amillennial elements. But there is
no doubt that his views on God’s sanctions in history
tended toward postmillennialism: the inescapable
triumph of covenant-breakers in history, before Jesus
returns in final judgment.

Meredith Kline vs. John Calvin

Calvinist theologian Meredith Kline, a consistent
amillennialist, is an equally consistent opponent of
Calvin's ethics-based social theory. He has fully un-
derstood the inescapable connection between Calvin’s
covenantal view of historical sanctions and postmillen-
nialism. He therefore rejects Calvin’s covenantal view
of historical sanctions. He adopts a view of ethical
cause and effect in history which is essentially ran-
dom — “largely unpredictable,” in his words — in the
name of the doctrine of common grace. “And mean-
while it [the common grace order] must run its course
within the uncertainties of the mutually conditioning
principles of common grace and common curse, pros-
perity and. adversity being experienced in a manner
largely unpredictable because of the inscrutable sov-
ereignty of the divine will that dispenses them in
mysterious ways.”!5 Compare this view with Calvin’s
view of non-random, providential history:

Thus you see how we may possess and en-
joy the blessings of God, which are set forth for
us in His law. And when we see that our Lord
interlaces these blessings with many afflictions
and corrections, as though He had cursed us,
we must realize that His purpose in this is to
provoke us day by day to repentance, and to
keep us from falling asleep in this present
world. We know that our pleasures make us
drunken and unmindful of God unless He con-
strains us by pricking and spurring us forward.
Thus you see how things that at first sight
seemed contraries agree very well in fact. And
in that respect does Moses say that these bless-
ings shall light upon us and encompass us
round about, as if he had said that we will

15. Meredith G. Kline, “Comments on an Old-New

Error," Westminster Theological Journal, XL1 (Fall 1978), p.
184.

16

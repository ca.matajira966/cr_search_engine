76 WESTMINSTER’S CONFESSION

giously necessary to require a Trinitarian oath in civil affairs.
This is Westminster’s confession.

By self-consciously abandoning the idea of a positive Chris-
tian confession in the realm of civil government, Christians have
actively participated in the de-Christianization of society. While the
State does not create society, it is a legitimate covenantal insti-
tution. It is an inescapable, God-mandated covenantal institu-
tion. Without the presence of a Christian confession for the
State, serving as it necessarily does as the third institutional
pillar of an expressly Christian socicty, there cannot be an
expressly Christian society. Those who deny the legitimacy of
Christian civil government understand this; they also self-cons-
ciously reject the idea of Christendom. Necessarily, they also accept
the idea of another law-order, another confession for the State,
and therefore for society, but they refuse to discuss its details.

In the days of the Westminster Assembly, such a confession
of another law-order was understood to be a confession for
another God; such a confession would have been unthinkable,
except in the distant North American colony of Rhode Island.
‘Today, the Westminster Assembly’s vision of confessional Chris-
tendom is unthinkable among Calvinists, except for the theono-
mists and Covenanters. The broad, international vision of the
Assembly has disappeared. What has taken its place?

A Degree of Confusion

Calvinism is known generally for its doctrine of the absolute
sovereignty of God. For most people, this means the doctrine
of predestination. The English-language acronym TULIP pres-
ents a clear, concise summary of Calvinism’s predestination:
Total depravity of man, Unconditional election by God, Limit-
ed (specific) atonement, Irresistible grace, and the Perseverance
of the saints. This is a five-point predestination model.

To a lesser extent, Calvinism is known as covenant theology.
For well over four centuries, no Calvinist theologian presented
a clear, concise, and biblically supported definition of what this

A Positive Biblical Confession Is Mandatory 81

He has not spoken with either of us for many years.* But this
is Rushdoony’s problem, not ours. It is also of no particular
theological benefit to theonomy’s critics.

The presence of personal rivalries within a denomination or
movement is not exactly front-page news these days. Machen
was not on close terms with fellow Presbyterian Robert E.
Speer. Van Til was not on close terms with fellow Presbyterian
Gordon Clark. Now let us look at dispensationalism. Charles
Ryrie is not on close terms with Dallas Seminary. John Whit-
comb is not on close terms with Grace Seminary. John MacAr-
thur is not on close terms with Zane Hodges. Bob Jones (any
number) is not on close terms with Billy Graham. Is Bob
Thieme on close terms with anybody? And then there is Con-
stance Cumbey, the sine qua non of “not on good terms.”

When you get down to it, Martin Luther was not chummy
with John Calvin. The division between them was expressly
theological: sanctions, the nature of the Lord’s Supper. Does
this call into question the legitimacy of the Protestant Reforma-
tion?

Judicial Inconsistency and Psychological Retief

Why, then, all the commotion among theonomy’s critics
about this or that intra-thconomic debate over the application
of a particular case law of the Old Testament? I think it has
something to do with the constancy of the theonomic vision:
our assertion that Old Testament case laws and their civil sanc-
tions still must be honored, unless there is a New Covenant

8. Several Christian leaders have attempted to get me and Rushdoony to sit
down and discuss our problems. I have in every case agreed, even flying to Wash-
ington, D.C., in 1981 to meet with him. He backed ont of his agreement when I
walked in the room, and he has refiused all mediation ever since, The mediator was
John Whitehead, whom I sent back again to get Rushdoony to agree. He failed. My
church has a large file documenting the many attempts, The latest allempt was
made by Dennis Peacock. I subsequently agreed to subordinate myself to a commit-
tee put together by Jay Grimstead, but he could get only one other Christian leader
to agree to sit on it, which I had told him would happen. He had to back out,

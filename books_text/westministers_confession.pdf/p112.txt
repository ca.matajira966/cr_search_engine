88 WESTMINSTER’S CONFESSION

stage of the conflict, Christians are fearful that the theonomists’
sounding of the trumpets was premature. Our humanist oppo-
nents are said to be too powerful; they could become aroused.
As dispensational author David Allen Lewis warns: “Unneces-
sary persecution could be stirred up.”!? Nevertheless, funda-
mentalists have begun to sound a similar alarm with respect to
public education. The home school movement has sounded an
alarm, and is now offensively engaged. Some of the anti-abor-
tionist groups have given up any further reliance on common-
ground, natural law defenses of the rights of the unborn. Bat-
defield by battlefield, Christians are responding to the trum-
pets. They are beginning to venture outside their ghettos.

Yet Westminster Seminary still refuses to sound the alarm.
Such an alarm would openly split the faculty, as Theonomy: A
Reformed Critique indicates. Worse, it might threaten funding.
The school still refuses to face up to the revolution that Van Til
hath wrought. Ic still cannot make up its mind: Jerusalem or
Athens. It desperately seeks an alternative. It continues to echo
the message that W. C. Fields is said to have had inscribed on
his tombstone: “Frankly, I’d rather be in Philadelphia.”

The Need for Confession

To say what Westminster Seminary is, we must identify what
it confesses. We can identify it by discovering what it believes
about the Bible. The editors of the symposium state that “The
Westminster Seminary tradition is one of academic freedom
within a framework of firm commitment to the authority of the
Bible and to our doctrinal standards as a faithful expression of
that truth.”” Academic freedom is as academic freedom does.
To put it bluntly, “tell it to Norman Shepherd!” But in any
case, there comes a time to put into action one’s formal com-

19. David Allen Lewis, Prophecy 2000 (Green Forest, Arkansas; New Leaf Press,
1990), p. 277.
20. Editors, “Preface,” Theonomy: A Reformed Critique, p. 11.

A Negative Confession Is Insufficient 105

national covenantal foundation. This is the central assertion of
the American Civil Religion.* It is the religion of Christian
American academics.’ It was a revolutionary idea in 1787-88.
No one on earth took any Church confession such as this seri-
ously in 1645, except in the tiny North American colony of
Rhode Island.

In 1643, Parliament faced a monumental crisis. England was
in the midst of the first modern revolution. William Haller
described this crisis in the late 1930’s. Religion was believed to
be central to the outcome of the Civil War and the nation.
“The question of how and in whose interest the church was to
be governed involved also the question of how and in whose
interests the loyalties and beliefs, the intellectual and spiritual
life, in a word the public opinion of the nation, were to be
directed.” In our day, the self-imposed, self-declared cultur-
al isolation and impotence of the Church is taken for granted;
not so in 1643,

Parliament called the Assembly in order to reorganize the
Church. What it should have done was to disestablish the
Church, thereby abandoning Erastianism. Had it done so,
there would probably not have been an American Revolution,
for in that later English civil war, colonial opposition to the

8 Russell E. Richey and Donald G, Jones (eds.), American. Civil Religion (New
York: Harper & Row, 1974); Sidney E. Mead, “American Protestantism During the
Revolutionary Epoch,” Church History, XXII (1953); reprinted in Religion in American
History: Interpretive Essays, edited by john M. Mulder and John F. Wilson (Englewood
Cliffs, New Jersey: Prentice-Hall, 1978); Mead, The Lively Experiment: The Shaping of
Christianity in America (New York: Harper & Row, 1963); Mead, The Nation With the
Soul of a Church (New York: Harper & Row, 1975); Robert N. Bellah, The Broken
Covenant: American Civil Religion in Time of Trial (New York: Crossroad. Book, Sea-
bury Press, 1975); Richard V. Pierard and Robert D. Linder, Civil Religion and the
Presidency (Grand Rapids, Michigan: Zondervan Academie, 1988).

9, North, Political Polytheism, ch. 5.

10. William Haller, The Rise of Puritanism (New York: Harper Torchbooks,
[1938] 1987), p. 374. I appreciate the book’s subtitle: Oy THE WAY OF THE NEW
JERUSALEM AS SET FORTH IN PULPIT AND PRESS FROM THOMAS CART-
RIGHT TO JOTIN LILBURNE AND MILTON, 1570-1643.

116 WESTMINSTER’S CONFESSION

intellectual and practical — facing the Christian world in gener-
al and Westminster Seminary in particular.

Without so much as a footnote, Van Til threw out political
pluralism’s baby with the Scholastic and Newtonian bath water,
yet Westminster Seminary simultaneously (1) parades itself as
a spiritual heir of Van Til and (2) defends the ideal of political
pluralism. This has been Westminster Seminary’s epistemo-
logical problem for a generation. If Van Til is correct, then
religious, intellectual, and political pluralism cannot possibly be
correct. Is Westminster Seminary going to abandon Van Til
publicly in favor of modern political pluralism, or scrap plural-
ism publicly and defend Van Til? Theonomy: A Reformed Critique
once again has dodged the issue.

This being the case, allow me to state the issuc one more
time. Civil law, at the very least, is the realm of negative sanc-
tions. These sanctions are physical or economic; they are also
compulsory (the “sword”). There is an inescapable principle in
all civil government: no sanction, no law. As the New England
Puritans put it in their 1647 law code, “The execution of the law
is the life of the law.”* In 1978, I put it this way in Appendix
4 of Institutes of Biblical Law, and I even put it in italics: “Of
covenant law is binding, then covenant law enforcement is equally
binding.”

Theonomy: A Reformed Critique is Westminster Seminary’s
long-delayed attempt to respond to the implications of this
easily understood statement, although no contributor cited it.
The reader must judge the competence of this response.
Westminster’s Confession is my attempt to assist the reader.

80. William S. Barker, “Theonomy, Pluralism, and the Bible,” in Theonomy: A
Reformed Critique.

31. “Book of the General Laws and Liberties Governing the Inhabitants of
Massachusetts, 1647,” in The Foundations of Colonial America: A Documentary History,
edited by W. Keith Kavenaugh, 8 vols. (New York: Chelsea House, 1978), I, p. 297.

32. North, “The Economics of Sabbath Keeping,” in R. J. Rushdoony, The
Institutes of Biblical Law (Nutley, New Jersey: Craig Press, 1978), p. 829.

The Question of Law 125

five-point covenant model.'° With respect to TULIP” (point
one of the biblical covenant model: God’s absolute sovereignty),
Westminster was a legitimate heir of Princeton: Westminster
was Calvinistic. With respect to point two, hierarchy, it was also
Princetonian, ie., Presbyterian. With respect to point three,
law, it was never made clear that a definitive break had been
made: from natural law theory to... ? Van Til never made
clear what he was substituting for natural law. His was an ex-
clusively negative judicial confession. This lack of clarity on the
question of civil justice is at the heart of today’s debate over
theonomy.” Point four — sanctions — was also a major transi-
tion: from the traditional Princetonian hope in God’s blessings
on the Church in history to a view that predicted escalating
cursings. Finally, eschatology: Westminster abandoned Prince-
ton’s traditional postmillennialism ~ the eschatology of Answer
191 of the Larger Catechism. So, in three crucial respects —
law, sanctions, and eschatology - Westminster Seminary be-
came a Dutch enclave within American Presbyterianism. The
Old Princeton really did perish in 1929. It left no heirs, either
theologically or institutionally. Today’s Calvinistic postmillen-
nialists are virtually all Vantilian in their apologetics: the Chris-
tian Reconstructionists. Yet on the question of law, the theono-
mists are neither Vantilian nor Princetonian: they are neo-
Puritan.

Two Views of the State

Francis Schaeffer asked: How Should We Then Live? This is
the question! He never provided an answer. Neither has any
theological seminary since the demise of Princcton. If there is

10. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987).

11, See above, pp. 76-77.
12. Gasy North, Viclim’s Rights: The Biblical View of Civil Justice (Tyler, Texas:
Institute for Christian Economics, 1990).

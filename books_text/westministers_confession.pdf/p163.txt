The Question of Law 139

be no such thing as the salvation of society apart from the
salvation of the individuals constituting society; . . .” But he
had denied that this widespread salvation of souls will ever take
place in history. Therefore, society will not be saved (ie.,
healed). Clowney saw the implications of this statement, and he
did not shrink from announcing them, although carefully
shiclding himself from critics by adopting the pejorative word
“sacralized” for the biblical word “healed”: “The world cannot
be sacralized by the fiat of the new theology to form the com-
munity of love Christ came to establish. The world lacks the
new life of the Spirit who sheds abroad the love of Christ in
human hearts. It cannot be governed by the spiritual structure
of Christ’s kingdom.” This shifts aif of the Christian’s coven-
antal-institutional concern to the Church and away from poli-
tics. (Note: What ever happened to the family?) “The politics of
the kingdom demand that Christians take seriously the struc-
ture of the church as the form of the people of God on
earth.”*°

In short, Clowney was preaching the non-politics of the king-
dom. But a kingdom in history without civil sanctions is not a
civilization; it is merely a ghetto, What Clowney was really
preaching was the non-politics of the non-kingdom. This is TULIP
pietism: the sovereignty of a God without judicially predictable
sanctions in history. This is predestined cultural impotence.

Christians: Devoid of Unique Skills

He assured his readers that “Christ has not promised to
make us wise in world politics, skillful in technology or talented
in the arts." No? Then He has surely short-changed His
Church, for this is exactly what He did for the Israelites as they

49, Edmund P, Clowney, “The Politics of the Kingdom,” ibid, XLI (Spring
1979), p. 309.

BO. Idem.

51. Idem,

The Question of Law 141

“And I appoint unto you a kingdom, as my Father hath ap-
pointed unto me; That ye may eat and drink at my table in my
kingdom, and sit on thrones judging the twelve tribes of Israel”
(Luke 22:29-30). No, that office of judge is not what Clowney
had in mind, “The military and police power needed to main-
tain a political community in this world cannot be sought in
Christ’s name.”* It can’t? To which theonomists ask: In whose
name must it be sought? Silence, Endless, self-conscious silence.
Endless, self-interested silence. And so we are back to square
one covenantally: By what standard? In whose name? Under
whose authority? Taking an oath to whom? There can be no
neutrality. This is what Van Til taught. Bui Edmund Prosper
Clowney was never a follower of Van Til. He was a defender,
implicidy but inescapably, of natural law theory. He insisted:
“In judging the good or evil performance of the state the
Christian may not, however, judge the state as a form of the
people of God but only as an ordinance given to all men to
preserve life. The distinction between the state as the form of
the city of this world and the church as the form of the heaven-
ly city remains essential.”°*

Redefining Kingdom and Church

What part does the family play in all this? His words are
clear: “The church and only the church is established by Jesus
Christ as the earthly form of the new and heavenly people of
God.”*? The theonomist asks: Is the family also only “an ordi-
nance given to all men to preserve life”? Clowney answers in
the affirmative: “The family remains as the institution of God
for the propagation of life; . . .”** This is, of course, the old
Roman Catholic definition of the family. Clowney’s definition

5B. Idem.
56. Ibid. p. 306.
57. Idem.
88. Idem.

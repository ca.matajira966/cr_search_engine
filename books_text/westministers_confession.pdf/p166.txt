142 WESTMINSTER’S CONFESSION

is self-consciously removed from the traditional Reformed
emphasis on the family as the primary agency of dominion in history.
It places Christian families outside the definition of the king-
dom of God, i.e., it removes the family from the list of cove-
nanted institutions. Only the Church is lawfully a covenanted,
oath-bound institution under Bible-revealed laws and sanctions.

We must pay close attention to Clowney’s use of “body of
Christ” - the familiar definition of God’s Church — and the
kingdom of God in history. He winds up equating Church and
kingdom, and then he removes all traces of the kingdom from
anything outside the institutional Church, This is the standard
pietist definition of Church and kingdom. It has not been the
historic Reformed definition. Substitute the word “family” for
“state” in this sentence, and see what effect it has on the defini-
tion of the kingdom of God in history: “To suppose that the
body of Christ finds institutional expression in both the church
and the state as religious and political spheres is to substitute a
sociological conception of the church for the teaching of the
New Testament. ... The church is the new nation (I Pet. 2:9),
the new family of God (Eph. 3:15).”

The theonomist asks: Does this mean that families are not to
be judged in terms of biblical law? Does this mean that the laws
of divorce are neutral, universal, and outside of biblical law’s
requirements? Does this mean that civil laws against polygamy
are wrong? Does this mean that civil laws against sons marry-
ing their widowed mothers are wrong? Clowney knows that
this list of questions follows from his presentation, so he hurries
to escape the obvious trap. He brings the family back into the
kingdom, sort of. He asserts, with no proof, that “The family,
as a form of God’s creation, is restored in relation to the
church in a way that the state, an institution made necessary by
the fall, is not.” Yet he immediately insists: “In God’s kingdom
there is restoration of creation, fulfillment of the ordinances of

59, Idem.

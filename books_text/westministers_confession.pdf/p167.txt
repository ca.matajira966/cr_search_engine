The Question of Law 143

God for a fallen world, and anticipation of the new crea-
tion.”®° So, the State is not to be restored, but the family is.
To which the theonomists say: this is a presupposition, not a
conclusion, of pictism’s rejection of the oath-bound legal status
of the civil government. What we need is biblical evidence.

He then writes: “Yet even the family is not identified with
the new order of the kingdom.”*! What does this mean? Ex-
actly how is it different in this regard from the State? For that
matter, how is it different from the Church in this regard?
Would any Reformed scholar argue that the institutional
Church is to be identified with the kingdom of God in history?
None that I know of. Yet this is where Clowney’s argument
logically leads. But he is clever. He has read Vos; he knows
that his identification of institutional Church and kingdom is
not a biblical argument. So he ends his discussion at this point.
He moves on, leaving confusion in his wake.

He has clearly and self-consciously broken with Vos’ view of
the kingdom, which he described in the final paragraph of his
book on Church and kingdom. Vos wrote:

Finally, the thought of the kingdom of God implies the sub-
jection of the entire range of human life in all its forms and
spheres to the ends of religion. The kingdom reminds us of the
absoluteness, the pervasiveness, the unrestricted dominion, which of
right belongs to all true religion. It proclaims that religion, and
religion alone, can act as the supreme wnifying, centralizing factor in
the life of man, as that which binds all together and perfects all
by leading it to its final goal in the service of God.”

Van Til always said that he derived much of his theology
from Vos. It is clear how Van Til could have come to his

60. Ibid., p. 307.
GI. Idem,

62. Geerhardus Vos, The Teaching of fesus Cancerning the Kingdom and the Church
(Grand Rapids, Michigan: Eerdmans, 1958), p. 103.

63. William White, Van Til: Defender of the Faith (Nashville, Tennessee: Nelson,

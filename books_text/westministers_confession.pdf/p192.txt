168 WESTMINSTER’S CONFESSION

tive tradition in the history of the church (and certainly the
Reformed tradition) has always insisted. . . . Or, alternatively,
will the Old Testament, particularly prophecies like Isaiah 32:1-
8 and 65:17-25, become the hermeneutical fulcrum?"

What of this initial presupposition, namely, that the New
Testament teaches suffering and cultural defeat for the prose-
cutors of God’s covenant lawsuit - the gospel of Jcsus Christ -
throughout history? Can this claim be substantiated exegetic-
ally? No. But it has been repeated so often in the twentieth
century that most Calvinistic Christians probably think that it
can be and has been substantiated exegetically. This is because
they are unfamiliar with the Anglo-American Calvinist tradi-
tion. They do not recognize the Continental accents of those
Calvinistic theologians who articulate eschatology today.

Progressive Sanctification

Because this doctrine is so often ignored by Christians, espe-
cially those few who bother to comment on the covenantal
meaning of New Covenant history, I need to remind the read-
er of the biblical doctrine of sanctification. God grants judicially
the perfect humanity of Christ to each individual convert to sav-
ing faith in Christ. This takes place at the point of his or her
conversion. Subsequently, this implicit, definitive moral perfec-
tion is to be worked out in history, We are to strive for the
mark. We are to run the good race (strive to win it, by the way;
not to hope for a covenantal tie, ic., pluralism).* We are to
imitate Christ’s perfect humanity, though of course not His
divinity, which is an incommunicable attribute.

The doctrine of definitive sanctification, if taken by itself,
would mean that a redeemed individual is perfect. Certain

3. Richard B. Gaffin, Jr. “Theonomy and Eschatology: Reflections on Postmil-
lennialism,” in Theonomy: A Reformed Critique, pp. 216-17.

4. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989).

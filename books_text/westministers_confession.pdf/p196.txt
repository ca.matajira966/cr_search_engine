172 WESTMINSTER’S CONFESSION

formulations will parallel and reinforce the maturation of the
Church? Finally, will such maturation have positive effects in
society? If not, then are you saying that the progress of the
Church and the creeds is socially irrelevant? Please be specific.
And when you have got your answers ready, don’t forget to
discuss them with your students. Perhaps some of them may
remind you of this assignment periodically. They do pay your
salary.

Let us continue, this time with the family. The marital vows
are definitive. The working out of these vows in the lives of a
married couple is progressive: love, honor, obey, cherish, etc.
Are we to say that an older couple has in no way matured cov-
enantally since their wedding day? No. But does this in any
way denigrate the integrity of those original vows, taken so
long ago? No. The vows were definitive. The covenantal process
of both personal and corporate maturing in terms of these
vows is progressive. This is so clear that even seminary profes-
sors ought to be able to understand it. They won't, of course.
They acknowledge dual sanctification with respect to the indi-
vidual Christian, but as soon as you raise the possibility that
sanctification in both aspects also applies to institutions, you get
a blank stare — what we might call blank stare apologetics. (If
pressed, the professor might respond, “I see.” He doesn’t.)

Maturation Beyond the Cloister and the Family

Now, let us get to the. heart of the matter: the application of
biblical law and its sanctions to the world outside of the institu-
tional Church and the family, leading progressively to the
triumph of Christendom in history. Here is where the pietist
gags. The pessimillennialist cannot tolerate the suggestion that
the same principle of definitive and progressive sanctification
applies to Christian societies, despite the fact that it applics to
the Church and to the Christian family. What biblical principle
do they invoke to prove the existence of such an interpretive
discontinuity between the world outside Church and family

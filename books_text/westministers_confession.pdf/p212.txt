188 WESTMINSTER’S CONFESSION

the direction of covenantal postmillennialism, which offers a
consistent and Bible-based alternative. Gaffin’s amillennialism
of pre-1940 Holland cannot compete effectively against it.

Naturally, the amillennialists at Westminster - as far as I can
tell, this means the entire faculty - believe that amillennialism
is quite serviceable. But there is a problem. They have not yet
begun to articulate the kind of social theory that amillennialism
produces. Deuteronomy 28 provides the Christian Reconstruc-
tionist with the judicial foundation of social theory. It presents
the case for God’s predictable historical sanctions. It offers
hope to covenant-keepers regarding the long-term efficacy of
their efforts, on earth and in history, In short, it offers them
the possibility of transferring to their covenantal heirs the
judicial foundations for building Christendom. But amillen-
nialists deny the New Testament reality of Deuteronomy 28
and its sanctions. They deny that, over time, covenant-keeping
produces victory. They offer to their spiritual heirs only the
prospects of assured defeat in history. They offer them the
sociology of suffering. Theonomists also proclaim a sociology of
suffering. Hlowever, we proclaim it to covenant-breakers. This
makes all the difference.

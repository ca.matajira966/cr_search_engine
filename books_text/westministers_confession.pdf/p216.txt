192 WESTMINSTER’S CONFESSION

Aramaic, and Ames’ Marrow of Sacred Divinity would save
them.* They lost the bet. Harvard went downhill, generation
by generation, until the climax in 1805, when Unitarian Henry
Ware, Sr., was appointed professor of divinity.? After that, the
Unitarians took over. Yet no one could enter the preaching
ministry in Puritan New England who was not a Harvard,
Cambridge, or Oxford graduate, except for a few men in the
howling wilderness of Maine and New Hampshire.” The re-
sult is described well by Westminster’s Samuel Logan: “The
Calvinist tulip was transformed first into an Arminian dandeli-
on and then into a Unitarian ragweed. .. .*"

In 1812, Princeton Theological Seminary was begun in
order to train men for the ministry, since their undergraduate
educations could no longer be trusted theologically. From the
beginning, then, the theological seminary was a stopgap, defen-
sive measure. Yet this most conservative of all theological semi-
naries adopted Scottish common sense realism as its apologetic
foundation — precisely the system that Harvard College was
proclaiming.” ‘This tradition ended at Princeton only with
Van Til’s appointment in 1928, and the next year, he left
Princeton and joined the Westminster faculty.

Can you imagine Martin Luther’s insisting that all candi-
dates for the Protestant ministry first be granted a degree from
the Pontifical Institute in Rome? Yet this is approximately what

8. David D. Hall, The Faithful Shepherd: A History of the New England Ministry in
the Seventeenth Century (Chapel Hill: University of North Carolina Press, 1972), p.
178.

9. Norman Fiering, Moral Philosophy at Seventeenth Century Harvard (Chapel Hill:
University of North Garolina Press, 1981}; Danicl Walker Howe, The Unitarian
Conscience: Harvard Moral Philosophy, 1805-1861 (Middletown, Connecticut: Wesleyan
University Press, [1970] 1988). Published originally by Harvard University Press.

10, Hall, Shepherd, p. 183. Lay preaching was made illegal by the General Court
in 1652: Hall, p. 184.

11. Samuel T: Logan, Jr, “Where Have All the Tulips Gone?” Westminster
‘Theological Journal, L (1988), p. 2.

12, Towe, The Unitarian Conscience, ch. 1.

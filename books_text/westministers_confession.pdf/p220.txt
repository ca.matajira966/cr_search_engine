196 WESTMINSTER’S CONFESSION

answer the crucial question: “What is to be done?” His lack of
specifics is the same problem I face in explaining his essay,
“May We Use the Term Theonomy?” The essay contains not one
footnote, not one reference to Bahnsen, and not one mention
of Rushdoony.

Dr. Knudsen’s chief problem in writing about theonomy is
that he just does not know what to believe about law. I can
understand this; he spent his career immersed in Dooyeweerd’s
New Critique of Theoretical Thought. (In the original Dutch!) He
writes of the Ten Commandments: “These are indeed com-
mandments, but they are not formulated in legal terms. It is
not stipulated exactly what would constitute keeping them or
transgressing them, or exactly what the rewards and punish-
ments might be.” This is vague enough to satisfy every state-
licensed abortionist in America. (To the reader: keep the word
“abortion” in the back of your mind as you read these essays
on what Christian ethics supposedly does not require us to
believe.}

How Should We Then Live?

Well, then, even the marginally inquisitive student might
ask, how did the ancient nation of Israel know what to do?
More to the point from the perspective of civil law, how did its
residents know what they were prohibited from doing? They
must have turned to the case laws that follow Exodus 20. This
is Rushdoony’s thesis in /nstitutes, but Knudsen does not men-
tion Rushdoony. This is also my thesis in Tools of Dominion,
which appeared far too late for Knudsen also to ignore.

Ah, but Knudsen has a unique solution to this problem. He
defines Christian responsibility, not in terms of knowing what
to do, but in nef knowing what to do. “In all their relationships
New Testament believers do not have less responsibility than

16. Theonamy: A Reformed Critique, p. 21.

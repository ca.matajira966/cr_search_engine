Sic et Non: Judicial Agnosticism 197

their Old Testament counterparts for obeying God’s will as
expressed in his law; in fact, they have greater personal res-
ponsibility, because it is not legally stipulated exactly what they
should and should not do.”"’ The Israelites knew what was
expected; Christians don’t. They had less responsibility; we
have more. In other words, “from him to whom less has been
given, more is expected,” and vice versa. This is the opposite of
what Luke 12:47-48 teaches, but as I said before, Dr. Knudsen
has immersed himself in Dooyeweerd’s books. The more of this
he got, the less anyone ever expected from him or received
from him (e.g., footnotes).

Amusing as all this may be, it brings us to the grim reality of
Knudsen’s view of civil law. This view of civil law is a perfect
prescription for tyranny. In the civil realm, if the State is not
limited by law in what it can and cannot legitimately do, then
the nation becomes subject to the whims of the leaders and the
bureaucrats. This was the lesson I learned in Knudsen’s class
when he assigned C. S. Lewis’ novel, That Hideous Strength,
which literally changed my life. But Knudsen obviously does
not understand what Lewis was getting at. He does not see the
threat posed by any civil government that is authorized to
rehabilitate criminals at will, but is not forced to specify specific
punishments for specific crimes. The evil policewoman of the
novel, Fairy Hardcastle, did understand:

“You've got to get the ordinary man into the state in which he
says ‘Sadism’ automatically when he hears the word Punish-
ment.” And then one would have carte blanche.'*

17. Bid, p. 94.

18. C. S. Lewis, That Hideous Strength: A Modern Fairy-Tale for Grown-Ups (New
York: Macmillan, 1946), p. 69. Lewis expands on this theme in his essay, “The
Humanitarian Theory of Punishment,” in God in the Dock: Essays on Theology and
Ethics, edited by Walter Hooper (Grand Rapids: Michigan, 1972).

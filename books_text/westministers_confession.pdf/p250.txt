226 WESTMINSTER’S CONFESSION

honest, and industrious citizens, and the obedient subjects of a
lawful government.”*” Here is common-ground religion with a
vengeance: the Church of Jesus Christ is reduced to the equiv-
alent of a cheering section at a football game in which it may
not morally or legally compete. It is clear why Barker appeals
to the 1789 position paper of the Presbyterian Church rather
than to the declarations of the Westminster Assembly.

Barker gives as an example of the illegitimacy of the State’s
interference with religion “the requirement of prayer or acts of
worship in the public schools.** Fine and dandy. We
theonomists agree. So did Machen. The two fundamental edu-
cational questions that the theonomist raises are these: (1) By
what biblical standard can anyone defend the legitimacy of
State-funded, State-controlled education? (2) What has hap-
pened to the educational responsibilities of the family?**
These questions do not even occur to Dr. Barker, or if they do,
he suppresses them. Rushdoony asked them as long ago as
1961 in his book, Inéellectual Schizophrenia. He pursued the
theme in his Messianic Character of American Education (1963).
But there are no references to Rushdoony in Barker’s chapter.

Barker says that the State should not promote a specific
religion, namely, Christianity. We theonomists agree.) We
maintain that civil law is used to suppress evil public acts, not
promote the general welfare, including the general religious
welfare. The State is to bring God’s specified negative sanc-
tions. But this is not the focus of Barker’s essay. He wants the
State to promote the general welfare, He just does not want it
to promote Christianity.

87. Cited by James Harris Patton, A Popular History of the Presbyterian. Church in
the United States of America (New York: Mighill, 1988), p. 209.

88. Barker, p. 289.

89. Robert L. Thoburn, The Childsen Trap: The Biblical Blueprint for Education (Ft.
Worth, Texas: Dominion Press, 1986), Thoburn holds the Th.M. from Westminster.

90. Barker, p. 232.

91. See Bahnsen’s essay in Theonomy: An Informed Response.

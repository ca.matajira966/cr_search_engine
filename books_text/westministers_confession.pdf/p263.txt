Abusing the Past 239

newsletter, Calvin Speaks, published by Geneva Ministries from
1980 to 1984, Sawyer’s thesis also refers to Calvin Speaks.’ God-
frey was remiss in not discussing the sermons at length, since
their very existence refutes his thesis of Calvin as a theologian
who systematically opposed the Old Testament’s civil sanctions.
Sometimes Calvin did oppose them, and sometimes he didn’t.

Godfrey shows that Calvin believed in a Christian State, in
which the magistrate brings sanctions against heresy. Calvin
believed in the State’s enforcement of all ten commandments.’
Already, one thing is sure: Calvin was surely not a 1788 Ameri-
can Revised Westminster Confession man!

Calvin rejected the Old Testament's civil laws as no longer
binding on the New Testament civil magistrate, Godfrey says.
He is correct, But on what basis could Calvin argue this way?
This is the old theonomic question: “If not biblical law, then
what?” Godfrey knows full well how Calvin argued, and he
cites the Institutes: Book IV, Chapter XX, Section 16. This is
Calvin’s defense of natural Jaw theory. “The law of God which
we call the moral law is nothing else than a testimony of natu-
ral law and of that conscience which God has engraved upon
the minds of men. Consequently, the entire scheme of this
equity of which we are now speaking has been prescribed in it.
Hence, this equity alone must be the goal and rule and limit of
all laws.”*

Godfrey makes it plain, at least with respect to Calvin's
Institutes, that “Calvin uses the law of nature to criticize the law
of Moses and declare it morally inferior.”° This is why Rush-
doony was so explicit in his rejection of Calvin on this point.

6. Jack W, Sawyer, “Moses and the Magistrate: Aspects of Calvin’s Political
Theory in Gontemporary Focus,” Westminster Theological Seminary, Th.M. thesis
(1986), p. 83n.

7. W. Robert Godlrey, “Calvin and Theonomy,” Theonomy: A Reformed Critique,
pp. 800-3.

8. Gited in ibid., p. 303.

9. Tid., p. 308.

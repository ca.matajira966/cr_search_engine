248 WESTMINSTER’S CONFESSION

It is worth noting that Dr Logan does not reproduce this crucial
list in his essay. He only talks mentions it briefly.” Reading it
makes ail the difference. It shows that the Puritans were essen-
tially theonomic in their view of capital crimes, given what was
believed by all of non-Puritan Christendom regarding civil law
in the mid-seventeenth century. But Logan writes that “this
should not be taken to mean that Massachusetts Bay now had
a law code.”"* This is an odd argument. Please follow his justi-
fication for making such a statement. He quotes Haskins.

As Haskins points out, “The Body of Liberties was less a code of
existing Jaws than it was a compilation of constitutional provi-
sions. . . . [Note: the three extra dots are Logan's - G.N_]
Viewed as a whole, it resembles a bill of rights of the type which
was later to become a familiar feature of American state and
federal constitutions.”**

Citing Haskins is crucial at this point, for Haskins seems to
buttress Logan’s argument that the 1641 legal code was not
yeally a legal code. Because this code was visibly theonomic,
Logan has to call into question its historical authority. He cites
the fact that the legislators continued to work on the codifica-
tion project. He thereby seeks to persuade the reader that the
1641 code was not all that significant. But it was significant.

There is a major problem with Logan’s argument: Haskins’
statement is being misused. Haskins was not arguing that these
laws were not laws. He was arguing only that taken as a whole,
they did not constitute a code in the structural sense because of
their lack of order. Logan knows this, which is why he left out
Haskins’ crucial explanatory statement by “three dotting” it.
(When you find a controversial quotation with three dots in the
middle of it, history graduate students are taught, check the

38. Logan, p. 881.
34. Ibid., p. 373.
35. Ibid., pp. 373-74.

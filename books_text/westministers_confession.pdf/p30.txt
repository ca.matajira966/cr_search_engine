6 WESTMINSTER’S CONFESSION

explicitly biblical alternative to the natural law theory that he so
thoroughly refuted."!

“Why Are You So Mean?”

Some critics (and even a few supporters) of theonomy pro-
fess astonishment and public consternation at my style of res-
ponding to published critics. They say that I have treated
critics in print as if they were liars, buffoons, and theological
incompetents. I have taken this approach self-consciously,
primarily because these critics have been liars, buffoons, and
theological incompetents. Anyone who reads Hal Lindsey’s
attempt to tar theonomists with anti-Semitism can sce what I
am talking about.” When the best-selling Christian author of
this generation informs his followers regarding the Christian
Reconstructionist movement, “This is the most anti-Semitic
movement I’ve scen since Hitler,” what is the proper res-
ponse?™ (It occurs to me that Hal Lindsey has had only one
fewer wife than Westminster Seminary has had published sym-
posiums. My public references to Lindsey’s marital status are
regarded by some of his fundamentalist followers as far more
damaging to me than his divorces are to him. Fundamentalist
priorities are sometimes ethically peculiar.)

What very few Christians recognize today is that direct con-
frontation through verbal abuse was basic to the Protestant
Reformation; indeed, it has been basic to the whole history of
Church doctrine. Few readers today are familiar with Luther’s

11, Gary North, Political Polyiheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989), ch. 3.

12, Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989). See Gary
DeMar and Peter J. Leithart, The Legacy of Hatred Continues (Tyler, Texas: Institute
for Christian Economics, 1989).

13, Lindsey, “The Dominion Theology Heresy,” audiotape #217, This is a
review of David Chilton’s Paradise Restored (Ft. Worth: Dominion Press, 1985).

14. Gary North and Gary DeMar, Christian Reconstruction: What It Is, What Is Isn't
(Tyler, Texas: Institute for Christian Economics, 1991), Question 13: “What Is the
Proper Response?”

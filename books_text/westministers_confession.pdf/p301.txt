An Editor's Task: Just Say No! 277

name of the latest scholarship at Gordon-Conwell Seminary
and Westminster.

Why was Joseph a bringer of God’s blessings, according to
Keller-Clowney? Their implicit answer: because he was a State
bureaucrat. Why is price gouging wrong? Apparently, only
because it is a private, voluntary transaction. I ask: What is
rent-gouging? How can it be defined, either biblically or econ-
omically? Yes, an owner sometimes raises the rent. This is
because he thinks that another would-be renter is willing to
pay him more money. Keller does not understand this funda-
mental principle of free market pricing: renters compete against
renters, while owners compete against owners.

Why is it wrong for a house owner to accept an offer from
someone to rent his house or apartment at a rent that another
renter is unwilling to pay? Why blame the house owner for
gouging? Why not blame the new renter as a “cut-throat com-
petitor” against the original renter? Why does the principle of
“high bid wins” outrage Keller in this case, but not when Jo-
seph honored it by enslaving the Egyptians? Because in this
case, a private individual is making money. Keller offers us no
other way to distinguish the two kinds of pricing.

My point in my Genesis commentary was this: when the
State has a monopoly, tyranny is always a threat. But when one
renter bids against another to rent scarce space from one house
owner among thousands, there is nothing remotely question-
able morally about allowing the owner to rent to the highest
bidder. Renters compete against renters. It is an auction process
for allocating scarce spacc. Keller, Protestant Scholastic that he
is, does not understand this. To prove that rent-gouging exists
and is immoral, he cites Richard Baxter, who wrote three hun-
dred years ago, and who offered no Bible verses to support his
position. Baxter, of course, was a Protestant Scholastic (e.g., A
Christian Directory). I guess this persuaded the editors.

tian Economics, [1974] 1988), ch. 2.

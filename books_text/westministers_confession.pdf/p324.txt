300 WESTMINSTER’S CONFESSION

ward. They have gone to the trouble of working out a road
map with what they perceive as the biblical signposts. Their
theology tells them where we theonomists have departed from
the pathway of civil righteousness. Liberty University philoso-
phy professor Norman Geisler is clear about the nature of our
deviation: we have abandoned natural law theory and the
doctrine of the Rapture. He understands the inescapable bur-
den of the person who rejects biblical law in the name of Chris-
tianity: to put forward an alternative concept of civil law, i.e.,
natural law. He has been willing to do this. Unlike the faculty at
‘Westminster Seminary, he is not attempting to beat something
with nothing specific. The demonstration that this proposed
dispensational alternative cannot stand the test of biblical revel-
alion and biblical philosophy was Van Til’s legacy to the
Church in general and Reformed theology specifically. But at
least they have identified the primary area of disagreement:
natural law. They have denied that Van Til was correct on this
point, and have then challenged our view of revealed law with
an appeal to traditional natural law theory.

Our response to these fundamentalist critics is three-fold,
First, where is your line-by-line refutation of Van Til? Where is
there a book that demonstrates that Van Til was wrong about
natural law theory? To go about one’s philosophical business
on the assumption that Van Til was wrong, but without public-
ly answering Van Til, is not sufficient. “But sanctify the Lord
God in your hearts: and be ready always to give an answer to
every man that asketh you a reason of the hope that is in you
with meckness and fear” (J Pet. 3:15). This includes one’s hope
in the earthly future.

Second, where is your body of published materials that shows
how Christianity affects social theory and social policy? What is
the distinctively Christian contribution to Stoic natural law
theory that would make society Christian? In what ways will
Roman Catholic social theorists in the Scholastic tradition differ
from Protestants who adopt the medieval synthesis of Bible and

Introduction 9

collapsing social order. Calvinism, which once called the West
to repentance and sought to restructure Western Civilization,
is nearly forgotten. Because Calvinism’s proponents have at-
tempted to adopt contemporary academia’s alien rhetorical
standard, its opponents have been safely able to ignore it. In its
most toothless and feckless form, Calvinism enlists that under-
funded academic curiosity, the theological seminary. No one
pays much attention. It was not Westminster Seminary, Re-
formed Seminary, Calvin Seminary, or Covenant Seminary that
called forth Lindsey’s The Road to Holocaust, Hunt’s Whatever
Happened to Heaven?, House and Ice’s Dominion Theology: Bless-
ing or Curse?, and Dager’s Vengeance Is Ours. It was my rhetoric
and my publishing money that did. To make an impact, you
have to put your money where your mouth is, and it helps to
have, a loud mouth.

The defenders of the mild-mannered, Clark Kent approach
to theological debate call their approach irenic. The word
means peaceful or non-polemical. The Oxford English Dictionary
cites Schaff’s Encyclopedia of Religious Knowledge: “Irenical Theol-
ogy, or Irenics . . . presents the points of agreement among
Christians with a view to the ultimate unity . . . of Christen-
dom.” While there are periods in Church history when the
issues have not been sorted out, the goal of orthodoxy has to
be the elimination of all false theological opinion in the long
run. But those theological doctrines that are regarded at all
times as fundamental threats to the faith must not be dealt with
irenically. They must be challenged root and branch ~ an old
Puritan phrase. They must also be challenged rhetorically. An
irenic approach is completely inappropriate in such cases. But
academic Calvinists cannot grasp this. For them, irenics is not a
temporary tactic; it is a way of life. The character in literature
who is the embodiment of this way of life is Dr. Pangloss in
Voltaire’s Candide.

Why are modern Calvinists, of all theologians, irenic? Be-
cause they have begged at the tables of their enemies for so

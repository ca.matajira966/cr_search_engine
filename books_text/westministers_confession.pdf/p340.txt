316 WESTMINSTER’S CONFESSION

on the contrary, a corpus of powerful and profound convic-
tions, many of them not open to logical analysis. Its inherent
improbabilities are not sources of weakness to it, but of
strength. It is potent in a man in proportion as he is willing to
reject all overt evidences, and accept its fundamental postu-
lates, however unprovable they may be by secular means, as
massive and incontrovertible facts.

These postulates, at least in the Western world, have been
challenged in recent years on many grounds, and in conse-
quence there has been a considerable decline in religious belief.
There was’a time, two or three centuries ago, when the over-
whelming majority of educated men were believers, but that is
apparently. true no longer. Indeed, it is my impression that at
least two-thirds of them are now frank skeptics. But it is one
thing to reject religion altogether, and quite another thing to
try to save it by pumping out of it all its essential substance,
leaving it in the equivocal position of a sort of pseudo-science,
comparable to graphology, “education,” or osteopathy.

That, it seems to me, is what the Modernists have done, no
doubt with the best intentions in the world. They have tried to
get rid of all the logical difficulties of religion, and yet preserve
a generally pious cast of mind. It is a vain enterprise. What
they have left, once they have achieved their imprudent scav-
enging, is hardly more than a row of hollow platitudes, as
empty as [of] psychological force and effect as so many nursery
rhymes. They may be good people and they may even be con-
tented and happy, but they are no more religious than Dr. Ein-
stein. Religion is something else again — in Henrik. Ibsen’s
phrase, something far more deep-down-diving and mud-
upbringing, Dr. Machen tried to impress that obvious fact
upon his fellow adherents of the Geneva Mohammed. He failed
~ but he was undoubtedly right.

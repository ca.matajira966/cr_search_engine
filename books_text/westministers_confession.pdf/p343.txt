Honest Reporting as Heresy 319

This would serve as a good introductory statement of the
Christian Reconstruction movement's view of civil and ecclesias-
tical authority. Reid goes on to say that

Calvin believed that the church's form of government was to be
fundamentally democratic. In this way it served as a pattern for
the state to imitate, He did not believe that ministers and other
church officials should be imposed on the church by the civil
government or by a small group of wealthy or aristocratic indi-
viduals. Instead, he believed that ministers, elders, and deacons
should be appointed by the people of the church as a whole.

This is the Reconstructionists’ outlook, too,

The Colonial American Puritans

Now, let us take a quick look at the Puritan experiment in
colonial America. The first small group arrived in 1629 in what
was to become the Massachusetts Bay Colony, and a larger
group arrived the next year. They immediately established the
General Court of the colony, which quickly became a legisla-
ture (1634), with deputies elected by the townspeople. The
legislature met together, and was made up of two parts: assis-
tants sclected by the governor (an elected official) and the
deputies, who were elected by the townspeople. Each had to
reach a majority decision for a law to be enforced. In 1644,
ibey set up a true bicameral legislature, as a result of a division
between assistants and deputies over who really had owned a
dead pig. (I someday intend to write an article called, “The Pig
that Shaped American Constitutional Law.”)

The Puritans believed in the rule of law ~ biblical law - but
also in political representation (an aspect of the Calvinistic
doctrine of the covenant). It is often argued that the first con-
stitution ever written by citizen’s representatives to create a
new government was the “Fundamental Orders of Connecti-
cut” (1639); this was followed shortly by the “Body of Liberties”

Honest Reporting as Heresy 327

thin vencer of humanist respectability. Christianity will have its
teeth removed. It will be nourished only by the baptized pabu-
tum of humanism.

This is what Christianity Today has been recommending for a
generation. I called attention to this practice over a decade
ago, in an essay I originally wrote in 1970.° Unlike the dog that
returns to its vomit, Christianity Today never leaves it.

Rushdoony’s concluding comments in his chapter on the
Second Council of Constantinople (553 A.D.), “The Fallacy of
Simplicity,” are appropriate:

The council, moreover, was unafraid of complexity and
refinement of doctrine. It drew the line sharply, because the
alternative was to erase or at lcast blur the lincs betwcen Chris-
tianity and humanism. A retreat towards simplicity of faith is a
retreat into death. The scorn men reserve for those whose teach-
ings are difficult is no evidence of character but is in their
throats the death-rattle of a church and culture. The churches
today which draw the line sharply are small and lonely congre-
gations, growing only with difficulty, whereas the modernists
and Arminians who erase the line of offense and introduce
humanism into the church seem to flourish. But their growth is
simply the growth of corruption, and their only light is the
phosphorescence of decay.”

The Article Christianity Today Rejected

Three years ago, CT accepted Prof, John Hannah’s offer to
write an article on Reconstructionism. Prof. Hannah teaches
church history at Dallas Theological Seminary, a dispensational
institution. Dr. Hannah is therefore not a Reconstructionist. He
is, however, a serious scholar. [is article was a fair-minded

6. “Drifting Along with Christianity Today.” Journal of Christian Reconstruction, 1
(Winter 1975-76).

7. R. J. Rushdoony, Foundations of Social Order: Studies in the Creeds and Councils
of the Early Church (Fairfax, Virginia: Thoburn Press, {1968} 1978), p. 112.

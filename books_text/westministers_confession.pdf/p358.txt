334 WESTMINSTER’S CONFESSION

which Rushdoony discusses in The Institutes of Biblical Law, pp.
782-3. Unquestionably, we in Tyler would utterly reject such a
practice as a heretical throwback to Old Testament “shadows”
that were completely fulfilled by the death and resurrection of
Christ.

It our rejection of what Mr. Clapp correctly identifies as
Rushdoony’s “Armenian Connection” that ultimately led to the
split in the Reconstructionist camp: Tyler vs. Vallecito.

The Tyler-Vallecito Split

Mr. Clapp has hung out some dirty wash — which is unques-
tionably dirty, and which he had no obligation to suppress — so
I choose to respond. The time has come to stop covering up
what really is going on.

Mr. Clapp pieced together a garbled version of the story of
the split between Rushdoony and Tyler. He says that as the
editor of Chalcedon’s Journal of Christian Reconstruction, I sub-
mitted someone's article for publication which dealt with the
meaning of the Passover blood, and that Rushdoony rejected it
because it “reeked of a fertility cult.” Mr. Clapp correctly re-
ports that Mr. Rushdoony and I have not spoken to each other
since then. If this story were true, then the reader could safely
conclude that the Reconstructionist leadership borders on the
egomaniacal, and should not be taken seriously.

This version Mr. Clapp reports is incorrect. I was the sole
editor of the Journal. Mr. Rushdoony always gave me a nearly
free hand regarding what went into it. Here is what really
happened. I submitted to his Chalcedon Report my monthly
essay. It relied on an insight regarding biblical symbolism in
James Jordan’s 1981 Westminster Seminary master’s thesis. My
essay discussed the background symbolism of the Passover,
Rushdoony sent it back and insisted that I rewrite it, saying
that it was heretical, and even worse. I refused to rewrite it. I
did not insist that he publish it; I just refused to rewrite it. He

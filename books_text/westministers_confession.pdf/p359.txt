Honest Reporting as Heresy 335

had rejected one other article of mine in the past, so I was not
too concerned.

He refused to let the matter rest. He challenged me to make
my theological position clear, to prove to him that it was not
heretical. I then wrote an extended defense. He still said it was
heretical. He then said that Jordan and I would have to recant
in writing, and also agree in writing never to publish our essays
in any form, before he would agree that we were no longer
heretical. When we refused, he submitted a protest to our
church elders informing them of our heresy, and asking them
to discipline us both. When the church sent the essay (and my
extended defense of it) to other theologians, including West-
minster Seminary’s John Frame, they replied that it was some-
what peculiar but certainly not heretical.

The then elders asked Mr. Rushdoony to submit formal
charges against us regarding the specific heresy involved. He
refused. They also reminded him that he was not a member of
any local congregation, and therefore was not subject to disci-
pline himself should his accusations prove false. He blew up
when challenged on this. He then publicly fired me and Jordan
from Chalcedon, announcing our dismissal without explanation
in the Chalcedon Report. This surprised Jordan, since he was not
even aware he was employed by Chalcedon, not having re-
ceived money from Chalcedon in years.

My full essay, “The Marriage Supper of the Lamb,” was
later published in Geneva Ministries’ Christianity and Civilization,
No. 4 (1985), and sank without a trace. I have never received
a single letter about it, pro or con. The “crisis of the essay” was
clearly a tempest in a teapot. But it points to the underlying
tension which Mr. Clapp refers to.

What is this disagreement all about? It is Tyler’s disagree-
ment with Mr. Rushdoony about the requirement of local
church attendance and taking the Lord’s Supper. We think all
Christians need to do both. The Tyler church practices weekly
communion. In contrast, Mr. Rushdoony has refused to take

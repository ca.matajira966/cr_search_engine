354 WESTMINSTER’S CONFESSION

ment of the prophecy on earth and the present distance from
that future post-historical fulfillment, he unquestionably adopt-
ed the language of modern Calvinistic amillennialism. He
focused on the personal relationships of Church and family
rather than the real possibility of transforming the social and
cultural aspects of a fallen civilization. This also is basic to
Calvinistic amillennialism.

In his comments on Matthew 24:37, he compared the world
of the era of the return of Christ to the days of Noah and
Sodom. “Since indifference of this sort will exist about the time
of the last day, believers ought not to indulge themselves after
the example of the multitude.”“ He did not link this prophe-
cy to the last days of Old Covenant Israel, but to the last days
of the world. So, we know that there will be scoffers and lax
faith.

Calvin’s Optimism
But consider his interpretation of I Corinthians 15:27: “For
he hath put all things under his feet. But when he saith all

things are put under him, it is manifest that he is excepted,
which did put all things under him.” Calvin wrote:

He insists upon iwo things — first, that all things must be brought
under subjection to Christ before he restores to the Father the
dominion of the world, and secondly, that the Father has given all
things into the hands of his Son in such a way as to retain the
principle right in his own hands. From the former of these it
follows, that the hour of the last judgment is not yet come —
from the second, that Christ is now the medium between us and
the Father in such a way as to bring us at length to him. Hence
he immediately infers as follows: After he shall have subjected all
things to him, then shall the Son be subjected io the Father. “Let us
wait patiently until Christ shall vanquish all his enemies, and

14. Calvin, Harmony of Evangelists, ITI, p. 156.

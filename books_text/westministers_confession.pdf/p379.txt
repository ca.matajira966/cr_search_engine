Caluin’s Millennial Confession 355

shall bring us, along with himself, under the dominion of God,
that the kingdom of God may in every respect be accomplished
in us.¥

Calvin was a optimist regarding the long-term success of
Christianity in history.** In this sense, the mid-seventeenth-
century Puritans were faithful to Calvin’s legacy; so were the
postmillennialists of Princeton Seminary in the nineteenth
century. Today’s amillennial Calvinists have abandoned this
postmillennial heritage in the name of Calvin, but without the
documentation from the corpus of his writings, They teach and
preach as if they were faithful heirs of Calvin on the question
of millennialism, but they are not. It is far easier to make the
case that Calvin was not a theonomist than it is to make the
case that he was not an optimist regarding the future of Chris-
tianity on earth.

Dialecticism?

It is possible, and has been done, to suggest a dialectical
relationship between Calvin's view of the present world and the
post-resurrection world. This is the standard interpretation of
his millennial views within amillennial scholarship. The Barth-
ian theologian Heinrich Quistorp writes of Calvin’s view of
hope: “What is promised to faith is properly the contradiction
of all that is visible; righteousness where there is sin; eternal
life in place of death; resurrection in place of extinction; bless-
edness where pain; fulness where hunger and thirst; divine
help where a helpless cry. In face of these contradictions be-
tween the divine word and reality, faith can only subsist
through hope which trusts in the word of promise more than

15. Calvin, Commentary on the Epistle of Paul the Apostle to the Corinthians (Grand
Rapids, Michigan: Baker Book House, [1546] 1979), II, p. 31.

16. James B, Jordan, “Calvin’s Incipient Postmillennialisrn,” Counsel of Chalcedon
(March 1981), pp. 4-7.

INDEX

Abelard, 303
abortion, 58, 88, 182, 266-
70, 328-29, 346
Abraham, 63-64
accreditation, 10, 91, 314
Adam, 186
Adams, Jay, 37-38
adultery, 240-41
Afghanistan, 262
agnosticism, xxii
AIDS, 213, 330, 340
alchemy, xvi
amillennialism
ascension, 177, 183
Fort Contraction, 26
pictism, 180
pluralism &, 229
Van Til’s, 23
wilderness, 22
Anabaptists, 205, 324
angels, 219-20
Antichrist, 352
apologetics, 41
Aquinas, Thomas, 305
Arianism, xv-xvi, 106
ascension, 117-18, 140, 170,
177-78, 183-84
Athens, 86

auction process, 277
Augustine, 49
authority, 344-45
autonomy, 303

Bahnsen, Greg L.
answer to, 392
case laws, 211
etiquette, 12
Kline vs., 19, 100-1
Machen & Van Til, 22n
Rushdoony vs., 261-62
thesis accepted, xx
Van Til’s replacement?
42
Van Til unmentioned,
55
Waltke vs., 262-63
‘Westminster &, xx-xxi
Bakker, Jim, 345

bankruptcy, 322
Barker, William
citizenship, 227
Clowney &, 230
coin, 225
natural law, 227-30
Baxter, Richard, 277

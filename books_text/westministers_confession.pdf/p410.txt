ABOUT THE AUTHOR

Gary North received his Ph.D. in history from the Universi-
ty of California, Riverside, in 1972. He specialized in colonial
USS, history. Ile wrote his doctoral dissertation on Puritan New
England’s economic history and the history of economic
thought. A simplified version of this dissertation has been pub-
lished as Puritan Economic Experimenis (Institute for Christian
Economics, [1974] 1988).

He is the author of approximately 30 books in the fields of
economics, history, and theology. Since 1973, he has been
writing a multi-volume economic commentary on the Bible,
which now covers Genesis (one volume) and Exodus (three
volumes). He is the general editor of the ten-volume set, the
Biblical Blueprints Series (1986-87).

Beginning in 1965, his articles and reviews have appeared in
over three dozen newspapers and periodicals, including the
Wall Street Journal, Modern Age, Journal of Political Economy,
National Review, and the Japan Times.

He edited the first fifteen issues of The Journal of Christian
Reconstruction, 1974-81. He edited two issues of Christianity and
Civilization in 1983: The Theology of Christian Resistance and
Tactics of Christian Resistance. He edited a festschrift for Cornelius
Van Til, Foundations of Christian Scholarship (1976).

He is the editor of the fortnightly economic newsletter,
Remnant Review, and the publisher of three additional economic
newsletters. He writes two bi-monthly Christian newsletters,
Biblical Economics Today and Christian Reconstruction, published
by the Institute for Christian Economics.

He lives in Tyler, Texas with his wife and four children. He
is a member of Good Shepherd Reformed Fpiscopal Church,
Tyler, Texas.

The Question of Inheritance 39

the laws of Moses have anything to do with the New Covenant
economy except as types and symbols of Christ’s death and
resurrection and the post-judgment kingdom of God, he went
far beyond the Westminster Confession of Faith and its sup-
porting prvof texts.

Kline was another useful means of deflecting the seminary
from the judicial implications of Van Til’s system. Kline was a
defender of the “framework hypothesis,” which argues that the
six days of creation are not literal, but are literary devices: day
one is linked to day four, day two to day five, and day three to
day six. Edward J. Young had attacked this thesis in his WIJ
articles that later became Studies in Genesis One (1964), politely
using Nic Ridderbos as surrogate in his attack on his colleague
Kline. Being a swect, lovable man, Young lost the fight. Few of
the students even knew there was a fight going on.

Kline made it intellectually acceptable for bright. Calvinist
students to reject six-day creationism. He also enabled them to
reject any notion of the judicial relevance in New Testament
times of God’s Old Testament law-order. This is just what
Clowney needed in the late 1960's: a non-covenantal Calvinism
that would appeal to the nco-evangelicals coming out of the
evangelical colleges.

When Kline joined the faculty of Gordon-Conwell Theologi-
cal Seminary, maintaining full professorships at both, it was
perfect. The institutional link was sealed between the post-Van
Til Calvinism of Westminster and the social ethics of nco-cvan-
gelicalism. The confession of neo-evangelical social ethics is
basically this: “We can legitimately adopt ten-year-old political
fads that have now been discarded by the liberals, and all in
the name of relevant Christianity.” This was perfect for Clow-
ney’s restructuring of the seminary. It could preach Christian
social relevance without the conservative judicial constraints of
biblical law.

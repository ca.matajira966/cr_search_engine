The Question of Inheritance 43

Norman Shepherd’s contract. (Iry and find anyone who can
tell you what theological grounds they had. His presbytery
found none.) Next, they hired Clowney’s son David, Wayne
State University M.A. in hand, to fill Van Til’s position as the
resident apologist at the Philadelphia campus. The transforma-
tion of Westminster East was sealed. For the next seven years
until David Clowney’s dismissal, apologetics at Westminster's
Philadelphia campus meant always having to say you were sorry —
sorry for being white, masculine, middle class, educated, Amer-
ican, and living in Philadelphia’s suburbs.”

The primary institutional task of Westminster’s resident apol-
ogist, whoever he may be ~ the ultimate institutional task for
which he is being paid — is to declare publicly (if only by his
silence) what Westminster isn’t: éheonomic. It was this priority
that made an M.A. from Wayne State more academically ac-
ceptable than a Ph.D. from USC. They bent the academic
rules. Surprise, surprise! It was one more bit of evidence sup-
porting Van Til’s claim that neutrality is a myth.

A New Confession for Westminster?

Theonomy: A Reformed Critique is a symposium written by
faculty members and former faculty members of Westminster
Theological Seminary. The book appeared a bit late, seventeen
years after: (1) Roe v. Wade, (2) Rushdoony’s Institutes of Biblical
Law, (3) the publication of my Introduction to Christian Economics,
(4) Chalcedon Report started publishing my “Economic Commen-
tary on the Bible” column, and (5) the acceptance by the West-
minster faculty of Bahnsen’s Th.M. thesis; and fourteen years
after the thesis was published, Theonemy in Christian Ethics. So
far, we have published well over one hundred volumes of
theonomic books and scholarly journals. Theonomy: A Reformed

22. David Clowney was dismissed from the faculty in 1988. He was replaced by
aman whose one book is on Christianity and music. His real job description is this:
“Keep Bahnsen out.”

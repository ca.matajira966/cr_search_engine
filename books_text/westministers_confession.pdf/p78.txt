54 WESTMINSTER’S CONFESSION

traces of natural law theory in apologetics, meaning the intel-
lectual defense of the faith. He traced Scholasticism’s rationalist
methodology down through Lutheranism and modern funda-
mentalism, which he attacked at every point. He dedicated his
career to demonstrating that any appeal to the hypothetical
neutrality and universality of the reason of self-proclaimed
autonomous man is a snare and a delusion. Thus, Van Til’s
system broke cleanly and totally from the view of civil law that
Calvin defended in the Jnstitutes. But Van Til was careful never
to discuss civil law; he only discussed the narrow issues of
philosophy, i.e., natural law as it relates to such topics as episte-
mology (the Scholastic proofs of God, etc.).

Because of this unwillingness on his part to extend the
obvious implications of his presuppositional thought to the
realm of social theory, Van Til could claim that he was not a
Christian Reconstructionist. His intellectual position was remin-
iscent of Charles Lyell’s, the systematizer of uniformitarian
geology, who insisted for several years after the publication of
Darwin’s Origin of Species that he himself was not a Darwinist,
since he did not believe that man had evolved. Man was dis-
continuous from nature, he insisted.” Yet it was Lyell’s doc-
trine of continuity in geological development (measured by the
presence of fossils) that had led Darwin to his theory of organic
continuity. It was Darwin’s reading of Lyell’s Principles of Geolo-
gy (1833) on the famous voyage of the Beagle that persuaded
Darwin to adopt a new explanation of biological development,
evolution by natural selection. (Late in life, almost a decade
after the Origin appeared, Lyell finally adopted Darwin’s
views.) Similarly, it was Rushdoony’s reading of Van Til in
the 1950’s that led him in the late 1960's to begin to develop

19. Loren Eiseley, Darwin's Century: Evolution and the Men Who Discovered It
(Garden City, New York: Anchor, [1958] 1961), p. 267.

20, William Invine, Apes, Angels, and Victorians: The Story of Darwin, Huxley, and
Evolution (New York: MaGraw-Hill, 1955), p. 210.

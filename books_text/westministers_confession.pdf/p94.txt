70 WESTMINSTER’S CONFESSION

In the Institutes, he did not go into comparable detail. Thus,
we find there no basis of determining what Calvin’s view of
social change was. It is not possible to construct a concept of
judicial cause and effect in history based on Calvin’s Justitutes.
This is why those Calvinists whose goal is to assert the indeter-
minacy of Calvinistic social theory - a Cole Porter view of social
theory: “Anything Goes,” meaning a theory of social open-ended-
ness — concentrate their attention on the Institutes. The anti-
theonomists are self-consciously not interested in exploring the
Calvin of the sermons on Deuteronomy. This is understand-
able, but it has produced misleading historical scholarship.

Conclusion

John Calvin assumed far more regarding Christendom than
he put on paper. Thus, those of his followers who today reject
both the historic ideal and possibility of Christendom — “Cons-
tantinianism,” in the jargon of Calvinistic pluralism®* - are not
continually confronted in his writings with the magnitude of
the difference between Calvin’s worldview and their own. Hay-
ing dismissed Calvin’s clear-cut assertion of the State as the
protector of the Church, they also dismiss his ideal of Christen-
dom. They necessarily pass over in silence his sermons on Deu-
teronomy regarding the legitimacy of the Mosaic law’s civil
sanctions, as well as his defense of the existence of God’s sanc-
tions in history. Then they recoil in shock, horror, and outrage
from the task that the theonomists have placed before them
since 1973: to offer a Calvinistic view of social theory without
{1) Calvin’s view of civil sanctions and (2) Calvin’s view of the
future of the gospel. They point defensively to his accep-
tance of sixteenth-century Protestant natural law theory, yet

59, The term is used against Calvin by Leonard Verduin, The Reformers and
Their Stepchildren (Grand Rapids, Michigan: Baker Book House, 1964), p. 82. Theo-
dore Beza was even worse: “undiluted Constantinianism” (p. 88).

60. On his millennial views, see Appendix D, below.

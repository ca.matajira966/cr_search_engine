x When Justice Is Aborted

murmured against me. Doubtless ye shall not come into the land,
concerning which I sware to make you dwell therein, save Caleb
the son of Jephunneh, and Joshua the son of Nun. But your little
ones, which ye said should be a prey, them will I bring in, and
they shall know the land. which ye have despised. But as for you,
your carcases, they shall fall in this wilderness (Numbers 14:28-32).

God had imposed His national negative sanctions against them for
their personal and corporate cowardice and lack of faith in Him.
Now Joshua was the new national leader. His claim almost forty
years earlier was about to be vindicated by God.

Notice the five major points of God’s instruction to Joshua.
First, God, the sovereign Lord of history, is commanding them.
He is present with them. Second, He refers to Moses, His represen-
tative and national leader over Israel, in His instructions to Joshua,
His new representative and national leader. Third, He tells Joshua
to honor and obey the law of God. Fourth, He tells him that if he
and the people obey this law, they will prosper. Fifth, He tells him
that they will inherit the land.

This is the Bible’s five-point covenant model. God reminded
Joshua of all five points before He led them into battle. It was on
the basis of this covenant and its promises that Joshua was ex-
pected to have courage.

When Christians face a corporate challenge to their faith, they
must exercise corporate responsibility. Today, Christians are in a
war, a war against secular humanism. The leaders of the humanist
camp are far more self-conscious about this war than most Chris-
tians are. This is why they have an inital advantage. But that
advantage can and will be overcome as Christians rediscover their
heritage of successful resistance to tyranny. The recovery of this
heritage must begin with an understanding of thé biblical covenant
model,

Faithful Christians should no longer ignore the comprehensive
nature of this war. The enemy's army is advancing toward us
whether we acknowledge it ornot. It has been advancing since the
day that Satan entered the garden. Let us not be so foolish or naive

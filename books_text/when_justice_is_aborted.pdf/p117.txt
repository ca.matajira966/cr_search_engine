Whose Sanctions Will Prevail? 99

Being an interposer in the face of physical danger is another
aspect of being an intercessor. As a stripe-bearer, the interposer
places himself as God’s agent of positive sanctions. God’s positive
sanctions (blessings) will come to the whole society if the voters
see what their civil representatives aré doing, and then replace
them,

Should Christians Resist Public Evil?

Chapter 2 begins with the seemingly contradictory biblical
passages regarding obedience to civil magistrates. If we under-
stand the covenant, we understand how these two principles fit
together: the doctrinc of hierarchical authority. The state is under
God. The Christian who protests a biblically evil law is being
faithful to God. He is calling the rulers to repentance by a public
stand against a bad law, The Christian must obcy the terms of
God’s covenant,

A similar and even parallel seeming dilemma is found in these
two passages:

But I say unto you, That ye resist not evil: but whosoever shall
smite thee on thy right check, turn to him the other also (Matthew
5:39).

Submit yourselves therefore to God. Resist the devil, and he
will flee from you (James 4:7).

The Christian is not called to strike the king’s agent on his
cheek; rather, he is to accept the blows on his own cheek. The
context of this passage is bondage. Israel was under the political
rule of the Roman Empire. Jesus was instructing His followers not
to become violent political revolutionaries. Submit to stronger
political and military power for the time being, He said. But this
did not mean that they should not oppose civil rulers who were
evil. It meant that the resistance program should be non-violent.
He was calling His followers to non-violent protests: to take the
blows of the unrighteous rulers,

James was saying the same thing. Christians are to submit to

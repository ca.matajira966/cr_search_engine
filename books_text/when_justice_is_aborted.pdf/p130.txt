112 When Justice Is Aborted

angels, nor principalities, nor powers, nor things present, nor
things to come, Nor height, nor depth, nor any other creature, shall
be able to separate us from the love of God, which is in Christ Jesus
our Lord (Romans 8:28-39).

Tn short, the discontinuity introduced in history by Satan is
overcome in Jesus Christ. Thus, it is the Christian’s God-assigned
task to preach the gospel of reconciliation, both in word and deed,
to the lost. The great discontinuity is forever behind us; the death
of Jesus Christ.

‘Whose Continuity: Satan’s or God’s?

Satan seeks to defend his kingdom. He seeks to get men to
worship him by failing to worship God. This was the essence of his
temptation of Jesus in the wilderness. “Again, the devil taketh him
up into an exceeding high mountain, and sheweth him all the
kingdoms of the world, and the glory of them; And saith unto
him, All these things will I give thee, if thou wilt fall down and
worship me. Then saith Jesus unto him, Get thee hence, Satan: for
it is written, Thou shalt worship the Lord thy God, and him only
shalt thou serve” (Matthew 4:8-10). Satan sought to lure Jesus
into Adam’s original discontinuity by tempting Him to rebel against
God. This strategy failed.

Satan seeks to maintain man’s continuity of rebellion against God.
He has captured his kingdom through Adam’s rebellion. He now
occupies it as a squatter occupies unclaimed or stolen land. He can
retain control over this domain only by getting the sons of Adam
to acknowledge his title to the inheritance. This is why the great
discontinuity of the crucifixion of Christ now threatens his king-
dom. That discontinuity re-established the original covenantal
continuity between God and redeemed mankind.

The continuity of covenant-breaking man’s rebellion is threat-
ened by God’s free offer of the gospel. Those who accept the offer
of the gospel break their existing covenant with Satan. They
establish a covenantal continuity with God by means of soul-
saving faith in Jesus Christ. This is what it means to be “born

122 When Justice Is Aborted

Transcendence and Presence.in
Progressive Sanctification

God the Creator and Judge is wholly transcendent to this
world, yet He is always present with His people. If He were not
totally transcendent to this world, He would be neither its Creator
nor its Judge. On the other hand, if He were not present with this
world, He could not influence its development. He would be bound
by a selfimposed “hands off” policy. The hypothetical god of
eighteenth-century Deism — transcendence without presence — was
so distant from his creation as to pay no attention to the world and
to leave it entirely alone. He just “wound the world up” like a clock
and departed. In contrast, the hypothetical god of panthe-
ism — presence without transcendence — is so much a part of this
world that he cannot bring it under judgment. He is incapable of
changing it because he is immersed in it. Covenant-breaking man
is willing to accept either of these two false gods in preference to
the true God of the Bible.

What is true of God is analogically true of Christians. This is
why we need a biblical definition of God: wholly transcendent, yet
personally present. If we were not linked covenantally to a tran-
scendent God, then we would have no legal authority as His
designated representatives to call other men to account in God’s
name. On the other hand, if God were not present with us in our
various callings and tasks, we could not change the world because
we would be neither of the world nor in it. But we are in this world,
set apart (sanctified) as saints, and therefore burdened with the
God-given responsibility of calling this world to account and also
working from within to change it: We are not of this world. because
we are linked to a transcendent God by means of His covenant,
and this covenantal bond defines us, not our present geographical
and temporal location. At the same time, we also are in the world
as His covenantal agents. Jesus made this clear in His public
prayer before God:

And all mine are thine, and thine are mine; and I am glorified
in them. And now I am no more in the world, but these are in the

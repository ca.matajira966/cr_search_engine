Appendix A

ARE OPERATION RESCUE’S
CRITICS SELF-SERVING?

And the princes of Issachar were with Deborah; even Issachar,
and also Barak: he was sent on foot into the valley. For the divisions
of Reuben there were great thoughts of heart. Why abodest thou among
the sheepfolds, to hear the bleatings of the flocks? For the divisions of
Reuben there were great searchings of heart, Gilead abode beyond
Jordan: and why did Dan remain in ships? Asher continued on the sea
shore, and abode in his breaches. Zebulun and Naphtali were a people
that jeoparded their lives unto the death in the high places of the field
(Judges 5:15-18),

Deborah had issued a challenge to Israel: Come and fight the
enemies of God! Some tribes came, but others did not. The tribe
of Issachar came, but the tribe of Reuben stayed home, safe among
the sheep. They were too busy searching their hearts. Dan went
fishing. But Zebulun and Naphtali jeopardized their lives unto
death. All this was immortalized by General Deborah in her song.

There are always those who choose to go fishing in the midst
of a life-and-death crisis. Christians in Russia did in 1917. Chris-
tians in Germany did in 1932. The Dans of this world are legion,
especially in the twentieth-century church. But my concern here
is not with Dan; it is with Reuben. Dans choose not to get involved,
but they are polite enough to remain quiet. They are not deep
thinkers.

183

6 When Justice Is Aborted

by going to the Bible to test the spirits of disobedience. Above all,
we must understand that ihe Bible is a covenantal document. To
understand the difference between good and evil, we must under-
stand what God’s covenant is.

The Covenant Structure

To get the right answers, we need first to ask the right ques-
tions. For a Jong, long time, Christians and Jews have had the
correct questions right under their noses, but no one paid any
attention. The questions concerning lawful government are organ-
ized in the Bible around a single theme: the covenant.

Most Christians and Jews have heard the word “covenant.”
They regard themselves (and occasionally even each other) as
covenant people. They are taught from their youth about God’s
covenant with Israel, and how this covenant extends (or doesn’t
extend) to the Christian Church, But not many people who use
the word really understand it. If you go to a Christian or a Jew
and ask him to outline the basic features of the biblical covenant,
he will not be able to do it rapidly or perhaps even believably. Ask
two Jews or two Christians who talk about the covenant, and
compare the answers. The answers will not fit very well.

In late 1985, Pastor Ray Sutton made an astounding diseov-
ery. He was thinking about biblical symbols, and he raised the
question of two New Testament covenant symbols, baptism and
communion. This raised the question of the Old Testament’s
covenant symbols, circumcision and passover. What did they have
in common? Obviously, the covenant. But what, precisely, is the
covenant? Is it the same in both Testaments (Covenants)?

He began rereading some books by theologian Meredith G.
Kline. In several books, Kline mentioned the structure of the Book
of Deuteronomy. He argued that the book’s structure in fact
parallels the ancient pagan world’s special documents that are
known today as the suzerain treaties. These treaties were imposed
by conquering kings on defeated kings who were offered the oppor-
tunity to become vassals of the conqueror.

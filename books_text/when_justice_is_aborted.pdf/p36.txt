18 When Justice Is Aborted

of origin, We are spiritual creatures. Our spiritual origin is outside
this world. Christians are citizens of heaven (Philippians 3:20).
Yet we are also citizens of this world. We are subject to a higher
heavenly power and also to lower earthly powers. We make ethical
decisions in this world, and these decisions have implications for
at least our starting position in the next world (I Corinthians 3).

The questions begin. How did these earthly powers gain lawful
authority over us? By what legal right do they exercise authority
over us if they command us to obey evil laws, or refuse to prosecute
evil acts, thereby delivering us into the hands of evil people? Thus,
the second step in developing a Christian applied ethical system
is to get a biblical answer to the question: “What is the source of
all earthly authority?”

We should begin our search with the most fundamental pair
of questions a person can ask: “Who is God, and what does he
‘want me to do?”

God the Sovereign Creator

God is absolutely sovereign. He is the Creator. “I have made
the earth, and created man upon it: I, even my hands, have
stretched out the heavens, and all their host have I commanded”
(Isaiah 45:12). He sustains the universe providentially through
His Son, Jesus Christ. It ig God the Father who “hath delivered
us from the power of darkness, and hath translated us into the
kingdom of his dear Son: In whom we have redemption through
his’ blood, even the forgiveness of sins: Who is the image of the
invisible God, the firstborn of every creature. For by him were all
things created, that are in heaven, and that are in earth, visible
and invisible, whether they be thrones, or dominions, or principali-
ties, or powers: all things were created by him, and for him: And
he is before all things, and by him all things consist” (Colossians
1:13-17).

God answers to no one except Himself. No one holds God
accountable for what He’has done or has failed to do. “Wilt thou
also disannul my judgment? Wilt thou condemn me, that thou

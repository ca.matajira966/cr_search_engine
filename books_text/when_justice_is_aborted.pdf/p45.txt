The Author of All Authority 27

send him unto you. And when he is come, he will reprove the world
of sin, and of righteousness, and of judgment: Of sin, because they
believe not on me; Of righteousness, because I go to my Father,
and ye see me no more; Of judgment, because the prince of this
world is judged. I have yet many things to say unto you, but ye
cannot bear them now. Howbeit when he, the Spirit of truth, is
come, he will guide you into all truth: for he shall not speak of
himself but whatsoever he shall hear, that shall he speak: and he
will shew you things to come. He shall glorify me: for he shall
receive of mine, and shall shew it unto you. All things that the
Father hath are mine: therefore said I, that he shall take of mine,
and shall shew it unto you. A little while, and ye shall not see me:
and again, a little while, and ye shall see me, because I go to the
Father (John 16:5-16).

There are millions of Christians today who feel impotent in the
face of this world’s powers. They believe that because Jesus Christ
is not physically present on earth that His people are at best people
without much influence. Worse; they believe that as time goes on,
God’s people will have even less influence. They will be steadily
surrounded and forced into the shadows of history.

Did Jesus say such a thing? No, He said that the prince of this
world is judged. He said that He had to go away so that His people
could gain more knowledge and more authority. He said that the
Holy Spirit would lead godly men into all truth. But today’s
Christians cannot seem to understand the extent of the authority
that Christ passed to His people when He sent the Holy Spirit into
the midst of the church. Christians lack confidence because they
do not fully understand the extent to which God is present with
His people in their battles against the spiritual heirs of Satan.
They believe that the only way for them to be salt and light and
the leaven of righteousness in history is for Jesus to come again
physically and set up an international Christian bureaucracy.

If this is not berating the work of the Holy Spirit, what is? If
this is not ignoring the specific words of Jesus — that His people
will receive power when the Holy Spirit comes to them — then
what is? Today Christ’s bodily resurrection is behind us. We have

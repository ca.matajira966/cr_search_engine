34 When Justice Is Aborted

needs. While the lost may be in the churches, and while many
denominations are lost, the church, as a church, is not in need of
revival, for God’s people have already been in principle resur-
tected. Their revival was. Their definitive sanctification was. Now
they have to apply it: progressive sanctification. For that, they do
not need revival. Instead, they need an awareness of the covenant.

A Stolen Vision

We can see how the biblical process of “continuity, discontinu-
ity, continuity” is supposed to work by considering a specific case
in American history when it failed to work. There was continuity
and discontinuity, but the subsequent continuity was cut short.

In the decades prior to the Civil War (1861-65), the second
Great Awakening, which began around 1800 and accelerated rap-
idly in the 1820-1850 period, brought many tens of thousands of
people to a profession of faith in Jesus Christ, especially in the
North and Midwest (then called the Northwest). (The First Great
Awakening was the revival a century earlier, 1735-55, whose most
famous preachers were the roving English evangelist George White-
field [WHITfield] and pastor Jonathan Edwards, whose sermon,
“Sinners in the Hands of an Angry God,” with its unforgettable
image of the spider suspended on a thread above the burning coals,
may be the most famous sermon in American history.) Many of
the evangelical leaders of the Great Awakening in the North
became social activists, campaigning publicly against the institu-
tion of chattel slavery. The continuity of evangelism (1800-1820)
brought on a religious discontinuity (the Second Great Awaken-
ing, 1820-50), which in turn helped bring a great political disconti-
nuity: the Civil War.

But the God-required continuity after 1865 did not appear.
After the ‘war, this evangelical enthusiasm for social reform waned
in Bible-believing circles. The revivalists from the beginning of the
abolitionist movement had deferred to the intellectual leadership
of liberal and radical Unitarians in New England, so the evangeli-
cals never gained social and intellectual leadership in this reform

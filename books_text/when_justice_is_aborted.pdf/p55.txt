The Author of All Authority 37

cal view that the great tribulation took place in 70 A.D. with the
fall of Jerusalem to the Roman army, is because of the coming of
the growing conflict over abortion. If Chilton is correct, this: means
that Christians’ socially paralyzing concern over some inevitable
catastrophe in the future—or the church’s inevitable removal
from history just before this inevitable catastrophe — is misplaced.
Tt means that Christians need not fear the supposedly inevitable
defeat of the gospel of Jesus Christ prior to His Second Coming in
final judgment. It means, in short, that the church is not a loser
in history. Therefore, our efforts as Christians to make the world
better before Jesus returns to earth in glory are not doomed to
inevitable defeat by Bible prophecy. Christians can lose many
battles but not the war. As with any army, we can experience
painful defeats, but the victory of our righteous cause is assured,

People do not want to join a personally risky battle that their
leaders say cannot be won before Jesus comes to rapture them to
the safety of heaven. Christian leaders who seek to mobilize follow-
ers in such confrontations have begun to abandon their former
commitment to eschatologies of earthly defeat, especially the younger
leaders. This is why the battle over abortion has led to a battle
over eschatology. This is also why those fundamentalists who
preached the older, culturally defeatist eschatologies did not imme-
diately start protesting abortion after Roe v. Wade in 1973, The
abortion issue has polarized Christians in many areas and for
many reasons. The old rule is true: “You cannot change just one
thing.”

Conclusion

We must preach Christ and Him crucified. But we must also
preach Christ resurrected and ascended, seated on the right hand
of God in full authority. We must preach the Holy Spirit, God's
representative who guides His people into all truth, We must
preach the transcendence of God on high and the presence of God
in our hearts and in our midst. Nothing less than this will do.

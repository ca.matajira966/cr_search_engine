Honoring God’s Law by Disobeying Evil Humanist Laws 79

can any Christian legitimately expect the public to change its mind
because of a massive organized protest? The answer is: “The same
way the voters changed their minds in 1960-64, during the sit-ins
in the American South.”

What we must understand is that there is still a large Christian
electorate. It is unorganized and anything but self-conscious, but
it is Christian. It has been buffaloed by the doctrine of judicial
supremacy (which was not a doctrine even conceived of by the
Constitutional Convention in 1788) and by endless liberal human-
ist propaganda abcut “freedom of choice (i.e., murder)” for women.

Even if all the voters were hard-core pagans, the law of God
still impresses them. Defending it and enforcing it is a form of
evangelism,

Behold, I have taught you statutes and judgments, even as the
Lorp my God commanded me, that ye should do so in the land
whither ye go to possess it. Keep therefore and do them; for this is
your wisdom and your understanding in the sight of the nations,
which shall hear all these statutes, and say, Surely this great nation
is a wise and understanding people. For what nation is there so
great, who hath God so nigh unto them, as the Lorn our God is
in all things that we call upon him for? And what nation is there
so great, that. hath statutes and judgments so righteous as all this
Jaw; which I set before you this day? (Deut. 4:5-8).

The reason why the defense of God’s law works as a means of
evangelism and persuasion is because all men have the works of
the law written on their hearts ~ not the law itself, the text says,
but at least the work of the law. “For when the Gentiles, which
have not the law, do by nature the things contained in the law,
these, having not the law, are a law unto themselves: Which shew
the work of the law written in their hearts, their conscience also
bearing witness, and their thoughts the mean while accusing or
else excusing one another” (Romans 2:14-15), Thus, when right-
eous people conduct their protests righteously, bearing the blows
of the civil government (see Chapter 4), the public will eventually
respond sympathetically, But the public has to know that the

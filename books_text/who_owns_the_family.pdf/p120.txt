94 Who Owns the Family?

fashioned’ parents, from bossy older brothers and sisters” (Macmil-
lan Gateway English, Macmillan, 1970).

Authority is necessary to a proper Christian education and
view. The school, acting as a surrogate parent, should reflect
God’s chain of command at every point. When the student re-
sponds obediently to the instructor, he should understand that he
is responding to God’s representative, hired by his parents. To
rebel is to revolt against God. Humanism places fina! authority in
“self.” Man is believed to be autonomous and totally unaccount-
able. His system says there is no God, so ultimately there is no
basis for submission except for brute power, tyranny or anarchy.
‘The person with the “biggest stick” is the leader.

Ethics

Christianity teaches that God’s Law is the basis of all authority
and life. There is an absolute standard of right and wrong. It used
to be that virtually every child learned these. Why? Children need
to learn the ethical “boundaries” of life. If they don’t, then they
will never be able to make good decisions:

Humanism sets out to destroy the Biblical standard. It hates
the Ten Commandments so badly that they are not even allowed
to be posted on any public building. The Humanist Manifesto II
says, “We reject all religious, ideological, or moral codes that den-
igrate the individual, suppress freedom, dull intellect, dehuman-
ize personality.”

And, in public school textbooks, there are such examples as
the following from a grade 3 textbook discussing “talking about
your own ideas.” It says, “Most people think that cheating is
wrong, even if it is only to get a penny, which is what Shan did.
Do you think there is ever a time when it might be right?” (Com-
municating: The Heath English Series, 1973).

The last example typifies the way humanist education is
always trying to get the student to question Christian values.
Christian education, on the other hand, provides a clear-cut sys-
tem of ethics, Biblical law. What is the best environment for the
student? One where God’s standard is constantly being challenged,

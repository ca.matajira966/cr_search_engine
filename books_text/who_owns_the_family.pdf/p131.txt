Whose Protects the Family, and How? 105

Looked at in this light, is the Bible unmerciful when it speaks
of death penalty crimes this way? No. The State’s responsibility is
to see that God’s laws are applied. Its responsibility is not educa-
tion. Not welfare. Not housing. Not printing money. Not 90% of
what it does. All of these things have been proven to durt the fam-
ily. What's killing the family are the thieves and thugs in and out~
side of government!

But how about the family itself? Is there anything i can do to
guard against all of the attacks on it?

The Family

Yes, the family can protect itself, but “it’ ll need to be shrewd.”
T don’t mean “illegal,” but crafty in its dealings with the civil gov-
ernment. It will have to know the law better than the government.
There is a good Biblical example. When the Apostle Paul was
taken prisoner, he demonstrated that he knew the law of the land
better than the officials who had him in custody. While standing
before Festus, a Roman official, the following conversation took
place.

But Festus, wanting to do the Jews a favor, answered Paul and
said, “Are you willing to go up to Jerusalem and there be judged
before me concerning these things?” Then Paul said, “I stand at
Caesar’s judgment seat, where I ought to be judged. To the Jews I
have done no wrong, as you very well know. For if I am an
offender, or have committed anything worthy of death, I do not
object to dying; but if there is nothing in these things of which
these men accuse me, no one can deliver me to them. [ appeal to
Caesar.” Then Festus, when he had conferred with the council, an-
swered, “You have appealed to Caesar? To Caesar you shall go!”
(Acts 25:9-12)

The-Apostle Paul was a Roman citizen. He knew that he
could appeal to Roman law, therefore, to receive a fair trial—any-
way, more fair than he had received so far. Besides, it kept him
from being murdered in Jerusalem.

Paul knew Roman law. He was not lawless or a rebel. Never-
theless, he used law to the advantage of the Kingdom of God. I

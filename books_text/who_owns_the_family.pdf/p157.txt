What Can the Family Do? 131

and blessed the people, and their voice was heard; and their prayer
came up to His holy dwelling place, to heaven. Now when al this
was finished, all Israel who were present went out to the cities of
Judah and broke the sacred (pagan) pillars in pieces, cut doen the wooden
images, and threw down the high places and the altars {from Judah, Ben-
jamin, Ephraim, and Manasseh—until they had utterly destroyed
them all. Then all the children of Israel returned to their own
cities, every man to his possessions (II Chronicles 30:13-31:1). (emphasis
added)

Hezekiah told the people they needed to return. Where did
they begin? Worship. Where did they end? “Every man returned
to his possessions.” In other words, ownership went back to the
proper trustees of the family. Is this what you want? Then the
place to begin is around God's throne.

The Bible begins here. Earlier, I talked about how Christ re-
stored the family. The principle I emphasized was, “Honor God’s
JSamily, and He'll honor yours.”

You want to know why family life has degenerated in Amer-
ica? It is because families have turned away from the Lord. If they
don’t honor God, then He will not honor them,

Look at all the efforts to save the family: Presidential commis-
sions, millions of books, magazine articles, psychological studies,
and even television shows. How many of them ever mention the
Church and worship before God's throne? None!

In the late 1940s, there was a radio program called (if memory
serves me correctly) the “Family Hour.” (Naturally, it lasted only a
half hour.) It ended each show with this slogan: “The family that
prays together stays together.” The place to begin is the Church,
around God’s throne, worship, and God’s house.

What Should You Look For in a Church?

1. You want to find a church that believes in the Word of God.
How can you tell? Here are a list of questions you can ask to find
out what the church believes. Ask the pastor and the officers.

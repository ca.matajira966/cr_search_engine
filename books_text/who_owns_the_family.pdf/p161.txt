What Can the Family Do? 5

derstood by things that are made, even His eternal power and
Godhead, so that they are without excuse, because, although they
knew God, they did not glorify Him as God, nor were thankful,
but became futile in their thoughts, and their foolish hearts were
darkened. Professing to be wise, they became fools, and changed
the glory of the incorruptible God into an image made like cor-
ruptible man—and birds and four-footed beasts and creeping
things (Romans 1:18-23).

The difference between the creationist and evolutionist
emerges in every academic discipline, since man is a religious
creature. In economics, there is no set, definite standard for how
an economy is to be run. In law, there is no fixed law to which
man is answerable, transcending above his national constitutions,
or his interpretations.

Do you begin to get the picture? Creationism is at the center of
the battle for our whole culture. With it comes the true belief that
there is only one way to God and one standard, Christianity.
That’s why Christian schools play such an important role in put-
ting the family back into the hands of the proper trustees.

If a Christian school “saves money” by using “free” state-
approved textbooks, find another school. There’s no sense in pay-
ing tuition to get warmed-over humanism with a morning prayer.
Those prayers are just too expensive.

Moral Environment

The second effect of Christian education has been the creation
of a better moral environment for children. A recent study re-
vealed that if a child spends 40 or more hours a week in a day-care
center, he will be permanently damaged psychologically. Why?

It’s the moral environment. In many Christian circles, environ-
ment has been greatly underestimated. Solomon didn’t
underestimate it. Listen to what he tells fathers to teach their sons
at the very beginning of his book.

My son, if sinners entice you, do not consent. If they say,
“Come with us, let us lie in wait to shed blood; let us lurk secretly
for the innocent without cause; let us swallow them alive like

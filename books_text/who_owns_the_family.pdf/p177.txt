What Can the Church Do? 151

Church discipline is a powerful weapon against the State. It
says to the government, “We're a separate kingdom, leave us
alone.”

How does church discipline work? Listen to the words of
Christ.

Moreover if your brother sins against you, go and tell him his
fault between you and him alone. If he hears you, you have gained
your brother, But if he will not hear you, take with you one or two
more, that by the mouth of two or three witnesses every word may
be established. And if he refuses to hear them, tell it to the church.
But if he refuses even to hear the church, let him be to you like
heathen and a tax collector. Assuredly, I say to you, whatever you
bind on earth will be bound in heaven, and whatever you loose on
earth will be loosed in heaven, Again I say to you that if two of you
agree on earth concerning anything that they ask, it will be done
for them by My Father in heaven. For where two or three are
gathered together in My name, I am there in the midst of them
(Matthew 18:15-20).

Here’s the principle of “binding” we talked about at the begin-
ning of the chapter. The Church can “bind” someone over and out
of the congregation. Is this “unloving”? No, the Bible says that dis-
cipline is a supreme act of love. The writer to the Hebrews states,
“And you have not forgotten the exhortation which speaks to you
as to sons: My son, do not despise the chastening of the Lord, nor.
be discouraged when you are rebuked by Him; for whom the Lord
loves He chastens, and scourges every son whom He reccives”
(Hebrews 12:5-6). (emphasis added)

Tf this is true of God, how much more should it be of the
Church? Love is not contrary to discipline. Both go together. But
the passage in Matthew lays out specific guidelines: (1) go
privately; (2) then go with witnesses; (3) then take it to the whole
church. How is this done? When the third stage of discipline is
reached, the officers of the church should be brought in. They
represent the church, and are the ones to handle the “tell it to the
church.” It may be that they will have to set up court and function
like judges, just as Paul said in the 1 Corinthians 6 passage. Ifa

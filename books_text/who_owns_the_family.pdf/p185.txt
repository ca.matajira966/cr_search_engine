What Can the State Do? 159

number in the telephone book. If not, then call the local Chamber
of Commerce. When you call the party headquarters you want to
work in, ask for a map of the precincts for the entire area. Take it
to your church and ask the officers if you can post it on the bulletin
board. This is just one more thing you can do to get Christians in-
volved in politics.

Remember, all party resolutions begin at the precinct level.
The party platform is set here, When you go to a precinct meeting
you'll be impressed with how much power you, John-Q-Citizen,
has in this country.

4, Get involved working for the party you’ve registered with,
Recently, my wife and I learned an important lesson at our party’s
county convention. The chairman of the resolutions committee
said to several Christians who were trying to get a strong pro-life
resolution through, “I'm all for pro-life but the party wants to
know if you people are just going to be ‘one-issue’ people, or if
you're going to work for the party. Because if you're just ‘single-
issue’ folks, you're not going to get the attention of the leadership.”
Whether you like this attitude or not, that’s the way it is in any or-
ganization. Jesus gave us this principle: dominion through service.
The people who roll up their shirtsleeves and work get listened to.

Hardly anyone is willing to work this way. Apathy rules the
public, so the political professionals rule the public. That’s why
the American party system is a sitting duck.

Act like a steward of political power. You already are one,
since you have the vote. There is no escape from this responsibil-
ity. It’s like responding to the offer of the Gospel: no decision is still a
decision. “I won’t get involved!” is a decision —a decision to remain
a political slave.

This is how the system works and can be changed. If you want
to change it, you've got to do these four things. But now, let’s get
more specific.

Suggested Resolutions
If you want to change society at the civil level, you've got to
use the existing political machinery. You've got to vote. But you've
got to do more. This means getting resolutions passed that can be made

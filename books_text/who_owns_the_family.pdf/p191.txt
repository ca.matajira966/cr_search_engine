What Can the State Do? 165

to American borders in Mexico. Keep in mind that we live in a
violent society. Keep in mind that there are not enough policemen
to respond quickly enough if thugs attack your house. One man
said in our last precinct meeting, “If I have a gun, I feel that I can
do at least something to protect my family if it’s attacked. But one
thing is for certain if I don’t have a gun—I can’t do much of any-
thing.” Another man said, “I have never owned a gun and don’t
plan to. But I think I have the right to own one in the event that 1
want to.”

When guns are outlawed, only outlaws will own guns. The
Constitution says that law-abiding citizens shouldn't have to
become outlaws with respect to gun ownership.

Resolution On Victim's Rights

WHEREAS, the federal and state judiciary have exhibited an
inordinate concern for the right of criminals, as opposed to the
rights of those who have been the victims of criminals and their
crimes; and

WHEREAS, this has led to a disrespect for law which threat-
ens to undermine the very foundations of our society; now there-
fore

BE IT RESOLVED by the party in Precinct #_______ that
restitution should be instituted so that victims of crime are com-
pensated by those who perpetrate crimes against them, for injuries
and losses to their person and private property; and that capital
punishment be meted out to those whose crimes are capital in
nature.

Jails aren’t Biblical. Restitution is Biblical. Jails are training
centers for crime, and they will soon become distribution centers
for AIDS.

Fines to the State aren’t Biblical, except to be used to compen-
sate victims of unsolved criminal acts, Criminals should pay
money to their victims, not to the State. Crime is personal; com-
pensation should be personal. The State is to promote justice, not
to become a self-financing bureaucracy.

As I've already said, you're not going to be able to get the

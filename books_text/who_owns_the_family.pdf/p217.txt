What Are Biblical Blueprints? 191

Many Christians still believe something dangerously close to
Marcionism: not a two-Gods view, exactly, but a God-who-
changed-all-His-rules sort of view. They begin with the accurate
teaching that the ceremonial laws of the Old Testament were ful-
filled by Christ, and therefore that the unchanging principles of Bibli-
cal worship are applied differently in the New Testament. But then
they erroneously conclude that the whole Old Testament system
of civil law was dropped by God, and nothing Biblical was put in its
place. In other words, God created a sort of vacuum for state law.

This idea turns civil law-making over to Satan. In our day,
this means that civil law-making is turned over to humanists.
Christians have unwittingly become the philosophical allies of the humanisis
with respect to civil law. With respect to their doctrine of the state,
therefore, most Christians hold what is in effect a two-Gods view
of the Bible.

Gnosticism’s Dualism

Another ancient heresy that is still with us is gnosticism. It
became a major threat to the early church almost from the begin-
ning. It was also a form of dualism, a theory of a radical split. The
gnostics taught that the split is between evil matter and good
spirit. Thus, their goal was to escape this material world through
other-worldly exercises that punish the body. They believed in re-
treat from the world of human conflicts and responsibility. Some of these
ideas got into the church, and people started doing ridiculous
things. One “saint” sat on a platform on top of a pole for several
decades. This was considered very spiritual. (Who fed him? Who
cleaned up after him?)

Thus, many Christians came to view “the world” as something
permanently outside the kingdom of God. They believed that this
hostile, forever-evil world cannot be redeemed, reformed, and re-
constructed. Jesus didn’t really die for it, and it can’t be healed. At
best, it can be subdued by power (maybe). This dualistic view of
the world vs. God’s kingdom narrowly restricted any earthly man-
ifestation of God’s kingdom. Christians who were influenced by
gnosticism concluded that God’s kingdom refers only to the insti-

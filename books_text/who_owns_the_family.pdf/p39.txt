A Covenant, Not a Contract 13

Summary

What have I covered in this chapter? I have tried to answer
the question, “Who owns the family?”

The correct answer is God!

1, I began by pointing out that the Supreme Court has
already recognized this “institutional” status of the family by “es-
sentially” declaring it a covenant: a covenant created by God and
patterned after His covenant with man.

2, Next, I showed that Deuteronomy breaks the covenant
into five parts:

. Transcendence: Rising above man

. Hierarchy: God’s authoritative chain-of- command
. Ethics: God’s laws of faithfulness

. Sanctions: Special “self-maledictory oath”

. Continuity: Bond based on God’s covenant

AP ene

3. Then, I developed the parallel between God’s covenant
with man and the marital covenant. The Bible actually calls mar-
riage a covenant, and the five-fold division carries through.

. Transcendence: Marriage created by God

. Hierarchy: Man is head over the woman

. Ethics: Family is governed by God’s commands
. Sanctions: Marital covenant created by oath

. Continuity: Bond based on covenant

om oN

One other concluding point should be made. The structure of
the principles of this book basically follow the covenantal pattern.
Also, since Deuteronomy is the second giving of God’s “Ten Com-
mandment Covenant,” the commandments themselves track this
covenantal pattern twice. The first five commandments are God-
ward, and the second five are man-ward. So, I will develop the
principles two times, once in a general God-ward direction, and
second with a man-ward emphasis.

In this chapter, I have concentrated on the fact that marriage
is a “Divinely created bond,” and not just an invention of man.
Although the entire covenant model has been presented, it has

By What Standard? 31

Failure to worship God is worse than murder! It’s worse than
adultery! It’s worse than stealing!

The writer to the Hebrews says, “Let us draw near (to the
throne of God) with a true heart in full assurance of faith, having
our hearts sprinkled from an evil conscience and our bodies washed
with pure water. Let us hold fast the confession of our hope without
wavering, for He who promised is faithful. And let us consider one
another in order to stir up love and good works, not forsaking the
assembling of ourselves together (in worship), as is the manner of some,
but exhorting one another, and so much the more as you see the
Day approaching” (Hebrews 10:22-25). (emphasis added)

The highest privilege of every family is worship. Parents
should take their children to Church so that they can worship as a
family. The family that “prays together stays together.” But fami-
lies should also worship together in the home. Worship should be
at the top of activities for the family.

Commandment 3

The third section of a Biblical covenant establishes the prin-
ciples of obedience, It is concerned with law.

This commandment is a little more difficult to apply. God says
that His name should not be taken in “vain.” Does this have to do
with “cussing”? Although I do not think it is good to use profanity,
TI don’t think that’s what God is referring to here, except indirectly.

The same Hebrew word is used by Job when he says, “For He
[God] knows deceitful [“vain”] men; He sees wickedness also” (Job
it:il), “Vanity” in this verse is parallel with “wickedness.” So, to
take the Lord’s name in “vain” means to be deceitful with His name.
How would this be done? For instance, it would mean swearing to
something, and trying to add power or authority to your testimony
by using God’s name. You would be calling down God's authority
to back up your word, as if He comes at man’s beck and call. God’s
name cannot be manipulated for men’s purposes.

A Biblical example is the case where some Jewish exorcists
tried to use Jesus’ name to cast out demons. But since God’s name
cannot be used “deceptively,” nor should it be applied in a manip-

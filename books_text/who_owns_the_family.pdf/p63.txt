By What Standard? 37

consider theft. This commandment refers to the unlawful attempt
to take another's entire “estate”: wife, animals, property, house
{real estate), etc. This commandment nips in the bud both theft
and adultery. It acknowledges that the eye is the seat of sin, which
is why Jesus warned, “If your eye offends you, pluck it out” (Mat-
thew 5:29). If it leads you to sin, then better to be blind. In short,
sin is a terrible evil, with consequences worse than blindness.

There is nothing wrong with wanting a better life. But families
should teach their children that covetousness is sin. To focus on
another’s possessions to the extent that the covetous person con-
siders theft or oppression of the other person’s estate (“patrimony”)
is a soul-destroying lust. Since our culture is a welfare society, fos-
tered by a covetous civil government, it is a world of lusting and
coveting. Children should learn early on that they should work for
their own possessions, and take their “eyes” off of what the other
children have.

Conclusion

The Ten Commandments present a double witness to God’s
covenant structure. They call men to obey a God who delivers
them from bondage. To refuse to obey Him is to accept moral
bondage as a way of life. The task of parents is to raise up a gener-
ation that wants moral freedom to obey God. The Ten Com-
mandments are the standard for every family.

Summary

Every family needs a standard. God’s revealed standard un-
dergirds the lawful, God-given independence of the family as a
separate covenant structure.

T have tried to.answer one basic question: By What Standard?

1. I started with the case of Nebraska ex rel. Douglas » Faith
Baptist Church. What was at stake? The standard by which parents
are allowed to raise their children. What is the standard? The Ten
Commandments.

2. I briefly summarized the commandments as they would
apply to the family.

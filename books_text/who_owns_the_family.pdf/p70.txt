44 Who Owns the Family?

capital offense: murder, There are other capital crimes. Adultery
is one of them (Leviticus 18:20). Adultery is not always punish-
able by death, even though the Bible allows for this punishment at
the request of the injured spouse. Matthew says, “Joseph was a
just man... and minded to put Mary away privately” (Matt.
1:19). He was “just” and able to avoid the death penalty. Obvi-
ously, the offense of adultery did not require capital punishment
in this case.

As mentioned, the only exception to the “not mandatory” prin-
ciple is murder. Why? Death is the only appropriate restitution in
the event of homicide. So, the incorrigible teenager is not necessar-
ily put to death in every case, only in an “unreformable” one prob-
ably where extreme violence would be involved.

We should also keep in mind that before Christ comes in his-
tory, redemption is not fully at work. The death penalty was to be
more rigidly applied. But after the death of Christ, the possibility
of reform is greater. A Christian approach to incorrigible teen-
agers should lessen the need for the death penalty. A non-Biblical
humanistic approach—“children will be children’—is guaranteed
to increase the need for the death penalty, if not when they are
children, then later, when they become murderers.

The State should uphold the law of God in the family. Parents
are not unreasonable when they expect their children, especially
teenagers, to obey. Parents are not over-demanding when they
demand that their children stay sober and stay away from drunk-
enness and drugs.

‘Too often, however, when the State is appealed to, # assumes the
role of parent. In the Bible, the State carries out a penalty for the
parents that they were not allowed to enforce: execution. The family
is not a servant of the State. The State is to be a servant of the family.

Who owns discipline? Ultimately, “God does,” as we have seen in
every area. Yes, He has entrusted it to the parents, but we should
keep our argument straight. Parents are given the power to punish,
but the Bible lays down strict guidelines. God entrusting parents
with discipline in the home does not mean they can do anything
they want! There are five Biblical methods. Let us consider them.

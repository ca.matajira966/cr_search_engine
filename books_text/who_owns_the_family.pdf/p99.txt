7
WHO OWNS SEXUAL PRIVACY?

I know, sex is one of the subjects, like religion and politics,
that you’re not supposed to talk about. (It’s a shame that people
don’t think about religion and politics as often.)

But these days, everybody on the TV talk shows seems to be
discussing the matter. So is the State,

You might be thinking, “Why in the world would the State be
concerned about the issue of sex?”

I think that's a good question, because it is rather strange that
the State would venture into such “private matters.” What's going
on? The State realizes that this is another one of those areas that
determines who owns the family.

How so?

In People v. Onofre (1980), the New York Court of Appeals ex-
tended the “constitutional right of privacy” to guard the right of
unmarried adults to seek “sexual gratification.”

Catch the implication?

Constitutional lawyer John Whitehead, in Parent’s Rights, ex-
plains what the whole issue of sexual privacy has to do with the
family. He says,

“While sexual privacy may at first seem unrelated to the issue
of family forms, this case (People ». Onofre) was a key factor in the
subsequent decision of a lower New York court in 1981 to allow
one adult male to adopt another adult male. On a variation of the
privacy theory, the Pennsylvania Supreme Court has given con-
stitutional protection to sex acts performed in a public lounge be-
tween dancing performers and lounge patrons.

73

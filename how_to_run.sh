#! /bin/bash
set -eu

cd database && docker-compose up -d
sleep 10
python3 ./upload_data_to_db.py

cd ../web/
./launch.sh
